<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Twilio\Rest\Client;

class CmsController extends AppController
{
    protected $_futureDate = '';

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }
    public function keyfeatures()
    {
        $staticPagesTable = TableRegistry::get('StaticPages');
        $data = $staticPagesTable->find('all')->where(['id'=> 6])->toArray();        
        $this->set('data', $data);
    }

   
}
