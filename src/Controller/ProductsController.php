<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[] paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['alreadyExisteditProductName1','alreadyExistProductName1','deletechild']);
    }
    public function index()
    {
        
       $page = 0;
       if(isset($_GET['category_id']) && ! isset($_GET['query']))
        {
            
                           $condition = [];
                             
                            
                            if(isset($_GET['category_id']) && ! empty($_GET['category_id'])){
                                $filterby = $_GET['category_id'];
                                 $condition = ['category_id'=>$_GET['category_id'],'Products.status'=>1];
                              }

                             if(isset($_GET['page']) && ! empty($_GET['page'])){
                                $page = $_GET['page'];
                              } 
                            $this->paginate = [
                             'sortWhitelist' => [
                                'Categories.name','Units.name','Products.price_per_unit','Products.name','Products.quantity'
                              ],
                            'limit' => 10,
                            'page'=> $page,
                            'contain' => ['Categories', 'Units'],
                            'order' => [
                                'Categories.id' => 'desc'
                            ],
                            'conditions'=>$condition
                        ];
                        $products = $this->paginate($this->Products);
                        $this->set(compact('products'));
                        $this->set('category_id',$_GET['category_id']); 
                        $categoriesTable = TableRegistry::get('Categories');
                        $categories = $categoriesTable->find('list')->toArray();
                        $this->set('categories',$categories);

        }else if(isset($_GET['query'])){
                 
                 if(isset($_GET['page']) && ! empty($_GET['page'])){
                                $page = $_GET['page'];
                              } 

                        $condition = [

                                          'OR' => [ 

                                             'Products.name LIKE'=>'%'.$_GET['query'].'%',
                                           ],
                                           'Products.status'=>1
                                        ];
                                       $this->paginate = [
                                        'sortWhitelist' => [
                            'Categories.name','Units.name','Products.price_per_unit','Products.name','Products.quantity'
                          ],
                                                      'limit' => 8,
                                                      'page'=> $page,
                                                      'order'=>['Products.id DESC'],
                                                      'conditions'=>$condition,
                                                      'contain' => ['Categories', 'Units']
                                                               
                                                  ]; 
                        $products = $this->paginate($this->Products);
                        $this->set(compact('products'));
                        $categoriesTable = TableRegistry::get('Categories');
                        $categories = $categoriesTable->find('list')->toArray();
                        $this->set('categories',$categories);                   
                       if(isset($_GET['query']) && !empty($_GET['query'])){
                          $this->set('querystring',$_GET['query']);
                        } 

            }else{    
                $this->paginate = [
                    'sortWhitelist' => [
                      'Categories.name','Units.name','Products.price_per_unit','Products.name','Products.quantity'
                    ],
                    'contain' => ['Categories', 'Units'],
                    'limit'=>10,
                    'conditions'=>['Products.status'=>1],
                    'order'=>['Products.id DESC']
                ];
                $products = $this->paginate($this->Products);
                $this->set(compact('products'));
                $categoriesTable = TableRegistry::get('Categories');
                $categories = $categoriesTable->find('list')->toArray();
                $this->set('categories',$categories);
            }
       
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $product = $this->Products->get($id, [
            'contain' => ['Categories', 'Units', 'ComplaintFeedbackSuggestions', 'CustomOrders', 'ProductChildren', 'Sales', 'UserSubscriptions']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);*/
    }

    private function alreadyExistProductName( $prodctName ){

      $productTable = TableRegistry::get('Products');
      $product = $productTable->find('all')->where(['name'=>trim($prodctName)])           ->toArray();                                
      if(count($product)){
        return true;
      } 
      return false;         

    }
    private function checkValuesEachArray( $chield ){
     
      foreach ($chield as $key => $value) {

               if($value == ''){
                return false;
               }
       }
      return true;
    }
    private function validateChieldProduct( $qtys, $units, $prices ){
              if( $this->checkValuesEachArray($qtys) ){
                  if( $this->checkValuesEachArray($units) ){                        
                    if( $this->checkValuesEachArray($prices) ){
                       return false;
                    }
                  }
              }
              return true;
    }
        
    private function validateAddProduct( $data ){

        $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = "PLease enter the Product name";
        }else if( $this->alreadyExistProductName( $data['name'] ) ){
            $error['name'] = 'Product Name already exist';
        }else if( ! isset( $data['quantity'] ) || empty( $data['quantity'] ) ){
            $error['name'] = "PLease enter the quantity";
        }else if( ! preg_match('/^\d+$/',$data['quantity']) ){
            $error['name'] = "PLease enter the valid quantity Number";
        }else if( ! isset( $data['category_id'] ) || empty( $data['category_id'] ) ){
            $error['name'] = "PLease select the category";  
        }else if( ! preg_match('/^\d+$/',$data['category_id']) ){
            $error['name'] = "PLease enter the valid category";
        }else if( ! isset( $data['unit_id'] ) || empty( $data['unit_id'] ) ){
            $error['name'] = "PLease select the unit";  
        }else if( ! preg_match('/^\d+$/',$data['unit_id']) ){
            $error['name'] = "PLease enter the valid unit";
        }else if( ! isset( $data['price_per_unit'] ) || empty( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the Price Per Unit";
        }else if( ! ctype_digit( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the valid Price Per Unit";
        } 
 
        if( count( $error ) > 0 ){
            $error['statuscode'] = 201;
        }else{
            if(
                ( isset($data['childprice'][0]) || isset($data['childunit'][0]) || isset($data['childqty'][0]) ) 
                &&
                ( 
                    ! empty($data['childqty'][0])
                    ||
                    ! empty($data['childprice'][0])
                    ||
                    ! empty($data['childunit'][0])  

                )

                ){
                
                if( $this->validateChieldProduct( $data['childqty'], $data['childunit'], $data['childprice']) ){
                   $error['name'] = "PLease check the all chiled product add values properly";
                }
            }
            if(count($error)>0){
            $error['statuscode'] = 201;    
            }else{
            $error['statuscode'] = 200;
            }
        } 
        return $error;               

    }

    private function alreadyExisteditProductName( $prodctName,$pro_id ){

      $productTable = TableRegistry::get('Products');
      $product = $productTable->find('all')
                             ->where(['id <>'=>$pro_id,'name'=>trim($prodctName)])->toArray();
                                
      if(count($product)){
        return true;
      } return false;         

    }
    public function alreadyExisteditProductName1(){
      if( $this->request->is('post') ){

        $data         = $this->request->getData();
        $pro_id       = $data['pro_id'];
        $prodctName   = $data['prodctName'];
        $productTable = TableRegistry::get('Products');
        $product      = $productTable->find('all')
                        ->where(['id <>'=>$pro_id,'name'=>trim($prodctName)])->toArray();
                                  
        if(count($product)){
          echo "found"; die;
        } else{
          echo "not found"; die;
        } 
      }

    }
    public function alreadyExistProductName1(){
      if( $this->request->is('post') ){
        $data = $this->request->getData();
        $prodctName = $data['prodctName'];
         $productTable = TableRegistry::get('Products');
         $product = $productTable->find('all')
                                 ->where(['name'=>trim($prodctName)])->toArray();                                  
            if(count($product)){
              echo "found"; die;
            } else{
              echo "not found"; die;
            }  
      }                         

    }

    private function validateEditProduct( $data ){

        $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = "PLease enter the Product name";
        }else if( $this->alreadyExisteditProductName( $data['name'],$data['pro_id'] ) ){
            $error['name'] = 'Product Name already exist';
        }else if( ! isset( $data['quantity'] ) || empty( $data['quantity'] ) ){
            $error['name'] = "PLease enter the quantity";
        }else if( ! preg_match('/^\d+$/',abs($data['quantity'])) ){
            $error['name'] = "PLease enter the valid quantity Number";
        }else if( ! isset( $data['category_id'] ) || empty( $data['category_id'] ) ){
            $error['name'] = "PLease select the category";  
        }else if( ! preg_match('/^\d+$/',$data['category_id']) ){
            $error['name'] = "PLease enter the valid category";
        }else if( ! isset( $data['unit_id'] ) || empty( $data['unit_id'] ) ){
            $error['name'] = "PLease select the unit";  
        }else if( ! preg_match('/^\d+$/',$data['unit_id']) ){
            $error['name'] = "PLease enter the valid unit";
        }else if( ! isset( $data['price_per_unit'] ) || empty( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the Price Per Unit";
        }else if( ! ctype_digit( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the valid Price Per Unit";
        }
         if( count( $error ) > 0 ){
          $error['statuscode'] = 201;
        }else{
            if(
                ( isset($data['childprice'][0]) || isset($data['childunit'][0]) || isset($data['childqty'][0]) ) 
                &&
                ( 
                    ! empty($data['childqty'][0])
                    ||
                    ! empty($data['childprice'][0])
                    ||
                    ! empty($data['childunit'][0])  

                )

                ){
                
                if( $this->validateChieldProduct( $data['childqty'], $data['childunit'], $data['childprice']) ){
                   $error['name'] = "PLease check the all chiled product add values properly";
                }
            }
            if(count($error)>0){
            $error['statuscode'] = 201;    
            }else{
            $error['statuscode'] = 200;
            }
        }
        return $error;
    }
    public function edit($id = null)
    {        
              if($this->request->is('post')){
                   
                $data = $this->request->getData();
                $imagename = ''; 
                $image= '';
                if($_FILES && $_FILES['image']['error'] == 0){
                    $image = true;
                      
                       $imagename = $this->uploadImage($_FILES);
                        
                       if($imagename != 1){
                        $this->request->data['image'] = $imagename; 
                       }else{
                        $this->request->data['image'] = NULL;
                       }

                }
                $error = $this->validateEditProduct($data);                
              if( $error['statuscode'] == 200 ){

                $productTable = TableRegistry::get('Products');
                $product = $productTable->get($data['pro_id']);
                $product->category_id = $data['category_id'];
                
                $product->name = $data['name'];
                $product->price_per_unit = $data['price_per_unit'];
                $product->quantity = $data['quantity'];
                $product->unit_id = $data['unit_id'];
                if($image){            
                $product->image = $imagename;
                }
                $product->status = 1;
                 if(isset($data['is_subscribe']) && !empty($data['is_subscribe'])){

                       if($data['is_subscribe'] == "yes"){
                        $product->is_subscribable = 1;
                      }else{
                        $product->is_subscribable = 0;
                      }
                }

                if(isset($data['iscontainer']) && !empty($data['iscontainer'])){

                       if($data['iscontainer'] == 1){
                        $product->iscontainer = 1;
                      }else{
                        $product->iscontainer = 0;
                      }
                }

                $product->modified = date('Y-m-d h:i:s');
                $product->description = $data['description'];
                $result = $productTable->save($product);

                $finalSaved = '';
                    if(
                        $result 
                        && 
                        ( 
                          isset($data['childqty'][0]) && ! empty($data['childqty'][0])
                          &&
                          isset($data['childprice'][0]) && ! empty($data['childprice'][0])
                          &&
                          isset($data['childunit'][0]) && ! empty($data['childunit'][0])
                          &&
                          isset($data['childstock'][0]) && ! empty($data['childstock'][0])
                        )
                     )
                           {
                                
                                $productchildrenTable = TableRegistry::get('ProductChildren'); 
                                /*$productchildrenTable->deleteAll(['ProductChildren.product_id' => $data['pro_id']]);*/  
                                 
                                for($i=0;$i<count($data['childqty']);$i++){
                                      $productChieldren = $productchildrenTable->newEntity();
                                      $productChieldren->id = @$data['pro_child_id'][$i];
                                      $productChieldren->product_id = $result->id;
                                      $productChieldren->quantity = $data['childqty'][$i];
                                      $productChieldren->in_stock = $data['childstock'][$i];
                                      $productChieldren->unit_id = $data['childunit'][$i];
                                      $productChieldren->price = $data['childprice'][$i];

                                      $finalSaved = $productchildrenTable->save($productChieldren); 
                                } 

                            
                    }else{
                      $productchildrenTable = TableRegistry::get('ProductChildren'); 
                      $productchildrenTable->deleteAll(['ProductChildren.product_id' => $data['pro_id']]); 
                    }

                 if($finalSaved || $result){
                    $productAreasTable = TableRegistry::get('ProductAreas');
                    $productAreasTable->deleteAll(['product_id' => $data['pro_id']]);
                    if($result && @$data['areas']){

                      foreach ($data['areas'] as $key => $value) {
                        
                          $productarea = $productAreasTable->newEntity();
                          $productarea->product_id = $result->id;
                          $productarea->area_id = $value;
                          $result1 = $productAreasTable->save($productarea);
                        
                      }
                    }
                  $error['statuscode'] = 200;
                  $error['name'] = "Product Has been saved successfully";
                  $this->Flash->success(__('Product Has been updated successfully.'));
                  return $this->redirect(['action' => 'index']);
                  

                 }
         }else{
            header("Refresh:0");
            $this->Flash->success(__('Error'));
         }
         }else{
             $id = base64_decode($id);
             $product = $this->Products->find()->where(['Products.id'=>$id])->contain(['ProductChildren'])->first();
             $this->set('data',$product);
             $categories = $this->Products->Categories->find('list')->toArray();
             $units = $this->Products->Units->find('list');
             $DeliverySchdules = $this->Products->DeliverySchdules->find('list');

            $areasTable = TableRegistry::get('Areas');                    
            $areas =  $areasTable->find('list')->select(['id','name'])->hydrate(false)->toArray();   
             $productAreasTable = TableRegistry::get('ProductAreas');
             $productArea = $productAreasTable->find('list',['keyField'=>'id','valueField'=>'area_id'])->where(['product_id'=>$id])->toArray();
              
             $this->set(compact('categories', 'units','DeliverySchdules','productArea','areas'));
             }

    }
    
    /* upload image */

    private function uploadImage( $image ){
            $error = 0;
            if (isset($_FILES["image"])) {
            $tmpFile = $_FILES["image"]["tmp_name"];
            $extension = $this->getExtension($_FILES['image']['name']);
            $tme = time();
            $fileName = IMG_PATH.$tme.'.'.$extension;

            $extensionArray = array('jpg','jpeg','png');
            
            list($width, $height) = getimagesize($tmpFile);
             
            if ($width == null && $height == null) {
                $error = 1;
                
            }
             
            if ( ! in_array($extension, $extensionArray) ) {
                
                $error = 1; 
            }
            else {

                if( move_uploaded_file($tmpFile, $fileName) ){
                    $error = IMG_PATHS.$tme.'.'.$extension; 
                }
            }
        }
        return $error;
 }

    /* upload image */

function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


    public function add()
    {
        
        
        if ($this->request->is('post')) {

             

            $data = $this->request->getData();
            
            $imagename = ''; 
            if($_FILES && $_FILES['image']['error'] == 0){
                  
                   $imagename = $this->uploadImage($_FILES);
                    
                   if($imagename != 1){
                    $this->request->data['image'] = $imagename; 
                   }else{
                    $this->request->data['image'] = NULL;
                   }

            }
            
            $error = $this->validateAddProduct($data);
            
            if( $error['statuscode'] == 200 ){
                    $productTable = TableRegistry::get('Products');
                    $product = $productTable->newEntity();
                    $product->category_id = $data['category_id'];
                    $product->name = $data['name'];
                    $product->image = $imagename; 
                    $product->price_per_unit = $data['price_per_unit'];
                    $product->quantity = $data['quantity'];
                    $product->unit_id = $data['unit_id'];
                    $product->status = 1;
                    $product->description = $data['description'];
                     if(isset($data['is_subscribe']) && !empty($data['is_subscribe'])){

                           if($data['is_subscribe'] == "yes"){
                            $product->is_subscribable = 1;
                          }else{
                            $product->is_subscribable = 0;
                          }
                    }

                    if(isset($data['iscontainer']) && !empty($data['iscontainer'])){

                           if($data['iscontainer'] == 1){
                            $product->iscontainer = 1;
                          }else{
                            $product->iscontainer = 0;
                          }
                    }


                    $product->created = date('Y-m-d h:i:s');
                    $product->modified = date('Y-m-d h:i:s');
                    $result = $productTable->save($product);
                    $resultschdule = '';
                    $finalSaved = '';
                    if(
                        $result 
                        && 
                        ( 
                          isset($data['childqty'][0]) && ! empty($data['childqty'][0])
                          &&
                          isset($data['childprice'][0]) && ! empty($data['childprice'][0])
                          &&
                          isset($data['childunit'][0]) && ! empty($data['childunit'][0])
                          &&
                          isset($data['childstock'][0]) && ! empty($data['childstock'][0])
                        )
                    )
                           {

                                $productchildrenTable = TableRegistry::get('ProductChildren'); 
                                  
                                 
                                for($i=0;$i<count($data['childqty']);$i++){
                                      $productChieldren = $productchildrenTable->newEntity();
                                      $productChieldren->product_id = $result->id;
                                      $productChieldren->quantity = $data['childqty'][$i];
                                      $productChieldren->in_stock = $data['childstock'][$i];
                                      $productChieldren->unit_id = $data['childunit'][$i];
                                      $productChieldren->price = $data['childprice'][$i];

                                      $finalSaved = $productchildrenTable->save($productChieldren); 
                                } 

                            
                    }

                    if($result && @$data['areas']){
                      foreach ($data['areas'] as $key => $value) {
                         
                        $productAreasTable = TableRegistry::get('ProductAreas');
                        $productarea = $productAreasTable->newEntity();
                        $productarea->product_id = $result->id;
                        $productarea->area_id = $value;
                        $result1 = $productAreasTable->save($productarea);
                      }
                    }

                 if($finalSaved || $result){
                  //$error['statuscode'] = 200;
                  $error = "Product Has been saved successfully";
                  header("Refresh:0");
                  $this->Flash->success(__('Product Has been saved successfully.'));
                  

                 }
                    

            }else{
                header("Refresh:0");
                $this->Flash->success(__($error));

            }
             
        }else{
                
                $regionTable = TableRegistry::get('Regions');
                $areasTable = TableRegistry::get('Areas');
              $categoriesTable = TableRegistry::get('Categories');
                   $categories = $this->Products->Categories->find('list')->toArray();
                   
                $units = $this->Products->Units->find('list');
                $DeliverySchdules = $this->Products->DeliverySchdules->find('list');
                $region_area_list = $regionTable->find()
                                        ->contain(['areas'])
                                        ->hydrate(false)
                                        ->join([
                                        'table' => 'areas',
                                        'alias' => 'areas',
                                        'type' => 'INNER',
                                        'conditions' => 'Regions.id = areas.region_id',
                                    ])->toArray();
                $areas =  $areasTable->find('list')->select(['id','name'])->hydrate(false)->toArray();                      
                $this->set(compact('product', 'categories', 'units','DeliverySchdules','region_area_list','areas'));
                
            }
    }

     

   
    public function delete($id = null)
    {
       
        $id = base64_decode($id);
         
        $products = $this->Products->get($id);
        
        $products->status = 0;
        if ($this->Products->save($products)) {
            $this->Flash->success(__('The products has been deleted.'));
        } else {
            $this->Flash->error(__('The products could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);

      
    }
    public function deletechild($id = null)
    {
      $id = $_POST['pro_id'];
      $productchildrenTable = TableRegistry::get('ProductChildren');                
      /*$products = $this->Products->get($id);
      $productchildrenTable->delete(['ProductChildren.id' => $products]);*/ 
      $query = $productchildrenTable->query();
      $query->delete()
          ->where(['id' => $id])
          ->execute();
    die('deleted');
    }
}
