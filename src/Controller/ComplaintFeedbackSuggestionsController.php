<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ComplaintFeedbackSuggestions Controller
 *
 * @property \App\Model\Table\ComplaintFeedbackSuggestionsTable $ComplaintFeedbackSuggestions
 *
 * @method \App\Model\Entity\ComplaintFeedbackSuggestion[] paginate($object = null, array $settings = [])
 */
class ComplaintFeedbackSuggestionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

            
        if (isset($_GET['start_date']) || (@$_GET['end_date']) || (@$_GET['type']) ) {

            if(@$_GET['start_date'] != '' && @$_GET['end_date'] != '' && @$_GET['type'] != ''){
                $condition[] = [
                                  'AND' => [ 
                                     'ComplaintFeedbackSuggestions.date >='=>@$_GET['start_date'], 'ComplaintFeedbackSuggestions.date <=' => @$_GET['end_date'], 'ComplaintFeedbackSuggestions.subject LIKE' =>  "%". @$_GET['type'] ."%"
                                   ]
                                ];
            }else if(@$_GET['start_date'] != '' && @$_GET['end_date'] != ''){
                $condition[] = [
                                  'AND' => [ 
                                     'ComplaintFeedbackSuggestions.date >='=>@$_GET['start_date'], 'ComplaintFeedbackSuggestions.date <=' => @$_GET['end_date']
                                   ]
                                ];
            }else if(@$_GET['start_date'] == '' && @$_GET['end_date'] == '' && $_GET['type']== ''){
                $condition[] = [];
            }else if(@$_GET['type']!= '' && @$_GET['start_date'] == '' && @$_GET['end_date'] == ''){
                $condition[] = [
                                  'AND' => ['ComplaintFeedbackSuggestions.subject LIKE' => "%". @$_GET['type'] ."%"
                                   ]
                                ];
            }else{
                $condition[] = [
                                  'AND' => ['ComplaintFeedbackSuggestions.subject LIKE' => @$_GET['type']
                                   ]
                                ];
            }
            $this->paginate = [
                'contain' => ['Users'],
                'order' => [
                                    'id' => 'desc'
                                ],
                'conditions'    => $condition,                
            ];
            $complaintFeedbackSuggestions = $this->paginate($this->ComplaintFeedbackSuggestions);
            $productsTable= TableRegistry::get('Products');
            $products = $productsTable->find()->select(['id','name'])->toArray();
            $this->set(compact('complaintFeedbackSuggestions','products'));
            $this->set('_serialize', ['complaintFeedbackSuggestions']);
        }else{
            $this->paginate = [
                'contain' => ['Users'],
                'order' => [
                                    'id' => 'desc'
                                ],
            ];
            $complaintFeedbackSuggestions = $this->paginate($this->ComplaintFeedbackSuggestions);
            $productsTable= TableRegistry::get('Products');
            $products = $productsTable->find()->select(['id','name'])->toArray();
            $this->set(compact('complaintFeedbackSuggestions','products'));
            $this->set('_serialize', ['complaintFeedbackSuggestions']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $id             = base64_decode($_GET['id']);
        $compTable      = TableRegistry::get('ComplaintFeedbackSuggestions');
        $complaints     = $compTable->find('all')->where(['id' => $id])->toArray();
        $repliesTable   = TableRegistry::get('Replies');
        $replies        = $repliesTable->find('all')->where(['complaint_feedback_suggestion_id' => $id]);
        $replies        = $this->paginate($replies)->toArray();

        $this->set('replies', $replies);
        $this->set(compact('replies', 'complaints'));
        $this->set('_serialize', ['replies']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->newEntity();
        if ($this->request->is('post')) {
            $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->patchEntity($complaintFeedbackSuggestion, $this->request->getData());
            if ($this->ComplaintFeedbackSuggestions->save($complaintFeedbackSuggestion)) {
                $this->Flash->success(__('The complaint feedback suggestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The complaint feedback suggestion could not be saved. Please, try again.'));
        }
        $users = $this->ComplaintFeedbackSuggestions->Users->find('list', ['limit' => 200]);
        $products = $this->ComplaintFeedbackSuggestions->Products->find('list', ['limit' => 200]);
        $this->set(compact('complaintFeedbackSuggestion', 'users', 'products'));
        $this->set('_serialize', ['complaintFeedbackSuggestion']);
    }

    public function markResolve(){
        if($_REQUEST){
            $id = base64_decode($_REQUEST['complaint_id']);
            $c_f_s = $this->ComplaintFeedbackSuggestions->get($id);
            $c_f_s->status = 1;
            $this->ComplaintFeedbackSuggestions->save($c_f_s);
            echo 200;die;
        }
    }
    public function replyComment(){
        date_default_timezone_set('Asia/Kolkata');
        if($_REQUEST){
            
            $user_id = base64_decode($_REQUEST['reciever_reciever_id']);
            $comment = $_REQUEST['reply_comment'];
            if($user_id && ! empty($user_id)){
             
               $userTable = TableRegistry::get('Users');
               $deviceId  = $userTable->find()->select(['phoneNo'])->where(['id'=>$user_id])->first();   
                     $id = base64_decode($_REQUEST['c_f_s']);
                     $c_f_s = $this->ComplaintFeedbackSuggestions->get($id);
                     $c_f_s->status = 0;
                     $this->ComplaintFeedbackSuggestions->save($c_f_s);
                     $replyTable    = TableRegistry::get('Replies');
                     $reply   = $replyTable->newEntity();
                     $reply->subject                            = $comment;
                     $reply->content                            = $comment;
                     $reply->complaint_feedback_suggestion_id   = $id;
                     $reply->datetime                           = date('Y-m-d H:i:s');
                     $result                                    = $replyTable->save($reply);

                     if($deviceId['phoneNo'] != ''){
                        $this->sendSMS($deviceId['phoneNo'],$comment);
                     }
                    echo 200;die;
            }
        }
    }

    private function sendFCM($deviceid,$message){
        
        $fcmApiKey      = 'AIzaSyDikqISQc4nza3yw2FqIBPmoiR4-2Xy314';       
        $url            = 'https://fcm.googleapis.com/fcm/send'; 
        $deviceId       = $deviceid;        
        $messageInfo    = $message;        
        $dataArr        = array();
        $id             = array();
        $id[]           = $deviceId;

        $dataArr['device_id']   = $id;
        $dataArr['message_key'] = $messageInfo; 
        $registrationIds        = $dataArr['device_id'];  
        $message                = $dataArr['message_key']; 
        $title                  = 'This is a title. title';
        $msg                    = array('message' => $message,'title'=>$title);
        $fields                 = array(
                                        'registration_ids' => $registrationIds,
                                        'data' => $msg,
                                        'priority'      =>'high'
                                    );  
        $headers                = array(
                                        'Authorization: key=' . $fcmApiKey,
                                        'Content-Type: application/json'
                                    ); 
        $ch                     = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $url );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );        
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }else{
            
        } 
        curl_close($ch);   
        $res = json_decode($result);
        if($res->success == 1){     
        return true;
        }else{
            return false;
        }
    
    }


    /**
     * Edit method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->patchEntity($complaintFeedbackSuggestion, $this->request->getData());
            if ($this->ComplaintFeedbackSuggestions->save($complaintFeedbackSuggestion)) {
                $this->Flash->success(__('The complaint feedback suggestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The complaint feedback suggestion could not be saved. Please, try again.'));
        }
        $users = $this->ComplaintFeedbackSuggestions->Users->find('list', ['limit' => 200]);
        $products = $this->ComplaintFeedbackSuggestions->Products->find('list', ['limit' => 200]);
        $this->set(compact('complaintFeedbackSuggestion', 'users', 'products'));
        $this->set('_serialize', ['complaintFeedbackSuggestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->get($id);
        if ($this->ComplaintFeedbackSuggestions->delete($complaintFeedbackSuggestion)) {
            $this->Flash->success(__('The complaint feedback suggestion has been deleted.'));
        } else {
            $this->Flash->error(__('The complaint feedback suggestion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
