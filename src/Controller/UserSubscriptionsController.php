<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * UserSubscriptions Controller
 *
 * @property \App\Model\Table\UserSubscriptionsTable $UserSubscriptions
 *
 * @method \App\Model\Entity\UserSubscription[] paginate($object = null, array $settings = [])
 */
class UserSubscriptionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['CustomOrders', 'addToNextDelivery', 'deletecustomorder', 'editOrder']);
    }
    public function index()
    {

        $this->paginate = [
            'contain' => ['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses', 'Users', 'Units'],
        ];
        $userSubscriptions = $this->paginate($this->UserSubscriptions);

        $this->set(compact('userSubscriptions'));
        $this->set('_serialize', ['userSubscriptions']);
    }
    public function addBalance()
    {

        if ($_REQUEST) {
            $userid            = $_REQUEST['notaddidbeforethisusergetfrommakeidmohan'];
            $amount            = $_REQUEST['amountnotneedtoberechargeonetime'];
            $UserBalancesTable = TableRegistry::get('UserBalances');
            $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $userid])->select('balance')->first();
            $balanceamount     = $UserBalances['balance'];
            $balanceamount     = $balanceamount + $amount;

            $query  = $UserBalancesTable->query();
            $result = $query->update()
                ->set(['balance' => $balanceamount])
                ->where(['user_id' => $userid])
                ->execute();
            $transactionTable                     = TableRegistry::get('Transactions');
            $transaction                          = $transactionTable->newEntity();
            $transaction->user_id                 = $userid;
            $transaction->amount                  = $amount;
            $transaction->transaction_amount_type = 'Cr';
            $transaction->created                 = date('Y-m-d');
            $date                                 = new \DateTime();
            $transaction->time                    = date_format($date, 'H:i:s');
            if (isset($_REQUEST['r_r'])) {
                $transaction->transaction_type_id = 4;
                $transaction->refund_type_id      = $_REQUEST['r_r'];
            } else {
                $transaction->transaction_type_id = 1;
            }
            if ($transactionTable->save($transaction)) {

                echo "200";die;
            } else {
                echo "202";die;
            }

        } else {
            echo "202";die;
        }
    }

    /**
     * View method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function view($id = null)
    {
        /*$userSubscription = $this->UserSubscriptions->get($id, [
    'contain' => ['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses', 'Users', 'Units']
    ]);

    $this->set('userSubscription', $userSubscription);
    $this->set('_serialize', ['userSubscription']);*/
    }
    public function deleteSubscription()
    {

        $id                 = base64_decode($_GET['id']);
        $subscription_types = TableRegistry::get('subscriptionTypes');
        $entity             = $subscription_types->get($id);
        $result             = $subscription_types->delete($entity);
        if ($result) {
            return $this->redirect(['action' => 'listSubscriptionsType']);
        }

    }

    private function getSubscriptonsTypes()
    {
        $subscription_types = TableRegistry::get('subscriptionTypes');
        $subscription_list  = $subscription_types->find('all')
            ->select(['id', 'subscription_type_name', 'created', 'modified'])
            ->hydrate(false)
            ->toArray();
        return $subscription_list;
    }

    public function changeStatus()
    {

        if ($this->request->is('post')) {
            $requestedata               = $this->request->getData();
            $user_id                    = $requestedata['user_id'];
            $sub_id                     = $requestedata['sub_id'];
            $sub_qty                    = $requestedata['sub_qty'];
            $s_id                       = $requestedata['s_id'];
            $start_date                 = $requestedata['start_date'];
            $pr_price                   = $requestedata['pr_price'];
            $schedule_id                = $requestedata['schedule_id'];
            $subscriptions_total_amount = $pr_price * $sub_qty;

            $userSubTable                        = TableRegistry::get('UserSubscriptions');
            $usersub                             = $userSubTable->get($s_id); // Return article with id 12
            $usersub->subscription_type_id       = $sub_id;
            $usersub->quantity                   = $sub_qty;
            $usersub->delivery_schdule_ids       = $schedule_id;
            $usersub->subscriptions_total_amount = $subscriptions_total_amount;
            $userSubTable->save($usersub);

            $SubscribedContainersTable          = TableRegistry::get('SubscribedContainers');
            $allotedContainers                  = $SubscribedContainersTable->get($user_id);
            $allotedContainers->container_count = $container_count1;
            $allotedContainers->save($allotedContainers);
            echo "updated";
            die;

        }

        if ($_GET) {

            if (isset($_GET['id']) && isset($_GET['type'])) {

                $usersSubscriptionStatusesTable = TableRegistry::get('UsersSubscriptionStatuses');
                $usersSubscriptionTable         = TableRegistry::get('UserSubscriptions');

                $redirectId     = base64_decode($_GET['redirected_ID']);
                $subscriptionId = base64_decode($_GET['id']);

                $userSub        = $usersSubscriptionTable->find()->select(['product_id','quantity'])->where(['UserSubscriptions.id' => $subscriptionId])->first();
                $quantityy      = $userSub['quantity'];
                $unitname       = $this->getUnitNameAndId($userSub['product_id']);
                $product_name   = $unitname['product_name'];
                $unit_name      = $unitname['name'];
                $type           = 1;

                $whatforChange  = $_GET['type'];
                if ($whatforChange == 'paused') {

                    $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'paused'])->first();

                    $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
                    $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
                    $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;

                    $usersSubscription->paused_since = date('Y-m-d h:i
                                :s');

                    if ($usersSubscriptionTable->save($usersSubscription)) {

                        $user_subscription_active_deactive_histories_table                 = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                        $user_subscription_active_deactive_histories                       = $user_subscription_active_deactive_histories_table->newEntity();
                        $user_subscription_active_deactive_histories->user_subscription_id = $subscriptionId;
                        $user_subscription_active_deactive_histories->paused_date          = date('Y-m-d h:i
                                :s');
                        $user_subscription_active_deactive_histories_table->save($user_subscription_active_deactive_histories);
                        $description = "".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";
                        $action      = "Paused"; 
                        $this->savedInActivity($redirectId, $description,$action,$type);
                        $this->Flash->success(__('The subscription has been paused.'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($redirectId)]]);

                    }

                } else if ($whatforChange == 'play') {

                    $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'active'])->first();

                    $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
                    $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
                    $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;
                    $usersSubscription->paused_since                 = '';

                    if ($usersSubscriptionTable->save($usersSubscription)) {

                        $user_subscription_active_deactive_histories_table = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');

                        $query  = $user_subscription_active_deactive_histories_table->query();
                        $result = $query->update()
                            ->set(['play_date' => date('Y-m-d h:i
                                :s')])
                            ->where(['user_subscription_id' => $subscriptionId, 'play_date IS NULL'])
                            ->execute();
                        $description = " ".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";  
                        $action      = "Resumed";    
                        $this->savedInActivity($redirectId, $description,$action,$type);
                        $this->Flash->success(__('The subscription has been Started.'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($redirectId)]]);

                    }

                } else if ($whatforChange == 'delete') {

                } else {

                    die('Insufficient Paramter Supplied');

                }

            } else {
                die('Insufficient Paramter Supplied');
            }

        } else {
            die('Hey Look');
        }

    }

    public function addSubscription()
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $type = 1;
            $error = $this->validateAddSubscription($this->request->getData());
            if ($error['statuscode'] == 200) {
                $userSubscriptionTable                  = TableRegistry::get('UserSubscriptions');
                $userSubscription                       = $userSubscriptionTable->newEntity();
                $userSubscription->subscription_type_id = $data['subscription_type_id'];

                if (isset($data['days']) && !empty($data['days'])) {
                    $userSubscription->days      = implode("-", $data['days']);
                    $userSubscription->startdate = $data['start_date'];
                } else {
                    $userSubscription->startdate = $data['start_date'];
                    $userSubscription->days      = '';
                }
                $userSubscription->product_id           = $data['product_id'];
                $userSubscription->delivery_schdule_ids = implode("-", $data['delivery_schdule_ids']);

                if(isset($data['quantity']) && !empty($data['quantity'])){

                 $userSubscription->quantity = $data['quantity'];
                 $userSubscription->subscriptions_total_amount = $data['subscription_price'];
                 $quantityy                                    = $data['quantity'];
                }else{
                  $userSubscription->quantity = $data['quantity_chield'];
                  $productChildrenTable = TableRegistry::get('ProductChildren');
                  $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id'])->where(['product_id' => $data['product_id'],'quantity'=> $data['quantity_chield']])->first();
                  $userSubscription->quantity                     = $data['quantity1'];
                  $userSubscription->subscriptions_total_amount   = $productChildren['price'] * $data['quantity1'];
                  $quantityy                                      = $data['quantity1'];
                  
                }

                $userSubscription->users_subscription_status_id = 1;
                $userSubscription->user_id                      = $data['user_id'];
                $userSubscription->unit_name                    = $data['unit_name'];
                $userSubscription->summary                      = $data['summary'];
                $userSubscription->notes                        = $data['notes'];
                $unitname                                       = $this->getUnitNameAndId($data['product_id']);
                $product_name                                   = $unitname['product_name'];
                $userSubscription->enddate = '2017-12-12';
                if ($user_sub = $userSubscriptionTable->save($userSubscription)) {
                    $description    = "".$product_name .' ' .$quantityy .' '. $unitname['name'] .' ([' .$user_sub->id."])"; 
                    $action         = "Subscribed"; 
                    $this->savedInActivity($data['user_id'], $description,$action,$type);
                    $saverouteCustomer = $this->addIntoRoute($data['user_id'], $data['delivery_schdule_ids']);
                    if ($saverouteCustomer) {
                        $productTable = TableRegistry::get('Products');
                        $productqty   = $productTable->find()->select(['quantity'])->where(['id' => $data['product_id']])->first();
                        $leftQty      = ($productqty['quantity'] - $userSubscription->quantity);
                        $query        = $productTable->query();
                        $result       = $query->update()
                            ->set(['quantity' => $leftQty])
                            ->where(['id' => $data['product_id']])
                            ->execute();
                        if (isset($data['subscription_containers']) && $data['subscription_containers'] > 0) {
                            $userContainerTable        = TableRegistry::get('UserContainers');
                            $subscribedContainersTable = TableRegistry::get('SubscribedContainers');
                            $userContainer             = $userContainerTable->find()->select(['container_given', 'id'])->where(['user_id' => $data['user_id']])->toArray();

                            if (isset($userContainer[0]['container_given'])) {

                                $container_given_db = $userContainer[0]['container_given'];
                                $container_given_db = $container_given_db + $data['subscription_containers'];

                                $query = $userContainerTable->query();
                                $query->update()
                                    ->set(['container_given' => $container_given_db])
                                    ->where(['id' => $userContainer[0]['id']])
                                    ->execute();

                                $query = $subscribedContainersTable->query();
                                $query->update()
                                    ->set(['container_count' => $container_given_db])
                                    ->where(['user_id' => $data['user_id']])
                                    ->execute();

                            } else {

                                $userContainer                       = $userContainerTable->newEntity();
                                $userContainer->container_given      = $data['subscription_containers'];
                                $userContainer->user_id              = $data['user_id'];
                                $userContainer->container_collect    = 0;
                                $userContainer->left_container_count = 0;
                                $userContainerTable->save($userContainer);

                                $subscribedContainer                  = $subscribedContainersTable->newEntity();
                                $subscribedContainer->container_count = $data['subscription_containers'];
                                $subscribedContainer->user_id         = $data['user_id'];
                                $subscribedContainersTable->save($subscribedContainer);

                            }

                        }

                        $error['statuscode'] = 200;
                        $error['name']       = "Subscription has been saved successfully";
                        echo json_encode($error);die;
                    } else {
                        $error['statuscode'] = 201;
                        $error['name']       = "Something went erong please try again";
                        echo json_encode($error);die;
                    }

                } else {
                    $error['statuscode'] = 201;
                    $error['name']       = "Something Went wrong.";
                    echo json_encode($error);die;
                }
            } else {
                echo json_encode($error);die;
            }
        }
    }

    public function customerSubscription()
    {

        if ($this->request->is('Ajax')) {

            $this->viewBuilder()->layout('ajax');
            $this->set('id', $_POST['id']);
            $id = base64_decode($_POST['id']);
            $this->set('user_customer_id', $id);
            $UsersTable = TableRegistry::get('Users');
            $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $id])->first()->toArray();
            $this->set('data', $users);

            $usersBalanceTable = TableRegistry::get('UserBalances');
            $UsersTable        = TableRegistry::get('Users');
            $usersBalance      = $usersBalanceTable->find('all')->where(['user_id' => $id])->select(['balance'])->first();
            $this->set('usersBalance', $usersBalance);

            $DeliverySchdulesTable = TableRegistry::get('DeliverySchdules');
            $subtype               = TableRegistry::get('SubscriptionTypes');
            $DeliverySchdules      = $DeliverySchdulesTable->find('list');
            $subscriptionTypes     = $subtype->find('all')->select(['id', 'subscription_type_name'])->order(['id'])->toArray();

            $usersTable     = TableRegistry::get('Users');
            $users          = $usersTable->find('list')->where(['id' => $id])->toArray();
            $categoryTable  = TableRegistry::get('Categories');
            $categories     = $categoryTable->find('list')->where(['Categories.status' => 1])->toArray();
            $containerTable = TableRegistry::get('Containers');
            $userContainer  = $containerTable->find()->count();
            if ($userContainer == 0) {
                $this->Flash->error(__('PLease add container first then add subscription.'));
                return $this->redirect('/Containers/add');

            }
            $units = $this->UserSubscriptions->Units->find('list')->where(['Units.name' => 'ltr']);
            $this->set(compact('categories', 'subscriptionTypes', 'users', 'DeliverySchdules'));
            
            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $userSubscription1      = $userSubscriptionTable->find('all')->contain(['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses'])->where(['UserSubscriptions.user_id' => $id, 'users_subscription_status_id !=' => 4])->toArray();/*
            pr($userSubscription); die;*/
             
             $userSubscription2= array();
            foreach ($userSubscription1 as $ke => $ve) { 
                $userSubscription = array();               
                if($ve['pro_child_id'] != 0 && $ve['pro_child_id'] != ""){         
                 
                    $unit   = $this->getUnitNameAndIdchild($ve['pro_child_id']);
                    $userSubscription2[]  = $ve;
                    $userSubscription2[$ke]['unit_name']= $unit['name'];
                    $userSubscription2[$ke]['quantity'] = $ve['quantity']*$ve['child_package_qty'];
                    $userSubscription2[$ke]['subscriptions_total_amount']  = $ve['quantity_child']*$unit['price'];

                }else{
                    $unit   = $this->getUnitNameAndId($ve['product_id']);
                    $userSubscription2[]  = $ve;
                    $userSubscription2[$ke]['unit_name'] = $ve['unit_name']; 
                    $userSubscription2[$ke]['quantity']  = $ve['quantity']; 
                    $userSubscription2[$ke]['subscriptions_total_amount']   = $ve['quantity']*$unit['price']; 
                }                 
                    array_push($userSubscription, $userSubscription2);
                    
            } 
           /* pr($userSubscription); die;*/
            $userSubscription = $userSubscription[0];
            $this->set('userSubscription', $userSubscription);

            $RefundTypesTable = TableRegistry::get('RefundTypes');
            $RefundTypes      = $RefundTypesTable->find('all')->toArray();
            $this->set('refunds', $RefundTypes);
        }

    }

    public function listSubscriptionsType()
    {

        $this->set('subscription_list', $this->getSubscriptonsTypes());
        if ($this->request->is('post')) {
            $error             = array();
            $data              = $this->request->data;
            $subscrition_types = array('everyday', 'weekly', 'alternate');

            $subscription_type_name = $data['subscription_type_name'];

            $alreadySaved = $this->getSubscriptonsTypes();
            $onlyname     = array();
            if (isset($alreadySaved) && count($alreadySaved) > 0) {
                foreach ($alreadySaved as $key => $value) {

                    $onlyname[] = $value['subscription_type_name'];

                }
            }

            if (!in_array($subscription_type_name, $subscrition_types)) {

                $error['subscription_type_name'] = 'only ( everyday,weekly,alternate) values are required ';
                $this->set('error', $error);
            } else if (in_array($subscription_type_name, $onlyname)) {

                $error['subscription_type_name'] = 'This Name Already saved';
                $this->set('error', $error);
            } else {
                $subscription_types             = TableRegistry::get('subscriptionTypes');
                $values                         = $subscription_types->newEntity();
                $values->subscription_type_name = $subscription_type_name;
                $values->created                = date('Y-m-d h:i:s');
                $values->modified               = date('Y-m-d h:i:s');
                if ($subscription_types->save($values)) {
                    $success['subscription_type_name'] = 'Saved Successfully!!!';
                    $this->set('success', $success);
                    return $this->redirect(['action' => 'listSubscriptionsType']);

                }
            }
        }

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    private function getSubType($subscription_type_id)
    {

        $subscription_typesTable = TableRegistry::get('SubscriptionTypes');
        $subscription            = $subscription_typesTable->find('all')->where(['id' => $subscription_type_id])->toArray();
        return $subscription['subscription_type_name'];

    }
    private function checkSubscription($userid, $productid, $delivery_schdule_ids = null, $subscription_type_id = null)
    {

        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $userSubscription      = $userSubscriptionTable->find('all')->where([

            'user_id'                         => $userid,
            'product_id'                      => $productid,
            'users_subscription_status_id <>' => 4,
        ])->toArray();
        if (count($userSubscription) <= 0) {
            return false;
        } else {

            return true;

        }

    }
    private function validateAddSubscription($data)
    {
        $error = array();

        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['name'] = 'Please select the user name';
        } else if (!isset($data['product_id']) || empty($data['product_id'])) {
            $error['name'] = 'Please select the product';
        } else if (!isset($data['delivery_schdule_ids']) || empty($data['delivery_schdule_ids'])) {
            $error['name'] = 'Please select Delivery Timing';
        } else if (

            (!isset($data['quantity']) || empty($data['quantity']))
            &&
            (!isset($data['quantity_chield']) || empty($data['quantity_chield']))

        ) {
            $error['name'] = 'Please select quantity';
        } else if (
            (!isset($data['days']) || empty($data['days']))
            &&
            (!isset($data['start_date']) || empty($data['start_date']))
        ) {
            $error['name'] = 'Please choose start date.';
        } else if (!isset($data['subscription_type_id']) || empty($data['subscription_type_id'])) {
            $error['name'] = 'Please select the subscription type';
        } else if ($this->checkSubscription($data['user_id'], $data['product_id'], $data['delivery_schdule_ids'], $data['subscription_type_id'])) {
            $error['name'] = 'This subscription already taken by this customer';
        }

        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }

        return $error;

    }
    private function addIntoRoute($user_id, $delivery_schdule_ids)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id = $value;
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            $route_position = $routeCustomerTable->find()->select(['position' => 'MAX(RouteCustomers.position)'])->where(['route_id'=> $routeCustomer[0]['route_id']])->hydrate(false)->toArray();
            if (count($routeCustomer) > 0) {
                $route_position1 = $routeCustomerTable->find()->select(['position'])->where(['user_id'=> $user_id ,'position IS NOT NULL' ])->hydrate(false)->first();
                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                if($route_position1){
                    $routeCustomers->position            = @$route_position1['position'];
                }else{
                    $routeCustomers->position            = @$route_position[0]['position'] + 1;
                }
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {

                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();

                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id,'position'=> 1])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }

            }

        }
        return true;
    }
    private function getPriceSubscription($productId, $unitId, $qty)
    {

        $uniTable     = TableRegistry::get('Units');
        $productTable = TableRegistry::get('Products');
        $unitname     = $uniTable->find()->where(['id' => $unitId])->first();
        if ($unitname['name'] == 'ltr') {

            $product = $productTable->find()->where(['id' => $productId])->select(['price_per_unit'])->first();

            return $qty * $product['price_per_unit'];

        }if ($unitname['name'] == 'kg') {

            return $qty * $product['price_per_unit'];

        }if ($unitname['name'] == 'ml') {

        }if ($unitname['name'] == 'gram') {

        }
        return 0;
    }

    public function add()
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $error = $this->validateAddSubscription($this->request->getData());
            if ($error['statuscode'] == 200) {
                $userSubscriptionTable                  = TableRegistry::get('UserSubscriptions');
                $userSubscription                       = $userSubscriptionTable->newEntity();
                $userSubscription->subscription_type_id = $data['subscription_type_id'];

                if (isset($data['days']) && !empty($data['days'])) {
                    $userSubscription->days      = implode("-", $data['days']);
                    $userSubscription->startdate = $data['start_date'];
                } else {
                    $userSubscription->startdate = $data['start_date'];
                    $userSubscription->days      = '';
                }
                $userSubscription->product_id           = $data['product_id'];
                $userSubscription->delivery_schdule_ids = implode("-", $data['delivery_schdule_ids']);

                if (isset($data['quantity']) && !empty($data['quantity'])) {

                    $userSubscription->quantity = $data['quantity'];

                } else {
                    $userSubscription->quantity = $data['quantity_chield'];

                }

                $userSubscription->users_subscription_status_id = 1;
                $userSubscription->user_id                      = $data['user_id'];

                $unitname = $this->getUnitNameAndId($data['product_id']);
                $userSubscription->unit_id                    = $unitname['id'];
                $userSubscription->unit_name                  = $unitname['name'];
                $userSubscription->summary                    = $data['summary'];
                $userSubscription->notes                      = $data['notes'];
                $userSubscription->subscriptions_total_amount = $data['subscription_price'];
                $userSubscription->enddate = '2017-12-12';
                if ($userSubscriptionTable->save($userSubscription)) {

                    $saverouteCustomer = $this->addIntoRoute($data['user_id'], $data['delivery_schdule_ids']);
                    if ($saverouteCustomer) {
                        /*----Save container---------*/

                        if (isset($data['subscription_containers']) && $data['subscription_containers'] > 0) {
                            $userContainerTable = TableRegistry::get('UserContainers');

                            $userContainer = $userContainerTable->find()->select(['container_given', 'id'])->where(['user_id' => $data['user_id']])->toArray();

                            if (isset($userContainer[0]['container_given'])) {

                                $container_given_db = $userContainer[0]['container_given'];
                                $container_given_db = $container_given_db + $data['subscription_containers'];

                                $query = $userContainerTable->query();
                                $query->update()
                                    ->set(['container_given' => $container_given_db])
                                    ->where(['id' => $userContainer[0]['id']])
                                    ->execute();

                            } else {
                                $userContainer                       = $userContainerTable->newEntity();
                                $userContainer->container_given      = $data['subscription_containers'];
                                $userContainer->user_id              = $data['user_id'];
                                $userContainer->container_collect    = 0;
                                $userContainer->left_container_count = 0;
                                $userContainerTable->save($userContainer);
                            }

                        }

                        /*----Save container---------*/

                        $error['statuscode'] = 200;
                        $error['name']       = "Subscription has been saved successfully";
                        echo json_encode($error);die;
                    } else {
                        $error['statuscode'] = 201;
                        $error['name']       = "Something went erong please try again";
                        echo json_encode($error);die;
                    }

                } else {
                    $error['statuscode'] = 201;
                    $error['name']       = "Something Went wrong.";
                    echo json_encode($error);die;
                }
            } else {
                echo json_encode($error);die;
            }
        } else {
            $DeliverySchdulesTable = TableRegistry::get('DeliverySchdules');
            $subtype               = TableRegistry::get('SubscriptionTypes');
            $DeliverySchdules      = $DeliverySchdulesTable->find('list');
            $subscriptionTypes     = $subtype->find('all')->select(['id', 'subscription_type_name'])->toArray();
            $usersTable     = TableRegistry::get('Users');
            $users          = $usersTable->find('list')->where(['Users.type_user' => 'customer', 'Users.status' => 1])->order(['id DESC'])->toArray();
            $categoryTable  = TableRegistry::get('Categories');
            $categories     = $categoryTable->find('list')->toArray();
            $containerTable = TableRegistry::get('Containers');
            $userContainer  = $containerTable->find()->count();
            if ($userContainer == 0) {
                $this->Flash->error(__('PLease add container first then add subscription.'));
                return $this->redirect('/Containers/add');

            }

            $units = $this->UserSubscriptions->Units->find('list')->where(['Units.name' => 'ltr']);
            $this->set(compact('categories', 'subscriptionTypes', 'users', 'DeliverySchdules'));

        }

    }

    /* get schedules by category */

    public function checkCategoryDelivery($customer_id, $cat_id)
    {
        $response           = array();
        $cat_id             = $cat_id;
        $customer_id        = $customer_id;
        $getRegionAreaTable = TableRegistry::get('Users');
        $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $customer_id])->toArray();

        $region                          = $getRegionArea[0]['region_id'];
        $area                            = $getRegionArea[0]['area_id'];
        $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
        $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name'])->where(['CategoryDeliverySchdules.category_id' => $cat_id, 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

        if (count($category_delivery) > 0) {

            foreach ($category_delivery as $key => $value) {

                $temp           = array();
                $temp['d_s_id'] = $value['delivery_schdule_id'];
                $temp['name']   = $value['delivery_schdule']['name'];
                $response[]     = $temp;
            }

        }
        return $response;
    }
    /* get schedules by category */

    public function checkCategoryDeliverThisCustomer($customer_id, $cat_id)
    {

        $response = array();

        $getRegionAreaTable = TableRegistry::get('Users');
        $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $customer_id])->toArray();

        $region                          = $getRegionArea[0]['region_id'];
        $area                            = $getRegionArea[0]['area_id'];
        $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
        $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name', 'DeliverySchdules.start_time', 'DeliverySchdules.end_time'])->where(['CategoryDeliverySchdules.category_id' => $cat_id, 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

        if (count($category_delivery) > 0) {
            $tempOutSide = array();
            foreach ($category_delivery as $key => $value) {

                $temp               = array();
                $temp['d_s_id']     = $value['delivery_schdule_id'];
                $temp['name']       = $value['delivery_schdule']['name'];
                $temp['start_time'] = $value['delivery_schdule']['start_time'];
                $temp['end_time']   = $value['delivery_schdule']['end_time'];
                $tempOutSide[]      = $temp;

            }
            //check driver route for this schedule
            $finalSchedule      = array();
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            foreach ($tempOutSide as $key => $value) {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $value['d_s_id'], 'region_id' => $region, 'area_id' => $area])->hydrate(false)->toArray();
                if (!empty($routeCustomer)) {
                    $finalSchedule[] = $value;
                }
            }

            if (empty($finalSchedule)) {
                $response['messageCode'] = 1952;
                $response['successCode'] = 1;
                $response['messageText'] = 'No schedule found for user route.';
            } else {

                $response = $finalSchedule;
            }

        }

        return $response;

    }

    /**
     * Edit method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subscriptionTypeTable = TableRegistry::get('SubscriptionTypes');
        $deliverySchdulesTable = TableRegistry::get('DeliverySchdules');
        $id                    = base64_decode($id);

        $usersSubscriptionTable  = TableRegistry::get('UserSubscriptions');
        $userSub        = $usersSubscriptionTable->find()->where(['UserSubscriptions.id' => $id])->first();
        $quantity_old      = $userSub['quantity'];
        $startdate_old     = $userSub['startdate']->format('Y-m-d'); 
        $type           = 1;
        $unit_name  = '';
        $product_name = '';   
        $quantity_a   = '';             
        $prochildTable = TableRegistry::get('ProductChildren');  
        $prochildpackage = $prochildTable->find()->select(['quantity'])->where(['id' => @$userSub['pro_child_id']])->first();
        if(@$userSub['pro_child_id'] != 0 && @$userSub['pro_child_id'] != ''){
            $unitname       = $this->getUnitNameAndIdchild(@$userSub['pro_child_id']);
            $product_name   = $unitname['product_name'];
            $unit_name      = $unitname['name']; 
            $quantity_a     = $quantity_old * @$prochildpackage['quantity'];                 
        }else{
            $unitname       = $this->getUnitNameAndId($userSub['product_id']);
            $product_name   = $unitname['product_name'];
            $unit_name      = $unitname['name'];
            $quantity_a     = $quantity_old;
        }

        $userSubscription = $this->UserSubscriptions->find()->where(['UserSubscriptions.id' => $id])->contain([
            'Products.Units'])->contain(['Users', 'SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses'])->first();

        $getSchedule = $this->checkCategoryDeliverThisCustomer($userSubscription['user_id'], $userSubscription['product']['category_id']);

        $subscriptionType = $subscriptionTypeTable->find()->select(['subscription_type_name', 'id'])->where(['status' => 1])->order('id')->hydrate(false)->toArray();

        $deliverySchdule = $deliverySchdulesTable->find()->select(['name', 'id'])->where(['status' => 1])->hydrate(false)->toArray();
        $this->set(compact('userSubscription', 'subscriptionType', 'deliverySchdule', 'getSchedule'));

        if ($this->request->is('post')) {
            $requestedata = $this->request->getData();
            $user_id      = $requestedata['user_id'];

            $sub_id                     = $requestedata['sub_id'];
            $sub_qty                    = $requestedata['sub_qty'];
            $s_id                       = $requestedata['s_id'];
            $pr_price                   = $requestedata['pr_price'];
            $container                  = $requestedata['container'];
            $container_old              = $requestedata['old_container'];
            $schedule_id1[]             = $requestedata['schedule_id'];
            $startdate                  = $requestedata['start_date']; 
            $schedule_id                = implode("-", $schedule_id1);
            $subscriptions_total_amount = $pr_price * $sub_qty;
            $userSubTable                        = TableRegistry::get('UserSubscriptions');
            $usersub                             = $userSubTable->get($s_id); // Return article with id 12
            $usersub->subscription_type_id       = $sub_id;

            $usersub->quantity                   = $sub_qty;
            // $usersub->quantity_child             = $sub_qty;
            $usersub->delivery_schdule_ids       = $schedule_id;
            $usersub->subscriptions_total_amount = $subscriptions_total_amount;
            $usersub->notes                      = $requestedata['notes'];
            $usersub->startdate                  = $startdate;
            $userSubTable->save($usersub);
            
            $sub_qty1                    = $requestedata['sub_qty'];
            if(@$requestedata['pro_child_id'] != 0 && @$requestedata['pro_child_id'] != ''){
                $sub_qty1                    = $requestedata['sub_qty'] * @$prochildpackage['quantity'];
            }else{
                $sub_qty1                    = $requestedata['sub_qty'];
            }
            $this->updateintoRoute($user_id, $schedule_id1);

            $subscribedContainersTable = TableRegistry::get('SubscribedContainers');
            $userContainersTable = TableRegistry::get('UserContainers');
            $subscribedContainers      = $subscribedContainersTable->find()->where(['user_id' => $user_id]);

            $query2 = $subscribedContainersTable->query();
            $query2->update()
                ->set(['container_count' => $container])
                ->where(['user_id' => $user_id])
                ->execute();

            $userContainers      = $userContainersTable->find()->where(['user_id' => $user_id]);
            $cont_count          = $userContainers->toArray();
            $container_given     = $cont_count[0]['container_given']  - $container_old + $container;
            $query21 = $userContainersTable->query();
            $query21->update()
                ->set(['container_given' => $container_given])
                ->where(['user_id' => $user_id])
                ->execute();

            $action      = "Updated";    
            if($startdate_old != $startdate && $quantity_a != $sub_qty1 ){
                $description = "".$product_name .' from '. $quantity_a.' '. $unit_name .' to ' .$sub_qty1 .' '. $unit_name .' ([' .$id."]) and Start date from ".$startdate_old." to ".$startdate;     
                $this->savedInActivity($user_id, $description,$action,$type);
            }else if($startdate_old != $startdate){
                $description = "".$product_name .' ([' .$id."]) Start date from ".$startdate_old." to ".$startdate;
                $this->savedInActivity($user_id, $description,$action,$type);
            }else if($quantity_a != $sub_qty1){
                $description = "".$product_name .' from '. $quantity_a.' '. $unit_name .' to ' .$sub_qty1 .' '. $unit_name .' ([' .$id."])";     
                $this->savedInActivity($user_id, $description,$action,$type);
            }else{
                $description = "".$product_name .' from '. $quantity_a.' '. $unit_name .' to ' .$sub_qty1 .' '. $unit_name .' ([' .$id."])";
                //$this->savedInActivity($user_id, $description,$action,$type);
            } 
            $this->Flash->success(__('The subscription has been Started.'));
            $this->Flash->success(__('The user subscription updated.'));
            return $this->redirect($this->here);
        }
        $subscriptionTypes         = $this->UserSubscriptions->SubscriptionTypes->find('list', ['limit' => 200]);
        $products                  = $this->UserSubscriptions->Products->find('list', ['limit' => 200]);
        $usersSubscriptionStatuses = $this->UserSubscriptions->UsersSubscriptionStatuses->find('list', ['limit' => 200]);
        $users                     = $this->UserSubscriptions->Users->find('list', ['limit' => 200]);
        $units                     = $this->UserSubscriptions->Units->find('list', ['limit' => 200]);

        $this->set(compact('userSubscription', 'subscriptionTypes', 'products', 'usersSubscriptionStatuses', 'users', 'units'));
        $this->set('_serialize', ['userSubscription']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        if ($_GET['redirected_ID'] && $_GET['id']) {
            $type           = 1;
            $redirectId     = base64_decode($_GET['redirected_ID']);
            $subscriptionId = base64_decode($_GET['id']);

            $usersSubscriptionTable  = TableRegistry::get('UserSubscriptions');
            $userSub        = $usersSubscriptionTable->find()->select(['product_id','quantity'])->where(['UserSubscriptions.id' => $subscriptionId])->first();
            $quantityy      = $userSub['quantity'];
            $unitname       = $this->getUnitNameAndId($userSub['product_id']);
            $product_name   = $unitname['product_name'];
            $unit_name      = $unitname['name'];

            $userSubscription = $this->UserSubscriptions->get($subscriptionId);
            if ($this->UserSubscriptions->delete($userSubscription)) {

                $description = " ".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";
                $action      = "Deleted";                         
                $this->savedInActivity($redirectId, $description,$action,$type);
                return $this->redirect($this->referer());

                $this->Flash->success(__('The user subscription has been deleted.'));
            } else {
                $this->Flash->error(__('The user subscription could not be deleted. Please, try again.'));
            }

            return $this->redirect(['action' => 'index']);
        } else {

            $this->request->allowMethod(['post', 'delete']);
        }

    }

    private function updateintoRoute($user_id, $delivery_schdule_ids)
    {
        $usersubscription = TableRegistry::get('UserSubscriptions');
        $subscriptions    = $usersubscription->find('all')->where(['user_id' => $user_id])->select(['delivery_schdule_ids'])->hydrate(false)->toArray();
        $dsids            = array();
        foreach ($subscriptions as $key => $value) {
            $dids = explode('-', $value['delivery_schdule_ids']);
            foreach ($dids as $val) {
                if (!in_array($val, $dsids)) {
                    array_push($dsids, $val);
                }
            }
        }
        //get all route customer for a user
        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        $routeCustomer      = $routeCustomerTable->find('all')->where(['user_id' => $user_id])->hydrate(false)->toArray();

        foreach ($routeCustomer as $key => $value) {
            if (in_array($value['delivery_schdule_id'], $dsids)) {
                continue;
            } else {
                $croutes = $routeCustomerTable->find('all')->where(['route_id' => $value['route_id']])->hydrate(false)->toArray();
                if (count($croutes) == 1) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => null])
                        ->where(['id' => $value['id']])
                        ->execute();
                } else {
                    $routcus = TableRegistry::get('RouteCustomers');
                    $entity  = $routcus->get($value['id']);
                    $result  = $routcus->delete($entity);
                }
            }
        }
        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id        = $value;
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            if (count($routeCustomer) > 0) {
                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                $routeCustomers->position            = '';
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();
                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }
            }
        }
        return true;
    }

    public function CustomOrders()
    {

        if ($this->request->is('Ajax')) {

            $this->viewBuilder()->layout('ajax');
            $this->set('id', $_POST['id']);
            $id = base64_decode($_POST['id']);
            $this->set('user_customer_id', $id);
            $UsersTable = TableRegistry::get('Users');
            $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $id])->first()->toArray();
            $this->set('data', $users);

            $usersBalanceTable = TableRegistry::get('UserBalances');
            $UsersTable        = TableRegistry::get('Users');
            $usersBalance      = $usersBalanceTable->find('all')->where(['user_id' => $id])->select(['balance'])->first();
            $this->set('usersBalance', $usersBalance);

            $DeliverySchdulesTable = TableRegistry::get('DeliverySchdules');
            $DeliverySchdules      = $DeliverySchdulesTable->find('list');
            $usersTable            = TableRegistry::get('Users');
            $users                 = $usersTable->find('list')->where(['id' => $id])->toArray();
            $categoryTable         = TableRegistry::get('Categories');
            $categories            = $categoryTable->find('list')->where(['Categories.status' => 1])->toArray();
            $containerTable        = TableRegistry::get('Containers');
            $userContainer         = $containerTable->find()->count();
            if ($userContainer == 0) {
                $this->Flash->error(__('PLease add container first then add subscription.'));
                return $this->redirect('/Containers/add');

            }
            $units = $this->UserSubscriptions->Units->find('list')->where(['Units.name' => 'ltr']);
            $this->set(compact('categories', 'subscriptionTypes', 'users', 'DeliverySchdules'));

            $customOrdersTable = TableRegistry::get('CustomOrders');
            $customorder1       = $customOrdersTable->find('all')->contain(['Products'])->where(['CustomOrders.user_id' => $id, 'CustomOrders.status' => 0])->toArray();
            $customorder = array();
            foreach ($customorder1 as $key => $value) {
                $customorder[] = $value;
                if($value['pro_child_id'] != 0 && $value['pro_child_id'] != ""){
                    $unit   = $this->getUnitNameAndIdchild($value['pro_child_id']);
                    $customorder[$key]['price'] = $unit['price']*$value['quantity_child'];
                }else{
                    $unit   = $this->getUnitNameAndIdchild($value['pro_child_id']);
                    $customorder[$key]['price'] = $unit['price']*$value['quantity'];
                }
            }
            $this->set('customorder', $customorder);

            $RefundTypesTable = TableRegistry::get('RefundTypes');
            $RefundTypes      = $RefundTypesTable->find('all')->toArray();
            $this->set('refunds', $RefundTypes);
        }

    }

    public function deletecustomorder()
    {
        if ($_GET['redirected_ID'] && $_GET['id']) {
            $redirectId        = base64_decode($_GET['redirected_ID']);
            $orderId           = base64_decode($_GET['id']);
            $customOrdersTable = TableRegistry::get('CustomOrders');
            $customOrder       = $customOrdersTable->get($orderId);
            if ($customOrdersTable->delete($customOrder)) {
                $this->Flash->success(__('The custom order has been deleted.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('The custom order could not be deleted. Please, try again.'));
            }
            return $this->redirect(['action' => 'index']);
        } else {
            $this->request->allowMethod(['post', 'delete']);
        }
    }

    public function editOrder($id = null)
    {
        $id                = base64_decode($id);
        $categoryTable     = TableRegistry::get('Categories');
        $categories        = $categoryTable->find('list')->where(['Categories.status' => 1])->toArray();
        $customOrdersTable = TableRegistry::get('CustomOrders');
        $productsTable     = TableRegistry::get('Products');
        $userSubscription  = $customOrdersTable->find()->where(['CustomOrders.id' => $id])->contain([
            'Products.Units'])->contain(['Users', 'Products'])->first();
        $products = $productsTable->find('list')->where(['category_id' => $userSubscription['product']['category_id']])->select(['id', 'name'])->toArray();
        $this->set(compact('userSubscription', 'categories', 'products'));

        if ($this->request->is('post')) {
            $requestedata = $this->request->getData();
            $user_id      = $requestedata['user_id'];

            $order_id       = $requestedata['s_id'];
            $sub_qty        = $requestedata['sub_qty'];
            $pr_price       = $requestedata['pr_price1'];
            $product_id     = $requestedata['pro_name'];
            $chkcustomorder = $this->validateeditOrder($requestedata);
            if ($chkcustomorder['messageCode'] == 3306) {
                $this->Flash->success(__('This Order already made by this customer for next delivery'));
                return $this->redirect($this->referer());
            } else {
                $customorder    = $customOrdersTable->get($order_id);
                $oldqty         = $customorder->toArray();

                $customorder->quantity          = $sub_qty;
                $customorder->quantity_child    = $sub_qty;
                $customorder->price             = $pr_price;
                $customorder->product_id        = $product_id;
                $customOrdersTable->save($customorder);
                
                if(@$oldqty['pro_child_id'] != '' && @$oldqty['pro_child_id'] != 0){
                    $productChildrenTable   = TableRegistry::get('ProductChildren');
                    $in_stock   = $productChildrenTable->find()->where(['id' => $oldqty['pro_child_id']])->select(['in_stock'])->first();
                      
                    $query2      = $productChildrenTable->query();
                    $result2     = $query2->update()
                        ->set(['in_stock' => $in_stock['in_stock']-$sub_qty+$oldqty['quantity']])
                        ->where(['id' => @$oldqty['pro_child_id']])
                        ->execute();
                }else{     
                    $productTable          = TableRegistry::get('Products');  
                    $in_stock1   = $productTable->find()->where(['id' => $value['pro_id']])->select(['quantity'])->first();
                 
                    $query1      = $productTable->query();
                    $result1     = $query1->update()
                        ->set(['quantity' => $in_stock1['quantity']-$sub_qty+$oldqty['quantity']])
                        ->where(['id' => $product_id])
                        ->execute();               
                }
                $this->Flash->success(__('The custom order has been updated.'));
                return $this->redirect($this->referer());
            }
        }
    }


    public function validateeditOrder($data)
    {
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()
            ->where(['user_id' => $data['user_id'], 'product_id' => $data['product_id'], 'status' => 0, 'id <>' => $data['s_id']])->count();
        if ($customorders) {
            $res['messageCode'] = 3306;
        } else {
            $res['messageCode'] = 200;
        }
        return $res;
    }

    private function saveOrder($data)
    {
        $res               = array();
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()
            ->where(['user_id' => $data['user_id'], 'product_id' => $data['product_id'], 'status' => 0])->count();
        if ($customorders) {
            $res['messageCode'] = 3306;
        } else {
            $customorders             = $customordersTable->newEntity();
            $customorders->user_id    = $data['user_id'];
            $customorders->product_id = $data['product_id'];
            $customorders->quantity   = $data['quantity'];
            if (isset($data['is_chield'])) {
                $productChildrenTable = TableRegistry::get('ProductChildren');
                $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id'])->where(['product_id' => $data['product_id'], 'quantity' => $data['quantity']])->first();
                if ($productChildren == "") {
                    $productTable          = TableRegistry::get('Products');
                    $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                    $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                    $customorders->unit_id = $products['unit_id'];
                } else {
                    $customorders->price   = $productChildren['price'];
                    $customorders->unit_id = $productChildren['unit_id'];
                }
            } else {
                $productTable          = TableRegistry::get('Products');
                $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                $customorders->unit_id = $productChildren['unit_id'];
            }

            $customorders->status = 0;
            $customorders->created  = date('Y-m-d');
            $customorders->modified = date('Y-m-d h:i:s');
            if ($customordersTable->save($customorders)) {
                $res['messageCode'] = 200;
            } else {
                $res['messageCode'] = 201;
            }

        }
        return $res;
    }

    public function addToNextDelivery()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $saveOrder = $this->saveOrder($data);
            if ($saveOrder['messageCode'] == 200) {
                $message                 = "Order has been saved successfully";
                $push                    = $this->pushnotifications($data['user_id'], $message);
                $response['statuscode']  = 13014;
                $response['successCode'] = 1;
                $response['messageText'] = 'Product has been added successfully';
                $this->Flash->success(__('Custom order has been placed successfully'));
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($data['user_id'])]]);
            } else if ($saveOrder['messageCode'] == 3306) {
                $response['statuscode']  = 13015;
                $response['successCode'] = 0;
                $response['messageText'] = 'This Order already made by this customer for next delivery';
                $this->Flash->success(__('This Order already made by this customer for next delivery'));
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($data['user_id'])]]);
            } else {
                $response['statuscode']  = 13016;
                $response['successCode'] = 0;
                $response['messageText'] = 'Something went wrong while save order';
            }

        }
    }

}
