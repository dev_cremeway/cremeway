<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = [
        'Acl' => [
            'className' => 'Acl.Acl',
        ],
    ];

    public static $AclActionsExclude = [
        'isAuthorized',
    ];
    public function initialize()
    {
        // die;
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize'            => [
                'Acl.Actions' => ['actionPath' => 'controllers/'],
            ],
            'loginAction'          => [
                'plugin'     => false,
                'controller' => 'Users',
                'action'     => 'login',
            ],
            'loginRedirect'        => [
                'plugin'     => false,
                'controller' => 'Users',
                'action'     => 'index',
            ],
            'logoutRedirect'       => [
                'plugin'     => false,
                'controller' => 'Users',
                'action'     => 'login',
            ],
            'unauthorizedRedirect' => [
                'controller' => 'Pages',
                'action'     => 'display',
                'prefix'     => false,
            ],
            'authError'            => 'You are not authorized to access that location.',
            'flash'                => [
                'element' => 'error',
            ],
        ]);

        // Only for ACL setup
        // $this->Auth->allow();
        //
        //
        /*$this->loadComponent('Acl', [
        'className' => 'Acl.Acl',
        ]);*/

        // $this->loadComponent('Auth', [
        //     'authorize'            => [
        //         'Acl.Actions' => ['actionPath' => 'controllers/'],
        //     ],
        //     'loginAction'          => [
        //         'plugin'     => false,
        //         'controller' => 'Users',
        //         'action'     => 'login',
        //     ],
        //     'loginRedirect'        => [
        //         'plugin'     => false,
        //         'controller' => 'Users',
        //         'action'     => 'index',
        //     ],
        //     'logoutRedirect'       => [
        //         'plugin'     => false,
        //         'controller' => 'Users',
        //         'action'     => 'login',
        //     ],
        //     'unauthorizedRedirect' => [
        //         'controller' => 'Users',
        //         'action'     => 'login',
        //         'prefix'     => false,
        //     ],
        //     // 'authError' => 'You are not authorized to access that location.',
        //     'flash'                => [
        //         'element' => 'error',
        //     ],
        // ]);
        /*define( 'API_ACCESS_KEY1', 'AIzaSyDikqISQc4nza3yw2FqIBPmoiR4-2Xy314' );*/
        $currentUser = array(); 
        $usertable = TableRegistry::get('Users');
        if(!empty($this->Auth->User())){
        $currentUser = $usertable->find()->where(['Users.id'=>$this->Auth->User('id')])->hydrate(false)->first();
        }
        $this->set('currentUser',$currentUser);

    }

    /**
     * isAuthorized.
     *
     * @param array $user user logged.
     * @return void
     */
    public function isAuthorized($user)
    {
        // return true;

        // Admin can access every action
        if (isset($user['role_id']) && $user['role_id'] === 1) {
            return true;
        }

        // Default deny
        return false;
    }

    public function getCouponCode(){
        $result = array();
        $trackerTable = TableRegistry::get('CouponTrackers');
        $query = $trackerTable->find();
        $srno = $query->select(['sr_no' => $query->func()->max('sr_no')])->hydrate(false)->first();
        $srno = ++$srno['sr_no'];
        $qr_code = hash_pbkdf2("md5", $srno, 'cremeway', 1000, 10);
        $result['sr_no']=$srno;
        $result['s_c_c']=$qr_code;
        return $result;
    }
    public function getInvoiceno(){
        $result = array();
        $invoiceTable = TableRegistry::get('Invoices');
        $query = $invoiceTable->find();
        $invoiceNo = $query->select(['Invoice_no' => $query->func()->max('Invoice_no')])->hydrate(false)->first();
        if(count($invoiceNo) == 1 ){
            $srno = ++$invoiceNo['Invoice_no'];
            $result['sr_no']=$srno;
            return $result;
        }else{
            $result['sr_no']=1000001;
            return $result;
        }
    }
    /*  public function getControllers()
    {
    $files      = scandir('../src/Controller/');
    $results    = [];
    $ignoreList = [
    '.',
    '..',
    'Component',
    'AppController.php',
    ];
    foreach ($files as $file) {
    if (!in_array($file, $ignoreList)) {
    $controller = explode('.', $file)[0];
    array_push($results, str_replace('Controller', '', $controller));
    }
    }

    $onlyforadmin = array('Error', 'Pages', 'Permissions', 'Groups');

    return array_values(array_diff($results, $onlyforadmin));
    }
     */
    public function savedInActivity($user_id, $description,$action=null,$type=false)
    {
        date_default_timezone_set('Asia/Kolkata');
        $userTable  = TableRegistry::get('Users');
        $user       = $userTable->find()->select(['name'])->where(['id' => $user_id])->first();

        $usersTable         = TableRegistry::get('UserActivites');
        $users              = $usersTable->newEntity();
        $users->user_id     = $user_id;
        if($type){
            $users->user_name   = 'Admin';
        }else{
            $users->user_name   = $user['name'];
        }

        $users->date        = date('Y-m-d H:i:s');
        $users->action      = $action;
        $users->description = $description;
        $usersTable->save($users);
        return;

    }

    /* public function isAuthorized($user)
    {
    if ($user) {
    $permissionsList   = array();
    $this->Permissions = TableRegistry::get('Permissions');
    $Aros              = $this->Acl->Aro->find('threaded')->where(['foreign_key' => $user['group_id']])->hydrate('false')->toArray();
    $Acos              = $this->Acl->Aco->find('threaded')->where(['id' => $Aros[0]['id']])->hydrate('false')->toArray();

    if ($Acos[0]['alias'] == 'controllers') {

    $permissionsList['admin'] = 1;

    } else {

    $permission     = TableRegistry::get('Permissions');
    $permissionlist = $permission->find('all')->where(['aro_id' => $Aros[0]['id'], '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1])->hydrate('false')->toArray();

    foreach ($permissionlist as $key => $value) {

    $Acos                            = $this->Acl->Aco->find('threaded')->where(['id' => $value['aco_id']])->hydrate('false')->toArray();
    $permissionsList['controller'][] = $Acos[0]['alias'];

    }
    }

    if (count($permissionsList) == 0) {
    $permissionsList['controller'] = [];
    }

    return $permissionsList;
    }

    }
     */
    /* public function makeAuthorizedFromAdmin($user)
    {

    if ($user) {
    $permissionsList   = array();
    $this->Permissions = TableRegistry::get('Permissions');
    $Aros              = $this->Acl->Aro->find('threaded')->where(['foreign_key' => $user['group_id']])->hydrate('false')->toArray();
    $Acos              = $this->Acl->Aco->find('threaded')->where(['id' => $Aros[0]['id']])->hydrate('false')->toArray();

    if ($Acos[0]['alias'] == 'controllers') {

    $permissionsList['admin'] = 1;

    } else {

    $permission     = TableRegistry::get('Permissions');
    $permissionlist = $permission->find('all')->where(['aro_id' => $Aros[0]['id'], '_create' => 1, '_read' => 1, '_update' => 1, '_delete' => 1])->hydrate('false')->toArray();

    foreach ($permissionlist as $key => $value) {

    $Acos                            = $this->Acl->Aco->find('threaded')->where(['id' => $value['aco_id']])->hydrate('false')->toArray();
    $permissionsList['controller'][] = $Acos[0]['alias'];

    }
    }

    if (count($permissionsList) == 0) {
    $permissionsList['controller'] = [];
    }

    return $permissionsList;
    }

    }*/

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow('display');
        // parent::beforeFilter($event);
        //$this->Auth->allow('logout');
        /* $usersTable = TableRegistry::get('Users');
        $usersCNT   = $usersTable->find()->where(['status' => 0, 'type_user' => 'customer'])->count();
        $this->set('usersCNT', $usersCNT);*/
        /*$permissionsList = array();
    $indexPermission = array();
    $user            = $this->request->session()->read('Auth.User');

    if ($user) {

    $permissionsList = $this->isAuthorized($user);

    $requestIngController = $this->request->params['controller'];
    $action               = $this->request->params['action'];
    if ($action != "logout") {
    if (isset($permissionsList['admin']) && $permissionsList['admin'] == 1) {
    $this->set('permissionsList', $permissionsList);
    } elseif (in_array($requestIngController, $permissionsList['controller'])) {

    foreach ($permissionsList['controller'] as $k => $va) {

    array_push($indexPermission, $va);
    }

    $this->set('permissionsList', $indexPermission);

    } else {

    $this->redirect(array('controller' => $permissionsList['controller'][0], 'action' => 'add'));
    }
    }
    } else {

    if (!$this->Auth->user()) {

    }

    }*/
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function pushnotifications($userid, $message)
    {

        $response               = array();
        $UserNotificationsTable = TableRegistry::get('UserNotifications');
        $Notification           = $UserNotificationsTable->newEntity();
        $Notification->user_id  = $userid;
        $Notification->message  = $message;
        $Notification->date     = date('Y-m-d');
        $result                 = $UserNotificationsTable->save($Notification);
        if ($result) {
            $response['messageText'] = "success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
        }

    }

    public function checkItemAnotherTime($userid, $schduleID)
    {

        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find('all')->where(['user_id' => $userid, 'delivery_schdule_ids' => $schduleID])->toArray();
        $deliversSchdulesIds    = array();
        foreach ($userSubscriptions as $key => $value) {

            $d_sids = explode(',', $value['delivery_schdule_ids']);
            foreach ($d_sids as $k => $v) {
                array_push($deliversSchdulesIds, $v);
            }
        }

        $deliversSchdulesIds = array_unique($deliversSchdulesIds);

        $currentDateTime    = date('Y-m-d h:i:s');
        $customerLoggedTime = date('h:i A', strtotime($currentDateTime));
        $dateObject         = new \DateTime;
        $nowTime            = $dateObject::createFromFormat('H:i A', $customerLoggedTime);
        $dayName            = '';
        $iterationCount     = 1;

        $deliverySchduleTable = TableRegistry::get('DeliverySchdules');

        $previous_diffrence   = 0;
        $next_diffrence       = 0;
        $finalDeliverySchdule = 0;
        foreach ($deliversSchdulesIds as $ke => $val) {

            $d_s        = $deliverySchduleTable->find()->where(['id' => $val])->first();
            $start_time = $d_s['start_time'];
            $end_time   = $d_s['end_time'];
            $d_s_t      = $dateObject::createFromFormat('H:i A', $start_time);
            $d_e_t      = $dateObject::createFromFormat('H:i A', $end_time);
            if ($nowTime < $d_s_t) {
                $dayName             = 'Today';
                $next_diffrence_temp = $d_s_t->diff($nowTime);

                $day            = $next_diffrence_temp->format('%d');
                $hour           = $next_diffrence_temp->format('%h');
                $minute         = $next_diffrence_temp->format('%i');
                $next_diffrence = ($day * 24 * 60) + ($hour * 60) + $minute;
                if ($iterationCount == 1) {
                    $previous_diffrence   = $next_diffrence;
                    $finalDeliverySchdule = $val;

                } else {

                    if ($next_diffrence < $previous_diffrence) {
                        $previous_diffrence   = $next_diffrence;
                        $finalDeliverySchdule = $val;
                    }
                }

            } else {

                $dayName             = 'Tomorrow';
                $next_diffrence_temp = $d_s_t->diff($nowTime);

                $day            = $next_diffrence_temp->format('%d');
                $hour           = $next_diffrence_temp->format('%h');
                $minute         = $next_diffrence_temp->format('%i');
                $next_diffrence = ($day * 24 * 60) + ($hour * 60) + $minute;

                if ($iterationCount == 1) {
                    $previous_diffrence   = $next_diffrence;
                    $finalDeliverySchdule = $val;
                } else {

                    if ($next_diffrence > $previous_diffrence) {
                        $previous_diffrence   = $next_diffrence;
                        $finalDeliverySchdule = $val;
                    }
                }
            }

            $iterationCount++;

        }

        //$isItemFoundForThisTime = $this->checkItemThisTime($userid,$finalDeliverySchdule);
        $isItemFoundForThisTime = $this->checkItemThisTime($userid, $schduleID);
        $response               = array();
        $response['itemss']     = $isItemFoundForThisTime;
        if (isset($value['id']) && !empty($value['id'])) {
            $response['d_s_i'] = $value['delivery_schdule_ids'];
        }

        $response['deliverydate1'] = $dayName;

        //echo $previous_diffrence;die;
        return $response;

    }

    /*--status code reserve for getCustomerBalance function Start from 2000 to 2030---*/

    public function updatedCustomersBalance($user_id)
    {
        $userBalanceTable = TableRegistry::get('UserBalances');
        $userBalance      = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $user_id])->toArray();
        if (count($userBalance) > 0) {

            return $userBalance[0]['balance'];

        } else {

            return 0;
        }

    }

    public function getPrice($orderInfo)
    {
        $totalPrice  = 0;
        $unitname    = 0;
        $producTable = TableRegistry::get('Products');
        if (isset($orderInfo['itemss'][0]['subscriptionInfo'])) {
            foreach ($orderInfo['itemss'][0]['subscriptionInfo'] as $key => $value) {
                if($value['pro_child_id'] != '' && $value['pro_child_id'] != 0 ){
                    $unitname = $this->getUnitNameAndIdchild($value['pro_child_id']);
                    $totalPrice = $totalPrice + $unitname['price']*$value['quantity_child'];
                }else{
                    $unitname = $this->getUnitNameAndId($value['pro_id']);
                    $totalPrice = $totalPrice + $unitname['price']*$value['quantity'];
                }
            }
        }
        if (isset($orderInfo['itemss']['customOrderInfo'])) {
            foreach ($orderInfo['itemss']['customOrderInfo'] as $key => $value) {
                if($value['pro_child_id'] != '' && $value['pro_child_id'] != 0 ){
                    $unitname   = $this->getUnitNameAndIdchild($value['pro_child_id']);
                    $totalPrice = $totalPrice + $unitname['price']*$value['quantity'];
                }else{
                    $unitname   = $this->getUnitNameAndId($value['pro_id']);
                    $totalPrice = $totalPrice + $unitname['price']*$value['quantity'];
                }
            }
        }
        return $totalPrice;
    }

    public function checkNextDelivery($data, $futureDate = null)
    {

        date_default_timezone_set('Asia/Kolkata');
        $response1             = array();
        $DeliverySchdules      = TableRegistry::get('DeliverySchdules');
        $DeliverySchdulesids   = $DeliverySchdules->find('list', ['keyField' => 'id', 'valueField' => 'id'])->hydrate(false)->toArray();
        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $usersub               = $userSubscriptionTable->find('all')->where(['user_id' => $data['user_id'], 'users_subscription_status_id' => 1])->hydrate(false)->toArray();
        $ds_id                 = array();
        foreach ($usersub as $key => $value) {
            $dids = explode(',', $value['delivery_schdule_ids']);
            foreach ($dids as $val) {
                if (!in_array($val, $ds_id) && in_array($val, $DeliverySchdulesids)) {
                    array_push($ds_id, $val);
                }
            }
        }

        $isItemFoundForThisTime = array();
        if ($this->_futureDate) {
            $currentDateTime = $this->_futureDate;
        } else {
            $currentDateTime = date('Y-m-d h:i:s A');
        }

        $newDateTime = date('h:i A', strtotime($currentDateTime));

        $response   = array();
        $schdulew   = array();
        $schduleq   = array();
        $final_dsid = array();
        foreach ($ds_id as $key11 => $value11) {
            $driverRouteTable = TableRegistry::get('DeliverySchdules');
            $schdulew         = $driverRouteTable->find('all')->where(['id' => $value11])->hydrate(false)->toArray();
            if (!empty($schdulew)) {
                array_push($schduleq, $schdulew);
            }
        }

        //Return subscription for current schedule of today
        if (count($schduleq) > 0) {

            foreach ($schduleq as $key => $value) {
                $current_time = $newDateTime;
                $startTime    = $value[0]['start_time'];
                $endTime      = $value[0]['end_time'];
                $dateObject   = new \DateTime;
                $date1        = $dateObject::createFromFormat('H:i a', $current_time);
                $date2        = $dateObject::createFromFormat('H:i a', $startTime);
                $date3        = $dateObject::createFromFormat('H:i a', $endTime);

                if ($date1 > $date2 && $date1 < $date3) {
                    $isItemFoundForThisTimee = $this->checkItemThisTime($data['user_id'], $value[0]['id'], 'todaycurrent');

                    if (!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])) {
                        $response['itemss']        = $isItemFoundForThisTimee;
                        $response['d_s_i']         = $value[0]['id'];
                        $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                        return $response;
                    }
                }
            }

            //sort schedules in ascending order
            $sorttime = array();
            $temp     = array();
            foreach ($schduleq as $key => $value) {
                $sorttime[$value[0]['id']] = $dateObject::createFromFormat('H:i a', $value[0]['start_time']);
                $temp[$value[0]['id']][]   = $value[0];
            }
            // array_multisort($startdate, SORT_ASC, $products);
            asort($sorttime);
            $schduleq = array();
            foreach ($sorttime as $key => $value) {
                $schduleq[] = $temp[$key];
            }
            $rep = array();
            //Return subscription for next schedule of today
            foreach ($schduleq as $key => $value) {

                $current_time = $newDateTime;
                // pr($current_time);die;
                $startTime  = $value[0]['start_time'];
                $endTime    = $value[0]['end_time'];
                $dateObject = new \DateTime;
                $date1      = $dateObject::createFromFormat('H:i a', $current_time);
                $date2      = $dateObject::createFromFormat('H:i a', $startTime);
                $date3      = $dateObject::createFromFormat('H:i a', $endTime);
                if ($date1 < $date2) {
                    $isItemFoundForThisTimee = $this->checkItemThisTime($data['user_id'], $value[0]['id']);
                    //pr($isItemFoundForThisTimee);die('h');
                    if (!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])) {
                        $response['itemss']        = $isItemFoundForThisTimee;
                        $response['d_s_i']         = $value[0]['id'];
                        $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                        if ($this->_futureDate) {
                            $rep[$value[0]['id']] = $response;

                        } else {
                            return $response;
                        }
                    }
                }
            }
            if ($this->_futureDate) {
                return $rep;
                // pr($rep);die;
            }
            //Return subscription for first schedule of tomorrow

            foreach ($schduleq as $key => $value) {
                $isItemFoundForThisTimee = $this->checkItemThisTime($data['user_id'], $value[0]['id'], 'tomorrow');

                if (!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])) {
                    $response['itemss']        = $isItemFoundForThisTimee;
                    $response['d_s_i']         = $value[0]['id'];
                    $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                    return $response;
                }
            }

            //Return subscription for other day after tomorrow
            foreach ($schduleq as $key => $value) {
                $tomorrow                = date("Y-m-d", strtotime("+1 day"));
                $isItemFoundForThisTimee = $this->checkItemThisTime($data['user_id'], $value[0]['id'], 'other');
                if (!empty($isItemFoundForThisTimee)) {
                    $response['itemss']        = $isItemFoundForThisTimee;
                    $response['d_s_i']         = $value[0]['id'];
                    $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                    return $response;
                }
            }
        }
        return false;
    }

    private function checkItemThisTime($user_id, $schdule_id, $day = null)
    {
        date_default_timezone_set('Asia/Kolkata');
        $userTable              = TableRegistry::get('Users');
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userId                 = $user_id;
        if ($day == 'tomorrow') {
            $tomorrow        = date("Y-m-d", strtotime("+1 day"));
            $day_to_del      = "tomorrow";
            $allUserSubOrder = $userTable->find('all')->contain([
                'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id, $tomorrow) {
                    return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $tomorrow, 'FIND_IN_SET (' . $schdule_id . ',UserSubscriptions.delivery_schdule_ids)']);
                },
            ])->where(['Users.id' => $userId])->hydrate(false)->toArray();

        } else if ($day == 'other') {
            $day_to_del = "otherafter";
            $tomorrow   = date("Y-m-d", strtotime("+1 day"));

            //check alternate day after tomorrow
            $aftertomorrow = date("Y-m-d", strtotime("$tomorrow+1 day"));

            $allUserSubOrder = $userTable->find('all')->contain([
                'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id, $aftertomorrow) {
                    return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $aftertomorrow, 'FIND_IN_SET (' . $schdule_id . ',UserSubscriptions.delivery_schdule_ids)']);
                },
            ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            //pr($aftertomorrow);die;
            if (isset($allUserSubOrder[0]['user_subscriptions']) && empty($allUserSubOrder[0]['user_subscriptions'])) {
                $day_to_del = "other";
                //check other day after tomorrow
                $usersub = $userSubscriptionsTable->find('all')->where(['user_id' => $userId, 'users_subscription_status_id' => 1, 'startdate >' => $tomorrow])->order(['startdate'])->hydrate(false)->toArray();
                if (!empty($usersub)) {
                    $otherdate       = $usersub[0]['startdate']->i18nFormat('yyyy-MM-dd');
                    $allUserSubOrder = $userTable->find('all')->contain([
                        'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id, $otherdate) {
                            return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $otherdate, 'FIND_IN_SET (' . $schdule_id . ',UserSubscriptions.delivery_schdule_ids)']);
                        },
                    ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
                } else {
                    return false;
                }
            }
        } else {
            if ($this->_futureDate) {
                $day_to_del      = "future";
                $futuredate      = $this->_futureDate;
                $allUserSubOrder = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id, $futuredate) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $futuredate, 'FIND_IN_SET (' . $schdule_id . ',UserSubscriptions.delivery_schdule_ids)']);
                    },
                ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            } else {
                $day_to_del      = "today";
                $allUserSubOrder = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => date('Y-m-d'), 'FIND_IN_SET (' . $schdule_id . ',UserSubscriptions.delivery_schdule_ids)']);
                    },
                ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            }
        }

        // get today deliver schedules ids
        if (isset($allUserSubOrder[0]['user_subscriptions']) && !empty($allUserSubOrder[0]['user_subscriptions'])) {
            // pr($allUserSubOrder);die;
            if ($day == 'todaycurrent') {
                $transtable = TableRegistry::get('Transactions');
                $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type' => 'Dr', 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $schdule_id])->orWhere(['Transactions.transaction_amount_type' => 'rejected', 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $schdule_id])->hydrate(false)->toArray();
                $subids     = array();
                if (!empty($trans)) {
                    foreach ($trans as $key => $value) {
                        $sids = explode(',', $value['user_subscription_ids']);
                        foreach ($sids as $val) {
                            if (!in_array($val, $subids)) {
                                array_push($subids, $val);
                            }
                        }
                    }
                }

                if (!empty($subids)) {
                    $usersubscriptions = array();
                    foreach ($allUserSubOrder[0]['user_subscriptions'] as $key => $value) {
                        if (!in_array($value['id'], $subids)) {
                            $usersubscriptions[] = $value;
                        }
                    }
                    $allUserSubOrder[0]['user_subscriptions'] = $usersubscriptions;
                }
            }

            $itemsInfno = $this->factorySubscription($allUserSubOrder, $schdule_id, $day_to_del);
            if (count($itemsInfno) > 0) {
                $itemsInfno['day'] = $day_to_del;
                return $itemsInfno;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getUnitNameAndId($pro_id)
    { 
        $productTable = TableRegistry::get('Products');
        $unitId       = $productTable->find()->where(['id' => trim($pro_id)])->select(['unit_id','name','price_per_unit'])->toArray();

        $uniTable   = TableRegistry::get('Units');
        $unitsName  = $uniTable->find()->select(['name', 'id'])->where(['id' => $unitId[0]['unit_id']])->toArray();

        $unit                   = array();
        $unit['name']           = $unitsName[0]['name'];
        $unit['id']             = $unitsName[0]['id'];
        $unit['product_name']   = $unitId[0]['name'];        
        $unit['price']          = $unitId[0]['price_per_unit'];        
        return $unit;

    }
    public function getUnitNameAndIdchild($pro_id=null)
    {
        $productTable = TableRegistry::get('ProductChildren');
        $unitId       = $productTable->find()->contain(['Products'])->where(['ProductChildren.id' => trim($pro_id)])->select(['ProductChildren.unit_id','Products.name','ProductChildren.price','ProductChildren.quantity'])->toArray();
        $uniTable = TableRegistry::get('Units');
        $unitsName = $uniTable->find()->select(['name', 'id'])->where(['id' => @$unitId[0]['unit_id']])->toArray();

        $unit                   = array();
        $unit['name']           = @$unitsName[0]['name'];
        $unit['id']             = @$unitsName[0]['id'];
        $unit['product_name']   = @$unitId[0]['product']['name'];        
        $unit['package_qty']    = @$unitId[0]['quantity'];         
        $unit['price']          = @$unitId[0]['price'];         
        return $unit;
    }

    public function factorySubscription($data, $schduleID, $day = null)
    {
        $childProductTable = TableRegistry::get('ProductChildren');
        if ($day == 'tomorrow') {
            $cdate = date("Y-m-d", strtotime("+1 day"));
        } else if ($day == 'other' || $day == 'otherafter') {
            if ($day == 'other') {
                $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                $tomorrow               = date("Y-m-d", strtotime("+1 day"));
                $usersub                = $userSubscriptionsTable->find('all')->where(['user_id' => $data[0]['id'], 'users_subscription_status_id' => 1, 'startdate >' => $tomorrow])->order(['startdate'])->hydrate(false)->toArray();
                $cdate                  = $usersub[0]['startdate']->i18nFormat('yyyy-MM-dd');
            } else {
                $cdate = date("Y-m-d", strtotime("+2 day"));
            }
        } else if ($this->_futureDate) {
            $cdate = $this->_futureDate;
        } else {
            $cdate = date("Y-m-d");
        }
        $final = array();
        foreach ($data as $key => $value) {
            // pr($value);die;
            $final1 = array();
            if ((isset($value['custom_orders']) && !empty($value['custom_orders'])) || (isset($value['user_subscriptions']) && !empty($value['user_subscriptions']))) {
                if (isset($value['user_subscriptions']) && !empty($value['user_subscriptions'])) {
                    foreach ($value['user_subscriptions'] as $k => $v) {
                        $insideorders = array();
                        $date         = $this->checkTodaySubscriptions($v['id'], $v['subscription_type_id'], $v['days'], $v['delivery_schdule_ids'],
                            $cdate);
                        if (isset($date['comingdate']) && !empty($date['comingdate'])) {
                            $modify_data                          = $this->getModifydata($v['id']);
                            $deliveryTimeTable                    = TableRegistry::get('DeliverySchdules');
                            $deliveryTime                         = $deliveryTimeTable->find()->select(['name', 'start_time', 'end_time'])->where(['id' => $schduleID])->first();
                            $insideorders['subscription_id']      = $v['id'];
                            $insideorders['subscription_type_id'] = $v['subscription_type_id'];
                            $insideorders['name']                 = $v['product']['name'];
                            $insideorders['description']          = @$v['product']['description'];
                            /* $insideorders['price']                = $v['subscriptions_total_amount'];*/
                            $insideorders['price']          = $modify_data['totalprice'];
                            $insideorders['price_per_unit'] = $v['product']['price_per_unit'];
                            $insideorders['pro_id']         = $v['product']['id'];
                            /*$insideorders['quantity']             = $v['quantity'];*/
                            $insideorders['quantity']    = $modify_data['quantity'];
                            $insideorders['quantity_child']    = @$v['quantity_child'];
                            $insideorders['pro_child_id']    = @$v['pro_child_id'];
                            $insideorders['child_package_qty']    = @$v['child_package_qty'];
                            $insideorders['pricr_per_package']    = @$v['pricr_per_package'];
                            $insideorders['modify_data'] = $modify_data['modifieddata'];

                            if(@$v['quantity_child'] != 0 && $v['pro_child_id']){
                                $unit_name  = $this->getUnitNameAndIdchild($v['pro_child_id']);
                                $insideorders['unit']            = $unit_name['name'] ? $unit_name['name'] : 0;
                            }else{
                                $insideorders['unit']            = $v['product']['unit']['name'];                                
                            }
                            $insideorders['image']           = @$v['product']['image'];
                            $insideorders['deliverydate']    = $date['comingdate'];
                            $insideorders['startdate']       = $v['startdate'];
                            $insideorders['timeToBeDeliver'] = $deliveryTime['name'];
                            $insideorders['between']         = $deliveryTime['start_time'] . '-' . $deliveryTime['end_time'];
                            $insideorders['24hours_formate'] = date("H:i", strtotime($deliveryTime['start_time'])) . '-' . date("H:i", strtotime($deliveryTime['end_time']));
                            $insideorders['type']            = 'subscription';
                        }
                        if (count($insideorders) > 0) {
                            $final1[] = $insideorders;
                        }
                    }
                }

                if (count($final1) > 0) {
                    $highestInfo                     = array();
                    $highestInfo['subscriptionInfo'] = $final1;
                    $final[]                         = $highestInfo;
                }
            } else {
                continue;
            }
        }
        $my_new_date       = $cdate;
        $customordersTable = TableRegistry::get('CustomOrders');
        $orderNext         = $customordersTable->find('all')
            ->where(['CustomOrders.user_id' => $data[0]['id'],
                //'CustomOrders.delivery_schdule_id' => $schduleID,
                'CustomOrders.status'           => 0,
                //'CustomOrders.created' => $my_new_date,
            ])->contain(['Products', 'Units'])->hydrate(false)->toArray();
        $orderInfo = array();
        if (count($orderNext) > 0) {
            foreach ($orderNext as $key => $value) {
                $orderItem                   = array();
                $orderItem['id']             = $value['id'];
                $orderItem['name']           = $value['product']['name'];
                $insideorders['description'] = @$v['product']['description'];
                $orderItem['image']          = @$value['product']['image'];
                $orderItem['pro_id']         = $value['product']['id'];
                $orderItem['quantity']       = $value['quantity'];
                $orderItem['quantity_child']    = @$value['quantity_child'];
                $orderItem['pro_child_id']    = @$value['pro_child_id'];
                $orderItem['child_package_qty']    = @$value['child_package_qty'];
                if($orderItem['pro_child_id'] != '' && $orderItem['pro_child_id'] != 0 ){
                    $unitname                    = $this->getUnitNameAndIdchild($orderItem['pro_child_id']);

                    $orderItem['pricr_per_package']    = $unitname['price'];
                    $orderItem['price']          = $unitname['price'] * $orderItem['quantity_child'];
                    $orderItem['price_per_unit'] = $unitname['price'] ? $unitname['price'] : 0;
                }else{
                    $unitname                    = $this->getUnitNameAndId($orderItem['pro_id']);

                    $orderItem['pricr_per_package']= $unitname['price'];
                    $orderItem['price']          = $unitname['price']*$orderItem['quantity'];
                    $orderItem['price_per_unit'] = $unitname['price'] ? $unitname['price'] : 0;
                }
                $orderItem['unit']           = $value['unit']['name'];
                $orderItem['type']           = 'customorder';
                $orderInfo[]                 = $orderItem;
            }
        }
        if (empty($final) && empty($orderInfo)) {
            return $final;
        }
        $final['customOrderInfo'] = $orderInfo;
        return $final;
    }

    private function checkTodaySubscriptions($id, $type, $days, $schduleID, $cdate = null)
    {
        if ($cdate) {
            $cdate = $cdate;
            $now   = strtotime($cdate);
        } else {
            $cdate = date('Y-m-d');
            $now   = time();
        }
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id' => $id])->hydrate(false)->first();
        if (count($userSubscriptions) > 0) {
            if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {

                /*  if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                if ($userSubscriptions['startdate']->format('Y-m-d') <= date('Y-m-d')) {
                $nextdate['comingdate'] = date('Y-m-d');
                } else {
                $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                }
                return $nextdate;
                }
                $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                return $nextdate;*/
                $today['comingdate'] = $cdate;
                return $today;
            } else if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate') {
                $startdate       = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                $subscriptionday = strtotime($startdate);
                $now             = $now;
                $datediff        = $now - $subscriptionday;
                $days            = floor($datediff / (60 * 60 * 24)) + 1;

                if ($days < 1) {
                    $today['comingdate'] = $cdate;
                    return $today;
                }
                /*   echo (0%2);
                pr($days);die('hh');*/
                if ($days % 2 == 0) {
                    /*if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                    $today['comingdate'] = date('Y-m-d');
                    return $today;
                    } else {
                    $today['comingdate'] = date('Y-m-d');
                    return $today;
                    }*/
                    return false;
                } else {
                    /*if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                    $date                   = date('Y-m-d');
                    $date1                  = str_replace('-', '/', $date);
                    $tomorrow               = date('Y-m-d', strtotime($date1 . "+1 days"));
                    $nextdate['comingdate'] = $tomorrow;
                    return $nextdate;
                    } else {
                    return false;
                    }*/
                    $today['comingdate'] = $cdate;
                    return $today;
                }

            }
        }
        return false;
    }

    public function getModifydata($id)
    {
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find()->contain(['Products'])->where(['UserSubscriptions.id' => $id])->hydrate(false)->first();
        // pr($userSubscriptions);die;
        $response = array();
        if (!empty($userSubscriptions)) {
            if ($userSubscriptions['modified_quantity'] > 0 && $userSubscriptions['modified_quantity']) {
                $response['messageCode']  = 200;
                $response['quantity']     = $userSubscriptions['modified_quantity'];
                if($userSubscriptions['quantity_child'] > 0){
                    $unitname  = $this->getUnitNameAndIdchild($userSubscriptions['pro_child_id']);
                    $response['totalprice']   = $userSubscriptions['modified_quantity'] * $unitname['price'];

                }else{
                    $unitname  = $this->getUnitNameAndId($userSubscriptions['product_id']);
                    $response['totalprice']   = $userSubscriptions['modified_quantity'] * $unitname['price'];
                }
                $response['modifieddata'] = 1;
            } else {
                $response['messageCode']  = 200;
                if($userSubscriptions['quantity_child'] > 0){                    
                    $unitname  = $this->getUnitNameAndIdchild($userSubscriptions['pro_child_id']);
                    $response['quantity']     = $userSubscriptions['quantity'];
                    $response['totalprice']   = $unitname['price'] * $userSubscriptions['quantity_child'];
                }else{
                    $unitname  = $this->getUnitNameAndId($userSubscriptions['product_id']);
                    $response['quantity']     = $userSubscriptions['quantity'];
                    $response['totalprice']   = $unitname['price'] * $userSubscriptions['quantity'];
                }
                $response['modifieddata'] = 0;
            }
        } else {
            $response['messageCode'] = 201;
        }
        return $response;
    }
    /*--status code reserve for getCustomerBalance function Start from 2150 to 2200---*/
/*public function addpush($userid,$message)
{
$userTable = TableRegistry::get('Users');
$user = $userTable->find()->where(['id'=>$userid,'type_user'=>'customer'])->select(['device_id'])->toArray();

$deviceids = $user[0]['device_id'];
if($deviceids != ""){

define( 'API_ACCESS_KEY', 'AIzaSyDikqISQc4nza3yw2FqIBPmoiR4-2Xy314' );

$registrationIds = array($deviceids);
$msg = array
(
'message'   => $message,
'title'     => 'Cremeway',
'vibrate'   => 3,
'sound'     => 1,
'largeIcon' => 'large_icon',
'smallIcon' => 'small_icon'
);
$fields = array
(
'registration_ids'  => $registrationIds,
'data'          => $msg
);

$headers = array
(
'Authorization: key=' . API_ACCESS_KEY,
'Content-Type: application/json'
);

$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
return $result;

}
return true;

//shell_exec("php /var/www/html/cremeway/notification/notification.php  '".$data['msg']."' 'alert' >> /var/www/html/cremeway/notification/notification.log &");

}*/

    public function addpush($userid, $message)
    {
        // return true;
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id' => $userid])->select(['device_id'])->toArray();

        foreach ($user as $key => $value) {
            $deviceids = @$value['device_id'];
            /*if ($deviceids && $message) {
            shell_exec("php" . ' ' . NOTIFICATION_URL . '  ' . "'" . $message . "' '" . $deviceids . "' 'alert' >>" . ' ' . NOTIFICATION_log . ' ' . "&");
            }*/
            $this->addpush1($deviceids, $message);

        }

    }

    public function sendmail($userid, $message,$subject)
    {
        //return true;
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id' => $userid])->select(['email_id'])->toArray();

        if ($user[0]['email_id'] != "") {
            $to = $user[0]['email_id'];

            $email = new Email('default');
            $email->from(['cremeway@gmail.com' => 'Cremeway'])
                ->to('cremeway@gmail.com')
                ->subject($subject)
                ->send($message);
        }
        return true;
    }

   public function addpush1($deviceids, $message)
    {   
       
        $deviceIds  = json_encode($deviceids);
        shell_exec("php" . ' ' . NOTIFICATION_URL . '  ' . "'" . $message . "' '" . $deviceIds . "' 'alert' >>" . ' ' . NOTIFICATION_log . ' ' . "&");
        // return true;
        /*$str = "php" .' '. NOTIFICATION_URL .'  '.   "'" . $message . "' '" . $deviceids . "' 'alert' >>".' '.NOTIFICATION_log.' '. "&";
        pr($str);die;*/
        /*$deviceIds  = json_encode($deviceids); 
        if ($deviceids && $message) {
        shell_exec("php" . ' ' . NOTIFICATION_URL . '  ' . "'" . $message . "' '" . $deviceIds . "' 'alert' >>" . ' ' . NOTIFICATION_log . ' ' . "&");
        }*/

        /*$fields = array
            (
            'registration_ids' => array($deviceids),
            'data'             => array('message' => $message, 'title' => 'Cremeway', 'vibrate' => 1, 'sound' => 1, 'largeIcon' => 'large_icon', 'smallIcon' => 'small_icon'),
        );

        $headers = array
            (
            'Authorization: key=AIzaSyDikqISQc4nza3yw2FqIBPmoiR4-2Xy314',
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;*/
    }

    public function sendSMS($number = null, $msg1 = null, $routId = SMS_TRANSACTION_ROUTE)
    {
        //return true;
        if ($number && $msg1) {
            $api_key  = SMS_KEY;
            $contacts = $number;
            $from     = SMS_SENDER;
            $sms_text = urlencode($msg1);
            //$routeid  = SMS_ROUTE;
            $routeid = $routId;
            //Submit to server
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://panel1.smskaro.co.in/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=" . $api_key . "&routeid=" . $routeid . "&type=text&contacts=" . $contacts . "&senderid=" . $from . "&msg=" . $sms_text);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }
        return false;
    }

}
