<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Coupons Controller
 *
 * @property \App\Model\Table\CouponsTable $Coupons
 *
 * @method \App\Model\Entity\Coupon[] paginate($object = null, array $settings = [])
 */
class CouponsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    
    public function dCsv($id){
        
       $milliseconds = round(microtime(true) * 1000);
        
        $this->response->download($milliseconds.'.csv');        
        $_header    = ['COUPON_ID', 'COUPONE_CODE', 'COUPONE_PRICE (Rs)','USE_STATUS'];
        $data       = $this->Coupons->find('all')->contain(['CouponTrackers'])->where(['Coupons.id'=>base64_decode($id)])->toArray();        
        $finaldata  = array();
 
        foreach ($data[0]['coupon_trackers'] as $key => $value) {             
              $temp                 = array();
              $temp['coupon_id']    =$value['id'];
              $temp['s_c_c']        =$value['s_c_c'];
              $temp['price_value']  =$data[0]['price_value'];
              $temp['use_status']   = $value['use_status'] == 1 ? 'Used' : 'Unused';
              $finaldata[]          = $temp; 
         }         

        $_serialize = 'finaldata';
        $this->set(compact('finaldata', '_serialize','_header'));
        $this->viewBuilder()->className('CsvView.Csv');
        return;
    }
    public function index()
    {
        $this->paginate = [
                            'limit' => 10,
                            'order' => [
                                'Coupons.id' => 'desc'
                            ] 
                        ];
                        $coupon = $this->paginate($this->Coupons)->toArray();

        $this->set('coupon', $coupon);

        if(isset($_GET['query'])){
                $currentDate = date('Y-m-d');
                $condition = [

                                    'OR' => [ 

                                       'Coupons.name LIKE'=>'%'.$_GET['query'].'%',
                                     ]
                                  ];
                $this->paginate = [
                                        'sortWhitelist' => [
                                        'Coupons.name','Coupons.price_value'
                                        ],
                                        'limit' => 10,
                                        'order'=>['Coupons.id DESC'],
                                        'conditions'=>$condition               
                                    ]; 
                $coupon = $this->paginate($this->Coupons);
                $this->set(compact('coupon'));

        }
    }

    /**
     * View method
     *
     * @param string|null $id Coupon id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $id = base64_decode($id);
        $couponsTable = TableRegistry::get('CouponTrackers');
        $coupon = $couponsTable->find('all')->where(['CouponTrackers.coupon_id'=>$id])->contain(['Coupons']);
        
        $this->paginate = [
                            'limit' => 20,
                            'order' => [
                                'Coupons.id' => 'desc'
                            ] 
                        ];
        $coupon = $this->paginate($coupon)->toArray();
        $this->set('coupon', $coupon);
         
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    private function nameAlreadyThere($name){

           $exits = $this->Coupons->find()->where(['Coupons.name'=>$name])->count();
           if($exits){
            return true;
           }return false;

    }
    private function validateAddCoupons( $data ){
      
        $error = array();
        if( ! isset($data['name']) || empty( $data['name'] ) ){
            $error['name'] = "Please enter the name";
        }else if( ! isset($data['price_value']) || empty( $data['price_value'] ) ){
            $error['name'] = "Please enter the Price value of coupons";
        }else if( ! isset($data['n_o_c']) || empty( $data['n_o_c'] ) ){
            $error['name'] = "Please enter the No Of Coupons";
        }else if( $this->nameAlreadyThere($data['name']) ){
            $error['name'] = "Coupons name already exists";
        }

        if(isset($error['name']) && count($error['name']) > 0){
            $error['statuscode'] = 201;
        }else{
            $error['statuscode'] = 200;
        }
        return $error;
    }

    private function nativeUniqueId(){
                $len=12;
                $last=-1;
                $code = '';
                for ($i=0;$i<$len;$i++)
                {
                    do
                    {
                        $next_digit=mt_rand(0,11);
                    }
                    while ($next_digit == $last);
                    $last=$next_digit;
                    $code.=$next_digit;
            }
            return $code;
    } 


    public function add()
    {
        
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $error = $this->validateAddCoupons($data);
             
           if($error['statuscode'] == 200)
           { 

                    
                    $coupon = $this->Coupons->newEntity();
                    $coupon->name = $data['name'];
                    $coupon->price_value = $data['price_value'];
                    $coupon->start_date = date('Y-m-d');
                    $coupon->end_date = $data['end_date'];
                    $res = $this->Coupons->save($coupon);                     
                    $coupons_id = $res->id;
                     
                    $couponsTrackersTable = TableRegistry::get('CouponTrackers');                     

                    for($it = 1;$it<=$data['n_o_c'];$it++){
                           
                           $couponsTrackers = $couponsTrackersTable->newEntity();
                           $couponsTrackers->coupon_id = $coupons_id;
                           $result = $this->getCouponCode();
                           $couponsTrackers->s_c_c = $result['s_c_c'];
                           $couponsTrackers->sr_no = $result['sr_no'];
                           $couponsTrackersTable->save($couponsTrackers);

                    }
           $this->Flash->success(__('The coupon has been saved.')); 
           return $this->redirect(['action' => 'index']);        
          }else{

            $this->set('data',$data);
            $this->set('error',$error);
          }
        

        }
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Coupon id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coupon = $this->Coupons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coupon = $this->Coupons->patchEntity($coupon, $this->request->getData());
            if ($this->Coupons->save($coupon)) {
                $this->Flash->success(__('The coupon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The coupon could not be saved. Please, try again.'));
        }
        $this->set(compact('coupon'));
        $this->set('_serialize', ['coupon']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Coupon id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coupon = $this->Coupons->get($id);
        if ($this->Coupons->delete($coupon)) {
            $this->Flash->success(__('The coupon has been deleted.'));
        } else {
            $this->Flash->error(__('The coupon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function barcode($id=null)
    {
        $id   = $_GET['id'];
        $id = base64_decode($id);
        $couponsTable = TableRegistry::get('CouponTrackers');
        $coupon = $couponsTable->find()->select(['s_c_c'])->where(['coupon_id'=>$id])->toArray();
        $this->set('coupon', $coupon);
    }
}
