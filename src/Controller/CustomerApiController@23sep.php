<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Twilio\Rest\Client;

class CustomerApiController extends AppController
{
    protected $_futureDate = '';

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['sendOtp', 'getSchdule', 'getBalanceAndDeliveryInfo', 'getCustomerBalance', 'recomdedItemMilkManDetails', 'categoryProduct', 'verificationOtp', 'updateProfile', 'getAllRegions', 'getAllRegionsArea', 'searchProduct', 'updateOrder', 'addSubscription', 'getCategoriesAndSubscriptionTypes', 'checkCategoryDeliverThisCustomer', 'getProductUnitNameAndChieldren', 'getProductChildrenPrice', 'getProductPriceManually', 'activeDeActiveSubscriptionList', 'changeSubscriptionStatus', 'addToNextDelivery', 'getCalendraEvent', 'gettransactions', 'aboutus', 'checkifcustomordersnotdelivered1', 'paytm', 'payumoney', 'createchecksum', 'verifychecksum', 'paytmurl', 'addtransaction', 'getorderhistory', 'pushnotifications', 'getnotifications', 'addpush', 'updateusersubscriptions', 'calculateContainers', 'addfeedback', 'feedbacklist', 'reedemcoupon', 'balancenotifications', 'sendmail', 'pushnotificationstoall', 'getdailymilk','deletecustomorder','getAllSubscriptions','contactus']);
    }

    /*---Start code for multilanguage from 1000-----*/

    /*--status code reserve for sendOtp function Start from 1000 to 1020---*/

    private function checkMobileNo($mobileNo = null)
    {

        if ($mobileNo) {

            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'type_user' => 'customer'])->count();

            if ($user) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private function validateLogin($data)
    {

        $error = array();
        if (!isset($data['mobileNo']) || empty($data['mobileNo'])) {
            $error['messageText'] = "Please enter your mobileNo";
            $error['messageCode'] = 1000;
            $error['successCode'] = 0;
        } else if (!isset($data['user_type']) || empty($data['user_type'])) {
            $error['messageText'] = "Please enter customer type";
            $error['messageCode'] = 1001;
            $error['successCode'] = 0;
        }

        /*else if( $this->checkMobileNo($data['mobileNo'])){
        $error['messageText'] = "Mobile no. already exist.";
        $error['messageCode'] = 1001;
        $error['successCode'] = 0;
        }*/else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    /* private function alreadyAuthorizedWithOtp($mobileNo){

    if($mobileNo){

    $userTable = TableRegistry::get('Users');
    $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer','otp <>'=>0])->count();
    if($user){
    return true;
    }else{
    return false;
    }

    }
    }*/

    private function alreadyAuthorizedWithOtp($mobileNo)
    {

        if ($mobileNo) {

            $userTable = TableRegistry::get('Users');

            $user = $userTable->find()->where(['phoneNo' => $mobileNo, 'type_user' => 'customer', 'otp <>' => 0, 'verifed_otp' => 1])->count();

            /* $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer'])->count();*/
            if ($user) {
                return true;
            } else {
                return false;
            }

        }
    }

    private function updateOtp($customer_type, $mobile)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => trim($mobile), 'type_user' => trim($customer_type)])->count();
        //pr($user);die;
        if ($user) {
            return true;
            /*$query = $userTable->query();
        $result = $query->update()
        ->set(['verifed_otp'=>0])
        ->where(['phoneNo' => $mobile])
        ->execute();
        if($result){
        return true;
        }else{
        return false;
        }*/

        } else {

            $userTable                = TableRegistry::get('Users');
            $newcustomer              = $userTable->newEntity();
            $newcustomer->verifed_otp = 0;
            $newcustomer->phoneNo     = $mobile;
            $newcustomer->created     = date('Y-m-d');
            $newcustomer->modified    = date('Y-m-d');
            $newcustomer->type_user   = 'customer';
            $newcustomer->status      = 0;
            $result                   = $userTable->save($newcustomer);

            if ($result->id) {
                return true;
            } else {
                return false;
            }
        }

    }

    private function updateToken($mobile)
    {

        $userTable = TableRegistry::get('Users');
        $token     = \Cake\Utility\Text::uuid();
        $modified  = date('Y-m-d h:i:s');
        $query     = $userTable->query();
        $result    = $query->update()
            ->set(['token' => $token, 'modified' => $modified])
            ->where(['phoneNo' => $mobile])
            ->execute();
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    private function sendOtpToCustomer($otp, $mobileNo)
    {

        require_once ROOT . DS . 'vendor' . DS . 'Twilio' . DS . 'autoload.php';
        $sid    = 'ACaf9007c5da5a2f68a4fed37ee698ad8b';
        $token  = '72c1413a90e3d1526e983167c1cc7030';
        $client = new Client($sid, $token);
        $client->messages->create(
            '+91' . $mobileNo,
            array(
                'from' => '+17603787142',
                'body' => 'Your cremeway Verification code is ' . $otp,
            )
        );

        return true;

    }

    private function checkAddByAdmin($mobileNo)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'status' => 1])->count();

        if ($user) {
            $error['messageCode'] = 200;
        } else {
            $error['messageCode'] = 201;
        }
        return $error;

    }

    private function verifedByOtp($mobileNo)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'otp IS NOT NULL', 'verifed_otp' => 1])->count();

        if ($user) {
            $error['messageCode'] = 200;
        } else {
            $error['messageCode'] = 201;
        }
        return $error;

    }

    /*--status code reserve for VerificationOtp function Start from 2400 2440 ---*/

    public function verificationOtp()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $this->updateOtp($data['user_type'], $data['mobileNo']);
            $error = $this->validateVerificationCode($data);
            if ($error['messageCode'] == 200) {
                $result = $this->updateAfterVerification($data);
                if ($result) {
                    $usersTable   = TableRegistry::get('Users');
                    $customerInfo = $usersTable
                        ->find('all')
                        //->contain(['Regions','Areas'])
                        ->where(['Users.phoneNo' => $data['mobileNo'], 'Users.type_user' => $data['user_type']])
                        ->toArray();
                    $error['customerInfo'] = $customerInfo;
                    $currentDateTime       = date('Y-m-d h:i:s');
                    $newDateTime           = date('h:i A', strtotime($currentDateTime));
                    $error['loggedtime']   = date("H:i", strtotime($newDateTime));
                } else {
                    $error['messageText'] = "Something went wrong please try again";
                    $error['successCode'] = 0;
                    $error['messageCode'] = 2403;
                }
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }
    private function updateAfterVerification($data)
    {

        $usersTable  = TableRegistry::get('Users');
        $token       = \Cake\Utility\Text::uuid();
        $modified    = date('Y-m-d h:i:s');
        $verifed_otp = 1;
       // $status      = 0;
        $query       = $usersTable->query();
        $result      = $query->update()
            ->set(['device_id' => $data['device_id'], 'token' => $token, 'modified' => $modified])
            ->where(['phoneNo' => $data['mobileNo'], 'type_user' => $data['user_type']])
            ->execute();

        /* update latlng in order delivery */
        $query1 = $usersTable->find('all')->select(['id'])->where(['phoneNo' => $data['mobileNo']])->hydrate(false)->toArray();

        $this->updateLatlng($query1[0]['id'], $data['lat'], $data['lng']);

        /* update latlng in order delivery */

        if ($result) {
            return true;
        }return false;

    }
    private function isMatchOtpMobile($mobileNo, $otp, $user_type)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'otp' => $otp, 'type_user' => $user_type])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateVerificationCode($data)
    {

        $error = array();
        if (!isset($data['mobileNo']) || empty($data['mobileNo'])) {
            $error['messageText'] = "Please enter your mobileNo";
            $error['messageCode'] = 1000;
            $error['successCode'] = 0;
        } /*elseif( ! isset( $data['otp_code'] ) || empty( $data['otp_code'] )){
        $error['messageText'] = "Please enter your otp code";
        $error['messageCode'] = 2401;
        $error['successCode'] = 0;
        }*/elseif (!isset($data['device_id']) || empty($data['device_id'])) {
            $error['messageText'] = "Please enter your device id";
            $error['messageCode'] = 2405;
            $error['successCode'] = 0;
        } else if (!isset($data['user_type']) || empty($data['user_type'])) {
            $error['messageText'] = "Please enter User type";
            $error['messageCode'] = 2406;
            $error['successCode'] = 0;
        }
        /*elseif( ! $this->isMatchOtpMobile($data['mobileNo'],$data['otp_code'],$data['user_type']) ){
        $error['messageText'] = "Invalid Details";
        $error['messageCode'] = 2402;
        $error['successCode'] = 0;
        }*/else {
            $error['messageCode'] = 200;
        }
        return $error;

    }
    public function sendOtp()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data  = $this->request->getData();
            $error = $this->validateLogin($data);
            if ($error['messageCode'] == 200) {
                $digits    = 4;
                $otp       = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $updateOtp = $this->updateOtp($data['user_type'], $data['mobileNo']);
                $sendOtp   = $this->sendOtpToCustomer($otp, $data['mobileNo']);
                if ($sendOtp && $updateOtp) {
                    $error['messageText'] = "Otp has been sent to customer";
                    $error['messageCode'] = 1003;
                    //$error['messageCode'] = 200;
                    $error['successCode'] = 1;

                } else {
                    $error['messageText'] = "Something went wrong.Please try again";
                    $error['messageCode'] = 1004;
                    $error['successCode'] = 0;
                }
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }

    /*--status code reserve for sendOtp function Start from 1020 to 1050---*/

    private function validateupdateProfileCustomer($data)
    {

        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageText'] = "User id can not be empty";
            $error['messageCode'] = 1051;
            $error['successCode'] = 0;
        } else if (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Token can not be empty";
            $error['messageCode'] = 1052;
            $error['successCode'] = 0;
        } else if (!$this->notThisUser($data['user_id'], $data['token'])) {
            $error['messageText'] = "Invlid user";
            $error['messageCode'] = 1053;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    public function getSchdule()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $driverRouteTable = TableRegistry::get('DeliverySchdules');
                $schdule          = $driverRouteTable->find('all')->hydrate(false)->toArray();

                if (count($schdule) > 0) {

                    foreach ($schdule as $key => $value) {

                        $newDateTime                 = date('h:i A', strtotime($value['start_time']));
                        $schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

                        $newDateTime               = date('h:i A', strtotime($value['end_time']));
                        $schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));

                    }

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = $schdule;

                } else {

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = count($schdule);
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function notThisUser($user_id, $token)
    {
        $driverTable = TableRegistry::get('Users');
        $driverInfo  = $driverTable
            ->find()
            ->select(['id', 'name', 'status'])
            ->where(['id' => $user_id])
            ->andwhere(['token' => $token])
            ->andwhere(['type_user' => 'customer'])
            ->andwhere(['is_deleted' => 0])
            ->toArray();
            
        if (count($driverInfo) > 0) {
            //if($driverInfo[0]->status){
              return true;
           /* } else {
              $response['messageText'] = "User is not activated";
              $response['messageCode'] = 1982;
              $response['successCode'] = 0;
              echo json_encode($response);die;
            }*/
        }
        return false;
    }

    /*--status code reserve for sendOtp function Start from 1051 to 1100---*/

    private function returnDayName($day = null)
    {
        if ($day && $day != '') {
            $name = $day;
        } else {
            $name = date('l');
        }
        switch ($name) {
            case 'Sunday':
                return 7;
                break;
            case 'Monday':
                return 1;
                break;
            case 'Tuesday':
                return 2;
                break;
            case 'Wednesday':
                return 3;
                break;
            case 'Thursday':
                return 4;
                break;
            case 'Friday':
                return 5;
                break;
            case 'Saturday':
                return 6;
                break;
            default:
                break;
        }
    }

    private function checkTodaySubscriptions($id, $type, $days, $schduleID, $cdate = null)
    {
        if($cdate) {
          $cdate = $cdate;
          $now  = strtotime($cdate);
        } else {
          $cdate = date('Y-m-d');
          $now  = time();
        }
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id' => $id])->hydrate(false)->first();
        if (count($userSubscriptions) > 0) {
            if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {
           
              /*  if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                    if ($userSubscriptions['startdate']->format('Y-m-d') <= date('Y-m-d')) {
                        $nextdate['comingdate'] = date('Y-m-d');
                    } else {
                        $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                    }
                    return $nextdate;
                }
                $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                return $nextdate;*/
                $today['comingdate'] = $cdate;
                return $today;
            } else if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate') {
                $startdate       = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                $subscriptionday = strtotime($startdate);
                $now             = $now;
                $datediff        = $now - $subscriptionday;
                $days            = floor($datediff / (60 * 60 * 24))+1;

                if($days < 1) {
                   $today['comingdate'] = $cdate;
                   return $today;
                }
             /*   echo (0%2);
                pr($days);die('hh');*/
                if ($days % 2 == 0) {
                    /*if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                        $today['comingdate'] = date('Y-m-d');
                        return $today;
                    } else {
                        $today['comingdate'] = date('Y-m-d');
                        return $today;
                    }*/
                    return false;
                } else {
                    /*if (in_array($schduleID, explode(',', $userSubscriptions['delivery_schdule_ids']))) {
                        $date                   = date('Y-m-d');
                        $date1                  = str_replace('-', '/', $date);
                        $tomorrow               = date('Y-m-d', strtotime($date1 . "+1 days"));
                        $nextdate['comingdate'] = $tomorrow;
                        return $nextdate;
                    } else {
                        return false;
                    }*/
                    $today['comingdate'] = $cdate;
                    return $today;
                }

            }
        }
        return false;
    }

    private function factorySubscription($data, $schduleID,$day = null)
    {
      if($day == 'tomorrow') {
        $cdate = date("Y-m-d", strtotime("+1 day"));
      } else if($day == 'other' || $day == 'otherafter') {
        if($day == 'other'){
          $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
          $tomorrow = date("Y-m-d", strtotime("+1 day"));
          $usersub = $userSubscriptionsTable->find('all')->where(['user_id' => $data[0]['id'],'users_subscription_status_id' => 1,'startdate >' => $tomorrow])->order(['startdate'])->hydrate(false)->toArray();
          $cdate = $usersub[0]['startdate']->i18nFormat('yyyy-MM-dd');
        } else {
          $cdate = date("Y-m-d", strtotime("+2 day"));
        }
      } else if($this->_futureDate) {
        $cdate = $this->_futureDate;
      }else {
        $cdate = date("Y-m-d");
      }
        $final = array();
        foreach ($data as $key => $value) {
          // pr($value);die;
            $final1 = array();
            if ((isset($value['custom_orders']) && !empty($value['custom_orders'])) || (isset($value['user_subscriptions']) && !empty($value['user_subscriptions']))) {
                if (isset($value['user_subscriptions']) && !empty($value['user_subscriptions'])) {
                    foreach ($value['user_subscriptions'] as $k => $v) {
                        $insideorders = array();
                        $date = $this->checkTodaySubscriptions($v['id'], $v['subscription_type_id'], $v['days'], $v['delivery_schdule_ids'],
                          $cdate);
                        if (isset($date['comingdate']) && !empty($date['comingdate'])) {
                            $deliveryTimeTable = TableRegistry::get('DeliverySchdules');
                            $deliveryTime      = $deliveryTimeTable->find()->select(['name', 'start_time', 'end_time'])->where(['id' => $schduleID])->first();
                            $insideorders['subscription_id']      = $v['id'];
                            $insideorders['subscription_type_id'] = $v['subscription_type_id'];
                            $insideorders['name']                 = $v['product']['name'];
                            $insideorders['description']          = @$v['product']['description'];
                            $insideorders['price']                = $v['subscriptions_total_amount'];
                            $insideorders['price_per_unit']       = $v['product']['price_per_unit'];
                            $insideorders['pro_id']               = $v['product']['id'];
                            $insideorders['quantity']             = $v['quantity'];
                            $insideorders['unit']                 = $v['product']['unit']['name'];
                            $insideorders['image']                 = @$v['product']['image'];
                            $insideorders['deliverydate'] = $date['comingdate'];
                            $insideorders['startdate']    = $v['startdate'];
                            $insideorders['timeToBeDeliver'] = $deliveryTime['name'];
                            $insideorders['between']         = $deliveryTime['start_time'] . '-' . $deliveryTime['end_time'];
                        }
                        if (count($insideorders) > 0) {
                            $final1[] = $insideorders;
                        }
                    }
                }

                if (count($final1) > 0) {
                    $highestInfo                     = array();
                    $highestInfo['subscriptionInfo'] = $final1;
                    $final[]                         = $highestInfo;
                }
            } else {
                continue;
            }
        }
        $my_new_date       = $cdate;
        $customordersTable = TableRegistry::get('CustomOrders');
        $orderNext = $customordersTable->find('all')
            ->where(['CustomOrders.user_id' => $data[0]['id'],
            //'CustomOrders.delivery_schdule_id' => $schduleID, 
            'CustomOrders.status' => 0, 
            //'CustomOrders.created' => $my_new_date,
            ])->contain(['Products', 'Units'])->hydrate(false)->toArray();
        $orderInfo = array();
        if (count($orderNext) > 0) {
            foreach ($orderNext as $key => $value) {
                $orderItem             = array();
                $orderItem['id']     = $value['id'];
                $orderItem['name']     = $value['product']['name'];
                $insideorders['description']  = @$v['product']['description'];
                $orderItem['image']     = @$value['product']['image'];
                $orderItem['pro_id']   = $value['product']['id'];
                $orderItem['quantity'] = $value['quantity'];
                $orderItem['price']    = $value['price'];
                $orderItem['price_per_unit']       = @$value['product']['price_per_unit'];
                $orderItem['unit']     = $value['unit']['name'];
                $orderInfo[]           = $orderItem;
            }
        }
       if(empty($final) && empty($orderInfo)){
        return $final;
       }
      $final['customOrderInfo'] = $orderInfo;
      return $final;
    }

    private function checkItemThisTime($user_id, $schdule_id, $day = null)
    {
        date_default_timezone_set('Asia/Kolkata');
        $userTable = TableRegistry::get('Users');
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userId    = $user_id;
        if($day == 'tomorrow'){
          $tomorrow = date("Y-m-d", strtotime("+1 day"));
          $day_to_del = "tomorrow";
          $allUserSubOrder = $userTable->find('all')->contain([
                'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id,$tomorrow) {
                    return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $tomorrow, 'FIND_IN_SET ('.$schdule_id.',UserSubscriptions.delivery_schdule_ids)']);
                },
            ])->where(['Users.id' => $userId])->hydrate(false)->toArray();

        } else if($day == 'other') {
            $day_to_del = "otherafter";
            $tomorrow = date("Y-m-d", strtotime("+1 day"));

            //check alternate day after tomorrow
            $aftertomorrow = date("Y-m-d", strtotime("$tomorrow+1 day"));
    
            $allUserSubOrder = $userTable->find('all')->contain([
                'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id,$aftertomorrow) {
                    return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $aftertomorrow, 'FIND_IN_SET ('.$schdule_id.',UserSubscriptions.delivery_schdule_ids)']);
                },
            ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            //pr($aftertomorrow);die;
            if(isset($allUserSubOrder[0]['user_subscriptions']) && empty($allUserSubOrder[0]['user_subscriptions'])){
               $day_to_del = "other";
              //check other day after tomorrow
                  $usersub = $userSubscriptionsTable->find('all')->where(['user_id' => $userId,'users_subscription_status_id' => 1,'startdate >' => $tomorrow])->order(['startdate'])->hydrate(false)->toArray();
                  if(!empty($usersub)){
                    $otherdate = $usersub[0]['startdate']->i18nFormat('yyyy-MM-dd');
                    $allUserSubOrder = $userTable->find('all')->contain([
                      'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id,$otherdate) {
                          return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $otherdate, 'FIND_IN_SET ('.$schdule_id.',UserSubscriptions.delivery_schdule_ids)']);
                      },
                    ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
                  } else {
                    return false;
                  }
            }    
        } else {
            if($this->_futureDate){
                $day_to_del = "future";
                $futuredate = $this->_futureDate;
                $allUserSubOrder = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id,$futuredate) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => $futuredate, 'FIND_IN_SET ('.$schdule_id.',UserSubscriptions.delivery_schdule_ids)']);
                    },
                ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            } else {
                $day_to_del = "today";
                $allUserSubOrder = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($schdule_id) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => date('Y-m-d'), 'FIND_IN_SET ('.$schdule_id.',UserSubscriptions.delivery_schdule_ids)']);
                    },
                ])->where(['Users.id' => $userId])->hydrate(false)->toArray();
            }
        }
        

        
        // get today deliver schedules ids
        if(isset($allUserSubOrder[0]['user_subscriptions']) && !empty($allUserSubOrder[0]['user_subscriptions'])){
            // pr($allUserSubOrder);die;
            if($day =='todaycurrent'){
              $transtable = TableRegistry::get('Transactions');
              $trans = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type'=>'Dr','Transactions.created' => date('Y-m-d'),'Transactions.delivery_schdule_id' => $schdule_id])->hydrate(false)->toArray();
              $subids = array();
              if(!empty($trans)){
                foreach ($trans as $key => $value) {
                    $sids = explode(',', $value['user_subscription_ids']);
                    foreach ($sids as $val) {
                        if (!in_array($val, $subids)) {
                            array_push($subids, $val);
                        }
                    }
                }
              }
              
              if(!empty($subids)){
                $usersubscriptions = array();
                foreach ($allUserSubOrder[0]['user_subscriptions'] as $key => $value) {
                  if(!in_array($value['id'], $subids)){
                    $usersubscriptions[] =  $value;
                  }
                }
                $allUserSubOrder[0]['user_subscriptions'] = $usersubscriptions;
              }
            }

            
            $itemsInfno = $this->factorySubscription($allUserSubOrder, $schdule_id,$day_to_del);
            if (count($itemsInfno) > 0) {
                $itemsInfno['day'] = $day_to_del;
                return $itemsInfno;
            } else {
                return false;
            }
        } else {
           return false;
        }
    }

    private function checkItemAnotherTime($userid, $schduleID)
    {

        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find('all')->where(['user_id' => $userid, 'delivery_schdule_ids' => $schduleID])->toArray();
        $deliversSchdulesIds    = array();
        foreach ($userSubscriptions as $key => $value) {

            $d_sids = explode(',', $value['delivery_schdule_ids']);
            foreach ($d_sids as $k => $v) {
                array_push($deliversSchdulesIds, $v);
            }
        }

        $deliversSchdulesIds = array_unique($deliversSchdulesIds);

        $currentDateTime    = date('Y-m-d h:i:s');
        $customerLoggedTime = date('h:i A', strtotime($currentDateTime));
        $dateObject         = new \DateTime;
        $nowTime            = $dateObject::createFromFormat('H:i A', $customerLoggedTime);
        $dayName            = '';
        $iterationCount     = 1;

        $deliverySchduleTable = TableRegistry::get('DeliverySchdules');

        $previous_diffrence   = 0;
        $next_diffrence       = 0;
        $finalDeliverySchdule = 0;
        foreach ($deliversSchdulesIds as $ke => $val) {

            $d_s        = $deliverySchduleTable->find()->where(['id' => $val])->first();
            $start_time = $d_s['start_time'];
            $end_time   = $d_s['end_time'];
            $d_s_t      = $dateObject::createFromFormat('H:i A', $start_time);
            $d_e_t      = $dateObject::createFromFormat('H:i A', $end_time);
            if ($nowTime < $d_s_t) {
                $dayName             = 'Today';
                $next_diffrence_temp = $d_s_t->diff($nowTime);

                $day            = $next_diffrence_temp->format('%d');
                $hour           = $next_diffrence_temp->format('%h');
                $minute         = $next_diffrence_temp->format('%i');
                $next_diffrence = ($day * 24 * 60) + ($hour * 60) + $minute;
                if ($iterationCount == 1) {
                    $previous_diffrence   = $next_diffrence;
                    $finalDeliverySchdule = $val;

                } else {

                    if ($next_diffrence < $previous_diffrence) {
                        $previous_diffrence   = $next_diffrence;
                        $finalDeliverySchdule = $val;
                    }
                }

            } else {

                $dayName             = 'Tomorrow';
                $next_diffrence_temp = $d_s_t->diff($nowTime);

                $day            = $next_diffrence_temp->format('%d');
                $hour           = $next_diffrence_temp->format('%h');
                $minute         = $next_diffrence_temp->format('%i');
                $next_diffrence = ($day * 24 * 60) + ($hour * 60) + $minute;

                if ($iterationCount == 1) {
                    $previous_diffrence   = $next_diffrence;
                    $finalDeliverySchdule = $val;
                } else {

                    if ($next_diffrence > $previous_diffrence) {
                        $previous_diffrence   = $next_diffrence;
                        $finalDeliverySchdule = $val;
                    }
                }
            }

            $iterationCount++;

        }

        //$isItemFoundForThisTime = $this->checkItemThisTime($userid,$finalDeliverySchdule);
        $isItemFoundForThisTime = $this->checkItemThisTime($userid, $schduleID);
        $response               = array();
        $response['itemss']     = $isItemFoundForThisTime;
        if (isset($value['id']) && !empty($value['id'])) {
            $response['d_s_i'] = $value['delivery_schdule_ids'];
        }

        $response['deliverydate1'] = $dayName;

        //echo $previous_diffrence;die;
        return $response;

    }

    private function checkNextDelivery($data , $futureDate = null)
    {

        date_default_timezone_set('Asia/Kolkata');
        $response1 = array();
        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $usersub               = $userSubscriptionTable->find('all')->where(['user_id' => $data['user_id'],'users_subscription_status_id' => 1])->hydrate(false)->toArray();
        $ds_id                 = array();
        foreach ($usersub as $key => $value) {
            $dids = explode(',', $value['delivery_schdule_ids']);
            foreach ($dids as $val) {
                if (!in_array($val, $ds_id)) {
                    array_push($ds_id, $val);
                }
            }
        }

        $isItemFoundForThisTime = array();
        if($this->_futureDate){
            $currentDateTime        = $this->_futureDate;
        }else{
            $currentDateTime        = date('Y-m-d h:i:s A');
        }
       
        $newDateTime            = date('h:i A', strtotime($currentDateTime));

        $response               = array();
        $schdulew               = array();
        $schduleq               = array();
        $final_dsid             = array();
        foreach ($ds_id as $key11 => $value11) {
            $driverRouteTable = TableRegistry::get('DeliverySchdules');
            $schdulew         = $driverRouteTable->find('all')->where(['id' => $value11])->hydrate(false)->toArray();
            if(!empty($schdulew)){
                array_push($schduleq, $schdulew);
            }
        }
       
        //Return subscription for current schedule of today
        if (count($schduleq) > 0) {

            foreach ($schduleq as $key => $value) {
                $current_time = $newDateTime;
                $startTime    = $value[0]['start_time'];
                $endTime      = $value[0]['end_time'];
                $dateObject   = new \DateTime;
                $date1        = $dateObject::createFromFormat('H:i a', $current_time);
                $date2        = $dateObject::createFromFormat('H:i a', $startTime);
                $date3        = $dateObject::createFromFormat('H:i a', $endTime);

                if ($date1 > $date2 && $date1 < $date3) {
                    $isItemFoundForThisTimee    = $this->checkItemThisTime($data['user_id'], $value[0]['id'],'todaycurrent');
                     
                    if(!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])){
                      $response['itemss']        = $isItemFoundForThisTimee;
                      $response['d_s_i']         = $value[0]['id'];
                      $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                      return $response;
                    }
                }
            }

            //sort schedules in ascending order
            $sorttime = array();
            $temp = array();
            foreach ($schduleq as $key => $value) {
              $sorttime[$value[0]['id']] =  $dateObject::createFromFormat('H:i a', $value[0]['start_time']);
              $temp[$value[0]['id']][] = $value[0];
            }
            // array_multisort($startdate, SORT_ASC, $products);
            asort($sorttime);
            $schduleq = array();
            foreach ($sorttime as $key => $value) {
              $schduleq[] = $temp[$key];
            }
            $rep = array();
            //Return subscription for next schedule of today
            foreach ($schduleq as $key => $value) {

              $current_time = $newDateTime;
              // pr($current_time);die;
              $startTime    = $value[0]['start_time'];
              $endTime      = $value[0]['end_time'];
              $dateObject   = new \DateTime;
              $date1        = $dateObject::createFromFormat('H:i a', $current_time);
              $date2        = $dateObject::createFromFormat('H:i a', $startTime);
              $date3        = $dateObject::createFromFormat('H:i a', $endTime);
              if ($date1 < $date2) {
                   $isItemFoundForThisTimee    = $this->checkItemThisTime($data['user_id'], $value[0]['id']);
                  //pr($isItemFoundForThisTimee);die('h');
                  if(!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])){
                    $response['itemss']        = $isItemFoundForThisTimee;
                    $response['d_s_i']         = $value[0]['id'];
                    $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
                    if($this->_futureDate){
                        $rep[$value[0]['id']] = $response;

                    } else {
                        return $response;
                    }
                  }
              }
            } 
            if($this->_futureDate){
                return $rep;
                pr($rep);die;
            }
          //Return subscription for first schedule of tomorrow
          
          foreach ($schduleq as $key => $value) {
            $isItemFoundForThisTimee    = $this->checkItemThisTime($data['user_id'], $value[0]['id'],'tomorrow');
           
            if(!empty($isItemFoundForThisTimee) && isset($isItemFoundForThisTimee[0]['subscriptionInfo']) && !empty($isItemFoundForThisTimee[0]['subscriptionInfo'])){
              $response['itemss']        = $isItemFoundForThisTimee;
              $response['d_s_i']         = $value[0]['id'];
              $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
              return $response;
            } 
          }

          //Return subscription for other day after tomorrow
          foreach ($schduleq as $key => $value) {
            $tomorrow = date("Y-m-d", strtotime("+1 day"));
            $isItemFoundForThisTimee    = $this->checkItemThisTime($data['user_id'], $value[0]['id'],'other');
            if(!empty($isItemFoundForThisTimee)){
              $response['itemss']        = $isItemFoundForThisTimee;
              $response['d_s_i']         = $value[0]['id'];
              $response['deliverydate1'] = $isItemFoundForThisTimee['day'];
              return $response;
            } 
          }
        } 
        return false;
    }

    private function getPrice($orderInfo)
    {
        $totalPrice  = 0;
        $producTable = TableRegistry::get('Products');
        if (isset($orderInfo['itemss'][0]['subscriptionInfo'])) {
            foreach ($orderInfo['itemss'][0]['subscriptionInfo'] as $key => $value) {
                $totalPrice = $totalPrice + $value['price'];
            }
        }
        if (isset($orderInfo['itemss']['customOrderInfo'])) {
            foreach ($orderInfo['itemss']['customOrderInfo'] as $key => $value) {
                $totalPrice = $totalPrice + $value['price'];
            }
        }
        return $totalPrice;
    }

    public function getBalanceAndDeliveryInfo($data=null,$datatype=null)
    {
        $futureDate = '';
        $todayDate = date('Y-m-d');
        $response = array();
        if ($this->request->is('post') || $datatype) {
            if($datatype){
                $data     = $data;
                if(isset($data['date']) && $data['date']){
                    if($datatype == 'future'){
                        $this->_futureDate = $data['date'];    
                    } 
                }
            }else{
                $data     = $this->request->getData();
            }
            
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable  = TableRegistry::get('Users');
                $userstatus = $userTable->find()->select(['status'])->where(['Users.id' => $data['user_id']])->toArray();

                $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
                $usersub               = $userSubscriptionTable->find('all')->where(['user_id' => $data['user_id'], 'users_subscription_status_id' => 1])->toArray();
                if (count($usersub) > 0) {
                    $checkNextDeliveryTime = $this->checkNextDelivery($data);

                    if($this->_futureDate){
                        return $checkNextDeliveryTime;
                    }
                    // pr($checkNextDeliveryTime);die('checkn');
                    $totalPrice            = $this->getPrice($checkNextDeliveryTime);
                    if ($checkNextDeliveryTime) {
                        $response['messageText']                 = "success";
                        $response['messageCode']                 = 200;
                        $response['successCode']                 = 1;
                        $response['userstatus']                  = $userstatus[0]['status'];
                        $response['subscriptionOrderTotalPrice'] = $totalPrice;
                        $response['customerBalance']             = $this->updatedCustomersBalance($data['user_id']);
                        if (isset($checkNextDeliveryTime['d_s_i']) && !empty($checkNextDeliveryTime['d_s_i'])) {
                            $response['deliver_schdule_id'] = $checkNextDeliveryTime['d_s_i'];
                        }

                        $response['willDeliver'] = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ?$checkNextDeliveryTime['itemss'][0]['subscriptionInfo'][0]['deliverydate'] :'';

                        $products = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ? $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] : array();

                        if(empty($products)) {
                            $response['messageText'] = "No Subscription found for this customer";
                            $response['messageCode'] = 1054;
                            $response['successCode'] = 0;
                            if($datatype){
                                return $response;
                            }
                            echo json_encode($response);die;
                        }
                        foreach ($products as $key2 => $row2) {
                            $startdate[$key2] = $row2['startdate'];

                        }
                        array_multisort($startdate, SORT_ASC, $products);
                        // pr($products);die('check');
                        /*$value = $products[0]['startdate'];
                        foreach ($products as $key => $product) {
                            if ($product['startdate'] != $value) {
                                unset($products[$key]);
                            }
                        }*/

                        $response['subscriptionItems'] = $products;
                        if (isset($checkNextDeliveryTime['itemss']['customOrderInfo'])) {
                            $response['orderItems'] = $checkNextDeliveryTime['itemss']['customOrderInfo'];
                        }
                    } else {
                        $response['messageText'] = "No Subscription found for this customer";
                        $response['messageCode'] = 1054;
                        $response['successCode'] = 0;
                    }
                } else {
                    $response['messageText']     = "No Subscription found for this customer";
                    $response['messageCode']     = 200;
                    $response['userstatus']      = $userstatus[0]['status'];
                    $response['customerBalance'] = $this->updatedCustomersBalance($data['user_id']);
                    $response['successCode']     = 1;
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        if($datatype){
            return $response;
        }
        echo json_encode($response);die;
    }

    /*--status code reserve for getCustomerBalance function Start from 2000 to 2030---*/

    private function updatedCustomersBalance($user_id)
    {
        $userBalanceTable = TableRegistry::get('UserBalances');
        $userBalance      = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $user_id])->toArray();
        if (count($userBalance) > 0) {

            return $userBalance[0]['balance'];

        } else {

            return 0;
        }

    }

    /*--status code reserve for getCustomerBalance function Start from 2150 to 2200---*/

    private function getRecomandedItems()
    {
        $producTable = TableRegistry::get('Products');
        $products    = $producTable->find()->select(['Products.id', 'Products.category_id', 'Products.name', 'Products.is_subscribable', 'Products.price_per_unit', 'Products.quantity', 'Products.iscontainer', 'Products.unit_id', 'Products.status', 'Products.created', 'Products.modified', 'Products.image', 'Units.name'])->order(['rand()'])->contain(['Units'])->toArray();
        return $products;
    }

    public function recomdedItemMilkManDetails()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                $routeCustomer      = $routeCustomerTable->find()->select(['route_id'])->where(
                    [
                        'user_id'             => $data['user_id'],
                        'delivery_schdule_id' => $data['delivery_schdule_id'],
                    ])->toArray();

                if (isset($routeCustomer) && count($routeCustomer) > 0) {

                    $routeid          = $routeCustomer[0]['route_id'];
                    $driverRouteTable = TableRegistry::get('DriverRoutes');
                    $driverRoute      = $driverRouteTable->find()->select([
                        'Users.name',
                        'Users.phoneNo',
                        'Users.image',
                        'Users.id',
                    ])->contain(['Users'])->where(['DriverRoutes.route_id' => $routeid])->toArray();
                    if(empty($driverRoute)){
                        $response['messageText'] = "Not Found Any Driver for this route";
                        $response['messageCode'] = 2151;
                        $response['successCode'] = 0;
                        echo json_encode($response);die;
                    }
                    $orderTrackingsTable = TableRegistry::get('OrderTrackings');
                    $driverRoute1        = $orderTrackingsTable->find()->select([
                        'OrderTrackings.lat',
                        'OrderTrackings.lng',
                    ])->where(['OrderTrackings.user_id' => $driverRoute[0]['Users']['id']])->toArray();

                    $regionsList1 = array();
                    foreach ($driverRoute as $key => $value) {

                        $temp            = array();
                        $temp['name']    = $value['Users']['name'];
                        $temp['phoneNo'] = $value['Users']['phoneNo'];
                        $temp['image']   = $value['Users']['image'];

                    }
                    foreach ($driverRoute1 as $key => $value) {
                        $temp['latitude']  = $value['lat'];
                        $temp['longitude'] = $value['lng'];
                    }

                    //$finaldriverRoute = array_merge($driverRoute[0]['Users'],$driverRoute1[0])

                    if (isset($driverRoute) && count($driverRoute) > 0) {

                        $recomandedItems             = array();
                        $recomandedItems             = $this->getRecomandedItems();
                        $response['messageText']     = "success";
                        $response['messageCode']     = 2152;
                        $response['successCode']     = 1;
                        $response['driverInfo']      = $temp;
                        $response['recomandedItems'] = $recomandedItems;

                    } else {

                        $response['messageText'] = "Not Found Any Driver for this route";
                        $response['messageCode'] = 2151;
                        $response['successCode'] = 0;
                    }

                } else {
                    $response['messageText'] = "Not Found Any Driver for this route";
                    $response['messageCode'] = 2151;
                    $response['successCode'] = 0;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    /*--status code reserve for getCustomerBalance function Start from 2201 to 2250 ---*/

    public function categoryProduct()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $category      = array();
                $categoryTable = TableRegistry::get('Categories');
                $category      = $categoryTable->find()->contain(['Products'])->count();
                if (count($category) > 0) {
                    $categorylist                        = $categoryTable->find('all')->contain(['Products.Units'])->hydrate(false)->toArray();
                    $response['messageText']             = "success";
                    $response['messageCode']             = 2251;
                    $response['successCode']             = 1;
                    $response['allCategoryProductsList'] = $categorylist;

                } else {

                    $response['messageText']             = "Not Found and Product";
                    $response['messageCode']             = 2252;
                    $response['successCode']             = 1;
                    $response['allCategoryProductsList'] = $category;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function getCustomerBalance()
    {
        /*NOT IN USE NOW*/
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $userBalanceTable = TableRegistry::get('UserBalances');
                $userBalance      = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $data['user_id']])->toArray();
                if (count($userBalance) > 0) {

                    $response['messageCode']     = 200;
                    $response['successCode']     = 1;
                    $response['customerBalance'] = $userBalance[0]['balance'];
                } else {

                    $response['messageCode']     = 200;
                    $response['successCode']     = 1;
                    $response['customerBalance'] = [];
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function getAllRegions()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $regions     = array();
                $regionTable = TableRegistry::get('Regions');
                $regions     = $regionTable->find('list')->toArray();
                if (count($regions) > 0) {

                    $regionsList = array();

                    foreach ($regions as $key => $value) {

                        $temp              = array();
                        $temp['regionId']  = $key;
                        $temp['regioName'] = $value;
                        array_push($regionsList, $temp);
                    }

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;

                } else {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    /*--status code reserve for getCustomerBalance function Start from 3101 to 3110 ---*/

    public function getAllRegionsArea()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if (!isset($data['region_id']) || empty($data['region_id'])) {

                $response['messageText'] = "Region id can not be empty";
                $response['messageCode'] = 3101;
                $response['successCode'] = 0;
                echo json_encode($response);die;

            }

            if ($response['messageCode'] == 200) {

                $regions    = array();
                $areasTable = TableRegistry::get('Areas');
                $areas      = $areasTable->find('list')->where(['region_id' => $data['region_id']])->toArray();

                $areaList = array();

                if (count($areas) > 0) {

                    $temp = array();
                    foreach ($areas as $key => $value) {
                        $temp['areaId']   = $key;
                        $temp['areaName'] = $value;
                        array_push($areaList, $temp);
                    }

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists']   = $areaList;

                } else {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists']   = $areaList;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

/*--status code reserve for getCustomerBalance function Start from 3111 to 3130 ---*/

    private function updateLatlng($userId, $latitude, $longitude)
    {
        /* update lat lng in OrderTrackings */

        $orderTrackingsTable = TableRegistry::get('OrderTrackings');
        $orderTrackings1     = $orderTrackingsTable->find('all')->where(['user_id' => $userId])->hydrate(false)->toArray();

        if (count($orderTrackings1) > 0) {
            $query  = $orderTrackingsTable->query();
            $result = $query->update()
                ->set(['lat' => $latitude, 'lng' => $longitude])
                ->where(['user_id' => $userId])
                ->execute();
        } else {
            $orderTrackings          = $orderTrackingsTable->newEntity();
            $orderTrackings->user_id = $userId;
            $orderTrackings->lat     = $latitude;
            $orderTrackings->lng     = $longitude;
            $orderTrackingsTable->save($orderTrackings);
        }

        /* update lat lng in OrderTrackings end */
    }

    public function updateProfile()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateupdateProfileUpdateCustomer($data);
                if ($response['messageCode'] == 200) {

                    if (isset($data['image']) && !empty($data['image'])) {
                        $response = $this->validateImage($data['image']);
                    }
                    if ($response['messageCode'] == 200) {

                        $userTable = TableRegistry::get('Users');
                        $user      = $userTable->get($data['user_id']);

                        if (isset($data['image']) && !empty($data['image'])) {
                            $imagename   = $this->upload_image($data['image'], 'customer', '../webroot/img/images/');
                            $user->image = HTTP_ROOT . '/img/images/' . $imagename;
                        }

                        if ($user->region_id != $data['region_id'] || $user->area_id != $data['area_id']) {
                            $message = "" . $user->name . " has updated his area.";
                            $email   = $this->sendmail(206, $message);
                            $push    = $this->pushnotifications(206, $message);
                        }

                        $user->name      = $data['name'];
                        $user->email_id  = $data['email'];
                        $user->region_id = $data['region_id'];
                        $user->area_id   = $data['area_id'];
                        $user->houseNo   = $data['address'];
                        $user->modified  = date('Y-m-d h:i:s');

                        if ($userTable->save($user)) {

                            $this->updateLatlng($data['user_id'], $data['lat'], $data['lng']);
                            $response['messageText'] = "Customer Has beed updated Successfully";
                            $response['messageCode'] = 3121;
                            $response['successCode'] = 1;
                            $response['updatedInfo'] = $user;

                        } else {
                            $response['messageText'] = "something went wrong";
                            $response['messageCode'] = 201;
                            $response['successCode'] = 0;
                        }
                    }
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateupdateProfileUpdateCustomer($data)
    {

        $error = array();
        if (!isset($data['area_id']) || empty($data['area_id'])) {
            $error['messageText'] = "Area id can not be empty";
            $error['messageCode'] = 3112;
            $error['successCode'] = 0;
        } else if (!isset($data['region_id']) || empty($data['region_id'])) {
            $error['messageText'] = "Region can not be empty";
            $error['messageCode'] = 3113;
            $error['successCode'] = 0;
        } /*else if( ! isset( $data['image'] ) || empty( $data['image'] )){
        $error['messageText'] = "Image can not be empty";
        $error['messageCode'] = 3114;
        $error['successCode'] = 0;
        }*/else if (!isset($data['name']) || empty($data['name'])) {
            $error['messageText'] = "Name can not be empty";
            $error['messageCode'] = 3115;
            $error['successCode'] = 0;
        } else if (!isset($data['email']) || empty($data['email'])) {
            $error['messageText'] = "Email can not be empty";
            $error['messageCode'] = 3116;
            $error['successCode'] = 0;
        } else if ($this->isEmailAlreadyExist($data['email'], $data['user_id'])) {
            $error['messageText'] = "This Email already Exist";
            $error['messageCode'] = 3117;
            $error['successCode'] = 0;
        } else if (!isset($data['address']) || empty($data['address'])) {
            $error['messageText'] = "Address can not be empty";
            $error['messageCode'] = 3118;
            $error['successCode'] = 0;
        } else if (!$this->notThisAreaRegionRelate($data['area_id'], $data['region_id'])) {
            $error['messageText'] = "Area does not belongs to Region";
            $error['messageCode'] = 3119;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function notThisAreaRegionRelate($area_id, $region_id)
    {

        $areasTable = TableRegistry::get('Areas');
        $areas      = $areasTable->find()->where(['id' => $area_id, 'region_id' => $region_id])->count();
        if ($areas) {
            return true;
        }return false;

    }

    private function isEmailAlreadyExist($email, $userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id <>' => $userid, 'email_id' => $email])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateImage($base64)
    {

        $imgdata = base64_decode($base64);

        $f = finfo_open();

        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

        $mime_type = explode('/', $mime_type);
        if ($mime_type[0] == 'image') {
            $response['messageCode'] = 200;
        } else {

            $response['messageText'] = "Invalid image";
            $response['messageCode'] = 3120;
            $response['successCode'] = 0;

        }

        return $response;

    }

    private function upload_image($content = null, $imgname = null, $dest = null, $extn = null)
    {

        if ($content) {
            $dataarray = explode('base64,', $content);
            if (count($dataarray) == 1) {
                $extn = 'jpg';

                if ($extn) {
                    $img_name = $imgname . "-" . time();
                    $data     = $content;
                    $data     = str_replace(' ', '+', $data);
                    $data     = base64_decode($data);
                    file_put_contents($dest . $img_name . "." . $extn, $data);

                    chmod($dest . $img_name . "." . $extn, 0777);
                    return $img_name . "." . $extn;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

/*--status code reserve for searchResult function Start from 3131 to 3140 ---*/

    public function searchProduct()
    {
        $response = array();
        if ($this->request->is('get')) {

            $data     = $_GET;
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $product = array();

                $producTable = TableRegistry::get('Products');
                $product     = $producTable->find('all')
                    ->contain(['Units'])
                    ->where(['OR' => [
                        'Products.name LIKE '     => "%" . $data['search_param'] . "%",
                        'Products.quantity LIKE ' => "%" . $data['search_param'] . "  %",
                    ]])->toArray();
                if (count($product) > 0) {

                    $response['messageCode']  = 3141;
                    $response['successCode']  = 1;
                    $response['productLists'] = $product;

                } else {

                    $response['messageCode']  = 3141;
                    $response['successCode']  = 1;
                    $response['productLists'] = $product;
                }

            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

/*--status code reserve for update Order Info function Start from 3150 to 3170 ---*/

    public function updateOrder()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                //$response = $this->validateupdateTiming($data);
                //if ($response['messageCode'] == 200) {
                   
                    $response = $this->isValidJson($data['updateorder'], $data['user_id']);

                    if ($response['messageCode'] == 200) {
                        $result = $this->updateOrderInfo($data);
                        if ($result) {
                            $customorders      = array();
                            $customordersTable = TableRegistry::get('CustomOrders');
                            $customorders      = $customordersTable->find('all')->contain(['Products'])->where(['user_id' => $data['user_id'],'CustomOrders.status' => '1'])->toArray();
                            if (count($customorders) > 0) {
                                $message                 = "Order has been updated successfully";
                                $push                    = $this->pushnotifications($data['user_id'], $message);
                                $response['messageCode'] = 3155;
                                $response['successCode'] = 1;
                                $response['orderLists']  = $customorders;
                            } else {
                                $response['messageCode'] = 3155;
                                $response['successCode'] = 1;
                                $response['orderLists']  = $customorders;
                            }
                        }
                    }
               // }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateupdateTiming($data)
    {

        $error = array();
        if (!isset($data['delivery_schdule_id']) || empty($data['delivery_schdule_id'])) {

            $error['messageText'] = "Delivery Update Timing can not be empty";
            $error['messageCode'] = 362;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function isValidJson($jsonHead, $user_id, $delivery_schdule_id = null)
    {
        $res = json_decode(stripslashes($jsonHead), true);
        if ($res === null) {
            $error['messageText'] = "Data format not supported";
            $error['messageCode'] = 363;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
       /* if ($error['messageCode'] == 200) {
            foreach ($res as $key => $value) {
                $error = $this->validOneResponse($value, $user_id, $delivery_schdule_id);
                if (!$error['messageCode'] == 200) {
                    return $error;
                }
            }
        }*/
        return $error;
    }

    private function validOneResponse($data, $user_id, $delivery_schdule_id)
    {
        $error   = array();
        $deleted = array("yes", "no");
        if (!isset($data['is_deleted']) || empty($data['is_deleted'])) {
            $error['messageText'] = "Deleted signal values can not be empty";
            $error['messageCode'] = 3151;
            $error['successCode'] = 0;
        } else if (!isset($data['pro_id']) || empty($data['pro_id'])) {
            $error['messageText'] = "Container given can not be empty";
            $error['messageCode'] = 3152;
            $error['successCode'] = 0;
        } else if (!in_array($data['is_deleted'], $deleted)) {
            $error['messageText'] = "Invlid Delete signal values";
            $error['messageCode'] = 3153;
            $error['successCode'] = 0;
        } elseif ($this->productRelateToCustomer($data['pro_id'], $user_id, $delivery_schdule_id)) {
            $error['messageText'] = "Invalid Products Customer";
            $error['messageCode'] = 3154;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;

    }

    private function productRelateToCustomer($pro_id, $user_id, $delivery_schdule_id)
    {

        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()->where(['user_id' => $user_id, 'product_id' => $pro_id, 'delivery_schdule_id' => $delivery_schdule_id])->count();

        if ($customorders > 0) {
            return false;
        }return true;
    }

    private function updateOrderInfo($data)
    {
        $productdata = json_decode($data['updateorder'], true);
        $customordersTable = TableRegistry::get('CustomOrders');
        $productsTable     = TableRegistry::get('Products');
        foreach ($productdata as $key => $value) {
          // pr($value);die;
            if (isset($value['is_deleted']) && $value['is_deleted'] == "yes") {
                $customordersRecord = $customordersTable->find('all')
                   ->where(['id' => $value['id']])->hydrate(false)->toArray();
                if(empty($customordersRecord)){
                   return false;
                }
                $customordersTable->id = $customordersRecord[0]['id'];
                $customorder           = $customordersTable->get($customordersTable->id);
                $customordersTable->delete($customorder);
            } elseif (isset($value['is_deleted']) && $value['is_deleted'] == "no") {
                //$productsinfo = $productsTable->find('all')->where(['id' => $value['pro_id']])->toArray();
                $query        = $customordersTable->query();
                
                $result       = $query->update()
                    ->set(['quantity' => $value['pro_qty'], 'price' => $value['total_price']])
                    ->where(['id' => $value['id']])
                    ->execute();
            }
        }
        return true;
    }

    /*-----Add Subscription Start From Here --------------------------------*/

    /*Status code startv from 10001 From Here    */

    public function addSubscription()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateUserRegion($data['user_id']);
                if ($response['messageCode'] == 200) {
                    $response = $this->validateAddSubscription($data);
                    if ($response['messageCode'] == 200) {
                        $userSubscriptionTable                  = TableRegistry::get('UserSubscriptions');
                        $userSubscription                       = $userSubscriptionTable->newEntity();
                        $userSubscription->subscription_type_id = $data['subscription_type_id'];
                        if (isset($data['days']) && !empty($data['days'])) {
                            $userSubscription->days      = implode(",", $data['days']);
                            $userSubscription->startdate = $data['start_date'];
                        } else {
                            $userSubscription->startdate = $data['start_date'];
                            $userSubscription->days      = '';
                        }
                        $userSubscription->product_id = $data['product_id'];
                        $d_s_ids                      = json_decode($data['delivery_schdule_ids'], true);

                        $userSubscription->delivery_schdule_ids = implode(",", $d_s_ids);

                        if (isset($data['quantity']) && !empty($data['quantity'])) {
                            $userSubscription->quantity = $data['quantity'];
                        } else {
                            $userSubscription->quantity = $data['quantity_chield'];
                        }

                        $userSubscription->users_subscription_status_id = 1;
                        $userSubscription->user_id                      = $data['user_id'];
                        $unitname                                       = $this->getUnitNameAndId($data['product_id']);
                        $userSubscription->unit_id                      = $unitname['id'];
                        $userSubscription->unit_name                    = $unitname['name'];
                        $userSubscription->summary                      = $data['summary'];
                        $userSubscription->notes                        = $data['notes'];
                        $userSubscription->subscriptions_total_amount   = $data['subscription_price'];
                        $userSubscription->enddate                      = '2017-12-12';
                        if ($userSubscriptionTable->save($userSubscription)) {
                            $saverouteCustomer = $this->addIntoRoute($data['user_id'], $d_s_ids);
                            if ($saverouteCustomer) {
                                $calcont = $this->calculateContainers($data['quantity'], $data['product_id']);
                                if (isset($calcont['containers']) && $calcont['containers'] > 0) {
                                    $userContainerTable = TableRegistry::get('UserContainers');

                                    $userContainer = $userContainerTable->find()->select(['container_given', 'id'])->where(['user_id' => $data['user_id']])->toArray();

                                    if (isset($userContainer[0]['container_given'])) {

                                        $container_given_db = $userContainer[0]['container_given'];
                                        $container_given_db = $container_given_db + $calcont['containers'];

                                        $query = $userContainerTable->query();
                                        $query->update()
                                            ->set(['container_given' => $container_given_db])
                                            ->where(['id' => $userContainer[0]['id']])
                                            ->execute();

                                        $subContainerTable = TableRegistry::get('SubscribedContainers');
                                        $query2            = $subContainerTable->query();
                                        $query2->update()
                                            ->set(['container_count' => $container_given_db])
                                            ->where(['user_id' => $data['user_id']])
                                            ->execute();

                                    } else {
                                        $userContainer                       = $userContainerTable->newEntity();
                                        $userContainer->container_given      = $calcont['containers'];
                                        $userContainer->user_id              = $data['user_id'];
                                        $userContainer->container_collect    = 0;
                                        $userContainer->left_container_count = 0;
                                        $userContainerTable->save($userContainer);

                                        $subContainerTable             = TableRegistry::get('SubscribedContainers');
                                        $subContainer                  = $subContainerTable->newEntity();
                                        $subContainer->user_id         = $data['user_id'];
                                        $subContainer->container_count = $calcont['containers'];
                                        $subContainerTable->save($subContainer);
                                    }
                                }
                                $response['messageCode'] = 200;
                                $response['successCode'] = 1;
                                $response['messageText'] = "Subscription has been saved successfully";
                            } else {
                                $response['messageCode'] = 10021;
                                $response['successCode'] = 0;
                                $response['messageText'] = "Something Went wrong while adding Subscription";
                            }
                        } else {
                            $response['messageCode'] = 10021;
                            $response['successCode'] = 0;
                            $response['messageText'] = "Something Went wrong while adding Subscription";
                        }
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    /**
     * validate user area
     *
     * @param  array  $user_id
     * @return Array
     */
    public function validateUserRegion($user_id)
    {
        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $areaId     = $users['area_id'];
        $areasTable = TableRegistry::get('Areas');
        $area       = $areasTable->find()->where(['id' => $areaId])->select(['id'])->hydrate(false)->first();

        if (!empty($area)) {
            $error['messageCode'] = 200;
        } else {
            $error['messageText'] = "Please update your area/region before adding subscription.";
            $error['messageCode'] = 1911;
            $error['successCode'] = 0;
        }
        return $error;
    }

    public function getUnitNameAndId($pro_id)
    {
        $productTable = TableRegistry::get('Products');
        $unitId       = $productTable->find()->where(['id' => trim($pro_id)])->select(['unit_id'])->toArray();

        $uniTable = TableRegistry::get('Units');

        $unitsName = $uniTable->find()->select(['name', 'id'])->where(['id' => $unitId[0]['unit_id']])->toArray();

        $unit         = array();
        $unit['name'] = $unitsName[0]['name'];
        $unit['id']   = $unitsName[0]['id'];
        return $unit;

    }

    private function addIntoRoute($user_id, $delivery_schdule_ids)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id = $value;
            /*echo $d_s_id;die;*/
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            //pr($routeCustomer);die;
            if (count($routeCustomer) > 0) {

                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                $routeCustomers->position            = '';
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {

                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();

                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }

            }

        }
        return true;
    }

    private function validateAddSubscription($data)
    {
        /*pr($data);die;*/

        $error = array();

        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageCode'] = 10014;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select the user name";

        } else if (!isset($data['product_id']) || empty($data['product_id'])) {
            $error['messageCode'] = 10015;
            $error['successCode'] = 0;
            $error['messageText'] = "Please select the product";
        } else if (!isset($data['delivery_schdule_ids']) || empty($data['delivery_schdule_ids'])) {
            $error['messageCode'] = 10016;
            $error['successCode'] = 0;
            $error['messageText'] = "Please select the timing";
        } else if (

            (!isset($data['quantity']) || empty($data['quantity']))
            &&
            (!isset($data['quantity_chield']) || empty($data['quantity_chield']))

        ) {
            $error['messageCode'] = 10017;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select the quantity";
        } else if ($data['quantity'] < 1) {
            $error['messageCode'] = 10021;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select valid quantity";
        } else if (!isset($data['start_date']) || empty($data['start_date'])) {
            $error['messageCode'] = 10018;
            $error['successCode'] = 0;
            $error['messageText'] = "Please enter the start date";
        } else if (!isset($data['subscription_type_id']) || empty($data['subscription_type_id'])) {
            $error['messageCode'] = 10019;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease selct the subscription type";
        } else if ($this->checkSubscription($data['user_id'], $data['product_id'], $data['delivery_schdule_ids'], $data['subscription_type_id'])) {
            $error['messageCode'] = 10020;
            $error['successCode'] = 0;
            $error['messageText'] = "This subscription already taken by this customer";
        }

        if (count($error) > 0) {
            //$error['messageCode'] = 201;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;

    }

    private function checkSubscription($userid, $productid, $delivery_schdule_ids = null, $subscription_type_id = null)
    {
        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $userSubscription      = $userSubscriptionTable->find('all')->where([
            'user_id'    => $userid,
            'product_id' => $productid,
            'users_subscription_status_id <>' => 4,
            //'delivery_schdule_ids' => trim($delivery_schdule_ids),
            //'subscription_type_id'=>$subscription_type_id
        ])->toArray();
        if (count($userSubscription) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getCategoriesAndSubscriptionTypes()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                // $categoryTable = TableRegistry::get('Categories');
                // $categories = $categoryTable->find('all')->select(['id','name'])->where(['Categories.status'=>1])->toArray();
                $subtype           = TableRegistry::get('SubscriptionTypes');
                $subscriptionTypes = $subtype->find('all')->select(['id', 'subscription_type_name'])->toArray();
                /* if( isset( $categories ) && count( $categories ) > 0 ) {
                $response['categoryList'] = $categories;
                }else{
                $response['categoryList'] = [];
                }*/

                if (isset($subscriptionTypes) && count($subscriptionTypes) > 0) {

                    $response['subscriptionTypesList'] = $subscriptionTypes;

                } else {
                    $response['subscriptionTypesList'] = [];
                }

                if (isset($response['subscriptionTypesList']) && count($response['subscriptionTypesList']) > 0) {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;

                } else {

                    $response['messageCode'] = 10001;
                    $response['successCode'] = 1;
                    $response['messageText'] = "Please Add Category and SubscriptionTypes First";

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function checkCategoryDeliverThisCustomer()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validatecategory($data);
                if ($response['messageCode'] == 200) {

                    $getRegionAreaTable = TableRegistry::get('Users');
                    $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $data['user_id']])->toArray();

                    $region                          = $getRegionArea[0]['region_id'];
                    $area                            = $getRegionArea[0]['area_id'];
                    $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
                    $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name', 'DeliverySchdules.start_time', 'DeliverySchdules.end_time'])->where(['CategoryDeliverySchdules.category_id' => $data['category_id'], 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

                    if (count($category_delivery) > 0) {
                        $tempOutSide = array();
                        foreach ($category_delivery as $key => $value) {

                            $temp               = array();
                            $temp['d_s_id']     = $value['delivery_schdule_id'];
                            $temp['name']       = $value['delivery_schdule']['name'];
                            $temp['start_time'] = $value['delivery_schdule']['start_time'];
                            $temp['end_time']   = $value['delivery_schdule']['end_time'];
                            $tempOutSide[]      = $temp;

                        }

                        //check driver route for this schedule
                        $finalSchedule      = array();
                        $routeCustomerTable = TableRegistry::get('RouteCustomers');
                        foreach ($tempOutSide as $key => $value) {
                            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $value['d_s_id'], 'region_id' => $region, 'area_id' => $area])->hydrate(false)->toArray();
                            if (!empty($routeCustomer)) {
                                $finalSchedule[] = $value;
                            }
                        }

                        if (empty($finalSchedule)) {
                            $response['messageCode'] = 1952;
                            $response['successCode'] = 1;
                            $response['messageText'] = 'No schedule found for user route.';
                        } else {
                            $response['messageCode']                   = 200;
                            $response['successCode']                   = 1;
                            $response['provideDeliveryTimingCustomer'] = $finalSchedule;
                        }

                        /* if( isset( $product ) && count( $product ) > 0 ){
                    $response['productLists'] = $product;
                    }else{
                    $response['productLists'] = [];
                    }*/

                    } else {

                        $response['messageCode'] = 10002;
                        $response['successCode'] = 1;
                        $response['messageText'] = "We are not deliver any of the time to this customer location";
                    }

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validatecategory($data)
    {

        $error = array();
        if (!isset($data['category_id']) or empty($data['category_id'])) {
            $error['messageCode'] = 10003;
            $error['successCode'] = 1;
            $error['messageText'] = "Category should not be empty";
        } else if (!is_numeric($data['category_id'])) {
            $error['messageCode'] = 10004;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid category";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    public function getProductUnitNameAndChieldren()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProduct($data);

                if ($response['messageCode'] == 200) {

                    $productTable = TableRegistry::get('Products');
                    $unitId       = $productTable->find()->where(['id' => trim($data['product_id'])])->select(['unit_id'])->toArray();

                    $uniTable = TableRegistry::get('Units');

                    $unitsName = $uniTable->find()->select(['name'])->where(['id' => $unitId[0]['unit_id']])->toArray();
                    $name      = str_replace('"', '', $unitsName[0]['name']);

                    $childProduct     = $productTable->find('all')->where(['id' => trim($data['product_id'])])->contain(['ProductChildren', 'ProductChildren.Units'])->toArray();
                    $productChieldres = array();

                    if (count($childProduct) > 0) {

                        foreach ($childProduct[0]['product_children'] as $key => $value) {

                            $temp               = array();
                            $temp['price']      = $value['price'];
                            $temp['unit']       = $value['unit']['name'];
                            $temp['quantity']   = $value['quantity'];
                            $temp['p_c_i']      = $value['id'];
                            $productChieldres[] = $temp;
                        }
                        $response['messageCode']                  = 200;
                        $response['successCode']                  = 1;
                        $response['childproduct']                 = $productChieldres;
                        $response['parentproduct_price_per_unit'] = $childProduct[0]['price_per_unit'];
                        $response['productname']                  = $childProduct[0]['name'];
                        $response['unitname']                     = strtoupper($name);
                    } else {
                        $response['messageCode']  = 200;
                        $response['successCode']  = 1;
                        $response['childproduct'] = [];
                        $response['unitname']     = strtoupper($name);
                    }
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateProduct($data)
    {

        $error = array();
        if (!isset($data['product_id']) or empty($data['product_id'])) {
            $error['messageCode'] = 10005;
            $error['successCode'] = 1;
            $error['messageText'] = "Product Id should not be empty";
        } else if (!is_numeric($data['product_id'])) {
            $error['messageCode'] = 10006;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product Id";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    public function getProductChildrenPrice()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProductchieldren($data);

                if ($response['messageCode'] == 200) {

                    $productTable                  = TableRegistry::get('ProductChildren');
                    $product                       = $productTable->find()->where(['id' => $data['product_chield_id']])->select(['price'])->toArray();
                    $price                         = $product[0]['price'];
                    $response['messageCode']       = 200;
                    $response['successCode']       = 1;
                    $response['subscriptionprice'] = $price;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateProductchieldren($data)
    {

        $error = array();
        if (!isset($data['product_chield_id']) or empty($data['product_chield_id'])) {
            $error['messageCode'] = 10007;
            $error['successCode'] = 1;
            $error['messageText'] = "Product chieldren Id should not be empty";
        } else if (!is_numeric($data['product_chield_id'])) {
            $error['messageCode'] = 10008;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product chieldren Id";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    public function getProductPriceManually()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProductManually($data);

                if ($response['messageCode'] == 200) {

                    $productTable = TableRegistry::get('Products');
                    $product      = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'quantity'])->toArray();

                    $leftQty = $product[0]['quantity'];
                    if ($leftQty >= $data['quantity']) {
                        $price                         = $product[0]['price_per_unit'];
                        $totalPrice                    = ($price * $data['quantity']);
                        $response['messageCode']       = 200;
                        $response['successCode']       = 1;
                        $response['subscriptionprice'] = $totalPrice;
                    } else {

                        $response['statuscode']  = 10013;
                        $response['messageText'] = "Entered quantity not available";
                        $response['successCode'] = 1;
                    }

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateProductManually($data)
    {

        $error = array();
        if (!isset($data['product_id']) or empty($data['product_id'])) {
            $error['messageCode'] = 10009;
            $error['successCode'] = 1;
            $error['messageText'] = "Product Id should not be empty";
        } else if (!is_numeric($data['product_id'])) {
            $error['messageCode'] = 10010;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product Id";
        } else if (!isset($data['quantity']) or empty($data['quantity'])) {
            $error['messageCode'] = 10011;
            $error['successCode'] = 1;
            $error['messageText'] = "Product quantity should not be empty";
        } else if (!is_numeric($data['quantity'])) {
            $error['messageCode'] = 10012;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid quantity";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    /*-------Add Subscription End WIll Here----------------------------------*/

    /*-----Return all Subscription List Start From Here --------------------------------*/

    /*Status code startv from 11001 From Here    */

    public function activeDeActiveSubscriptionList()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $usersSubscriptionTable = TableRegistry::get('UserSubscriptions');
                $usersSubscription = $usersSubscriptionTable->find('all')->contain(['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses'])->where(['UserSubscriptions.user_id' => $data['user_id'],'users_subscription_status_id <>' =>4])->hydrate(false)->toArray();
                //pr($usersSubscription);die;
                if (count($usersSubscription) > 0) {
                    $allSubscription               = $this->formatAllSubscription($usersSubscription);
                    $response['statuscode']        = 200;
                    $response['successCode']       = 1;
                    $response['subscriptionsList'] = $allSubscription;
                } else {
                    $response['statuscode']        = 11001;
                    $response['messageText']       = "Not Found Any Subscription For This Customer";
                    $response['successCode']       = 1;
                    $response['subscriptionsList'] = [];
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function formatAllSubscription($data)
    {
        $finalResponse = array();
        foreach ($data as $key => $value) {
            $d_s_is               = explode(',', $value['delivery_schdule_ids']);
            $deliverySchduleTable = TableRegistry::get('DeliverySchdules');
            $d_s_name = array();
            foreach ($d_s_is as $k => $v) {
                $temp                            = array();
                $deliverySchdule                 = $deliverySchduleTable->find()->select(['name', 'start_time', 'end_time'])->where(['id' => $v])->first();
                $temp['ds_id']                   = $v;
                $temp['name']                    = $deliverySchdule['name'];
                $temp['delivery_timing_between'] = $deliverySchdule['start_time'] . '  To ' . $deliverySchdule['end_time'];
                $d_s_name[]                      = $temp;
            }
            $temp                    = array();
            $temp['subscriptionId']  = $value['id'];
            $temp['subscriptionTypeId']  = $value['subscription_type_id'];
            $temp['productName']     = $value['product']['name'];
            $temp['category_id']     = $value['product']['category_id'];
            $temp['productId']       = $value['product']['id'];
            $temp['image']       = @$value['product']['image'];
            $temp['price_per_unit']  = $value['product']['price_per_unit'];
            if($value['startdate']){
                    $temp['startdate']  = $value['startdate']->i18nFormat('dd-MM-yyyy HH:mm:ss');
                } else {
                    $temp['SubscriptionsPausedSince'] = $value['startdate'];
                }
            
            $temp['productQuantity'] = $value['quantity'];
            if ($value['users_subscription_status']['name'] == 'active') {
                $temp['Subscriptionstatus'] = $value['users_subscription_status']['name'];
            } else if ($value['users_subscription_status']['name'] == 'paused') {
                $temp['Subscriptionstatus']       = $value['users_subscription_status']['name'];
                if($value['paused_since']){
                    $temp['SubscriptionsPausedSince'] = $value['paused_since']->i18nFormat('dd-MM-yyyy HH:mm:ss');
                } else {
                    $temp['SubscriptionsPausedSince'] = $value['paused_since'];
                }
                
            } else if ($value['users_subscription_status']['name'] == 'cancel') {
                $temp['Subscriptionstatus'] = $value['users_subscription_status']['name'];
            }
            $temp['unitName']               = $value['unit_name'];
            $temp['subscriptionTotalPrice'] = $value['subscriptions_total_amount'];
            $temp['repeatOn']               = $value['subscription_type']['subscription_type_name'];
            $temp['deliverySchduleTimig']   = $d_s_name;
            $finalResponse[]                = $temp;

        }
        return $finalResponse;
    }

    /*--Return all Subscription List Start From Here---*/

    /*-----Change Subscription status START here --------------------------------*/

    /*Status code startv from 12001 From Here    */

    public function changeSubscriptionStatus()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateStatusChange($data);
                if ($response['messageCode'] == 200) {

                    $updatedFlag = $this->alreadyHaveThatStatus($data['subscription_id'], trim($data['status']));
                    if ($updatedFlag) {

                        $response['statuscode']  = 200;
                        $response['successCode'] = 1;
                        $response['messageText'] = 'subscription has been updated successfully';

                    } else {

                        $newUpdatedFlag = $this->updateSubscriptionStatus($data);
                        if ($newUpdatedFlag['messageCode'] == 200) {

                            $response['statuscode']  = 200;
                            $response['successCode'] = 1;
                            $response['messageText'] = 'subscription has been updated successfully';

                        } else {

                            $response['statuscode']  = 12005;
                            $response['successCode'] = 0;
                            $response['messageText'] = 'Something went wrong while status change';

                        }

                    }

                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateStatusChange($data)
    {

        $subscriptionStatus = ['active', 'cancel', 'paused'];
        $response           = array();
        if (!isset($data['status']) || empty($data['status'])) {

            $response['messageCode'] = 12001;
            $response['successCode'] = 0;
            $response['messageText'] = "Status value should not be empty";

        } else if (!in_array($data['status'], $subscriptionStatus)) {

            $response['messageCode'] = 12002;
            $response['successCode'] = 0;
            $response['messageText'] = "Wrong Status value. status value should be one out of ( 'active','cancel','paused' ) ";

        } else if (!isset($data['subscription_id']) || empty($data['subscription_id'])) {

            $response['messageCode'] = 12003;
            $response['successCode'] = 0;
            $response['messageText'] = "subscription id should not be empty";

        } else if (!is_numeric($data['subscription_id'])) {

            $response['messageCode'] = 12004;
            $response['successCode'] = 0;
            $response['messageText'] = "Invalid Subscription id";

        } else if ($this->subscriptionBelongsToCustomer($data['user_id'], $data['subscription_id'])) {

            $response['messageCode'] = 12005;
            $response['successCode'] = 0;
            $response['messageText'] = "Subscriptio does not associated with this customer";

        } else {
            $response['messageCode'] = 200;
        }
        return $response;

    }

    private function subscriptionBelongsToCustomer($user_id, $subscription_id)
    {

        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $userSubscription      = $userSubscriptionTable->find()->where(['user_id' => $user_id, 'id' => $subscription_id])->count();
        if (!$userSubscription) {
            return true;
        }return false;

    }

    private function alreadyHaveThatStatus($subscription_id, $status)
    {

        $userSubscriptionStatusTable = TableRegistry::get('UsersSubscriptionStatuses');
        $userSubscriptionTable       = TableRegistry::get('UserSubscriptions');
        $userSubscriptionStatus      = $userSubscriptionStatusTable->find()->select(['id'])->where(['name' => $status])->first();
        $id                          = $userSubscriptionStatus['id'];
        $flagDbStatus                = $userSubscriptionTable->find()->where(['id' => $subscription_id, 'users_subscription_status_id' => $id])->count();
        if ($flagDbStatus) {
            return true;
        }return false;

    }

    private function updateSubscriptionStatus($data)
    {

        $usersSubscriptionStatusesTable                    = TableRegistry::get('UsersSubscriptionStatuses');
        $usersSubscriptionTable                            = TableRegistry::get('UserSubscriptions');
        $user_subscription_active_deactive_histories_table = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
        $subscriptionId                                    = $data['subscription_id'];
        $newUpdatedFlag                                    = array();

        if (trim($data['status']) == 'active') {
            $usersSubscriptionStatusesOBJ                    = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'active'])->first();
            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;
            $usersSubscription->paused_since                 = '';
            if ($usersSubscriptionTable->save($usersSubscription)) {
                $user_subscription_active_deactive_histories_table = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $query                                             = $user_subscription_active_deactive_histories_table->query();
                $result                                            = $query->update()
                    ->set(['play_date' => date('Y-m-d h:i:s')])
                    ->where(['user_subscription_id' => $subscriptionId, 'play_date IS NULL'])
                    ->execute();
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        } else if (trim($data['status']) == 'paused') {

            $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'paused'])->first();

            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;

            $usersSubscription->paused_since = date('Y-m-d h:i:s');

            if ($usersSubscriptionTable->save($usersSubscription)) {

                $user_subscription_active_deactive_histories_table                         = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $user_subscription_active_deactive_histories                               = $user_subscription_active_deactive_histories_table->newEntity();
                $user_subscription_active_deactive_histories->user_subscription_id         = $subscriptionId;
                $user_subscription_active_deactive_histories->paused_date                  = date('Y-m-d h:i:s');
                $user_subscription_active_deactive_histories->users_subscription_status_id = $usersSubscriptionStatusesId;
                $user_subscription_active_deactive_histories_table->save($user_subscription_active_deactive_histories);
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        } else if (trim($data['status']) == 'cancel') {

            /* $userSubscription = $usersSubscriptionTable->get($subscriptionId);

            if ($usersSubscriptionTable->delete($userSubscription)) {
            $newUpdatedFlag['messageCode'] = 200;

            }else{
            $newUpdatedFlag['messageCode'] = 201;
            }*/

            $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'cancel'])->first();

            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;

            $usersSubscription->paused_since = date('Y-m-d h:i:s');

            if ($usersSubscriptionTable->save($usersSubscription)) {

                $user_subscription_active_deactive_histories_table                         = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $user_subscription_active_deactive_histories                               = $user_subscription_active_deactive_histories_table->newEntity();
                $user_subscription_active_deactive_histories->user_subscription_id         = $subscriptionId;
                $user_subscription_active_deactive_histories->paused_date                  = date('Y-m-d h:i:s');
                $user_subscription_active_deactive_histories->users_subscription_status_id = $usersSubscriptionStatusesId;
                $user_subscription_active_deactive_histories_table->save($user_subscription_active_deactive_histories);
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        }

        return $newUpdatedFlag;

    }

/*----- Change Subscription status END here --------------------------------*/

    /*-----Add to Next Deliver START here --------------------------------*/

    /*Status code startv from 13001 From Here    */

    public function addToNextDelivery()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateAddNextDelivery($data);
                if ($response['messageCode'] == 200) {
                    $saveOrder = $this->saveOrder($data);
                    if ($saveOrder['messageCode'] == 200) {
                        $message = "Order has been saved successfully";
                        $push    = $this->pushnotifications($data['user_id'], $message);
                        $response['statuscode']  = 13014;
                        $response['successCode'] = 1;
                        $response['messageText'] = 'Product has been added successfully';
                    } else if ($saveOrder['messageCode'] == 3306) {
                        $response['statuscode']  = 13015;
                        $response['successCode'] = 0;
                        $response['messageText'] = 'This Order already made by this customer for next delivery';
                    } else {
                        $response['statuscode']  = 13016;
                        $response['successCode'] = 0;
                        $response['messageText'] = 'Something went wrong while save order';
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function saveOrder($data)
    {
        $res               = array();
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()
            ->where(['user_id' => $data['user_id'], 'product_id' => $data['product_id'], 'status'=>0])->count();
        if ($customorders) {
            $res['messageCode'] = 3306;
        } else {
            $customorders             = $customordersTable->newEntity();
            $customorders->user_id    = $data['user_id'];
            $customorders->product_id = $data['product_id'];
            $customorders->quantity   = $data['quantity'];
            if (isset($data['is_chield'])) {
                $productChildrenTable = TableRegistry::get('ProductChildren');
                $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id'])->where(['product_id' => $data['product_id'], 'quantity' => $data['quantity']])->first();
                //print_r($productChildren); die;
                if ($productChildren == "") {
                    $productTable          = TableRegistry::get('Products');
                    $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                    $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                    $customorders->unit_id = $products['unit_id'];
                } else {
                    $customorders->price   = $productChildren['price'];
                    $customorders->unit_id = $productChildren['unit_id'];
                }
            } else {
                $productTable          = TableRegistry::get('Products');
                $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                $customorders->unit_id = $productChildren['unit_id'];
            }

            $customorders->status              = 0;
           // $customorders->delivery_schdule_id = $data['delivery_schdule_id'];
            $customorders->created             = date('Y-m-d');
            $customorders->modified            = date('Y-m-d h:i:s');
            if ($customordersTable->save($customorders)) {
                $res['messageCode'] = 200;
            } else {
                $res['messageCode'] = 201;
            }

        }
        return $res;
    }

    private function validateAddNextDelivery($data)
    {
        $response = array();
       /* if (!isset($data['delivery_schdule_id']) || empty($data['delivery_schdule_id'])) {
            $response['messageCode'] = 13001;
            $response['successCode'] = 0;
            $response['messageText'] = "Delivery Schdule Id value should not be empty";

        } else */
        if (!isset($data['product_id']) || empty($data['product_id'])) {
            $response['messageCode'] = 13002;
            $response['successCode'] = 0;
            $response['messageText'] = "Product id value should not be empty";

        } else if (!isset($data['unit_name']) || empty($data['unit_name'])) {

            $response['messageCode'] = 13003;
            $response['successCode'] = 0;
            $response['messageText'] = "Unit Name  should not be empty";

        } /*else if (!isset($data['summary']) || empty($data['summary'])) {

            $response['messageCode'] = 13004;
            $response['successCode'] = 0;
            $response['messageText'] = "Summary should not be empty";

        } */ else if (!isset($data['order_price']) || empty($data['order_price'])) {

            $response['messageCode'] = 13005;
            $response['successCode'] = 0;
            $response['messageText'] = "Order Price should not be empty";

        } /*else if (!isset($data['date']) || empty($data['date'])) {

            $response['messageCode'] = 13006;
            $response['successCode'] = 0;
            $response['messageText'] = "Date should not be empty";

        }*/ else if (!isset($data['quantity']) || empty($data['quantity'])) {

            $response['messageCode'] = 13007;
            $response['successCode'] = 0;
            $response['messageText'] = "Quantity should not be empty";

        } else if (!$this->existProduct(trim($data['product_id']))) {

            $response['messageCode'] = 13010;
            $response['successCode'] = 0;
            $response['messageText'] = "Product Id does not exist";

        } else if (!$this->associatedWithProduct($data['product_id'], $data['quantity'], $data['is_chield'])) {
            $response['messageCode'] = 13008;
            $response['successCode'] = 0;
            $response['messageText'] = "Child Value Must Be 1 And must be exist with Quantity of that Product in chieldren";

        } /*else if (!$this->existDst(trim($data['delivery_schdule_id']))) {

            $response['messageCode'] = 13009;
            $response['successCode'] = 0;
            $response['messageText'] = "Delivery Schdule Id does not exist";

        }*/ else if (!$this->existUnit(trim($data['unit_name']))) {

            $response['messageCode'] = 13011;
            $response['successCode'] = 0;
            $response['messageText'] = "Unit Name does not exist";

        }/* else if (!$this->isValidDate(trim($data['date']))) {

            $response['messageCode'] = 13012;
            $response['successCode'] = 0;
            $response['messageText'] = "Date Format Should be ( YYY-MM-dd )";

        }*/ else if (!is_numeric($data['quantity']) || empty($data['quantity'])) {

            $response['messageCode'] = 13013;
            $response['successCode'] = 0;
            $response['messageText'] = "Invlid Quantity Value";

        } else if(!$this->getUserSubs($data['user_id'])){
          $response['messageCode'] = 1972;
          $response['successCode'] = 0;
          $response['messageText'] = "You have not subscribed any product yet.";
        }else {
            $response['messageCode'] = 200;
        }
        return $response;

    }

    public function getUserSubs($userid) {
      $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
      $usersub = $userSubscriptionsTable->find('all')
         ->where(['user_id' => $userid,'users_subscription_status_id' => 1])->hydrate(false)->toArray();
      if(count($usersub)>0){
         return true;
      } else {
         return false;
      }
    }

    private function existDst($id)
    {

        $deliverySchduleTable = TableRegistry::get('DeliverySchdules');
        $deliverySchdule      = $deliverySchduleTable->find()->where(['id' => $id])->count();
        if ($deliverySchdule) {
            return true;
        }return false;

    }

    private function existProduct($id)
    {

        $productTable = TableRegistry::get('Products');
        $products     = $productTable->find()->where(['id' => $id])->count();
        if ($products) {
            return true;
        }return false;

    }

    private function existUnit($name)
    {

        $unitTable = TableRegistry::get('Units');
        $unit      = $unitTable->find()->where(['name' => strtolower($name)])->count();
        if ($unit) {
            return true;
        }return false;

    }

    private function isValidDate($date)
    {

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return true;
        } else {
            return false;
        }
    }
    private function associatedWithProduct($pro_id, $qty, $ischield_set)
    {

        if (isset($ischield_set) && $ischield_set == 1) {
            $productChildrenTable = TableRegistry::get('ProductChildren');
            $productsTable        = TableRegistry::get('Products');
            //$productChildren = $productChildrenTable->find()->where(['product_id'=>$pro_id,'quantity'=>$qty])->count();
            $productChildren = $productChildrenTable->find()->where(['product_id' => $pro_id])->count();
            $products        = $productsTable->find()->where(['id' => $pro_id])->count();
            if ($productChildren || $products) {
                return true;
            }return false;
        } else if (isset($ischield_set) && $ischield_set != 1) {
            return false;
        } else {
            return true;
        }
    }

    /*-----CALENDRA EVENT START FROM HERE...... here --------------------------------*/

    /*Status code start from 14001 From Here    */

    public function getCalendraEvent()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();

            $date     = date('Y-m-d');
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                if (!$this->isValidDate(trim($data['date']))) {
                    $response['messageCode'] = 14012;
                    $response['successCode'] = 0;
                    $response['messageText'] = "Date Format Should be ( YYYY-MM-dd )";
                } else {
                    $staus = $this->checkDateDeliveryOptions($data);
                    if (isset($staus) && count($staus) > 0) {
                        $response['statuscode']  = 14014;
                        $response['successCode'] = 1;
                        $response['data']        = $staus;
                    } else {
                        $response['statuscode']  = 14014;
                        $response['successCode'] = 1;
                        $response['data']        = [];
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function checkDateDeliveryOptions($data)
    {
        $nextDelivery = $this->getBalanceAndDeliveryInfo($data,'current');
        // $nextDeliveryData = array();

        $Deliveryondate = $this->getBalanceAndDeliveryInfo($data,'future');
         
         $today = date('Y-m-d');
         $today_delivered_temp = array();
         $delivered = array();
         $rejected = array();
         $cuorders = array();
         $customorders = array();
         $cuordersid = array();
         if(strtotime(@$data['date']) >= strtotime($today)){
            if(!empty($Deliveryondate)){
                foreach ($Deliveryondate as $key => $value) {
                   /*if(strtotime(@$data['date']) == strtotime($today)){
                        $transtable = TableRegistry::get('Transactions');
                        $trans      = $transtable->find('all')->where(['Transactions.transaction_amount_type' => 'Dr', 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $key, 'Transactions.user_id' => $data['user_id']])->hydrate(false)->toArray();
                        if(!empty($trans)){
                            foreach ($trans as $tk => $tv) {
                                if($tv['rejected']== 0 && $tv['status'] == 1){
                                    $subid = explode(',', $tv['user_subscription_ids']) ;
                                    foreach ($subid as $sid) {
                                        if(!in_array($sid, $delivered)){
                                            $delivered[] = $sid;
                                        }
                                    }
                                } else if($tv['rejected']== 1){
                                    $subid = explode(',', $tv['user_subscription_ids']) ;
                                    foreach ($subid as $sid) {
                                        if(!in_array($sid, $rejected)){
                                            $rejected[] = $sid;
                                        }
                                    }
                                }

                                if($tv['custom_order_ids']){
                                    $coid = explode(',', $tv['custom_order_ids']) ;
                                    foreach ($coid as $id) {
                                        if(!in_array($id, $cuorders)){
                                            $cuorders[] = $id;
                                        }
                                    }
                                }
                            }
                        }
                        if(!empty($cuorders)) {
                            $customordersTable = TableRegistry::get('CustomOrders');
                            $orders      = $customordersTable->find('all')->contain(['Products', 'Units','DeliverySchdules'])->where(['CustomOrders.id IN' => $cuorders,'delivery_schdule_id' => $key,'user_id' => $data['user_id']])->hydrate(false)->toArray();
                            
                            if(!empty($orders)){
                                foreach ($orders as $ok => $ov) {
                                    //pr($customorders);die;
                                    $temp                    = array();
                                    $temp['quantity']        = $ov['quantity'];
                                    $temp['custom_id'] = $ov['id'];
                                    $temp['productName']     = @$ov['product']['name'];
                                    $temp['image']     = @$ov['product']['image'];
                                    $temp['unit']            = @$ov['unit']['name'];
                                    $temp['amount']          = $ov['price'];
                                    if($ov['rejected'] == 0){
                                        $temp['status']          = 'DELIVERED';
                                    } else {
                                        $temp['status']          = 'REJECTED';
                                    }
                                    $temp['timing']          = @$ov['delivery_schdule']['name'];
                                    $temp['between']          = @$ov['delivery_schdule']['start_time'].'-'.@$ov['delivery_schdule']['end_time'];
                                    $temp['deliverdate']          = $data['date'];
                                    $temp['sid']             = $key;
                                    $customorders[] = $temp;
                                }
                            }
                        } 
                    }*/
                    if(!empty($nextDelivery) && isset($nextDelivery['orderItems']) && !empty($nextDelivery['orderItems'])){
                        if(isset($nextDelivery['willDeliver']) && (strtotime($nextDelivery['willDeliver']) == strtotime($data['date'])) 
                            && isset($nextDelivery['deliver_schdule_id']) && $nextDelivery['deliver_schdule_id'] == $key){
                         
                            foreach ($nextDelivery['orderItems'] as $nk => $nv) {
                               $cuordersid[] = $nv['id'];
                            }

                            if(!empty($cuordersid)) {
                                $customordersTable = TableRegistry::get('CustomOrders');
                                $orders      = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.id IN' => $cuordersid])->hydrate(false)->toArray();
                                //pr($orders);die;
                                if(!empty($orders)){
                                    foreach ($orders as $ok => $ov) {
                                        //pr($customorders);die;
                                        $temp                    = array();
                                        $temp['quantity']        = $ov['quantity'];
                                        $temp['custom_id'] = $ov['id'];
                                        $temp['productName']     = @$ov['product']['name'];
                                        $temp['image']     = @$ov['product']['image'];
                                        $temp['unit']            = @$ov['unit']['name'];
                                        $temp['amount']          = $ov['price'];
                                        $temp['status']          = 'UPCOMING';
                                       $temp['timing']          = @$nextDelivery['subscriptionItems'][0]['timeToBeDeliver'];
                                        $temp['between']          = @$nextDelivery['subscriptionItems'][0]['between'];
                                        $temp['deliverdate']          = $data['date'];
                                        $temp['sid']             = $key;
                                        $customorders[] = $temp;
                                    }
                                }
                            }

                        }
                    }
                   if(isset($value['itemss'][0]['subscriptionInfo']) && !empty($value['itemss'][0]['subscriptionInfo'])){
                        foreach ($value['itemss'][0]['subscriptionInfo'] as $k => $v) {
                            $temp                    = array();
                            $temp['quantity']        = $v['quantity'];
                            $temp['subscription_id'] = $v['subscription_id'];
                            $temp['productName']     = $v['name'];
                            $temp['image']     = @$v['image'];
                            $temp['unit']            = $v['unit'];
                            $temp['amount']          = $v['price'];
                            if(in_array($v['subscription_id'], $delivered)){
                                $temp['status']          = 'DELIVERED';
                            } else if(in_array($v['subscription_id'], $rejected)) {
                                $temp['status']          = 'REJECTED';
                            } else {
                                $temp['status']          = 'UPCOMING';
                            }
                            $temp['timing']          = $v['timeToBeDeliver'];
                            $temp['between']          = $v['between'];
                            $temp['deliverdate']          = $data['date'];
                            $temp['sid']             = $key;
                            $today_delivered_temp[]  = $temp;
                        }
                   }
                }
                $response['subscriptions'] = $today_delivered_temp;
                $response['customorders'] = $customorders;
                return $response;
            } else {
                return array();
            }
         } else {
            return array();
         }
    }

    private function checkifcustomordersdelivered($user_id, $delivery_schdule_id, $date)
    {

        $todaydate         = date('Y-m-d');
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.user_id' => $user_id]);
        
        $custom_orders = array();
        foreach ($customorders as $key => $value) {
            $temp = array();

            $temp['quantity'] = $value['quantity'];

            if ($value['status'] == 1 && $todaydate >= $date) {

                $temp['order_status'] = 'DELIVERED';

            } else if ($value['status'] == 0 && $todaydate > $date) {
                $temp['order_status'] = 'NOTDELIVERED';
            } else if ($value['status'] == 0 && $todaydate <= $date) {
                $temp['order_status'] = 'UPCOMING';
            }

            $temp['custom_order_id'] = $value['id'];
            $temp['productName']     = $value['product']['name'];
            $temp['unit']            = $value['unit']['name'];
            $temp['amount']          = $value['price'];
            $custom_orders[]         = $temp;

        }

        return $custom_orders;

    }

    private function checkifcustomordersnotdelivered($user_id, $delivery_schdule_id, $date)
    {

        $todaydate         = date('Y-m-d');
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.user_id' => $user_id, 'CustomOrders.created' => $date, 'CustomOrders.delivery_schdule_id' => $delivery_schdule_id, 'CustomOrders.status' => 0]);

        $custom_orders = array();
        foreach ($customorders as $key => $value) {
            $temp             = array();
            $temp['quantity'] = $value['quantity'];

            if ($value['status'] == 1 && $todaydate > $date) {

                $temp['order_status'] = 'DELIVERED';

            } else if ($value['status'] == 0 && $todaydate > $date) {
                $temp['order_status'] = 'NOTDELIVERED';
            } else if ($value['status'] == 0 && $todaydate <= $date) {
                $temp['order_status'] = 'UPCOMING';
            }

            $temp['custom_order_id'] = $value['id'];
            $temp['custom_order_id'] = $value['id'];
            $temp['productName']     = $value['product']['name'];
            $temp['unit']            = $value['unit']['name'];
            $temp['amount']          = $value['price'];
            $custom_orders[]         = $temp;
        }

        return $custom_orders;

    }

    private function checkFromRouteCustomer($user_id, $d_s_id, $date, $subid=null)
    {
        date_default_timezone_set('Asia/Kolkata');
        $routeCustomerTable   = TableRegistry::get('RouteCustomers');
        $rejected_ordersTable = TableRegistry::get('RejectedOrders');
        $scheduleTable = TableRegistry::get('DeliverySchdules');
        $status = array();
        /*    202   for delivered    and 200  for not delivered---*/
        
        $transtable = TableRegistry::get('Transactions');
        $trans = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type'=>'Dr','Transactions.created' => $date])->hydrate(false)->toArray();
        $subids = array();
        if(!empty($trans)){
          $subids = explode(',', $trans[0] ['user_subscription_ids']);
        }
//pr($subids);die;

       /* $routeCustomer = $routeCustomerTable->find()->contain(['DeliverySchdules'])->where(['user_id' => $user_id, 'delivery_schdule_id' => $d_s_id])->first();*/

        $routeCustomer = $scheduleTable->find()->where(['id' => $d_s_id])->hydrate(false)->first();
        if($routeCustomer){
          $sname = $routeCustomer['name'];
        }else{
          $sname = '';
        }
        
        $datetoday = date('Y-m-d');
        //$route_date = $routeCustomer['date'];
        if (in_array($subid, $subids)) {
            $status['statuscode'] = 202;
            $status['time']       = $sname;
            return $status;
        }
        // pr($routeCustomer);die;
        /*$rejected_orders = $rejected_ordersTable->find()->where(['user_id' => $user_id, 'date' => $date, 'delivery_schdule_id' => $d_s_id])->count();
         if ($rejected_orders == 0) {
              $status['statuscode'] = 202;
              $status['time']       = $date . ' - ' . $sname;
              return $status;
          } else {
              $status['statuscode'] = 201;
              $status['time']       = $date;
              return $status;
          }*/
          $status['statuscode'] = 200;
          $status['time']       = $sname;
         /*else {
            $status['statuscode'] = 200;
            $status['time']       = $sname;
        }*/

        /*} else if ($datetoday > $date) {
            $rejected_orders = $rejected_ordersTable->find()->where(['user_id' => $user_id, 'date' => $date, 'delivery_schdule_id' => $d_s_id])->count();
            if ($rejected_orders == 0) {
                $status['statuscode'] = 202;
                $status['time']       = $date . ' - ' . $routeCustomer['delivery_schdule']['name'];
            } else {
                $status['statuscode'] = 201;
                $status['time']       = $date;
            }
        } else if ($datetoday < $date) {
            $status['statuscode'] = 200;
            $status['time']       = $date . ' - ' . $routeCustomer['delivery_schdule']['name'];
        }*/
        return $status;
    }

    private function checkalternateday($startdate, $provide_date)
    {

        $startdate       = $startdate->i18nFormat('YYY-MM-dd');
        $subscriptionday = strtotime($startdate);
        $now             = strtotime($provide_date);
        $datediff        = $now - $subscriptionday;
        $days            = floor($datediff / (60 * 60 * 24));
        //echo $days;die;
        if ($days % 2 == 0) {
            $tomorrow = $provide_date;

        } else {
            $tomorrow = date('Y-m-d', strtotime($provide_date . "+1 days"));
        }

        return $tomorrow;

    }

    /*Status code startv from 14001 From Here    */

    private function emptyToken($userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->get($userid);

        $user->token    = '';
        $user->modified = date('Y-m-d h:i:s');
        if ($userTable->save($user)) {
            return true;
        } else {
            return false;
        }

    }

    public function logout()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data  = $this->request->getData();
            $error = $this->validateupdateProfileCustomer($data);
            if ($error['messageCode'] == 200) {
                if ($this->emptyToken($data['user_id'])) {
                    $error['messageText'] = "Logged out Successfully";
                    $error['messageCode'] = 200;
                    $error['successCode'] = 1;
                }
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }

    public function gettransactions()
    {
        if ($this->request->is('post')) {

            $data             = $this->request->getData();
            $user_id          = $data['user_id'];
            $transactionTable = TableRegistry::get('Transactions');
            $transactions     = $transactionTable->find('all')->contain(['TransactionTypes'])->order(['created' => 'desc'])->where(['Transactions.user_id' => $user_id])->toArray();

            $final_array    = array();
            $check_in_array = array();
            $before_final   = array();
            $checkarray     = array();

            foreach ($transactions as $key => $value) {

                $userTable     = TableRegistry::get('Users');
                $transactions1 = $userTable->find('all')->contain(['Regions', 'Areas'])->where(['Users.id' => $user_id])->toArray();

                $usersSubscriptionTable = TableRegistry::get('userSubscriptions');
                $transactions2          = $usersSubscriptionTable->find('all')->contain(['Products'])->where(['user_id' => $user_id])->toArray();

                foreach ($transactions1 as $key1 => $value1) {

                    $temp = array();
                    //$temp['region_area_name'] = $value1['region']['name'];
                    //$temp['name'] = $value1['name'];

                    $temp['transaction_id']          = $value['id'];
                    $temp['amount']                  = $value['amount'];
                    $temp['created']                 = $value['created'];
                    $temp['time']                    = $value['time'];
                    $temp['transaction_amount_type'] = $value['transaction_amount_type'];
                    $temp['transaction_type']        = $value['transaction_type']['transaction_type_name'];
                    $status                          = $value['status'];
                    if ($status == 1) {
                        $temp['status'] = "delivered";
                    } else {
                        $temp['status'] = "cancelled";
                    }
                    //$temp['area_name'] = $value1['area']['name'];
                    //$temp['phoneNo'] = $value1['phoneNo'];
                    $final_array[] = $temp;

                }
                foreach ($transactions2 as $key2 => $value2) {

                    //$temp['quantity'] = $value2['quantity'];
                    //$temp['product_name'] = $value2['product']['name'];
                    //$temp['product_price'] = $value2['product']['price_per_unit']*$value2['quantity'];
                    //$final_array[] = $temp;

                }

            }

            $response['messageText']  = "success";
            $response['messageCode']  = 200;
            $response['successCode']  = 1;
            $response['count']        = count($final_array);
            $response['transactions'] = $final_array;
            echo json_encode($response);die;
            ///$this->set('transactions',$final_array);
        }

    }

    public function aboutus()
    {

        //if ($this->request->is('post')) {
            $staticPagesTable = TableRegistry::get('StaticPages');
            $about_desc       = $staticPagesTable->find('all')->select(['title', 'description'])->where(['id' => 1])->toArray();

            $response['messageText']  = "success";
            $response['messageCode']  = 200;
            $response['successCode']  = 1;
            $response['content'] = $about_desc;
            // pr($about_desc);die;
            echo json_encode($response);die;
        //}

    }

    public function paytm()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            // following files need to be included
            require_once ROOT . '/paytm' . DS . 'lib' . DS . 'config_paytm.php';
            require_once ROOT . '/paytm' . DS . 'lib' . DS . 'encdec_paytm.php';

            $checkSum  = "";
            $paramList = array();

            $ORDER_ID         = $data["ORDER_ID"];
            $CUST_ID          = $data["CUST_ID"];
            $INDUSTRY_TYPE_ID = $data["INDUSTRY_TYPE_ID"];
            $CHANNEL_ID       = $data["CHANNEL_ID"];
            $TXN_AMOUNT       = $data["TXN_AMOUNT"];

            // Create an array having all required parameters for creating checksum.
            $paramList["MID"]              = PAYTM_MERCHANT_MID;
            $paramList["ORDER_ID"]         = $ORDER_ID;
            $paramList["CUST_ID"]          = $CUST_ID;
            $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
            $paramList["CHANNEL_ID"]       = $CHANNEL_ID;
            $paramList["TXN_AMOUNT"]       = $TXN_AMOUNT;
            $paramList["WEBSITE"]          = PAYTM_MERCHANT_WEBSITE;

            /*
            $paramList["MSISDN"] = $MSISDN; //Mobile number of customer
            $paramList["EMAIL"] = $EMAIL; //Email ID of customer
            $paramList["VERIFIED_BY"] = "EMAIL"; //
            $paramList["IS_USER_VERIFIED"] = "YES"; //

             */

            //Here checksum string will return by getChecksumFromArray() function.
            $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
            echo "<html>
    <head>
    <title>Merchant Check Out Page</title>
    </head>
    <body>
        <center><h1>Please do not refresh this page...</h1></center>
            <form method='post' action='" . PAYTM_TXN_URL . "' name='f1'>
    <table border='1'>
     <tbody>";

            foreach ($paramList as $name => $value) {
                echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
            }

            echo "<input type='hidden' name='CHECKSUMHASH' value='" . $checkSum . "'>
     </tbody>
    </table>
    <script type='text/javascript'>
     document.f1.submit();
    </script>
    </form>
    </body>
    </html>";

        }
    }

    public function payumoney()
    {
        date_default_timezone_set('Asia/Kolkata');
        if ($this->request->is('post')) {

            $json_t = $this->request->getData();

            $data = json_decode($json_t['res'], true);

            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $transactionsTable   = TableRegistry::get('Transactions');
                $istransactionexists = $transactionsTable->find()->where(['online_transaction_id' => $data['id']])->count();

                if ($istransactionexists > 0) {

                    return;
                } else {

                    $transactionTable                       = TableRegistry::get('Transactions');
                    $transactions                           = $transactionTable->newEntity();
                    $transactions->user_id                  = $data['productinfo'];
                    $transactions->transaction_amount_type  = "Cr";
                    $transactions->amount                   = $data['amount'];
                    $transactions->onlinetransactionhistory = $json_t;
                    $transactions->created                  = date('Y-m-d');
                    $transactions->time                     = date('H:i:s');
                    if ($data['status'] === "success") {
                        $transactions->status = 1;
                    } else {
                        $transactions->status = 0;
                    }

                    $transactions->delivery_schdule_id          = 0;
                    $transactions->transaction_type_id          = 1;
                    $transactions->refund_type_id               = 0;
                    $transactions->users_subscription_status_id = 1;
                    $transactions->payment_gateway_type         = "payumoney";
                    $transactions->online_transaction_id        = $data['id'];

                    $result = $transactionTable->save($transactions);

                    $UserBalancesTable = TableRegistry::get('UserBalances');
                    $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $data1['user_id']])->select('balance')->first();
                    $balanceamount     = $UserBalances['balance'];
                    $balanceamount     = $balanceamount + $data['amount'];

                    $query   = $UserBalancesTable->query();
                    $result1 = $query->update()
                        ->set(['balance' => $balanceamount])
                        ->where(['user_id' => $data['user_id']])
                        ->execute();

                    if ($result) {
                        $message                 = "Rs. " . $data["amount"] . " has been added to your account successfully";
                        $push                    = $this->pushnotifications($data['user_id'], $message);
                        $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['message']     = "Your Transaction has been saved successfully.";
                    } else {
                        $response['messageText'] = "Invalid Request";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 0;
                        $response['message']     = "Error processing your request.";
                    }
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;

        }
    }

/* paytm */
    public function createchecksum()
    {

        if ($this->request->is('post')) {

            $data = $this->request->getData();
            require_once "lib/config_paytm.php";
            require_once "lib/encdec_paytm.php";
            $checkSum = "";

            // below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
            $findme     = 'REFUND';
            $findmepipe = '|';

            $paramList = array();

            $paramList["MID"]                       = $data["MID"];
            $paramList["ORDER_ID"]                  = $data["ORDER_ID"];
            $paramList["CUST_ID"]                   = $data["CUST_ID"];
            $paramList["INDUSTRY_TYPE_ID"]          = $data["INDUSTRY_TYPE_ID"];
            $paramList["CHANNEL_ID"]                = $data["CHANNEL_ID"];
            $paramList["TXN_AMOUNT"]                = $data["TXN_AMOUNT"];
            $paramList["PAYTM_MERCHANT_WEBSITEITE"] = $data["WEBSITE"];

            foreach ($_POST as $key => $value) {
                $pos     = strpos($value, $findme);
                $pospipe = strpos($value, $findmepipe);
                if ($pos === false || $pospipe === false) {
                    $paramList[$key] = $value;
                }
            }

            //Here checksum string will return by getChecksumFromArray() function.
            $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
            //print_r($_POST);
            echo json_encode(array("CHECKSUMHASH" => $checkSum, "ORDER_ID" => $data["ORDER_ID"], "payt_STATUS" => "1"));
            die;
            //Sample response return to SDK

            //  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"}
        }
    }

    public function verifychecksum()
    {
        if ($this->request->is('post')) {
            header("Pragma: no-cache");
            header("Cache-Control: no-cache");
            header("Expires: 0");

            // following files need to be included
            require_once "lib/config_paytm.php";
            require_once "lib/encdec_paytm.php";

            $paytmChecksum   = "";
            $paramList       = array();
            $isValidChecksum = false;

            $paramList     = $_POST;
            $return_array  = $_POST;
            $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

            //Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
            $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

            // if ($isValidChecksum===TRUE)
            //  $return_array["IS_CHECKSUM_VALID"] = "Y";
            // else
            //  $return_array["IS_CHECKSUM_VALID"] = "N";

            $return_array["IS_CHECKSUM_VALID"] = $isValidChecksum;
            //$return_array["TXNTYPE"] = "";
            //$return_array["REFUNDAMT"] = "";
            unset($return_array["CHECKSUMHASH"]);

            echo $encoded_json = htmlentities(json_encode($return_array));

        }
    }
/*  paytm */

    public function paytmurl()
    {

        $paytmTable         = TableRegistry::get('Paytms');
        $paytm              = $paytmTable->newEntity();
        $paytm->description = "New paytm transaction";
        $result             = $paytmTable->save($paytm);
        echo "inserted";
        die;

    }

    public function addtransaction()
    {
        date_default_timezone_set('Asia/Kolkata');
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $data1 = json_decode($data['res'], true);

                $transactionTable                       = TableRegistry::get('Transactions');
                $transactions                           = $transactionTable->newEntity();
                $transactions->user_id                  = $data['user_id'];
                $transactions->transaction_amount_type  = "Cr";
                $transactions->amount                   = $data['amount'];
                $transactions->onlinetransactionhistory = $data['res'];
                $transactions->created                  = date('Y-m-d');
                $transactions->time                     = date('H:i:s');

                if ($data['status'] === "success") {
                    $transactions->status = 1;
                } else {
                    $transactions->status = 0;
                }
                $transactions->delivery_schdule_id          = 0;
                $transactions->transaction_type_id          = 2;
                $transactions->refund_type_id               = 0;
                $transactions->users_subscription_status_id = 1;
                $transactions->payment_gateway_type         = $data['payment_gateway_type'];
                $transactions->online_transaction_id        = $data1['id'];

                $result = $transactionTable->save($transactions);

                $UserBalancesTable = TableRegistry::get('UserBalances');
                $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $data['user_id']])->select('balance')->first();

                if (count($UserBalances) > 0) {
                    $balanceamount = $UserBalances['balance'];
                    if ($data['status'] === "success") {
                        $balanceamount = $balanceamount + $data['amount'];
                        $message       = "Rs. " . $data["amount"] . " has been added to your account successfully";
                    } else {
                        $balanceamount = $balanceamount;
                        $message       = "Sorry your transaction has been failed.";
                    }
                    $query   = $UserBalancesTable->query();
                    $result1 = $query->update()
                        ->set(['balance' => $balanceamount])
                        ->where(['user_id' => $data['user_id']])
                        ->execute();

                } else {
                    $userbal = $UserBalancesTable->newEntity();

                    $userbal->user_id        = $data['user_id'];
                    $userbal->balance        = $data['amount'];
                    $userbal->thresholdmoney = 0.00;
                    $UserBalancesTable->save($userbal);
                }

                if ($result) {

                    $push1 = $this->addpush($data['user_id'], $message);
                    $push  = $this->pushnotifications($data['user_id'], $message);
                    $email = $this->sendmail($data['user_id'], $message);

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['message']     = "Your Transaction has been saved successfully.";
                } else {
                    $response['messageText'] = "Invalid Request";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                    $response['message']     = "Error processing your request.";
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);

            die;
        }

    }

    public function getorderhistory()
    {
        if ($this->request->is('post')) {

            $data    = $this->request->getData();
            $user_id = $data['user_id'];

            $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
            $UserBalances1          = $userSubscriptionsTable->find()->where(['user_id' => $data['user_id']]);

        }
    }

    public function getnotifications()
    {
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = array();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $UserNotificationsTable = TableRegistry::get('UserNotifications');
                $UserNotifications      = $UserNotificationsTable->find()->where(['user_id' => $data['user_id']])->orWhere(['user_id IS' => null])->order(['date']);

                if ($UserNotifications->count() > 1) {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['successCode']   = 1;
                    $response['notifications'] = $UserNotifications;

                } else {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['successCode']   = 1;
                    $response['notifications'] = "No notifications";
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);
            die;

        }
    }

    private function calculateContainers($globalQty, $product_id)
    {

        $response = array();

        $productTable = TableRegistry::get('Products');
        $iscontainer  = $productTable->find()->where(['id' => $product_id, 'iscontainer' => 1])->count();

        if ($iscontainer > 0) {
            $containerRuleTable = TableRegistry::get('ContainerRules');
            $containerRule      = $containerRuleTable->find('all')->where(['subscription_qty' => $globalQty])->toArray();
            $totalConatiners    = 0;
            if (count($containerRule)) {
                foreach ($containerRule as $key => $value) {

                    $totalConatiners = $totalConatiners + $value['container_count'];
                }
            }

            $response['containers'] = $totalConatiners;
            $response['status']     = 200;
            return $response;die;
        } else {
            $response['status'] = 404;
            return $response;die;
        }

    }

    public function updateusersubscriptions()
    {
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            //next subscription
            $nextDelivery = $this->getBalanceAndDeliveryInfo($data,'current');
            // pr($nextDelivery);die('h');
            if(!empty($nextDelivery) && isset($nextDelivery['subscriptionItems']) && !empty($nextDelivery['subscriptionItems'])){
                $subids = array();
                $time = array();
                foreach ($nextDelivery['subscriptionItems'] as $key => $value) {
                    $subids[] = $value['subscription_id'];
                    $time [$value['subscription_id']]  = $value['between'];
                }
                if(in_array($data['subscription_id'], $subids)){
                    $timebetween = explode('-', $time[$data['subscription_id']]);
                    $startTime    = $timebetween[0];
                    $currentDateTime        = date('Y-m-d h:i:s A');
                    $newDateTime  = date('h:i A', strtotime($currentDateTime));
                    pr($newDateTime);die('h');
                }
            }
            
            if ($response['messageCode'] == 200) {
                $user_id                    = @$data['user_id'];
                $subscription_id            = @$data['subscription_id'];
                $subscriptions_total_amount = @$data['total_price'];
                $subscription_type_id       = @$data['subscription_type_id'];
                if(!$subscription_type_id){
                      $response['messageText'] = "Subscription id is not valid.";
                      $response['messageCode'] = 1962;
                      $response['successCode'] = 0;
                      echo json_encode($response);die;
                }
                $ds_ids               = json_decode($data['delivery_schdule_ids'], true);
                $delivery_schdule_ids = implode(",", $ds_ids);
                //$delivery_schdule_ids = $data['delivery_schdule_ids'];
                $quantity   = $data['quantity'];
                $product_id = $data['product_id'];
                if (isset($data['notes']) && $data['notes']) {
                    $notes = $data['notes'];
                } else {
                    $notes = '';
                }
                $UserSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                $sub = $UserSubscriptionsTable->find()->where(['id'=>$subscription_id])->first();
                if(!$sub){
                  $response['messageText'] = "Subscription id is not valid.";
                  $response['messageCode'] = 1962;
                  $response['successCode'] = 0;
                  echo json_encode($response);die;
                }
               
                $query                  = $UserSubscriptionsTable->query();
                $query->update()
                    ->set(['quantity' => $quantity, 'delivery_schdule_ids' => $delivery_schdule_ids, 'subscriptions_total_amount' => $subscriptions_total_amount, 'notes' => $notes, 'subscription_type_id' => $subscription_type_id])
                    ->where(['id' => $subscription_id])
                    ->execute();
               
                if ($query) {
                    $this->updateintoRoute($user_id, $ds_ids);
                    $updatecontainer = $this->calculateContainers($quantity, $product_id);
                    if ($updatecontainer['status'] == 200) {
                        $userContainerTable = TableRegistry::get('UserContainers');
                        $query1             = $userContainerTable->query();
                        $query1->update()
                            ->set(['container_given' => $updatecontainer['containers']])
                            ->where(['user_id' => $user_id])
                            ->execute();

                        $subContainerTable = TableRegistry::get('SubscribedContainers');
                        $query2            = $subContainerTable->query();
                        $query2->update()
                            ->set(['container_count' => $updatecontainer['containers']])
                            ->where(['user_id' => $user_id])
                            ->execute();
                    }
                    $response['messageText'] = "Subscription updated";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                }
            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function addfeedback()
    {
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $feedbackTable        = TableRegistry::get('ComplaintFeedbackSuggestions');
                $newfeedback          = $feedbackTable->newEntity();
                $newfeedback->subject = $data['subject'];
                $newfeedback->content = $data['message'];
                $newfeedback->user_id = $data['user_id'];
                $newfeedback->date    = date('Y-m-d');
                if (trim($data['subject']) == 'Product Issue') {
                    $newfeedback->product_id = $data['product_id'];
                }
                $newfeedback->type   = $data['type'];
                $newfeedback->status = 0;
                $result              = $feedbackTable->save($newfeedback);

                if ($result) {
                    $message                 = "We have received your complaint. Your complaint will be resolved ASAP.";
                    $email                   = $this->sendmail($data['user_id'], $message);
                    $response['messageText'] = "Your complaint is saved.";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 0;
                }

            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }

            echo json_encode($response);die;

        }
    }

    public function feedbacklist()
    {
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $feedbackTable = TableRegistry::get('ComplaintFeedbackSuggestions');
                $result        = $feedbackTable->find()->order(['id' => 'desc'])->where(['user_id' => $data['user_id']])->toArray();

                if ($result) {
                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 0;
                    $response['feedback']    = $result;
                }
            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    private function updatecoupon($coupon_code)
    {
        $CouponTrackersTable = TableRegistry::get('CouponTrackers');
        $query               = $CouponTrackersTable->query();
        $result              = $query->update()->set(['use_status' => 1])->where(['s_c_c' => $coupon_code])->execute();
        if ($result) {return true;}
    }
    private function addcoupon($user_id, $amount)
    {
        date_default_timezone_set('Asia/Kolkata');
        $transactionTable                           = TableRegistry::get('Transactions');
        $transactions                               = $transactionTable->newEntity();
        $transactions->user_id                      = $user_id;
        $transactions->transaction_amount_type      = "Cr";
        $transactions->amount                       = $amount;
        $transactions->created                      = date('Y-m-d');
        $transactions->time                         = date('H:i:s');
        $transactions->status                       = 1;
        $transactions->delivery_schdule_id          = 0;
        $transactions->transaction_type_id          = 3;
        $transactions->refund_type_id               = 0;
        $transactions->users_subscription_status_id = 1;
        $result                                     = $transactionTable->save($transactions);

        $UserBalancesTable = TableRegistry::get('UserBalances');
        $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $user_id])->select('balance')->first();
        $balanceamount     = $UserBalances['balance'];
        $balanceamount     = $balanceamount + $amount;

        $query   = $UserBalancesTable->query();
        $result1 = $query->update()
            ->set(['balance' => $balanceamount])
            ->where(['user_id' => $user_id])
            ->execute();

        if ($result) {
            $message = "Rs. " . $amount . " has been added to your account successfully";
            //$push = $this->pushnotifications($amount,$message);

            $response['messageCode'] = 200;
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
            $response['message']     = "Error processing your request.";
        }
        return $response;
    }

    public function reedemcoupon()
    {
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            if(!@$data['coupon_code']){
                $response['messageText'] = "Coupon Code is required.";
                $response['messageCode'] = 1987;
                $response['successCode'] = 0;
                echo json_encode($response);die;
            }
            $coupon   = $data['coupon_code'];
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $coupon              = $data['coupon_code'];
                $coupontrackersTable = TableRegistry::get('CouponTrackers');

                $result  = $coupontrackersTable->find()->where(['s_c_c' => $coupon, 'use_status' => 0])->toArray();
                $result1 = $coupontrackersTable->find()->where(['s_c_c' => $coupon, 'use_status' => 1])->toArray();
                if (count($result) > 0) {
                    $coupon_id = $result[0]['coupon_id'];

                    $couponsTable = TableRegistry::get('Coupons');
                    $couponresult = $couponsTable->find()->where(['id' => $coupon_id])->toArray();

                    //print_r($couponresult);
                    $amount      = $couponresult[0]['price_value'];
                    $currentDate = date('Y-m-d');
                    $startDate   = date('Y-m-d', strtotime($couponresult[0]['start_date']));

                    $endDate = date('Y-m-d', strtotime($couponresult[0]['end_date']));

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        $res = $this->addcoupon($data['user_id'], $amount);
                        if ($res['messageCode'] == 200) {
                            $res1    = $this->updatecoupon($data['coupon_code']);
                            $message = "Rs. " . $amount . " has been added by Coupon " . $data['coupon_code'] . " to your account successfully";
                            if ($res1) {
                                $push1 = $this->addpush($data['user_id'], $message);
                                $email = $this->sendmail($data['user_id'], $message);
                            }
                            $response['messageText'] = "Coupon value credited to your account";
                            $response['messageCode'] = 200;
                            $response['successCode'] = 0;
                        }

                    } else {
                        $response['messageText'] = "Coupon has expired";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 0;
                    }

                } else if (count($result1) > 0) {
                    $response['messageText'] = "Coupon Already Used";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;

                } else {
                    $response['messageText'] = "Wrong Coupon Code";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                }

            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function balancenotifications()
    {

        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $checkNextDeliveryTime = $this->checkNextDelivery($data);
                $totalPrice            = $this->getPrice($checkNextDeliveryTime);
                if ($checkNextDeliveryTime) {

                    $subscriptionOrderTotalPrice = $totalPrice;
                    $customerBalance             = $this->updatedCustomersBalance($data['user_id']);
                    if ($customerBalance < $subscriptionOrderTotalPrice) {
                        $message                 = "Your account balance is low.";
                        $push1                   = $this->addpush($data['user_id'], $message);
                        $email                   = $this->sendmail($data['user_id'], $message);
                        $response['messageText'] = "error";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 1;
                        $response['message']     = "Your account balance is low.";
                    } else {
                        $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['Balance']     = $customerBalance;
                    }
                }

            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function pushnotificationstoall()
    {
        $message   = "Daily notification message.";
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->select(['device_id'])->where(['device_id IS NOT' => null])->toArray();
        foreach ($user as $key => $value) {
            $userid = $value['device_id'];

            $push1 = $this->addpush1($userid, $message);
        }
        die;

    }

    public function getdailymilk()
    {

        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $usersub               = $userSubscriptionTable->find('all')->where(['product_id' => 86, 'users_subscription_status_id' => 1, 'startdate <=' => date('Y-m-d')])->select(['id', 'subscription_type_id', 'product_id', 'delivery_schdule_ids', 'quantity', 'users_subscription_status_id', 'user_id', 'subscriptions_total_amount'])->toArray();
            $milktotal             = 0;
            $i                     = 0;
            foreach ($usersub as $key => $value) {
                $milk      = $value['quantity'];
                $milktotal = $milktotal + $milk;
                $milk1     = $value['subscription_type_id'];
                if ($milk1 = 30) {$i++;}
            }
            echo $i;
            echo "   ";
            echo $milktotal;
            echo "   ";
            echo json_encode($usersub);

        }

        //echo json_encode($response);
        die;

    }

    private function updateintoRoute($user_id, $delivery_schdule_ids)
    {
        $usersubscription = TableRegistry::get('UserSubscriptions');
        $subscriptions    = $usersubscription->find('all')->where(['user_id' => $user_id])->select(['delivery_schdule_ids'])->hydrate(false)->toArray();
        $dsids            = array();
        foreach ($subscriptions as $key => $value) {
            $dids = explode(',', $value['delivery_schdule_ids']);
            foreach ($dids as $val) {
                if (!in_array($val, $dsids)) {
                    array_push($dsids, $val);
                }
            }
        }

        //get all route customer for a user
        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        $routeCustomer      = $routeCustomerTable->find('all')->where(['user_id' => $user_id])->hydrate(false)->toArray();

        foreach ($routeCustomer as $key => $value) {
            if (in_array($value['delivery_schdule_id'], $dsids)) {
                continue;
            } else {
                $croutes = $routeCustomerTable->find('all')->where(['route_id' => $value['route_id']])->hydrate(false)->toArray();
                if (count($croutes) == 1) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => null])
                        ->where(['id' => $value['id']])
                        ->execute();
                } else {
                    $routcus = TableRegistry::get('RouteCustomers');
                    $entity  = $routcus->get($value['id']);
                    $result  = $routcus->delete($entity);
                }
            }
        }
        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id        = $value;
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            //pr($routeCustomer);die;
            if (count($routeCustomer) > 0) {
                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                $routeCustomers->position            = '';
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();
                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }
            }
        }
        return true;
    }

    public function deletecustomorder(){

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $customordersTable = TableRegistry::get('CustomOrders');
                    $entity  = $customordersTable->get($data['order_id']);
                    
                    $result  = $customordersTable->delete($entity);

                    if($result){
                        $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['message'] = "Order has been deleted successfully.";
                    }
                    else{
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                    $response['message'] = "Something went wrong.";
                    }

            } else{
                    $response['messageCode'] = 1053;
                    $response['successCode'] = 0;
                    $response['message'] = "Invalid User";
                    }

            echo json_encode($response); die;
        }
    }

    public function getAllSubscriptions() {
        $response = array();
        $subscriptions = array();
        $sub_status = array(1,3);
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable = TableRegistry::get('Users');
                $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                $allUserSubOrder = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($sub_status) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id IN' => $sub_status]);
                    },
                ])->where(['Users.id' => $data['user_id']])->hydrate(false)->toArray();
                if(isset($allUserSubOrder[0]['user_subscriptions']) && !empty($allUserSubOrder[0]['user_subscriptions'])){
                    foreach ($allUserSubOrder[0]['user_subscriptions'] as $key => $value) {
                        $temp                    = array();
                        $temp['subscription_id'] = $value['id'];
                        $temp['subscription_type_id'] = $value['subscription_type_id'];
                        $temp['statusId']     = $value['users_subscription_status_id'];
                        $temp['quantity']        = $value['quantity'];
                        $temp['price_per_unit'] = $value['product']['price_per_unit'];
                        $temp['productId']     = $value['product']['id'];
                        $temp['productName']     = $value['product']['name'];
                        $temp['image']     = @$value['product']['image'];
                        $temp['description']     = @$value['product']['description'];
                        $temp['unit']            = $value['unit_name'];
                        $temp['amount']          = $value['subscriptions_total_amount'];
                        $temp['startdate']          = $value['startdate']->i18nFormat('dd-MM-YYY');
                        $temp ['daterange']          = $this->getDateRange($temp['startdate'],$temp['subscription_type_id']);
                        $temp['sid']             = $value['delivery_schdule_ids'];
                        $subscriptions[]  = $temp;
                    }
                }
                $response['messageText'] = "success";
                $response['messageCode'] = 200;
                $response['successCode'] = 1;
                $response['subscriptions'] = $subscriptions;
                // pr($allUserSubOrder);die;
            } else {
                $response['messageCode'] = 1053;
                $response['successCode'] = 0;
                $response['message'] = "Invalid User";
            }
        }else{
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response); die;
    }

    public function getDateRange($startdate,$typeid) {
        $range = array();
        if($typeid == 30){
            $p = 'P2D';
        }else{
            $p = 'P1D';
        }

        $period = new \DatePeriod(
            new \DateTime($startdate),
            new \DateInterval($p),
            new \DateTime(END_DATE)
        );

        $i = 0;
        foreach ($period as $date) {
            $i++;
            $range[] = $date->format("d-m-Y"); 
            //echo $date->format("Y-m-d") . "<br>";
        }
        return $range;
    }

    public function contactus(){
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable = TableRegistry::get('Users');
                $user = $userTable->find('all')->where(['id'=> $data['user_id']])->toArray();
                $areasTable = TableRegistry::get('Areas'); 
                $area = $areasTable->find('all')->select(['manager_name','manager_phoneNo','manager_email'])->where(['id'=> $user[0]['area_id']])->toArray();
                $contactsTable = TableRegistry::get('Contacts');
                $contacts = $contactsTable->find('all')->toArray();

                $response['messageText'] = "Success";
                $response['messageCode'] = 200;
                $response['successCode'] = 1;
                $response['area_manager'] = $area;
                $response['Contacts'] = $contacts;
            }else{
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
        echo json_encode($response); die;
        }
    }

}
