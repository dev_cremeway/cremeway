<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Twilio\Rest\Client;
use Cake\Mailer\Email;

class CustomerApiController extends AppController
{
    protected $_futureDate = '';

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['sendOtp', 'getSchdule', 'getBalanceAndDeliveryInfo', 'getCustomerBalance', 'recomdedItemMilkManDetails', 'categoryProduct', 'verificationOtp', 'updateProfile', 'getAllRegions', 'getAllRegionsArea', 'searchProduct', 'updateOrder', 'addSubscription', 'getCategoriesAndSubscriptionTypes', 'checkCategoryDeliverThisCustomer', 'getProductUnitNameAndChieldren', 'getProductChildrenPrice', 'getProductPriceManually', 'activeDeActiveSubscriptionList', 'changeSubscriptionStatus', 'addToNextDelivery', 'getCalendraEvent', 'gettransactions', 'aboutus', 'checkifcustomordersnotdelivered1', 'paytm', 'payumoney', 'createchecksum', 'verifychecksum', 'paytmurl', 'addtransaction', 'getorderhistory', 'pushnotifications', 'getnotifications', 'addpush', 'updateusersubscriptions', 'calculateContainers', 'addfeedback', 'feedbacklist', 'reedemcoupon', 'balancenotifications', 'sendmail', 'pushnotificationstoall', 'getdailymilk', 'deletecustomorder', 'getAllSubscriptions', 'contactus', 'settings', 'getProfile', 'getProfileInfo', 'changeOrder', 'getModifydata', 'notify','versions','getbalance','logout','referAfriend','transactiondetails','paytmstatus','checkpaytmtransactions','gallery','galleryimages','payumoney1','success','failure','payuwebhook','getPaymentMethod','getPaymentMethodConfig']);
    }

    /*---Start code for multilanguage from 1000-----*/

    /*--status code reserve for sendOtp function Start from 1000 to 1020---*/

    private function checkMobileNo($mobileNo = null)
    {

        if ($mobileNo) {

            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'type_user' => 'customer'])->count();

            if ($user) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public function notify($deviceids = 'eK-uiTQ9K4Q:APA91bF2Yg6Nr8aIx_Yak73dx6GoP2WNbhHw8Y_nGJJGrbtPvh-RzzGQUrDkK5iO2NPaGCidx3JsDXYPxJflFc-87V8X39yOfEDgriMWx2E3feD4B2TvAYJXpe4mcY3nbb2hqbEFk8kC', $message = 'hi')
    {
        if ($deviceids && $message) {
            $fields = array
                (
                'registration_ids' => array($deviceids),
                'data'             => array('message'=>$message,'title' => 'Cremeway','vibrate'=> 1,'sound' => 1,'largeIcon' => 'large_icon','smallIcon' => 'small_icon'),
            );

            $headers = array
                (
                'Authorization: key=AIzaSyDikqISQc4nza3yw2FqIBPmoiR4-2Xy314',
                'Content-Type: application/json',
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            echo $result;die;
        }
    }

    private function validateLogin($data)
    {

        $error = array();
        if (!isset($data['mobileNo']) || empty($data['mobileNo'])) {
            $error['messageText'] = "Please enter your mobileNo";
            $error['messageCode'] = 1000;
            $error['successCode'] = 0;
        } else if (!isset($data['user_type']) || empty($data['user_type'])) {
            $error['messageText'] = "Please enter customer type";
            $error['messageCode'] = 1001;
            $error['successCode'] = 0;
        }else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    private function alreadyAuthorizedWithOtp($mobileNo)
    {

        if ($mobileNo) {

            $userTable = TableRegistry::get('Users');

            $user = $userTable->find()->where(['phoneNo' => $mobileNo, 'type_user' => 'customer', 'otp <>' => 0, 'verifed_otp' => 1])->count();
            if ($user) {
                return true;
            } else {
                return false;
            }

        }
    }

    private function updateOtp($customer_type, $mobile)
    {
        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => trim($mobile), 'type_user' => trim($customer_type)])->count();
        if ($user) {
            return true;
        } else {
            $userTable                = TableRegistry::get('Users');
            $newcustomer              = $userTable->newEntity();
            $newcustomer->verifed_otp = 0;
            $newcustomer->phoneNo     = $mobile;
            $newcustomer->created     = date('Y-m-d');
            $newcustomer->modified    = date('Y-m-d');
            $newcustomer->type_user   = 'customer';
            $result                   = $userTable->save($newcustomer);

            if ($result->id) {
                return true;
            } else {
                return false;
            }
        }

    }

    private function updateToken($mobile)
    {

        $userTable = TableRegistry::get('Users');
        $token     = \Cake\Utility\Text::uuid();
        $modified  = date('Y-m-d h:i:s');
        $query     = $userTable->query();
        $result    = $query->update()
            ->set(['token' => $token, 'modified' => $modified])
            ->where(['phoneNo' => $mobile])
            ->execute();
        if ($result) {
            return true;
        } else {
            return false;
        }

    }
    private function sendOtpToCustomer($otp, $mobileNo)
    {

        require_once ROOT . DS . 'vendor' . DS . 'Twilio' . DS . 'autoload.php';
        $sid    = 'ACaf9007c5da5a2f68a4fed37ee698ad8b';
        $token  = '72c1413a90e3d1526e983167c1cc7030';
        $client = new Client($sid, $token);
        $client->messages->create(
            '+91' . $mobileNo,
            array(
                'from' => '+17603787142',
                'body' => 'Your cremeway Verification code is ' . $otp,
            )
        );
        return true;

    }

    private function checkAddByAdmin($mobileNo)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'status' => 1])->count();

        if ($user) {
            $error['messageCode'] = 200;
        } else {
            $error['messageCode'] = 201;
        }
        return $error;

    }

    private function verifedByOtp($mobileNo)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'otp IS NOT NULL', 'verifed_otp' => 1])->count();

        if ($user) {
            $error['messageCode'] = 200;
        } else {
            $error['messageCode'] = 201;
        }
        return $error;

    }

    /*--status code reserve for VerificationOtp function Start from 2400 2440 ---*/

    public function verificationOtp()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $this->updateOtp($data['user_type'], $data['mobileNo']);
            $error = $this->validateVerificationCode($data);
            if ($error['messageCode'] == 200) {
                $result = $this->updateAfterVerification($data);
                if ($result) {
                    $usersTable   = TableRegistry::get('Users');
                    $customerInfo = $usersTable
                        ->find('all')
                        ->where(['Users.phoneNo' => $data['mobileNo'], 'Users.type_user' => $data['user_type']])
                        ->toArray();
                    $error['customerInfo'] = $customerInfo;
                    $currentDateTime       = date('Y-m-d h:i:s');
                    $newDateTime           = date('h:i A', strtotime($currentDateTime));
                    $error['loggedtime']   = date("H:i", strtotime($newDateTime));
                } else {
                    $error['messageText'] = "Something went wrong please try again";
                    $error['successCode'] = 0;
                    $error['messageCode'] = 2403;
                }
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }
    private function updateAfterVerification($data)
    {

        $usersTable  = TableRegistry::get('Users');
        $token       = \Cake\Utility\Text::uuid();
        $modified    = date('Y-m-d h:i:s');
        $verifed_otp = 1;
        $query  = $usersTable->query();
        $result = $query->update()
            ->set(['device_id' => $data['device_id'], 'token' => $token, 'modified' => $modified, 'latitude'=> $data['lat'], 'longitude'=> $data['lng']])
            ->where(['phoneNo' => $data['mobileNo'], 'type_user' => $data['user_type']])
            ->execute();

        /* update latlng in order delivery */
        $query1 = $usersTable->find('all')->select(['id'])->where(['phoneNo' => $data['mobileNo']])->hydrate(false)->toArray();

        $this->updateLatlng($query1[0]['id'], $data['lat'], $data['lng']);

        /* update latlng in order delivery */
        /* create entries in Devices table */
        $query2    = $usersTable->find('all')->select(['id'])->where(['phoneNo' => $data['mobileNo'], 'type_user' => 'customer'])->hydrate(false)->toArray();
        $condition = array();

        $condition    = ['user_id'=> $query2[0]['id'],'device_id' => $data['device_id'], 'device_type' => "android"];
        $devicesTable = TableRegistry::get('Devices');
        $device       = $devicesTable->find('all')->where($condition)->hydrate(false)->toArray();
        if (!$device) {
            $devices              = $devicesTable->newEntity();
            $devices->user_id     = $query2[0]['id'];
            $devices->device_id   = $data['device_id'];
            $devices->device_type = "android";
            $devicesTable->save($devices);
        }
        /* create entries in Devices table */

        if ($result) {
            return true;
        }return false;

    }
    private function isMatchOtpMobile($mobileNo, $otp, $user_type)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['phoneNo' => $mobileNo, 'otp' => $otp, 'type_user' => $user_type])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateVerificationCode($data)
    {

        $error = array();
        if (!isset($data['mobileNo']) || empty($data['mobileNo'])) {
            $error['messageText'] = "Please enter your mobileNo";
            $error['messageCode'] = 1000;
            $error['successCode'] = 0;
        }elseif (!isset($data['device_id']) || empty($data['device_id'])) {
            $error['messageText'] = "Please enter your device id";
            $error['messageCode'] = 2405;
            $error['successCode'] = 0;
        } else if (!isset($data['user_type']) || empty($data['user_type'])) {
            $error['messageText'] = "Please enter User type";
            $error['messageCode'] = 2406;
            $error['successCode'] = 0;
        }else {
            $error['messageCode'] = 200;
        }
        return $error;

    }
    public function sendOtp()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data  = $this->request->getData();
            $error = $this->validateLogin($data);
            if ($error['messageCode'] == 200) {
                $digits    = 4;
                $otp       = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                $updateOtp = $this->updateOtp($data['user_type'], $data['mobileNo']);
                $sendOtp   = $this->sendOtpToCustomer($otp, $data['mobileNo']);
                if ($sendOtp && $updateOtp) {
                    $error['messageText'] = "Otp has been sent to customer";
                    $error['messageCode'] = 1003;
                    $error['successCode'] = 1;

                } else {
                    $error['messageText'] = "Something went wrong.Please try again";
                    $error['messageCode'] = 1004;
                    $error['successCode'] = 0;
                }
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }

    /*--status code reserve for sendOtp function Start from 1020 to 1050---*/

    private function validateupdateProfileCustomer($data)
    {

        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageText'] = "User id can not be empty";
            $error['messageCode'] = 1051;
            $error['successCode'] = 0;
        } else if (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Token can not be empty";
            $error['messageCode'] = 1052;
            $error['successCode'] = 0;
        } else if (!$this->notThisUser($data['user_id'], $data['token'])) {
            $error['messageText'] = "Invalid user";
            $error['messageCode'] = 1053;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    public function getSchdule($data=null)
    {
        date_default_timezone_set('Asia/Kolkata');
        $time   = date('H:i');
        $response = array();

            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $driverRouteTable = TableRegistry::get('DeliverySchdules');
                $schdule          = $driverRouteTable->find('all')->where(['id'=>$data['schedule_id']])->hydrate(false)->toArray();

                if (count($schdule) > 0) {

                    foreach ($schdule as $key => $value) {

                        $newDateTime                 = date('h:i A', strtotime($value['start_time']));
                        $schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

                        $newDateTime               = date('h:i A', strtotime($value['end_time']));
                        $schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));
                        $changetime = date('H:i', strtotime($value['start_time']) - $value['edit_time']*3600);

                        if($time < $changetime){
                            $schdule[$key]['edit_order'] = "yes";
                        }else{
                            $schdule[$key]['edit_order'] = "no";
                        }
                        $schdule[$key]['edit_order_time'] = date('h:i A', strtotime($changetime));
                        $schdule[$key]['current_date'] = date('Y-m-d');

                    }

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = $schdule;

                } else {

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = count($schdule);
                }
            }

        return $response;

    }

    private function notThisUser($user_id, $token)
    {
        $driverTable = TableRegistry::get('Users');
        $driverInfo  = $driverTable
            ->find()
            ->select(['id', 'name', 'status'])
            ->where(['id' => $user_id])
            ->andwhere(['token' => $token])
            ->andwhere(['type_user' => 'customer'])
            ->andwhere(['is_deleted' => 0])
            ->toArray();

        if (count($driverInfo) > 0) {
            return true;
        }
        return false;
    }

    /*--status code reserve for sendOtp function Start from 1051 to 1100---*/

    private function returnDayName($day = null)
    {
        if ($day && $day != '') {
            $name = $day;
        } else {
            $name = date('l');
        }
        switch ($name) {
            case 'Sunday':
                return 7;
                break;
            case 'Monday':
                return 1;
                break;
            case 'Tuesday':
                return 2;
                break;
            case 'Wednesday':
                return 3;
                break;
            case 'Thursday':
                return 4;
                break;
            case 'Friday':
                return 5;
                break;
            case 'Saturday':
                return 6;
                break;
            default:
                break;
        }
    }

    public function getBalanceAndDeliveryInfo($data = null, $datatype = null)
    {
        $futureDate = '';
        $todayDate  = date('Y-m-d');
        $response   = array();
        if ($this->request->is('post') || $datatype) {
            if ($datatype) {
                $data = $data;
                if (isset($data['date']) && $data['date']) {
                    if ($datatype == 'future') {
                        $this->_futureDate = $data['date'];
                    }
                }
            } else {
                $data = $this->request->getData();
            }

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable  = TableRegistry::get('Users');
                $userstatus = $userTable->find()->select(['status'])->where(['Users.id' => $data['user_id']])->toArray();

                $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
                $usersub               = $userSubscriptionTable->find('all')->where(['user_id' => $data['user_id'], 'users_subscription_status_id' => 1])->toArray();
                if (count($usersub) > 0) {
                    $checkNextDeliveryTime = $this->checkNextDelivery($data);

                    if ($this->_futureDate) {
                        return $checkNextDeliveryTime;
                    }
                    $totalPrice = $this->getPrice($checkNextDeliveryTime);
                    if ($checkNextDeliveryTime) {
                        $response['messageText']                 = "success";
                        $response['messageCode']                 = 200;
                        $response['successCode']                 = 1;
                        $response['userstatus']                  = $userstatus[0]['status'];
                        $response['subscriptionOrderTotalPrice'] = $totalPrice;
                        $response['customerBalance']             = number_format((float)$this->updatedCustomersBalance($data['user_id']), 2, '.', '');
                        if (isset($checkNextDeliveryTime['d_s_i']) && !empty($checkNextDeliveryTime['d_s_i'])) {
                            $response['deliver_schdule_id'] = $checkNextDeliveryTime['d_s_i'];
                            $data1['user_id']   = $data['user_id'];
                            $data1['token']     = $data['token'];
                            $data1['schedule_id'] = $checkNextDeliveryTime['d_s_i'];
                        $response['willDeliver'] = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ? $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'][0]['deliverydate'] : '';

                        $products = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ? $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] : array();


                         $test = $this->getSchdule($data1);
                            if(strtotime($response['willDeliver']) > strtotime(date('Y-m-d')) ){
                               $response['edit_order'] = 'yes';
                            } else {
                                $response['edit_order'] = $test['schduleInfo'][0]['edit_order'];
                            }

                            $response['edit_time']  = $test['schduleInfo'][0]['edit_time'];
                            $response['current_date']  = date('Y-m-d');
                            $response['edit__order_time']  = $test['schduleInfo'][0]['edit_order_time'];
                        }

                        if (empty($products)) {
                            $response['messageText'] = "No Subscription found for this customer";
                            $response['messageCode'] = 1054;
                            $response['successCode'] = 0;
                            if ($datatype) {
                                return $response;
                            }
                            echo json_encode($response);die;
                        }
                        foreach ($products as $key2 => $row2) {
                            $startdate[$key2] = $row2['startdate'];

                        }
                        array_multisort($startdate, SORT_ASC, $products);
                        $response['subscriptionItems'] = $products;
                        if (isset($checkNextDeliveryTime['itemss']['customOrderInfo'])) {
                            $response['orderItems'] = $checkNextDeliveryTime['itemss']['customOrderInfo'];
                        }
                    } else {
                        $response['messageText'] = "No Subscription found for this customer";
                        $response['messageCode'] = 1054;
                        $response['successCode'] = 0;
                    }
                } else {
                    $response['messageText']     = "No Subscription found for this customer";
                    $response['messageCode']     = 200;
                    $response['userstatus']      = $userstatus[0]['status'];
                    $response['customerBalance'] = number_format((float)$this->updatedCustomersBalance($data['user_id']), 2, '.', '');
                    $response['successCode']     = 1;
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        if ($datatype) {
            return $response;
        }
        echo json_encode($response);die;
    }

    private function getRecomandedItems($data = null)
    {
        $userTable         = TableRegistry::get('Users');
        $userdata          = $userTable->find()->select(['area_id'])->where(['id' => @$data['user_id']])->hydrate(false)->first();
        $areaId            = $userdata['area_id'];
        $productAreasTable = TableRegistry::get('ProductAreas');
        $productareas      = $productAreasTable->find('list', ['keyField' => 'id', 'valueField' => 'product_id'])->where(['area_id' => $areaId])->toArray();
        if (empty($productareas)) {
            $productareas = array('0');
        }
        $producTable = TableRegistry::get('Products');
        $products    = $producTable->find()->select(['Products.id', 'Products.category_id', 'Products.name', 'Products.is_subscribable', 'Products.price_per_unit', 'Products.quantity', 'Products.iscontainer', 'Products.unit_id', 'Products.status', 'Products.created', 'Products.modified', 'Products.image', 'Products.description', 'Units.name'])->where(['Products.id IN' => $productareas])->order(['rand()'])->contain(['Units'])->toArray();
        return $products;
    }

    public function recomdedItemMilkManDetails()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                $routeCustomer      = $routeCustomerTable->find()->select(['route_id'])->where(
                    [
                        'user_id'             => $data['user_id'],
                        'delivery_schdule_id' => $data['delivery_schdule_id'],
                    ])->toArray();

                if (isset($routeCustomer) && count($routeCustomer) > 0) {

                    $routeid          = $routeCustomer[0]['route_id'];
                    $driverRouteTable = TableRegistry::get('DriverRoutes');
                    $driverRoute      = $driverRouteTable->find()->select([
                        'Users.name',
                        'Users.phoneNo',
                        'Users.image',
                        'Users.id',
                    ])->contain(['Users'])->where(['DriverRoutes.route_id' => $routeid])->toArray();
                    if (empty($driverRoute)) {
                        $response['messageText'] = "Not Found Any Driver for this route";
                        $response['messageCode'] = 2151;
                        $response['successCode'] = 0;
                        echo json_encode($response);die;
                    }
                    $orderTrackingsTable = TableRegistry::get('OrderTrackings');
                    $driverRoute1        = $orderTrackingsTable->find()->select([
                        'OrderTrackings.lat',
                        'OrderTrackings.lng',
                    ])->where(['OrderTrackings.user_id' => $driverRoute[0]['Users']['id']])->toArray();

                    $regionsList1 = array();
                    foreach ($driverRoute as $key => $value) {
                        $temp            = array();
                        $temp['id']      = $value['Users']['id'];
                        $temp['name']    = $value['Users']['name'];
                        $temp['phoneNo'] = $value['Users']['phoneNo'];
                        $temp['image']   = $value['Users']['image'];

                    }
                    foreach ($driverRoute1 as $key => $value) {
                        $temp['latitude']  = $value['lat'];
                        $temp['longitude'] = $value['lng'];
                    }
                    if (isset($driverRoute) && count($driverRoute) > 0) {

                        $recomandedItems             = array();
                        $recomandedItems             = $this->getRecomandedItems($data);
                        $response['messageText']     = "success";
                        $response['messageCode']     = 2152;
                        $response['successCode']     = 1;
                        $response['driverInfo']      = $temp;
                        $response['recomandedItems'] = $recomandedItems;

                    } else {

                        $response['messageText'] = "Not Found Any Driver for this route";
                        $response['messageCode'] = 2151;
                        $response['successCode'] = 0;
                    }

                } else {
                    $response['messageText'] = "Not Found Any Driver for this route";
                    $response['messageCode'] = 2151;
                    $response['successCode'] = 0;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    /*--status code reserve for getCustomerBalance function Start from 2201 to 2250 ---*/

    public function categoryProduct()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable         = TableRegistry::get('Users');
                $userdata          = $userTable->find()->select(['area_id'])->where(['id' => $data['user_id']])->hydrate(false)->first();
                $areaId            = $userdata['area_id'];
                $productAreasTable = TableRegistry::get('ProductAreas');
                $productareas      = $productAreasTable->find('list', ['keyField' => 'id', 'valueField' => 'product_id'])->where(['area_id' => $areaId])->toArray();
                if (empty($productareas)) {
                    $productareas = array('0');
                }
                $category      = array();
                $categoryTable = TableRegistry::get('Categories');
                $category      = $categoryTable->find()->contain(['Products'])->count();
                if (count($category) > 0) {
                    $categorylist = $categoryTable->find('all')
                        ->contain(['Products.Units' => function (\Cake\ORM\Query $query) use ($productareas) {
                            return $query->where(['Products.id IN' => $productareas]);
                        },
                        ])->hydrate(false)->toArray();
                    $response['messageText']             = "success";
                    $response['messageCode']             = 2251;
                    $response['successCode']             = 1;
                    $response['allCategoryProductsList'] = $categorylist;

                } else {

                    $response['messageText']             = "Not Found and Product";
                    $response['messageCode']             = 2252;
                    $response['successCode']             = 1;
                    $response['allCategoryProductsList'] = $category;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function getCustomerBalance()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $userBalanceTable = TableRegistry::get('UserBalances');
                $userBalance      = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $data['user_id']])->toArray();
                if (count($userBalance) > 0) {

                    $response['messageCode']     = 200;
                    $response['successCode']     = 1;
                    $response['customerBalance'] = $userBalance[0]['balance'];
                } else {

                    $response['messageCode']     = 200;
                    $response['successCode']     = 1;
                    $response['customerBalance'] = [];
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function getAllRegions()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $regions     = array();
                $regionTable = TableRegistry::get('Regions');
                $regions     = $regionTable->find('list')->toArray();
                if (count($regions) > 0) {

                    $regionsList = array();

                    foreach ($regions as $key => $value) {

                        $temp              = array();
                        $temp['regionId']  = $key;
                        $temp['regioName'] = $value;
                        array_push($regionsList, $temp);
                    }

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;

                } else {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    /*--status code reserve for getCustomerBalance function Start from 3101 to 3110 ---*/

    public function getAllRegionsArea()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if (!isset($data['region_id']) || empty($data['region_id'])) {

                $response['messageText'] = "Region id can not be empty";
                $response['messageCode'] = 3101;
                $response['successCode'] = 0;
                echo json_encode($response);die;

            }

            if ($response['messageCode'] == 200) {

                $regions    = array();
                $areasTable = TableRegistry::get('Areas');
                $areas      = $areasTable->find('list')->where(['region_id' => $data['region_id']])->toArray();

                $areaList = array();

                if (count($areas) > 0) {

                    $temp = array();
                    foreach ($areas as $key => $value) {
                        $temp['areaId']   = $key;
                        $temp['areaName'] = $value;
                        array_push($areaList, $temp);
                    }

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists']   = $areaList;

                } else {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists']   = $areaList;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

/*--status code reserve for getCustomerBalance function Start from 3111 to 3130 ---*/

    private function updateLatlng($userId, $latitude, $longitude)
    {
        /* update lat lng in OrderTrackings */

        $orderTrackingsTable = TableRegistry::get('OrderTrackings');
        $orderTrackings1     = $orderTrackingsTable->find('all')->where(['user_id' => $userId])->hydrate(false)->toArray();

        if (count($orderTrackings1) > 0) {
            $query  = $orderTrackingsTable->query();
            $result = $query->update()
                ->set(['lat' => $latitude, 'lng' => $longitude])
                ->where(['user_id' => $userId])
                ->execute();
        } else {
            $orderTrackings          = $orderTrackingsTable->newEntity();
            $orderTrackings->user_id = $userId;
            $orderTrackings->lat     = $latitude;
            $orderTrackings->lng     = $longitude;
            $orderTrackingsTable->save($orderTrackings);
        }

        /* update lat lng in OrderTrackings end */
    }
    public function updateProfile()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $UsersTable = TableRegistry::get('Users');
                $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $data['user_id']])->first()->toArray();
                $pre_area   = $users['area_id'];
                $pre_region = $users['region_id'];

                $response = $this->validateupdateProfileUpdateCustomer($data);
                if ($response['messageCode'] == 200) {

                    if (isset($data['image']) && !empty($data['image'])) {
                        $response = $this->validateImage($data['image']);
                    }
                    if ($response['messageCode'] == 200) {

                        $userTable = TableRegistry::get('Users');
                        $user      = $userTable->get($data['user_id']);

                        if (isset($data['image']) && !empty($data['image'])) {
                            $imagename   = $this->upload_image($data['image'], 'customer', '../webroot/img/images/');
                            $user->image = 'img/images/' . $imagename;
                        }

                        if ($user->region_id != $data['region_id'] || $user->area_id != $data['area_id']) {
                            $message = "" . $user->name . " has updated his area.";
                        }

                        $user->name      = $data['name'];
                        $user->email_id  = @$data['email'];
                        $user->region_id = $data['region_id'];
                        $user->area_id   = $data['area_id'];
                        $user->houseNo   = $data['address'];
                        $user->modified  = date('Y-m-d h:i:s');

                        if ($userTable->save($user)) {

                            if ($pre_region != $data['region_id'] || $pre_area != $data['area_id']) {
                                $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                                $query                  = $userSubscriptionsTable->query();
                                $result                 = $query->update()
                                    ->set(['users_subscription_status_id' => 4])
                                    ->where(['user_id' => $data['user_id']])
                                    ->execute();
                                $RouteCustomers = TableRegistry::get('RouteCustomers');
                                $RouteCustomers->deleteAll(['user_id IN' => array($data['user_id'])]);
                                $userContainers = TableRegistry::get('UserContainers');
                                $userscont      = $userContainers->find()->where(['user_id' => $data['user_id']])->toArray();
                                if($userscont){
                                    $query1                 = $userContainers->query();
                                    $result1                = $query1->update()
                                        ->set(['container_given' => 0])
                                        ->where(['user_id' => $data['user_id']])
                                        ->execute();
                                }

                            }

                            $this->updateLatlng($data['user_id'], $data['lat'], $data['lng']);
                            $response['messageText'] = "Customer Has beed updated Successfully";
                            $response['messageCode'] = 3121;
                            $response['successCode'] = 1;
                            $response['updatedInfo'] = $user;

                        } else {
                            $response['messageText'] = "something went wrong";
                            $response['messageCode'] = 201;
                            $response['successCode'] = 0;
                        }
                    }
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }
    private function validateupdateProfileUpdateCustomer($data)
    {

        $error = array();
        if (!isset($data['area_id']) || empty($data['area_id'])) {
            $error['messageText'] = "Area id can not be empty";
            $error['messageCode'] = 3112;
            $error['successCode'] = 0;
        } else if (!isset($data['region_id']) || empty($data['region_id'])) {
            $error['messageText'] = "Region can not be empty";
            $error['messageCode'] = 3113;
            $error['successCode'] = 0;
        } else if (!isset($data['address']) || empty($data['address'])) {
            $error['messageText'] = "Address can not be empty";
            $error['messageCode'] = 3118;
            $error['successCode'] = 0;
        } else if (!$this->notThisAreaRegionRelate($data['area_id'], $data['region_id'])) {
            $error['messageText'] = "Area does not belongs to Region";
            $error['messageCode'] = 3119;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function notThisAreaRegionRelate($area_id, $region_id)
    {

        $areasTable = TableRegistry::get('Areas');
        $areas      = $areasTable->find()->where(['id' => $area_id, 'region_id' => $region_id])->count();
        if ($areas) {
            return true;
        }return false;

    }

    private function isEmailAlreadyExist($email, $userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id <>' => $userid, 'email_id' => $email])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateImage($base64)
    {

        $imgdata = base64_decode($base64);

        $f = finfo_open();

        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

        $mime_type = explode('/', $mime_type);
        if ($mime_type[0] == 'image') {
            $response['messageCode'] = 200;
        } else {

            $response['messageText'] = "Invalid image";
            $response['messageCode'] = 3120;
            $response['successCode'] = 0;

        }

        return $response;

    }

    private function upload_image($content = null, $imgname = null, $dest = null, $extn = null)
    {
        if ($content) {
            $dataarray = explode('base64,', $content);
            if (count($dataarray) == 1) {
                $extn = 'jpg';

                if ($extn) {
                    $img_name = $imgname . "-" . time();
                    $data     = $content;
                    $data     = str_replace(' ', '+', $data);
                    $data     = base64_decode($data);
                    file_put_contents($dest . $img_name . "." . $extn, $data);

                    chmod($dest . $img_name . "." . $extn, 0777);
                    return $img_name . "." . $extn;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

/*--status code reserve for searchResult function Start from 3131 to 3140 ---*/

    public function searchProduct()
    {
        $response = array();
        if ($this->request->is('get')) {

            $data     = $_GET;
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable         = TableRegistry::get('Users');
                $userdata          = $userTable->find()->select(['area_id'])->where(['id' => $data['user_id']])->hydrate(false)->first();
                $areaId            = $userdata['area_id'];
                $productAreasTable = TableRegistry::get('ProductAreas');
                $productareas      = $productAreasTable->find('list', ['keyField' => 'id', 'valueField' => 'product_id'])->where(['area_id' => $areaId])->toArray();
                if (empty($productareas)) {
                    $productareas = array('0');
                }
                $product = array();

                $producTable = TableRegistry::get('Products');
                $product     = $producTable->find('all')
                    ->contain(['Units'])
                    ->where(['OR' => [
                        'Products.name LIKE '     => "%" . $data['search_param'] . "%",
                        'Products.quantity LIKE ' => "%" . $data['search_param'] . "  %",
                    ], 'Products.id IN'=> $productareas])->toArray();
                if (count($product) > 0) {

                    $response['messageCode']  = 3141;
                    $response['successCode']  = 1;
                    $response['productLists'] = $product;

                } else {

                    $response['messageCode']  = 3141;
                    $response['successCode']  = 1;
                    $response['productLists'] = $product;
                }

            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

/*--status code reserve for update Order Info function Start from 3150 to 3170 ---*/

    public function updateOrder()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->isValidJson($data['updateorder'], $data['user_id']);

                if ($response['messageCode'] == 200) {
                    $result = $this->updateOrderInfo($data);
                    if ($result) {
                        $customorders      = array();
                        $customordersTable = TableRegistry::get('CustomOrders');
                        $customorders      = $customordersTable->find('all')->contain(['Products'])->where(['user_id' => $data['user_id'], 'CustomOrders.status' => '1'])->toArray();
                        if (count($customorders) > 0) {
                            $response['messageCode'] = 3155;
                            $response['successCode'] = 1;
                            $response['orderLists']  = $customorders;
                        } else {
                            $response['messageCode'] = 3155;
                            $response['successCode'] = 1;
                            $response['orderLists']  = $customorders;
                        }
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateupdateTiming($data)
    {
        $error = array();
        if (!isset($data['delivery_schdule_id']) || empty($data['delivery_schdule_id'])) {

            $error['messageText'] = "Delivery Update Timing can not be empty";
            $error['messageCode'] = 362;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    private function isValidJson($jsonHead, $user_id, $delivery_schdule_id = null)
    {
        $res = json_decode(stripslashes($jsonHead), true);
        if ($res === null) {
            $error['messageText'] = "Data format not supported";
            $error['messageCode'] = 363;
            $error['successCode'] = 0;
        } else {
            $productdata = json_decode($jsonHead, true);
            foreach ($productdata as $key => $value) {
                if (!isset($value['pro_qty']) || $value['pro_qty'] == 0) {
                    $error['messageText'] = "Quantity can not be empty.";
                    $error['messageCode'] = 364;
                    $error['successCode'] = 0;
                } else {
                    $error['messageCode'] = 200;
                }
            }

        }
        return $error;
    }

    private function validOneResponse($data, $user_id, $delivery_schdule_id)
    {
        $error   = array();
        $deleted = array("yes", "no");
        if (!isset($data['is_deleted']) || empty($data['is_deleted'])) {
            $error['messageText'] = "Deleted signal values can not be empty";
            $error['messageCode'] = 3151;
            $error['successCode'] = 0;
        } else if (!isset($data['pro_id']) || empty($data['pro_id'])) {
            $error['messageText'] = "Container given can not be empty";
            $error['messageCode'] = 3152;
            $error['successCode'] = 0;
        } else if (!in_array($data['is_deleted'], $deleted)) {
            $error['messageText'] = "Invalid Delete signal values";
            $error['messageCode'] = 3153;
            $error['successCode'] = 0;
        } elseif ($this->productRelateToCustomer($data['pro_id'], $user_id, $delivery_schdule_id)) {
            $error['messageText'] = "Invalid Products Customer";
            $error['messageCode'] = 3154;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;

    }

    private function productRelateToCustomer($pro_id, $user_id, $delivery_schdule_id)
    {

        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()->where(['user_id' => $user_id, 'product_id' => $pro_id, 'delivery_schdule_id' => $delivery_schdule_id])->count();

        if ($customorders > 0) {
            return false;
        }return true;
    }

    private function updateOrderInfo($data)
    {
        $productdata            = json_decode($data['updateorder'], true);
        $customordersTable      = TableRegistry::get('CustomOrders');
        $usersubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $productsTable          = TableRegistry::get('Products');
        foreach ($productdata as $key => $value) {
            if (isset($value['type']) && $value['type'] == 'subscription') {
                $query = $usersubscriptionsTable->query();

                $result = $query->update()
                    ->set(['modified_quantity' => $value['pro_qty'], 'delivery_date' => @$data['delivery_date'], 'delivery_time' => @$data['delivery_time']])
                    ->where(['id' => $value['id']])
                    ->execute();

            } else {
                $oldqty = $customordersTable->find()->select(['quantity','pro_child_id'])
                        ->where(['id' => $value['id']])->hydrate(false)->first();
                if (isset($value['is_deleted']) && $value['is_deleted'] == "yes") {
                    $customordersRecord = $customordersTable->find('all')
                        ->where(['id' => $value['id']])->hydrate(false)->toArray();
                    if (empty($customordersRecord)) {
                        return false;
                    }
                    $customordersTable->id = $customordersRecord[0]['id'];
                    $customorder           = $customordersTable->get($customordersTable->id);
                    $customordersTable->delete($customorder);

                    if(@$oldqty['pro_child_id'] != 0 && @$oldqty['pro_child_id'] != ''){
                        $productChildrenTable   = TableRegistry::get('ProductChildren');
                        $in_stock   = $productChildrenTable->find()->where(['id' => $oldqty['pro_child_id']])->select(['in_stock'])->first();

                        $query2      = $productChildrenTable->query();
                        $result2     = $query2->update()
                            ->set(['in_stock' => $in_stock['in_stock']+$oldqty['quantity']])
                            ->where(['id' => @$oldqty['pro_child_id']])
                            ->execute();
                    }else{
                        $productTable          = TableRegistry::get('Products');
                        $in_stock1   = $productTable->find()->where(['id' => $value['pro_id']])->select(['quantity'])->first();

                        $query1      = $productTable->query();
                        $result1     = $query1->update()
                            ->set(['quantity' => $in_stock1['quantity']+$oldqty['quantity']])
                            ->where(['id' => $value['pro_id']])
                            ->execute();
                    }
                } elseif (isset($value['is_deleted']) && $value['is_deleted'] == "no") {
                    $query = $customordersTable->query();
                    $result = $query->update()
                        ->set(['quantity' => $value['pro_qty'], 'quantity_child' => $value['pro_qty'], 'price' => $value['total_price']])
                        ->where(['id' => $value['id']])
                        ->execute();
                    if(@$oldqty['pro_child_id'] != 0 && @$oldqty['pro_child_id'] != ''){
                        $productChildrenTable   = TableRegistry::get('ProductChildren');
                        $in_stock   = $productChildrenTable->find()->where(['id' => $oldqty['pro_child_id']])->select(['in_stock'])->first();

                        $query2      = $productChildrenTable->query();
                        $result2     = $query2->update()
                            ->set(['in_stock' => $in_stock['in_stock']-$value['pro_qty']+$oldqty['quantity']])
                            ->where(['id' => @$oldqty['pro_child_id']])
                            ->execute();
                    }else{
                        $productTable          = TableRegistry::get('Products');
                        $in_stock1   = $productTable->find()->where(['id' => $value['pro_id']])->select(['quantity'])->first();

                        $query1      = $productTable->query();
                        $result1     = $query1->update()
                            ->set(['quantity' => $in_stock1['quantity']-$value['pro_qty']+$oldqty['quantity']])
                            ->where(['id' => $value['pro_id']])
                            ->execute();
                    }
                }
            }
        }
        return true;
    }

    /*-----Add Subscription Start From Here --------------------------------*/

    /*Status code startv from 10001 From Here    */

    public function addSubscription()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateUserRegion($data['user_id']);
                if ($response['messageCode'] == 200) {
                    $response = $this->validateAddSubscription($data);
                    if ($response['messageCode'] == 200) {
                        $userSubscriptionTable                  = TableRegistry::get('UserSubscriptions');
                        $userSubscription                       = $userSubscriptionTable->newEntity();
                        $userSubscription->subscription_type_id = $data['subscription_type_id'];
                        if (isset($data['days']) && !empty($data['days'])) {
                            $userSubscription->days      = implode(",", $data['days']);
                            $ddata = explode('-', $data['start_date']);
                            if($ddata[1]=='null'){
                                $ddata[1] = 10;
                                $data['start_date'] = implode('-',$ddata);
                            }
                            $userSubscription->startdate = $data['start_date'];
                        } else {
                            $ddata = explode('-', $data['start_date']);
                            if($ddata[1]=='null'){
                                $ddata[1] = 10;
                                $data['start_date'] = implode('-',$ddata);
                            }
                            $userSubscription->startdate = $data['start_date'];
                            $userSubscription->days      = '';
                        }
                        $userSubscription->product_id = $data['product_id'];
                        $d_s_ids                      = json_decode($data['delivery_schdule_ids'], true);

                        $userSubscription->delivery_schdule_ids = implode(",", $d_s_ids);
                        $quantityy     = 0; $unit_name = "";
                        if(isset($data['is_child']) && !empty($data['is_child'])) {
                            $productChildrenTable = TableRegistry::get('ProductChildren');
                            $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id','quantity'])->where(['id' => $data['pro_child_id']])->first();
                            $userSubscription->quantity                     = $data['quantity_child'];
                            $userSubscription->quantity_child               = $data['quantity_child'];
                            $userSubscription->child_package_qty            = $productChildren['quantity'];
                            $userSubscription->pricr_per_package            = $productChildren['price'];
                            $userSubscription->pro_child_id                 = $data['pro_child_id'];
                            $userSubscription->subscriptions_total_amount   = $productChildren['price'] * $data['quantity_child'];

                            $quantityy                                      = $data['quantity_child'];
                            $unitname                                       = $this->getUnitNameAndIdchild($data['pro_child_id']);
                            $product_name                                   = $unitname['product_name'];
                            $userSubscription->unit_id                      = $unitname['id'];
                            $userSubscription->unit_name                    = $unitname['name'];
                            $unit_name                                      = $unitname['name'];
                        } else {
                            $userSubscription->quantity = $data['quantity'];
                            $quantityy                  = $data['quantity'];
                            $unitname                                       = $this->getUnitNameAndId($data['product_id']);
                            $product_name                                   = $unitname['product_name'];
                            $userSubscription->unit_id                      = $unitname['id'];
                            $userSubscription->unit_name                    = $unitname['name'];
                        }

                        $userSubscription->users_subscription_status_id = 1;
                        $userSubscription->user_id                      = $data['user_id'];
                        $userSubscription->summary                      = $data['summary'];
                        $userSubscription->notes                        = $data['notes'];
                        $userSubscription->subscriptions_total_amount   = $data['subscription_price'];
                        $userSubscription->enddate                      = '2017-12-12';
                        if ($usersub    = $userSubscriptionTable->save($userSubscription)) {
                            $saverouteCustomer = $this->addIntoRoute($data['user_id'], $d_s_ids);
                            if ($saverouteCustomer) {
                                $calcont = $this->calculateContainers($data['quantity'], $data['product_id']);
                                if (isset($calcont['containers']) && $calcont['containers'] > 0) {
                                    $userContainerTable = TableRegistry::get('UserContainers');

                                    $userContainer = $userContainerTable->find()->select(['container_given', 'id'])->where(['user_id' => $data['user_id']])->toArray();

                                    if (isset($userContainer[0]['container_given'])) {

                                        $container_given_db = $userContainer[0]['container_given'];
                                        $container_given_db = $container_given_db + $calcont['containers'];

                                        $query = $userContainerTable->query();
                                        $query->update()
                                            ->set(['container_given' => $container_given_db])
                                            ->where(['id' => $userContainer[0]['id']])
                                            ->execute();

                                        $subContainerTable = TableRegistry::get('SubscribedContainers');
                                        $query2            = $subContainerTable->query();
                                        $query2->update()
                                            ->set(['container_count' => $container_given_db])
                                            ->where(['user_id' => $data['user_id']])
                                            ->execute();

                                    } else {
                                        $userContainer                       = $userContainerTable->newEntity();
                                        $userContainer->container_given      = $calcont['containers'];
                                        $userContainer->user_id              = $data['user_id'];
                                        $userContainer->container_collect    = 0;
                                        $userContainer->left_container_count = 0;
                                        $userContainerTable->save($userContainer);

                                        $subContainerTable             = TableRegistry::get('SubscribedContainers');
                                        $subContainer                  = $subContainerTable->newEntity();
                                        $subContainer->user_id         = $data['user_id'];
                                        $subContainer->container_count = $calcont['containers'];
                                        $subContainerTable->save($subContainer);
                                    }
                                }

                                $description = "".$product_name .' ' .$quantityy .' '. $unitname['name'] .' ([' .$usersub->id."])";
                                $action         = "Subscribed";
                                $this->savedInActivity($data['user_id'], $description,$action);
                                $response['messageCode'] = 200;
                                $response['successCode'] = 1;
                                $response['messageText'] = "Subscription has been saved successfully";
                            } else {
                                $response['messageCode'] = 10021;
                                $response['successCode'] = 0;
                                $response['messageText'] = "Something Went wrong while adding Subscription";
                            }
                        } else {
                            $response['messageCode'] = 10021;
                            $response['successCode'] = 0;
                            $response['messageText'] = "Something Went wrong while adding Subscription";
                        }
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    /**
     * validate user area
     *
     * @param  array  $user_id
     * @return Array
     */
    public function validateUserRegion($user_id)
    {
        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $areaId     = $users['area_id'];
        $areasTable = TableRegistry::get('Areas');
        $area       = $areasTable->find()->where(['id' => $areaId])->select(['id'])->hydrate(false)->first();

        if (!empty($area)) {
            $error['messageCode'] = 200;
        } else {
            $error['messageText'] = "Please update your area/region before adding subscription.";
            $error['messageCode'] = 1911;
            $error['successCode'] = 0;
        }
        return $error;
    }

    public function getUnitNameAndId($pro_id)
    {
        $productTable = TableRegistry::get('Products');
        $unitId       = $productTable->find()->where(['id' => trim($pro_id)])->select(['unit_id','name','price_per_unit'])->toArray();

        $uniTable = TableRegistry::get('Units');
        $unitsName = $uniTable->find()->select(['name', 'id'])->where(['id' => @$unitId[0]['unit_id']])->toArray();

        $unit                   = array();
        $unit['name']           = @$unitsName[0]['name'];
        $unit['id']             = @$unitsName[0]['id'];
        $unit['product_name']   = @$unitId[0]['name'];
        $unit['price']          = @$unitId[0]['price_per_unit'];
        return $unit;

    }
    /*public function getUnitNameAndIdchild($pro_id=null)
    {
        $productTable = TableRegistry::get('ProductChildren');
        $unitId       = $productTable->find()->contain(['Products'])->where(['ProductChildren.id' => trim($pro_id)])->select(['ProductChildren.unit_id','Products.name'])->toArray();
        $uniTable = TableRegistry::get('Units');
        $unitsName = $uniTable->find()->select(['name', 'id'])->where(['id' => @$unitId[0]['unit_id']])->toArray();

        $unit                   = array();
        $unit['name']           = @$unitsName[0]['name'];
        $unit['id']             = @$unitsName[0]['id'];
        $unit['product_name']   = @$unitId[0]['product']['name'];
        return $unit;
    }*/
    private function addIntoRoute($user_id, $delivery_schdule_ids)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id = $value;
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            $route_position = $routeCustomerTable->find()->select(['position' => 'MAX(RouteCustomers.position)'])->where(['route_id'=> $routeCustomer[0]['route_id']])->hydrate(false)->toArray();
            if (count($routeCustomer) > 0) {
                $route_position1 = $routeCustomerTable->find()->select(['position'])->where(['user_id'=> $user_id ,'position IS NOT NULL' ])->hydrate(false)->first();
                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                if($route_position1){
                    $routeCustomers->position            = @$route_position1['position'];
                }else{
                    $routeCustomers->position            = @$route_position[0]['position'] + 1;
                }
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();

                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id,'position'=> 1])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }

            }

        }
        return true;
    }

    private function validateAddSubscription($data)
    {
        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageCode'] = 10014;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select the user name";

        } else if (!isset($data['product_id']) || empty($data['product_id'])) {
            $error['messageCode'] = 10015;
            $error['successCode'] = 0;
            $error['messageText'] = "Please select the product";
        } else if (!isset($data['delivery_schdule_ids']) || empty($data['delivery_schdule_ids'])) {
            $error['messageCode'] = 10016;
            $error['successCode'] = 0;
            $error['messageText'] = "Please select the timing";
        } else if (

            (!isset($data['quantity']) || empty($data['quantity']))
            &&
            (!isset($data['quantity_chield']) || empty($data['quantity_chield']))

        ) {
            $error['messageCode'] = 10017;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select the quantity";
        } else if ($data['quantity'] < 1 && $data['quantity_chield'] < 1) {
            $error['messageCode'] = 10021;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease select valid quantity";
        } else if (!isset($data['start_date']) || empty($data['start_date'])) {
            $error['messageCode'] = 10018;
            $error['successCode'] = 0;
            $error['messageText'] = "Please enter the start date";
        } else if (!isset($data['subscription_type_id']) || empty($data['subscription_type_id'])) {
            $error['messageCode'] = 10019;
            $error['successCode'] = 0;
            $error['messageText'] = "PLease selct the subscription type";
        } else if ($this->checkSubscription($data['user_id'], $data['product_id'], $data['delivery_schdule_ids'], $data['subscription_type_id'])) {
            $error['messageCode'] = 10020;
            $error['successCode'] = 0;
            $error['messageText'] = "This subscription already taken by this customer";
        }
        if (count($error) > 0) {
            //$error['messageCode'] = 201;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;

    }

    private function checkSubscription($userid, $productid, $delivery_schdule_ids = null, $subscription_type_id = null)
    {
        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $userSubscription      = $userSubscriptionTable->find('all')->where([
            'user_id'                         => $userid,
            'product_id'                      => $productid,
            'users_subscription_status_id <>' => 4,
        ])->toArray();
        if (count($userSubscription) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getCategoriesAndSubscriptionTypes()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $subtype           = TableRegistry::get('SubscriptionTypes');
                $subscriptionTypes = $subtype->find('all')->select(['id', 'subscription_type_name'])->toArray();
                if (isset($subscriptionTypes) && count($subscriptionTypes) > 0) {
                    $response['subscriptionTypesList'] = $subscriptionTypes;

                } else {
                    $response['subscriptionTypesList'] = [];
                }

                if (isset($response['subscriptionTypesList']) && count($response['subscriptionTypesList']) > 0) {

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;

                } else {

                    $response['messageCode'] = 10001;
                    $response['successCode'] = 1;
                    $response['messageText'] = "Please Add Category and SubscriptionTypes First";

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function checkCategoryDeliverThisCustomer()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validatecategory($data);
                if ($response['messageCode'] == 200) {

                    $getRegionAreaTable = TableRegistry::get('Users');
                    $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $data['user_id']])->toArray();

                    $region                          = $getRegionArea[0]['region_id'];
                    $area                            = $getRegionArea[0]['area_id'];
                    $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
                    $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name', 'DeliverySchdules.start_time', 'DeliverySchdules.end_time'])->where(['CategoryDeliverySchdules.category_id' => $data['category_id'], 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

                    if (count($category_delivery) > 0) {
                        $tempOutSide = array();
                        foreach ($category_delivery as $key => $value) {

                            $temp               = array();
                            $temp['d_s_id']     = $value['delivery_schdule_id'];
                            $temp['name']       = $value['delivery_schdule']['name'];
                            $temp['start_time'] = $value['delivery_schdule']['start_time'];
                            $temp['end_time']   = $value['delivery_schdule']['end_time'];
                            $tempOutSide[]      = $temp;

                        }
                        //check driver route for this schedule
                        $finalSchedule      = array();
                        $routeCustomerTable = TableRegistry::get('RouteCustomers');
                        foreach ($tempOutSide as $key => $value) {
                            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $value['d_s_id'], 'region_id' => $region, 'area_id' => $area])->hydrate(false)->toArray();
                            if (!empty($routeCustomer)) {
                                $finalSchedule[] = $value;
                            }
                        }

                        if (empty($finalSchedule)) {
                            $response['messageCode'] = 1952;
                            $response['successCode'] = 1;
                            $response['messageText'] = 'No schedule found for user route.';
                        } else {
                            $response['messageCode']                   = 200;
                            $response['successCode']                   = 1;
                            $response['provideDeliveryTimingCustomer'] = $finalSchedule;
                        }
                    } else {

                        $response['messageCode'] = 10002;
                        $response['successCode'] = 1;
                        $response['messageText'] = "We are not deliver any of the time to this customer location";
                    }

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validatecategory($data)
    {

        $error = array();
        if (!isset($data['category_id']) or empty($data['category_id'])) {
            $error['messageCode'] = 10003;
            $error['successCode'] = 1;
            $error['messageText'] = "Category should not be empty";
        } else if (!is_numeric($data['category_id'])) {
            $error['messageCode'] = 10004;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid category";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    public function getProductUnitNameAndChieldren()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProduct($data);

                if ($response['messageCode'] == 200) {

                    $productTable = TableRegistry::get('Products');
                    $unitId       = $productTable->find()->where(['id' => trim($data['product_id'])])->select(['unit_id'])->toArray();

                    $uniTable = TableRegistry::get('Units');

                    $unitsName = $uniTable->find()->select(['name'])->where(['id' => $unitId[0]['unit_id']])->toArray();
                    $name      = str_replace('"', '', $unitsName[0]['name']);

                    $childProduct     = $productTable->find('all')->where(['id' => trim($data['product_id'])])->contain(['ProductChildren', 'ProductChildren.Units'])->toArray();
                    $productChieldres = array();

                    if (count($childProduct) > 0) {

                        foreach ($childProduct[0]['product_children'] as $key => $value) {

                            $temp               = array();
                            $temp['price']      = $value['price'];
                            $temp['unit']       = $value['unit']['name'];
                            $temp['quantity']   = $value['quantity'];
                            $temp['p_c_i']      = $value['id'];
                            $productChieldres[] = $temp;
                        }
                        $response['messageCode']                  = 200;
                        $response['successCode']                  = 1;
                        $response['childproduct']                 = $productChieldres;
                        $response['parentproduct_price_per_unit'] = $childProduct[0]['price_per_unit'];
                        $response['productname']                  = $childProduct[0]['name'];
                        $response['unitname']                     = strtoupper($name);
                    } else {
                        $response['messageCode']  = 200;
                        $response['successCode']  = 1;
                        $response['childproduct'] = [];
                        $response['unitname']     = strtoupper($name);
                    }
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateProduct($data)
    {

        $error = array();
        if (!isset($data['product_id']) or empty($data['product_id'])) {
            $error['messageCode'] = 10005;
            $error['successCode'] = 1;
            $error['messageText'] = "Product Id should not be empty";
        } else if (!is_numeric($data['product_id'])) {
            $error['messageCode'] = 10006;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product Id";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    public function getProductChildrenPrice()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProductchieldren($data);

                if ($response['messageCode'] == 200) {

                    $productTable                  = TableRegistry::get('ProductChildren');
                    $product                       = $productTable->find()->where(['id' => $data['product_chield_id']])->select(['price'])->toArray();
                    $price                         = $product[0]['price'];
                    $response['messageCode']       = 200;
                    $response['successCode']       = 1;
                    $response['subscriptionprice'] = $price;
                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateProductchieldren($data)
    {

        $error = array();
        if (!isset($data['product_chield_id']) or empty($data['product_chield_id'])) {
            $error['messageCode'] = 10007;
            $error['successCode'] = 1;
            $error['messageText'] = "Product chieldren Id should not be empty";
        } else if (!is_numeric($data['product_chield_id'])) {
            $error['messageCode'] = 10008;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product chieldren Id";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    public function getProductPriceManually()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $response = $this->validateProductManually($data);

                if ($response['messageCode'] == 200) {

                    $productTable = TableRegistry::get('Products');
                    $product      = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'quantity'])->toArray();

                    $leftQty = $product[0]['quantity'];
                    if ($leftQty >= $data['quantity']) {
                        $price                         = $product[0]['price_per_unit'];
                        $totalPrice                    = ($price * $data['quantity']);
                        $response['messageCode']       = 200;
                        $response['successCode']       = 1;
                        $response['subscriptionprice'] = $totalPrice;
                    } else {

                        $response['statuscode']  = 10013;
                        $response['messageText'] = "Entered quantity not available";
                        $response['successCode'] = 1;
                    }

                }

            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateProductManually($data)
    {

        $error = array();
        if (!isset($data['product_id']) or empty($data['product_id'])) {
            $error['messageCode'] = 10009;
            $error['successCode'] = 1;
            $error['messageText'] = "Product Id should not be empty";
        } else if (!is_numeric($data['product_id'])) {
            $error['messageCode'] = 10010;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid Product Id";
        } else if (!isset($data['quantity']) or empty($data['quantity'])) {
            $error['messageCode'] = 10011;
            $error['successCode'] = 1;
            $error['messageText'] = "Product quantity should not be empty";
        } else if (!is_numeric($data['quantity'])) {
            $error['messageCode'] = 10012;
            $error['successCode'] = 1;
            $error['messageText'] = "Invalid quantity";
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    /*-------Add Subscription End WIll Here----------------------------------*/

    /*-----Return all Subscription List Start From Here --------------------------------*/

    /*Status code startv from 11001 From Here    */

    public function activeDeActiveSubscriptionList()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $usersSubscriptionTable = TableRegistry::get('UserSubscriptions');
                $usersSubscription      = $usersSubscriptionTable->find('all')->contain(['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses'])->where(['UserSubscriptions.user_id' => $data['user_id'], 'users_subscription_status_id <>' => 4])->hydrate(false)->toArray();
                //pr($usersSubscription);die;
                if (count($usersSubscription) > 0) {
                    $allSubscription               = $this->formatAllSubscription($usersSubscription);
                    $response['statuscode']        = 200;
                    $response['successCode']       = 1;
                    $response['subscriptionsList'] = $allSubscription;
                } else {
                    $response['statuscode']        = 11001;
                    $response['messageText']       = "Not Found Any Subscription For This Customer";
                    $response['successCode']       = 1;
                    $response['subscriptionsList'] = [];
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function formatAllSubscription($data)
    {
        $finalResponse = array();
        foreach ($data as $key => $value) {
            $d_s_is               = explode(',', $value['delivery_schdule_ids']);
            $deliverySchduleTable = TableRegistry::get('DeliverySchdules');
            $d_s_name             = array();
            foreach ($d_s_is as $k => $v) {
                $temp                            = array();
                $deliverySchdule                 = $deliverySchduleTable->find()->select(['name', 'start_time', 'end_time'])->where(['id' => $v])->first();
                $temp['ds_id']                   = $v;
                $temp['name']                    = $deliverySchdule['name'];
                $temp['delivery_timing_between'] = $deliverySchdule['start_time'] . '  To ' . $deliverySchdule['end_time'];
                $d_s_name[]                      = $temp;
            }
            $temp                       = array();
            $temp['subscriptionId']     = $value['id'];
            $temp['subscriptionTypeId'] = $value['subscription_type_id'];
            $temp['notes']              = $value['notes'];
            $temp['productName']        = $value['product']['name'];
            $temp['category_id']        = $value['product']['category_id'];
            $temp['productId']          = $value['product']['id'];
            $temp['image']              = @$value['product']['image'];
            if ($value['startdate']) {
                $temp['startdate'] = $value['startdate']->i18nFormat('dd-MM-yyyy HH:mm:ss');
                $temp['startdate'] = str_replace(' ', 'T', $temp['startdate']);
            } else {
                $temp['startdate'] = $value['startdate'];
            }
            if ($value['paused_since']) {
                $temp['SubscriptionsPausedSince'] = $value['paused_since']->i18nFormat('dd-MM-yyyy HH:mm:ss');
            } else {
                $temp['SubscriptionsPausedSince'] = $value['paused_since'];
            }
            if ($value['users_subscription_status']['name'] == 'active') {
                $temp['Subscriptionstatus'] = $value['users_subscription_status']['name'];
            } else if ($value['users_subscription_status']['name'] == 'paused') {
                $temp['Subscriptionstatus'] = $value['users_subscription_status']['name'];
            } else if ($value['users_subscription_status']['name'] == 'cancel') {
                $temp['Subscriptionstatus'] = $value['users_subscription_status']['name'];
            }
            $pro_unit                       = $this->getUnitNameAndId($value['product_id']);
            if($value['pro_child_id'] != 0){
                $unit                       = $this->getUnitNameAndIdchild($value['pro_child_id']);
                $temp['price_per_unit']         = $unit['price'];
                $temp['unitName']               = $unit['name'];
                $temp['productQuantity']        = $value['quantity'];
                $temp['child_package_qty']      = $value['child_package_qty'];
                $temp['product_unit']           = $pro_unit['name'];
                $temp['subscriptionTotalPrice'] = $unit['price']*$value['quantity'];
            }else{
                $temp['price_per_unit']         = $pro_unit['price'];
                $temp['unitName']               = $value['unit_name'];
                $temp['productQuantity']        = $value['quantity'];
                $temp['child_package_qty']      = 0;
                $temp['product_unit']           = $pro_unit['name'];
                $temp['subscriptionTotalPrice'] = $pro_unit['price']*$value['quantity'];
            }
            $temp['repeatOn']               = $value['subscription_type']['subscription_type_name'];
            $temp['deliverySchduleTimig']   = $d_s_name;
            $finalResponse[]                = $temp;

        }
        return $finalResponse;
    }

    /*--Return all Subscription List Start From Here---*/

    /*-----Change Subscription status START here --------------------------------*/

    /*Status code startv from 12001 From Here    */

    public function changeSubscriptionStatus()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateStatusChange($data);
                if ($response['messageCode'] == 200) {

                    $updatedFlag = $this->alreadyHaveThatStatus($data['subscription_id'], trim($data['status']));
                    if ($updatedFlag) {

                        $response['statuscode']  = 200;
                        $response['successCode'] = 1;
                        $response['messageText'] = 'subscription has been updated successfully';

                    } else {

                        $newUpdatedFlag = $this->updateSubscriptionStatus($data);
                        if ($newUpdatedFlag['messageCode'] == 200) {

                            $response['statuscode']  = 200;
                            $response['successCode'] = 1;
                            $response['messageText'] = 'subscription has been updated successfully';

                        } else {

                            $response['statuscode']  = 12005;
                            $response['successCode'] = 0;
                            $response['messageText'] = 'Something went wrong while status change';

                        }

                    }

                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function validateStatusChange($data)
    {

        $subscriptionStatus = ['active', 'cancel', 'paused'];
        $response           = array();
        if (!isset($data['status']) || empty($data['status'])) {

            $response['messageCode'] = 12001;
            $response['successCode'] = 0;
            $response['messageText'] = "Status value should not be empty";

        } else if (!in_array($data['status'], $subscriptionStatus)) {

            $response['messageCode'] = 12002;
            $response['successCode'] = 0;
            $response['messageText'] = "Wrong Status value. status value should be one out of ( 'active','cancel','paused' ) ";

        } else if (!isset($data['subscription_id']) || empty($data['subscription_id'])) {

            $response['messageCode'] = 12003;
            $response['successCode'] = 0;
            $response['messageText'] = "subscription id should not be empty";

        } else if (!is_numeric($data['subscription_id'])) {

            $response['messageCode'] = 12004;
            $response['successCode'] = 0;
            $response['messageText'] = "Invalid Subscription id";

        } else if ($this->subscriptionBelongsToCustomer($data['user_id'], $data['subscription_id'])) {

            $response['messageCode'] = 12005;
            $response['successCode'] = 0;
            $response['messageText'] = "Subscriptio does not associated with this customer";

        } else {
            $response['messageCode'] = 200;
        }
        return $response;

    }

    private function subscriptionBelongsToCustomer($user_id, $subscription_id)
    {

        $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
        $userSubscription      = $userSubscriptionTable->find()->where(['user_id' => $user_id, 'id' => $subscription_id])->count();
        if (!$userSubscription) {
            return true;
        }return false;

    }

    private function alreadyHaveThatStatus($subscription_id, $status)
    {

        $userSubscriptionStatusTable = TableRegistry::get('UsersSubscriptionStatuses');
        $userSubscriptionTable       = TableRegistry::get('UserSubscriptions');
        $userSubscriptionStatus      = $userSubscriptionStatusTable->find()->select(['id'])->where(['name' => $status])->first();
        $id                          = $userSubscriptionStatus['id'];
        $flagDbStatus                = $userSubscriptionTable->find()->where(['id' => $subscription_id, 'users_subscription_status_id' => $id])->count();
        if ($flagDbStatus) {
            return true;
        }return false;

    }

    private function updateSubscriptionStatus($data)
    {

        $usersSubscriptionStatusesTable                    = TableRegistry::get('UsersSubscriptionStatuses');
        $usersSubscriptionTable                            = TableRegistry::get('UserSubscriptions');
        $userSub                                           = $usersSubscriptionTable->find()->where(['UserSubscriptions.id' => $data['subscription_id']])->first();

        $unit_name = ""; $quantityy = 0; $product_name = ""; $price = 0;
        if(@$userSub['quantity_child'] != 0) {
            $quantityy                                         = $userSub['quantity']* $userSub['child_package_qty'];
            $unitname                                          = $this->getUnitNameAndIdchild($userSub['pro_child_id']);
            $product_name                                      = $unitname['product_name'];
            $unit_name                                         = $unitname['name'];
            $price                                             = $unitname['price']* @$userSub['quantity'];
        }else{
            $quantityy                                         = $userSub['quantity'];
            $unitname                                          = $this->getUnitNameAndId($userSub['product_id']);
            $product_name                                      = $unitname['product_name'];
            $unit_name                                         = $unitname['name'];
            $price                                             = $unitname['price']*$userSub['quantity'];
        }

        $user_subscription_active_deactive_histories_table = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
        $subscriptionId                                    = $data['subscription_id'];
        $newUpdatedFlag                                    = array();

        if (trim($data['status']) == 'active') {

            $usersSubscriptionStatusesOBJ                    = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'active'])->first();
            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;
            $usersSubscription->paused_since                 = '';
            $usersSubscription->delivery_date                = null;
            $usersSubscription->delivery_time                = null;
            $usersSubscription->modified_quantity            = null;

            if(@$data['product_id'] || @$data['pro_child_id']){
                if(@$data['pro_child_id'] && @$data['pro_child_id'] != 0) {
                    $unitname                                       = $this->getUnitNameAndIdchild(@$data['pro_child_id']);
                    $quantityy                                      = @$data['quantity']* $unitname['package_qty'];
                    $product_name                                   = $unitname['product_name'];
                    $unit_name                                      = $unitname['name'];
                    $price                                          = $unitname['price'];
                }else{
                    $quantityy                                      = @$data['quantity'];
                    $unitname                                       = $this->getUnitNameAndId(@$data['product_id']);
                    $product_name                                   = $unitname['product_name'];
                    $unit_name                                      = $unitname['name'];
                    $price                                          = $unitname['price'];
                }
                $usersSubscription->quantity                        = @$data['quantity'];
                $usersSubscription->subscriptions_total_amount      = @$price * @$data['quantity'];
                if(@$data['pro_child_id'] && @$data['pro_child_id'] != 0) {
                    $usersSubscription->child_package_qty           = @$quantityy;
                    $usersSubscription->quantity_child              = @$data['quantity_child'];
                    $usersSubscription->pro_child_id                = @$data['pro_child_id'];
                }
                $usersSubscription->pricr_per_package               = @$price;
                $usersSubscription->startdate                       = @$data['start_date'];

            }

            if ($usersSubscriptionTable->save($usersSubscription)) {
                $user_subscription_active_deactive_histories_table = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $query                                             = $user_subscription_active_deactive_histories_table->query();
                $result                                            = $query->update()
                    ->set(['play_date' => date('Y-m-d h:i:s')])
                    ->where(['user_subscription_id' => $subscriptionId, 'play_date IS NULL'])
                    ->execute();
                $description = "".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";
                $action      = "Resumed";
                $this->savedInActivity($data['user_id'], $description,$action);
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        } else if (trim($data['status']) == 'paused') {

            $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'paused'])->first();

            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;

            $usersSubscription->paused_since      = date('Y-m-d h:i:s');
            $usersSubscription->delivery_date     = null;
            $usersSubscription->delivery_time     = null;
            $usersSubscription->modified_quantity = null;

            if ($usersSubscriptionTable->save($usersSubscription)) {

                $user_subscription_active_deactive_histories_table                         = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $user_subscription_active_deactive_histories                               = $user_subscription_active_deactive_histories_table->newEntity();
                $user_subscription_active_deactive_histories->user_subscription_id         = $subscriptionId;
                $user_subscription_active_deactive_histories->paused_date                  = date('Y-m-d h:i:s');
                $user_subscription_active_deactive_histories->users_subscription_status_id = $usersSubscriptionStatusesId;
                $user_subscription_active_deactive_histories_table->save($user_subscription_active_deactive_histories);
                $description = "".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";
                $action      = "Paused";
                $this->savedInActivity($data['user_id'], $description,$action);
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        } else if (trim($data['status']) == 'cancel') {
            $usersSubscriptionStatusesOBJ = $usersSubscriptionStatusesTable->find()->select(['id'])->where(['UsersSubscriptionStatuses.name' => 'cancel'])->first();

            $usersSubscriptionStatusesId                     = $usersSubscriptionStatusesOBJ['id'];
            $usersSubscription                               = $usersSubscriptionTable->get($subscriptionId);
            $usersSubscription->users_subscription_status_id = $usersSubscriptionStatusesId;

            $usersSubscription->paused_since      = date('Y-m-d h:i:s');
            $usersSubscription->delivery_date     = null;
            $usersSubscription->delivery_time     = null;
            $usersSubscription->modified_quantity = null;

            if ($usersSubscriptionTable->save($usersSubscription)) {

                $user_subscription_active_deactive_histories_table                         = TableRegistry::get('UserSubscriptionActiveDeactiveHistories');
                $user_subscription_active_deactive_histories                               = $user_subscription_active_deactive_histories_table->newEntity();
                $user_subscription_active_deactive_histories->user_subscription_id         = $subscriptionId;
                $user_subscription_active_deactive_histories->paused_date                  = date('Y-m-d h:i:s');
                $user_subscription_active_deactive_histories->users_subscription_status_id = $usersSubscriptionStatusesId;
                $user_subscription_active_deactive_histories_table->save($user_subscription_active_deactive_histories);
                $description = "".$product_name .' ' .$quantityy .' '. $unit_name .' ([' .$subscriptionId."])";
                $action      = "Cancelled";
                $this->savedInActivity($data['user_id'], $description,$action);
                $newUpdatedFlag['messageCode'] = 200;
            } else {
                $newUpdatedFlag['messageCode'] = 201;
            }

        }

        return $newUpdatedFlag;

    }
/*----- Change Subscription status END here --------------------------------*/

    /*-----Add to Next Deliver START here --------------------------------*/

    /*Status code startv from 13001 From Here    */

    public function addToNextDelivery()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateAddNextDelivery($data);
                if ($response['messageCode'] == 200) {
                    $saveOrder = $this->saveOrder($data);
                    if ($saveOrder['messageCode'] == 200) {
                        $response['statuscode']  = 13014;
                        $response['successCode'] = 1;
                        $response['messageText'] = 'Product has been added successfully';
                    } else if ($saveOrder['messageCode'] == 3306) {
                        $response['statuscode']  = 13015;
                        $response['successCode'] = 0;
                        $response['messageText'] = 'This Order already made by this customer for next delivery';
                    }else if ($saveOrder['messageCode'] == 202) {
                        $response['statuscode']  = 13017;
                        $response['successCode'] = 0;
                        $response['messageText'] = 'Requested quantity is not in stock.';
                    } else {
                        $response['statuscode']  = 13016;
                        $response['successCode'] = 0;
                        $response['messageText'] = 'Something went wrong while save order';
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function saveOrder($data)
    {
        $res                    = array();
        $productChildrenTable   = TableRegistry::get('ProductChildren');
        $productTable           = TableRegistry::get('Products');
        $customordersTable      = TableRegistry::get('CustomOrders');
        $customorders           = $customordersTable->find()
            ->where(['user_id' => $data['user_id'], 'product_id' => $data['product_id'], 'status' => 0])->count();
        if ($customorders) {
            $res['messageCode'] = 3306;
        } else {
            $customorders             = $customordersTable->newEntity();
            $customorders->user_id    = $data['user_id'];
            $customorders->product_id = $data['product_id'];
            $customorders->quantity   = $data['quantity'];
            if (isset($data['is_child'])) {
                $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id', 'quantity','in_stock'])->where(['id' => $data['pro_child_id']])->hydrate(false)->first();

                if ($productChildren == "") {
                    $productTable          = TableRegistry::get('Products');
                    $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id','quantity'])->first();
                    if($products['quantity'] >= $data['quantity'] ){
                        $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                        $customorders->unit_id = $products['unit_id'];
                    }else{
                        $res['messageCode'] = 202;
                        return $res;
                    }

                } else {
                    if($productChildren['in_stock'] >= $data['quantity_child'] ){
                        $customorders->price                = $productChildren['price']*$data['quantity'];
                        $customorders->unit_id              = $productChildren['unit_id'];
                        $customorders->quantity             = $data['quantity_child'];
                        $customorders->quantity_child       = $data['quantity_child'];
                        $customorders->pro_child_id         = @$data['pro_child_id'];
                        $customorders->child_package_qty    = $productChildren['quantity'];
                        $customorders->pricr_per_package    = $productChildren['price'];
                    }else{
                        $res['messageCode'] = 202;
                        return $res;
                    }
                }
            } else {
                $productTable          = TableRegistry::get('Products');
                $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id','quantity'])->first();
                if($products['quantity'] >= $data['quantity'] ){
                    $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                    $customorders->unit_id = $products['unit_id'];
                }else{
                    $res['messageCode'] = 202;
                    return $res;
                }
            }

            $customorders->status   = 0;
            $customorders->created  = date('Y-m-d');
            $customorders->modified = date('Y-m-d h:i:s');
            $order_qty              = $customorders->quantity;
            if ($customordersTable->save($customorders)) {

                    $in_stock1   = $productTable->find()->where(['id' => $data['product_id']])->select(['quantity'])->first();

                    $query1      = $productTable->query();
                    $result1     = $query1->update()
                        ->set(['quantity' => $in_stock1['quantity']-$order_qty])
                        ->where(['id' => $data['product_id']])
                        ->execute();

                    $in_stock   = $productChildrenTable->find()->where(['id' => $data['pro_child_id']])->select(['in_stock'])->first();

                    $query      = $productChildrenTable->query();
                    $result     = $query->update()
                        ->set(['in_stock' => $in_stock['in_stock']-$order_qty])
                        ->where(['id' => $data['pro_child_id']])
                        ->execute();

                $res['messageCode'] = 200;
            } else {
                $res['messageCode'] = 201;
            }

        }
        return $res;
    }

    private function validateAddNextDelivery($data)
    {
        $response = array();
        if (!isset($data['product_id']) || empty($data['product_id'])) {
            $response['messageCode'] = 13002;
            $response['successCode'] = 0;
            $response['messageText'] = "Product id value should not be empty";

        } else if (!isset($data['unit_name']) || empty($data['unit_name'])) {

            $response['messageCode'] = 13003;
            $response['successCode'] = 0;
            $response['messageText'] = "Unit Name  should not be empty";

        }else if (!isset($data['order_price']) || empty($data['order_price'])) {

            $response['messageCode'] = 13005;
            $response['successCode'] = 0;
            $response['messageText'] = "Order Price should not be empty";

        }else if (!isset($data['quantity']) || empty($data['quantity'])) {

            $response['messageCode'] = 13007;
            $response['successCode'] = 0;
            $response['messageText'] = "Quantity should not be empty";

        } else if (!$this->existProduct(trim($data['product_id']))) {

            $response['messageCode'] = 13010;
            $response['successCode'] = 0;
            $response['messageText'] = "Product Id does not exist";

        }else if (!$this->existUnit(trim($data['unit_name']))) {

            $response['messageCode'] = 13011;
            $response['successCode'] = 0;
            $response['messageText'] = "Unit Name does not exist";

        }else if (!is_numeric($data['quantity']) || empty($data['quantity'])) {

            $response['messageCode'] = 13013;
            $response['successCode'] = 0;
            $response['messageText'] = "Invalid Quantity Value";

        } else if (!$this->getUserSubs($data['user_id'])) {
            $response['messageCode'] = 1972;
            $response['successCode'] = 0;
            $response['messageText'] = "You have not subscribed any product yet.";
        } else {
            $response['messageCode'] = 200;
        }
        return $response;

    }

    public function getUserSubs($userid)
    {
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $usersub                = $userSubscriptionsTable->find('all')
            ->where(['user_id' => $userid, 'users_subscription_status_id' => 1])->hydrate(false)->toArray();
        if (count($usersub) > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function existDst($id)
    {

        $deliverySchduleTable = TableRegistry::get('DeliverySchdules');
        $deliverySchdule      = $deliverySchduleTable->find()->where(['id' => $id])->count();
        if ($deliverySchdule) {
            return true;
        }return false;

    }

    private function existProduct($id)
    {

        $productTable = TableRegistry::get('Products');
        $products     = $productTable->find()->where(['id' => $id])->count();
        if ($products) {
            return true;
        }return false;

    }

    private function existUnit($name)
    {

        $unitTable = TableRegistry::get('Units');
        $unit      = $unitTable->find()->where(['name' => strtolower($name)])->count();
        if ($unit) {
            return true;
        }return false;

    }

    private function isValidDate($date)
    {

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return true;
        } else {
            return false;
        }
    }
    private function associatedWithProduct($pro_id, $qty, $ischield_set)
    {

        if (isset($ischield_set) && $ischield_set == 1) {
            $productChildrenTable = TableRegistry::get('ProductChildren');
            $productsTable        = TableRegistry::get('Products');
            $productChildren = $productChildrenTable->find()->where(['product_id' => $pro_id])->count();
            $products        = $productsTable->find()->where(['id' => $pro_id])->count();
            if ($productChildren || $products) {
                return true;
            }return false;
        } else if (isset($ischield_set) && $ischield_set != 1) {
            return false;
        } else {
            return true;
        }
    }

    /*-----CALENDRA EVENT START FROM HERE...... here --------------------------------*/

    /*Status code start from 14001 From Here    */

    public function getCalendraEvent()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $date     = date('Y-m-d');
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                if (!$this->isValidDate(trim($data['date']))) {
                    $response['messageCode'] = 14012;
                    $response['successCode'] = 0;
                    $response['messageText'] = "Date Format Should be ( YYYY-MM-dd )";
                } else {
                    $staus = $this->checkDateDeliveryOptions($data);
                    if (isset($staus) && count($staus) > 0) {
                        $response['statuscode']  = 14014;
                        $response['successCode'] = 1;
                        $response['data']        = $staus;
                    } else {
                        $response['statuscode']  = 14014;
                        $response['successCode'] = 1;
                        $response['data']        = [];
                    }
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function checkDateDeliveryOptions($data)
    {
        $nextDelivery   = $this->getBalanceAndDeliveryInfo($data, 'current');
        $Deliveryondate = $this->getBalanceAndDeliveryInfo($data, 'future');

        $today                = date('Y-m-d');
        $today_delivered_temp = array();
        $delivered            = array();
        $rejected             = array();
        $cuorders             = array();
        $customorders         = array();
        $cuordersid           = array();

        if (strtotime(@$data['date']) <= strtotime($today)) {
            $deliverySchduleTable = TableRegistry::get('DeliverySchdules');
            $productTable = TableRegistry::get('Products');
            $product      = $productTable->find()->hydrate(false)->toArray();
            $transtable = TableRegistry::get('Transactions');
            $trans      = $transtable->find('all')->where(['Transactions.created' => $data['date'], 'Transactions.transaction_type_id' => 6, 'Transactions.user_id' => $data['user_id']])->hydrate(false)->toArray();
            $usersub                    = array();
            if($trans){
                foreach ($trans as $keyt => $tvalue) {

                    $order_data = json_decode($tvalue['order_data'], TRUE);
                    foreach ($order_data as $keyd => $valued) {
                      if($valued['userId'] == $data['user_id']){
                        foreach ($valued['items'] as $keyv => $valuev) {
                            $d_s                        = $deliverySchduleTable->find()->where(['id'=>$tvalue['delivery_schdule_id']])->first();
                            if($valuev['type']== "subscription"){
                                $temp                   = array();
                                $temp['productName']    = $valuev['name'];
                                foreach ($product as $k1 => $v1) {
                                    if($v1['id'] == $valuev['id']){
                                      $temp['image']          = $v1['image'];
                                    }
                                }
                                $temp['unit']           = $valuev['unit_name'];
                                if(@$valuev['quantity_child'] != 0){
                                    $temp['quantity']       = @$valuev['quantity_child'];
                                    $temp['child_package_quantity']       = @$valuev['child_package_qty'];
                                    $temp['amount']         = @$valuev['pricr_per_package']*@$valuev['quantity_child'];

                                }else{
                                    $temp['quantity']       = $valuev['quantity'];
                                    $temp['child_package_quantity']       = 0;
                                    $temp['amount']         = $valuev['pro_price']*$valuev['quantity'];
                                }

                                if($valued['delivered']=='yes'){
                                    $temp['status']         = "Delivered";
                                    $temp['reason']         = "delivered";
                                }else{
                                    $temp['status']         = "RejectedOrders";
                                    $temp['reason']         = @$tvalue['rejected_reason'];
                                }
                                $temp['timing']             = $d_s['name'];
                                $temp['between']            = $d_s['start_time']."-".$d_s['end_time'];
                                $temp['deliverdate']        = $valued['date'];
                                $temp['customorders']       = array();
                                array_push($usersub, $temp);
                            }else{
                                $temp                   = array();
                                $temp['productName']    = $valuev['name'];
                                foreach ($product as $k1 => $v1) {
                                    if($v1['id'] == $valuev['id']){
                                      $temp['image']          = $v1['image'];
                                    }
                                }
                                $temp['unit']           = $valuev['unit_name'];
                                if(@$valuev['quantity_child'] != 0){
                                    $temp['quantity']       = @$valuev['quantity_child'];
                                    $temp['child_package_quantity']       = @$valuev['child_package_qty'];
                                    $temp['amount']         = @$valuev['pricr_per_package']*@$valuev['quantity_child'];

                                }else{
                                    $temp['quantity']       = $valuev['quantity'];
                                    $temp['child_package_quantity']       = 0;
                                    $temp['amount']         = $valuev['pro_price']*$valuev['quantity'];
                                }
                                if($valued['delivered']=='yes'){
                                    $temp['status']         = "Delivered";
                                    $temp['reason']         = "delivered";
                                }else{
                                    $temp['status']         = "RejectedOrders";
                                    $temp['reason']         = @$tvalue['rejected_reason'];
                                }
                                $temp['timing']             = $d_s['name'];
                                $temp['between']            = $d_s['start_time']."-".$d_s['end_time'];
                                $temp['deliverdate']        = $valued['date'];
                                //$customorders[]             = $temp;
                                array_push($customorders, $temp);
                            }
                        }

                        }
                    }
                }
                if(!empty($customorders)){
                    $usersub[0]['customorders'] = $customorders;
                }
                $response['subscriptions']                  = array();
                $response['subscriptions']                  = $usersub;
                return $response;
            }
        }

        if (strtotime(@$data['date']) >= strtotime($today)) {
            if (!empty($Deliveryondate)) {
                foreach ($Deliveryondate as $key => $value) {
                    if (!empty($nextDelivery) && isset($nextDelivery['orderItems']) && !empty($nextDelivery['orderItems'])) {
                        if (isset($nextDelivery['willDeliver']) && (strtotime($nextDelivery['willDeliver']) == strtotime($data['date']))
                            && isset($nextDelivery['deliver_schdule_id']) && $nextDelivery['deliver_schdule_id'] == $key) {

                            foreach ($nextDelivery['orderItems'] as $nk => $nv) {
                                $cuordersid[] = $nv['id'];
                            }

                            if (!empty($cuordersid)) {
                                $customordersTable = TableRegistry::get('CustomOrders');
                                $orders            = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.id IN' => $cuordersid])->hydrate(false)->toArray();
                                if (!empty($orders)) {
                                    foreach ($orders as $ok => $ov) {
                                        $temp                = array();
                                        $temp['quantity']    = $ov['quantity'];
                                        $temp['custom_id']   = $ov['id'];
                                        $temp['productName'] = @$ov['product']['name'];
                                        $temp['image']       = @$ov['product']['image'];
                                        $temp['unit']        = @$ov['unit']['name'];
                                        $temp['amount']      = $ov['price'];
                                        $temp['status']      = 'UPCOMING';
                                        $temp['timing']      = @$nextDelivery['subscriptionItems'][0]['timeToBeDeliver'];
                                        $temp['between']     = @$nextDelivery['subscriptionItems'][0]['between'];
                                        $temp['deliverdate'] = $data['date'];
                                        $temp['child_package_quantity']       = @$ov['child_package_qty'];
                                        $temp['sid']         = $key;
                                        $customorders[]      = $temp;
                                    }
                                }
                            }

                        }
                    }

                    if (isset($value['itemss'][0]['subscriptionInfo']) && !empty($value['itemss'][0]['subscriptionInfo'])) {
                        foreach ($value['itemss'][0]['subscriptionInfo'] as $k => $v) {
                            $temp                    = array();
                            $temp['quantity']        = $v['quantity'];
                            $temp['subscription_id'] = $v['subscription_id'];
                            $temp['productName']     = $v['name'];
                            $temp['image']           = @$v['image'];
                            $temp['unit']            = $v['unit'];
                            $temp['amount']          = $v['price'];
                            if (in_array($v['subscription_id'], $delivered)) {
                                $temp['status'] = 'DELIVERED';
                            } else if (in_array($v['subscription_id'], $rejected)) {
                                $temp['status'] = 'REJECTED';
                            } else {
                                $temp['status'] = 'UPCOMING';
                            }
                            $temp['timing']      = $v['timeToBeDeliver'];
                            $temp['between']     = $v['between'];
                            $temp['deliverdate'] = $data['date'];
                            $temp['sid']         = $key;

                            $temp['customorders']   = array();
                            $today_delivered_temp[] = $temp;
                        }
                    }
                }
                if (isset($today_delivered_temp[0]['customorders']) && !empty($customorders)) {
                    $today_delivered_temp[0]['customorders'] = $customorders;

                }
                $response['subscriptions'] = $today_delivered_temp;
                return $response;
            } else {
                $response['subscriptions'] = array();
                return $response;
            }
        } else {
            $response['subscriptions'] = array();
            return $response;
        }
    }

    private function checkifcustomordersdelivered($user_id, $delivery_schdule_id, $date)
    {

        $todaydate         = date('Y-m-d');
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.user_id' => $user_id]);

        $custom_orders = array();
        foreach ($customorders as $key => $value) {
            $temp = array();

            $temp['quantity'] = $value['quantity'];

            if ($value['status'] == 1 && $todaydate >= $date) {

                $temp['order_status'] = 'DELIVERED';

            } else if ($value['status'] == 0 && $todaydate > $date) {
                $temp['order_status'] = 'NOTDELIVERED';
            } else if ($value['status'] == 0 && $todaydate <= $date) {
                $temp['order_status'] = 'UPCOMING';
            }

            $temp['custom_order_id'] = $value['id'];
            $temp['productName']     = $value['product']['name'];
            $temp['unit']            = $value['unit']['name'];
            $temp['amount']          = $value['price'];
            $custom_orders[]         = $temp;

        }

        return $custom_orders;

    }

    private function checkifcustomordersnotdelivered($user_id, $delivery_schdule_id, $date)
    {

        $todaydate         = date('Y-m-d');
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find('all')->contain(['Products', 'Units'])->where(['CustomOrders.user_id' => $user_id, 'CustomOrders.created' => $date, 'CustomOrders.delivery_schdule_id' => $delivery_schdule_id, 'CustomOrders.status' => 0]);

        $custom_orders = array();
        foreach ($customorders as $key => $value) {
            $temp             = array();
            $temp['quantity'] = $value['quantity'];

            if ($value['status'] == 1 && $todaydate > $date) {

                $temp['order_status'] = 'DELIVERED';

            } else if ($value['status'] == 0 && $todaydate > $date) {
                $temp['order_status'] = 'NOTDELIVERED';
            } else if ($value['status'] == 0 && $todaydate <= $date) {
                $temp['order_status'] = 'UPCOMING';
            }

            $temp['custom_order_id'] = $value['id'];
            $temp['custom_order_id'] = $value['id'];
            $temp['productName']     = $value['product']['name'];
            $temp['unit']            = $value['unit']['name'];
            $temp['amount']          = $value['price'];
            $custom_orders[]         = $temp;
        }

        return $custom_orders;

    }

    private function checkFromRouteCustomer($user_id, $d_s_id, $date, $subid = null)
    {
        date_default_timezone_set('Asia/Kolkata');
        $routeCustomerTable   = TableRegistry::get('RouteCustomers');
        $rejected_ordersTable = TableRegistry::get('RejectedOrders');
        $scheduleTable        = TableRegistry::get('DeliverySchdules');
        $status               = array();
        /*    202   for delivered    and 200  for not delivered---*/

        $transtable = TableRegistry::get('Transactions');
        $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type' => 'Dr', 'Transactions.created' => $date])->hydrate(false)->toArray();
        $subids     = array();
        if (!empty($trans)) {
            $subids = explode(',', $trans[0]['user_subscription_ids']);
        }
        $routeCustomer = $scheduleTable->find()->where(['id' => $d_s_id])->hydrate(false)->first();
        if ($routeCustomer) {
            $sname = $routeCustomer['name'];
        } else {
            $sname = '';
        }

        $datetoday = date('Y-m-d');
        if (in_array($subid, $subids)) {
            $status['statuscode'] = 202;
            $status['time']       = $sname;
            return $status;
        }
        $status['statuscode'] = 200;
        $status['time']       = $sname;
        return $status;
    }

    private function checkalternateday($startdate, $provide_date)
    {

        $startdate       = $startdate->i18nFormat('YYY-MM-dd');
        $subscriptionday = strtotime($startdate);
        $now             = strtotime($provide_date);
        $datediff        = $now - $subscriptionday;
        $days            = floor($datediff / (60 * 60 * 24));
        if ($days % 2 == 0) {
            $tomorrow = $provide_date;

        } else {
            $tomorrow = date('Y-m-d', strtotime($provide_date . "+1 days"));
        }
        return $tomorrow;

    }

    /*Status code startv from 14001 From Here    */

    private function emptyToken($userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->get($userid);

        $user->token    = '';
        $user->modified = date('Y-m-d h:i:s');
        if ($userTable->save($user)) {
            return true;
        } else {
            return false;
        }

    }

    public function logout()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data  = $this->request->getData();
            $error = $this->validateupdateProfileCustomer($data);
            if ($error['messageCode'] == 200) {
                    $error['messageText'] = "Logged out Successfully";
                    $error['messageCode'] = 200;
                    $error['successCode'] = 1;
            }
        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }

    public function gettransactions()
    {
        if ($this->request->is('post')) {

            $data             = $this->request->getData();
            $user_id          = $data['user_id'];
            $startdate        = @$data['start_date'];
            $enddate          = @$data['end_date'];
            $transactionTable = TableRegistry::get('Transactions');
            if ($startdate && $enddate) {
                $transactions = $transactionTable->find('all')->contain(['TransactionTypes'])->order(['Transactions.id' => 'desc'])->where(['Transactions.user_id' => $user_id, 'Transactions.created >=' => $startdate, 'Transactions.created <=' => $enddate])->toArray();
            } else {
                $transactions = $transactionTable->find('all')->contain(['TransactionTypes'])->order(['Transactions.id' => 'desc'])->where(['Transactions.user_id' => $user_id])->toArray();
            }

            $final_array    = array();
            $check_in_array = array();
            $before_final   = array();
            $checkarray     = array();

            foreach ($transactions as $key => $value) {

                $userTable     = TableRegistry::get('Users');
                $transactions1 = $userTable->find('all')->contain(['Regions', 'Areas'])->where(['Users.id' => $user_id])->toArray();
                foreach ($transactions1 as $key1 => $value1) {

                    $temp                            = array();
                    $temp['transaction_id']          = $value['id'];
                    $temp['amount']                  = $value['amount'];
                    $temp['created']                 = $value['created'];
                    $temp['time']                    = $value['time'];
                    $temp['transaction_amount_type'] = $value['transaction_amount_type'];
                    $temp['transaction_type']        = $value['transaction_type']['transaction_type_name'];
                    $temp['transaction_type_id']        = $value['transaction_type_id'];
                    $status                          = $value['status'];
                    if(@$value['transaction_type_id'] == 6 ){
                        if ($status == 1) {
                            $temp['status'] = "delivered";
                        } else {
                            $temp['status'] = "cancelled";
                        }
                    }else if(@$value['transaction_type_id'] == 4 ){
                        $temp['status'] = "Credited";
                    }else if(@$value['transaction_type_id'] == 2 ){

                        $order_data     = json_decode($value['order_data'],true);
                        if ($status == 1) {
                            $temp['status'] = "success";
                            $temp['TXNID'] = $order_data['TXNID'];
                        } else {
                            $temp['status'] = "failed";
                            $temp['TXNID'] = $order_data['TXNID'];
                        }
                    }else if(@$value['transaction_type_id'] == 8 ){

                        $order_data     = json_decode($value['order_data'],true);
                        $temp['transaction_type']        = 'Online Recharge' ;
                        if ($status == 1) {
                            $temp['status'] = "success";                       
                            $temp['TXNID'] = $value['online_transaction_id'];
                        } else {
                            $temp['status'] = "failed";
                            $temp['TXNID'] = $value['online_transaction_id'];
                        }
                    }else{
                        $temp['status'] = $value['transaction_type']['transaction_type_name'];
                    }
                    $final_array[] = $temp;

                }
            }

            $response['messageText']  = "success";
            $response['messageCode']  = 200;
            $response['successCode']  = 1;
            $response['count']        = count($final_array);
            $response['transactions'] = $final_array;
            echo json_encode($response);die;
        }

    }

    public function aboutus()
    {
        $staticPagesTable = TableRegistry::get('StaticPages');
        $about_desc       = $staticPagesTable->find('all')->select(['title', 'description'])->where(['id' => 1])->toArray();

        $response['messageText'] = "success";
        $response['messageCode'] = 200;
        $response['successCode'] = 1;
        $response['content']     = $about_desc;
        echo json_encode($response);die;

    }

    public function paytm()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            // following files need to be included
            require_once ROOT . '/paytm' . DS . 'lib' . DS . 'config_paytm.php';
            require_once ROOT . '/paytm' . DS . 'lib' . DS . 'encdec_paytm.php';

            $checkSum  = "";
            $paramList = array();

            $ORDER_ID         = $data["ORDER_ID"];
            $CUST_ID          = $data["CUST_ID"];
            $INDUSTRY_TYPE_ID = $data["INDUSTRY_TYPE_ID"];
            $CHANNEL_ID       = $data["CHANNEL_ID"];
            $TXN_AMOUNT       = $data["TXN_AMOUNT"];

            // Create an array having all required parameters for creating checksum.
            $paramList["MID"]              = PAYTM_MERCHANT_MID;
            $paramList["ORDER_ID"]         = $ORDER_ID;
            $paramList["CUST_ID"]          = $CUST_ID;
            $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
            $paramList["CHANNEL_ID"]       = $CHANNEL_ID;
            $paramList["TXN_AMOUNT"]       = $TXN_AMOUNT;
            $paramList["WEBSITE"]          = PAYTM_MERCHANT_WEBSITE;

            $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
            echo "<html>
    <head>
    <title>Merchant Check Out Page</title>
    </head>
    <body>
        <center><h1>Please do not refresh this page...</h1></center>
            <form method='post' action='" . PAYTM_TXN_URL . "' name='f1'>
    <table border='1'>
     <tbody>";

            foreach ($paramList as $name => $value) {
                echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
            }

            echo "<input type='hidden' name='CHECKSUMHASH' value='" . $checkSum . "'>
     </tbody>
    </table>
    <script type='text/javascript'>
     document.f1.submit();
    </script>
    </form>
    </body>
    </html>";

        }
    }

    public function payumoney(){
        // Merchant key here as provided by Payu
        $MERCHANT_KEY = "1fY9an9t";
        // Merchant Salt as provided by Payu
        $SALT = "BFwFkAgPrm";
        // End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL = "https://secure.payu.in";
        $action = '';
        $posted = array();
        if(!empty($_POST)) {
            //print_r($_POST);
          foreach($_POST as $key => $value) {
            $posted[$key] = $value;

          }
        }

        $formError = 0;

        if(empty($posted['txnid'])) {
          // Generate random transaction id
          $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
          $txnid = $posted['txnid'];
        }
        $hash = '';
        // Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash']) && sizeof($posted) > 0) {
          if(
                  empty($posted['key'])
                  || empty($posted['txnid'])
                  || empty($posted['amount'])
                  || empty($posted['firstname'])
                  || empty($posted['email'])
                  || empty($posted['phone'])
                  || empty($posted['productinfo'])
                  || empty($posted['surl'])
                  || empty($posted['furl'])
                  || empty($posted['service_provider'])
          ) {
            $formError = 1;
          } else {
            $posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';
            foreach($hashVarsSeq as $hash_var) {
              $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
              $hash_string .= '|';
            }

            $hash_string .= $SALT;
            $hash = strtolower(hash('sha512', $hash_string));
            $action = $PAYU_BASE_URL . '/_payment';
          }
        } elseif(!empty($posted['hash'])) {
          $hash = $posted['hash'];
          $action = $PAYU_BASE_URL . '/_payment';
        }
        $this->set(compact('action','hash','hash_string','MERCHANT_KEY','SALT','PAYU_BASE_URL','formError','txnid','posted'));
    }
    public function payumoney1()
    {
        $data                                       = $_POST;
        $response                                   = array();
        $usertable                                  = TableRegistry::get('Users');
        $user                                       = $usertable->find()->select(['area_id'])->where(['Users.id'=>$data['udf1']])->hydrate(false)->first();

        $UserBalancesTable                          = TableRegistry::get('UserBalances');
        $UserBalances                               = $UserBalancesTable->find()->where(['user_id' => $data['udf1']])->select('balance')->first();
        $transactionTable                           = TableRegistry::get('Transactions');
        $transactions                               = $transactionTable->newEntity();
        $transactions->user_id                      = $data['udf1'];
        $transactions->transaction_amount_type      = "Cr";
        $transactions->amount                       = $data['amount'];
        $transactions->onlinetransactionhistory     = $data['status'];
        $transactions->order_data                   = json_encode($data);
        $transactions->created                      = substr($data['addedon'],0,10);
        $transactions->time                         = substr($data['addedon'],11,8);
        if ($data['status'] === "success") {
            $transactions->status                   = 1;
            if(count($UserBalances) > 0){
                $transactions->balance              = $UserBalances['balance']+$data['amount'];
            }else{
                $transactions->balance              = $data['amount'];
            }
        } else {
            $transactions->status                   = 0;
            if(count($UserBalances) > 0){
                $transactions->balance               = $UserBalances['balance'];
            }else{
                $transactions->balance               = 0.00;     
            }
        }
        $transactions->delivery_schdule_id          = 0;
        $transactions->transaction_type_id          = 8;
        $transactions->refund_type_id               = 0;
        $transactions->users_subscription_status_id = 1;
        $transactions->payment_gateway_type         = "payumoney";
        $transactions->online_transaction_id        = $data['payuMoneyId'];
        $transactions->area_id                      = $user['area_id'];
        $result = $transactionTable->save($transactions);
        if ($result)
        {
            if(count($UserBalances) > 0){

                $balanceamount     = $UserBalances['balance'];
                $balanceamount     = $balanceamount + $data['amount'];
                $query   = $UserBalancesTable->query();
                $result1 = $query->update()
                    ->set(['balance' => $balanceamount])
                    ->where(['user_id' => $data['udf1']])
                    ->execute();
            }else{
                $userbal = $UserBalancesTable->newEntity();
                $userbal->user_id        = $data['udf1'];
                if ($data['status'] === "success") {
                    $userbal->balance        = $data['amount'];
                }else{
                    $userbal->balance        = 0.00;
                }
                $userbal->thresholdmoney = 0.00;
                $UserBalancesTable->save($userbal);
            }
            $response['messageText'] = "success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
            $response['message']     = "Your Transaction has been saved successfully.";
            if ($data['status'] === "success") {
                $this->Flash->success(__('Transaction added successfully.'));
                return $this->redirect(['controller' => 'CustomerApi', 'action' => 'success']);
            }else{
                $this->Flash->success(__('Some error occured.'));
                return $this->redirect(['controller' => 'CustomerApi', 'action' => 'failure']);
            }
        }
    }
    public function success()
    {
        $response['messageText'] = "success";
        $response['messageCode'] = 200;
        $response['successCode'] = 1;
        $response['message']     = "Your Transaction has been saved successfully.";
        echo json_encode($response); die;
    }
    public function failure()
    {
        $response['messageText'] = "failure";
        $response['messageCode'] = 201;
        $response['successCode'] = 0;
        $response['message']     = "Your Transaction has been failed. Please try again later.";
        echo json_encode($response); die;
    }
    public function payuwebhook()
    {
        $data1          = $_POST;
        //echo 200; die;
        $data           = json_decode($data1,TRUE);
        $UserBalancesTable = TableRegistry::get('UserBalances');
        $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $data['udf1']])->select('balance')->first();
        $transactionTable = TableRegistry::get('Transactions');
        $transaction      = $transactionTable->find()->where(['online_transaction_id'=> $data['paymentId']])->first();
        if(count($transaction)>0){
           if($transaction['status'] == 0 && $data['status'] == 'success' ){
                $query1   = $transactionTable->query();
                $result1 = $query1->update()
                    ->set(['balance' => $UserBalances['balance']+$data['amount'],'status'=> 1])
                    ->where(['id' => $transaction['id']])
                    ->execute();

                $query   = $UserBalancesTable->query();
                $result1 = $query->update()
                    ->set(['balance' => $UserBalances['balance']+$data['amount']])
                    ->where(['user_id' => $data['udf1']])
                    ->execute();
           }
        }else{
            $usertable                                  = TableRegistry::get('Users');
            $user                                       = $usertable->find()->select(['area_id'])->where(['Users.id'=>$data['udf1']])->hydrate(false)->first();
            $transactions                               = $transactionTable->newEntity();
            $transactions->user_id                      = $data['udf1'];
            $transactions->transaction_amount_type      = "Cr";
            $transactions->amount                       = $data['amount'];
            $transactions->onlinetransactionhistory     = $data['status'];
            $transactions->order_data                   = json_encode($data);
            $transactions->created                      = date('Y-m-d');
            $transactions->time                         = date('H:i:s');
            if ($data['status'] === "success") {
                $transactions->status                   = 1;
                if(count($UserBalances) > 0){
                    $transactions->balance              = $UserBalances['balance']+$data['amount'];
                }else{
                    $transactions->balance              = $data['amount'];
                }
            } else {
                $transactions->status                   = 0;
                if(count($UserBalances) > 0){
                    $transactions->amount               = $UserBalances['balance'];
                }else{
                    $transactions->amount               = 0.00;
                }
            }
            $transactions->delivery_schdule_id          = 0;
            $transactions->transaction_type_id          = 8;
            $transactions->refund_type_id               = 0;
            $transactions->users_subscription_status_id = 1;
            $transactions->payment_gateway_type         = "payumoney";
            $transactions->online_transaction_id        = $data['paymentId'];
            $transactions->area_id                      = $user['area_id'];
            $result = $transactionTable->save($transactions);
            if ($result)
            {
                if(count($UserBalances) > 0){
                    $balanceamount     = $UserBalances['balance'];
                    $balanceamount     = $balanceamount + $data['amount'];
                    $query   = $UserBalancesTable->query();
                    $result1 = $query->update()
                        ->set(['balance' => $balanceamount])
                        ->where(['user_id' => $data['udf1']])
                        ->execute();
                }else{
                    $userbal = $UserBalancesTable->newEntity();
                    $userbal->user_id        = $data['udf1'];
                    if ($data['status'] === "success") {
                        $userbal->balance        = $data['amount'];
                    }else{
                        $userbal->balance        = 0.00;
                    }
                    $userbal->thresholdmoney = 0.00;
                    $UserBalancesTable->save($userbal);
                }
            }
        }
        /*$file       = IMG_PATHQ.'payumoney/response.log';
        $current    = file_get_contents($file);
        $current    .= $data1;
        file_put_contents($file, $current);*/
        $response   = array();
        $calltable  = TableRegistry::get('Callbacks');
        $call       = $calltable->newEntity();
        $call->data = $data1;
        $calltable->save($call);
        echo 200; die;
    }

/* paytm */
    public function createchecksum()
    {

        if ($this->request->is('post')) {

            $data = $this->request->getData();
            require_once "lib/config_paytm.php";
            require_once "lib/encdec_paytm.php";
            $checkSum = "";

            // below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
            $findme     = 'REFUND';
            $findmepipe = '|';

            $paramList = array();

            $paramList["MID"]                       = $data["MID"];
            $paramList["ORDER_ID"]                  = $data["ORDER_ID"];
            $paramList["CUST_ID"]                   = $data["CUST_ID"];
            $paramList["INDUSTRY_TYPE_ID"]          = $data["INDUSTRY_TYPE_ID"];
            $paramList["CHANNEL_ID"]                = $data["CHANNEL_ID"];
            $paramList["TXN_AMOUNT"]                = $data["TXN_AMOUNT"];
            $paramList["PAYTM_MERCHANT_WEBSITEITE"] = $data["WEBSITE"];

            foreach ($_POST as $key => $value) {
                $pos     = strpos($value, $findme);
                $pospipe = strpos($value, $findmepipe);
                if ($pos === false || $pospipe === false) {
                    $paramList[$key] = $value;
                }
            }

            //Here checksum string will return by getChecksumFromArray() function.
            $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
            //print_r($_POST);
            echo json_encode(array("CHECKSUMHASH" => $checkSum, "ORDER_ID" => $data["ORDER_ID"], "payt_STATUS" => "1"));
            die;
            //Sample response return to SDK

            //  {"CHECKSUMHASH":"GhAJV057opOCD3KJuVWesQ9pUxMtyUGLPAiIRtkEQXBeSws2hYvxaj7jRn33rTYGRLx2TosFkgReyCslu4OUj\/A85AvNC6E4wUP+CZnrBGM=","ORDER_ID":"asgasfgasfsdfhl7","payt_STATUS":"1"}
        }
    }

   public function verifychecksum()
    {
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");
        // following files need to be included
        require_once("../webroot/paytm/lib/config_paytm.php");
        require_once("../webroot/paytm/lib/encdec_paytm.php");
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = FALSE;
        $paramList      = $_POST;
        $return_array   = $_POST;
        $paytmChecksum  = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : "ghgfhfh";


        $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

        if ($isValidChecksum===TRUE){
            $return_array["IS_CHECKSUM_VALID"] = "Y";
        }else{
            $return_array["IS_CHECKSUM_VALID"] = "N";
        }
        $return_array["IS_CHECKSUM_VALID"] = $isValidChecksum ? "Y" : "N";
        $return_array["TXNTYPE"] = "";
        $return_array["REFUNDAMT"] = "";
        unset($return_array["CHECKSUMHASH"]);
        $encoded_json = htmlentities(json_encode($return_array));
        $data = json_decode($encoded_json, true);
        $udata = explode("-", $return_array['ORDERID']);
        $return_array['user_id'] = $udata[1];
        $res   = json_decode($this->checkstatus($return_array['ORDERID']), true);
        $return_array['STATUS'] = isset($res['STATUS']) ? $res['STATUS'] : $return_array['STATUS'];
        //pr($return_array);
        $recharge = $this->addtransaction($return_array);
        $this->set(compact('encoded_json'));
    }

/*  paytm */

    public function paytmurl()
    {

        $paytmTable         = TableRegistry::get('Paytms');
        $paytm              = $paytmTable->newEntity();
        $paytm->description = "New paytm transaction";
        $result             = $paytmTable->save($paytm);
        echo "inserted";
        die;

    }
    public function checkstatus($order_id =null){
        $paytm_url =HTTP_ROOT.'webroot/paytm/TxnStatus.php';
        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $paytm_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "ORDER_ID=" . $order_id);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
    }
    public function checkpaytmtransactions($userId=null){

        $current_date   = date('Y-m-d');
        $third_day = date('Y-m-d', strtotime('-3 days', strtotime($current_date)));
        //die;
        $transtable = TableRegistry::get('Transactions');
        if($userId){
            $transactions      = $transtable->find('all')->where(['Transactions.transaction_amount_type' => 'Cr', 'Transactions.transaction_type_id' => 2, 'Transactions.status' => 0, 'Transactions.user_id' => $userId, function ($exp) use ($current_date, $third_day) {
                    return $exp->between('Transactions.created', $third_day, $current_date);
                } ])->hydrate(false)->toArray();
        }else{
            $transactions      = $transtable->find('all')->where(['Transactions.transaction_amount_type' => 'Cr', 'Transactions.transaction_type_id' => 2, 'Transactions.status' => 0, function ($exp) use ($current_date, $third_day) {
                    return $exp->between('Transactions.created', $third_day, $current_date);
                } ])->hydrate(false)->toArray();
        }
        foreach ($transactions as $key => $value) {
            $paytm_data = json_decode($value['order_data'],true);
            $checkstatus      = json_decode($this->checkstatus($paytm_data['ORDERID']), true);
            if ($checkstatus['STATUS'] === "TXN_SUCCESS") {
                $status = 1;
            }else {
                $status = 0;
            }
            $order_data = json_encode($paytm_data);
            if($checkstatus['STATUS'] == "TXN_SUCCESS" && $value['status'] == 0){
                /*save in duplicate transaction table */
                $orderdata      = json_decode($transdata->order_data, true);
                $orderdata['STATUS']    = $orderdata['STATUS']."->".$checkstatus['STATUS'];
                $transdata      = $transtable->get($value['id']);
                $duplicate      = TableRegistry::get('DuplicateTransactions');
                $data           = $duplicate->newEntity();
                $data -> transaction_id = $transdata->id;
                $data -> user_id = $transdata->user_id;
                $data -> transaction_amount_type = $transdata->transaction_amount_type;
                $data -> amount = $transdata->amount;
                $data -> onlinetransactionhistory = $transdata->onlinetransactionhistory;
                $data -> created = $transdata->created;
                $data -> time = $transdata->time;
                $data -> syc_time = $transdata->syc_time;
                $data -> status = $transdata->status;
                $data -> rejected = $transdata->rejected;
                $data -> rejected_reason = $transdata->rejected_reason;
                $data -> delivery_schdule_id = $transdata->delivery_schdule_id;
                $data -> transaction_type_id = $transdata->transaction_type_id;
                $data -> refund_type_id = $transdata->refund_type_id;
                $data -> users_subscription_status_id = $transdata->users_subscription_status_id;
                $data -> payment_gateway_type = $transdata->payment_gateway_type;
                $data -> order_data = json_encode($orderdata);
                $data -> online_transaction_id = $transdata->online_transaction_id;
                $data -> user_subscription_ids = $transdata->user_subscription_ids;
                $data -> custom_order_ids = $transdata->custom_order_ids;
                $data -> balance = $transdata->balance;
                $data -> products_ids = $transdata->products_ids;
                $data -> area_id = $transdata->area_id;
                $data -> transaction_month = $transdata->transaction_month;
                $data -> modified = $transdata->modified;
                $data -> updatedDate = date('Y-m-d H:i:s');
                $duplicate->save($data);
                /*save in duplicate transaction table */

                $query = $transtable->query();
                $query->update()
                    ->set(['order_data' => $order_data, 'status'=> $status, 'balance' => $value['balance'] + $checkstatus['TXNAMOUNT']])
                    ->where(['user_id' => $value['user_id'], 'online_transaction_id'=> $value['online_transaction_id']])
                    ->execute();
                $UserBalancesTable = TableRegistry::get('UserBalances');
                $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $value['user_id']])->select('balance')->first();
                $balanceamount = $UserBalances['balance'] + $checkstatus['TXNAMOUNT'] ;
                $query1   = $UserBalancesTable->query();
                $query1->update()
                    ->set(['balance' => $balanceamount])
                    ->where(['user_id' => $value['user_id']])
                    ->execute();


                $transaction1      = $transtable->find()->where(['user_id' => $value['user_id'],'id >'=> $value['id']])->hydrate(false)->toArray();
                foreach ($transaction1 as $key1 => $value1) {
                    /*save in duplicate transaction table */
                $transdata1 = $transtable->get($value1['id']);
                $duplicate1  = TableRegistry::get('DuplicateTransactions');
                $data1 = $duplicate1->newEntity();
                $data1 -> transaction_id = $transdata1->id;
                $data1 -> user_id = $transdata1->user_id;
                $data1 -> transaction_amount_type = $transdata1->transaction_amount_type;
                $data1 -> amount = $transdata1->amount;
                $data1 -> onlinetransactionhistory = $transdata1->onlinetransactionhistory;
                $data1 -> created = $transdata1->created;
                $data1 -> time = $transdata1->time;
                $data1 -> syc_time = $transdata1->syc_time;
                $data1 -> status = $transdata1->status;
                $data1 -> rejected = $transdata1->rejected;
                $data1 -> rejected_reason = $transdata1->rejected_reason;
                $data1 -> delivery_schdule_id = $transdata1->delivery_schdule_id;
                $data1 -> transaction_type_id = $transdata1->transaction_type_id;
                $data1 -> refund_type_id = $transdata1->refund_type_id;
                $data1 -> users_subscription_status_id = $transdata1->users_subscription_status_id;
                $data1 -> payment_gateway_type = $transdata1->payment_gateway_type;
                $data1 -> order_data = $transdata1->order_data;
                $data1 -> online_transaction_id = $transdata1->online_transaction_id;
                $data1 -> user_subscription_ids = $transdata1->user_subscription_ids;
                $data1 -> custom_order_ids = $transdata1->custom_order_ids;
                $data1 -> balance = $transdata1->balance;
                $data1 -> products_ids = $transdata1->products_ids;
                $data1 -> area_id = $transdata1->area_id;
                $data1 -> transaction_month = $transdata1->transaction_month;
                $data1 -> modified = $transdata1->modified;
                $data1 -> updatedDate = date('Y-m-d H:i:s');
                $duplicate->save($data1);
                /*save in duplicate transaction table */
                    //echo "<br>case2";
                    $query2 = $transtable->query();
                    $query2->update()
                        ->set(['balance' => $value1['balance']+ $checkstatus['TXNAMOUNT']])
                        ->where(['id' => $value1['id']])
                        ->execute();
                }
            }
        }

        $response['messageText'] = "success";
        $response['messageCode'] = 200;
        $response['successCode'] = 1;
        $response['message']     = "Your Transactions has been updated successfully.";
        echo json_encode($response); die;
    }
    public function paytmstatus(){
        if ($this->request->is('post')) {
            $data = $_POST;
            $this->log('Transaction data:-' . json_encode($data), 'debug');
            $user_id = $data['user_id'];
            $transactionTable = TableRegistry::get('Transactions');
            $transaction      = $transactionTable->find()->where(['user_id' => $data['user_id'],'online_transaction_id'=> $data['TXNID']])->first();
            $checkstatus      = json_decode($this->checkstatus($data['ORDERID']), true);
            if(count($transaction) > 0){
                if ($data['STATUS'] === "TXN_SUCCESS") {
                    $status = 1;
                }else {
                    $status = 0;
                }
                $order_data = json_encode($data);
                if($checkstatus['STATUS'] == "TXN_SUCCESS" && $transaction['status'] == 0){

                    $query = $transactionTable->query();
                    $query->update()
                        ->set(['order_data' => $order_data, 'status'=> $status, 'balance' => $transaction['balance'] + $checkstatus['TXNAMOUNT']])
                        ->where(['user_id' => $user_id, 'online_transaction_id'=> $data['TXNID']])
                        ->execute();
                    if($transaction['status'] == 0 && $data['STATUS'] === "TXN_SUCCESS"){
                        $UserBalancesTable = TableRegistry::get('UserBalances');
                        $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $data['user_id']])->select('balance')->first();
                        $balanceamount = $UserBalances['balance'] + $checkstatus['TXNAMOUNT'] ;
                        $query1   = $UserBalancesTable->query();
                        $query1->update()
                            ->set(['balance' => $balanceamount])
                            ->where(['user_id' => $data['user_id']])
                            ->execute();

                    }
                    $transaction1      = $transactionTable->find()->where(['user_id' => $data['user_id'],'id >'=> $transaction['id']])->hydrate(false)->toArray();
                    foreach ($transaction1 as $key => $value) {
                        $query2 = $transactionTable->query();
                        $query2->update()
                            ->set(['balance' => $value['balance']+ $checkstatus['TXNAMOUNT']])
                            ->where(['id' => $value['id']])
                            ->execute();
                    }
                }
                $response['messageText'] = "success";
                $response['messageCode'] = 200;
                $response['successCode'] = 1;
                $response['message']     = "Your Transaction has been updated successfully.";
                $response['order_data']     = $data;
                echo json_encode($response); die;
            }else{
               $recharge = $this->addtransaction($data);
               if($recharge['messageText'] == "success" ){
                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['message']     = $recharge['message'];
                    $response['order_data']     = $data;
               }else{
                    $response['messageText'] = "failure";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                    $response['message']     = "Something went wrong";
                    $response['order_data']  = $data;
               }
               echo json_encode($response); die;
            }
        }
    }

    public function addtransaction($data= null)
    {
        date_default_timezone_set('Asia/Kolkata');
        //$response = $this->validateupdateProfileCustomer($data);

        //if ($response['messageCode'] == 200) {
            $UsersTable = TableRegistry::get('Users');
            $Users      = $UsersTable->find()->where(['id' => $data['user_id']])->select('area_id')->first();
            $UserBalancesTable = TableRegistry::get('UserBalances');
            $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $data['user_id']])->select('balance')->first();

            //$data1 = json_decode($data['res'], true);
            $transactionTable                       = TableRegistry::get('Transactions');
            $transactions                           = $transactionTable->newEntity();
            $transactions->user_id                  = $data['user_id'];
            $transactions->transaction_amount_type  = "Cr";
            $transactions->amount                   = isset($data['TXNAMOUNT']) ? $data['TXNAMOUNT'] : 0;
            $transactions->onlinetransactionhistory = isset($data['RESPMSG']) ? $data['RESPMSG'] : 0;
            $transactions->created                  = isset($data['TXNDATE']) ? substr($data['TXNDATE'],0,10) : date('Y-m-d');
            $transactions->time                     = isset($data['TXNDATE']) ? substr($data['TXNDATE'],11,8) : date('H:i:s');

            if ($data['STATUS'] === "TXN_SUCCESS") {
                $transactions->status = 1;
            } else {
                $transactions->status = 0;
            }
            $transactions->delivery_schdule_id          = 0;
            $transactions->transaction_type_id          = 2;
            $transactions->refund_type_id               = 0;
            $transactions->users_subscription_status_id = 1;
            $transactions->payment_gateway_type         = "Paytm";
            $transactions->order_data                   = json_encode($data);
            $transactions->online_transaction_id        = isset($data['TXNID']) ? $data['TXNID'] : 0;
            $transactions->area_id                      = $Users['area_id'];
            if (count($UserBalances) > 0 && $data['STATUS'] === "TXN_SUCCESS") {
                $transactions->balance                  = $UserBalances['balance'] + $data['TXNAMOUNT'];
            }else if (count($UserBalances) > 0 && $data['STATUS'] != "TXN_SUCCESS") {
                $transactions->balance                  = $UserBalances['balance'];
            }else{
               $transactions->balance                   = isset($data['TXNAMOUNT']) ? $data['TXNAMOUNT'] : 0;
            }

            $result = $transactionTable->save($transactions);

            if (count($UserBalances) > 0) {
                $balanceamount = $UserBalances['balance'];
                if ($data['STATUS'] === "TXN_SUCCESS") {
                    $balanceamount = $balanceamount + $data['TXNAMOUNT'];
                    $message       = "Rs. " . $data["TXNAMOUNT"] . " has been added to your account. Transaction ID: ".$result->id .' DATE: '.date('d-m-Y').' '.substr($data['TXNDATE'],11,8).' Current a/c balance: Rs.'.$balanceamount.'. Welcome to the life of health and convenience. Welcome to the CREMEWAY LIFE';
                } else {
                    $balanceamount = $balanceamount;
                    $message       = "Sorry your transaction has been failed.";
                }
                $query   = $UserBalancesTable->query();
                $result1 = $query->update()
                    ->set(['balance' => $balanceamount])
                    ->where(['user_id' => $data['user_id']])
                    ->execute();

            } else {
                if ($data['STATUS'] === "TXN_SUCCESS") {
                    $userbal = $UserBalancesTable->newEntity();
                    $userbal->user_id        = $data['user_id'];
                    $userbal->balance        = $data['TXNAMOUNT'];
                    $userbal->thresholdmoney = 0.00;
                    $UserBalancesTable->save($userbal);

                    $message       = "Rs. " . $data["TXNAMOUNT"] . " has been added to your account. Transaction ID: ".$result->id .' DATE: '.date('d-m-Y').' '.$transaction->time.' Current a/c balance: Rs.'.$data['TXNAMOUNT'].'. Welcome to the life of health and convenience. Welcome to the CREMEWAY LIFE';
                }else {
                    $userbal = $UserBalancesTable->newEntity();
                    $userbal->user_id        = $data['user_id'];
                    $userbal->balance        = 0.00;
                    $userbal->thresholdmoney = 0.00;
                    $UserBalancesTable->save($userbal);
                    $balanceamount = $data['TXNAMOUNT'];
                    $message       = "Sorry your transaction has been failed.";
                }
            }

            if ($result) {
                $push1 = $this->addpush($data['user_id'], $message);
                $push  = $this->pushnotifications($data['user_id'], $message);
                $UsersTable = TableRegistry::get('Users');
                $User      = $UsersTable->find()->where(['id' => $data['user_id']])->select('phoneNo')->first();
                $this->sendsms($User['phoneNo'],$message);
                // $email = $this->sendmail($data['user_id'], $message);
                if ($data['STATUS'] === "TXN_SUCCESS") {
                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['message']     = "Your Transaction has been saved successfully.";
                    $response['order_data']     = $data;
                }else{
                    $response['messageText'] = "success";
                    $response['messageCode'] = 202;
                    $response['successCode'] = 2;
                    $response['message']     = "Sorry your transaction has been failed.";
                    $response['order_data']     = $data;
                }
            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
                $response['message']     = "Error processing your request.";
            }

        /*} else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }*/
        return $response;


    }

    public function getorderhistory()
    {
        if ($this->request->is('post')) {

            $data    = $this->request->getData();
            $user_id = $data['user_id'];
            $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
            $UserBalances1          = $userSubscriptionsTable->find()->where(['user_id' => $data['user_id']]);

        }
    }

    public function getnotifications()
    {
        $this->loadComponent('Paginator');
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = array();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $UserNotificationsTable = TableRegistry::get('UserNotifications');
                $this->paginate         = [
                    'limit'      => 20,
                    'conditions' => ['user_id IN' => array($data['user_id'], 'null')],
                    'order'      => ['id desc'],
                ];
                $UserNotifications = $this->paginate($UserNotificationsTable)->toArray();

                if ($UserNotifications) {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['successCode']   = 1;
                    $response['notifications'] = $UserNotifications;

                } else {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['successCode']   = 1;
                    $response['notifications'] = "No notifications";
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);
            die;

        }
    }

    private function calculateContainers($globalQty, $product_id)
    {

        $response     = array();
        $productTable = TableRegistry::get('Products');
        $iscontainer  = $productTable->find()->where(['id' => $product_id, 'iscontainer' => 1])->count();

        if ($iscontainer > 0) {
            $containerRuleTable = TableRegistry::get('ContainerRules');
            $containerRule      = $containerRuleTable->find('all')->where(['subscription_qty' => $globalQty])->toArray();
            $totalConatiners    = 0;
            if (count($containerRule)) {
                foreach ($containerRule as $key => $value) {

                    $totalConatiners = $totalConatiners + $value['container_count'];
                }
            }

            $response['containers'] = $totalConatiners;
            $response['status']     = 200;
            return $response;die;
        } else {
            $response['status'] = 404;
            return $response;die;
        }

    }

    public function updateusersubscriptions()
    {
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            //next subscription
            if ($response['messageCode'] == 200) {
                $user_id                    = @$data['user_id'];
                $subscription_id            = @$data['subscription_id'];
                $subscriptions_total_amount = @$data['total_price'];
                $subscription_type_id       = @$data['subscription_type_id'];
                if (!$subscription_type_id) {
                    $response['messageText'] = "Subscription id is not valid.";
                    $response['messageCode'] = 1962;
                    $response['successCode'] = 0;
                    echo json_encode($response);die;
                }
                $ds_ids = array($data['delivery_schdule_ids']);
                $delivery_schdule_ids = $data['delivery_schdule_ids'];
                $quantity   = $data['quantity'];
                $product_id = $data['product_id'];

                $unit_name  = '';
                $product_name = '';
                $quantity_a   = '';
                $prochildTable = TableRegistry::get('ProductChildren');
                $prochildpackage = $prochildTable->find()->select(['quantity'])->where(['id' => @$data['pro_child_id']])->first();
                if(@$data['pro_child_id'] != 0 && @$data['pro_child_id'] != ''){
                    $unitname       = $this->getUnitNameAndIdchild(@$data['pro_child_id']);
                    $product_name   = $unitname['product_name'];
                    $unit_name      = $unitname['name'];
                    $quantity_a       = $quantity * @$prochildpackage['quantity'];
                }else{
                    $unitname       = $this->getUnitNameAndId($product_id);
                    $product_name   = $unitname['product_name'];
                    $unit_name      = $unitname['name'];
                }

                if (isset($data['notes']) && $data['notes']) {
                    $notes = $data['notes'];
                } else {
                    $notes = '';
                }
                $UserSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                $sub                    = $UserSubscriptionsTable->find()->where(['id' => $subscription_id])->first();
                if(@$sub['pro_child_id'] != 0 && @$sub['pro_child_id'] != '' ){
                    $quantity_old           = $sub['quantity'] * $sub['child_package_qty'];
                }else{
                    $quantity_old           = $sub['quantity'];
                }
                if (!$sub) {
                    $response['messageText'] = "Subscription id is not valid.";
                    $response['messageCode'] = 1962;
                    $response['successCode'] = 0;
                    echo json_encode($response);die;
                }
                $prochildTable = TableRegistry::get('ProductChildren');
                $prochildpackage = $prochildTable->find()->select(['quantity'])->where(['id' => @$data['pro_child_id']])->first();
                $query = $UserSubscriptionsTable->query();
                $query->update()
                    ->set(['quantity' => $quantity, 'delivery_schdule_ids' => $delivery_schdule_ids, 'subscriptions_total_amount' => $subscriptions_total_amount, 'notes' => $notes, 'subscription_type_id' => $subscription_type_id, 'delivery_date' => null, 'delivery_time' => null, 'modified_quantity' => null, 'pro_child_id'=> @$data['pro_child_id'], 'child_package_qty'=> @$prochildpackage['quantity']])
                    ->where(['id' => $subscription_id])
                    ->execute();

                if ($query) {
                    $this->updateintoRoute($user_id, $ds_ids);
                    $updatecontainer = $this->calculateContainers($quantity, $product_id);
                    if ($updatecontainer['status'] == 200) {
                        $userContainerTable = TableRegistry::get('UserContainers');
                        $query1             = $userContainerTable->query();
                        $query1->update()
                            ->set(['container_given' => $updatecontainer['containers']])
                            ->where(['user_id' => $user_id])
                            ->execute();

                        $subContainerTable = TableRegistry::get('SubscribedContainers');
                        $query2            = $subContainerTable->query();
                        $query2->update()
                            ->set(['container_count' => $updatecontainer['containers']])
                            ->where(['user_id' => $user_id])
                            ->execute();
                    }
                    $description = "".$product_name .' from '. $quantity_old.' '.$unit_name.' to ' .$quantity_a .' '. $unit_name .' ([' .$subscription_id."])";
                    $action      = "Updated";
                    $this->savedInActivity($data['user_id'], $description,$action);
                    $response['messageText'] = "Subscription updated";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                }
            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function addfeedback()
    {
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $feedbackTable              = TableRegistry::get('ComplaintFeedbackSuggestions');
                $newfeedback                = $feedbackTable->newEntity();
                $newfeedback->subject       = $data['subject'];
                $newfeedback->content       = $data['message'];
                $newfeedback->user_id       = $data['user_id'];
                $newfeedback->date          = date('Y-m-d');
                if(@$data['product_id']){
                    $newfeedback->product_id    = @$data['product_id'];
                }
                $newfeedback->type          = $data['type'];
                $newfeedback->status        = 0;
                $result                     = $feedbackTable->save($newfeedback);

                if ($result) {
                    $email                   = $this->sendmail($data['user_id'], $data['message'],$data['subject']);
                    $response['messageText'] = "Your complaint is saved.";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;

        }
    }

    public function feedbacklist()
    {
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $feedbackTable = TableRegistry::get('ComplaintFeedbackSuggestions');
                $result        = $feedbackTable->find()->order(['id' => 'desc'])->where(['user_id' => $data['user_id']])->toArray();

                if ($result) {
                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 0;
                    $response['feedback']    = $result;
                }
            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    private function updatecoupon($coupon_code,$user_id)
    {
        $CouponTrackersTable = TableRegistry::get('CouponTrackers');
        $query               = $CouponTrackersTable->query();
        $result              = $query->update()->set(['use_status' => 1,'user_id'=> $user_id])->where(['s_c_c' => $coupon_code])->execute();
        if ($result) {return true;}
    }
    private function addcoupon($user_id, $amount)
    {
        date_default_timezone_set('Asia/Kolkata');
        $transactionTable                           = TableRegistry::get('Transactions');
        $transactions                               = $transactionTable->newEntity();
        $transactions->user_id                      = $user_id;
        $transactions->transaction_amount_type      = "Cr";
        $transactions->amount                       = $amount;
        $transactions->created                      = date('Y-m-d');
        $transactions->time                         = date('H:i:s');
        $transactions->status                       = 1;
        $transactions->delivery_schdule_id          = 0;
        $transactions->transaction_type_id          = 3;
        $transactions->refund_type_id               = 0;
        $transactions->users_subscription_status_id = 1;
        $result                                     = $transactionTable->save($transactions);

        $UserBalancesTable = TableRegistry::get('UserBalances');
        $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $user_id])->select('balance')->first();
        if($UserBalances){
            $balanceamount     = $UserBalances['balance'];
            $balanceamount     = $balanceamount + $amount;
            $query   = $UserBalancesTable->query();
            $result1 = $query->update()
                ->set(['balance' => $balanceamount])
                ->where(['user_id' => $user_id])
                ->execute();
        }else{
            $balanceamount            = $amount;
            $userbalance              = $UserBalancesTable->newEntity();
            $userbalance->balance     = $amount;
            $userbalance->user_id     = $user_id;
            $result1                  = $UserBalancesTable->save($userbalance);

        }


        if ($result1) {
            $message = "Rs. " . $amount . " has been added to your account successfully";
            //$push = $this->pushnotifications($amount,$message);

            $response['messageCode']        = 200;
            $response['transaction_id']     = $result->id;
            $response['current_balance']    = $balanceamount;
            $response['tr_time']            = $transactions->time;


        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
            $response['message']     = "Error processing your request.";
        }
        return $response;
    }

    public function reedemcoupon()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!@$data['coupon_code']) {
                $response['messageText'] = "Coupon Code is required.";
                $response['messageCode'] = 1987;
                $response['successCode'] = 0;
                echo json_encode($response);die;
            }
            $coupon   = $data['coupon_code'];
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {
                $coupon              = $data['coupon_code'];
                $coupontrackersTable = TableRegistry::get('CouponTrackers');

                $result  = $coupontrackersTable->find()->where(['s_c_c' => $coupon, 'use_status' => 0])->toArray();
                $result1 = $coupontrackersTable->find()->where(['s_c_c' => $coupon, 'use_status' => 1])->toArray();
                if (count($result) > 0) {
                    $coupon_id = $result[0]['coupon_id'];

                    $couponsTable = TableRegistry::get('Coupons');
                    $couponresult = $couponsTable->find()->where(['id' => $coupon_id])->toArray();
                    $amount      = $couponresult[0]['price_value'];
                    $currentDate = date('Y-m-d');
                    $startDate   = date('Y-m-d', strtotime($couponresult[0]['start_date']));

                    $endDate = date('Y-m-d', strtotime($couponresult[0]['end_date']));

                    if (($currentDate >= $startDate) && ($currentDate <= $endDate)) {
                        $res = $this->addcoupon($data['user_id'], $amount);
                        if ($res['messageCode'] == 200) {
                            $res1    = $this->updatecoupon($data['coupon_code'],$data['user_id']);
                            //$message = "Rs. " . $amount . " has been added by Coupon " . $data['coupon_code'] . " to your account successfully";
                            $message = "Rs.".$amount.' added to your account. Transaction ID: '. $res['transaction_id'] .' DATE: '.date('d-m-Y').' '.$res['tr_time'].' Current a/c balance: Rs.'.$res['current_balance'].'. Welcome to the life of health and convenience. Welcome to the CREMEWAY LIFE';
                            if ($res1) {
                                $push1 = $this->addpush($data['user_id'], $message);
                                $userTable = TableRegistry::get('Users');
                                $userdata      = $userTable->find()->where(['Users.id' => $data['user_id']])->hydrate(false)->first();
                                if($userdata){
                                    $phone = @$userdata['phoneNo'];
                                    if($phone){
                                        $this->sendSMS($phone,$message);
                                    }
                                }
                            }
                            $description    = "Recharged by coupon.";
                            $this->savedInActivity($data['user_id'], $description);
                            $response['messageText'] = "Coupon value credited to your account";
                            $response['messageCode'] = 200;
                            $response['successCode'] = 1;
                        }

                    } else {
                        $response['messageText'] = "Coupon has expired";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 0;
                    }

                } else if (count($result1) > 0) {
                    $response['messageText'] = "Coupon Already Used";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;

                } else {
                    $response['messageText'] = "Wrong Coupon Code";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                }

            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function balancenotifications()
    {

        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {

                $checkNextDeliveryTime = $this->checkNextDelivery($data);
                $totalPrice            = $this->getPrice($checkNextDeliveryTime);
                if ($checkNextDeliveryTime) {

                    $subscriptionOrderTotalPrice = $totalPrice;
                    $customerBalance             = $this->updatedCustomersBalance($data['user_id']);
                    if ($customerBalance < $subscriptionOrderTotalPrice) {
                        $message = "Your account balance is low.";
                        $push1   = $this->addpush($data['user_id'], $message);
                        $response['messageText'] = "error";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 1;
                        $response['message']     = "Your account balance is low.";
                    } else {
                        $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['Balance']     = $customerBalance;
                    }
                }

            } else {

                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function pushnotificationstoall()
    {
        $message   = "Daily notification message.";
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->select(['device_id'])->where(['device_id IS NOT' => null])->toArray();
        foreach ($user as $key => $value) {
            $userid = $value['device_id'];

            $push1 = $this->addpush1($userid, $message);
        }
        die;

    }

    public function getdailymilk()
    {

        $response = array();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $usersub               = $userSubscriptionTable->find('all')->where(['product_id' => 86, 'users_subscription_status_id' => 1, 'startdate <=' => date('Y-m-d')])->select(['id', 'subscription_type_id', 'product_id', 'delivery_schdule_ids', 'quantity', 'users_subscription_status_id', 'user_id', 'subscriptions_total_amount'])->toArray();
            $milktotal             = 0;
            $i                     = 0;
            foreach ($usersub as $key => $value) {
                $milk      = $value['quantity'];
                $milktotal = $milktotal + $milk;
                $milk1     = $value['subscription_type_id'];
                if ($milk1 = 30) {$i++;}
            }
            echo $i;
            echo "   ";
            echo $milktotal;
            echo "   ";
            echo json_encode($usersub);

        }
        die;

    }

    private function updateintoRoute($user_id, $delivery_schdule_ids)
    {
        $usersubscription = TableRegistry::get('UserSubscriptions');
        $subscriptions    = $usersubscription->find('all')->where(['user_id' => $user_id])->select(['delivery_schdule_ids'])->hydrate(false)->toArray();
        $dsids            = array();
        foreach ($subscriptions as $key => $value) {
            $dids = explode(',', $value['delivery_schdule_ids']);
            foreach ($dids as $val) {
                if (!in_array($val, $dsids)) {
                    array_push($dsids, $val);
                }
            }
        }

        //get all route customer for a user
        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        $routeCustomer      = $routeCustomerTable->find('all')->where(['user_id' => $user_id])->hydrate(false)->toArray();

        foreach ($routeCustomer as $key => $value) {
            if (in_array($value['delivery_schdule_id'], $dsids)) {
                continue;
            } else {
                $croutes = $routeCustomerTable->find('all')->where(['route_id' => $value['route_id']])->hydrate(false)->toArray();
                if (count($croutes) == 1) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => null])
                        ->where(['id' => $value['id']])
                        ->execute();
                } else {
                    $routcus = TableRegistry::get('RouteCustomers');
                    $entity  = $routcus->get($value['id']);
                    $result  = $routcus->delete($entity);
                }
            }
        }
        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['id' => $user_id])->select(['area_id', 'region_id'])->hydrate(false)->first();
        $region     = $users['region_id'];
        $area       = $users['area_id'];

        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        foreach ($delivery_schdule_ids as $key => $value) {
            $d_s_id        = $value;
            $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id <>' => $user_id])->hydrate(false)->toArray();
            if (count($routeCustomer) > 0) {
                $routeCustomers                      = $routeCustomerTable->newEntity();
                $routeCustomers->user_id             = $user_id;
                $routeCustomers->route_id            = $routeCustomer[0]['route_id'];
                $routeCustomers->delivery_schdule_id = $d_s_id;
                $routeCustomers->position            = '';
                $routeCustomers->date                = '2017-01-01';
                $routeCustomers->status              = 0;
                $routeCustomers->region_id           = $region;
                $routeCustomers->area_id             = $area;
                $routeCustomerTable->save($routeCustomers);
            } else {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $d_s_id, 'region_id' => $region, 'area_id' => $area, 'user_id IS NULL'])->hydrate(false)->toArray();
                if (count($routeCustomer) > 0) {
                    $query  = $routeCustomerTable->query();
                    $result = $query->update()
                        ->set(['user_id' => $user_id])
                        ->where(['id' => $routeCustomer[0]['id']])
                        ->execute();
                }
            }
        }
        return true;
    }

    public function deletecustomorder()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);

            if ($response['messageCode'] == 200) {

                $customordersTable = TableRegistry::get('CustomOrders');
                $entity            = $customordersTable->get($data['order_id']);

                $result = $customordersTable->delete($entity);

                if ($result) {
                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['message']     = "Order has been deleted successfully.";
                } else {
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                    $response['message']     = "Something went wrong.";
                }

            } else {
                $response['messageCode'] = 1053;
                $response['successCode'] = 0;
                $response['message']     = "Invalid User";
            }

            echo json_encode($response);die;
        }
    }

    public function getAllSubscriptions()
    {
        $response      = array();
        $subscriptions = array();
        $sub_status    = array(1, 3);
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable              = TableRegistry::get('Users');
                $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                $allUserSubOrder        = $userTable->find('all')->contain([
                    'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) use ($sub_status) {
                        return $query->where(['UserSubscriptions.users_subscription_status_id IN' => $sub_status]);
                    },
                ])->where(['Users.id' => $data['user_id']])->hydrate(false)->toArray();
                if (isset($allUserSubOrder[0]['user_subscriptions']) && !empty($allUserSubOrder[0]['user_subscriptions'])) {
                    foreach ($allUserSubOrder[0]['user_subscriptions'] as $key => $value) {
                        $temp                         = array();
                        $temp['subscription_id']      = $value['id'];
                        $temp['subscription_type_id'] = $value['subscription_type_id'];
                        $temp['statusId']             = $value['users_subscription_status_id'];
                        $temp['quantity']             = $value['quantity'];
                        $temp['price_per_unit']       = $value['product']['price_per_unit'];
                        $temp['productId']            = $value['product']['id'];
                        $temp['productName']          = $value['product']['name'];
                        $temp['image']                = @$value['product']['image'];
                        $temp['description']          = @$value['product']['description'];
                        $temp['unit']                 = $value['unit_name'];
                        $temp['amount']               = $value['subscriptions_total_amount'];
                        if($value['startdate']){
                            $temp['startdate']            = $value['startdate']->i18nFormat('dd-MM-YYY');
                        } else {
                            $temp['startdate'] = $value['startdate'];
                        }

                        $temp['daterange']            = $this->getDateRange($value['startdate']->i18nFormat('YYY-MM-dd'), $temp['subscription_type_id']);
                        $temp['sid']                  = $value['delivery_schdule_ids'];
                        $subscriptions[]              = $temp;
                    }
                }
                $response['messageText']   = "success";
                $response['messageCode']   = 200;
                $response['successCode']   = 1;
                $response['subscriptions'] = $subscriptions;
            } else {
                $response['messageCode'] = 1053;
                $response['successCode'] = 0;
                $response['message']     = "Invalid User";
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    public function getDateRange($startdate, $typeid)
    {
        $range = array();
        if ($typeid == 30) {
            $p = 'P2D';
        } else {
            $p = 'P1D';
        }

        $period = new \DatePeriod(
            new \DateTime($startdate),
            new \DateInterval($p),
            new \DateTime(END_DATE)
        );

        $i = 0;
        foreach ($period as $date) {
            if(strtotime($date->format('Y-m-d')) < strtotime(date('Y-m-d'))){
                continue;
            }
            $i++;
            $range[] = $date->format("d-m-Y");
        }
        return $range;
    }

    public function contactus()
    {
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable     = TableRegistry::get('Users');
                $user          = $userTable->find('all')->where(['id' => $data['user_id']])->toArray();
                $areasTable    = TableRegistry::get('Areas');
                $area          = $areasTable->find('all')->select(['manager_name', 'manager_phoneNo', 'manager_email'])->where(['id' => $user[0]['area_id']])->toArray();
                $contactsTable = TableRegistry::get('Contacts');
                $contacts      = $contactsTable->find('all')->toArray();

                $response['messageText']  = "Success";
                $response['messageCode']  = 200;
                $response['successCode']  = 1;
                $response['area_manager'] = $area;
                $response['Contacts']     = $contacts;
            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function settings()
    {
        if ($this->request->is('get')) {
            $settingsTable = TableRegistry::get('Settings');
            $settings      = $settingsTable->find()->where(['identifier' => $_GET['identifier']])->hydrate(false)->first();
            if (!empty($settings)) {
                $time = $settings['configuration'];
            } else {
                $time = 0;
            }
            $response['messageText'] = "Success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
            $response['time']        = $time;
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    public function getProfile()
    {

        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            if ($response['messageCode'] == 200) {
                $userTable = TableRegistry::get('Users');
                $user      = $userTable->find('all')->contain(['Regions', 'Areas'])->where(['Users.id' => $data['user_id']])->toArray();

                $response['messageText'] = "Success";
                $response['messageCode'] = 200;
                $response['successCode'] = 1;
                $response['profile']     = $user;
            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);die;
        }
    }

    public function getProfileInfo($id = null)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->contain(['Regions', 'Areas'])->where(['Users.id' => $id])->hydrate(false)->toArray();

        $orderTrackingTable = TableRegistry::get('OrderTrackings');
        $orderTracking      = $orderTrackingTable->find()->where(['user_id' => $id])->hydrate(false)->first();
        if (!empty($user)) {
            $lat = @$orderTracking['lat'];
            $lng = @$orderTracking['lng'];
        } else {
            $lat = '';
            $lng = '';
        }
        if (empty($user)) {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        } else {
            $user[0]['latitude']     = $lat;
            $user[0]['longitude']    = $lng;
            $response['messageText'] = "Success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
            $response['profile']     = $user;
        }

        echo json_encode($response);die;
    }

    public function changeOrder()
    {
        $response = array();
        $data     = $this->request->getData();
        $response = $this->validateupdateProfileCustomer($data);
        $temp     = array();
        if ($response['messageCode'] == 200) {
            $nextDelivery                        = $this->getBalanceAndDeliveryInfo($data, 'current');
            $temp['messageCode']                 = $nextDelivery['messageCode'];
            $temp['messageText']                 = $nextDelivery['messageText'];
            $temp['successCode']                 = $nextDelivery['successCode'];
            $temp['userstatus']                  = $nextDelivery['userstatus'];
            $temp['subscriptionOrderTotalPrice'] = $nextDelivery['subscriptionOrderTotalPrice'];
            $temp['customerBalance']             = $nextDelivery['customerBalance'];
            $temp['deliver_schdule_id']          = $nextDelivery['deliver_schdule_id'];
            $temp['willDeliver']                 = $nextDelivery['willDeliver'];
            $temp['subscriptionItems']           = array_merge($nextDelivery['subscriptionItems'], $nextDelivery['orderItems']);

        } else {
            $temp['messageText'] = "Invalid Request";
            $temp['messageCode'] = 201;
            $temp['successCode'] = 0;
        }
        echo json_encode($temp);die;

    }

    public function versions(){
        $settingsTable = TableRegistry::get('Settings');
        $settings      = $settingsTable->find()->where(['identifier' => 'ConsumerAppVersion'])->hydrate(false)->last();
        if($settings){
            $temp['messageText'] = "Success";
            $temp['messageCode'] = 200;
            $temp['successCode'] = 1;
            $temp['App_info'] = $settings;
        }else{
            $temp['messageText'] = "failure";
            $temp['messageCode'] = 201;
            $temp['successCode'] = 0;
            $temp['App_info'] = 'No App info found';
        }
        echo json_encode($temp); die;
    }
    public function getbalance(){
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            $temp = array();
            if ($response['messageCode'] == 200) {
                $userBalancesTable = TableRegistry::get('UserBalances');
                $balance      = $userBalancesTable->find()->select(['balance'])->where(['user_id' => $data['user_id']])->hydrate(false)->first();
                if($balance){
                    $temp['messageText'] = "Success";
                    $temp['messageCode'] = 200;
                    $temp['successCode'] = 1;
                    $temp['balance']     = number_format((float)$balance['balance'], 2, '.', '');
                }else{
                    $temp['messageText'] = "Success";
                    $temp['messageCode'] = 200;
                    $temp['successCode'] = 1;
                    $temp['balance'] = 0;
                }
            }else{
                $temp['messageText'] = "Invalid details";
                $temp['messageCode'] = 201;
                $temp['successCode'] = 0;
            }
            echo json_encode($temp); die;
        }
    }
    public function referAfriend(){
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileCustomer($data);
            $temp = array();
            if ($response['messageCode'] == 200) {
                $userTable  = TableRegistry::get('Users');
                $user       = $userTable->find()->where(['id' => $data['user_id']])->select(['name'])->first();

                $subject    = "Referral";
                $message    = $user['name']." has referred ". $data['name'] .". Contact No.:- ".  $data['phone_no'];
                $email      = new Email('default');
                ;
                if ( $email->from(['cremeway@gmail.com' => 'Cremeway'])
                ->to('cremeway@gmail.com')
                ->subject($subject)
                ->send($message) ) {
                    $temp['messageText'] = "Success";
                    $temp['messageCode'] = 200;
                    $temp['successCode'] = 1;
                } else {
                    $temp['messageText'] = "Something went wrong";
                    $temp['messageCode'] = 202;
                    $temp['successCode'] = 0;
                }
            }else{
                $temp['messageText'] = "Invalid details";
                $temp['messageCode'] = 201;
                $temp['successCode'] = 0;
            }
            echo json_encode($temp); die;
        }
    }

    public function transactiondetails(){
        if ($this->request->is('post')) {
            $response   = array();
            $data       = $this->request->getData();
            $response   = $this->validateupdateProfileCustomer($data);
            $temp       = array();
            if ($response['messageCode'] == 200) {
                $transactionTable = TableRegistry::get('Transactions');
                $transaction = $transactionTable->find()->contain(['RefundTypes'])->where(['Transactions.id'=> $data['transaction_id']])->hydrate(false)->first();
                if($transaction['transaction_amount_type']=="Dr" && $transaction['transaction_type_id']== 6){
                    $order_data = json_decode($transaction['order_data'], true);
                    $temp2 = array();
                    foreach ($order_data as $key => $value) {
                        if($value['userId'] == $data['user_id'] ){
                          $temp['delivered']    = $value['delivered'];
                          foreach ($value['items'] as $ki => $vi) {
                            if($vi['quantity_child'] != 0 ){
                               $temp1            = $vi;
                               $temp1['sub_price'] = $vi['pricr_per_package']* $vi['quantity'];
                            }else{
                               $temp1            = $vi;
                               $temp1['sub_price'] = @$vi['pro_price']* $vi['quantity'];
                            }
                            array_push($temp2, $temp1);
                           }
                         $temp['items']        = $temp2;
                        }
                    }
                }
                if($transaction['transaction_amount_type']=="Cr" && $transaction['transaction_type_id']== 2){

                    $order_data = json_decode($transaction['order_data'], true);
                    if($transaction['status']== 1){
                    $temp['delivered']    = "Success";
                    }else{
                    $temp['delivered']    = "Failed";
                    }
                    $temp['TXNID']    = $order_data['TXNID']; 
                    $temp['items']    = Array();                    

                }
                $temp['reason']           = @$transaction['refund_type']['refund_reasons'];
                $temp['balance']          = @$transaction['balance'];

                $response['messageText']    = "Success";
                $response['messageCode']    = 200;
                $response['successCode']    = 1;
                if($temp){
                    $response['products']   = $temp;
                }else{
                    $response['products']   = "{}";
                }

            }else{
                $response['messageText'] = "Invalid details";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response); die;
        }
    }
    
    public function gallery(){
        $response       = array();
        $sub_status     = 1;
        $gallaryTable   = TableRegistry::get('Gallery');
        $galleries1     = array();
        $galleries = $gallaryTable->find()->contain(['GalleryImages' => function (\Cake\ORM\Query $query) use ($sub_status) {
                        return $query->where(['GalleryImages.image <>' => ""])->orWhere(['GalleryImages.image IS NOT NULL']);
                    },])->hydrate(false)->toArray();
        foreach($galleries as $key=>$value)
        {

            if(count($value['gallery_images']) < 1 )
                unset($value['gallery_images']);

            array_push($galleries1, $value);
        }
        if(count($galleries1) > 0){
            $response['messageText']    = "Success";
            $response['messageCode']    = 200;
            $response['successCode']    = 1;
            $response['gallery']        = $galleries1;
        }else{
            $response['messageText']    = "No gallery found.";
            $response['messageCode']    = 501;
            $response['successCode']    = 0;
            $response['gallery']        = array();
        }
        echo json_encode($response); die;
    }
    public function galleryimages(){
        $response       = array();
        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $galleryTable   = TableRegistry::get('GalleryImages');
            $galleries      = $galleryTable->find()->where(['GalleryImages.gallery_id'=> $data['gallery_id'], 'GalleryImages.image IS NOT NULl'])->order(['GalleryImages.position'=> 'ASC'])->toArray();
            if(count($galleries) > 0){
                $response['messageText']    = "Success";
                $response['messageCode']    = 200;
                $response['successCode']    = 1;
                $response['gallery']        = $galleries;
            }else{
                $response['messageText']    = "No image found for this gallery.";
                $response['messageCode']    = 201;
                $response['successCode']    = 0;
                $response['gallery']        = array();
            }
        }
        echo json_encode($response); die;
    }

    public function getPaymentMethod()
    {
        $this->loadComponent('Paginator');
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = array();
            $response = $this->validateupdateProfileCustomer($data);
            // $response['messageCode']=200;
            if ($response['messageCode'] == 200) {
                $paymentMethodsTable = TableRegistry::get('payment_methods');
                $this->paginate = [
                    'limit'      => 20,
                ];
                $paymentMethods = $this->paginate($paymentMethodsTable)->toArray();

                if ($paymentMethods) {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['paymentMethods'] = $paymentMethods;

                } else {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['paymentMethods'] = "No payment method";
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);
            die;

        }
    }

    public function getPaymentMethodConfig()
    {
        $this->loadComponent('Paginator');
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = array();
            $response = $this->validateupdateProfileCustomer($data);
            // $response['messageCode']=200;
            if ($response['messageCode'] == 200) {
              $paytmsTable = TableRegistry::get('paytms');
              $payumoney = TableRegistry::get('payumoneys');
                // $this->paginate = [
                //     'limit'      => 20,
                // ];
                $paymentMethodConfig['paytm'] = $this->paginate($paytmsTable)->toArray();
                $paymentMethodConfig['payumoney'] = $this->paginate($payumoney)->toArray();

                if ($paymentMethodConfig) {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['paymentMethodConfig'] = $paymentMethodConfig;

                } else {
                    $response['messageText']   = "success";
                    $response['messageCode']   = 200;
                    $response['successCode']   = 1;
                    $response['paymentMethodConfig'] = "No payment account";
                }

            } else {
                $response['messageText'] = "Invalid Request";
                $response['messageCode'] = 201;
                $response['successCode'] = 0;
            }
            echo json_encode($response);
            die;

        }
    }
}
