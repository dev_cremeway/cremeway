<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use App\Controller\ExperttextingController;
use Twilio\Rest\Client;


 
class ContactsController extends AppController
{

     public function initialize()
        {
            parent::initialize();
        }

       /*---Start code for multilanguage from 1000-----*/

       /*--status code reserve for sendOtp function Start from 1000 to 1020---*/
       
       public function index()
       {
        $contactsTable = TableRegistry::get('contacts');
        $user = $contactsTable->find('all')->toArray();

        $this->set('user', $user);
       }
 
}