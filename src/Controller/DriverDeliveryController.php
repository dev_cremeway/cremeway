<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
//use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class DriverDeliveryController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
        {   
             parent::initialize();
            $this->loadComponent('Paginator');
            $this->loadComponent('RequestHandler');
            // $this->Auth->allow();
        }
     

     public function index(){
       
        $UsersTable = TableRegistry::get('Users');
        $users1 = $UsersTable->find()->contain(['OrderTrackings'])->select(['OrderTrackings.lat','OrderTrackings.lng','Users.name','Users.id','Users.area_id','Users.region_id','Users.phoneNo'])->where(['Users.type_user'=>'driver'])->toArray();
        //print_r($users);
        $this->set('users1',$users1);

if (isset($_POST["schedule_id"])) {
  
$delivery_schdule_id = $_POST['schedule_id'];
$driver_id = $_POST['user_id'];

$DriverRoutesTable = TableRegistry::get('DriverRoutes');
$d_routes = $DriverRoutesTable->find('all')->where(['user_id'=>$driver_id,'delivery_schdule_id'=>$delivery_schdule_id])->toArray();
foreach ($d_routes as $key => $value) { 
  $route_id = $value['route_id'];


$routeCustomerTable = TableRegistry::get('RouteCustomers');
          $routeCustomer = $routeCustomerTable->find('all')->contain(['Regions','Areas','Users'])->where(['delivery_schdule_id'=>$delivery_schdule_id,'route_id'=> $route_id,'user_id IS NOT NULL'])->order(['RouteCustomers.position' => 'ASC'])->toArray();
            // pr($routeCustomer);die; 
          
         $final_array=array();
         $check_in_array=array();
         $before_final = array();
         $checkarray = array();


           
            $userIds = array();

            foreach ($routeCustomer as $key => $value) {
                $area_region = $value['region_id'].'_'.$value['area_id'];
                array_push($userIds, $value['user_id']);
            }

            $userIds = array_unique($userIds);
            $userTable = TableRegistry::get('Users');
            $allUserSubOrder = $userTable->find('all')->contain([
                                     'Regions',
                                     'RouteCustomers',
                                     'Areas',
                                     'UserContainers',
                                     'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) {
                                        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1,'UserSubscriptions.startdate <='=>date('Y-m-d')]);
                                    },
                                     
                                     
                                      'CustomOrders.Products.Units' => function (\Cake\ORM\Query $query) use($delivery_schdule_id) {
                                        return $query->where(['CustomOrders.created' => date('Y-m-d'),
                                            'CustomOrders.status'=>0,'CustomOrders.delivery_schdule_id'=>$delivery_schdule_id]);
                                    }


                                     ])->where(['Users.id IN'=>$userIds,'Users.is_deleted'=>0])->order(['RouteCustomers.position' => 'ASC'])->hydrate(false)->toArray();

            $itemsInfno = $this->factorySubscription($allUserSubOrder,$delivery_schdule_id);
            //pr($allUserSubOrder); 
            //$final_array[$area_region][] = $allUserSubOrder;

            //echo json_encode($itemsInfno); die();
                

$itemsInfno = array_map('unserialize', array_unique(array_map('serialize', $itemsInfno)));

         foreach ($itemsInfno as $key => $value) {   
                 $transtable = TableRegistry::get('Transactions');
                 $trans      = $transtable->find('all')->select(['Transactions.created','Transactions.transaction_amount_type','Transactions.rejected_reason'])->where(['Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $delivery_schdule_id, 'Transactions.user_id' => $value['id']])->hydrate(false)->toArray(); 
                 //pr($trans); die;

                 $area_region = $value['regionId'].'_'.$value['areaId']; 
               
                 if( ! in_array($area_region, $checkarray)){
                 $temp = array();     
                 $temp['region_area_name'] = $value['region_name'];
                 $temp['area_name'] = $value['area_name'];
                 //$temp['date'] = $value['date'];
                 if($trans){
                  $temp['date'] = @$trans[0]['created']->format('Y-m-d');
                  $temp['reason'] = @$trans[0]['transaction_amount_type'];
                  }else{
                    $temp['date'] = "2017-01-01";
                    $temp['reason'] = "";
                  }
                 $temp['name'] = $value['name']; 
                 //$temp['reason'] = $value['reason'];
                 
                 $temp['address'] = $value['address'];
                 $temp['phoneNo'] = $value['phoneNo'];
                 $final_array[$area_region][] = $temp;

                 }else{
                 $temp = array();   
                 $temp['region_area_name'] = $value['region_name'];
                 $temp['area_name'] = $value['area_name'];
                 //$temp['date'] = $value['date'];
                 if($trans){
                  $temp['date'] = @$trans[0]['created']->format('Y-m-d');
                  $temp['reason'] = @$trans[0]['transaction_amount_type'];
                  }else{
                    $temp['date'] = "2017-01-01";
                    $temp['reason'] = "";
                  }
                 $temp['name'] = $value['name'];
                 //$temp['reason'] = $value['reason'];
                 $temp['address'] = $value['address'];
                 $temp['phoneNo'] = $value['phoneNo'];                 


                  if( isset($temp)&& ! empty($temp) )
                  {
                  $final_array[$area_region][] = $temp;  
                  $final_array[$area_region] = array_map("unserialize", array_unique(array_map("serialize", $final_array[$area_region])));  
                  }
                 }

                 
                 array_push($checkarray, $area_region);

                // $final_array[] = $before_final;

             
          }
         
         echo json_encode($final_array);
         die;      

}

 

}

if (isset($_POST["id"])) {
       $id = $_POST['id'];
          
           $currentDateTime = date('Y-m-d h:i:s');
           //$newDateTime = date('h:i A', strtotime($currentDateTime)); 

//pr($newDateTime);die;


 $currentDateTime = date('Y-m-d h:i:s A');
                                    $dt = new \DateTime($currentDateTime);
                                    $tz = new \DateTimeZone('Asia/Kolkata'); 
                                    $dt->setTimezone($tz);
                                    $newDateTime = $dt->format('h:i A');
                                    

//echo $newDateTime; 
                                    /*
$currentDateTime = date("m/d/Y h:i A", time());
$newDateTime = date('h:i A', strtotime($currentDateTime));
*/
        
            

              $driverRouteTable = TableRegistry::get('DeliverySchdules');
              $schdule = $driverRouteTable->find('all')->hydrate(false)->toArray();
             
            if(count($schdule)>0){
                  
                  foreach ($schdule as $key => $value) {  
 
                  $current_time = $newDateTime;
                  $startTime = $value['start_time'];
                  $endTime = $value['end_time'];
                  $dateObject = new \DateTime;
                  $date1 = $dateObject::createFromFormat('H:i a', $current_time);
                  $date2 = $dateObject::createFromFormat('H:i a', $startTime);
                  $date3 = $dateObject::createFromFormat('H:i a', $endTime);

                  if ($date1 >= $date2 && $date1 <= $date3)
                  {
                                        
                           echo $value['id']; die;             

                  }  

                }


          
    }




     }

}
       
 private function getAllRegionsLists(){
         
          $areaTable = TableRegistry::get('Regions');
          $regions = $areaTable->find('list')->hydrate(false)->toArray();
          return $regions;
    }   





public function factorySubscription( $data,$schduleID, $day = NULL){

                $final = array();

                foreach ($data as $key => $value) {
                $totalIndividualPrice = 0;  
                $final1 = array(); 
                 if( ( isset($value['custom_orders']) && ! empty( $value['custom_orders'] ) ) || ( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ) ) 
                {
                      if( isset($value['custom_orders']) && ! empty( $value['custom_orders'] ) ){
                              
                               $temp1 = array(); 
                               foreach ($value['custom_orders'] as $k => $v) {
                                 $insideorders = array(); 
                                  $insideorders['name'] = $v['product']['name'];
                                  $insideorders['pro_id'] = $v['product']['id'];
                                  $insideorders['iscontainer'] = $v['product']['iscontainer'];
                                  $insideorders['quantity'] = $v['quantity'];
                                  $insideorders['unit'] = $v['product']['unit']['name'];
                                  $totalIndividualPrice = $totalIndividualPrice + $v['price'];
                                  $final1[] = $insideorders; 
                               } 
                          
                        }
                        if( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ){


                            $temp = array(); 
                               foreach ($value['user_subscriptions'] as $k => $v) {
                                 $insideorders = array();
                                 if( $this->checkTodaySubscriptions( $v['id'],$v['subscription_type_id'],$v['days'],$schduleID ) ){
     
                                      $insideorders['name'] = $v['product']['name'];
                                      $insideorders['pro_id'] = $v['product']['id'];
                                      $insideorders['iscontainer'] = $v['product']['iscontainer'];
                                      $insideorders['quantity'] = $v['quantity'];
                                      $insideorders['unit'] = $v['unit_name'];
                                      $totalIndividualPrice = $totalIndividualPrice + $v['subscriptions_total_amount'];
                                  
                                  }     
                                   if(count($insideorders)>0)
                                   {
                                    $final1[] = $insideorders;
                                   }
                               }
                              
                        }
                        
               if(count($final1)>0)
               {                      

                       $isNotify = $this->isNotThisCustomer($schduleID,$value['id']);

                        if($isNotify){
                        $customerAddress['notified'] = 1;
                        }else{
                           $customerAddress['notified'] = 0;
                        }
                     
                        $containerInfo = $this->customerContainer($value['id']);
                        if($containerInfo){
                          
                           $customerAddress['containerNeedToBeCollect'] = $containerInfo;

                        }else{
                             
                             $customerAddress['containerNeedToBeCollect'] = 0;
                        }

                        $customerAddress['id'] = $value['id'];
                        $customerAddress['date'] = $value['route_customer']['date'];
                        $customerAddress['reason'] = $value['route_customer']['reason'];
                        $customerAddress['name'] = $value['name'];
                        $customerAddress['totalOrderSubscriptionPrice'] = $totalIndividualPrice;
                        $customerAddress['phoneNo'] = $value['phoneNo'];
                        $customerAddress['lat'] = $value['latitude'];
                        $customerAddress['lng'] = $value['longitude'];
                        $customerAddress['regionId'] = $value['region_id'];
                        $customerAddress['areaId'] = $value['area_id'];
                        $customerAddress['address'] = "#".$value['houseNo'].', '.$value['area']['name'] .' - '.$value['region']['name'];
                        $customerAddress['area_name'] = $value['area']['name'];
                        $customerAddress['region_name'] = $value['region']['name']; 

                       
                        $highestInfo = array();
                        //$highestInfo['subscriptionInfo'] = $final1;
                        $highestInfo = $customerAddress; 


                        $final[] = $customerAddress;
                    }    


                     }else{
                      continue;
                     } 
                  } 
              return $final; 
       }

private function customerContainer($customerId){

       $usersContainerTable = TableRegistry::get('UserContainers');
         $usersContainer = $usersContainerTable->find()->select(['container_given'])->where(['user_id'=>$customerId])->toArray();
         if(isset($usersContainer)&&count($usersContainer)>0){
          return $usersContainer[0]['container_given'];
         }else{
          return false;
         }
     }  

private function isNotThisCustomer($schduleID,$customerId){

              $routecustomersTable = TableRegistry::get('RouteCustomers');
              $routecustomers = $routecustomersTable->find()->where(['user_id'=>$customerId,'delivery_schdule_id'=>$schduleID,'notify_date'=>date('Y-m-d'),'is_notify'=>1])->count();
              return $routecustomers;


      } 


      private function checkTodaySubscriptions($id, $type, $days, $schduleID ){

                     $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                     $userSubscriptions = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id'=>$id])->first();

                      
                     if( count($userSubscriptions) > 0 ){

                          if($userSubscriptions['subscription_type']['subscription_type_name'] == 'weekly'){
                                    

                                    $days = explode('-', $userSubscriptions['days']);
                                    $todayName = $this->returnDayName();
                                    if( in_array($todayName, $days) ){
     
                                      if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){

                                        return true;
                                      }
                                          return false;
                                    }
                                       return false;
                          }else if($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {

                                  
                                   
                                   if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                        return true;
                                      }
                                          return false;                                 

                          


                          }else if($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate'){
                              

                               $startdate = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                               $subscriptionday = strtotime($startdate);
                               $now = time();
                               $datediff = $now - $subscriptionday;
                               $days = floor($datediff / (60 * 60 * 24));
                              

                               if($days%2==0){
 
                                 if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                        return true;
                                      }
                                          return false;

                               }

                          }
                     }
                      return false;




       }

         
}
