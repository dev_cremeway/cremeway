<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Units Controller
 *
 * @property \App\Model\Table\UnitsTable $Units
 *
 * @method \App\Model\Entity\Unit[] paginate($object = null, array $settings = [])
 */
class UnitsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    { 
        return $this->redirect(array('Controller'=>'Units','action'=>'add')); 
        $units = $this->paginate($this->Units);

        $this->set(compact('units'));
        $this->set('_serialize', ['units']);
    }

    /**
     * View method
     *
     * @param string|null $id Unit id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {   
       /* echo "Please wait Your dashboard under construction now";die;  
        $unit = $this->Units->get($id, [
            'contain' => ['CustomOrders', 'ProductChildren', 'Products', 'UserSubscriptions']
        ]);

        $this->set('unit', $unit);
        $this->set('_serialize', ['unit']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
   
    private function getAllUnits(){
          
           $unitTable = TableRegistry::get('Units');
           $unitList = $unitTable->find('all')
                                       ->where(['status'=>1])
                                       ->hydrate(false)
                                       ->toArray();

           return $unitList;
   }
   private function getUnit( $name ){

             $untTable = TableRegistry::get('Units');
             $singleUnit = $untTable->find()
                                    ->where(['name'=>$name])
                                    ->count();
                                     
             return $singleUnit;
   }
    private function validateaddUnit( $data ){
                 
                  $error = array();
                  
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the unit name';
                  }/*else if( ! in_array($data['name'], $u_List) ) {
                     $error['name'] = 'Invalid Unit Name';
                  }*/else if( $this->getUnit( trim( $data['name'] ) ) ){
                    $error['name'] = 'Unit name already taken';
                  }
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
       return $error;
    }


    public function add()
    {
        /*$this->set('unit',$this->getAllUnits());*/


        $this->paginate = [
            'limit' => 10,
            'conditions'=>['status'=>1]
        ];
        $unit = $this->paginate($this->Units);
        $this->set(compact('unit'));



        if(isset($_GET['query']))
                    {
                            $condition = ['status'=>1];
                            $filterby = '';
                            $page = 0;
                            if(isset($_GET['page']) && ! empty($_GET['page'])){
                            $page = $_GET['page'];
                             }
                           if(isset($_GET['query'])){
          
                                       $condition[] = [

                                          'OR' => [ 

                                             'name LIKE'=>'%'.$_GET['query'].'%'
                                           ]
                                        ];
                                       $this->paginate = [
                                                      'limit' => 8,
                                                      'page'=> $page,
                                                      'order' => [
                                                          'id' => 'desc'
                                                      ],
                                                      'conditions'=>$condition
                                                  ];
                                       }
                                 $unit = $this->paginate($this->Units);
                                 $this->set(compact('unit'));      
                                  if(isset($_GET['query']) && !empty($_GET['query'])){
                                  $this->set('querystring',$_GET['query']);
                                }


                    }
        

            $error = array();
            if ($this->request->is('post')) {
                 
                $error = $this->validateaddUnit($this->request->getData());
                if($error['statuscode'] == 200)
                {
                    $unit = $this->Units->newEntity();
                    $unit = $this->Units->patchEntity($unit, $this->request->getData());
                    if ($this->Units->save($unit)) {
                        $this->Flash->success(__('Units has been Added successfully')); 
                        return $this->redirect(['action' => 'add']);
                    }
                  
                }else{
                     $this->set('error',$error);
                     $this->set('data',$this->request->getData());
                }


            }
        
    

    }

    /**
     * Edit method
     *
     * @param string|null $id Unit id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

     private function getifnameExistTwice( $id, $name ){


             $getname = TableRegistry::get('Units');

             $getnameexist = $getname->find()
                                     ->where(['AND'=>['name'=>$name,'id <>'=>$id]])
                                     ->count();
             return $getnameexist;


     }
     
     private function validateEditunit( $data ){

        $error = array();
        
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Unit name can not be empty';
        }/*else if( ! in_array($data['name'],$_ds)){

              $error['name'] = 'Invalid unit name'; 
        }*/else if($this->getifnameExistTwice($data['id'],$data['name'])){

               $error['name'] = 'This unit already taken'; 
        }
        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;

             
     }

    public function edit( $id = null )
    {

        $error = array();
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $error = $this->validateEditunit($this->request->getData());
            if($error['statuscode'] == 200)
            {
                $unit = $this->Units->get($this->request->getData('id'), ['contain' => []]);
                $unit = $this->Units->patchEntity($unit, $this->request->getData());
                if ($this->Units->save($unit)) {
                    $this->Flash->success(__('Units has been Updated successfully')); 
                    return $this->redirect(['action' => 'add']);
                }
            }else{
                 $this->set('error',$error);
                 $this->set('data',$this->request->getData());
            }

        }
        else{
                $id = base64_decode($id);
                $Unit = $this->Units->get($id, [
                    'contain' => []
                ]);    
                $this->set('data',$Unit);
      }
    }

    /**
     * Delete method
     *
     * @param string|null $id Unit id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*$this->request->allowMethod(['post', 'delete']);*/
        $unit = $this->Units->get(base64_decode($id));
        $unit->status = 0;
        if ($this->Units->save($unit)) {
            $this->Flash->success(__('The unit has been deleted.'));
        } else {
            $this->Flash->error(__('The unit could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add']);
    }
    
    
}
