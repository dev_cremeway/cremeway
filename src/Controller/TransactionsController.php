<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

Configure::write('CakePdf', [
        'engine' => 'CakePdf.DomPdf',
        'margin' => [
            'bottom' => 15,
            'left' => 50,
            'right' => 30,
            'top' => 45
        ],
        'orientation' => 'landscape',
        'download' => true
    ]);
/**
 * Transactions Controller
 *
 * @property \App\Model\Table\TransactionsTable $Transactions
 *
 * @method \App\Model\Entity\Transaction[] paginate($object = null, array $settings = [])
 */
class TransactionsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['updateBalance','invoice','getduplicateentries','updateUserbal']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $transactionsTable  = TableRegistry::get('Transactions');
        if (isset($_GET['query']) && trim($_GET['query']) != '') {
            $page = 0;
            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }

            $condition[] = [

                'OR' => [
                    'Transactions.id'      => $_GET['query'],
                    'Transactions.created' => $_GET['query'],
                ],
            ];
            $this->paginate = [
                'sortWhitelist' => [
                    'Transactions.id', 'Transactions.transaction_amount_type', 'Transactions.amount', 'Transactions.created',
                ],
                'limit'         => 20,
                'page'          => $page,
                'contain'       => ['Users', 'TransactionTypes', 'Users.Areas', 'DeliverySchdules'],
                'conditions'    => $condition,
                'order'         => [
                    'Transactions.id' => 'desc',
                ],

            ];
            if (isset($_GET['query']) && !empty($_GET['query'])) {
                $this->set('querystring', $_GET['query']);
            }
        } else {
            $conditions = array();
            if (isset($_GET['area_id'])) {
                if (trim(@$_GET['transaction_id'])) {
                    $conditions['Transactions.id'] = trim(@$_GET['transaction_id']);
                }
                if (trim(@$_GET['user_id'])) {
                    $conditions['Transactions.user_id'] = base64_decode(trim(@$_GET['user_id']));
                }
                if (@$_GET['schedule_id']) {
                    $conditions['Transactions.delivery_schdule_id'] = @$_GET['schedule_id'];
                }
                if (@$_GET['area_id']) {
                    $conditions['Transactions.area_id'] = @$_GET['area_id'];
                }
                if (@$_GET['payment_type']) {
                    $conditions['Transactions.transaction_amount_type'] = @$_GET['payment_type'];
                }
                if (@$_GET['transaction_type_id']) {
                    if($_GET['transaction_type_id'] == 10){
                        $conditions['Transactions.transaction_type_id'] = 2;
                        $conditions['Transactions.status'] = 1;
                    }else if($_GET['transaction_type_id'] == 11){
                        $conditions['Transactions.transaction_type_id'] = 2;
                        $conditions['Transactions.status'] = 0;
                    }else{
                        if($_GET['transaction_type_id'] == 2){
                            $conditions['Transactions.transaction_type_id IN'] = array(2,8);
                        } else {
                            $conditions['Transactions.transaction_type_id'] = $_GET['transaction_type_id'];
                        }
                    }
                }
                if (@$_GET['product_id']) {
                    //$conditions['Transactions.products_ids '] = $_GET['product_id'];
                    $conditions[] = 'FIND_IN_SET('.@$_GET["product_id"].',Transactions.products_ids)';
                }
                if (@$_GET['from']) {
                    $conditions['Transactions.created >='] = $_GET['from'];
                }
                if (@$_GET['to']) {
                    $conditions['Transactions.created <='] = $_GET['to'];
                }

            }

            if (!empty($conditions)) {
                $this->paginate = [

                    'sortWhitelist' => [
                        'Transactions.id', 'Transactions.transaction_amount_type', 'Transactions.amount', 'Transactions.created',
                    ],

                    'limit'         => 20,
                    'contain'       => ['Users', 'TransactionTypes', 'Users.Areas', 'DeliverySchdules'],
                    'conditions' => $conditions,
                    'order'         => [
                        'Transactions.id' => 'desc',
                    ],
                ];
                $transactions1 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'Dr';
                $transactions1->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
                $transactions2 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'Cr';
                $conditions['status']                  = 1;
                $transactions2->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
                $transactions3 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'rejected';
                $conditions['status']                  = 0;
                $transactions3->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
            } else {
                $this->paginate = [
                    'sortWhitelist' => [
                        'Transactions.id', 'Transactions.transaction_amount_type', 'Transactions.amount', 'Transactions.created',
                    ],

                    'limit'         => 20,
                    'contain'       => ['Users', 'TransactionTypes', 'Users.Areas', 'DeliverySchdules'],
                    'order'         => [
                        'Transactions.id' => 'desc',
                    ],
                ];
                $transactions1 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'Dr';
                $transactions1->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
                $transactions2 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'Cr';
                $conditions['status']                  = 1;
                $transactions2->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
                $transactions3 = $transactionsTable->find('all');
                $conditions['transaction_amount_type'] = 'rejected';
                $conditions['status']                  = 0;
                $transactions3->select([ 'created', 'revenue' => $transactions1->func()->sum('amount')])
                    ->group('created')->where($conditions)->all()->toArray();
            }
        }
        $transactions   = $this->paginate($this->Transactions)->toArray();
        $schedulesTable = TableRegistry::get('DeliverySchdules');
        $productsTable  = TableRegistry::get('Products');
        $areasTable     = TableRegistry::get('Areas');
        $ttypesTable    = TableRegistry::get('TransactionTypes');
        $schedules      = $schedulesTable->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
        $products       = $productsTable->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
        $areas          = $areasTable->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
        $ttypes         = $ttypesTable->find('list', ['keyField' => 'id', 'valueField' => 'transaction_type_name'])->toArray();
        //pr($ttypes);die;
        $this->set(compact('transactions', 'transactions1', 'transactions2', 'transactions3','schedules', 'products', 'areas', 'ttypes'));



    }
    public function transactionsCsv(){

        $conditions1 = array();
        if (!empty($_GET)) {
            if (@$_GET['schedule_id']) {
                $conditions1['DeliverySchdules.id'] = $_GET['schedule_id'];
            }
            if (@$_GET['user_id']) {
                $conditions1['Transactions.user_id'] = base64_decode(trim(@$_GET['user_id']));
            }
            if (@$_GET['area_id']) {
                $conditions1['Users.area_id'] = $_GET['area_id'];
            }
            if (@$_GET['payment_type']) {
                $conditions1['Transactions.transaction_amount_type'] = @$_GET['payment_type'];
            }
            if (@$_GET['transaction_type_id']) {
                if($_GET['transaction_type_id'] == 10){
                        $conditions1['Transactions.transaction_type_id'] = 2;
                        $conditions1['Transactions.status'] = 1;
                    }else if($_GET['transaction_type_id'] == 11){
                        $conditions1['Transactions.transaction_type_id'] = 2;
                        $conditions1['Transactions.status'] = 0;
                    }else{
                        $conditions1['Transactions.transaction_type_id'] = $_GET['transaction_type_id'];
                    }
            }
            if (@$_GET['product_id']) {
                $conditions1[] = 'FIND_IN_SET('.@$_GET["product_id"].',Transactions.products_ids)';
            }
            if (@$_GET['from']) {
                $conditions1['Transactions.created >='] = $_GET['from'];
            }
            if (@$_GET['to']) {
                $conditions1['Transactions.created <='] = $_GET['to'];
            }

        }

        if (!empty($conditions1)) {
            $transactions1 = $this->Transactions->find('all')->where($conditions1)->contain(['Users', 'TransactionTypes', 'Users.Areas', 'DeliverySchdules'])->order(['Transactions.id' => 'desc'])->limit(15000)->toArray();
        } else {
            $transactions1 = $this->Transactions->find('all')->contain(['Users', 'TransactionTypes', 'Users.Areas', 'DeliverySchdules'])->order(['Transactions.id' => 'desc'])->limit(15000)->toArray();
        }
        $milliseconds   = round(microtime(true) * 1000);
        $this->response->download($milliseconds.'.csv');
        $_header        = ['Transaction_ID','Schedule', 'Customer_Name', 'Address', 'Area', 'Mobile', 'Last Recharge Amount', 'Last Recharge Date', 'Current Balance', 'Amount (Rs)','Payment_Type','Transaction_Type','Date','paytm_TXNID'];

        $userBalanceTable = TableRegistry::get('UserBalances');
        $finaldata = array();
        foreach($transactions1 as $key => $value)
          {
            $last_rech = $this->Transactions->find()->where(['user_id' => $value['user_id'], 'transaction_amount_type' => "Cr", 'status' => 1 ])->order(['id' => 'desc'])->first();
            $userBalance  = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $value['user_id']])->first();
            $temp   = array();
            $temp['transaction_id']         = $value['id'];
            $temp['schedule_name']          = $value['delivery_schdule']['name'];
            $temp['user_name']              = $value['user']['name'];
            $temp['houseNo']                = $value['user']['houseNo'];
            $temp['area_name']              = $value['user']['area']['name'];
            $temp['mobile']                 = $value['user']['phoneNo'];
            if($last_rech){
                $temp['last_rchg_amt']          = $last_rech['amount'];
                $temp['last_rchg_date']         = $last_rech['created']->format('Y-m-d');
            }else{
                $temp['last_rchg_amt']          = "--";
                $temp['last_rchg_date']         = "--";
            }
            $temp['current_balance']        = $userBalance['balance'];
            $temp['amount']                 = $value['amount'];
            $temp['amount_type']            = $value['transaction_amount_type'];
            $temp['transaction_type_name']  = $value['transaction_type']['transaction_type_name'];
            $temp['created']                = $value['created']->format('Y-m-d');
            if( $value['transaction_amount_type'] === 'Cr' && $value['transaction_type_id'] === 2 ){
                $paytm_res                      = json_decode($value['order_data'],true);
                $temp['paytm_TXNID']            = $paytm_res['TXNID'];
            }else{
                $temp['paytm_TXNID']            = "---";
            }

            $finaldata[] = $temp;
          }

        $_serialize = 'finaldata';
        $this->set(compact('finaldata', '_serialize','_header'));
        $this->viewBuilder()->className('CsvView.Csv');
        return;

      die;
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
            $id = base64_decode($_GET['id']);
            $transactionTable = TableRegistry::get('Transactions');
            $transactions     = $transactionTable->find('all')->contain(['Users','TransactionTypes'])->where(['Transactions.id' => $id])->toArray();
            $subIds     = explode(",",$transactions[0]['user_subscription_ids']);
            $cusIds     = explode(",",$transactions[0]['custom_order_ids']);

            $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
            $customOrdersTable = TableRegistry::get('CustomOrders');

            $userSub     = $userSubscriptionsTable->find('all')->contain(['Products.Units'])->where(['UserSubscriptions.id IN' => $subIds])->toArray();

            $customOrders     = $customOrdersTable->find('all')->contain(['Products.Units'])->where(['CustomOrders.id IN' => $cusIds])->toArray();
            $this->set(compact('transactions','userSub','customOrders'));

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transaction = $this->Transactions->newEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $users                     = $this->Transactions->Users->find('list', ['limit' => 200]);
        $usersSubscriptionStatuses = $this->Transactions->UsersSubscriptionStatuses->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'users', 'usersSubscriptionStatuses'));
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
        }
        $users                     = $this->Transactions->Users->find('list', ['limit' => 200]);
        $usersSubscriptionStatuses = $this->Transactions->UsersSubscriptionStatuses->find('list', ['limit' => 200]);
        $this->set(compact('transaction', 'users', 'usersSubscriptionStatuses'));
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function data()
    {
        if ($this->request->is('post')) {
            $requestedata = $this->request->getData();
            $start_date   = $requestedata['start_date'];
            $end_date     = $requestedata['end_date'];

            $transactionTable = TableRegistry::get('Transactions');
            $transactions     = $transactionTable->find('all')->contain(['Users'])->where(['Transactions.created >=' => $start_date, 'Transactions.created' => $end_date])->toArray();

            echo "done";
            die;

        }
    }

    /*
     *remove duplicate transactions and update correct users balance
     *
     */

    public function updateBalance()
    {
        $UserTable = TableRegistry::get('Users');
        //$uids = $UserTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->toArray();
        $uids = array();
       /* $uids = array(908,920,900,902,1007,1513,1002,1033,1212,1243,1248,1153,1152,1150,1168,1173,1170,1164,1163,1256,1190,1203,1211,1253,1255,1198,1200,994,1031,999,1186,1144,1143,1131,1138,1132,1130,1193,1195,1192,1222,1223,1221,1229,1230,1231,1224,1232,1235,1254,1251,1210,1207,1206,1201,1209,759,1180,1182,1179,1176,1184,1657,1204,1618,1169,1171,1127,1122,1125,1128,1117,1589,1121,1220,1219,1216,1214,1213,1661,1217,1187,1189,1239,1240,1241,1238,1237,1137,1139,1140,1146,909,911,912,913,921,916,917,922,918,927,903,923,904,924,907,1622,906,1579,1009,1008,1032,1014,1012,1013,1035,1016,1015,1017,1034,1658,1030,1001,1003,1023,1024,1029,1027,1026,1025,1028,1049,1048,1047,1046,1045,1613,1044,1042,1036,1040,1039,1043,1108,1109,1096,1095,1110,1099,1101,1112,1102,1113,1115,1105,1103,1104,1106,1114,1116,925,976,978,1523,1481,1000,998,997,996,995,993,992,991,990,989,988,986,985,984,983,982,981,980,979);*/
        // pr($uids);die;
        $transactionTable = TableRegistry::get('Transactions');
        foreach ($uids as $key => $value) {
            $bal          = 0;
            $del = array();
            $transactions = $transactionTable->find('all')->select(['Transactions.id', 'Transactions.created', 'Transactions.time', 'Transactions.amount', 'Transactions.transaction_amount_type'])->where(['Transactions.user_id' => $value,'transaction_type_id' => 6])->hydrate(false)->toArray();
            if (!empty($transactions) && count($transactions) > 1) {
                foreach ($transactions as $tk => $tv) {
                    if(in_array($tv['id'], $del)){
                        continue;
                    }
                    /*$transaction = $transactionTable->find('all')->select(['Transactions.id', 'Transactions.created', 'Transactions.time', 'Transactions.amount', 'Transactions.transaction_amount_type'])->where(['Transactions.user_id' => $value, 'Transactions.created' => $tv['created'], 'Transactions.time' => $tv['time'], 'Transactions.id <>' => $tv['id']])->hydrate(false)->toArray();*/
                    $transaction = $transactionTable->find('all')->select(['Transactions.id', 'Transactions.created', 'Transactions.time', 'Transactions.amount', 'Transactions.transaction_amount_type'])->where(['Transactions.user_id' => $value,'transaction_type_id' => 6, 'Transactions.created' => $tv['created'], 'Transactions.id <>' => $tv['id']])->hydrate(false)->toArray();
                    if (!empty($transaction)) {
                        foreach ($transaction as $k => $v) {
                            $transdata = $transactionTable->get($v['id']);
                            $duplicate = TableRegistry::get('DuplicateTransactions');
                            $data = $duplicate->newEntity();
                            $data -> transaction_id = $transdata->id;
                            $data -> user_id = $transdata->user_id;
                            $data -> transaction_amount_type = $transdata->transaction_amount_type;
                            $data -> amount = $transdata->amount;
                            $data -> onlinetransactionhistory = $transdata->onlinetransactionhistory;
                            $data -> created = $transdata->created;
                            $data -> time = $transdata->time;
                            $data -> syc_time = $transdata->syc_time;
                            $data -> status = $transdata->status;
                            $data -> rejected = $transdata->rejected;
                            $data -> rejected_reason = $transdata->rejected_reason;
                            $data -> delivery_schdule_id = $transdata->delivery_schdule_id;
                            $data -> transaction_type_id = $transdata->transaction_type_id;
                            $data -> refund_type_id = $transdata->refund_type_id;
                            $data -> users_subscription_status_id = $transdata->users_subscription_status_id;
                            $data -> payment_gateway_type = $transdata->payment_gateway_type;
                            $data -> order_data = $transdata->order_data;
                            $data -> online_transaction_id = $transdata->online_transaction_id;
                            $data -> user_subscription_ids = $transdata->user_subscription_ids;
                            $data -> custom_order_ids = $transdata->custom_order_ids;
                            $data -> balance = $transdata->balance;
                            $data -> products_ids = $transdata->products_ids;
                            $data -> area_id = $transdata->area_id;
                            $data -> transaction_month = $transdata->transaction_month;
                            $data -> modified = $transdata->modified;
                            $data -> updatedDate = date('Y-m-d H:i:s');
                            $duplicate->save($data);
                            //pr($data);die;
                            if($transactionTable->delete($transdata)){
                                $del[] = $v['id'];
                                 echo $v['id']."- Deleted.";
                            }
                        }
                    }
                    if ($tv['transaction_amount_type'] == 'Dr') {
                        $bal = $bal - $tv['amount'];
                    }
                    if ($tv['transaction_amount_type'] == 'Cr') {
                        $bal = $bal + $tv['amount'];
                    }
                    /*$query2   = $transactionTable->query();
                    $result1 = $query2->update()
                        ->set(['balance' => $bal])
                        ->where(['id' => $tv['id']])
                        ->execute();*/
                }
            }
            //$UserBalancesTable = TableRegistry::get('UserBalances');

            /*$query   = $UserBalancesTable->query();
            $result1 = $query->update()
                ->set(['balance' => $bal])
                ->where(['user_id' => $value])
                ->execute();*/

        }die('Transactions Deleted Successfully.');
    }

    public function updateUserbal(){
        $UserTable = TableRegistry::get('Users');
//        $uids = $UserTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->toArray();
        $uids = array();
        /*$uids = array(908,920,900,902,1007,1513,1002,1033,1212,1243,1248,1153,1152,1150,1168,1173,1170,1164,1163,1256,1190,1203,1211,1253,1255,1198,1200,994,1031,999,1186,1144,1143,1131,1138,1132,1130,1193,1195,1192,1222,1223,1221,1229,1230,1231,1224,1232,1235,1254,1251,1210,1207,1206,1201,1209,759,1180,1182,1179,1176,1184,1657,1204,1618,1169,1171,1127,1122,1125,1128,1117,1589,1121,1220,1219,1216,1214,1213,1661,1217,1187,1189,1239,1240,1241,1238,1237,1137,1139,1140,1146,909,911,912,913,921,916,917,922,918,927,903,923,904,924,907,1622,906,1579,1009,1008,1032,1014,1012,1013,1035,1016,1015,1017,1034,1658,1030,1001,1003,1023,1024,1029,1027,1026,1025,1028,1049,1048,1047,1046,1045,1613,1044,1042,1036,1040,1039,1043,1108,1109,1096,1095,1110,1099,1101,1112,1102,1113,1115,1105,1103,1104,1106,1114,1116,925,976,978,1523,1481,1000,998,997,996,995,993,992,991,990,989,988,986,985,984,983,982,981,980,979);*/
        $transactionTable = TableRegistry::get('Transactions');
        foreach ($uids as $key => $value) {
            $bal          = 0;
            $del = array();

            $transactions = $transactionTable->find('all')->select(['Transactions.id', 'Transactions.created', 'Transactions.time', 'Transactions.amount', 'Transactions.transaction_amount_type'])->where(['Transactions.user_id' => $value])->hydrate(false)->toArray();
            //pr($transactions);die;
            if (!empty($transactions) && count($transactions) > 1) {
                foreach ($transactions as $tk => $tv) {

                    if ($tv['transaction_amount_type'] == 'Dr') {
                        $bal = $bal - $tv['amount'];
                    }
                    if ($tv['transaction_amount_type'] == 'Cr') {
                        $bal = $bal + $tv['amount'];
                    }
                    $query2   = $transactionTable->query();
                    $result1 = $query2->update()
                        ->set(['balance' => $bal])
                        ->where(['id' => $tv['id']])
                        ->execute();
                }
            }
            $UserBalancesTable = TableRegistry::get('UserBalances');

            $query   = $UserBalancesTable->query();
            $result1 = $query->update()
                ->set(['balance' => $bal])
                ->where(['user_id' => $value])
                ->execute();
            echo $value."- Updated.";
        }die('Transactions Updated Successfully.');
    }

    public function updateareas(){
        $transactionTable = TableRegistry::get('Transactions');
        $usersTable = TableRegistry::get('Users');
        $transaction = $transactionTable->find()->select(['user_id'])->hydrate(false)->toArray();
        $transaction = array_map("unserialize", array_unique(array_map("serialize", $transaction)));
        foreach ($transaction as $key => $value) {
            $userId = $value['user_id'];
            $user = $usersTable->find()->select(['area_id'])->where(['id'=> $userId])->hydrate(false)->first();

            $query  = $transactionTable->query();
            $result = $query->update()
                ->set(['area_id' => $user['area_id']])
                ->where(['user_id' => $userId])
                ->execute();

        }
        die('Transactions Updated Successfully with Area_id.');
    }

    public function updateproductids(){
        $transactionTable = TableRegistry::get('Transactions');
        $usersTable = TableRegistry::get('Users');
        $transaction = $transactionTable->find('all')->where(['transaction_type_id'=> 6, 'OR' => [['  transaction_amount_type' => 'Dr'], ['  transaction_amount_type' => 'rejected']],])->order(['id'=> 'desc'])->hydrate(false)->toArray();
        //pr($transaction); die;
        foreach ($transaction as $key => $value) {
            $tr_id = $value['id'];
            $order_data = json_decode($value['order_data'], TRUE);
            foreach ($order_data as $key1 => $value1) {
              if($value1['userId'] == $value['user_id']){
                    $product_id = '';
                    foreach ($value1['items'] as $keyv => $valuev) {
                        $product_id = $product_id.','.$valuev['id'];
                    }
                    $product_id = ltrim($product_id,',');

                }
            }
            $query  = $transactionTable->query();
            $result = $query->update()
                ->set(['products_ids' => $product_id])
                ->where(['id' => $value['id']])
                ->execute();

        }
        die('Transactions Updated Successfully with Product_ids.');
    }

    public function invoice(){

        date_default_timezone_set('Asia/Kolkata');
        $path = ROOT.DS.'webroot/invoices/';
        if ($handle = opendir($path)) {
         while (false !== ($file = readdir($handle))) {
            if ((time()-filemtime($path.$file)) > 10800  ) {

               if (preg_match('/\.pdf$/i', $file)) {
                  unlink($path.$file);
               }
            }
         }
        }
        $url            = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        // $url1           = substr($url,0,- 3);
        // $user_id        = substr($url1, strrpos($url1, '/') + 1);
        //
        // $query_date1    = substr($url, strrpos($url, '/') + 1);
        //
        // $query_date     = '2017-'.$query_date1.'-04';

        $user_id = $_GET['user_customer_id'];
        $year = $_GET['year'];
        $month = $_GET['month'];

        $query_date     = $year.'-'.$month.'-04';
        $start_date     = date('Y-m-01', strtotime($query_date));
        $end_date       = date('Y-m-t', strtotime($query_date));
        $response       = array();

        $UsersTable       = TableRegistry::get('Users');
        $Users      = $UsersTable->find('all')->where(['id'=> $user_id])->toArray();

        $invoiceTable       = TableRegistry::get('Invoices');
        $transactionTable   = TableRegistry::get('Transactions');
        $transactions1      = $transactionTable->find('all')->where(['Transactions.user_id'=> $user_id, 'AND' => [['Transactions.created >=' => $start_date], ['  Transactions.created <=' => $end_date]],])->order(['Transactions.id'=> 'asc'])->contain(['Users', 'TransactionTypes', 'Users.Areas.Regions', 'DeliverySchdules'])->toArray();

        $invoice_id= $this->getInvoiceno();


        $finaldata = array();
        if(count($Users) < 1){
            $response['messageText']    = "No record found for this UserId.";
            $response['messageCode']    = 201;
            $finaldata[]                = $response;
        }else if($month < 1 || $month > 12){
            $response['messageText']    = "Please enter a valid month";
            $response['messageCode']    = 201;
            $finaldata[]                = $response;
        }else if( count($transactions1) < 1 ){
            $temp   = array();
            $temp['messageText']    = "No record found for this month";
            $temp['messageCode']    = 201;
            $finaldata[]            = $temp;
        }else{
            foreach($transactions1 as $key => $value)
            {
                $temp   = array();
                $temp['transaction_id']         = $value['id'];
                $temp['transaction_type_id']    = $value['transaction_type_id'];
                $temp['user_name']              = $value['user']['name'];
                $temp['amount']                 = $value['amount'];
                $temp['order_data']             = $value['order_data'];
                $temp['user_id']                = $value['user_id'];
                $temp['amount_type']            = $value['transaction_amount_type'];
                $temp['transaction_type_name']  = $value['transaction_type']['transaction_type_name'];
                $temp['created']                = $value['created']->format('d/m/Y');
                $temp['schedule_name']          = $value['delivery_schdule']['name'];
                $temp['area_name']              = $value['user']['area']['name'];
                $temp['region_name']            = $value['user']['area']['region']['name'];
                $temp['address']                = $value['user']['houseNo'];
                $temp['contact']                = $value['user']['phoneNo'];
                $temp['balance']                = $value['balance'];
                $temp['invoice_no']             = $invoice_id['sr_no'];
                $finaldata[] = $temp;
            }
            $total_amount = 0;
            foreach ($finaldata as $keyf => $valuef) {
              $total_amount = $total_amount +   $valuef['amount'];
            }
            $invoices                = $invoiceTable->newEntity();
            $invoices->date          = date('Y-m-d H:i:s');
            $invoices->Invoice_no    = $invoice_id['sr_no'];
            $invoices->user_id       = $user_id;
            $invoices->amount        = $total_amount;
            $result11                = $invoiceTable->save($invoices);
        }


        $this->viewBuilder()->options([
                'pdfConfig' => [
                    'orientation' => 'portrait',
                    'filename' => 'Invoice_' . $user_id,
                    'download' => true
                ]
            ]);
        $this->set('finaldata', $finaldata);
        $CakePdf = new \CakePdf\Pdf\CakePdf();
        $CakePdf->template('invoice', 'invoice');
        $CakePdf->viewVars($this->viewVars);
        $milliseconds   = round(microtime(true) * 1000);
        // Get the PDF string returned
        $pdf = $CakePdf->output();
        // Or write it to file directly
        $pdf = $CakePdf->write(ROOT.DS.'webroot/invoices'. DS . $milliseconds.'.pdf');
        header('Location: '.HTTP_ROOT.'webroot/invoices'. DS . $milliseconds.'.pdf');
        exit;
    }
    public function updatetransactionmonth(){
        $transactionTable = TableRegistry::get('Transactions');
        $transaction = $transactionTable->find()->select(['id','created'])->hydrate(false)->toArray();
        foreach ($transaction as $key => $value) {
            $query  = $transactionTable->query();
            $result = $query->update()
                ->set(['transaction_month' => $value['created']->format('mY')])
                ->where(['id' => $value['id']])
                ->execute();
        }
        die('Transactions Updated Successfully with Transaction_Month.');
    }
    public function getduplicateentries(){
        $transactionTable = TableRegistry::get('Transactions');
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            foreach ($data['delete_dup'] as $key => $value) {
                $entity = $transactionTable->get($value);
                $result = $transactionTable->delete($entity);
            }
        }else if (isset($_GET['date']) && !empty($_GET['date'])) {
            $date     = $_GET['date'];
            $transaction = $transactionTable->find('all')->where(['transaction_amount_type'=> 'Dr','transaction_type_id' => 6, 'created' => $date])->group(['user_id','amount','created'])->having(['count(*) >' => 1])->order(['id'=> 'desc'])->hydrate(false)->toArray();
            $this->set(compact('transaction'));
        }else{
            $transaction = $transactionTable->find('all')->where(['transaction_amount_type'=> 'Dr','transaction_type_id' => 6])->group(['user_id','amount','created'])->having(['count(*) >' => 1])->order(['id'=> 'desc'])->hydrate(false)->toArray();
            $this->set(compact('transaction'));
        }


    }
}
