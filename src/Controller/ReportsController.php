<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


 
class ReportsController extends AppController
{
 
    public function index()
    {
        die('asfas');
    }

    public function refusedProductsDCSV(){   


    if( $this->request->is( 'post' ) ){
         $data = $this->request->getData();
         $conditions = array();
         $contain = array();

         if( 
             ( isset( $data['start_date'] ) && ! empty( $data['start_date'] ) )
             &&
             ( isset( $data['end_date'] ) && ! empty( $data['end_date'] ) )
           ){
             $start_date = $data['start_date'];
             $end_date = $data['end_date'];
             $this->set(compact('start_date','end_date'));  
             $conditions[] = ['RejectedOrders.date >= ' => $data['start_date'],'RejectedOrders.date <= ' => $data['end_date']]; 
             $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
            }
            if( isset( $data['user_id'] ) && ! empty( $data['user_id'] ) ){

             $user_id = $data['user_id'];
             $this->set(compact('user_id'));   
             $conditions[] = ['RejectedOrders.user_id'=>$data['user_id']];
             $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
             $usersTable = TableRegistry::get('Users');
             $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray();   $this->set(compact('users'));     
            
            }
            if( isset( $data['period'] ) && ! empty( $data['period'] ) ){

              
                   $period  = $data['period'];
                   $this->set(compact('period'));
                   $dateRange = explode('_',$this->getDateRange($data['period']));
                   if(isset($dateRange) && !empty($dateRange)){
                   $startdate = $dateRange[0];
                   $enddate = $dateRange[1];
                   $conditions[] = ['RejectedOrders.date >= ' => $startdate,'RejectedOrders.date <= ' => $enddate];
                   $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
                   }

            }

            if( ( isset( $data['region_id'] ) && ! empty( $data['region_id'] ) ) && ( isset( $data['area_id'] ) && empty($data['area_id']) ) ){   
              $region_id  = $data['region_id'];
              $areaTable = TableRegistry::get('Areas');
              $areas = $areaTable->find('list')->where(['region_id'=>$region_id])->toArray();
              $this->set(compact('region_id','areas'));
               
              
               $contain = ['RejectedOrderItems','DeliverySchdules','Users' => function(\Cake\ORM\Query $q) use( $region_id ) {
                            return $q->where(['Users.region_id'=>$region_id]);    
                }
               ,'RejectedOrderItems.Products']; 
            } 
            if( isset( $data['area_id'] ) && ! empty( $data['area_id'] ) ) {
              $region_id = $data['region_id'];
              $area_id = $data['area_id'];
              $areaTable = TableRegistry::get('Areas');
              $areas = $areaTable->find('list')->where(['region_id'=>$region_id])->toArray();
              $this->set(compact('area_id','region_id','areas'));
              $contain = ['RejectedOrderItems','DeliverySchdules','Users' => function(\Cake\ORM\Query $q) use( $area_id,$region_id ) {
                            return $q->where(['Users.region_id'=>$region_id,'Users.area_id'=>$area_id]);    
                }
               ,'RejectedOrderItems.Products'];
            }
              $regionsTable = TableRegistry::get('Regions');  
              $regions = $regionsTable->find('list')->toArray();

              $this->set('regions',$regions);

             $rejectedOrdersTable = TableRegistry::get('RejectedOrders');
             $rejectedOrders = $rejectedOrdersTable->find('all',[
                       
                       'conditions'=>$conditions,
                       'contain'=>$contain
                  ])->toArray();
            
          if(isset($rejectedOrders) && count($rejectedOrders)>0){
              
                     $this->set('rejectedOrders',$rejectedOrders);

                     $milliseconds = round(microtime(true) * 1000);
                     $this->response->download($milliseconds.'.csv');
                     $_header = ['CUSTOMER NAME', 'REASON.', 'TIMING','PRODUCT','QUANTITY','PRICE'];
                     $finaldata = array();
                      foreach ($rejectedOrders as $key => $value) {
                               
                               $temp = array();
                               $temp['customer_name'] = $value['user']['name'];
                               $temp['reason'] = $value['reason'];
                               $temp['timing'] = $value['delivery_schdule']['name'];
                               $temp['product'] = $value['rejected_order_items'][0]['product']['name'];
                               $temp['quantity'] = $value['rejected_order_items'][0]['qty'];
                               $temp['price'] = $value['rejected_order_items'][0]['price'];
                               
                               $finaldata[] = $temp;  
                       }
                            $_serialize = 'finaldata';
                            $this->set(compact('finaldata', '_serialize','_header'));
                            $this->viewBuilder()->className('CsvView.Csv');

            } 

      }         
        $conditions = array();
         $contain = array();
        $myDate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) ); 

                $todayFilter = date('Y-m-d');


             $rejectedOrdersTable = TableRegistry::get('RejectedOrders');
              $usersTable = TableRegistry::get('Users');
              $regionsTable = TableRegistry::get('Regions');
              $rejectedOrders = $rejectedOrdersTable->find('all',[
                     
                    'conditions' => ['RejectedOrders.date >= ' => $myDate,'RejectedOrders.date <= ' => $todayFilter],
                     'contain'=>['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products']
                ])->toArray();
                
              $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray(); 
              $regions = $regionsTable->find('list')->toArray();
              $this->set(compact('regions','rejectedOrders','users'));

                     $milliseconds = round(microtime(true) * 1000);
                     $this->response->download($milliseconds.'.csv');
                     $_header = ['CUSTOMER NAME', 'REASON.', 'TIMING','PRODUCT','QUANTITY','PRICE'];
                     $finaldata = array();
                      foreach ($rejectedOrders as $key => $value) {
                               
                               $temp = array();
                               $temp['customer_name'] = $value['user']['name'];
                               $temp['reason'] = $value['reason'];
                               $temp['timing'] = $value['delivery_schdule']['name'];
                               $temp['product'] = $value['rejected_order_items'][0]['product']['name'];
                               $temp['quantity'] = $value['rejected_order_items'][0]['qty'];
                               $temp['price'] = $value['rejected_order_items'][0]['price'];
                               
                               $finaldata[] = $temp;  
                       }
                            $_serialize = 'finaldata';
                            $this->set(compact('finaldata', '_serialize','_header'));
                            $this->viewBuilder()->className('CsvView.Csv');

    }


    private function lastWeekDateRange(){
      
      $previous_week = strtotime("-1 week +1 day");

      $start_week = strtotime("last sunday midnight",$previous_week);
      $end_week = strtotime("next saturday",$start_week);

      $start_week = date("Y-m-d",$start_week);
      $end_week = date("Y-m-d",$end_week);

      return $start_week.'_'.$end_week ;

}
  private function getDateRange( $period ){
       $todayFilter = date('Y-m-d');
       $lastWeekDates = $this->lastWeekDateRange();
       $thismonthRangeStart = date('Y-m-01');
       $thismonthRangeEnd = $todayFilter;
       $lastMonthStartDateRange = date('Y-m-d', strtotime('first day of last month'));
       $lastMonthEndDateRange = date('Y-m-d', strtotime('last day of last month'));
       $sixMonthBeforeDate = date("Y-m-d", strtotime("-6 months"));
       $thisYearFirstDate = (new \DateTime(date("Y")."-01-01"))->format("Y-m-d");

       switch ($period) {
                 case 'today':
                    return $todayFilter.'_'.$todayFilter;
                 break;
                 case 'lastweek':
                    return $lastWeekDates;
                 break;
                 case 'thismonth':
                    return $thismonthRangeStart.'_'.$thismonthRangeEnd;
                 break;
                 case 'lastmonth':
                    return $lastMonthStartDateRange.'_'.$lastMonthEndDateRange;
                 break;
                 case 'halfyearly':
                    return $sixMonthBeforeDate.'_'.$todayFilter;
                 break;
                 case 'thisyear':
                   return $thisYearFirstDate.'_'.$todayFilter;
                 break;
              default:
              break;
             }      

  }


    public function refusedProducts(){
      

      $usersTable = TableRegistry::get('Users');
             $users = $usersTable->find('list')->order(['id DESC'])->toArray();   
             $this->set(compact('users'));
              
             $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];



      if( $this->request->is( 'post' ) ){
         $data = $this->request->getData();
         $conditions = array();
         $contain = array();
         if( 
             ( isset( $data['start_date'] ) && ! empty( $data['start_date'] ) )
             &&
             ( isset( $data['end_date'] ) && ! empty( $data['end_date'] ) )
           ){
             
             $usersTable = TableRegistry::get('Users');
             $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray();   $this->set(compact('users'));


             $start_date = $data['start_date'];
             $end_date = $data['end_date'];
             $this->set(compact('start_date','end_date'));  
             $conditions[] = ['RejectedOrders.date >= ' => $data['start_date'],'RejectedOrders.date <= ' => $data['end_date']]; 
             $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
            }
            if( isset( $data['user_id'] ) && ! empty( $data['user_id'] ) ){

             $user_id = $data['user_id'];
             $this->set(compact('user_id'));   
             $conditions[] = ['RejectedOrders.user_id'=>$data['user_id']];
             $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
             $usersTable = TableRegistry::get('Users');
             $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray();   $this->set(compact('users'));     
            
            }
            if( isset( $data['period'] ) && ! empty( $data['period'] ) ){

              
                   $period  = $data['period'];
                   $this->set(compact('period'));
                   $dateRange = explode('_',$this->getDateRange($data['period']));
                   if(isset($dateRange) && !empty($dateRange)){
                   $startdate = $dateRange[0];
                   $enddate = $dateRange[1];
                   $conditions[] = ['RejectedOrders.date >= ' => $startdate,'RejectedOrders.date <= ' => $enddate];
                   $contain = ['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products'];
                   }

            }

            if( ( isset( $data['region_id'] ) && ! empty( $data['region_id'] ) ) && ( isset( $data['area_id'] ) && empty($data['area_id']) ) ){   
              $region_id  = $data['region_id'];
              $areaTable = TableRegistry::get('Areas');
              $areas = $areaTable->find('list')->where(['region_id'=>$region_id])->toArray();
              $this->set(compact('region_id','areas'));
               
              
               $contain = ['RejectedOrderItems','DeliverySchdules','Users' => function(\Cake\ORM\Query $q) use( $region_id ) {
                            return $q->where(['Users.region_id'=>$region_id]);    
                }
               ,'RejectedOrderItems.Products']; 
            } 
            if( isset( $data['area_id'] ) && ! empty( $data['area_id'] ) ) {
              $region_id = $data['region_id'];
              $area_id = $data['area_id'];
              $areaTable = TableRegistry::get('Areas');
              $areas = $areaTable->find('list')->where(['region_id'=>$region_id])->toArray();
              $this->set(compact('area_id','region_id','areas'));
              $contain = ['RejectedOrderItems','DeliverySchdules','Users' => function(\Cake\ORM\Query $q) use( $area_id,$region_id ) {
                            return $q->where(['Users.region_id'=>$region_id,'Users.area_id'=>$area_id]);    
                }
               ,'RejectedOrderItems.Products'];
            }
              $regionsTable = TableRegistry::get('Regions');  
              $regions = $regionsTable->find('list')->toArray();

              $this->set('regions',$regions);

             $rejectedOrdersTable = TableRegistry::get('RejectedOrders');
             $rejectedOrders = $rejectedOrdersTable->find('all',[
                       
                       'conditions'=>$conditions,
                       'contain'=>$contain
                  ])->toArray();
            
          if(isset($rejectedOrders) && count($rejectedOrders)>0){
              
                     $this->set('rejectedOrders',$rejectedOrders); 

            } 

      }  
      if( ! $this->request->is( 'post' ) ){  

          $myDate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) ); 

              $todayFilter = date('Y-m-d');
              $rejectedOrdersTable = TableRegistry::get('RejectedOrders');
              $usersTable = TableRegistry::get('Users');
              $regionsTable = TableRegistry::get('Regions');
              $rejectedOrders = $rejectedOrdersTable->find('all',[
                     
                    'conditions' => ['RejectedOrders.date >= ' => $myDate,'RejectedOrders.date <= ' => $todayFilter],
                     'contain'=>['RejectedOrderItems','DeliverySchdules','Users','RejectedOrderItems.Products']
                ])->toArray();
                
              $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray(); 
              $regions = $regionsTable->find('list')->toArray();
              $this->set(compact('regions','rejectedOrders','users')); 
            }
    
    }

}
     