<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class DriverApiController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'index', 'updateProfileDriver', 'logout', 'getAllRoute', 'getSchdule', 'getAllRouteCustomer', 'updateDelivery', 'startTracking', 'stopTracking', 'sendPushToCustomerFromDriver', 'getAllCustomer','updateDeliveryadmin']);
    }

    public function index()
    {
        echo "HI!!!!";die;
    }
    private function validateLogin($data)
    {

        $error = array();
        if (!isset($data['username']) || empty($data['username'])) {
            $error['messageText'] = "Please enter your username";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        } else if (!isset($data['password']) || empty($data['password'])) {
            $error['messageText'] = "Please enter your password";
            $error['messageCode'] = 202;
            $error['successCode'] = 0;
        } else if (!isset($data['type_user']) || empty($data['type_user'])) {
            $error['messageText'] = "Please enter your role";
            $error['messageCode'] = 203;
            $error['successCode'] = 0;
        } else if ($data['type_user'] != 'driver') {
            $error['messageText'] = "Please enter your valid role";
            $error['messageCode'] = 204;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }

    private function updateToken($userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->get($userid);

        $user->token    = \Cake\Utility\Text::uuid();
        $user->modified = date('Y-m-d h:i:s');
        if ($userTable->save($user)) {
            return true;
        } else {
            return false;
        }

    }

    public function login()
    {
        $response = array();
        if ($this->request->is('post')) {

            $data  = $this->request->getData();
            $error = $this->validateLogin($data);
            if ($error['messageCode'] == 200) {

                $username = $data['username'];

                $driverTable = TableRegistry::get('Users');
                $driverInfo  = $driverTable
                    ->find()
                    ->select(['id', 'name'])
                    ->where(['username' => $username])
                    ->andwhere(['password' => md5($data['password'])])
                    ->toArray();

                if (count($driverInfo) > 0) {
                    $updateToken = $this->updateToken($driverInfo[0]['id']);
                    if ($updateToken) {

                        $driverInfo1 = $driverTable
                            ->find('all')
                            //->contain(['Regions','Areas'])
                            ->where(['Users.id' => $driverInfo[0]['id']])
                            ->toArray();

                        //$driverInfo1[0]['houseNo'] = $driverInfo1[0]['houseNo'];/* .$driverInfo1[0]['area']['name'].' '.$driverInfo1[0]['region']['name'];*/
                        //unset($driverInfo1[0]['area']);
                        //unset($driverInfo1[0]['region']);

                        $error['messageText'] = "success";
                        $error['messageCode'] = 200;
                        $error['successCode'] = 1;
                        $error['driverInfo']  = $driverInfo1;
                        $currentDateTime      = date('Y-m-d h:i:s');
                        $dt                   = new \DateTime($currentDateTime);
                        $tz                   = new \DateTimeZone('Asia/Kolkata');
                        $dt->setTimezone($tz);
                        $error['loggedtime'] = $dt->format('H:i:s');

                    } else {

                        $error['messageText'] = "Please enter the valid login details.";
                        $error['messageCode'] = 205;
                        $error['successCode'] = 0;

                    }
                } else {
                    $error['messageText'] = "Please enter the valid login details.";
                    $error['messageCode'] = 205;
                    $error['successCode'] = 0;
                }

            }

        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;
    }

    private function validateLogout($data)
    {

        $error = array();
        if (!isset($data['id']) || empty($data['id'])) {
            $error['messageText'] = "User Id can not be empty";
            $error['messageCode'] = 207;
            $error['successCode'] = 0;
        } else if (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Token can not be empty";
            $error['messageCode'] = 208;
            $error['successCode'] = 0;
        } else if (!$this->notThisUser($data['id'], $data['token'])) {
            $error['messageText'] = "Invalid user";
            $error['messageCode'] = 209;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function emptyToken($userid)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->get($userid);

        $user->token    = '';
        $user->modified = date('Y-m-d h:i:s');
        if ($userTable->save($user)) {
            return true;
        } else {
            return false;
        }

    }

    public function logout()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data  = $this->request->getData();
            $error = $this->validateLogout($data);
            if ($error['messageCode'] == 200) {

                if ($this->emptyToken($data['id'])) {

                    $error['messageText'] = "Logged out Successfully";
                    $error['messageCode'] = 200;
                    $error['successCode'] = 1;

                }

            }

        } else {
            $error['messageText'] = "Invalid Request";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        $response = json_encode($error);
        echo $response;die;

    }

    private function alreadyHaveSomeone($email, $user_id)
    {

        $driverTable = TableRegistry::get('Users');
        $driverInfo  = $driverTable
            ->find()
            ->where(['email_id' => $email])
            ->andwhere(['id <>' => $user_id])
            ->toArray();
        if (count($driverInfo) > 0) {
            return true;
        }return false;

    }

    private function validateupdateProfileDriver($data)
    {
        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageText'] = "User Id can not be empty";
            $error['messageCode'] = 207;
            $error['successCode'] = 0;
        } else if (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Token can not be empty";
            $error['messageCode'] = 208;
            $error['successCode'] = 0;
        } else if (!$this->notThisUser($data['user_id'], $data['token'])) {
            $error['messageText'] = "Invalid user";
            $error['messageCode'] = 209;
            $error['successCode'] = 0;
        } else if (isset($data['email_id']) && !filter_var($data['email_id'], FILTER_VALIDATE_EMAIL)) {

            $error['messageText'] = "Invalid email Id";
            $error['messageCode'] = 210;
            $error['successCode'] = 0;
        } else if (isset($data['email_id']) && $this->alreadyHaveSomeone($data['email_id'], $data['user_id'])) {
            $error['messageText'] = "Email Already occupied";
            $error['messageCode'] = 210;
            $error['successCode'] = 0;

        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }
    private function validateName($name)
    {

        $error = array();
        if (preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $name)) {
            $error['messageText'] = "Invalid Name";
            $error['messageCode'] = 210;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;
    }
    private function validatPhone($phone)
    {

        $error = array();
        if (!preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/', $phone)) {
            $error['messageText'] = "Invalid Phone No.";
            $error['messageCode'] = 211;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function upload_image($content = null, $imgname = null, $dest = null, $extn = null)
    {

        if ($content) {
            $dataarray = explode('base64,', $content);
            if (count($dataarray) == 1) {
                $extn = 'jpg';

                if ($extn) {
                    $img_name = $imgname . "-" . time();
                    $data     = $content;
                    $data     = str_replace(' ', '+', $data);
                    $data     = base64_decode($data);
                    file_put_contents($dest . $img_name . "." . $extn, $data);

                    chmod($dest . $img_name . "." . $extn, 0777);
                    return $img_name . "." . $extn;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function validateImage($base64)
    {

        $imgdata = base64_decode($base64);

        $f = finfo_open();

        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

        $mime_type = explode('/', $mime_type);
        if ($mime_type[0] == 'image') {
            $response['messageCode'] = 200;
        } else {

            $response['messageText'] = "Invalid image";
            $response['messageCode'] = 212;
            $response['successCode'] = 0;

        }

        return $response;

    }

    public function updateProfileDriver()
    {

        $response  = array();
        $imagename = '';
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $response = $this->validateupdateProfileDriver($data);

            if ($response['messageCode'] == 200) {

                if (isset($data['name']) && !empty($data['name'])) {

                    $response = $this->validateName($data['name']);

                    if ($response['messageCode'] == 200) {

                        if (isset($data['phone']) && !empty($data['phone'])) {

                            $response = $this->validatPhone($data['phone']);

                            if ($response['messageCode'] == 200) {

                                if (isset($data['image']) && !empty($data['image'])) {

                                    $response = $this->validateImage($data['image']);

                                    if ($response['messageCode'] == 200) {
                                        $imagename = $this->upload_image($data['image'], 'driver', '../webroot/img/images/');

                                    }

                                }
                            }

                        }

                    }

                }

                if ($response['messageCode'] == 200) {
                    $userTable = TableRegistry::get('Users');
                    $user      = $userTable->get($data['user_id']);

                    if (isset($imagename) && !empty($imagename)) {
                        $user->image = 'img/images/' . $imagename;
                    }
                    if (isset($data['name']) && !empty($data['name'])) {
                        $user->name = $data['name'];
                    }
                    if (isset($data['email_id']) && !empty($data['email_id'])) {
                        $user->email_id = $data['email_id'];
                    }
                    if (isset($data['phone']) && !empty($data['phone'])) {
                        $user->phoneNo = $data['phone'];
                    }
                    if (isset($data['address']) && !empty($data['address'])) {
                        $user->houseNo = $data['address'];
                    }
                    $user->modified = date('Y-m-d h:i:s');
                    if ($userTable->save($user)) {

                        $driverTable = TableRegistry::get('Users');
                        $driverInfo1 = $driverTable
                            ->find('all')
                            // ->contain(['Regions','Areas'])
                            ->where(['Users.id' => $data['user_id']])
                            ->toArray();

                        /*  $driverInfo1[0]['houseNo'] = $driverInfo1[0]['houseNo'] .$driverInfo1[0]['area']['name'].' '.$driverInfo1[0]['region']['name'];
                        unset($driverInfo1[0]['area']);
                        unset($driverInfo1[0]['region']);*/

                        $response['messageText'] = "User Has beed updated Successfully";
                        $response['messageCode'] = 206;
                        $response['successCode'] = 1;
                        $response['updatedInfo'] = $driverInfo1;

                    } else {
                        $response['messageText'] = "something went wrong";
                        $response['messageCode'] = 201;
                        $response['successCode'] = 0;
                    }
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    private function notThisUser($id, $token)
    {

        $driverTable = TableRegistry::get('Users');
        $driverInfo  = $driverTable
            ->find()
            ->select(['id', 'name'])
            ->where(['id' => $id])
            ->andwhere(['token' => $token])
            ->toArray();
        if (count($driverInfo) > 0) {
            return true;
        }return false;

    }

    public function getSchdule()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);

            if ($response['messageCode'] == 200) {
                $driverRouteTable = TableRegistry::get('DeliverySchdules');
                $schdule          = $driverRouteTable->find('all')->hydrate(false)->toArray();

                if (count($schdule) > 0) {

                    foreach ($schdule as $key => $value) {

                        $newDateTime                 = date('h:i A', strtotime($value['start_time']));
                        $schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

                        $newDateTime               = date('h:i A', strtotime($value['end_time']));
                        $schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));

                    }

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = $schdule;

                } else {

                    $response['messageText'] = "success";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['schduleInfo'] = count($schdule);
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }

    public function getAllCustomer($userId, $routeid, $schduleID)
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateupdateRouteDriver($userId, $routeid, $schduleID);
                if ($response['messageCode'] == 200) {
                    $routeCustomerTable = TableRegistry::get('RouteCustomers');

                    /*'status'=>0, was removed*/
                    $routeCustomerInfo = $routeCustomerTable->find()->select('user_id')->where(['route_id' => $routeid, 'delivery_schdule_id' => $schduleID])->hydrate(false)->toArray();

                    $userIds    = array();
                    $schdule_id = $schduleID;
                    if (count($routeCustomerInfo) > 0) {

                        foreach ($routeCustomerInfo as $key => $value) {

                            array_push($userIds, $value['user_id']);
                        }
                        $userIds = array_unique($userIds);
                        // pr($userIds);die;
                        $userTable       = TableRegistry::get('Users');
                        $allUserSubOrder = $userTable->find('all')->contain([
                            'Regions',
                            'Areas',
                            'UserContainers',
                            'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) {
                                return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => date('Y-m-d')]);
                            },
                            /*,
                        'CustomOrders.Products.Units'      => function (\Cake\ORM\Query $query) use ($schdule_id) {
                        return $query->where([
                        'CustomOrders.status'                        => 0]);
                        },*/

                        ])->where(['Users.id IN' => $userIds, 'Users.is_deleted' => 0, 'Users.status' => 1])->hydrate(false)->toArray();

                        $itemsInfno = $this->factorySubscription($allUserSubOrder, $schduleID);
                        $finalinfo  = array();
                        if (!empty($itemsInfno) && isset($itemsInfno[0]['subscriptionInfo']) && !empty($itemsInfno[0]['subscriptionInfo'])) {
                            foreach ($itemsInfno as $key => $value) {
                                $temp = array();
                                foreach ($value['subscriptionInfo'] as $k => $v) {
                                    $subid      = $v['sub_id'];
                                    $schduleid  = $schduleID;
                                    $userid     = $value['customerAddress']['id'];
                                    $transtable = TableRegistry::get('Transactions');
                                    $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type' => 'Dr', 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $schduleid, 'Transactions.user_id' => $userid, 'FIND_IN_SET (' . $subid . ',Transactions.user_subscription_ids)'])->hydrate(false)->toArray();
                                    if (!empty($trans) && count($trans) > 0) {
                                        $temp[] = $trans;
                                    }
                                }
                                if (!empty($temp) && count($temp) > 0) {
                                    continue;
                                } else {
                                    $finalinfo[] = $value;
                                }
                            }
                        }

                        $itemsInfno              = $finalinfo;
                        $allAreaRegionThisRoutes = $this->findRouteRegionAreas($routeid);

                        $response['messageText']            = "success";
                        $response['messageCode']            = 200;
                        $response['successCode']            = 1;
                        $response['user_count']             = count($itemsInfno);
                        $response['allRegionAreaThisRoute'] = $allAreaRegionThisRoutes;
                        $response['routesCustomer']         = $itemsInfno;

                    } else {
                        $response['messageText']    = "There is not any customer on this route.";
                        $response['messageCode']    = 200;
                        $response['successCode']    = 1;
                        $response['routesCustomer'] = [];
                    }
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        return $response;die;
    }

    public function getAllRoute()
    {
        // die('h');
        $response = array();
        $data1 = array(); 
        $fres = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $driverRouteTable = TableRegistry::get('DriverRoutes');
                $driverRoute      = $driverRouteTable->find('all')->where(['DriverRoutes.delivery_schdule_id' => $data['schdule_id'],
                    'DriverRoutes.user_id'                                                                        => $data['user_id']])->contain(['Routes'])->hydrate(false)->toArray();
                $total_customer = 0;
                foreach ($driverRoute as $key => $value) {
                    $customer_count = $this->getAllCustomer($data['user_id'], $value['route_id'], $value['delivery_schdule_id']);
                    //$customer_count =json_decode($customer_count,true);
                    $data1['user_id']    =   $data['user_id'];
                    $data1['token']      =   $data['token'];
                    $data1['route_id']   =   $value['route_id'];
                    $data1['schdule_id'] =   $data['schdule_id'];
                    $routeCustomers = $this->getAllRouteCustomerv2($data1);
                    $value['routecustomers'] = $routeCustomers;
                    array_push($fres, $value);
                    $total_customer = $total_customer + $customer_count['user_count'];
                }
                if (count($driverRoute) > 0) {
                    $response['messageText']    = "success";
                    $response['messageCode']    = 200;
                    $response['successCode']    = 1;
                    $response['customer_count'] = $total_customer;
                    $response['routesInfo']     = $fres;
                } else {
                    $response['messageText']    = "Driver does not have any route with this time";
                    $response['messageCode']    = 261;
                    $response['successCode']    = 1;
                    $response['customer_count'] = $total_customer;
                    $response['routesInfo']     = [];
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    private function validateupdateRouteDriver($userId, $routeId, $schduleId)
    {
        $error                  = array();
        $driverRouteTable       = TableRegistry::get('DriverRoutes');
        $isRouteBelongsToDriver = $driverRouteTable->find()->where(['DriverRoutes.route_id' => $routeId, 'DriverRoutes.user_id' => $userId, 'DriverRoutes.delivery_schdule_id' => $schduleId])->count();
        if ($isRouteBelongsToDriver == 1) {
            $error['messageCode'] = 200;
        } else {
            $error['messageText'] = "This Route not Associated with this Driver.";
            $error['messageCode'] = 281;
            $error['successCode'] = 0;
        }
        return $error;
    }

    private function returnDayName($day = null)
    {

        if ($day && $day != '') {
            $name = $day;
        } else {
            $name = date('l');
        }

        switch ($name) {
            case 'Sunday':
                return 7;
                break;
            case 'Monday':
                return 1;
                break;
            case 'Tuesday':
                return 2;
                break;
            case 'Wednesday':
                return 3;
                break;
            case 'Thursday':
                return 4;
                break;
            case 'Friday':
                return 5;
                break;
            case 'Saturday':
                return 6;
                break;

            default:

                break;
        }

    }
    private function checkTodaySubscriptions($id, $type, $days, $schduleID, $cdate = null)
    {
        if ($cdate) {
            $cdate = $cdate;
            $now   = strtotime($cdate);
        } else {
            $cdate = date('Y-m-d');
            $now   = time();
        }
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $userSubscriptions      = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id' => $id])->hydrate(false)->first();
        if (count($userSubscriptions) > 0) {
            if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {

                /*  if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {
                if ($userSubscriptions['startdate']->format('Y-m-d') <= date('Y-m-d')) {
                $nextdate['comingdate'] = date('Y-m-d');
                } else {
                $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                }
                return $nextdate;
                }
                $nextdate['comingdate'] = $userSubscriptions['startdate']->format('Y-m-d');
                return $nextdate;*/
                $today['comingdate'] = $cdate;
                return $today;
            } else if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate') {
                $startdate       = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                $subscriptionday = strtotime($startdate);
                $now             = $now;
                $datediff        = $now - $subscriptionday;
                $days            = floor($datediff / (60 * 60 * 24)) + 1;

                if ($days < 1) {
                    $today['comingdate'] = $cdate;
                    return $today;
                }
                /*   echo (0%2);
                pr($days);die('hh');*/
                if ($days % 2 == 0) {
                    /*if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {
                    $today['comingdate'] = date('Y-m-d');
                    return $today;
                    } else {
                    $today['comingdate'] = date('Y-m-d');
                    return $today;
                    }*/
                    return false;
                } else {
                    /*if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {
                    $date                   = date('Y-m-d');
                    $date1                  = str_replace('-', '/', $date);
                    $tomorrow               = date('Y-m-d', strtotime($date1 . "+1 days"));
                    $nextdate['comingdate'] = $tomorrow;
                    return $nextdate;
                    } else {
                    return false;
                    }*/
                    $today['comingdate'] = $cdate;
                    return $today;
                }

            }
        }
        return false;
    }
    /* private function checkTodaySubscriptions($id, $type, $days, $schduleID)
    {

    $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
    $userSubscriptions      = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id' => $id])->first();

    if (count($userSubscriptions) > 0) {

    if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'weekly') {

    $days      = explode('-', $userSubscriptions['days']);
    $todayName = $this->returnDayName();
    if (in_array($todayName, $days)) {

    if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {

    return true;
    }
    return false;
    }
    return false;
    } else if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {

    if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {

    return true;
    }
    return false;

    } else if ($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate') {

    $startdate       = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
    $subscriptionday = strtotime($startdate);
    $now             = time();
    $datediff        = $now - $subscriptionday;
    $days            = floor($datediff / (60 * 60 * 24));

    if ($days % 2 == 0) {

    if (in_array($schduleID, explode('-', $userSubscriptions['delivery_schdule_ids']))) {

    return true;
    }
    return false;

    }

    }
    }
    return false;

    }*/

    public function factorySubscription($data, $schduleID, $day = null)
    {
        $final = array();
        foreach ($data as $key => $value) {
            $totalIndividualPrice = 0;
            $final1               = array();
            if ((isset($value['custom_orders']) && !empty($value['custom_orders'])) || (isset($value['user_subscriptions']) && !empty($value['user_subscriptions']))) {
                /* if (isset($value['custom_orders']) && !empty($value['custom_orders'])) {
                $temp1 = array();
                foreach ($value['custom_orders'] as $k => $v) {
                $insideorders                = array();
                $insideorders['sub_id']      = $v['id'];
                $insideorders['name']        = $v['product']['name'];
                $insideorders['pro_id']      = $v['product']['id'];
                $insideorders['iscontainer'] = $v['product']['iscontainer'];
                $insideorders['quantity']    = $v['quantity'];
                $insideorders['unit']        = $v['product']['unit']['name'];
                $totalIndividualPrice        = $totalIndividualPrice + $v['price'];
                $final1[]                    = $insideorders;
                }
                }*/
                if (isset($value['user_subscriptions']) && !empty($value['user_subscriptions'])) {
                    $temp = array();
                    foreach ($value['user_subscriptions'] as $k => $v) {
                        $sids = explode(',', $v['delivery_schdule_ids']);
                        if (!in_array($schduleID, $sids)) {
                            continue;
                        }
                        $modify_data  = $this->getModifydata($v['id']);
                        $insideorders = array();
                        if ($this->checkTodaySubscriptions($v['id'], $v['subscription_type_id'], $v['days'], $schduleID)) {
                            $insideorders['sub_id']         = $v['id'];
                            $insideorders['price_per_unit'] = $v['product']['price_per_unit'];
                            $insideorders['name']           = $v['product']['name'];
                            $insideorders['image']          = $v['product']['image'];
                            $insideorders['pro_id']         = $v['product']['id'];
                            $insideorders['iscontainer']    = $v['product']['iscontainer'];
                            /*$insideorders['quantity']       = $v['quantity'];*/
                            $insideorders['quantity']    = $modify_data['quantity'];
                            $insideorders['modify_data'] = $modify_data['modifieddata'];
                            $insideorders['unit']        = $v['unit_name'];
                            $insideorders['type']        = 'subscription';
                            /*$totalIndividualPrice           = $totalIndividualPrice + $v['subscriptions_total_amount'];*/
                            $totalIndividualPrice = $totalIndividualPrice + $modify_data['totalprice'];

                        }
                        if (count($insideorders) > 0) {
                            $final1[] = $insideorders;
                        }
                    }
                }
                if (count($final1) > 0) {

                    // Add custom order of user
                    $customordersTable = TableRegistry::get('CustomOrders');
                    $orderNext         = $customordersTable->find('all')
                        ->where(['CustomOrders.user_id' => $value['id'],
                            //'CustomOrders.delivery_schdule_id' => $schduleID,
                            'CustomOrders.status'           => 0,
                            //'CustomOrders.created' => $my_new_date,
                        ])->contain(['Products', 'Units'])->hydrate(false)->toArray();
                    $temp1 = array();
                    foreach ($orderNext as $k => $v) {
                        $insideorders                   = array();
                        $insideorders['sub_id']         = $v['id'];
                        $insideorders['price_per_unit'] = $v['product']['price_per_unit'];
                        $insideorders['custome_price']  = $v['price'];
                        $insideorders['name']           = $v['product']['name'];
                        $insideorders['image']          = $v['product']['image'];
                        $insideorders['pro_id']         = $v['product']['id'];
                        $insideorders['iscontainer']    = $v['product']['iscontainer'];
                        $insideorders['quantity']       = $v['quantity'];
                        $insideorders['unit']           = $v['unit']['name'];
                        $insideorders['type']           = 'customorder';
                        $totalIndividualPrice           = $totalIndividualPrice + $v['price'];
                        $final1[]                       = $insideorders;
                    }
                    // pr($orderNext);die;
                    $isNotify = $this->isNotThisCustomer($schduleID, $value['id']);
                    if ($isNotify) {
                        $customerAddress['notified'] = 1;
                    } else {
                        $customerAddress['notified'] = 0;
                    }
                    $containerInfo = $this->customerContainer($value['id']);
                    if ($containerInfo) {
                        $customerAddress['containerNeedToBeCollect'] = $containerInfo['left_container_count'];
                        $customerAddress['containerNeedToBeGiven']   = $containerInfo['container_given'];
                    } else {
                        $customerAddress['containerNeedToBeCollect'] = 0;
                    }
                    $customerAddress['id']                          = $value['id'];
                    $customerAddress['name']                        = $value['name'];
                    $customerAddress['totalOrderSubscriptionPrice'] = $totalIndividualPrice;
                    $customerAddress['phoneNo']                     = $value['phoneNo'];
                    $customerAddress['lat']                         = $value['latitude'];
                    $customerAddress['lng']                         = $value['longitude'];
                    $customerAddress['regionId']                    = $value['region_id'];
                    $customerAddress['areaId']                      = $value['area_id'];
                    $customerAddress['address']                     = $value['houseNo'];
                    $customerAddress['region_name']                 = $value['area']['name'] . ' - ' . $value['region']['name'];
                    $highestInfo                                    = array();
                    $highestInfo['subscriptionInfo']                = $final1;
                    $highestInfo['customerAddress']                 = $customerAddress;
                    $final[]                                        = $highestInfo;
                }
            } else {
                continue;
            }
        }
        return $final;
    }

    private function customerContainer($customerId)
    {
        $usersContainerTable = TableRegistry::get('UserContainers');
        $usersContainer      = $usersContainerTable->find()->where(['user_id' => $customerId])->toArray();
        if (isset($usersContainer) && count($usersContainer) > 0) {
            return $usersContainer[0];
        } else {
            return false;
        }
    }

    private function isNotThisCustomer($schduleID, $customerId)
    {
        $routecustomersTable = TableRegistry::get('RouteCustomers');
        $routecustomers      = $routecustomersTable->find()->where(['user_id' => $customerId, 'delivery_schdule_id' => $schduleID, 'notify_date' => date('Y-m-d'), 'is_notify' => 1])->count();
        return $routecustomers;
    }

    private function findRouteRegionAreas($routeId)
    {
        $routeCustomerTable = TableRegistry::get('RouteCustomers');
        $routeCustomer      = $routeCustomerTable->find('all')->contain(['Regions', 'Areas'])->group(['RouteCustomers.region_id', 'RouteCustomers.area_id'])->where(['route_id' => $routeId])->hydrate(false)->toArray();
        $area_region        = array();
        foreach ($routeCustomer as $key => $value) {
            if(!$value['region']['id']){
                continue;
            }
            $local              = array();
            $local['regionId']  = $value['region']['id'];
            $local['regioName'] = $value['region']['name'];
            $local['areaId']    = $value['area']['id'];
            $local['areaName']  = $value['area']['name'];
            $area_region[]      = $local;
        }
        return $area_region;
    }

    public function getAllRouteCustomer()
    {
        // die('h');
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateupdateRouteDriver($data['user_id'], $data['route_id'], $data['schdule_id']);
                if ($response['messageCode'] == 200) {
                    $routeCustomerTable = TableRegistry::get('RouteCustomers');

                    /*'status'=>0, was removed*/
                    $routeCustomerInfo = $routeCustomerTable->find()->select('user_id')->where(['route_id' => $data['route_id'], 'delivery_schdule_id' => $data['schdule_id']])->hydrate(false)->toArray();

                    $userIds    = array();
                    $schdule_id = $data['schdule_id'];
                    if (count($routeCustomerInfo) > 0) {

                        foreach ($routeCustomerInfo as $key => $value) {

                            array_push($userIds, $value['user_id']);
                        }
                        $userIds = array_unique($userIds);
                        // pr($userIds);die;
                        $userTable       = TableRegistry::get('Users');
                        $allUserSubOrder = $userTable->find('all')->contain([
                            'Regions',
                            'Areas',
                            'UserContainers',
                            'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) {
                                return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => date('Y-m-d')]);
                            },
                            /*,
                        'CustomOrders.Products.Units'      => function (\Cake\ORM\Query $query) use ($schdule_id) {
                        return $query->where([
                        'CustomOrders.status'                        => 0]);
                        },*/

                        ])->where(['Users.id IN' => $userIds, 'Users.is_deleted' => 0, 'Users.status' => 1])->hydrate(false)->toArray();

                        $itemsInfno = $this->factorySubscription($allUserSubOrder, $data['schdule_id']);
                        $finalinfo  = array();
                        if (!empty($itemsInfno) && isset($itemsInfno[0]['subscriptionInfo']) && !empty($itemsInfno[0]['subscriptionInfo'])) {
                            foreach ($itemsInfno as $key => $value) {
                                $temp = array();
                                $cond = array('Dr','rejected');
                                foreach ($value['subscriptionInfo'] as $k => $v) {
                                    $subid      = $v['sub_id'];
                                    $schduleid  = $data['schdule_id'];
                                    $userid     = $value['customerAddress']['id'];
                                    $transtable = TableRegistry::get('Transactions');
                                    $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type IN' => $cond, 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $schduleid, 'Transactions.user_id' => $userid, 'FIND_IN_SET (' . $subid . ',Transactions.user_subscription_ids)'])->hydrate(false)->toArray();
                                    if (!empty($trans) && count($trans) > 0) {
                                        $temp[] = $trans;
                                    }
                                }
                                if (!empty($temp) && count($temp) > 0) {
                                    continue;
                                } else {
                                    $finalinfo[] = $value;
                                }
                            }
                        }

                        $itemsInfno              = $finalinfo;
                        $allAreaRegionThisRoutes = $this->findRouteRegionAreas($data['route_id']);

                        $response['messageText']            = "success";
                        $response['messageCode']            = 200;
                        $response['successCode']            = 1;
                        $response['user_count']             = count($itemsInfno);
                        $response['allRegionAreaThisRoute'] = $allAreaRegionThisRoutes;
                        $response['routesCustomer']         = $itemsInfno;

                    } else {
                        $response['messageText']    = "There is not any customer on this route.";
                        $response['messageCode']    = 200;
                        $response['successCode']    = 1;
                        $response['routesCustomer'] = [];
                    }
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    public function getAllRouteCustomerv2($data)
    {
        // die('h');
        $response = array();
        /*if ($this->request->is('post')) {
            $data     = $this->request->getData();*/
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateupdateRouteDriver($data['user_id'], $data['route_id'], $data['schdule_id']);
                if ($response['messageCode'] == 200) {
                    $routeCustomerTable = TableRegistry::get('RouteCustomers');

                    /*'status'=>0, was removed*/
                    $routeCustomerInfo = $routeCustomerTable->find()->select('user_id')->where(['route_id' => $data['route_id'], 'delivery_schdule_id' => $data['schdule_id']])->hydrate(false)->toArray();

                    $userIds    = array();
                    $schdule_id = $data['schdule_id'];
                    if (count($routeCustomerInfo) > 0) {

                        foreach ($routeCustomerInfo as $key => $value) {

                            array_push($userIds, $value['user_id']);
                        }
                        $userIds = array_unique($userIds);
                        // pr($userIds);die;
                        $userTable       = TableRegistry::get('Users');
                        $allUserSubOrder = $userTable->find('all')->contain([
                            'Regions',
                            'Areas',
                            'UserContainers',
                            'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) {
                                return $query->where(['UserSubscriptions.users_subscription_status_id' => 1, 'UserSubscriptions.startdate <=' => date('Y-m-d')]);
                            },
                            /*,
                        'CustomOrders.Products.Units'      => function (\Cake\ORM\Query $query) use ($schdule_id) {
                        return $query->where([
                        'CustomOrders.status'                        => 0]);
                        },*/

                        ])->where(['Users.id IN' => $userIds, 'Users.is_deleted' => 0, 'Users.status' => 1])->hydrate(false)->toArray();

                        $itemsInfno = $this->factorySubscription($allUserSubOrder, $data['schdule_id']);
                        $finalinfo  = array();
                        $cond = array('Dr','rejected');
                        if (!empty($itemsInfno) && isset($itemsInfno[0]['subscriptionInfo']) && !empty($itemsInfno[0]['subscriptionInfo'])) {
                            foreach ($itemsInfno as $key => $value) {
                                $temp = array();
                                foreach ($value['subscriptionInfo'] as $k => $v) {
                                    $subid      = $v['sub_id'];
                                    $schduleid  = $data['schdule_id'];
                                    $userid     = $value['customerAddress']['id'];
                                    $transtable = TableRegistry::get('Transactions');
                                    $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids'])->where(['Transactions.transaction_amount_type IN' => $cond, 'Transactions.created' => date('Y-m-d'), 'Transactions.delivery_schdule_id' => $schduleid, 'Transactions.user_id' => $userid, 'FIND_IN_SET (' . $subid . ',Transactions.user_subscription_ids)'])->hydrate(false)->toArray();
                                    if (!empty($trans) && count($trans) > 0) {
                                        $temp[] = $trans;
                                    }
                                }
                                if (!empty($temp) && count($temp) > 0) {
                                    continue;
                                } else {
                                    $finalinfo[] = $value;
                                }
                            }
                        }

                        $itemsInfno              = $finalinfo;
                        $allAreaRegionThisRoutes = $this->findRouteRegionAreas($data['route_id']);

                        /*$response['messageText']            = "success";
                        $response['messageCode']            = 200;
                        $response['successCode']            = 1;*/
                        $response['user_count']             = count($itemsInfno);
                        $response['allRegionAreaThisRoute'] = $allAreaRegionThisRoutes;
                        $response['routesCustomer']         = $itemsInfno;

                    } else {
                        $response['messageText']    = "There is not any customer on this route.";
                        $response['messageCode']    = 200;
                        $response['successCode']    = 1;
                        $response['routesCustomer'] = [];
                    }
                }
            }

        /*} else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;*/
        return $response;
    }

    private function validateCustomerId($user_id)
    {
        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id' => $user_id])->count();
        if ($user == 1) {

            $error['messageCode'] = 200;
        } else {

            $error['messageText'] = "This Route not Associated with this Driver.";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }
        return $error;

    }
    private function notThisUserUpdate($updateId)
    {

        $userTable = TableRegistry::get('Users');
        $users     = $userTable->find()->where(['id' => $updateId])->count();

        if ($users) {
            return true;
        }return false;

    }
    private function productExist($productId)
    {

        $product = TableRegistry::get('Products');

        $users = $product->find()->where(['id' => $productId])->count();

        if ($users) {
            return true;
        }return false;

    }
    private function notValidItems($items)
    {

        if (!isset($items[0]['name']) || count($items[0]['name']) == 0) {

            return true;
        } else if (!isset($items[0]['id']) || count($items[0]['id']) == 0) {

            return true;
        } else if (!$this->productExist($items[0]['id'])) {

            return true;
        }

    }
    private function validOneResponse($data)
    {

        $error    = array();
        $delivere = array('yes', 'no');
        if (!isset($data['userId']) || empty($data['userId'])) {
            $error['messageText'] = "User Id in raw dara can not be empty";
            $error['messageCode'] = 400;
            $error['successCode'] = 0;
        } else if ((!isset($data['containerGiven']) || empty($data['containerGiven'])) && ($data['containerGiven'] != 0)) {
            $error['messageText'] = "Container given can not be empty";
            $error['messageCode'] = 401;
            $error['successCode'] = 0;
        } else if ((!isset($data['containerCollect']) || empty($data['containerCollect'])) && ($data['containerCollect'] != 0)) {
            $error['messageText'] = "Container collect can not be empty";
            $error['messageCode'] = 402;
            $error['successCode'] = 0;
        } else if (!isset($data['totalOrderSubscriptionPrice']) || empty($data['totalOrderSubscriptionPrice'])) {
            $error['messageText'] = "Total amount can not be empty";
            $error['messageCode'] = 208;
            $error['successCode'] = 0;
        } else if (!isset($data['delivered']) || empty($data['delivered'])) {
            $error['messageText'] = "Delivered value  can not be empty";
            $error['messageCode'] = 410;
            $error['successCode'] = 0;
        } else if (!in_array($data['delivered'], $delivere)) {

            $error['messageText'] = "Delivered value invalid";
            $error['messageCode'] = 411;
            $error['successCode'] = 0;

        } else if (($data['delivered'] == 'yes') && (isset($data['reason']))) {

            $error['messageText'] = "Delivered value 1 but reason is present";
            $error['messageCode'] = 411;
            $error['successCode'] = 0;
        } else if (($data['delivered'] == 'no') && (!isset($data['reason']))) {

            $error['messageText'] = "Delivered value 0 but reason is absent";
            $error['messageCode'] = 411;
            $error['successCode'] = 0;
        } else if (!$this->notThisUserUpdate($data['userId'])) {
            $error['messageText'] = "Invalid user";
            $error['messageCode'] = 403;
            $error['successCode'] = 0;
        } else if ($this->notValidItems($data['items'])) {
            $error['messageText'] = "Please check the all items in Raw data properly";
            $error['messageCode'] = 404;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;

    }
    private function validateOrderString($data)
    {

        $res = json_decode(stripslashes($data), true);

        if ($res === null) {
            $error['messageText'] = "Data format not supported";
            $error['messageCode'] = 363;
            $error['successCode'] = 0;
        } else {

            $error['messageCode'] = 200;

        }

        if ($error['messageCode'] == 200) {

            foreach ($res as $key => $value) {

                $error = $this->validOneResponse($value);
                if (!$error['messageCode'] == 200) {

                    return $error;

                }

            }

        }

        return $error;

    }

    private function notTimingExist($schduleId)
    {

        $delivery_schdule_Table = TableRegistry::get('DeliverySchdules');
        $isExist                = $delivery_schdule_Table->find()->where(['DeliverySchdules.id' => $schduleId])->count();
        if ($isExist) {
            return true;
        } else {
            return false;
        }

    }
    private function validateupdateUpdateDelivery($data)
    {
        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageText'] = "User Id can not be empty";
            $error['messageCode'] = 207;
            $error['successCode'] = 0;
        } else if (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Token can not be empty";
            $error['messageCode'] = 208;
            $error['successCode'] = 0;
        } else if (!isset($data['schdule_id']) || empty($data['schdule_id'])) {
            $error['messageText'] = "Delivery Update Timing can not be empty";
            $error['messageCode'] = 362;
            $error['successCode'] = 0;
        } else if (!$this->notTimingExist($data['schdule_id'])) {
            $error['messageText'] = "Invalid Timing Id";
            $error['messageCode'] = 361;
            $error['successCode'] = 0;
        } else if (!$this->notThisUser($data['user_id'], $data['token'])) {
            $error['messageText'] = "Invalid user";
            $error['messageCode'] = 209;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }
        return $error;

    }

    private function updateContainer($customer_Id, $containerGiven, $containerCollect)
    {

        $usercontainerTable = TableRegistry::get('UserContainers');
        $usercontainer      = $usercontainerTable->find()->where(['user_id' => $customer_Id])->first();

        /*    if($containerCollect == 0)
        {

        $left_container_count = $usercontainer['container_given'] + $containerGiven;

        $query = $usercontainerTable->query();
        $result = $query->update()
        ->set(['container_given' => $containerGiven ,'container_collect' => $containerCollect,'left_container_count'=>$left_container_count])
        ->where(['user_id' => $customer_Id])
        ->execute();
        if($result){
        return true;
        }return false;
        }else{*/

        $left_container_count = (int) (($usercontainer['left_container_count'] + $containerGiven) - $containerCollect);

        $query  = $usercontainerTable->query();
        $result = $query->update()
            ->set(['container_given' => $containerGiven, 'container_collect' => $containerCollect, 'left_container_count' => $left_container_count])
            ->where(['user_id' => $customer_Id])
            ->execute();
        if ($result) {
            return true;
        }return false;

        //}

    }

    private function alreadyUpdated($customerId, $schduleId, $delivere, $reson, $items, $deliverddata)
    {
        $sub    = json_decode($deliverddata, true);
        $sub_id = '';
        if (isset($sub['subscription_ids']) && !empty($sub['subscription_ids'])) {
            $sub_id = implode(',', json_decode($sub['subscription_ids']));
        } else {
            $response['messageText'] = "Subscriptions ids can't be empty to sync data.";
            $response['messageCode'] = 1978;
            $response['successCode'] = 0;
            echo json_encode($response);die;
        }
        if (isset($sub['date']) && !empty($sub['date'])) {
            $date = $sub['date'];
        } else {
            $date = date('Y-m-d');
        }
        
        $transtable = TableRegistry::get('Transactions');
        $trans      = $transtable->find()->where(['user_id' => $customerId, 'created' => $date, 'delivery_schdule_id' => $schduleId, 'transaction_type_id' => 6])->count();
        if ($trans > 0) {
            return true;
        } else {
            return false;
        }
        if ($delivere == "yes") {
            $status     = 1;
            $transtable = TableRegistry::get('Transactions');
            $trans      = $transtable->find()->where(['user_id' => $customerId, 'transaction_amount_type' => 'Dr', 'created' => $date, 'delivery_schdule_id' => $schduleId, 'status' => 1, 'transaction_type_id' => 6, 'user_subscription_ids' => $sub_id])->count();
            if ($trans) {
                return true;
            } else {
                return false;
            }
        } else if ($delivere == "no") {
            $status     = 0;
            $transtable = TableRegistry::get('Transactions');
            $trans      = $transtable->find()->where(['user_id' => $customerId, 'transaction_amount_type' => 'rejected', 'created' => $date, 'delivery_schdule_id' => $schduleId, 'status' => 0, 'transaction_type_id' => 6, 'user_subscription_ids' => $sub_id])->count();
            if ($trans) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function AddTransaction($useritems, $schduleId, $totalPrice, $orderdata)
    {
        $trans_id = 0;
        if (isset($useritems['date']) && !empty($useritems['date'])) {
            $date = $useritems['date'];
        } else {
            $date = date('Y-m-d');
        }
        $sub_id                 = (isset($useritems['subscription_ids']) && $useritems['subscription_ids']) ? implode(',', json_decode($useritems['subscription_ids'])) : '';
        $custom_id              = (isset($useritems['custom_ids']) && $useritems['custom_ids']) ? implode(',', json_decode($useritems['custom_ids'])) : '';
        $reason                 = (isset($useritems['reason']) && $useritems['reason']) ? $useritems['reason'] : '';
        $customOrderTable       = TableRegistry::get('CustomOrders');
        $transactionTable       = TableRegistry::get('Transactions');
        $userBalanceTable       = TableRegistry::get('UserBalances');
        $productsTable          = TableRegistry::get('Products');
        $UserSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        if ($totalPrice > 0) {
            if ($useritems['delivered'] === "no") {
                $userBalance = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $useritems['userId']])->first();
                $leftBalance                          = $userBalance['balance'];
                $transaction                          = $transactionTable->newEntity();
                $transaction->user_id                 = $useritems['userId'];
                $transaction->transaction_amount_type = 'rejected';
                $transaction->amount                  = $totalPrice;
                $transaction->balance                 = $leftBalance;
                $transaction->created                 = $date;
                $datetime                             = new \DateTime();
                $tz        = new \DateTimeZone('Asia/Kolkata');
                $datetime->setTimezone($tz);
                $transaction->time                    = date_format($datetime, 'H:i:s');
                $transaction->delivery_schdule_id     = $schduleId;
                $transaction->status                  = 0;
                $transaction->transaction_type        = 'subscription/order';
                $transaction->transaction_type_id     = 6;
                $transaction->order_data              = $orderdata;
                $transaction->user_subscription_ids   = $sub_id;
                $transaction->custom_order_ids        = $custom_id;
                $transaction->rejected                = 1;
                $transaction->rejected_reason         = $reason;
                if ($result = $transactionTable->save($transaction)) {
                    $trans_id = $result->id;
                    $query = $customOrderTable->query();
                    if ($custom_id) {
                        $useritems['custom_ids'] = explode(',', $custom_id);
                        foreach ($useritems['custom_ids'] as $key => $value) {
                            $result = $query->update()
                                ->set(['status' => 1, 'created' => $date, 'rejected' => 1, 'rejected_reason' => $reason, 'delivery_schdule_id' => $schduleId])
                                ->where(['id' => $value])
                                ->execute();
                        }
                    }
                    if ($sub_id) {
                        $subids = explode(',', $sub_id);
                        foreach ($subids as $sk => $sv) {
                            // reset values
                            $query = $UserSubscriptionsTable->query();
                            $query->update()
                                ->set(['delivery_date' => null, 'delivery_time' => null, 'modified_quantity' => null])
                                ->where(['id' => $sv])
                                ->execute();

                            /*$key1 = array_search($sv, array_column($subscriptionItems, 'sub_id'));
                        $usersub = $UserSubscriptionsTable->find()->where('id'=> $sv)->hydrate(false)->first();
                        $product = $productsTable->find()->where('id'=> $usersub['product_id'])->hydrate(false)->first();
                        $newcustomorder = $customOrderTable->newEntity();
                        $newcustomorder->user_id = $useritems['userId'];
                        $newcustomorder->product_id = $subscriptionItems[$key1]['id'];
                        $newcustomorder->quantity = $subscriptionItems[$key1]['quantity'];
                        $newcustomorder->price = $subscriptionItems[$key1]['pro_price'];
                        $newcustomorder->unit_id = $usersub['unit_id'];
                        $newcustomorder->delivery_schdule_id = $schduleId;
                        $newcustomorder->status = 2;
                        $newcustomorder->unit_name = $subscriptionItems[$key1]['unit_name'];
                        $newcustomorder->price_per_unit = $product['price_per_unit'];
                        $newcustomorder->product_name = $subscriptionItems[$key1]['name'];
                        $newcustomorder->created  = date('Y-m-d');
                        $newcustomorder->modified = date('Y-m-d h:i:s');

                        $result1  = $userTable->save($newcustomorder); */
                        }
                    }
                }
            } else {
                $userBalance1 = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $useritems['userId']])->first();
                $leftBalance1 = ($userBalance1['balance'] - $totalPrice);
                $transaction                          = $transactionTable->newEntity();
                $transaction->user_id                 = $useritems['userId'];
                $transaction->transaction_amount_type = 'Dr';
                $transaction->amount                  = $totalPrice;
                $transaction->balance                 = $leftBalance1;
                $transaction->created                 = $date;
                $datetime                             = new \DateTime();
                $tz        = new \DateTimeZone('Asia/Kolkata');
                $datetime->setTimezone($tz);
                $transaction->time                    = date_format($datetime, 'H:i:s');
                $transaction->delivery_schdule_id     = $schduleId;
                $transaction->status                  = 1;
                $transaction->transaction_type        = 'subscription/order';
                $transaction->transaction_type_id     = 6;
                $transaction->order_data              = $orderdata;
                $transaction->user_subscription_ids   = $sub_id;
                $transaction->custom_order_ids        = $custom_id;
                if ($result = $transactionTable->save($transaction)) {
                    $trans_id = $result->id;
                    $query = $customOrderTable->query();
                    if ($custom_id) {
                        $useritems['custom_ids'] = explode(',', $custom_id);
                        foreach ($useritems['custom_ids'] as $key => $value) {
                            $result = $query->update()
                                ->set(['status' => 1, 'created' => $date, 'delivery_schdule_id' => $schduleId])
                                ->where(['id' => $value])
                                ->execute();
                        }
                    }
                    if ($sub_id) {
                        $subids = explode(',', $sub_id);
                        foreach ($subids as $sk => $sv) {
                            // reset values
                            $query = $UserSubscriptionsTable->query();
                            $query->update()
                                ->set(['delivery_date' => null, 'delivery_time' => null, 'modified_quantity' => null])
                                ->where(['id' => $sv])
                                ->execute();

                            /*$usersub = $UserSubscriptionsTable->find()->where('id'=> $sv)->hydrate(false)->first();
                        $product = $productsTable->find()->where('id'=> $usersub['product_id'])->hydrate(false)->first();

                        $newcustomorder = $customOrderTable->newEntity();
                        $newcustomorder->user_id = $useritems['userId'];
                        $newcustomorder->product_id = $subscriptionItems[$key1]['id'];
                        $newcustomorder->quantity = $subscriptionItems[$key1]['quantity'];
                        $newcustomorder->price = $subscriptionItems[$key1]['pro_price'];
                        $newcustomorder->unit_id = $usersub['unit_id'];
                        $newcustomorder->delivery_schdule_id = $schduleId;
                        $newcustomorder->status = 2;
                        $newcustomorder->unit_name = $subscriptionItems[$key1]['unit_name'];
                        $newcustomorder->price_per_unit = $product['price_per_unit'];
                        $newcustomorder->product_name = $subscriptionItems[$key1]['name'];
                        $newcustomorder->created  = date('Y-m-d');
                        $newcustomorder->modified = date('Y-m-d h:i:s');

                        $result1  = $userTable->save($newcustomorder); */
                        }
                    }
                    $userBalance = $userBalanceTable->find()->select(['balance'])->where(['user_id' => $useritems['userId']])->first();
                    if(!$userBalance){
                        $UserBalances          = $userBalanceTable->newEntity();
                        $UserBalances->user_id = $useritems['userId'];
                        $UserBalances->balance = 0 - $totalPrice;
                        $leftBalance = 0 - $totalPrice;
                        $userBalanceTable->save($UserBalances);
                    }else{
                        $leftBalance = ($userBalance['balance'] - $totalPrice);
                        $query       = $userBalanceTable->query();
                        $result      = $query->update()
                            ->set(['balance' => $leftBalance])
                            ->where(['user_id' => $useritems['userId']])
                            ->execute();
                    }
                    $msg       = "Rs.$totalPrice deducted from your account. Your available balance is Rs.$leftBalance";
                    $userTable = TableRegistry::get('Users');
                    $user      = $userTable->find()->where(['id' => $useritems['userId']])->hydrate(false)->first();
                    if ($user) {
                        if (isset($user['device_id'])) {
                            $this->addpush1($user['device_id'], $msg);
                        }
                        if (isset($user['phoneNo'])) {
                            $this->sendSMS($user['phoneNo'], $msg);
                        }
                    }
                }
            }
        }
        return $trans_id;
    }

    public function updateDelivery()
    {   
        $trans_response = array();
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateUpdateDelivery($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateOrderString($data['data']);
                if ($response['messageCode'] == 200) {
                    $resultedString = json_decode($data['data'], true);

                    $routecustomersTable = TableRegistry::get('RouteCustomers');
                    foreach ($resultedString as $key => $value) {
                        $customer_Id      = $value['userId'];
                        $containerGiven   = $value['containerGiven'];
                        $containerCollect = $value['containerCollect'];
                        $deliverdvalue    = $value['delivered'];
                        $deliverddata     = json_encode($value);
                        $reson            = '';
                        if (!$this->alreadyUpdated($customer_Id, $data['schdule_id'], $deliverdvalue, $reson, $value, $deliverddata)) {
                            $transactionUpdate = $this->AddTransaction($value, $data['schdule_id'], $value['totalOrderSubscriptionPrice'], $data['data']);
                            $value['transaction_id'] = $transactionUpdate;
                            array_push($trans_response,$value);
                            $updateContiner    = $this->updateContainer($customer_Id, $containerGiven, $containerCollect);
                        } else {
                            continue;
                        }

                    }

                    $response['messageText'] = "Updated Successfully";
                    $response['transaction_data']   = $trans_response;
                    $response['messageCode'] = 331;
                    $response['successCode'] = 1;

                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    public function updateDeliveryadmin()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
                $response = $this->validateOrderString($data['data']);
                if ($response['messageCode'] == 200) {
                    $resultedString = json_decode($data['data'], true);

                    $routecustomersTable = TableRegistry::get('RouteCustomers');
                    foreach ($resultedString as $key => $value) {
                        $customer_Id      = $value['userId'];
                        $containerGiven   = $value['containerGiven'];
                        $containerCollect = $value['containerCollect'];
                        $deliverdvalue    = $value['delivered'];
                        $deliverddata     = json_encode($value);
                        $reson            = '';
                        if (!$this->alreadyUpdated($customer_Id, $data['schdule_id'], $deliverdvalue, $reson, $value, $deliverddata)) {
                            $transactionUpdate = $this->AddTransaction($value, $data['schdule_id'], $value['totalOrderSubscriptionPrice'], $data['data']);
                        } else {
                            continue;
                        }

                    }

                    $response['messageText'] = "Updated Successfully";
                    $response['messageCode'] = 331;
                    $response['successCode'] = 1;

                }
            
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }
    
    private function validateLatLng($data)
    {
        //    preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $geoResults['latitude']);
        $response = array();
        if (!isset($data['lat']) || empty($data['lat'])) {
            $response['messageText'] = "Please enter Latitude";
            $response['messageCode'] = 232;
            $response['successCode'] = 0;
        } else if (!isset($data['lng']) || empty($data['lng'])) {
            $response['messageText'] = "Please enter Longitude";
            $response['messageCode'] = 233;
            $response['successCode'] = 0;
        } else if (preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $data['lat'])) {
            $response['messageText'] = "Invalid Latitude";
            $response['messageCode'] = 234;
            $response['successCode'] = 0;
        } else if (preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $data['lng'])) {
            $response['messageText'] = "Invalid Longitude";
            $response['messageCode'] = 235;
            $response['successCode'] = 0;
        }
        if (count($response) == 0) {
            $response['messageCode'] = 200;
        }
        return $response;
    }

    public function startTracking()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $response = $this->validateLatLng($data);
                if ($response['messageCode'] == 200) {
                    $orderTrackingTable = TableRegistry::get('OrderTrackings');
                    $orderTracking      = $orderTrackingTable->find()->where(['user_id' => $data['user_id']])->count();
                    if ($orderTracking == 0) {
                        $orderTracking          = $orderTrackingTable->newEntity();
                        $orderTracking->user_id = $data['user_id'];
                        $orderTracking->lat     = $data['lat'];
                        $orderTracking->lng     = $data['lng'];
                        if ($orderTrackingTable->save($orderTracking)) {
                            $response['messageText'] = "Tracking Saved Successfully";
                            $response['messageCode'] = 231;
                            $response['successCode'] = 1;
                            $currentDateTime         = date('Y-m-d h:i:s');
                            $dt                      = new \DateTime($currentDateTime);
                            $tz                      = new \DateTimeZone('Asia/Kolkata');
                            $dt->setTimezone($tz);
                            $response['loggedtime'] = $dt->format('Y-m-d H:i:s');
                        }
                    } else {
                        $currentDateTime = date('Y-m-d h:i:s');
                        $query           = $orderTrackingTable->query();
                        $result          = $query->update()
                            ->set(['lat' => $data['lat'], 'lng' => $data['lng']])
                            ->where(['user_id' => $data['user_id']])
                            ->execute();
                        if ($result) {
                            $response['messageText'] = "Tracking Saved Successfully";
                            $response['messageCode'] = 231;
                            $response['successCode'] = 1;
                            $dt                      = new \DateTime($currentDateTime);
                            $tz                      = new \DateTimeZone('Asia/Kolkata');
                            $dt->setTimezone($tz);

                            //$response['loggedtime'] = $dt->format('Y-m-d a H:i:s');
                            date_default_timezone_set('Asia/Kolkata');
                            $response['loggedtime'] = date('Y-m-d H:i:s');
                        } else {
                            $response['messageText'] = "Something Went Wrong Please try again";
                            $response['messageCode'] = 201;
                            $response['successCode'] = 0;
                        }

                    }

                }

            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    public function stopTracking()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $response = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {
                $orderTrackingTable = TableRegistry::get('OrderTrackings');
                $query              = $orderTrackingTable->query();
                $result             = $query->update()
                    ->set(['lat' => '', 'lng' => ''])
                    ->where(['user_id' => $data['user_id']])
                    ->execute();
                if ($result) {
                    $response['messageText'] = "Tracking Stoped Successfully";
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                } else {
                    $response['messageText'] = "Something Went Wrong Please try again";
                    $response['messageCode'] = 201;
                    $response['successCode'] = 0;
                }
            }
        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;
    }

    /*--status code reserve for getCustomerBalance function Start from 2051 to 2100---*/

    public function sendPushToCustomerFromDriver()
    {

        $response = array();
        if ($this->request->is('post')) {

            $data['user_id'] = $this->request->getData('driver_id');
            $data['token']   = $this->request->getData('token');
            $response        = $this->validateupdateProfileDriver($data);
            if ($response['messageCode'] == 200) {

                $isDSTExist = $this->notTimingExist($this->request->getData('schdule_id'));

                if ($isDSTExist) {

                    $customerId = $this->request->getData('customer_id');
                    if (!isset($customerId) || empty($customerId)) {

                        $response['messageText'] = "Customer id can not be empty";
                        $response['messageCode'] = 2051;
                        $response['successCode'] = 0;

                    } else {

                        $checkUserExist = $this->isUserExist($this->request->getData('customer_id'));

                        if ($checkUserExist) {

                            $userTable = TableRegistry::get('Users');
                            $user      = $userTable->find()->where(['id' => $customerId, 'type_user' => 'customer'])->select(['device_id'])->toArray();
                            if (count($user) > 0) {

                                $deviceids = $user[0]['device_id'];
                                $res       = $this->sendFCM($deviceids);
                                if ($res) {

                                    $this->updateRouteNotifyCustomer($this->request->getData('schdule_id'), $customerId);

                                    $response['messageText'] = "Push Message Sent Successfully";
                                    $response['messageCode'] = 2054;
                                    $response['successCode'] = 1;

                                } else {

                                    $response['messageText'] = "Something went wrong.Not Sent Push Message";
                                    $response['messageCode'] = 2055;
                                    $response['successCode'] = 1;
                                }

                            } else {

                                $response['messageText'] = "Device ID not Registered With Cremeway";
                                $response['messageCode'] = 2053;
                                $response['successCode'] = 0;
                            }

                        } else {

                            $response['messageText'] = "Customer id can not be recognized";
                            $response['messageCode'] = 2052;
                            $response['successCode'] = 0;

                        }

                    }

                } else {
                    $response['messageText'] = "Invalid Timing Id";
                    $response['messageCode'] = 361;
                    $response['successCode'] = 0;
                }
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        echo json_encode($response);die;

    }
    private function updateRouteNotifyCustomer($schdule_id, $customer_id)
    {

        $route_customerTable = TableRegistry::get('RouteCustomers');
        $query               = $route_customerTable->query();
        $result_update       = $query->update()
            ->set(['notify_date ' => date('Y-m-d'), 'is_notify' => 1])
            ->where(['user_id' => $customer_id, 'delivery_schdule_id' => $schdule_id])
            ->execute();
        return true;

    }

    private function isUserExist($customer_id)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id' => $customer_id, 'type_user' => 'customer'])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function sendFCM($deviceid)
    {
        return true;
        $fcmApiKey = 'AIzaSyBBQ245rIcvNo-WMQwPsOUOVF8lvZyThmU';

        $url = 'https://fcm.googleapis.com/fcm/send';

        /*$deviceId = 'eu-C4spFaRM:APA91bHum5PFoqdHhABnaiOG1kxRDIZGaPEoZYo7N8gKgJDlz7lFo2flIyKY5WBkxpPTcNZOKXHNdg0OVUSARUfpADv-wD1tpsaCYBGH3DizDsr_RSz3oDCfL0_wKu2qzGNk3SBK6UED';*/
        $deviceId = $deviceid;

        $messageInfo = "Cremeway Driver Sunder has been Arrived Please ready to take milk";

        $dataArr                = array();
        $id                     = array();
        $id[]                   = $deviceId;
        $dataArr['device_id']   = $id;
        $dataArr['message_key'] = $messageInfo;

        $registrationIds = $dataArr['device_id'];

        $message = $dataArr['message_key'];
        $title   = 'This is a title. title';

        $msg    = array('message' => $message, 'title' => $title);
        $fields = array(
            'registration_ids' => $registrationIds,
            'data'             => $msg,
            'priority'         => 'high',
        );

        $headers = array(
            'Authorization: key=' . $fcmApiKey,
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        } else {

        }
        curl_close($ch);
        $res = json_decode($result);
        if ($res->success == 1) {
            return true;
        } else {
            return false;
        }

    }

}
