<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
 
class PermissionsController extends AppController
{
    public function initialize()
        {

            parent::initialize();
        }
    
    public function index()
    {
         $groupTable = TableRegistry::get('Groups');
         $groupdata = $groupTable->find('all')->hydrate(false)->toArray();
         $this->set('groupdata',$groupdata);
         $this->set('modules',$this->getControllers()); 
         if($this->request->is('post')){
              $error = $this->validateaddPermission($this->request->getData());
              if($error['statuscode'] == 200)
              {
                  $entity = $groupTable->newEntity();
                  $entity->name = trim($this->request->getData('name'));
                  $entity->created = date('y-m-d h:i:s');
                  $entity->modified = date('y-m-d h:i:s'); 
                  $result = $groupTable->save($entity);
                  if ($result) {
                    
                    $group_id=$result->id;
                    $grantedmoduleList = $this->request->getData('permission');
                    if($group_id && !empty($group_id))
                    {  
                      if(isset($grantedmoduleList) && count($grantedmoduleList)>0){
   
                              foreach ($grantedmoduleList as $ke => $val) {
                                
                              shell_exec('/var/www/html/cremeway/bin/cake acl grant Groups.'.$group_id.' controllers/'.$val);
                              
                              }

                          }
                    }

                    return $this->redirect(['action'=>'index']);
                        

                        }
                }else{
                  $this->set('error',$error);
                  $this->set('data',$this->request->getData());
                }    
         

         }
         
    }

     private function getRole( $name ){
             
             $untTable = TableRegistry::get('groups');
             $singleUnit = $untTable->find()
                                    ->where(['name'=>$name])
                                    ->count();
                                     
             return $singleUnit;
   }


    private function validateaddPermission( $data ){
                 
                  $error = array();
                  if( ! isset( $data['name'] ) || empty( trim( $data['name']) ) ) {
                     $error['name'] = 'Please enter the Role name';
                  }else if( $this->getRole( trim( $data['name'] ) ) ){
                    $error['name'] = 'Role name already taken';
                  }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Role name';
                  }else if(strlen($data['name']) > 50 ){
                     $error['name'] = 'Please enter Valid Role name in 50 character Range';
        
                  }else if( ! isset($data['permission']) || count($data['permission']) == 0){

                     $error['name'] = 'Please select atleast one permission other wise this user can not be logged in';
                  }
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
       return $error;
    }


    private function getifnameExistTwice( $id, $name ){


             $getname = TableRegistry::get('groups');

             $getnameexist = $getname->find()
                                     ->where(['AND'=>['name'=>$name,'id <>'=>$id]])
                                     ->count();
             return $getnameexist;


     }


    private function validateEditPermission( $data ){
        
        $error = array();
        if( ! isset( $data['name'] ) || empty( trim($data['name']) ) ){
            $error['name'] = 'Role name can not be empty';
        }else if($this->getifnameExistTwice($data['id'],$data['name'])){

               $error['name'] = 'This Role already taken'; 
        }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Role name';
        
        }else if(strlen($data['name']) > 50 ){
                     $error['name'] = 'Please enter Valid Role name in 50 character Range';
        
        }else if( ! isset($data['newpermission']) || count($data['newpermission']) == 0){

           $error['name'] = 'Please select atleast one permission other wise this user can not be logged in';
        }

        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;
    }

    
    public function edit( $id ){
         
         $error = array();
         $groupTable = TableRegistry::get('groups');
         if($this->request->is('post')){
             
               $error = $this->validateEditPermission($this->request->getData());
               if($error['statuscode'] == 200){
                      
                        $group = $groupTable->get($this->request->getData('id'));  

                        $group->name = trim($this->request->getData('name'));
                        $group->modified = date('y-m-d h:i:s');
                        

                        $newpermission =$this->request->getData('newpermission');
                        
                        $userPermission['group_id'] = $this->request->getData('id');
                        $permissionsLists = $this->makeAuthorizedFromAdmin($userPermission);
                        $oldpermission = $permissionsLists['controller'];
                        
                        $deniedmoduleList=array_values(array_diff($oldpermission,$newpermission));
                        
                         
                         
                        if(isset($deniedmoduleList) && count($deniedmoduleList)>0){

                            foreach ($deniedmoduleList as $key => $value) {
                              
                            shell_exec('/var/www/html/cremeway/bin/cake acl deny Groups.'.$this->request->getData('id').' controllers/'.$value);
                            
                            }

                        } 

                        $grantedmoduleList=array_values(array_diff($newpermission,$oldpermission));

                         
                        if(isset($grantedmoduleList) && count($grantedmoduleList)>0){
 
                            foreach ($grantedmoduleList as $ke => $val) {
                              
                            shell_exec('/var/www/html/cremeway/bin/cake acl grant Groups.'.$this->request->getData('id').' controllers/'.$val);
                            
                            }

                        }  



                        
                               



                                if($groupTable->save($group)){
                                    return $this->redirect(['action'=>'index']);
                                }


               }else{

                      
                     $userPermission['group_id'] = $this->request->getData('id');

                     $permissionsLists = $this->makeAuthorizedFromAdmin($userPermission);

                      
                     $this->set('modules',$this->getControllers()); 
                     $this->set('listPermissions',$permissionsLists['controller']); 
                     

                      $this->set('error',$error);
                      $this->set('data',$this->request->getData());

               }

           }else{
             $id = base64_decode($id);
             
             $groupdata = $groupTable->find()->where(['id'=>$id])->first();
             $this->set('data',$groupdata); 

             $userPermission['group_id'] = $id;
             
             $permissionsLists = $this->makeAuthorizedFromAdmin($userPermission);

               
             $this->set('modules',$this->getControllers()); 
             $this->set('listPermissions',$permissionsLists['controller']);
           
          


           }

    }

   

    
}

    
