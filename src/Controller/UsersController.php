<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\CustomerApiController;
use App\Controller\ContainersController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\TimeHelper;

//use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['driverRouteOrders', 'orders', 'dCsv', 'settings', 'pages', 'versions', 'login', 'getProduct1','getdashboardData','getProductUnitNamechild','paymentMethod','listPaymentmethods','updatePaymentmethod']);
    }
    public function downLoadInvoicePdf($id)
    {

        $date = date('Y-m-d H:i:s');

        $customerId = base64_decode($id);

        $usersTable = TableRegistry::get('Users');
        $name       = $usersTable->find()->select(['name'])->where(['id' => $customerId])->first();

        $pdfCustomerName = strtoupper($name['name']) . '_' . base64_decode($id) . '_' . $date;

        Configure::write('CakePdf', [
            'binary'      => '/opt/wkhtmltox/bin/wkhtmltopdf',
            'engine'      => 'CakePdf.WkHtmlToPdf',
            'title'       => 'Invoice_' . $name['name'],
            'encoding'    => 'UTF-8',
            'margin'      => [
                'bottom' => 15,
                'left'   => 50,
                'right'  => 30,
                'top'    => 45,
            ],
            'orientation' => 'landscape',
            'download'    => false,
        ]);

        $transactionTable = TableRegistry::get('Transactions');

        $start_date = date('Y-m-01');
        $end_date   = date('Y-m-d');
        $query      = $transactionTable->find('all')
            ->where(function ($exp, $q) use ($start_date, $end_date) {
                return $exp->between('created', $start_date, $end_date);
            })
            ->andwhere(['Transactions.transaction_amount_type' => 'Dr', 'user_id' => $customerId]);
        $result = $query->toArray();

        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename'    => 'Invoice_' . $pdfCustomerName,
            ],
        ]);
        $this->set('invoice', $result);

    }

    /*---Activity Start here------------------*/
    public function activity()
    {

        if ($this->request->is('Ajax')) {
            $this->viewBuilder()->layout('ajax');
            $id                = base64_decode($_POST['id']);
            $useractivityTable = TableRegistry::get('UserActivites');
            $useractivity      = $useractivityTable->find()->where(['user_id' => $id])->order(['id'=> 'desc'])->toArray();

            $user_customer_id = $id;
            $this->set(compact('useractivity', 'user_customer_id'));

        }

    }
    /*----Activity End Here-------------------*/

    public function getProductUnitName()
    {

        if ($_REQUEST) {

            $pro_id       = $_REQUEST['pro_id'];
            $productTable = TableRegistry::get('Products');
            $unitId       = $productTable->find()->where(['id' => trim($pro_id)])->select(['unit_id', 'price_per_unit'])->toArray();

            $uniTable = TableRegistry::get('Units');

            $unitsName = $uniTable->find()->select(['name'])->where(['id' => $unitId[0]['unit_id']])->toArray();
            $name      = str_replace('"', '', $unitsName[0]['name']);

            $childProduct     = $productTable->find('all')->where(['id' => trim($pro_id)])->contain(['ProductChildren', 'ProductChildren.Units'])->toArray();
            $productChieldres = array();

            if (count($childProduct) > 0) {

                foreach ($childProduct[0]['product_children'] as $key => $value) {

                    $temp               = array();
                    $temp['price']      = $value['price'];
                    $temp['unit']       = $value['unit']['name'];
                    $temp['quantity']   = $value['quantity'];
                    $temp['p_c_i']      = $value['id'];
                    $productChieldres[] = $temp;
                }
            }

            $final                   = array();
            $final['childproduct']   = $productChieldres;
            $final['unitname']       = strtoupper($name);
            $final['price_per_unit'] = $unitId[0]['price_per_unit'];

            echo json_encode($final);die;

        } else {
            echo false;die;
        }

    }
    public function getProductUnitNamechild()
    {
        if ($_REQUEST) {
            $pro_id       = $_REQUEST['pro_child_id'];
            $productTable = TableRegistry::get('ProductChildren');
            $unitId       = $productTable->find()->contain(['Products'])->where(['ProductChildren.id' => trim($pro_id)])->select(['ProductChildren.unit_id','Products.name','ProductChildren.price','ProductChildren.quantity'])->toArray();
            $uniTable = TableRegistry::get('Units');
            $unitsName = $uniTable->find()->select(['name', 'id'])->where(['id' => @$unitId[0]['unit_id']])->toArray();

            $unit                   = array();
            $unit['unitname']       = @$unitsName[0]['name'];
            $unit['id']             = @$unitsName[0]['id'];
            $unit['product_name']   = @$unitId[0]['product']['name'];
            $unit['package_qty']    = @$unitId[0]['quantity'];
            $unit['price_per_unit'] = @$unitId[0]['price'];
            echo json_encode($unit);die;
        }
    }

    public function getProductChildrenPriceAndCustomerBalance()
    {

        if ($_REQUEST) {
            $response         = array();
            $pro_c_id         = $_REQUEST['pro_c_id'];
            $user_id          = $_REQUEST['customer_id'];
            $userbalanceTable = TableRegistry::get('UserBalances');
            $balance          = $userbalanceTable->find()->select(['balance'])->where(['user_id' => $user_id])->first();

            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $userSubscription      = $userSubscriptionTable->find('all')->select(['subscriptions_total_amount'])->where(['user_id' => $user_id])->toArray();

            $alreadySubscribedAmount = 0;
            if (count($userSubscription) > 0) {
                foreach ($userSubscription as $key => $value) {

                    $alreadySubscribedAmount = $alreadySubscribedAmount + $value['subscriptions_total_amount'];

                }
            }
            if ($balance) {

                $productTable = TableRegistry::get('ProductChildren');
                $product      = $productTable->find()->where(['id' => $pro_c_id])->select(['price'])->toArray();

                $price      = $product[0]['price'];
                $totalPrice = $price;

                $response['statuscode']        = 301;
                $response['subscriptionprice'] = $totalPrice;
                $response['userbalance']       = $balance['balance'] - $alreadySubscribedAmount;

            } else {
                $productTable = TableRegistry::get('ProductChildren');
                $product      = $productTable->find()->where(['id' => $pro_c_id])->select(['price'])->toArray();
                $price        = $product[0]['price'];
                //$totalPrice = ( $price * $qty);
                $response['statuscode']        = 300;
                $response['subscriptionprice'] = $price;

            }
            echo json_encode($response);die;
        } else {
            echo false;die;
        }

    }

    public function getProductPriceAndCustomerBalance()
    {

        if ($_REQUEST) {
            $response         = array();
            $pro_id           = $_REQUEST['pro_id'];
            $qty              = $_REQUEST['qty'];
            $user_id          = $_REQUEST['customer_id'];
            $userbalanceTable = TableRegistry::get('UserBalances');
            $balance          = $userbalanceTable->find()->select(['balance'])->where(['user_id' => $user_id])->first();

            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $userSubscription      = $userSubscriptionTable->find('all')->select(['subscriptions_total_amount'])->where(['user_id' => $user_id, 'users_subscription_status_id'=> 1])->toArray();

            $alreadySubscribedAmount = 0;

            if (count($userSubscription) > 0) {
                foreach ($userSubscription as $key => $value) {

                    $alreadySubscribedAmount = $alreadySubscribedAmount + $value['subscriptions_total_amount'];

                }
            }

            if ($balance) {

                $productTable = TableRegistry::get('Products');
                $product      = $productTable->find()->where(['id' => $pro_id])->select(['price_per_unit', 'quantity'])->toArray();

                $leftQty = $product[0]['quantity'];
                if ($leftQty >= $qty) {
                    $price      = $product[0]['price_per_unit'];
                    $totalPrice = ($price * $qty);

                    $response['statuscode']        = 301;
                    $response['subscriptionprice'] = $totalPrice;
                    $response['userbalance']       = $balance['balance'] - $alreadySubscribedAmount;
                } else {
                    $response['statuscode'] = 308;
                }

            } else {
                $productTable = TableRegistry::get('Products');
                $product      = $productTable->find()->where(['id' => $pro_id])->select(['price_per_unit', 'quantity'])->toArray();

                $leftQty = $product[0]['quantity'];
                if ($leftQty >= $qty) {
                    $price                         = $product[0]['price_per_unit'];
                    $totalPrice                    = ($price * $qty);
                    $response['statuscode']        = 300;
                    $response['subscriptionprice'] = $totalPrice;
                } else {

                    $response['statuscode'] = 308;
                }

            }

            echo json_encode($response);die;

        } else {
            echo false;die;
        }

    }

    public function calculateContainers()
    {

        if ($_REQUEST) {

            $globalQty  = $_REQUEST['qty'];
            $product_id = $_REQUEST['pro_id'];
            $response   = array();

            $productTable = TableRegistry::get('Products');
            $iscontainer  = $productTable->find()->where(['id' => $product_id, 'iscontainer' => 1])->count();

            if ($iscontainer > 0) {
                $containerRuleTable = TableRegistry::get('ContainerRules');
                $containerRule      = $containerRuleTable->find('all')->where(['subscription_qty' => $globalQty])->toArray();
                $totalConatiners    = 0;
                if (count($containerRule)) {
                    foreach ($containerRule as $key => $value) {

                        $totalConatiners = $totalConatiners + $value['container_count'];
                    }
                }
                $response['status']     = 200;
                $response['containers'] = $totalConatiners;
                echo json_encode($response);die;
            } else {
                $response['status'] = 404;
                echo json_encode($response);die;
            }

        } else {
            echo false;die;
        }

    }

    public function checkCategoryDelivery($customer_id, $cat_id)
    {
        $response           = array();
        $cat_id             = $cat_id;
        $customer_id        = $customer_id;
        $getRegionAreaTable = TableRegistry::get('Users');
        $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $customer_id])->toArray();

        $region                          = $getRegionArea[0]['region_id'];
        $area                            = $getRegionArea[0]['area_id'];
        $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
        $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name'])->where(['CategoryDeliverySchdules.category_id' => $cat_id, 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

        if (count($category_delivery) > 0) {

            foreach ($category_delivery as $key => $value) {

                $temp           = array();
                $temp['d_s_id'] = $value['delivery_schdule_id'];
                $temp['name']   = $value['delivery_schdule']['name'];
                $response[]     = $temp;
            }

        }
        return $response;
    }

    /* get schedules */
    public function checkCategoryDeliverThisCustomer($customer_id, $cat_id)
    {

        $response = array();

        $getRegionAreaTable = TableRegistry::get('Users');
        $getRegionArea      = $getRegionAreaTable->find()->select(['region_id', 'area_id'])->where(['id' => $customer_id])->toArray();

        $region                          = $getRegionArea[0]['region_id'];
        $area                            = $getRegionArea[0]['area_id'];
        $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
        $category_delivery               = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id', 'DeliverySchdules.name', 'DeliverySchdules.start_time', 'DeliverySchdules.end_time'])->where(['CategoryDeliverySchdules.category_id' => $cat_id, 'CategoryDeliverySchdules.region_id' => $region, 'CategoryDeliverySchdules.area_id' => $area])->contain(['DeliverySchdules'])->toArray();

        if (count($category_delivery) > 0) {
            $tempOutSide = array();
            foreach ($category_delivery as $key => $value) {

                $temp               = array();
                $temp['d_s_id']     = $value['delivery_schdule_id'];
                $temp['name']       = $value['delivery_schdule']['name'];
                $temp['start_time'] = $value['delivery_schdule']['start_time'];
                $temp['end_time']   = $value['delivery_schdule']['end_time'];
                $tempOutSide[]      = $temp;

            }

            //check driver route for this schedule
            $finalSchedule      = array();
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            foreach ($tempOutSide as $key => $value) {
                $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id' => $value['d_s_id'], 'region_id' => $region, 'area_id' => $area])->hydrate(false)->toArray();
                if (!empty($routeCustomer)) {
                    $finalSchedule[] = $value;
                }
            }

            if (empty($finalSchedule)) {
                $response['messageCode'] = 1952;
                $response['successCode'] = 1;
                $response['messageText'] = 'No schedule found for user route.';
            } else {

                $response = $finalSchedule;
            }

        }

        return $response;

    }
    /* get schedules end */

    public function getProduct()
    {

        if ($_REQUEST) {
            $customer_id  = $_REQUEST['customer_id'];
            $cat_id       = $_REQUEST['cat_id'];
            $productTable = TableRegistry::get('Products');
            $product      = $productTable->find('list')->where(['category_id' => trim($cat_id), 'is_subscribable' => 1, 'Products.status' => 1])->toArray();

            /*----Get This category and user DS*/

            $result                    = $this->checkCategoryDeliverThisCustomer($customer_id, $cat_id);
            $results                   = array();
            $results['products']       = $product;
            $results['deliverytiming'] = $result;
            echo json_encode($results);die;

        } else {
            echo false;die;
        }
    }

    public function getProduct1()
    {

        if ($_REQUEST) {
            $customer_id  = $_REQUEST['customer_id'];
            $cat_id       = $_REQUEST['cat_id'];
            $productTable = TableRegistry::get('Products');
            $product      = $productTable->find('list')->where(['category_id' => trim($cat_id), 'Products.status' => 1])->toArray();

            /*----Get This category and user DS*/

            $result                    = $this->checkCategoryDeliverThisCustomer($customer_id, $cat_id);
            $results                   = array();
            $results['products']       = $product;
            $results['deliverytiming'] = $result;
            echo json_encode($results);die;

        } else {
            echo false;die;
        }
    }
    public function activate($id)
    {

        if ($id) {

            $id = base64_decode($id);

            $userTable = TableRegistry::get('Users');
            $modified  = date('Y-m-d h:i:s');
            $query     = $userTable->query();
            $result    = $query->update()
                ->set(['status' => 1, 'modified' => $modified])
                ->where(['id' => $id])
                ->execute();
            if ($result) {
                $this->Flash->success(__('Customer Has beed Activated'));
                $message = "Your account has been activated.";
                //$email = $this->sendmail($id,$message);
                return $this->redirect(['action' => 'view']);
            } else {
                $this->Flash->error(__('Something went wrong. Please, try again.'));
            }

        } else {
            return $this->redirect(['action' => 'view']);
        }

    }

    public function deActivate($id)
    {

        if ($id) {
            $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
            $id                     = base64_decode($id);
            $userTable              = TableRegistry::get('Users');
            $modified               = date('Y-m-d h:i:s');
            $query                  = $userTable->query();
            $result                 = $query->update()
                ->set(['status' => 0, 'modified' => $modified])
                ->where(['id' => $id])
                ->execute();
            if ($result) {
                $query1  = $userSubscriptionsTable->query();
                $result1 = $query1->update()
                    ->set(['users_subscription_status_id  ' => 3])
                    ->where(['user_id' => $id])
                    ->execute();
                $this->Flash->success(__('Customer Has beed De-activated'));
                return $this->redirect(['action' => 'view']);
            } else {
                $this->Flash->error(__('Something went wrong. Please, try again.'));
            }

        } else {
            return $this->redirect(['action' => 'view']);
        }

    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {

                $this->Auth->setUser($user);
                return $this->redirect($this->referer());
                //return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        } else {
            if ($this->Auth->User()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function getdashboardData(){

            $data = date('Y-m-d');
            $milkToday  = $this->orders($data);
            $milkQty = 0;
            foreach ($milkToday as $key1 => $value1) {
                foreach ($value1['orders'][0]['subscriptionInfo'] as $key2 => $value2) {
                    if($value2['unit'] == 'liter' ){
                           $milkQty = $milkQty + $value2['quantity'];
                         }
                  }
            }
            echo $milkQty; die;
    }
    public function index()
    {

        $first_day_this_month = date('Y-m-01');
        $last_day_this_month  = date('Y-m-t');

        $transtable = TableRegistry::get('Transactions');
        $transactions      = $transtable->find('all')->where(['Transactions.transaction_amount_type' => 'Dr', 'Transactions.transaction_type_id' => 6, function ($exp) use ($first_day_this_month, $last_day_this_month) {
                return $exp->between('Transactions.created', $first_day_this_month, $last_day_this_month);
            } ])->hydrate(false)->toArray();
        $transactions1      = $transtable->find('all')->where(['transaction_amount_type' => 'Cr', 'transaction_type_id'=> 3 ])->hydrate(false)->toArray();
        $transactions_on    = $transtable->find('all')->where(['transaction_amount_type' => 'Cr', 'transaction_type_id'=> 2, 'status'=> 1 ])->hydrate(false)->toArray();
        $couponTransactions = 0;
        foreach ($transactions1 as $key1 => $value1) {
            $couponTransactions = $couponTransactions + $value1['amount'];
        }
        $onlineTransactions = 0;
        foreach ($transactions_on as $ko => $vo) {
            $onlineTransactions = $onlineTransactions + $vo['amount'];
        }
        $transactionsThisMonth = 0;
        foreach ($transactions as $key => $value) {
            $transactionsThisMonth = $transactionsThisMonth + $value['amount'];
        }

        $usersTable             = TableRegistry::get('Users');
        $userBalanceTable       = TableRegistry::get('UserBalances');
        $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
        $usersContainersTable   =  TableRegistry::get('UserContainers');
        $complaintTable         =  TableRegistry::get('ComplaintFeedbackSuggestions');

        $this->paginate = [
            'contain'    => ['Regions', 'Areas', 'Groups'],
            'conditions' => ['Users.status' => 0, 'type_user' => 'customer'],
        ];
        $newusersList   = $this->paginate($this->Users)->toArray();
        $newuserscount      = $usersTable->find()->where(['Users.status' => 0, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0])->count();
        $totalUsersCount      = $usersTable->find()->where(['status' => 1, 'is_deleted'=> 0, 'type_user'=> 'customer'])->count();
        $totalUsersCountnodevice      = $usersTable->find()->where(['status' => 1, 'is_deleted'=> 0, 'type_user'=> 'customer','device_id IS NULL'])->count();
        $negativeBalance      = $userBalanceTable->find()->where(['UserBalances.balance < '=> 0])->count();
        $userSub       = $userSubscriptionsTable->find()->select(['user_id'])->toArray();
        $ids = array(2);
        $userSub1       = $userSubscriptionsTable->find()->select(['user_id'])->where(['users_subscription_status_id NOT IN'=> $ids]);

        $totalUsersCount1      = $usersTable->find()->where(['status' => 1, 'is_deleted'=> 0, 'type_user'=> 'customer','id NOT IN'=> $userSub1])->count();

        $userSub        = count(array_unique($userSub));
        $userswithnoSub = $totalUsersCount1;

        $leftContainerCount       = $usersContainersTable->find()->where(['left_container_count >' => 0])->count();
        $pendingcomplaintCount    = $complaintTable->find()->where(['status' => 0,'type'=> 'complaint'])->count();

        $this->set(compact('newusersList','newuserscount','totalUsersCount','transactionsThisMonth','couponTransactions','onlineTransactions','negativeBalance','totalUsersCountnodevice','userswithnoSub','leftContainerCount','pendingcomplaintCount','milkToday'));

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    private function usernamealreadyExistEditDriver($username, $id)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['Users.username' => $username, 'Users.id <>' => $id])->count();
        if ($users) {
            return true;
        }return false;

    }
    private function emailalreadyExistEditDriver($email, $id)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['Users.email_id' => $email, 'Users.id <>' => $id, 'is_deleted' => 0])->count();
        if ($users) {
            return true;
        }return false;

    }
    private function mobilealreadyExistEditDriver($mobile, $id)
    {

        $usersTable = TableRegistry::get('Users');
        $users      = $usersTable->find()->where(['Users.phoneNo' => $mobile, 'Users.id <>' => $id, 'is_deleted' => 0])->count();
        if ($users) {
            return true;
        }return false;

    }

    private function validateUpdateDriver($data)
    {

        $error = array();

        $error = array();

        if (!isset($data['name']) || empty($data['name'])) {
            $error['name'] = 'Name Can not be empty';
        }else if (!isset($data['houseNo']) || empty($data['houseNo'])) {
            $error['name'] = 'House No can not be empty';
        }

        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }
        return $error;

    }

    public function driverProfile()
    {

        if ($_GET) {
            $this->set('id', $_GET['id']);
            $id               = base64_decode($_GET['id']);
            $driverRouteTable = TableRegistry::get('DriverRoutes');
            $driverRoute      = $driverRouteTable->find('all')->contain(['Routes'])->where(['DriverRoutes.user_id' => $id])->hydrate(false)->toArray();
            $routeIds         = array('0');
            foreach ($driverRoute as $key => $value) {
                array_push($routeIds, $value['route_id']);
            }

            $routeIds           = array_unique($routeIds);
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            $routeCustomer      = $routeCustomerTable->find('all')->where(['route_id IN' => $routeIds])->contain(['DeliverySchdules', 'Regions', 'Areas', 'Routes'])->group(['RouteCustomers.region_id', 'RouteCustomers.area_id', 'RouteCustomers.delivery_schdule_id'])->toArray();
            $this->set('driverRoute', $routeCustomer);
            $driverVehicleTable = TableRegistry::get('DriverVehicles');
            $driverVehicle      = $driverVehicleTable->find('all')->contain(['Vehicles'])->where(['DriverVehicles.user_id' => $id])->hydrate(false)->toArray();
            $usersTable         = TableRegistry::get('Users');
            $users              = $usersTable->find()->where(['Users.id' => $id])->contain(['Regions', 'Areas'])->first()->toArray();
            $this->set('data', $users);
            $this->set('regions', $this->getAllRegionsList());
            $this->set('driverVehicle', $driverVehicle);
            $areaTable = TableRegistry::get('Areas');
            $this->set('areas', $areaTable->find('list')->toArray());

        } else {

            if ($this->request->is('post')) {
                $data  = $this->request->getData();
                $error = $this->validateUpdateDriver($data);
                if ($error['statuscode'] == 200) {

                    $imagename = '';
                    if ($_FILES && $_FILES['image']['error'] == 0) {

                        $imagename = $this->uploadImage($_FILES);

                    }
                    $usersTable = TableRegistry::get('Users');
                    $users      = $usersTable->get($data['id']);
                    if ($imagename && $imagename != '') {
                        $users->image = $imagename;
                    }
                    $users->name = $data['name'];
                    if ($data['password'] != "" && $data['password'] != "acxsgcxxzs") {
                        $users->password = md5(trim($data['password']));
                    }
                    $users->phoneNo = $data['phoneNo'];
                    $users->houseNo = $data['houseNo'];
                    if ($usersTable->save($users)) {
                        $this->Flash->success(__('The Driver has been Updated.'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'driverProfile', '?' => ['id' => base64_encode($data['id'])]]);

                    } else {
                        $this->Flash->error(__('Something went wrong Please Try Again'));
                        return $this->redirect(['controller' => 'Users', 'action' => 'driverProfile', '?' => ['id' => base64_encode($data['id'])]]);

                    }

                } else {

                    $this->set('error', $error);

                    $this->set('id', $data['id']);
                    $id               = $data['id'];
                    $driverRouteTable = TableRegistry::get('DriverRoutes');
                    $driverRoute      = $driverRouteTable->find('all')->contain(['Routes'])->where(['DriverRoutes.user_id' => $id])->hydrate(false)->toArray();
                    $routeIds         = array('0');
                    foreach ($driverRoute as $key => $value) {
                        array_push($routeIds, $value['route_id']);
                    }

                    $routeIds           = array_unique($routeIds);
                    $routeCustomerTable = TableRegistry::get('RouteCustomers');
                    $routeCustomer      = $routeCustomerTable->find('all')->where(['route_id IN' => $routeIds])->contain(['DeliverySchdules', 'Regions', 'Areas', 'Routes'])->group(['RouteCustomers.region_id', 'RouteCustomers.area_id', 'RouteCustomers.delivery_schdule_id'])->toArray();
                    $this->set('driverRoute', $routeCustomer);
                    $driverVehicleTable = TableRegistry::get('DriverVehicles');
                    $driverVehicle      = $driverVehicleTable->find('all')->contain(['Vehicles'])->where(['DriverVehicles.user_id' => $id])->hydrate(false)->toArray();
                    $usersTable         = TableRegistry::get('Users');
                    $users              = $usersTable->find()->where(['Users.id' => $id])->contain(['Regions', 'Areas'])->first()->toArray();
                    $this->set('data', $users);
                    $this->set('regions', $this->getAllRegionsList());
                    $this->set('driverVehicle', $driverVehicle);
                    $areaTable = TableRegistry::get('Areas');
                    $this->set('areas', $areaTable->find('list')->toArray());
                }

            }

        }

    }
    public function addBalance()
    {

        if ($_REQUEST) {
            $userid            = $_REQUEST['notaddidbeforethisusergetfrommakeidmohan'];
            $amount            = $_REQUEST['amountnotneedtoberechargeonetime'];
            $UserBalancesTable = TableRegistry::get('UserBalances');
            $UserBalances      = $UserBalancesTable->find()->where(['user_id' => $userid])->select('balance')->first();
            $balanceamount     = $UserBalances['balance'];

            if (isset($UserBalances) && count($UserBalances) > 0) {
                if (isset($_REQUEST['d_r'])) {
                    $balanceamount = $balanceamount - $amount;

                    $query  = $UserBalancesTable->query();
                    $result = $query->update()
                        ->set(['balance' => $balanceamount])
                        ->where(['user_id' => $userid])
                        ->execute();
                }else{
                    $balanceamount = $balanceamount + $amount;

                    $query  = $UserBalancesTable->query();
                    $result = $query->update()
                        ->set(['balance' => $balanceamount])
                        ->where(['user_id' => $userid])
                        ->execute();
                }
            } else {
                if (isset($_REQUEST['d_r'])) {
                    $balanceamount = 0 - $amount;
                }else{
                    $balanceamount         = $amount;
                }
                $UserBalances          = $UserBalancesTable->newEntity();
                $UserBalances->user_id = $userid;
                $UserBalances->balance = $balanceamount;
                $UserBalancesTable->save($UserBalances);
            }

            $transactionTable                     = TableRegistry::get('Transactions');
            $transaction                          = $transactionTable->newEntity();
            $transaction->user_id                 = $userid;
            $transaction->status                  = 1;
            $transaction->amount                  = $amount;
            $transaction->balance                 = $balanceamount;
            $transaction->created                 = date('Y-m-d');
            $date                                 = new \DateTime();
            $tz                                   = new \DateTimeZone('Asia/Kolkata');
            $date->setTimezone($tz);
            $transaction->time = date_format($date, 'H:i:s');
            if (isset($_REQUEST['r_r'])) {
                $transaction->transaction_type_id = 4;
                $transaction->refund_type_id      = $_REQUEST['r_r'];
                $transaction->transaction_amount_type = 'Cr';
            }else if (isset($_REQUEST['d_r'])) {
                $transaction->transaction_type_id = 7;
                $transaction->refund_type_id      = $_REQUEST['d_r'];
                $transaction->transaction_amount_type = 'Dr';
            } else {
                $transaction->transaction_amount_type = 'Cr';
                $transaction->transaction_type_id = 1;
            }
            if (isset($_REQUEST['refund_reason'])) {
                $transaction->refund_reason = $_REQUEST['refund_reason'];
            }
            if ($tdata = $transactionTable->save($transaction)) {
                $tid = $tdata->id;
                $userTable = TableRegistry::get('Users');
                $user      = $userTable->find()->where(['Users.id'=>$userid])->hydrate(false)->first();
                if (isset($_REQUEST['r_r'])) {
                    $message = "Rs.".$amount.' refunded to your account. Transaction ID: '. $tid .' DATE: '.date('d-m-Y').' '.$transaction->time.' Current a/c balance: Rs.'.$balanceamount;
                    $push1   = $this->addpush($userid, $message);
                    $push    = $this->pushnotifications($userid, $message);
                    if(isset($user['phoneNo']) && $user['phoneNo']){
                      $sms = $this->sendSMS($user['phoneNo'],$message);
                    }
                } else if (isset($_REQUEST['d_r'])) {
                    $message = "Rs.".$amount.' deducted from your account. Transaction ID: '. $tid .' DATE: '.date('d-m-Y').' '.$transaction->time.' Current a/c balance: Rs.'.$balanceamount;
                    $push1   = $this->addpush($userid, $message);
                    $push    = $this->pushnotifications($userid, $message);
                    if(isset($user['phoneNo']) && $user['phoneNo']){
                      $sms = $this->sendSMS($user['phoneNo'],$message);
                    }
                } else {
                    //$message = "Rs. " . $amount . " has been added to your account by Admin successfully";
                    $message = "Rs.".$amount.' added to your account. Transaction ID: '. $tid .' DATE: '.date('d-m-Y').' '.$transaction->time.' Current a/c balance: Rs.'.$balanceamount.'. Welcome to the life of health and convenience. Welcome to the CREMEWAY LIFE';

                    $push1   = $this->addpush($userid, $message);
                    $push    = $this->pushnotifications($userid, $message);
                    if(isset($user['phoneNo']) && $user['phoneNo']){
                      $sms = $this->sendSMS($user['phoneNo'],$message);
                    }
                }

                echo "200";die;
            } else {
                echo "202";die;
            }

        } else {
            echo "202";die;
        }
    }

    public function usersBalance()
    {

        if ($this->request->is('Ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set('id', $_POST['id']);
            $id = base64_decode($_POST['id']);
            $this->set('user_customer_id', $id);
            $usersBalanceTable = TableRegistry::get('UserBalances');
            $UsersTable        = TableRegistry::get('Users');
            $usersBalance      = $usersBalanceTable->find('all')->where(['user_id' => $id])->select(['balance'])->first();
            $this->set('usersBalance', $usersBalance);

            $users = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $id])->first()->toArray();
            $this->set('data', $users);

            $transactionTable = TableRegistry::get('Transactions');
            $transaction      = $transactionTable->find('all')->contain(['DeliverySchdules', 'RefundTypes', 'TransactionTypes'])->order(['Transactions.id DESC'])->limit(30)->where(['user_id' => $id])->toArray();

            $this->set('transaction', $transaction);
            $RefundTypesTable = TableRegistry::get('RefundTypes');
            $RefundTypes      = $RefundTypesTable->find('all')->toArray();
            $this->set('refunds', $RefundTypes);

        }

    }

    public function nextDelivery()
    {
        $response = array();
        if ($this->request->is('Ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set('id', $_POST['id']);
            $user_id         = base64_decode($_POST['id']);
            $data['user_id'] = $user_id;

            $userTable  = TableRegistry::get('Users');
            $userstatus = $userTable->find()->select(['status'])->where(['Users.id' => $user_id])->toArray();

            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $usersub               = $userSubscriptionTable->find('all')->where(['user_id' => $user_id, 'users_subscription_status_id' => 1])->toArray();
            if (count($usersub) > 0) {
                $checkNextDeliveryTime = $this->checkNextDelivery($data);
                $totalPrice            = $this->getPrice($checkNextDeliveryTime);
                if ($checkNextDeliveryTime) {
                    $response['messageText']                 = "success";
                    $response['messageCode']                 = 200;
                    $response['successCode']                 = 1;
                    $response['userstatus']                  = $userstatus[0]['status'];
                    $response['subscriptionOrderTotalPrice'] = $totalPrice;
                    $response['customerBalance']             = $this->updatedCustomersBalance($user_id);
                    if (isset($checkNextDeliveryTime['d_s_i']) && !empty($checkNextDeliveryTime['d_s_i'])) {
                        $response['deliver_schdule_id'] = $checkNextDeliveryTime['d_s_i'];
                    }

                    $response['willDeliver'] = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ? $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'][0]['deliverydate'] : '';

                    $products = isset($checkNextDeliveryTime['itemss'][0]['subscriptionInfo']) && $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] ? $checkNextDeliveryTime['itemss'][0]['subscriptionInfo'] : array();

                    if (empty($products)) {
                        $response['messageText'] = "No Subscription found for this customer";
                        $response['messageCode'] = 1054;
                        $response['successCode'] = 0;
                        $this->set('response', $response);
                        return;
                    }
                    foreach ($products as $key2 => $row2) {
                        $startdate[$key2] = $row2['startdate'];

                    }
                    array_multisort($startdate, SORT_ASC, $products);

                    $response['subscriptionItems'] = $products;
                    if (isset($checkNextDeliveryTime['itemss']['customOrderInfo'])) {
                        $response['orderItems'] = $checkNextDeliveryTime['itemss']['customOrderInfo'];
                    }
                } else {
                    $response['messageText'] = "No Subscription found for this customer";
                    $response['messageCode'] = 1054;
                    $response['successCode'] = 0;
                }
            } else {
                $response['messageText']     = "No Subscription found for this customer";
                $response['messageCode']     = 200;
                $response['userstatus']      = $userstatus[0]['status'];
                $response['customerBalance'] = $this->updatedCustomersBalance($user_id);
                $response['successCode']     = 1;
            }

        } else {
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0;
        }
        $this->set('response', $response);
    }

/* dashboard get balance and API */

/*-----------Get this customers Next Delivery--------------------------*/

    public function usersContainers()
    {

        if ($this->request->is('Ajax')) {
            $this->set('id', $_POST['id']);
            $id = base64_decode($_POST['id']);
            $this->set('user_customer_id', $id);
            $usersBalanceTable = TableRegistry::get('UserBalances');
            $UsersTable        = TableRegistry::get('Users');
            $usersBalance      = $usersBalanceTable->find('all')->where(['user_id' => $id])->select(['balance'])->first();
            $this->set('usersBalance', $usersBalance);

            $usersContainersTable = TableRegistry::get('UserContainers');
            $usersContainers      = $usersContainersTable->find()->select(['left_container_count'])->where(['user_id' => $id])->first();

            $SubscribedContainersTable = TableRegistry::get('SubscribedContainers');
            $allotedContainers         = $SubscribedContainersTable->find()->select(['container_count'])->where(['user_id' => $id])->first();

            $UsersTable = TableRegistry::get('Users');
            $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $id])->first()->toArray();
            $this->set('data', $users);

            $this->set('container', $usersContainers);
            $this->set('allotedContainers', $allotedContainers);
            $RefundTypesTable = TableRegistry::get('RefundTypes');
            $RefundTypes      = $RefundTypesTable->find('all')->toArray();
            $this->set('refunds', $RefundTypes);

        }

    }

    public function driver()
    {
        $page = 0;
        if ($_GET && !isset($_GET['query'])) {

            $condition = ['Users.is_deleted' => 0, 'Users.type_user' => 'driver'];
            $filterby  = '';

            if (isset($_GET['filterby']) && !empty($_GET['filterby'])) {
                $filterby  = $_GET['filterby'];
                $condition = ['type_user' => $_GET['filterby'], 'Users.is_deleted' => 0];
            }

            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.name', 'Users.phoneNo', 'Users.email_id',
                ],
                'limit'         => 10,
                'page'          => $page,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'conditions'    => $condition,
                'contain'       => ['Regions', 'Areas'],
            ];
            $user = $this->paginate($this->Users)->toArray();
            $this->set('user', $user);
            $this->set('filterby', $filterby);

        } else if (isset($_GET['query'])) {

            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }

            $condition = [

                'OR'              => [

                    'Users.phoneNo LIKE' => '%' . $_GET['query'] . '%',
                    'Users.name LIKE'    => '%' . $_GET['query'] . '%',
                ],
                'Users.type_user' => 'driver', 'Users.is_deleted' => 0,
            ];
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.name', 'Users.phoneNo', 'Users.email_id',
                ],
                'limit'         => 10,
                'page'          => $page,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'conditions'    => $condition,
                'contain'       => [

                    'Regions',
                    'Areas',

                ],
                'contain'       => ['Groups', 'Regions', 'Areas'],

            ];
            $user = $this->paginate($this->Users)->toArray();
            $this->set('user', $user);
            if (isset($_GET['query']) && !empty($_GET['query'])) {
                $this->set('querystring', $_GET['query']);
            }

        } else {
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.name', 'Users.phoneNo', 'Users.email_id',
                ],
                'limit'         => 10,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'contain'       => ['Regions', 'Areas'],
                'conditions'    => ['Users.type_user' => 'driver', 'Users.is_deleted' => 0],
            ];
            $user = $this->paginate($this->Users)->toArray();

            $this->set('user', $user);
        }

    }

    public function admin()
    {
        $groupTable = TableRegistry::get('Groups');
        if (isset($_GET['role_id'])) {

            $condition = [];

            $page = 0;
            if (isset($_GET['role_id']) && !empty($_GET['role_id'])) {

                $condition = ['group_id' => $_GET['role_id'], 'Users.type_user' => 'admin'];
            }

            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.name', 'Users.phoneNo', 'Groups.name', 'Users.email_id',
                ],
                'contain'       => ['Groups'],
                'limit'         => 10,
                'page'          => $page,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'conditions'    => $condition,

            ];
            $user = $this->paginate($this->Users)->toArray();
            $this->set('user', $user);
            $this->set('role_id', $_GET['role_id']);
            $groups = $groupTable->find('list')->toArray();
            $this->set('groups', $groups);

        } else {
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.name', 'Users.phoneNo', 'Groups.name', 'Users.email_id',
                ],
                'contain'       => ['Groups'],
                'limit'         => 10,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'conditions'    => ['Users.type_user' => 'admin'],
            ];
            $user = $this->paginate($this->Users)->toArray();

            $this->set('user', $user);

            $groups = $groupTable->find('list')->toArray();
            $this->set('groups', $groups);

        }

    }

    private function getAllRegionsList()
    {

        $areaTable = TableRegistry::get('Regions');
        $regions   = $areaTable->find('list')->hydrate(false)->toArray();
        return $regions;
    }

    public function view()
    {

        if ($_GET) {

            $condition = ['Users.is_deleted' => 0, 'Users.type_user' => 'customer'];
            $filterby  = '';
            $page      = 0;

            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }

            $this->paginate = [
                'sortWhitelist' => [
                    'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                ],
                'limit'         => 10,
                'page'          => $page,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'conditions'    => $condition,
                'contain'       => ['Groups', 'Regions', 'Areas', 'UserBalances',
                'Transactions'=> [
                  'sort' => ['Transactions.created' => 'DESC'],
                  'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                ]],
            ];

            if (isset($_GET['latest_45']) && !empty($_GET['latest_45']) && $_GET['latest_45']== 'yes') {
                $latest_45 = $_GET['latest_45'];

                $end_date= date('Y-m-d H:i:s');
                $start_date = date('Y-m-d H:i:s', strtotime('-45 days', strtotime($end_date)));
                $condition = [
                              'AND' => [
                                 'Users.created >='=>$start_date, 'Users.created <=' => $end_date, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0
                               ]
                            ];

                $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => ['Regions', 'Areas', 'UserBalances', 'UserContainers',
                        'Transactions'=> [
                          'sort' => ['Transactions.created' => 'DESC'],
                          'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                        ]],
                    ];

                    $user = $this->paginate($this->Users)->toArray();
                    $this->set('user', $user);
                    $this->set('latest_45', $latest_45);
                    $this->set('regions', $this->getAllRegionsList());
            }
            else if ((isset($_GET['filterby']) && !empty($_GET['filterby']))
                ||
                (isset($_GET['region_id']) || !empty($_GET['area_id']))
                ||
                (isset($_GET['query']))) {

                if (isset($_GET['filterby']) && !empty($_GET['filterby'])  && empty($_GET['region_id'])) {
                    $filterby = $_GET['filterby'];

                    if ($filterby == 'active') {
                        $condition = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0];
                    }
                    if ($filterby == 'deactive') {
                        $condition = ['Users.status' => 0, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0];
                    }
                    if ($filterby == 'noapp') {
                        $condition = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0, 'Users.device_id IS NULL'];
                    }
                    if ($filterby == 'negativebalance') {
                        $condition = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0,'UserBalances.balance <'=> 0];
                    }
                    if ($filterby == 'leftcontainer') {
                        $condition = ['Users.type_user' => 'customer', 'UserContainers.left_container_count >'=> 0];
                    }
                    $usersubTable  = TableRegistry::get('UserSubscriptions');
                    $ids = array(2);
                    $usersub       = $usersubTable->find()->select(['user_id'])->where(['UserSubscriptions.users_subscription_status_id NOT IN'=> $ids]);
                    if ($filterby == 'noSubscription') {
                        $condition = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0,'Users.id NOT IN'=> $usersub];
                    }

                    $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => ['Regions', 'Areas', 'UserBalances', 'UserContainers',
                        'Transactions'=> [
                          'sort' => ['Transactions.created' => 'DESC'],
                          'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                        ]],
                    ];

                }

                if (isset($_GET['region_id']) && !isset($_GET['area_id']) && !empty($_GET['region_id'])) {
                    $region_id      = $_GET['region_id'];
                    $condition      = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0, 'Users.region_id' => $region_id];
                    $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => [
                            'Regions',
                            'Areas',

                        ],

                    ];

                }

                if (isset($_GET['area_id']) && !isset($_GET['region_id']) && !empty($_GET['area_id'])) {

                    $region_id      = $_GET['area_id'];
                    $condition      = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0, 'Users.area_id' => $_GET['area_id']];
                    $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => [

                            'Regions',
                            'Areas',
                            'UserBalances',

                        ],

                    ];

                }

                if (isset($_GET['region_id']) && isset($_GET['area_id']) && !empty($_GET['area_id']) && !empty($_GET['region_id'])) {

                    $area_id   = $_GET['area_id'];
                    $region_id = $_GET['region_id'];
                    $condition      = ['Users.status' => 1, 'Users.type_user' => 'customer', 'Users.is_deleted' => 0, 'Users.area_id' => $area_id, 'Users.region_id' => $region_id];
                    $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => [
                            'UserBalances',
                            'Regions' => function (\Cake\ORM\Query $query) use ($region_id) {
                                return $query->where(['Users.region_id' => $region_id]);
                            },
                            'Areas'   => function (\Cake\ORM\Query $query) use ($area_id) {
                                return $query->where(['Users.area_id' => $area_id]);
                            },
                            'Transactions'=> [
                              'sort' => ['Transactions.created' => 'DESC'],
                              'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                            ]

                        ],

                    ];

                }

                if (isset($_GET['query'])) {

                    $condition[] = [

                        'OR' => [

                            'Users.phoneNo LIKE' => '%' . $_GET['query'] . '%',
                            'Users.name LIKE'    => '%' . $_GET['query'] . '%', 'Users.houseNo LIKE' => '%' . $_GET['query'] . '%','Users.id' => trim($_GET['query'])
                        ],
                    ];
                    $this->paginate = [
                        'sortWhitelist' => [
                            'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                        ],

                        'limit'         => 10,
                        'page'          => $page,
                        'order'         => [
                            'Users.id' => 'desc',
                        ],
                        'conditions'    => $condition,
                        'contain'       => [

                            'Regions',
                            'Areas',
                            'UserBalances',
                            'Transactions'=> [
                              'sort' => ['Transactions.created' => 'DESC'],
                              'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                            ]

                        ],

                    ];

                }

            } else {
                $this->paginate = [
                    'sortWhitelist' => [
                        'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                    ],

                    'limit'         => 10,
                    'order'         => [
                        'Users.id' => 'desc',
                    ],
                    'contain'       => ['Regions', 'Areas', 'UserBalances',
                    'Transactions'=> [
                      'sort' => ['Transactions.created' => 'DESC'],
                      'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                    ]],
                    'conditions'    => ['Users.type_user' => 'customer', 'Users.is_deleted' => 0],
                ];
            }
            $user = $this->paginate($this->Users)->toArray();
            $this->set('user', $user);
            $this->set('filterby', $filterby);
            $this->set('regions', $this->getAllRegionsList());
            if (isset($_GET['region_id']) && !isset($_GET['area_id'])) {
                $this->set('region_id', $_GET['region_id']);
                $arealist = $this->getRegionsArea($_GET['region_id']);
                $this->set('areas', $arealist);
            }
            if (isset($_GET['area_id']) && !isset($_GET['region_id'])) {
                $areaTable = TableRegistry::get('Areas');
                $region_id = $areaTable->find()->where(['id' => $_GET['area_id']])->select(['region_id'])->first();
                $this->set('region_id', $region_id['region_id']);
                $arealist = $this->getRegionsArea($region_id['region_id']);
                $this->set('areas', $arealist);
            }
            if (isset($_GET['query']) && !empty($_GET['query'])) {
                $this->set('querystring', $_GET['query']);
            }
            if (isset($_GET['area_id']) && isset($_GET['region_id'])) {
                $this->set('area_id', $_GET['area_id']);
                $this->set('region_id', $_GET['region_id']);
                $arealist = $this->getRegionsArea($_GET['region_id']);
                $this->set('areas', $arealist);
            }
        } else {
            $this->paginate = [
                'sortWhitelist' => [
                    'Users.email_id', 'Users.name', 'Users.phoneNo', 'Users.email_id', 'Regions.name', 'Areas.name', 'Users.status', 'Users.id',
                ],

                'limit'         => 10,
                'order'         => [
                    'Users.id' => 'desc',
                ],
                'contain'       => ['Regions', 'Areas', 'UserBalances',
                                    'Transactions'=> [
                                      'sort' => ['Transactions.created' => 'DESC'],
                                      'conditions' => ['Transactions.transaction_amount_type' => 'Cr']
                                    ]
                ],
                'conditions'    => ['Users.type_user' => 'customer', 'Users.is_deleted' => 0],
            ];
            $user = $this->paginate($this->Users)->toArray();
            // echo "<pre>";
            // // debug($user);
            // print_r($user);
            // echo "</pre>";die;
            $this->set('regions', $this->getAllRegionsList());
            $this->set('user', $user);
        }

    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function getRegionsArea($regionId)
    {

        $areaTable = TableRegistry::get('Areas');
        $areas     = $areaTable->find('list')->where(['region_id' => $regionId])->hydrate(false)->toArray();
        return $areas;
    }

    public function getArea()
    {
        if ($_GET['id']) {
            $region_id = $_GET['id'];
            $areas     = $this->Users->Areas->find('list')->where(['region_id' => $region_id])->hydrate(false)->toArray();
            echo json_encode($areas);die;
        } else {
            echo "Error";die;
        }
    }
    private function usernamealreadyExist($username)
    {

        $user    = TableRegistry::get('Users');
        $isExist = $user->find()->where(['username' => $username])->count();
        if ($isExist) {
            return true;
        }return false;
    }

    private function editusernamealreadyExist($username, $id)
    {

        $user    = TableRegistry::get('Users');
        $isExist = $user->find()->where(['username' => $username, 'id <>' => $id])->count();
        if ($isExist) {
            return true;
        }return false;
    }
    private function mobilealreadyExist($mobile_no)
    {
        $user    = TableRegistry::get('Users');
        $isExist = $user->find()->where(['phoneNo' => $mobile_no, 'is_deleted' => 0])->count();
        if ($isExist) {
            return true;
        }return false;
    }

    private function validateAddadmin($data)
    {

        $error = array();
        if (!isset($data['name']) || empty($data['name'])) {
            $error['name'] = 'Name Can not be empty';
        } else if (preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $data['name'])) {
            $error['name'] = 'Please check Name name';
        } else if (!isset($data['username']) || empty($data['username'])) {
            $error['name'] = 'Username can not be empty';
        } else if ($this->usernamealreadyExist($data['username'])) {
            $error['name'] = 'Username already Exist';
        } else if (!isset($data['group_id']) || empty($data['group_id'])) {
            $error['name'] = 'User Group can not be empty';
        } else if (!isset($data['role_id']) || empty($data['role_id'])) {
            $error['name'] = 'User Role can not be empty';
        }

        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }
        return $error;

    }

    private function validateEditadmin($data)
    {

        $error = array();
        if (!isset($data['name']) || empty($data['name'])) {
            $error['name'] = 'Name Can not be empty';
        } else if (preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $data['name'])) {
            $error['name'] = 'Please check Name name';
        } else if (!isset($data['username']) || empty($data['username'])) {
            $error['name'] = 'Username can not be empty';
        } else if ($this->editusernamealreadyExist($data['username'], $data['id'])) {
            $error['name'] = 'Username already Exist';
        } else if (!isset($data['group_id']) || empty($data['group_id'])) {
            $error['name'] = 'User Group can not be empty';
        } else if (!isset($data['role_id']) || empty($data['role_id'])) {
            $error['name'] = 'User Role can not be empty';
        }

        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }
        return $error;

    }

    public function adminadd()
    {
        if ($this->request->is('post')) {

            $imagename = '';
            if ($_FILES && $_FILES['image']['error'] == 0) {

                $imagename = $this->uploadImage($_FILES);

                if ($imagename != 1) {
                    $this->request->data['image'] = $imagename;
                } else {
                    $this->request->data['image'] = null;
                }

            }

            if ($imagename != 1) {

                $error = $this->validateAddadmin($this->request->getData());
                if ($error['statuscode'] == 200) {
                    $user            = $this->Users->newEntity();
                    $user->type_user = 'admin';
                    $user->name      = trim($this->request->getData('name'));
                    $user->username  = trim($this->request->getData('username'));
                    $user->password  = md5(trim($this->request->getData('password')));
                    $user->email_id  = trim($this->request->getData('email_id'));
                    $user->group_id  = trim($this->request->getData('group_id'));
                    $user->role_id   = trim($this->request->getData('role_id'));
                    $user->houseNo   = trim($this->request->getData('houseNo'));
                    $user->status    = 1;

                    if ($this->Users->save($user)) {

                        $this->Flash->success(__('The Admin has been saved.'));
                        return $this->redirect('/AclManager');
                    }
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                } else {

                    $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                    $groups  = $this->Users->Groups->find('list')->hydrate(false)->toArray();
                    $roles   = $this->Users->Roles->find('list')->hydrate(false)->toArray();
                    $this->set(compact('regions', 'groups', 'roles'));
                    $this->set('error', $error);
                    $this->set('data', $this->request->getData());
                }
            } else {
                $error['name'] = 'Invalid Image';
                $regions       = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                $groups        = $this->Users->Groups->find('list')->hydrate(false)->toArray();
                $roles         = $this->Users->Roles->find('list')->hydrate(false)->toArray();
                $this->set(compact('regions', 'groups', 'roles'));
                $this->set('error', $error);
                $this->set('data', $this->request->getData());
            }

        } else {
            $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
            $groups  = $this->Users->Groups->find('list')->hydrate(false)->toArray();
            $roles   = $this->Users->Roles->find('list')->hydrate(false)->toArray();
            $this->set(compact('regions', 'groups', 'roles'));
        }

    }

    public function editadmin($id = null)
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $imagename = '';
            if ($_FILES && $_FILES['image']['error'] == 0) {

                $imagename = $this->uploadImage($_FILES);
                if ($imagename != 1) {
                    $data['image'] = $imagename;
                } else {
                    $data['image'] = null;
                }
            }
            if ($imagename != 1) {

                $error      = $this->validateEditadmin($data);
                $usersTable = TableRegistry::get('Users');
                $user       = $usersTable->get($data['id']);
                if ($error['statuscode'] == 200) {
                    $user->name     = trim($data['name']);
                    $user->username = trim($data['username']);
                    if ($data['password'] != '********') {
                        $user->password = md5(trim($data['password']));
                    }
                    if ($imagename) {
                        $user->image = $imagename;
                    }
                    $user->email_id = trim($data['email_id']);
                    $user->group_id = trim($data['group_id']);
                    $user->role_id  = trim($data['role_id']);
                    $user->houseNo  = trim($data['houseNo']);

                    if ($this->Users->save($user)) {

                        $this->Flash->success(__('The Admin has been saved.'));

                        return $this->redirect('/AclManager');
                    }
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                } else {
                    $userTable = TableRegistry::get('Users');
                    $data      = $this->Users->find()->where(['id' => base64_decode($id)])->hydrate(false)->first();
                    $regions   = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                    $groups    = $this->Users->Groups->find('list')->hydrate(false)->toArray();
                    $roles     = $this->Users->Roles->find('list')->hydrate(false)->toArray();
                    $this->set(compact('regions', 'groups', 'roles', 'data'));
                    $this->set('error', $error);
                    $this->set('data', $data);
                }
            } else {
                $error['name'] = 'Invalid Image';
                $userTable     = TableRegistry::get('Users');
                $data          = $this->Users->find()->where(['id' => base64_decode($id)])->hydrate(false)->first();
                $regions       = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                $groups        = $this->Users->Groups->find('list')->hydrate(false)->toArray();
                $roles         = $this->Users->Roles->find('list')->hydrate(false)->toArray();
                $this->set(compact('regions', 'groups', 'roles', 'data'));
                $this->set('error', $error);
                $this->set('data', $data);
            }

        } else {
            $userTable = TableRegistry::get('Users');
            $data      = $this->Users->find()->where(['id' => base64_decode($id)])->hydrate(false)->first();
            $regions   = $this->Users->Regions->find('list')->hydrate(false)->toArray();
            $groups    = $this->Users->Groups->find('list')->hydrate(false)->toArray();
            $roles     = $this->Users->Roles->find('list')->hydrate(false)->toArray();
            $this->set(compact('regions', 'groups', 'roles', 'data'));

        }

    }

    private function usernamealreadyExistEdit($username, $id)
    {
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id <>' => $id, 'username' => $username])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateAddUser($data)
    {
        $error = array();
        $type  = array('customer', 'driver');
        if (!isset($data['name']) || empty($data['name'])) {
            $error['name'] = 'Name Can not be empty';
        } else if (preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $data['name'])) {
            $error['name'] = 'Please check Name name';
        }else if (!isset($data['phoneNo']) || empty($data['phoneNo'])) {
            $error['name'] = 'PhoneNo can not be empty';
        }
        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }
        return $error;

    }
    public function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {return "";}
        $l   = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    private function uploadImage($image)
    {
        $error = 0;
        if (isset($_FILES["image"])) {
            $tmpFile   = $_FILES["image"]["tmp_name"];
            $extension = $this->getExtension($_FILES['image']['name']);
            $tme       = time();
            $fileName  = IMG_PATH. $tme . '.' . $extension;

            $extensionArray = array('jpg', 'jpeg', 'png');

            list($width, $height) = getimagesize($tmpFile);

            if ($width == null && $height == null) {
                $error = 1;
            }
            if (!in_array($extension, $extensionArray)) {
                $error = 1;
            } else {
                if (move_uploaded_file($tmpFile, $fileName)) {
                    $error = IMG_PATHS. $tme . '.' . $extension;
                }
            }
        }
        return $error;
    }

    /*----------------Add by vipin--------------------------*/
    public function driverAdd()
    {
        if ($this->request->is('post')) {
            $imagename = '';
            if ($_FILES && $_FILES['image']['error'] == 0) {
                $imagename = $this->uploadImage($_FILES);
                if ($imagename != 1) {
                    $this->request->data['image'] = $imagename;
                } else {
                    $this->request->data['image'] = null;
                }

            }
            if ($imagename != 1) {

                $error = $this->validateAddUser($this->request->getData());
                if ($error['statuscode'] == 200) {
                    $user       = $this->Users->newEntity();
                    $user->name = trim($this->request->getData('name'));

                    $user->phoneNo = trim($this->request->getData('phoneNo'));
                    $user->houseNo   = trim($this->request->getData('houseNo'));
                    $user->username  = trim($this->request->getData('username'));
                    $user->password  = md5(trim($this->request->getData('password')));
                    $user->type_user = 'driver';
                    $user->image     = $imagename;
                    $user->status    = 1;
                    $resSave = $this->Users->save($user);
                    if ($resSave) {
                        $this->Flash->success(__('The Driver has been saved.'));
                        return $this->redirect(['action' => 'driver']);
                    }
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                } else {

                    $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                    $this->set(compact('regions'));
                    $this->set('error', $error);
                    $this->set('data', $this->request->getData());
                }

            } else {
                $error['name'] = 'Invalid Image';
                $regions       = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                $this->set(compact('regions'));
                $this->set('error', $error);
                $this->set('data', $this->request->getData());
            }

        } else {
            $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
            $this->set(compact('regions'));

        }

    }
    /*-----------------Date----==============================*/

    public function customerProfile()
    {

        if ($_GET) {
            $this->set('id', $_GET['id']);
            $id = base64_decode($_GET['id']);
            $this->set('user_customer_id', $id);
            $UsersTable = TableRegistry::get('Users');
            $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $id])->first()->toArray();
            $this->set('data', $users);
            $usersBalanceTable = TableRegistry::get('UserBalances');
            $usersBalance      = $usersBalanceTable->find('all')->where(['user_id' => $id])->select(['balance'])->first();
            $this->set('usersBalance', $usersBalance);

            $regionTable = TableRegistry::get('Regions');
            $areaTable   = TableRegistry::get('Areas');

            $this->set('regions', $regionTable->find('list')->toArray());
            $this->set('areas', $areaTable->find('list')->toArray());

            $RefundTypesTable = TableRegistry::get('RefundTypes');
            $refunds      = $RefundTypesTable->find('all')->where(['type'=> 0])->toArray();
            $refunds1      = $RefundTypesTable->find('all')->where(['type'=> 1])->toArray();
            $this->set(compact('refunds', 'refunds1'));

        }
    }

    public function updateCustomerProfile()
    {
        if ($this->request->is('post')) {
        $requestedata = $this->request->getData();
        $UsersTable   = TableRegistry::get('Users');
        $users        = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $requestedata['id']])->first()->toArray();
        $pre_area     = $users['area_id'];
        $pre_region   = $users['region_id'];

        $imagename = '';
        if ($_FILES && $_FILES['image']['error'] == 0) {

            $imagename = $this->uploadImage($_FILES);

            if ($imagename != 1) {
                $this->request->data['image'] = $imagename;
            } else {
                $this->request->data['image'] = null;
            }

        }
        $error = $this->validateEditCustomer($this->request->getData());
        if ($error['statuscode'] == 200) {

            $user = $this->Users->get($requestedata['id']);
            if ($_FILES && $_FILES['image']['error'] > 0) {
                unset($this->request->data['image']);
            }
            if (isset($requestedata['user_cat_type']) && $requestedata['user_cat_type'] == 'vip') {

                $this->request->data['customer_category'] = $requestedata['user_cat_type'];
            } else {
                $this->request->data['customer_category'] = 'normal';
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been Updated.'));
                if ($pre_region != $requestedata['region_id'] || $pre_area != $requestedata['area_id']) {
                    $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                    $query                  = $userSubscriptionsTable->query();
                    $result                 = $query->update()
                        ->set(['users_subscription_status_id' => 4])
                        ->where(['user_id' => $requestedata['id']])
                        ->execute();
                    $RouteCustomers = TableRegistry::get('RouteCustomers');
                    $RouteCustomers->deleteAll(['user_id IN' => array($requestedata['id'])]);
                    $userContainers = TableRegistry::get('UserContainers');
                    $userscont          = $userContainers->find()->where(['user_id' => $requestedata['id']])->toArray();
                    if($userscont){
                        $query1         = $userContainers->query();
                        $result1        = $query1->update()
                                ->set(['container_given' => 0])
                                ->where(['user_id' => $requestedata['id']])
                                ->execute();
                    }

                }

                $id = base64_encode($requestedata['id']);
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => $id]]);
            }
            $id = base64_encode($requestedata['id']);
            return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => $id]]);
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        } else {
            $data = $requestedata = $this->request->getData();
            $this->set('id', $data['id']);
            $this->set('user_customer_id', $data['id']);
            $UsersTable = TableRegistry::get('Users');
            $users      = $UsersTable->find()->contain(['Regions', 'Areas', 'UserBalances'])->where(['Users.id' => $data['id']])->first()->toArray();

            $regionTable = TableRegistry::get('Regions');
            $areaTable   = TableRegistry::get('Areas');
            $this->set('regions', $regionTable->find('list')->toArray());
            $this->set('areas', $areaTable->find('list')->toArray());
            $this->set('data', $users);
            $this->set('error', $error);

        }

        } else {
        return $this->redirect($this->referer);
        }
    }

    private function emailalreadyExist($email)
    {
        $user    = TableRegistry::get('Users');
        $isExist = $user->find()->where(['email_id' => $email, 'is_deleted' => 0])->count();
        if ($isExist) {
            return true;
        }return false;
    }

    public function isusernamealreadyExist()
    {
        if ($this->request->is('post')) {
            $data       = $this->request->getData();
            $username   = $data['username'];
            $usersTable = TableRegistry::get('Users');
            $users      = $usersTable->find()->where(['Users.username' => $username])->count();
            if ($users) {
                echo "found";die;
            } else {
                echo "not found";die;
            }
        }

    }

    public function isemailalreadyExist()
    {
        if ($this->request->is('post')) {
            $data  = $this->request->getData();
            $email = $data['email'];
            if ($email != "") {
                $user    = TableRegistry::get('Users');
                $isExist = $user->find()->where(['email_id' => $email])->count();
                if ($isExist || (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
                    echo "found";die;
                } else {
                    echo "not found";die;
                }

            } else {
                echo "not found";die;
            }
        }
    }

    public function ismobilealreadyExistEdit()
    {
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $mobile    = $data['phone'];
            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobile, 'is_deleted' => 0, 'type_user' => 'customer'])->count();
            if ($user) {
                echo "found";die;
            } else {
                echo "not found";die;
            }
        }

    }
    public function ismobilealreadyExistEditt()
    {
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $mobile    = $data['phone'];
            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobile, 'is_deleted' => 0, 'type_user' => 'driver'])->count();
            if ($user) {
                echo "found";die;
            } else {
                echo "not found";die;
            }
        }

    }

    public function ismobilealreadyExistEdit1()
    {
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $mobile    = $data['phone'];
            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobile, 'is_deleted' => 0, 'id <>' => $data['user_id'], 'type_user' => 'customer'])->count();
            if ($user) {
                echo "found";die;
            } else {
                echo "not found";die;
            }
        }

    }
    public function ismobilealreadyExistEdit11()
    {
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $mobile    = $data['phone'];
            $userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['phoneNo' => $mobile, 'is_deleted' => 0, 'id <>' => $data['user_id'], 'type_user' => 'driver'])->count();
            if ($user) {
                echo "found";die;
            } else {
                echo "not found";die;
            }
        }

    }

    private function mobilealreadyExistEdit($mobile, $id)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id' => $id])->select(['type_user'])->hydrate(false)->first();
        $user      = $userTable->find()->where(['id <>' => $id, 'phoneNo' => $mobile, 'is_deleted' => 0, 'type_user' => $user['type_user']])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function emailalreadyExistEdit($email, $id)
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['id <>' => $id, 'email_id' => $email, 'is_deleted' => 0])->count();
        if ($user) {
            return true;
        }return false;

    }

    private function validateEditCustomer($data)
    {

        $error = array();

        if (!isset($data['name']) || empty($data['name'])) {
            $error['name'] = 'Name Can not be empty';
        } else if (preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/', $data['name'])) {
            $error['name'] = 'Please check Name';
        } else if (!isset($data['phoneNo']) || empty($data['phoneNo'])) {
            $error['name'] = 'PhoneNo can not be empty';
        } else if ($this->mobilealreadyExistEdit($data['phoneNo'], $data['id'])){
            $error['name'] = 'Phone No Already Exist';
        } else if (!isset($data['houseNo']) || empty($data['houseNo'])) {
            $error['name'] = 'House No can not be empty';
        } else if (!isset($data['region_id']) || empty($data['region_id'])) {
            $error['name'] = 'House No can not be empty';
        } else if (!isset($data['area_id']) || empty($data['area_id'])) {
            $error['name'] = 'House No can not be empty';
        }

        if (count($error) > 0) {
            $error['statuscode'] = 201;
        } else {
            $error['statuscode'] = 200;
        }
        return $error;
    }
    public function add()
    {
        if ($this->request->is('post')) {
            $imagename = '';
            if ($_FILES && $_FILES['image']['error'] == 0) {

                $imagename = $this->uploadImage($_FILES);

                if ($imagename != 1) {
                    $this->request->data['image'] = $imagename;
                } else {
                    $this->request->data['image'] = null;
                }

            }
            if ($imagename != 1) {
                $error = $this->validateAddUser($this->request->getData());

                if ($error['statuscode'] == 200) {
                    $error1 = $this->isdeletedCustomer(trim($this->request->getData('phoneNo')));
                    if ($error1['statuscode'] == 200) {
                        $userTable = TableRegistry::get('Users');
                        $query  = $userTable->query();
                        $result = $query->update()
                            ->set(['name' => trim($this->request->getData('name')), 'phoneNo' => trim($this->request->getData('phoneNo')), 'email_id' => trim($this->request->getData('email_id')), 'password' => md5(trim($this->request->getData('password'))), 'status' => 1, 'is_deleted' => 0, 'image' => $imagename, 'houseNo' => trim($this->request->getData('houseNo')), 'region_id' => $this->request->getData('region_id'), 'area_id' => $this->request->getData('area_id')])
                            ->where(['id' => $error1['user_id']])
                            ->execute();

                        $this->Flash->success(__('The Customer has been saved.Please add Subscription for this Customer'));
                        $id = base64_encode($error1['user_id']);

                        return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => $id]]);
                    } else {
                        $user                           = $this->Users->newEntity();
                        $this->request->data['name']    = trim($this->request->getData('name'));
                        $this->request->data['phoneNo'] = trim($this->request->getData('phoneNo'));
                        $this->request->data['houseNo'] = trim($this->request->getData('houseNo'));
                        $this->request->data['email_id']  = trim($this->request->getData('email_id'));
                        $this->request->data['password']  = md5(trim($this->request->getData('password')));
                        $this->request->data['type_user'] = 'customer';
                        $this->request->data['token']     = 'qwerty';
                        $normal_vip = $this->request->getData('user_cat_type');
                        if (isset($normal_vip) && !empty($normal_vip)) {

                            $this->request->data['customer_category'] = trim($this->request->getData('user_cat_type'));
                        } else {
                            $this->request->data['customer_category'] = 'normal';
                        }
                        $this->request->data['status'] = 1;
                        $user                          = $this->Users->patchEntity($user, $this->request->getData());
                        $resSave                       = $this->Users->save($user);
                        if ($resSave) {

                            $userbalanceTable     = TableRegistry::get('UserBalances');
                            $userbalance          = $userbalanceTable->newEntity();
                            $userbalance->balance = $this->request->getData('amount');
                            $userbalance->user_id = $resSave->id;
                            $userbalanceTable->save($userbalance);

                            /*----Add in Transactions Table--------------*/
                            $transactionTable                     = TableRegistry::get('Transactions');
                            $transaction                          = $transactionTable->newEntity();
                            $transaction->user_id                 = $resSave->id;
                            $transaction->amount                  = $this->request->getData('amount');
                            $transaction->transaction_amount_type = 'Cr';
                            $transaction->created                 = date('Y-m-d');
                            $date                                 = new \DateTime();
                            $tz                                   = new \DateTimeZone('Asia/Kolkata');
                            $date->setTimezone($tz);
                            $transaction->time                = date_format($date, 'H:i:s');
                            $transaction->status              = 1;
                            $transaction->transaction_type_id = 1;
                            $transactionTable->save($transaction);

                            /*----Add in Transactions Table--------------*/

                            $this->Flash->success(__('The Customer has been saved.Please add Subscription for this Customer'));
                            $id = base64_encode($resSave->id);

                            return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => $id]]);
                        }
                    }
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                } else {

                    $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                    $this->set(compact('regions'));
                    $this->set('error', $error);
                    $this->set('data', $this->request->getData());
                }

            } else {
                $error['name'] = 'Invalid Image';
                $regions       = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                $this->set(compact('regions'));
                $this->set('error', $error);
                $this->set('data', $this->request->getData());
            }

        } else {
            $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
            $this->set(compact('regions'));

        }

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $devices   = $this->Users->Devices->find('list', ['limit' => 200]);
        $regions   = $this->Users->Regions->find('list', ['limit' => 200]);
        $areas     = $this->Users->Areas->find('list', ['limit' => 200]);
        $groups    = $this->Users->Groups->find('list', ['limit' => 200]);
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'devices', 'regions', 'areas', 'groups', 'userTypes'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $isDriver = null)
    {
        $user = $this->Users->get(base64_decode($id));
        if (isset($isDriver) && !empty($isDriver)) {
            $driverid = base64_decode($id);
            $driverRouteTable = TableRegistry::get('DriverRoutes');
            $driverRoute      = $driverRouteTable->find('all')->where(['user_id' => $driverid])->toArray();
            if (isset($driverRoute) && count($driverRoute) > 0) {
                foreach ($driverRoute as $key => $value) {
                    $query  = $driverRouteTable->query();
                    $result = $query->update()
                        ->set(['user_id' => null])
                        ->where(['user_id' => $value['user_id']])
                        ->execute();
                }
            }
            return $this->redirect(['action' => 'driver']);
        } else {
            $user = $this->Users->get(base64_decode($id));
            if ($this->Users->delete($user)) {
                $this->Flash->success(__('The user has been deleted.'));
            } else {
                $this->Flash->error(__('The user could not be deleted. Please, try again.'));
            }
            return $this->redirect('/AclManager');
        }

    }

    public function customnotifications()
    {

        if ($this->request->is('post')) {
            $data              = $this->request->getData();
            $message           = $data['message'];
            $area_id           = array();
            $area_id           = $data['area_id'];
            $notification_type = $data['notifictaion_type'];

            $userTable = TableRegistry::get('Users');
             $user      = $userTable->find('list', ['keyField' => 'id', 'valueField' => 'phoneNo'])->where(['area_id IN' => $area_id])->toArray();
            $user1      = $userTable->find()->select(['device_id'])->where(['device_id IS NOT' => null, 'area_id IN' => $area_id])->toArray();

            $UserNotificationsTable = TableRegistry::get('UserNotifications');
            $Notification           = $UserNotificationsTable->newEntity();
            $Notification->message  = $message;
            $Notification->date     = date('Y-m-d');
            $result                 = $UserNotificationsTable->save($Notification);
            if ($notification_type == 'sms') {
                if(!empty($user)){
                    $contacts = implode(',', $user);
                    $this->sendSMS($contacts, $message,SMS_ROUTE);
                    $this->Flash->success(__('Notification has been sent.'));
                }
            } else {
                $push1  = $this->addpush1($user1, $message);
                $this->Flash->success(__('Notification has been sent.'));
            }

        }
        $conditions             = ['user_id IS NULL'];
        $this->paginate         = ['limit' => 10];
        $userNotificationsTable = TableRegistry::get('UserNotifications');
        $user                   = $userNotificationsTable->find('all', ['conditions' => $conditions])->order(['id' => 'desc']);

        $areasTable = TableRegistry::get('Areas');
        $areas      = $areasTable->find('list')->select(['id', 'name'])->hydrate(false)->toArray();
        $user = $this->paginate($user);
        $this->set(compact('user', 'areas'));

    }

    public function notifications()
    {
        $this->paginate         = ['limit' => 10];
        $userNotificationsTable = TableRegistry::get('UserNotifications');
        $user                   = $userNotificationsTable->find('all');
        $results = $this->paginate($user);
        $this->set('user', $results);
    }

    public function contacts()
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $id   = $data['id'];

            $contactsTable = TableRegistry::get('contacts');
            $query         = $contactsTable->query();
            $result        = $query->update()
                ->set(['name' => $data['name'], 'address' => $data['address'], 'phoneNo' => $data['phoneNo'], 'email_id' => $data['email_id']])
                ->where(['id' => $id])
                ->execute();

        }
        $contactsTable = TableRegistry::get('contacts');
        $user          = $contactsTable->find('all')->toArray();
        $this->set('user', $user);

    }

    public function pages($id = null)
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $id   = $data['id'];

            $staticPagesTable = TableRegistry::get('StaticPages');
            $query            = $staticPagesTable->query();
            $result           = $query->update()
                ->set(['title' => $data['title'], 'description' => $data['description']])
                ->where(['id' => $id])
                ->execute();
            $this->Flash->success(__('Page has beed updated'));
            return $this->redirect(['controller' => 'Users', 'action' => 'pages', base64_encode($id)]);

        }
        $id               = base64_decode($id);
        $staticPagesTable = TableRegistry::get('StaticPages');
        $data             = $staticPagesTable->find('all')->where(['id' => $id])->toArray();
        $this->set('data', $data);

    }

    private function isdeletedCustomer($phoneNo)
    {
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->select(['id'])->where(['phoneNo' => $phoneNo, 'type_user' => 'customer', 'is_deleted' => 1])->toArray();

        if (count($user) > 0) {
            $error['statuscode'] = 200;
            $error['user_id']    = $user[0]['id'];
        } else {
            $error['statuscode'] = 201;
        }
        return $error;

    }

    public function settings()
    {

        $settingsTable = TableRegistry::get('Settings');
        $settings      = $settingsTable->find('all')->toArray();
        $this->set('data', $settings);
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $id   = $data['id'];

            $settingsTable = TableRegistry::get('Settings');
            $query         = $settingsTable->query();
            $result        = $query->update()
                ->set(['configuration' => $data['configuration']])
                ->where(['id' => $id])
                ->execute();
            $this->Flash->success(__('Settings has beed updated'));
            return $this->redirect(['controller' => 'Users', 'action' => 'settings']);

        }

    }

    public function dCsv()
    {

        $milliseconds = round(microtime(true) * 1000);

        $this->response->download($milliseconds . '.csv');

        $_header = ['USER_NAME', 'PHONE_NO', 'USER_BALANCE (Rs)', 'REGION_AREA', 'DELIVERY_SCHEDULE_ID', 'ORDER_ITEMS', 'TOTAL_PRICE (Rs)'];

        $finaldata = array();

        $this->paginate = [
            'order'      => [
                'Users.id' => 'desc',
            ],
            'conditions' => ['Users.type_user' => 'customer', 'Users.is_deleted' => 0, 'Users.status' => 1],
        ];
        $user           = $this->paginate($this->Users)->toArray();
        $CustomerApis   = new CustomerApiController;

        $totalorders    = array();
        $finaldata      = array();
        foreach ($user as $key => $value) {
            $data['user_id'] = $value['id'];
            $data['token']   = $value['token'];
            $orderstoday = $CustomerApis->getBalanceAndDeliveryInfo($data, 'future');

            $userTable = TableRegistry::get('Users');
            $user1     = $userTable->find()->contain(['Regions', 'Areas'])->where(['Users.id' => $data['user_id']])->hydrate(false)->toArray();

            $orderstoday['user_id']     = $data['user_id'];
            $orderstoday['user_name']   = $user1[0]['name'];
            $orderstoday['phoneNo']     = $user1[0]['phoneNo'];
            $orderstoday['region_name'] = $user1[0]['region']['name'];
            $orderstoday['area_name']   = $user1[0]['area']['name'];
            array_push($totalorders, $orderstoday);

        }

        $temp = array();
        if (isset($totalorders) && count($totalorders) > 0) {
            foreach ($totalorders as $key => $value) {
                if ($value['messageCode'] == 200 && $value['messageText'] == "success") {

                    $temp['user_name']       = $value['user_name'];
                    $temp['phoneNo']         = $value['phoneNo'];
                    $temp['customerBalance'] = $value['customerBalance'];
                    $temp['region_name']     = $value['region_name'] . "-" . $value['area_name'];

                    $temp['deliver_schdule_id'] = $value['deliver_schdule_id'];
                    $price                      = 0;
                    $name                       = '';
                    if (isset($value['subscriptionItems'])) {
                        foreach ($value['subscriptionItems'] as $key1 => $value1) {
                            $name  = $value1['name'] . "," . $name;
                            $price = $price + $value1['price'];
                        }
                        if (isset($value['orderItems'])) {
                            foreach ($value['orderItems'] as $key2 => $value2) {
                                $name  = $value2['name'] . "," . $name;
                                $price = $price + $value2['price'];
                            }
                        }
                        $temp['items'] = $name1 = rtrim($name, ", ");
                        $temp['price'] = $price;

                    }
                    $finaldata[] = $temp;

                }
            }

            $_serialize = 'finaldata';
            $this->set(compact('finaldata', '_serialize', '_header'));
            $this->viewBuilder()->className('CsvView.Csv');
            return;

        }
    }
    private function saveOrder($data)
    {
        $res               = array();
        $customordersTable = TableRegistry::get('CustomOrders');
        $customorders      = $customordersTable->find()
            ->where(['user_id' => $data['user_id'], 'product_id' => $data['product_id'], 'status' => 0])->count();
        if ($customorders) {
            $res['messageCode'] = 3306;
        } else {
            $customorders             = $customordersTable->newEntity();
            $customorders->user_id    = $data['user_id'];
            $customorders->product_id = $data['product_id'];
            $customorders->quantity   = $data['quantity'];
            if (isset($data['is_chield'])) {
                $productChildrenTable = TableRegistry::get('ProductChildren');
                $productChildren      = $productChildrenTable->find()->select(['price', 'unit_id'])->where(['product_id' => $data['product_id'], 'quantity' => $data['quantity']])->first();
                if ($productChildren == "") {
                    $productTable          = TableRegistry::get('Products');
                    $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                    $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                    $customorders->unit_id = $products['unit_id'];
                } else {
                    $customorders->price   = $productChildren['price'];
                    $customorders->unit_id = $productChildren['unit_id'];
                }
            } else {
                $productTable          = TableRegistry::get('Products');
                $products              = $productTable->find()->where(['id' => $data['product_id']])->select(['price_per_unit', 'unit_id'])->first();
                $customorders->price   = ($products['price_per_unit'] * $data['quantity']);
                $customorders->unit_id = $productChildren['unit_id'];
            }

            $customorders->status = 0;
            $customorders->created  = date('Y-m-d');
            $customorders->modified = date('Y-m-d h:i:s');
            if ($customordersTable->save($customorders)) {
                $res['messageCode'] = 200;
            } else {
                $res['messageCode'] = 201;
            }

        }
        return $res;
    }

    public function addToNextDelivery()
    {
        $response = array();
        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $saveOrder = $this->saveOrder($data);
            if ($saveOrder['messageCode'] == 200) {
                $message                 = "Order has been saved successfully";
                $push                    = $this->pushnotifications($data['user_id'], $message);
                $response['statuscode']  = 13014;
                $response['successCode'] = 1;
                $response['messageText'] = 'Product has been added successfully';
                $this->Flash->success(__('Product has been added successfully'));
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($data['user_id'])]]);
            } else if ($saveOrder['messageCode'] == 3306) {
                $response['statuscode']  = 13015;
                $response['successCode'] = 0;
                $response['messageText'] = 'This Order already made by this customer for next delivery';
                $this->Flash->success(__('This Order already made by this customer for next delivery'));
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($data['user_id'])]]);
            }else if ($saveOrder['messageCode'] == 202) {
                $response['statuscode']  = 202;
                $response['successCode'] = 0;
                $response['messageText'] = 'Requested quantity is out of stock.';
                $this->Flash->success(__('Requested quantity is out of stock'));
                return $this->redirect(['controller' => 'Users', 'action' => 'customerProfile', '?' => ['id' => base64_encode($data['user_id'])]]);
            } else {
                $response['statuscode']  = 13016;
                $response['successCode'] = 0;
                $response['messageText'] = 'Something went wrong while save order';
            }

        }
    }

    public function modifyOrder()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $requestedata = $this->request->getData();
            $user_id      = $requestedata['user_id'];

            $order_id          = $requestedata['s_id'];
            $sub_qty           = $requestedata['sub_qty'];
            $pr_price          = $requestedata['pr_price1'];
            $customOrdersTable = TableRegistry::get('CustomOrders');
            $customorder       = $customOrdersTable->get($order_id);
            $oldqty            = $customorder->toArray();
            $customorder->quantity          = $sub_qty;
            $customorder->quantity_child    = $sub_qty;
            $customorder->price             = $pr_price;
            $customOrdersTable->save($customorder);

            if(@$oldqty['pro_child_id'] != '' && @$oldqty['pro_child_id'] != 0){
                    $productChildrenTable   = TableRegistry::get('ProductChildren');
                    $in_stock   = $productChildrenTable->find()->where(['id' => $oldqty['pro_child_id']])->select(['in_stock'])->first();

                    $query2      = $productChildrenTable->query();
                    $result2     = $query2->update()
                        ->set(['in_stock' => $in_stock['in_stock']-$sub_qty+$oldqty['quantity']])
                        ->where(['id' => @$oldqty['pro_child_id']])
                        ->execute();
                }else{
                    $productTable          = TableRegistry::get('Products');
                    $in_stock1   = $productTable->find()->where(['id' => $oldqty['product_id']])->select(['quantity'])->first();

                    $query1      = $productTable->query();
                    $result1     = $query1->update()
                        ->set(['quantity' => $in_stock1['quantity']-$sub_qty+$oldqty['quantity']])
                        ->where(['id' => $product_id])
                        ->execute();
                }
            return $this->redirect($this->referer());

        }
    }
    public function modifySubscription()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $requestedata                        = $this->request->getData();
            $sub_id                              = $requestedata['s_id'];
            $userSubscriptionsTable              = TableRegistry::get('UserSubscriptions');
            $userSubscription                    = $userSubscriptionsTable->get($sub_id);
            $userSubscription->modified_quantity = $requestedata['sub_qty'];
            $userSubscriptionsTable->save($userSubscription);
            return $this->redirect($this->referer());

        }
    }

    public function driverRouteOrders($id = null)
    {
        $id                 = $_GET['id'];
        $id                 = base64_decode($id);
        $users2Table        = TableRegistry::get('Users');
        $user2              = $users2Table->find()->select(['name'])->where(['id' => $id])->hydrate(false)->first();
        $driverName         = $user2['name'];
        $driverRoutesTable  = TableRegistry::get('DriverRoutes');
        $driverRoutes       = $driverRoutesTable->find('list', ['keyField' => 'id', 'valueField' => 'route_id'])->where(['user_id' => $id])->hydrate(false)->toArray();

        $routeCustomersTable = TableRegistry::get('RouteCustomers');
        $routeCustomers      = $routeCustomersTable->find('list', ['keyField' => 'id', 'valueField' => 'area_id'])->where(['route_id IN' => $driverRoutes])->hydrate(false)->toArray();
        $routeCustomers      = array_unique($routeCustomers);

        $milliseconds = round(microtime(true) * 1000);
        $this->response->download($milliseconds . '.csv');

        $_header = ['AREA', 'Driver_Name', 'CUSTOMER_NAME', 'PHONE_NUMBER', 'SCHEDULE', 'CONTAINER OUT', 'CONTAINER_TO_BE_COLLECTED', 'ORDER_ITEMS', 'TOTAL_PRICE (Rs)'];

        $finaldata = array();

        $usersTable = TableRegistry::get('Users');
        $user       = $usersTable->find()->select(['id', 'token'])->where(['area_id IN' => $routeCustomers, 'is_deleted' => 0, 'type_user' => 'customer', 'status' => 1])->hydrate(false)->toArray();
        $CustomerApis = new CustomerApiController;

        $totalorders = array();
        $finaldata   = array();
        foreach ($user as $key => $value) {
            $data['user_id'] = $value['id'];
            $data['token']   = $value['token'];
            $orderstoday = $CustomerApis->getBalanceAndDeliveryInfo($data, 'future');

            $userTable = TableRegistry::get('Users');
            $user1     = $userTable->find()->contain(['Regions', 'Areas', 'UserContainers'])->where(['Users.id' => $data['user_id']])->hydrate(false)->toArray();

            $orderstoday['user_id']              = $data['user_id'];
            $orderstoday['user_name']            = $user1[0]['name'];
            $orderstoday['phoneNo']              = $user1[0]['phoneNo'];
            $orderstoday['region_name']          = $user1[0]['region']['name'];
            $orderstoday['area_name']            = $user1[0]['area']['name'];
            $orderstoday['container_given']      = @$user1[0]['user_containers'][0]['container_given'];
            $orderstoday['left_container_count'] = @$user1[0]['user_containers'][0]['left_container_count'];

            array_push($totalorders, $orderstoday);

        }

        $temp = array();
        if (isset($totalorders) && count($totalorders) > 0) {
            foreach ($totalorders as $key => $value) {
                if ($value['messageCode'] == 200 && $value['messageText'] == "success") {

                    $temp['area_name']   = $value['area_name'];
                    $temp['Driver_Name'] = $driverName;
                    $temp['user_name']   = $value['user_name'];
                    $temp['phoneNo']     = $value['phoneNo'];
                    $schedule            = '';
                    if (isset($value['subscriptionItems'])) {
                        foreach ($value['subscriptionItems'] as $key11 => $value11) {
                            $schedule = $value11['timeToBeDeliver'] . "," . $schedule;
                        }
                    }
                    $str                        = implode(',', array_unique(explode(',', $schedule)));
                    $str                        = rtrim($str, ", ");
                    $temp['deliver_schdule_id'] = $str;

                    $temp['container_given']      = $value['container_given'];
                    $temp['left_container_count'] = $value['left_container_count'];
                    $price                        = 0;
                    $name                         = '';
                    if (isset($value['subscriptionItems'])) {
                        foreach ($value['subscriptionItems'] as $key1 => $value1) {
                            $name  = $value1['name'] . "," . $name;
                            $price = $price + $value1['price'];
                        }
                        if (isset($value['orderItems'])) {
                            foreach ($value['orderItems'] as $key2 => $value2) {
                                $name  = $value2['name'] . "," . $name;
                                $price = $price + $value2['price'];

                            }
                        }
                        $temp['items'] = $name1 = rtrim($name, ", ");
                        $temp['price'] = $price;

                    }
                    $finaldata[] = $temp;
                }
            }

            $_serialize = 'finaldata';
            $this->set(compact('finaldata', '_serialize', '_header'));
            $this->viewBuilder()->className('CsvView.Csv');
            return;

        }
    }

    public function calculateContainers1($product_id, $globalQty)
    {

        $response     = array();
        $productTable = TableRegistry::get('Products');
        $iscontainer  = $productTable->find()->where(['id' => $product_id, 'iscontainer' => 1])->count();

        if ($iscontainer > 0) {
            $containerRuleTable = TableRegistry::get('ContainerRules');
            $containerRule      = $containerRuleTable->find('all')->where(['subscription_qty' => $globalQty])->toArray();

            $totalConatiners = 0;
            if (count($containerRule)) {
                foreach ($containerRule as $key => $value) {
                    $totalConatiners = $totalConatiners + $value['container_count'];
                }
            }
            $response['status']     = 200;
            $response['containers'] = $containerRule;
            return $response;
        } else {
            $response['status'] = 404;
            return;
        }

    }

    public function orders($dataa = false)
    {

        $finaldata     = array();
        $users2Table   = TableRegistry::get('Users');
        $user2         = $users2Table->find()->select(['id', 'name'])->where(['type_user' => 'driver'])->hydrate(false)->toArray();
        $totalorders   = array();
        $orderstoday1  = array();
        $orderstoday11 = array();
        foreach ($user2 as $kq => $vq) {
            # code...
            $id                = $vq['id'];
            $driverName        = $vq['name'];
            $deliveryTimeTable = TableRegistry::get('DeliverySchdules');
            $deliveryTime      = $deliveryTimeTable->find('list', ['keyField' => 'name', 'valueField' => 'id'])->toArray();

            $routesTable         = TableRegistry::get('Routes');
            $driverRoutesTable   = TableRegistry::get('DriverRoutes');
            $driverRoutes        = $driverRoutesTable->find('list', ['keyField' => 'id', 'valueField' => 'route_id'])->where(['user_id' => $id])->hydrate(false)->toArray();
            $routeCustomersTable = TableRegistry::get('RouteCustomers');
            $routeCustomers      = $routeCustomersTable->find('list', ['keyField' => 'id', 'valueField' => 'area_id'])->where(['route_id IN' => $driverRoutes])->hydrate(false)->toArray();
            $routeCustomers      = array_unique($routeCustomers);
            $usersTable          = TableRegistry::get('Users');
            $user                = $usersTable->find()->contain(['RouteCustomers'])->select(['Users.id', 'Users.token'])->where(['Users.area_id IN' => $routeCustomers, 'Users.is_deleted' => 0, 'Users.type_user' => 'customer', 'Users.status' => 1])->order(['RouteCustomers.position'=> 'ASC'])->hydrate(false)->toArray();
            $CustomerApis        = new CustomerApiController;
            $user                = array_unique($user, SORT_REGULAR);
            foreach ($user as $key => $value) {
                $data['user_id'] = $value['id'];
                $data['token']   = $value['token'];
                if ($this->request->is('post')) {
                    $final          = array();
                    $data1          = $this->request->getData();
                    $data['date']   = $data1['date'];
                    $transtable     = TableRegistry::get('Transactions');
                    $trans          = $transtable->find('all')->select(['Transactions.user_subscription_ids','Transactions.custom_order_ids'])->where(['Transactions.user_id' => $data['user_id'], 'Transactions.created' => $data['date']])->hydrate(false)->toArray();
                    $subids     = array();
                    $custom_ids = array();
                    if (!empty($trans)) {
                        foreach ($trans as $key => $value) {
                            $cids = explode(',', $value['custom_order_ids']);
                            foreach ($cids as $vall) {
                                if (!in_array($vall, $custom_ids)) {
                                    array_push($custom_ids, $vall);
                                }
                            }
                            $customordersTable = TableRegistry::get('CustomOrders');
                            $orderNext         = $customordersTable->find('all')
                                ->where(['CustomOrders.user_id' => $data['user_id'],
                                    'CustomOrders.id IN' => $custom_ids,
                                ])->contain(['Products', 'Units'])->hydrate(false)->toArray();
                            $orderInfo = array();
                            if (count($orderNext) > 0) {
                                foreach ($orderNext as $key1 => $value1) {
                                    $orderItem                   = array();
                                    $orderItem['id']             = $value1['id'];
                                    $orderItem['name']           = $value1['product']['name'];
                                    $insideorders['description'] = @$v['product']['description'];
                                    $orderItem['image']          = @$value1['product']['image'];
                                    $orderItem['pro_id']         = $value1['product']['id'];
                                    $orderItem['quantity']       = $value1['quantity'];
                                    $orderItem['quantity_child']    = @$value1['quantity_child'];
                                    $orderItem['price']          = $value1['price'];
                                    $orderItem['price_per_unit'] = @$value1['product']['price_per_unit'];
                                    $orderItem['unit']           = $value1['unit']['name'];
                                    $orderItem['type']           = 'customorder';
                                    $orderInfo[]                 = $orderItem;
                                }
                            }
                            if (!empty($orderInfo)) {
                                $final['customOrderInfo'] = $orderInfo;
                            }

                        }
                    }

                } else {
                    $data['date'] = date('Y-m-d');
                }
                $orderstoday = $CustomerApis->getBalanceAndDeliveryInfo($data, 'future');
                if (!@$orderstoday['messageCode']) {

                    $ve = array();
                    foreach ($deliveryTime as $key => $value) {
                        if (!empty($orderstoday)) {

                            foreach ($orderstoday as $ky => $ve) {
                                if ($value == $ky) {
                                    $routeCustomers1 = $routeCustomersTable->find()->select(['route_id'])->where(['user_id' => $data['user_id'], 'delivery_schdule_id' => $value])->hydrate(false)->first();
                                    $routes          = $routesTable->find()->select(['name'])->where(['id' => $routeCustomers1['route_id']])->hydrate(false)->first();

                                    $userTable = TableRegistry::get('Users');
                                    $user1     = $userTable->find()->contain(['Regions', 'Areas', 'UserContainers'])->where(['Users.id' => $data['user_id']])->hydrate(false)->toArray();
                                    $cont1     = 0;
                                    $cont2     = 0;
                                    $cont3     = 0;
                                    if ($this->request->is('post')) {
                                        if(@$final['customOrderInfo']){
                                          $orderstoday[$ky]['itemss']['customOrderInfo'] = array_merge($orderstoday[$ky]['itemss']['customOrderInfo'],$final['customOrderInfo']);
                                        }
                                    }
                                    foreach (@$orderstoday[$ky]['itemss'][0]['subscriptionInfo'] as $kye => $vle) {
                                        $container = $this->calculateContainers1($vle['pro_id'], $vle['quantity']);

                                        if ($container['status'] == 200) {
                                            foreach ($container['containers'] as $kyw => $vw) {

                                                if ($vw['container_capacity'] == 1) {
                                                    $cont1 = $cont1 + $vw['container_count'];
                                                } else if ($vw['container_capacity'] == 1.5) {
                                                    $cont2 = $cont2 + $vw['container_count'];
                                                } else {
                                                    $cont3 = $cont3 + $vw['container_count'];
                                                }

                                            }

                                        }
                                    }
                                    /* container_count for custom order *
                                    foreach (@$orderstoday[$ky]['itemss']['customOrderInfo'] as $kye1 => $vle1) {


                                        $container1 = $this->calculateContainers1($vle1['pro_id'], $vle1['quantity']);

                                        if ($container1['status'] == 200) {
                                            foreach ($container1['containers'] as $kyw1 => $vw1) {

                                                if ($vw['container_capacity'] == 1) {
                                                    $cont1 = $cont1 + $vw1['container_count'];
                                                } else if ($vw['container_capacity'] == 1.5) {
                                                    $cont2 = $cont2 + $vw1['container_count'];
                                                } else {
                                                    $cont3 = $cont3 + $vw1['container_count'];
                                                }

                                            }

                                        }
                                    }
                                    /* container_count for custom order */

                                    $transtable = TableRegistry::get('Transactions');
                                    $trans      = $transtable->find('all')->select(['Transactions.user_subscription_ids', 'Transactions.transaction_amount_type', 'Transactions.rejected_reason'])->where(['Transactions.created' => $data['date'], 'Transactions.delivery_schdule_id' => $value, 'Transactions.user_id' => $data['user_id']])->hydrate(false)->toArray();

                                    if (!empty($trans) && count($trans) > 0) {
                                        if ($trans[0]['transaction_amount_type'] == 'rejected') {
                                            $orderstoday1['status'] = 'Rejected';
                                        } else if ($trans[0]['transaction_amount_type'] == 'Dr') {
                                            $orderstoday1['status'] = 'Delivered';
                                        } else {
                                            $orderstoday1['status'] = 'Pending';
                                        }
                                        $orderstoday1['reason'] = $trans[0]['rejected_reason'];
                                    } else {
                                        $orderstoday1['status'] = 'Pending';
                                        $orderstoday1['reason'] = '';
                                    }

                                    $orderstoday1['schedule_id'] = $value;
                                    $orderstoday1['orders']      = $orderstoday[$ky]['itemss'];
                                    $orderstoday1['route_name']  = $routes['name'];
                                    $orderstoday1['route_id']    = $routeCustomers1['route_id'];
                                    $orderstoday1['user_id']     = $data['user_id'];
                                    $orderstoday1['user_name']   = $user1[0]['name'];
                                    $orderstoday1['address']     = $user1[0]['houseNo'];
                                    $orderstoday1['phoneNo']     = $user1[0]['phoneNo'];
                                    $orderstoday1['region_name'] = $user1[0]['region']['name'];
                                    $orderstoday1['area_name']   = $user1[0]['area']['name'];

                                    $orderstoday1['cont_one']  = @$cont1;
                                    $orderstoday1['cont_oneh'] = @$cont2;
                                    $orderstoday1['cont_two']  = @$cont3;

                                    $orderstoday1['container_given']      = @$user1[0]['user_containers'][0]['container_given'];
                                    $orderstoday1['left_container_count'] = @$user1[0]['user_containers'][0]['left_container_count'];

                                    array_push($totalorders, $orderstoday1);
                                }
                            }

                        }

                    }

                }

            }
        }
    if(!$dataa){
        $fnal_data = array();
        $routesf   = $driverRoutesTable->find('all')->contain(['Routes', 'Users', 'DeliverySchdules'])->select(['DeliverySchdules.start_time', 'DeliverySchdules.end_time', 'DriverRoutes.route_id', 'Routes.name', 'Users.name'])->hydrate(false)->toArray();
        $this->set(compact('totalorders', 'routesf'));
    }else{
        return $totalorders;
    }


    }
    public function versions()
    {
        $settingsTable = TableRegistry::get('Settings');
        $settings1     = $settingsTable->find('all')->where(['identifier' => 'ConsumerAppVersion'])->last();
        $settings2     = $settingsTable->find('all')->where(['identifier' => 'DriverAppVersion'])->last();
        $this->set(compact('settings2', 'settings1'));
        if ($this->request->is('post')) {
            $data                    = $this->request->getData();
            $settingsTable           = TableRegistry::get('Settings');
            $settings                = $settingsTable->newEntity();
            $settings->identifier    = $data['app_type'];
            $settings->configuration = $data['version'];
            $settingsTable->save($settings);
            $this->Flash->success(__('Version has beed saved'));
            return $this->redirect(['controller' => 'Users', 'action' => 'versions']);
        }

    }
    public function invoices(){
     $id                = base64_decode($_POST['id']);
     $year = $_POST['year'];
     $user_customer_id = $id;
     $this->set(compact('user_customer_id','year'));
    }
    public function viewtransaction($id = null)
    {
            $id = base64_decode($_GET['id']);
            $transactionTable = TableRegistry::get('Transactions');
            $transactions     = $transactionTable->find('all')->contain(['Users','TransactionTypes'])->where(['Transactions.id' => $id])->toArray();
            $subIds     = explode(",",$transactions[0]['user_subscription_ids']);
            $cusIds     = explode(",",$transactions[0]['custom_order_ids']);

            $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
            $customOrdersTable = TableRegistry::get('CustomOrders');

            $userSub     = $userSubscriptionsTable->find('all')->contain(['Products.Units'])->where(['UserSubscriptions.id IN' => $subIds])->toArray();

            $customOrders     = $customOrdersTable->find('all')->contain(['Products.Units'])->where(['CustomOrders.id IN' => $cusIds])->toArray();
            $this->set(compact('transactions','userSub','customOrders'));

    }

    public function paymentMethod()
    {
      $paymentMethodsTable = TableRegistry::get('payment_methods');
      $paymentMethods = $paymentMethodsTable->find('all')->toArray();
      $this->set('data', $paymentMethods);

      if ($this->request->is('post')) {
          $data = $this->request->getData();
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
          // die;
          // $method   = $data['method'];
          $paymentMethodsTable->updateAll(
              ['status' => 0], // fields
              ['status' => 1] // conditions
          );
          foreach ($data as $key=>$entity) {
              // Update entity
              $paymentMethodsTable = TableRegistry::get('payment_methods');
              $query = $paymentMethodsTable->query();
              $result = $query->update()
                  ->set(['status' => $entity])
                  ->where(['method' => $key])
                  ->execute();
          }

          $this->Flash->success(__('Settings has beed updated'));
          return $this->redirect(['controller' => 'Users', 'action' => 'paymentMethod']);

      }
    }

    public function addPaytm($id='')
    {
      if ($id) {
        # code...
        $paytms = TableRegistry::get('paytms');
        $query = $paytms->find('all')->where(['id' => $id])->toArray();
        $this->set('data', $query);
        if ($this->request->is('post')) {
          $data = $this->request->getData();

          // Update entity
          if ($data['status']==1) {
            # desable other
            $paytms->updateAll(
              ['status' => 0], // fields
              ['status' => 1] // conditions
            );
          }

          $query = $paytms->query();
          $result = $query->update()
              ->set([
                'merchant_key' => $data['merchant_key'],
                'merchant_mid' => $data['merchant_mid'],
                'merchant_website' => $data['merchant_website'],
                'status' => $data['status'],
                'description' => $data['description'],
                'modified' => date("Y-m-d H:i:s"),
              ])
              ->where(['id' => $data['merchant_id']])
              ->execute();
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
          // die;
          $this->Flash->success(__('Details has beed updated'));
          return $this->redirect(['controller' => 'Users', 'action' => 'addPaytm',$id]);
        }
      }else {
        # for adding merchant details provided by paytm
        if ($this->request->is('post')) {
          $data = $this->request->getData();

          $data['created'] = date("Y-m-d H:i:s");
          $paytmsTable = TableRegistry::get('paytms');
          $paytm = $paytmsTable->newEntity($data);

          if ($paytmsTable->save($paytm)) {
            // The $article entity contains the id now
            $id = $paytm->id;
          }

          $this->Flash->success(__('Details has beed saved'));
          return $this->redirect(['controller' => 'Users', 'action' => 'addPaytm']);
        }
      }
    }

    public function addPayumoney($id='')
    {
      if ($id) {
        # code...
        $payumoneys = TableRegistry::get('payumoneys');
        $query = $payumoneys->find('all')->where(['id' => $id])->toArray();
        $this->set('data', $query);
        if ($this->request->is('post')) {
          $data = $this->request->getData();
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
          // die;

          // Update entity
          if ($data['status']==1) {
            # desable other
            $payumoneys->updateAll(
              ['status' => 0], // fields
              ['status' => 1] // conditions
            );
          }

          $query = $payumoneys->query();
          $result = $query->update()
              ->set([
                'merchant_key' => $data['merchant_key'],
                'merchant_salt' => $data['merchant_salt'],
                'status' => $data['status'],
                'description' => $data['description'],
                'modified' => date("Y-m-d H:i:s"),
              ])
              ->where(['id' => $data['merchant_id']])
              ->execute();
          $this->Flash->success(__('Details has beed updated'));
          return $this->redirect(['controller' => 'Users', 'action' => 'addPayumoney',$id]);
        }
      }else {
        # for adding merchant details provided by payumoney
        if ($this->request->is('post')) {
          $data = $this->request->getData();
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
          // die;
          $data['created'] = date("Y-m-d H:i:s");
          $payumoneysTable = TableRegistry::get('payumoneys');
          $payumoney = $payumoneysTable->newEntity($data);

          if ($payumoneysTable->save($payumoney)) {
            // The $article entity contains the id now
            $id = $payumoney->id;
          }

          $this->Flash->success(__('Details has beed saved'));
          return $this->redirect(['controller' => 'Users', 'action' => 'addPayumoney']);
        }
      }
    }

    public function listPaytm()
    {
      # list paytm merchant accounts
      $paytmsTable = TableRegistry::get('paytms');
      $paytms = $paytmsTable->find('all')->toArray();
      $this->set('data', $paytms);
    }

    public function listPayumoney()
    {
      # list payumoney merchant accounts
      $payumoneysTable = TableRegistry::get('payumoneys');
      $payumoneys = $payumoneysTable->find('all')->toArray();
      $this->set('data', $payumoneys);
    }

    public function listPaymentmethods()
    {
      # list payment merchant accounts
      $table = TableRegistry::get('payment_methods');
      $payment_methods = $table->find('all')->toArray();
      $this->set('data', $payment_methods);
    }

    public function updatePaymentmethod($id='')
    {
      # code...
      $table = TableRegistry::get('payment_methods');
      $query = $table->find()->where(['id' => $id])->first();
      $this->set('data', $query);

      if ($this->request->is('post')) {
        $data = $this->request->getData();

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;
        // Update entity

        $query = $table->query();

        if ($data['method_id']==2) {
          # method paytm
          $result = $query->update()
              ->set([
                'merchant_key' => $data['merchant_key'],
                'merchant_mid' => $data['merchant_mid'],
                'merchant_website' => $data['merchant_website'],
                'status' => $data['status'],
                'description' => $data['description'],
                'modified' => date("Y-m-d H:i:s"),
              ])
              ->where(['id' => $data['method_id']])
              ->execute();
        }elseif ($data['method_id']==3) {
          # mathod PayUmoney...
          $result = $query->update()
          ->set([
            'merchant_key' => $data['merchant_key'],
            'merchant_salt' => $data['merchant_salt'],
            'status' => $data['status'],
            'description' => $data['description'],
            'modified' => date("Y-m-d H:i:s"),
          ])
          ->where(['id' => $data['method_id']])
          ->execute();
        } else {
          # method coupon
          $result = $query->update()
          ->set([
            'status' => $data['status'],
            'description' => $data['description'],
            'modified' => date("Y-m-d H:i:s"),
          ])
          ->where(['id' => $data['method_id']])
          ->execute();
        }

        $this->Flash->success(__('Details has beed updated'));
        return $this->redirect(['controller' => 'Users', 'action' => 'updatePaymentmethod',$id]);
      }
    }
}
