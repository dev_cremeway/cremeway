<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[] paginate($object = null, array $settings = [])
 */
class RoutesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['getSchdule']);
    }
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Categories', 'Units'],
            'limit'=>10
        ];
        $products = $this->paginate($this->Products);
        $this->set(compact('products'));*/
       
    }
     private function getroute( $name ){

            $areaTable = TableRegistry::get('Routes');
               $areaList = $areaTable->find('all')
                                     ->where(['name'=>$name])
                                     ->hydrate(false)
                                     ->toArray();
                                     if(count($areaList)>0){
                                        return true;
                                     }return false;


    }
    private function routeAlreadyAssignedWithSameTime($driverid,$deliverytiming){

           
       /* $driverRouteTable = TableRegistry::get('DriverRoutes');
                    $driverRoute = $driverRouteTable->find()->where(['user_id'=>$driverid,'delivery_schdule_id'=>$deliverytiming])->count();
                     if($driverRoute > 0 ){
                      return true;
                     } */
                
                return false;
          

    }
    private function validateAddRoutes( $data ){

             $error = array();
                  $data['name'] = trim($data['name']);
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the Route name';
                  }else if( $this->getroute( trim( $data['name'] ) ) ){
                    $error['name'] = 'Route Name already exist';
                  }else if( ! isset( $data['driver_id'] ) || empty( $data['driver_id'] ) ){
                     $error['name'] = 'Please select the driver';
                  }else if( ! isset( $data['deliverytiming'] ) || empty( $data['deliverytiming'] ) ){
                     $error['name'] = 'Please select Delivery Timing';
                  } else if( ! isset( $data['region_area'] ) || empty( $data['region_area'] ) ){
                     $error['name'] = 'Please fill delivery plcae values are properly';
                  }else if($this->routeAlreadyAssignedWithSameTime($data['driver_id'],$data['deliverytiming'])){
                     $error['name'] = 'This Time Route already assigned to this driver Please select other timing';
                  }
                  
                   
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                     $error['statuscode'] = 200;
                  }
         return $error;
   
    }

    private function getAllCustomer( $regionID, $areaID,$delivery_schdule_id ){

         $usersTable = TableRegistry::get('Users');
         $subscriptionTable = TableRegistry::get('UserSubscriptions');
         $users = $usersTable->find('all')->select(['id'])->where(['region_id'=>$regionID,'area_id'=>$areaID,'type_user'=>'customer'])->hydrate(false)->toArray();
         $userHaveSubscriptionForThis = array(); 
         if(count($users)>0){
 
                   foreach ($users as $key => $value) {
                         
                        $subscription  = $subscriptionTable->find('all')->select(['delivery_schdule_ids','user_id'])->where(['user_id'=>$value['id']])->toArray();
                          if( isset($subscription) && count($subscription) >0 ){
                             
                                  
                                  foreach ($subscription as $ke => $val) {

                                        $d_s_ids = explode('-', $val['delivery_schdule_ids']);

                                        if(in_array($delivery_schdule_id, $d_s_ids)){

                                           $userHaveSubscriptionForThis[] = $val['user_id'];

                                        }

                                     
                                  }


                          } 

                   }
                  

         }
          

        return array_unique($userHaveSubscriptionForThis);  

    }

    private function SaverouteCustomer( $data, $routeID ){

                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                foreach ($data['region_area'] as $key => $value) {
                              $region_area = explode('_', $value);
                              $region = $region_area[0];
                              $area = $region_area[1];
                              $getAllCustomer = $this->getAllCustomer($region,$area,$data['deliverytiming']);
                              //pr($getAllCustomer);die;
                              if(count($getAllCustomer)>0){
                                     foreach ($getAllCustomer as $k => $v) {
                                         //foreach ($data['deliverytiming'] as $ke => $val) {
                                          $routeCustomer = $routeCustomerTable->newEntity();
                                          $routeCustomer->user_id = $v;
                                          $routeCustomer->route_id = $routeID;
                                          $routeCustomer->delivery_schdule_id  = $data['deliverytiming'];
                                          $routeCustomer->position  = '';
                                          $routeCustomer->date  = '';
                                          $routeCustomer->region_id  = $region;
                                          $routeCustomer->area_id  = $area;
                                          $routeCustomer->status  = 0;
                                          //pr($routeCustomer);die;
                                          $routeCustomerTable->save($routeCustomer);
                                        // }
                                     }
                              }else{
                                 
                                          //foreach ($data['deliverytiming'] as $ke => $val) {
                                          $routeCustomer = $routeCustomerTable->newEntity();
                                          //$routeCustomer->user_id = $v['id'];
                                          $routeCustomer->route_id = $routeID;
                                          $routeCustomer->delivery_schdule_id  = $data['deliverytiming'];
                                          $routeCustomer->position  = '';
                                          $routeCustomer->date  = '';
                                          $routeCustomer->region_id  = $region;
                                          $routeCustomer->area_id  = $area;
                                          $routeCustomer->status  = 0;
                                          //pr($routeCustomer);die;
                                          $routeCustomerTable->save($routeCustomer);
                                        // }
                              }
                }


   return true;

    }

 public function changeRouteDriver(){
       
         if(isset($_GET['route_id'] ) && ! empty($_GET['route_id'])){
              
              
          $route_id = base64_decode($_GET['route_id']);
          $routeTable = TableRegistry::get('Routes');
          $routeName = $routeTable->find()->select('name')->where(['id'=>$route_id])->first();
          if(isset($routeName)&&!empty($routeName)){
            
             $usersTable = TableRegistry::get('Users');
             $drivers = $usersTable->find('list')->where(['Users.type_user'=>'driver','Users.is_deleted'=>0])->toArray();

            $nameroute = $routeName['name'];    
            $this->set(compact('drivers','nameroute','route_id'));

          }
        }

         else if($this->request->is('post')){

             $data = $this->request->getData();
             $route_id = $data['route_id'];
             $user_id = $data['user_id'];
             
             $driverroutesTable = TableRegistry::get('DriverRoutes');
             $driverroutes = $driverroutesTable->find('all')->where(['route_id'=>$route_id])->toArray();
             if(isset($driverroutes) && count($driverroutes)>0){

               
               foreach ($driverroutes as $key => $value) {
                 
                 $query = $driverroutesTable->query();
                                $result = $query->update()
                                  ->set(['user_id' => $user_id])
                                  ->where(['route_id' => $route_id])
                                  ->execute();
               }
                       

             }
                $this->Flash->success(__('Driver Has beed set to this Route'));
                return $this->redirect(['action' => 'lists']);



         }else{
          die('Invalid Access Id');
         }  

 }


   public function lists(){



             
           if(isset($_GET['driverid'])){
                             

             if(isset($_GET['driverid']) && !empty($_GET['driverid'])){

                  

                  $driverroutesTable = TableRegistry::get('DriverRoutes');
                            $filterby = '';
                            $page = 0;

                            if(isset($_GET['page']) && ! empty($_GET['page'])){
                            $page = $_GET['page'];
                          } 


                            $this->paginate = [
                             'sortWhitelist' => [
                                        'Routes.name','Users.name'
                                  ],
                                     'limit'=>10,
                                     'conditions'=>['DriverRoutes.user_id'=>$_GET['driverid']],
                                      'contain' => [
                                                       'Routes',
                                                       'Users' => function (\Cake\ORM\Query $query)  {
                                                          return $query->where(['Users.is_deleted' => 0]);
                                                      }
                                                  ],
                                                  'order'=>['Routes.id'=>'DESC'],
                                                  'group'=>['DriverRoutes.user_id','DriverRoutes.route_id']
                              ];


            $routes = $this->paginate($driverroutesTable)->toArray();

            $this->set(compact('routes')); 
            $driverTable = TableRegistry::get('Users');
            $driver = $driverTable->find('list')->where(['Users.type_user'=>'driver','Users.is_deleted'=>0])->toArray();
            $this->set('driver',$driver);
            $this->set('driver_id',$_GET['driverid']);




             }           





           }else if(isset($_GET['query']))
                    {
                            $driverroutesTable = TableRegistry::get('DriverRoutes');
                            $filterby = '';
                            $page = 0;
                            $routesname = $_GET['query'];
                            if(isset($_GET['page']) && ! empty($_GET['page'])){
                            $page = $_GET['page'];
                          } 


                            $this->paginate = [
                            'sortWhitelist' => [
                                        'Routes.name','Users.name'
                                  ],
                                     'limit'=>10,
                                     'contain' => [
                                                       'Routes'=> function (\Cake\ORM\Query $query) use($routesname)  {
                                                          return $query->where(['Routes.name LIKE'=>'%'.$routesname.'%']);
                                                      },
                                                       'Users' => function (\Cake\ORM\Query $query)  {
                                                          return $query->where(['Users.is_deleted' => 0]);
                                                      }
                                                  ],
                                                  'order'=>['Routes.id'=>'DESC'],
                                                  'group'=>['DriverRoutes.user_id','DriverRoutes.route_id']
                              ];


                      $routes = $this->paginate($driverroutesTable)->toArray();
                       
                      $this->set(compact('routes')); 
                      if(isset($_GET['query']) && !empty($_GET['query'])){
                                  $this->set('querystring',$_GET['query']);
                                }

                      


                  }









           else{
             $driverroutesTable = TableRegistry::get('DriverRoutes'); 

             $this->paginate = [
             'sortWhitelist' => [
                                        'Routes.name','Users.name'
                                  ],
                                     'limit'=>10,
                                      'contain' => [
                                                       'Routes',
                                                       'Users' => function (\Cake\ORM\Query $query)  {
                                                          return $query->where(['Users.is_deleted' => 0]);
                                                      }
                                                  ],
                                                  'order'=>['Routes.id'=>'DESC'],
                                                  'group'=>['DriverRoutes.user_id','DriverRoutes.route_id']
                              ];

            $routes = $this->paginate($driverroutesTable)->toArray();
             //pr($routes);die;
              
            $this->set(compact('routes')); 
            $driverTable = TableRegistry::get('Users');
            $driver = $driverTable->find('list')->where(['Users.type_user'=>'driver','Users.is_deleted'=>0])->toArray();
            $this->set('driver',$driver);
             

          }

}


      private  function getDeliverSchdules($deliveryschduleid)
        {
          $regionAreaFinal = array();
          $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules');
            $categoryDeliverySchdule = $categoryDeliverySchduleTable->find('all')->where(['CategoryDeliverySchdules.delivery_schdule_id'=>$deliveryschduleid])->select([
                  'Areas.id','Regions.id','Areas.name','Regions.name']
               )->contain([
                   
                   'Regions' => function (\Cake\ORM\Query $query)  {
                                                        return $query->where(['Regions.status' => 1]);
                                                    },
                   'Areas' => function (\Cake\ORM\Query $query)  {
                                                        return $query->where(['Areas.status' => 1]);
                                                    }                                 



            ])->group(['CategoryDeliverySchdules.region_id','CategoryDeliverySchdules.area_id'])->toArray();

            if(isset($categoryDeliverySchdule)&&count($categoryDeliverySchdule)){
                           
                       
                       foreach ($categoryDeliverySchdule as $key => $value) {
                              $temp = array();
                              
                            if( 
                               ( isset($value['Regions']) && count($value['Regions']) > 0 ) 
                               &&
                               ( isset($value['Areas']) && count($value['Areas']) > 0 ) 
                               ){

                                  
                                  $regionAreaFinal[] = $value['Regions']['id'].'_'.$value['Areas']['id'];
                          }
                       }    
                    }    
                return $regionAreaFinal;
         
        }

   private function getFinalRegionArea($remainingarea_region_this_time,$FLAG){

    //pr($remainingarea_region_this_time);die;

   $regionAreaFinal = array();
   $allun_assigined_places = array();

   $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules');
    
   foreach ($remainingarea_region_this_time as $key=> $value) {
    
    $filter = explode('_', $value);
    $region_id = $filter[0];
    $area_id = $filter[1]; 
    $conditions = ['CategoryDeliverySchdules.region_id'=>$region_id,'CategoryDeliverySchdules.area_id'=>$area_id];
    $categoryDeliverySchdule = $categoryDeliverySchduleTable->find('all',[
                                  'select' =>['Areas.id','Regions.id','Areas.name','Regions.name'], 
                                  'contain'=>[
                                  'Regions' => function (\Cake\ORM\Query $query)  {
                                               return $query->where(['Regions.status' => 1]);
                                                            },
                                  'Areas' => function (\Cake\ORM\Query $query)  {
                                                return $query->where(['Areas.status' => 1]);
                                    }  

                                  ],
                                  'group'=>['CategoryDeliverySchdules.region_id','CategoryDeliverySchdules.area_id'], 
                                  'conditions'=>$conditions
                                  ])->toArray();
      $regionAreaFinal[] = $categoryDeliverySchdule;   
          
    }



    if(isset($regionAreaFinal)&&count($regionAreaFinal)){
                           
                       
                       foreach ($regionAreaFinal as $key => $value) {
                              $temp = array();
                              
                            if( 
                               ( isset($value[0]['region']) && count($value[0]['region']) > 0 ) 
                               &&
                               ( isset($value[0]['area']) && count($value[0]['area']) > 0 ) 
                               ){

                                  $temp['area_region_id'] = $value[0]['region']['id'].'_'.$value[0]['area']['id'];
                                           if($FLAG){
                                                $temp['added'] = 1;
                                               }else{
                                                $temp['added'] = 0;
                                               }
                                  
                                  $temp['area_region_name'] = $value[0]['region']['name'].'_'.$value[0]['area']['name'];
                                  $allun_assigined_places[] = $temp;
                          }
                       }    
                    } 
    return $allun_assigined_places;
          


   }
   private function getrouteEdit($name,$route_id){


            $areaTable = TableRegistry::get('Routes');
               $areaList = $areaTable->find('all')
                                     ->where(['name'=>$name,'id <>'=>$route_id])
                                     ->hydrate(false)
                                     ->toArray();
                                     if(count($areaList)>0){
                                        return true;
                                     }return false;


     
   }

   private function validateEditRoutes( $data ){
            /* pr($data);die;*/
             $error = array();
                  $data['name'] = trim($data['name']);
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the Route name';
                  }else if( $this->getrouteEdit( trim( $data['name'] ),$data['route_id'] ) ){
                    $error['name'] = 'Route Name already exist';
                  }else if( ! isset( $data['driver_id'] ) || empty( $data['driver_id'] ) ){
                     $error['name'] = 'Please select the driver';
                  }else if( ! isset( $data['deliverytiming'] ) || empty( $data['deliverytiming'] ) ){
                     $error['name'] = 'Please select Delivery Timing';
                  } else if( ! isset( $data['region_area'] ) || empty( $data['region_area'] ) ){
                     $error['name'] = 'Please fill delivery plcae values are properly';
                  }
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                     $error['statuscode'] = 200;
                  }
         return $error;
   
    }     



   public function getSchdule($delivery_schdule_id=null)
    {   
        date_default_timezone_set('Asia/Kolkata');
        if( $this->request->is('post') ){ 
          $data                 = $this->request->getData();
          $delivery_schdule_id  = $data['schedule_id'];
        }
        $time   = date('H:i');
        $response = array();
        $driverRouteTable = TableRegistry::get('DeliverySchdules');
        $schdule          = $driverRouteTable->find('all')->where(['id'=>$delivery_schdule_id])->hydrate(false)->toArray();

        if (count($schdule) > 0) {

            foreach ($schdule as $key => $value) {

                $newDateTime                 = date('h:i A', strtotime($value['start_time']));
                $schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

                $newDateTime               = date('h:i A', strtotime($value['end_time']));
                $schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));
                $changetime = date('H:i', strtotime($value['start_time']) - $value['edit_time']*3600);
                
                if($time < $changetime){
                    $schdule[$key]['edit_order'] = "yes"; 
                }else{
                    $schdule[$key]['edit_order'] = "no";
                }
                $schdule[$key]['edit_order_time'] = date('h:i A', strtotime($changetime));
                $schdule[$key]['current_date'] = date('Y-m-d');

            }
            $response['messageText'] = "success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
            $response['schduleInfo'] = $schdule;
        } else {

            $response['messageText'] = "success";
            $response['messageCode'] = 200;
            $response['successCode'] = 1;
            $response['schduleInfo'] = count($schdule);
        }
        
        if( $this->request->is('post') ){
          echo json_encode($response); die;
        }else{
          return $response;
        }
    } 

   public  function edit($id,$delivery_schdule_id) {
              
             if( 
                  ( isset ( $id )  && ! empty( $id ) ) && ( isset( $delivery_schdule_id ) && ! empty( $delivery_schdule_id ) )
                  &&
                  ( ! $this->request->is('post') )
              ) 
             {
              

              $id = base64_decode( $id );
              $delivery_schdule_id = base64_decode( $delivery_schdule_id );
              $routeCustomerTable = TableRegistry::get('RouteCustomers');
              $routeCustomer = $routeCustomerTable->find('all')->select(['area_id','region_id'])->where(['RouteCustomers.route_id'=>$id,'RouteCustomers.delivery_schdule_id'=>$delivery_schdule_id])->toArray();
              $region_area_route_customers = array();

              if( isset( $routeCustomer ) && count( $routeCustomer ) > 0 ){
                    
                    foreach ($routeCustomer as $key => $value) {
                     
                     $region_area_route_customers[] = $value['region_id'].'_'.$value['area_id'];

                     }


                $routeCustomer_unadded = $routeCustomerTable->find('all')->select(['area_id','region_id'])->where(['RouteCustomers.route_id <>'=>$id,'RouteCustomers.delivery_schdule_id'=>$delivery_schdule_id])->Andwhere(['RouteCustomers.route_id !='=> 0])->toArray();
                //pr($routeCustomer_unadded); die;
              $region_area_route_customers_unadded = array();

              if( isset( $routeCustomer_unadded ) && count( $routeCustomer_unadded ) > 0 ){
                    
                    foreach ($routeCustomer_unadded as $ke => $valu) {
                     
                     $region_area_route_customers_unadded[] = $valu['region_id'].'_'.$valu['area_id'];

                     }
                   }
                   
                   $getthisTimigRegioArea = $this->getDeliverSchdules($delivery_schdule_id);
                
                   $added_in_route = array_intersect($getthisTimigRegioArea, $region_area_route_customers);
                   $unadded_in_route = array_diff($getthisTimigRegioArea, $region_area_route_customers_unadded);

                   $unadded_in_route = array_diff($unadded_in_route, $added_in_route);
                  
                  if( isset( $added_in_route ) && count( $added_in_route ) > 0 ){

                     $all_added_already = $this->getFinalRegionArea($added_in_route,1);
                     $all_unadded_in_route = $this->getFinalRegionArea($unadded_in_route,0);
                     $all_added_unadded = array_merge($all_added_already,$all_unadded_in_route);
                     //pr($all_added_unadded);die;
                     $this->set(compact('all_added_unadded','id','delivery_schdule_id','user'));

                  }



              }

              $usersTable   = TableRegistry::get('Users');
              $users = $usersTable
                  ->find('all')->select(['id','name'])
                  ->where(['Users.type_user' => 'driver','Users.status' => 1])
                  ->toArray();

              $driverroutesTable = TableRegistry::get('DriverRoutes'); 

               
              $driver_routes_name = $driverroutesTable->find('all',[
                 'conditions'=>['DriverRoutes.route_id'=>$id],
                 'contain' => [
                                                       'Routes',
                                                       'Users' => function (\Cake\ORM\Query $query)  {
                                                          return $query->where(['Users.is_deleted' => 0]);
                                                      }
                                                  ]
                                                  ])->toArray();
              
              $edit_route = $this->getSchdule($driver_routes_name[0]['delivery_schdule_id']);
              $this->set('driver_routes_name',$driver_routes_name);
              $this->set('edit_route',$edit_route);
              $this->set('users',$users);                                    

              $categoryDeliverySchduleTable = TableRegistry::get('DeliverySchdules');
              $categoryDeliverySchdule = $categoryDeliverySchduleTable->find()->select(['name','id'])->where(['id'=>$delivery_schdule_id])->first();
              $this->set('categoryDeliverySchdule',$categoryDeliverySchdule);
             }else if( $this->request->is('post') ){
              /*pr($this->request->getData());die; */
              $error = $this->validateEditRoutes($this->request->getData());
               if($error['statuscode'] == 200)
                {
                 $data = $this->request->getData();
                 $routesTable = TableRegistry::get('Routes');
                 $routes = $routesTable->get($data['route_id']);
                 $routes->name = trim($data['name']);
                 $result = $routesTable->save($routes);
                 if($result->id){

                     $driverRoutesTable = TableRegistry::get('DriverRoutes');
                     //$driverRoutes = $routesTable->get($data['route_id']);
                     //$driverRoutes->user_id=$data['driver_id'];
                     $query       = $driverRoutesTable->query();
                     $result1      = $query->update()
                        ->set(['user_id' => $data['driver_id']])
                        ->where(['route_id' => $data['route_id']])
                        ->execute();
                     

                     $routeCustomerTable = TableRegistry::get('RouteCustomers');
                     $routeIDSDONT = array();
                     $routeIDSDO = array();
                     $regions = array();
                     $areas = array();

                      foreach ($data['region_area'] as $key => $value) {
                         $area_region = explode('_', $value);
                         $region = $area_region[0];
                         $area = $area_region[1];
                         array_push($areas, $area);
                         array_push($regions, $region);
                         $isExist = $routeCustomerTable->find()->select(['id'])->where(['route_id'=>$data['route_id'],'delivery_schdule_id'=>$data['deliverytiming'],'region_id'=>$region,'area_id'=>$area])->first();

                         if(count($isExist) > 0 ) { 
                            
                              
                              array_push($routeIDSDO, $isExist['id']);

                            
                              $isExist_data = $routeCustomerTable->find('all')->select(['id'])->where(['route_id'=>$data['route_id'],'delivery_schdule_id'=>$data['deliverytiming'],'region_id <>'=>$region,'area_id <>'=>$area])->toArray();
                              
                              foreach ($isExist_data as $kee => $valee) {
                                 array_push($routeIDSDONT, $valee['id']);
                               }
                           

                           unset($data['region_area'][$key]); 

                         } 

                      }
                      /* unselect routes */
                      $route_to_updated = $routeCustomerTable->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['route_id'=>$data['route_id'],'delivery_schdule_id'=>$data['deliverytiming'],'area_id NOT IN'=>$areas])->toArray();
                      if(count($route_to_updated)>0){
                        /*$query   = $routeCustomerTable->query();
                        $result1 = $query->update()
                                ->set(['route_id' => NULL])
                                ->where(['id IN' => $route_to_updated])
                                ->execute();*/
                          foreach ($route_to_updated as $ke => $va) {
                            $entity = $routeCustomerTable->get($va);
                            $result = $routeCustomerTable->delete($entity);
                          }
                                
                      }
                             
                    /* unselect routes */            
                    if( 
                       ( isset($routeIDSDONT) && count( $routeIDSDONT ) > 0 )
                       ||
                       ( isset($routeIDSDO) && count( $routeIDSDO ) > 0 )
                     ) {
                        $routeIDSDONT = array_unique($routeIDSDONT);
                        $routeIDSDO = array_unique($routeIDSDO);

                        $mustBeDeleted = array_diff($routeIDSDONT, $routeIDSDO);
                        if( ( isset($mustBeDeleted)) && count( $mustBeDeleted ) > 0 ){

                           $routeCustomerTable->deleteAll(['id IN' => $mustBeDeleted]);

                        } 

                      }
                      $input = array_values($data['region_area']);
                      unset($data['region_area']);
                      $data['region_area'] = $input;

                       $saved = $this->SaverouteCustomer($data,$data['route_id']);
                       if($saved){
                          
                          $error['statuscode'] = 200;
                          $error['name'] = "Route Has Been Updated successfully";
                          echo json_encode($error);die; 

                       }else{
                          
                          $error['statuscode'] = 201;
                          $error['name'] = "Please try again Something went wrong.";
                          echo json_encode($error);die; 
                       } 

                 } 

                }else{
               echo json_encode($error);die;
              }
             }
             $userTable = TableRegistry::get('Users');
              $user = $userTable->find()->select(['name','id'])->where(['type_user'=> 'driver'])->toArray();
              $this->set('user', $user);
           
    }
    public function add(){

      

       if ($this->request->is('post')) {

         $data = $this->request->getData();
           

          $error = $this->validateAddRoutes($this->request->getData());
          if($error['statuscode'] == 200)
            {  
                  
                   $routesTable = TableRegistry::get('Routes');
                   $routes = $routesTable->newEntity();
                   $routes->name = trim($data['name']);
                   $routes->created = date('Y-m-d');
                   $result = $routesTable->save($routes);
                   $globalRes = '';
                   if($result->id){
                           
                           $driveroutesTable = TableRegistry::get('DriverRoutes');
                           
                           //foreach ($data['deliverytiming'] as $key => $value) {

                           $driverroutes = $driveroutesTable->newEntity();
                           $driverroutes->route_id = $result->id;
                           $driverroutes->user_id = $data['driver_id'];
                           $driverroutes->tracking = '';
                           $driverroutes->delivery_schdule_id = $data['deliverytiming'];
                           $globalRes = $driveroutesTable->save($driverroutes);
                          // }

                          if($globalRes){

                               /*-----save data in route customer table -----*/
                               $saved = $this->SaverouteCustomer($data,$result->id);
                               if($saved){
                                  
                                  $error['statuscode'] = 200;
                                  $error['name'] = "Saved successfully";

                                  
                                  echo json_encode($error);die; 

                               }else{
                                  
                                  $error['statuscode'] = 201;
                                  $error['name'] = "Please try again Something went wrong.";
                                  echo json_encode($error);die; 
                               }
                               /*-----save data in route customer table -----*/


                          }else{

                            $error['statuscode'] = 201;
                            $error['name'] = "Please try again Something went wrong.";
                            echo json_encode($error);die; 
                          } 
                   }else{

                    $error['statuscode'] = 201;
                    $error['name'] = "Please try again Something went wrong.";
                    echo json_encode($error);die; 
                   }
            }else{
             echo json_encode($error);die;
            }
          }else{
            $driverTable = TableRegistry::get('Users');
            $drivers = $driverTable->find('list')->where(['Users.type_user'=>'driver','Users.is_deleted'=>0])->hydrate(false)->toArray();
            $DeliverySchdulesTables = TableRegistry::get('DeliverySchdules');
            $DeliverySchdules = $DeliverySchdulesTables->find('list')->where(['DeliverySchdules.status'=>1]);
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            $driverroutesTable = TableRegistry::get('DriverRoutes');
            

            $routes = $driverroutesTable->find('all')->contain([
                                       'Routes',
                                       'Users'
                                       ])->order(['Routes.id'=>'DESC'])->group(['DriverRoutes.user_id'])->hydrate(false)->toArray();
   
            $this->set(compact('routes','DeliverySchdules','drivers'));

          }
        }

    public function getAllAreaReginsList(){

          $response = array();
          $regionAreaFinal = array();
          if($_POST){

            $deliveryschduleid = $_POST['deliveryschduleid'];
            
            /*---already assigned region area------*/
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            
            $regionids = ['0'];

            $routeCustomer = $routeCustomerTable->find('all')->select(['area_id'])->where(['delivery_schdule_id'=>$deliveryschduleid])->group(['region_id','area_id'])->toArray();
            if(isset($routeCustomer) && count( $routeCustomer ) >0 ){
              foreach ($routeCustomer as $key => $value) {
                         
                       array_push($regionids, $value['area_id']);     
              }
            }

            /*---already assigned region area------*/ 



            $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules');
            $categoryDeliverySchdule = $categoryDeliverySchduleTable->find('all')->where(['CategoryDeliverySchdules.delivery_schdule_id'=>$deliveryschduleid,'CategoryDeliverySchdules.area_id NOT IN'=>$regionids])->select([
                  'Areas.id','Regions.id','Areas.name','Regions.name']
               )->contain([
                   
                   'Regions' => function (\Cake\ORM\Query $query)  {
                                                        return $query->where(['Regions.status' => 1]);
                                                    },
                   'Areas' => function (\Cake\ORM\Query $query)  {
                                                        return $query->where(['Areas.status' => 1]);
                                                    }                                 



            ])->group(['CategoryDeliverySchdules.region_id','CategoryDeliverySchdules.area_id'])->toArray();







             if(isset($categoryDeliverySchdule)&&count($categoryDeliverySchdule)){
                           
                       
                       foreach ($categoryDeliverySchdule as $key => $value) {
                              $temp = array();
                              
                            if( 
                               ( isset($value['Regions']) && count($value['Regions']) > 0 ) 
                               &&
                               ( isset($value['Areas']) && count($value['Areas']) > 0 ) 
                               ){

                                  $temp['area_region_id'] = $value['Regions']['id'].'_'.$value['Areas']['id'];
                                  $temp['area_region_name'] = $value['Regions']['name'].'_'.$value['Areas']['name'];
                                  $regionAreaFinal[] = $temp;
                          }
                       }    
                    } 
                 }

           if(isset($regionAreaFinal)&&count($regionAreaFinal)>0){
            $response['statuscode'] = 200;
            $response['data'] = $regionAreaFinal;
           }else{
            $response['statuscode'] = 201;
           }
         echo json_encode($response);die;             
  }



  public function order($id=NULL){

      if(isset($id)){

               
                $now   = time();
                $route_id = base64_decode($id);
                $userSubTable = TableRegistry::get('UserSubscriptions');
                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                $userSubscriptions       = $userSubTable->find('all')->contain(['SubscriptionTypes'])->where(['UserSubscriptions.users_subscription_status_id'=> 1])->hydrate(false)->toArray();
                $userSubq = array();
                foreach ($userSubscriptions as $key => $value) {
                  if ($value['subscription_type']['subscription_type_name'] == 'alternate') {
                    $startdate       = $value['startdate']->i18nFormat('YYY-MM-dd');
                    $subscriptionday = strtotime($startdate);
                    $now             = $now;
                    $datediff        = $now - $subscriptionday;
                    $days            = floor($datediff / (60 * 60 * 24)) + 1 ;
                    if ($days < 1) {
                        $userSubq[] = $value['user_id'];
                    } 
                    if ($days % 2 != 0) {
                        $userSubq[] = $value['user_id'];
                    }

                }else{
                  $userSubq[] = $value['user_id'];
                }
                  
                } 
                $userSubq = array_unique($userSubq);
                $routeCustomer = $routeCustomerTable->find('all')->contain([
                  'Users',
                  'Regions',
                  'Areas'
                  ])->where([
                  'RouteCustomers.route_id'=>$route_id,
                  'RouteCustomers.user_id IN'=> $userSubq,'Users.status'=> 1,'Users.is_deleted'=> 0
                  ])->group([
                   'RouteCustomers.user_id'
                  ])->order(['RouteCustomers.position'=>'ASC'])->hydrate(false)->toArray();
                 $this->set('routeCustomer',$routeCustomer); 

                 

      }
      else if($this->request->is('ajax')){
        $data = $_POST;
        if(count($data)>0){
                   $routeCustomerTable = TableRegistry::get('RouteCustomers');
                    foreach ($data['user'] as $key => $value) {
                      $query = $routeCustomerTable->query();
                      $result = $query->update()
                        ->set(['position' => $key+1])
                        ->where(['user_id' => $value])
                        ->execute();

                    }
            echo "Success";die;         
               
        }else{
          echo "Error";die;
        } 
      }
      else{
        die('Something Went Wrong');
      }  
  }  

}
