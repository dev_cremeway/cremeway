<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use App\Controller\ExperttextingController;
use Twilio\Rest\Client;


 
class PrivacyPolicyController extends AppController
{

     public function initialize()
        {
            parent::initialize();
            $this->Auth->allow(['index']);
        }

       /*---Start code for multilanguage from 1000-----*/

       /*--status code reserve for sendOtp function Start from 1000 to 1020---*/
       
       public function index()
       {
        $staticPagesTable = TableRegistry::get('StaticPages');
        $data = $staticPagesTable->find('all')->where(['id'=> 2])->toArray();        
        $this->set('data', $data);
       }
 
}