<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DriverVehicles Controller
 *
 * @property \App\Model\Table\DriverVehiclesTable $DriverVehicles
 *
 * @method \App\Model\Entity\DriverVehicle[] paginate($object = null, array $settings = [])
 */
class DriverVehiclesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Vehicles', 'Users']
        ];
        $driverVehicles = $this->paginate($this->DriverVehicles);

        $this->set(compact('driverVehicles'));
        $this->set('_serialize', ['driverVehicles']);
    }

    /**
     * View method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $driverVehicle = $this->DriverVehicles->get($id, [
            'contain' => ['Vehicles', 'Users']
        ]);

        $this->set('driverVehicle', $driverVehicle);
        $this->set('_serialize', ['driverVehicle']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driverVehicle = $this->DriverVehicles->newEntity();
        if ($this->request->is('post')) {
            $driverVehicle = $this->DriverVehicles->patchEntity($driverVehicle, $this->request->getData());
            if ($this->DriverVehicles->save($driverVehicle)) {
                $this->Flash->success(__('The driver vehicle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver vehicle could not be saved. Please, try again.'));
        }
        $vehicles = $this->DriverVehicles->Vehicles->find('list', ['limit' => 200]);
        $users = $this->DriverVehicles->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverVehicle', 'vehicles', 'users'));
        $this->set('_serialize', ['driverVehicle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $driverVehicle = $this->DriverVehicles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $driverVehicle = $this->DriverVehicles->patchEntity($driverVehicle, $this->request->getData());
            if ($this->DriverVehicles->save($driverVehicle)) {
                $this->Flash->success(__('The driver vehicle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver vehicle could not be saved. Please, try again.'));
        }
        $vehicles = $this->DriverVehicles->Vehicles->find('list', ['limit' => 200]);
        $users = $this->DriverVehicles->Users->find('list', ['limit' => 200]);
        $this->set(compact('driverVehicle', 'vehicles', 'users'));
        $this->set('_serialize', ['driverVehicle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driverVehicle = $this->DriverVehicles->get($id);
        if ($this->DriverVehicles->delete($driverVehicle)) {
            $this->Flash->success(__('The driver vehicle has been deleted.'));
        } else {
            $this->Flash->error(__('The driver vehicle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
