<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * Areas Controller
 *
 * @property \App\Model\Table\AreasTable $Areas
 *
 * @method \App\Model\Entity\Area[] paginate($object = null, array $settings = [])
 */
class AreasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        
        // $this->Auth->allow();
    }
        
    public function index()
    {
        $this->paginate = [
            'contain' => ['Regions']
        ];
        $areas = $this->paginate($this->Areas);

        $this->set(compact('areas'));
        $this->set('_serialize', ['areas']);
    }
    /**
     * View method
     *
     * @param string|null $id Area id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $area = $this->Areas->get($id, [
            'contain' => ['Regions', 'AreaRegionNotifications', 'Sales', 'Users']
        ]);

        $this->set('area', $area);
        $this->set('_serialize', ['area']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    private function getAllRegionsList(){

        $regions = $this->Areas->Regions->find('list')->where(['status'=>1])->hydrate(false)->toArray();
        return $regions;
    }
    private function getAllAreas(){
               
        $areaTable = TableRegistry::get('Areas');
        $areaList = $areaTable->find('all')
                     ->contain(['Regions'])
                     ->hydrate(false)
                     ->toArray();
        return $areaList;
    }
    private function getArea( $name, $region_id ){
        $areaTable = TableRegistry::get('Areas');
        $areaList = $areaTable->find('all')
                         ->where(['name'=>trim($name)])
                         ->andwhere(['region_id'=>$region_id])
                         ->hydrate(false)
                         ->toArray();
        if(count($areaList)>0){
            return true;
        }
        return false;

    }
    private function validateAddareas($data){
           
            $error = array();
                  $data['name'] = trim($data['name']);
                  if( !    isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the Area name';
                  }else if( $this->getArea( trim( $data['name'] ), $data['region_id'] ) ){
                    $error['name'] = 'Area name already taken under this region';
                  }else if( empty($data['region_id']) ){
                      $error['name'] = 'Please select Region name';
                  }if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
       return $error;

    }
    public function add()
    {


      if(isset($_GET['query']))
            {
                    $condition = ['Areas.status'=>1];
                    $filterby = '';
                    $page = 0;
                    if(isset($_GET['page']) && ! empty($_GET['page'])){
                    $page = $_GET['page'];
                     }
                   if(isset($_GET['query'])){
  
                               $condition[] = [

                                  'OR' => [ 

                                     'Areas.name LIKE'=>'%'.$_GET['query'].'%'
                                   ]
                                ];
                               $this->paginate = [
                                              'limit' => 8,
                                              'page'=> $page,
                                              'contain' => ['Regions'],
                                              'order' => [
                                                  'Areas.id' => 'desc'
                                              ],
                                              'conditions'=>$condition
                                          ];
                               }
                         $areas = $this->paginate($this->Areas);
                         $this->set(compact('areas'));      
                          if(isset($_GET['query']) && !empty($_GET['query'])){
                          $this->set('querystring',$_GET['query']);
                        }


            }else if($_GET && ! isset($_GET['query'])){
                          
                    $condition = ['Areas.status'=>1];
                    $filterby = '';
                    $page = 0;
                    if(isset($_GET['filterby']) && ! empty($_GET['filterby'])){
                        $filterby = $_GET['filterby'];
                         $condition = ['region_id'=>$_GET['filterby']];
                      }

                     if(isset($_GET['page']) && ! empty($_GET['page'])){
                        $page = $_GET['page'];
                      } 
                    $this->paginate = [
                    'limit' => 8,
                    'page'=> $page,
                    'contain' => ['Regions'],
                    'order' => [
                        'Regions.id' => 'desc'
                    ],
                    'conditions'=>$condition
                ];
                $areas = $this->paginate($this->Areas)->toArray();
                $this->set('areas', $areas);
                $this->set('filterby',$filterby);
                $this->set('regions',$this->getAllRegionsList()); 


            }else if ($this->request->is('post')) {
                        $error = $this->validateAddareas($this->request->getData());
                        if($error['statuscode'] == 200)
                        {
                                $area = $this->Areas->newEntity();
                                $this->request->data['name'] = trim($this->request->getData('name'));
                                $this->request->data['manager_phoneNo'] = trim($this->request->getData('manager_phoneNo'));
                                $this->request->data['manager_name'] = trim($this->request->getData('manager_name'));
                                $this->request->data['manager_email'] = trim($this->request->getData('manager_email'));
                                $area = $this->Areas->patchEntity($area, $this->request->getData());
                                if ($this->Areas->save($area)) {

                                    $this->Flash->success(__('Area has been added successfully')); 
                                    return $this->redirect(['action' => 'add']);
                                }
                        }else{
                             $this->set('error',$error);
                             $this->set('data',$this->request->getData());
                        } 
             
        }else{
                $this->set('regions',$this->getAllRegionsList());
                $this->paginate = [
                    'contain' => ['Regions'],
                    'limit'=>10,
                    'conditions'=>['Areas.status'=>1],
                    'order'=>['Areas.id DESC']
                ];
                $areas = $this->paginate($this->Areas);
                $this->set(compact('areas'));
        }
       
         
    }

    /**
     * Edit method
     *
     * @param string|null $id Area id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

 private function getAreaNotThis($name, $regionId, $ID){
 
             $untTable = TableRegistry::get('Areas');
             $singleUnit = $untTable->find()
                                    ->where(['name'=>trim($name)])
                                    ->andwhere(['region_id'=>$regionId])
                                    ->andwhere(['id <>'=>$ID])
                                    ->count();
                                    
             return $singleUnit;
             

 }

    
    private function validateEditArea( $data ){
      
      $error = array();
          $data['name'] = trim($data['name']);
          if( !    isset( $data['name'] ) || empty( $data['name'] ) ) {
             $error['name'] = 'Please enter the Area name';
          }else if( $this->getAreaNotThis( trim( $data['name'] ), $data['region_id'], $data['id'] ) ){
            $error['name'] = 'Area name already taken under this region';
          }else if( empty($data['region_id']) ){
              $error['name'] = 'Please select Region name';
          }if( count( $error ) > 0 ){
            $error['statuscode'] = 201;
          }else{
            $error['statuscode'] = 200;
          }
    return $error;               
    } 

    public function edit($id = null)
    {
            
              $error = array();
                
                if ($this->request->is(['post'])) { 
 
                    $error = $this->validateEditArea($this->request->getData());
                    
                    if($error['statuscode'] == 200)
                        { 
                   
                    $areaTable = TableRegistry::get('Areas');
                    $area = $areaTable->get($this->request->getData('id'));
                    $area->name = trim($this->request->getData('name'));
                    $area->status = $this->request->getData('status');
                    $area->region_id = $this->request->getData('region_id');
                    $area->manager_name = $this->request->getData('manager_name');
                    $area->manager_email = $this->request->getData('manager_email');
                    $area->manager_phoneNo = $this->request->getData('manager_phoneNo'); 

                    if ($areaTable->save($area)) { 
                        $this->Flash->success(__('The area has been updated.'));
                        return $this->redirect(['action' => 'add']);
                    }
                    $this->Flash->error(__('The area could not be saved. Please, try again.'));
                    }else{

                             $this->set('error',$error);
                             $this->set('data',$this->request->getData());
                             $this->set('regions',$this->getAllRegionsList());
                    }
                }else{
                 
                $this->set('regions',$this->getAllRegionsList());    
                $id = base64_decode($id);
                $Unit = $this->Areas->get($id, [
                    'contain' => []
                ]);
                   
                $this->set('data',$Unit);
      }
                
    }
    /**
     * Delete method
     *
     * @param string|null $id Area id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $area = $this->Areas->get(base64_decode($id));
        $area->status = 0;
        if ($this->Areas->save($area)) {
            $this->Flash->success(__('The area has been deleted.'));
        } else {
            $this->Flash->error(__('The area could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add']);
    }
}
