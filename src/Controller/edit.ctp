 
<?php /*echo $this->Html->css('sol');
      echo $this->Html->script('sol');*/
 ?> 
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
  
</style>

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Route
              </h1>
             
              
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       



          <div class="box box-info">
              <div class="box-header">

               <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Routes/lists" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>  
                 
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-8'>
                   <?php print_r($user); ?>
                   <form id="addProductForm" method="post" action="">


                   <div class="form-group" id="err_div"></div>
                   <div class="form-group" id="err_div_green" style="color:green;"></div> 
                      
                           <div class="form-group required">



                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>
                            
                                <label for="exampleInputEmail1">Route Name</label>
                                
                                <input type="text" class="form-control" id="" name="name" value="<?php if(isset($driver_routes_name[0]['route']['name'])){ echo $driver_routes_name[0]['route']['name']; } ?>">
                                
                              </div> 

                              <div class="form-group required">
 
                                 <label for="exampleInputEmail1">Assign To Driver</label>
                                
                               <select class="form-control" id="getcategory"  name="driver_id">
                                
                                <option selected="selected" value="<?php echo $driver_routes_name[0]['user']['id'] ?>"><?php echo $driver_routes_name[0]['user']['name'] ?></option>
                              
                              </select>
                                
                              </div>


 <input type="hidden" name="route_id" value="<?php echo $id; ?>">



       <div class="form-group required">
 
                                 <label for="exampleInputEmail1">Select Timing</label>
                                
                                 <select id="deliverytimingselection" class="form-control" name="deliverytiming">

                                 <option value="<?php echo $categoryDeliverySchdule['id'] ?>" selected><?php echo $categoryDeliverySchdule['name'] ?></option>
                                   
                                 </select>
                                
                              </div>




                         
                         <div class="col-sm-6 checkboxes" id="show-after-response"> 
                        

                         <?php if( isset( $all_added_unadded ) && count( $all_added_unadded ) > 0 ) {
                          ?>
                          <label for="exampleInputEmail1">Below Places not added any Route for this timing.</label>
                          
                          <?php foreach ($all_added_unadded as $key => $value) {
                           ?>

                           <li class="checkbox"><label><input type="checkbox" <?php if( $value['added'] == 1 ) {
                            ?>
                            checked
                            <?php
                           } ?> value="<?php echo $value['area_region_id'] ?>" name="region_area[]"><?php echo $value['area_region_name'] ?></label></li>



                         <?php
                        }
                      }else{
                        ?>
                        <p style="color:red;">All Region area already added to route in this Timing </p>
                        <?php
                        } ?>          
                                
                                   
                                
                       </div>
   







        
                          


                   


                

                   


                  </div>
                </div> 
                 <input type="button" id="submit" class="btn btn-primary" value="Submit"> 
                </form> 
                
                <div id='loadingmessage' style="display:none;">
                <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
               </div>


            </div>  
          </div> 
        </div> 
      </div> 
    </section>








</div>





<script type="text/javascript"> 
 
$("#submit").click(function(){
    $('#loadingmessage').show();
    $.ajax({
         type: 'POST',
         url:  "edit",
         data: $('#addProductForm').serialize(), 
         success: function(response) {
            var isError = JSON.parse(response);
            if(isError.statuscode == 200){
                $('#loadingmessage').hide();  
                $("#err_div").show();
                $("#err_div").text(isError.name);
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                setTimeout(function(){
                   window.location.reload();
                },3000);
                

            }else{
                $('#loadingmessage').hide(); 
              $("#err_div").show();
              $("#err_div").text(isError.name);
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            }
         },
        error: function() {
             
        }
     });
});

</script>





