<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Controller\Component\ImageresizeComponent;
use Cake\Core\Configure;
/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[] paginate($object = null, array $settings = [])
 */
class GalleryController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        //$this->Auth->allow(['index','view','uploadImage','delete']);
        $this->loadComponent('Imageresize');
    }
    public function index()
    {   
      $this->paginate = [
                            'limit' => 20,
                            'order' => [
                                'Gallery.id' => 'desc'
                            ] 
                        ];
        $galleries = $this->paginate($this->Gallery);

        $this->set(compact('galleries'));
        $this->set('_serialize', ['galleries']);
    }

    /* upload image */
    function getExtension($str) {
      $i = strrpos($str,".");
      if (!$i) { return ""; } 
      $l = strlen($str) - $i;
      $ext = substr($str,$i+1,$l);
      return $ext;
    }
    public function uploadImage( $image=null, $path=null ){
            $error = 0;
            if (isset($image["image"])) {
            $tmpFile   = $image["image"]["tmp_name"];
            $file_name = substr($image['image']['name'], 0, strpos($image['image']['name'], "."));
            $extension = $this->getExtension($image['image']['name']);
            $tme = time();
            $file_name = str_replace(' ', '-', $file_name);
            $fileName = $path.$file_name.$tme.'.'.$extension;

            $extensionArray = array('jpg','jpeg','png','PNG');
            
            list($width, $height) = getimagesize($tmpFile);
             
            if ($width == null && $height == null) {
                $error = 1;
                
            }             
            if ( ! in_array($extension, $extensionArray) ) {                
                $error = 1; 
            }
            else {

                if( move_uploaded_file($tmpFile, $fileName) ){
                    $error = $file_name.$tme.'.'.$extension; 
                }
            }
        }
        return $error;
    } 

    /* upload image */

    public function view($id = null)
    {
      $id = base64_decode($id);            
      $galleryTable = TableRegistry::get('GalleryImages');
      $gallaryTable = TableRegistry::get('Gallery');         
      $galleries    = $gallaryTable->find()->where(['id'=> $id])->hydrate(false)->first();
      $this->set('mg',$galleries);
      $structure  = $galleries['directory'];
      $structure1 = $galleries['directory'];
      if ($this->request->is('post')) {
        $data = $this->request->getData();         
        $imagename = '';
        $query     = $gallaryTable->query();
        if($data['title']){
          $result1    = $query->update()
            ->set(['title' => $data['title'], 'description' => $data['description']])
            ->where(['id' => $id])
            ->execute();            
        
        if($_FILES ){
          foreach ($_FILES['image']['error'] as $key => $value) {
            /*echo "test<br>";*/           
            $temp = array();
            if($value == 0){
              $temp['image']['name']       =  $_FILES['image']['name'][$key];
              $temp['image']['type']       =  $_FILES['image']['type'][$key];
              $temp['image']['tmp_name']   =  $_FILES['image']['tmp_name'][$key];
              $temp['image']['error']      =  $_FILES['image']['name'][$key];
              $temp['image']['size']       =  $_FILES['image']['name'][$key];
              $imagename = $this->uploadImage($temp,$structure);
              if($imagename != 1){
                $components = array('Imageresize');/*
                $components   = new ImageresizeComponent;*/
                $this->Imageresize->load($structure.$imagename);
                $this->Imageresize->resize(640,1100);
                $this->Imageresize->save($structure.'resized/'.$imagename);
                $this->request->data['image'] = $imagename; 
                $image = $galleryTable->newEntity();
                $image->gallery_id  = $data['gallery_id'];
                $image->thumbnail   = $structure.'resized/'.$imagename;
                $image->image       = $structure.$imagename;
                $result = $galleryTable->save($image);
                
              }
            }
            
          } 
          $this->Flash->success(__('Gallery has been updated successfully.'));
        }else{
          $this->request->data['image'] = NULL;
          $this->Flash->success(__('Error has been occurred'));
        }
          }else{
            $this->Flash->success(__('Please enter a title.'));
          }
      //  $product->image = $imagename; 
      }
      $galleries = $galleryTable->find('all')->where(['GalleryImages.gallery_id'=> $id])->contain(['Gallery']);
      $this->paginate = [
                            'limit' => 20,
                            'order'=>['GalleryImages.position ASC'],
                                     
                        ];                 
      $galleries = $this->paginate($galleries)->toArray();
      $this->set('galleries', $galleries);   
    }

    public function delete($id = null)
    { 
      if($this->request->is('ajax')){
        $data = $this->request->getData();
        $id   = $data['id'];
        $galleryTable = TableRegistry::get('GalleryImages');       
        $image        = $galleryTable->get($id);

        $galleries = $galleryTable->find()->where(['GalleryImages.gallery_id'=> $image->gallery_id])->contain(['Gallery'])->toArray();
        if(count($galleries) > 1){
          $filename = IMG_PATHQ.$image->thumbnail;
          if (file_exists($filename)) {
            unlink(IMG_PATHQ.$image->thumbnail);
          }
          $filename1 = IMG_PATHQ.$image->thumbnail;        
          $filename1 = str_replace("/resized","",$filename1); 
          if (file_exists($filename1)) {
            unlink($filename1);
          }
          $galleryTable->delete($image);
          echo "deleted"; die;/*
          $this->Flash->success(__('Gallery images have been deleted.'));
         
          return $this->redirect( $this->referer() );*/
        }else{
          echo "not deleted"; die;/*
          $this->Flash->success(__('All images can not be deleted.'));
         
          return $this->redirect( $this->referer() );*/
        }
      }
            
    }
    public function deletegalleryimages($id = null)
    { 
      $galleryTable = TableRegistry::get('GalleryImages');       
      $image        = $galleryTable->get($id);

      $galleries = $galleryTable->find()->where(['GalleryImages.gallery_id'=> $image->gallery_id])->contain(['Gallery'])->toArray();
        $filename = IMG_PATH.$image->thumbnail;
        if (file_exists($filename)) {
          unlink(IMG_PATH.$image->thumbnail);
        }
        $filename1 = IMG_PATH."resized/".$image->thumbnail;
        if (file_exists($filename1)) {
          unlink(IMG_PATH."resized/".$image->thumbnail);
        }
        if($galleryTable->delete($image)){
          return true;
        }else{
          return false;
        }            
    }
    public static function deleteDir($dirPath) {
      if (! is_dir($dirPath)) {
          throw new InvalidArgumentException('$dirPath must be a directory');
      }
      if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
          $dirPath .= '/';
      }
      $files = glob($dirPath . '*', GLOB_MARK);
      foreach ($files as $file) {
          if (is_dir($file)) {
              self::deleteDir($file);
          } else {
              unlink($file);
          }
      }
      rmdir($dirPath);
    }
    public function deletegallery($id = null)
    {
      $id = base64_decode($id);
      $galleryTable = TableRegistry::get('Gallery'); 
      $gallaryTable = TableRegistry::get('GalleryImages'); 
      $galleries    = $galleryTable->find()->where(['id'=> $id])->first();
      $path = $galleries['directory'];
      $this->deleteDir($path);   
      $image        = $galleryTable->get($id);
      $galleryTable->delete($image);
      $this->Flash->success(__('The gallery has been deleted.'));
     
      return $this->redirect( $this->referer() );      
    }
    public function add()
    {
      if ($this->request->is('post')) {
        $gallaryTable = TableRegistry::get('Gallery');
        $galleryTable = TableRegistry::get('GalleryImages');
        $data = $this->request->getData();         
        $imagename = '';
        $structure = GALLERY_PATH.str_replace(' ', '_', strtolower($data['title']))."/";
        $structure1 = GALLERY_PATH.str_replace(' ', '_', strtolower($data['title'])).'/resized/';      
        if (!mkdir($structure, 0777, true) || !mkdir($structure1, 0777, true)) {
          $this->Flash->success(__('Unable to create directory for gallery.'));
        }
        $gallary = $gallaryTable->newEntity();
        $gallary->title         = $data['title'];
        $gallary->description   = $data['description'];
        $gallary->directory      = $structure;
        $i = 0;
        if($result1 = $gallaryTable->save($gallary)){
          if($_FILES ){
            foreach ($_FILES['image']['error'] as $key => $value) {
              /*echo "test<br>";*/           
              $temp = array();
              if($value == 0){
                $temp['image']['name']       =  $_FILES['image']['name'][$key];
                $temp['image']['type']       =  $_FILES['image']['type'][$key];
                $temp['image']['tmp_name']   =  $_FILES['image']['tmp_name'][$key];
                $temp['image']['error']      =  $_FILES['image']['name'][$key];
                $temp['image']['size']       =  $_FILES['image']['name'][$key];
                $imagename = $this->uploadImage($temp,$structure);
                if($imagename != 1){
                $components = array('Imageresize');
                  $this->Imageresize->load($structure.$imagename);
                  $this->Imageresize->resize(640,1100);
                  $this->Imageresize->save($structure.'resized/'.$imagename);            
                  $this->request->data['image'] = $imagename; 
                  $image = $galleryTable->newEntity();
                  $image->gallery_id    = $result1->id;
                  $image->thumbnail     = $structure.'resized/'.$imagename;
                  $image->image         = $structure.$imagename;
                  $result = $galleryTable->save($image);                
                }
              }
              $i++;
            } 
            $this->Flash->success(__('Gallery has been created successfully.'));
            return $this->redirect(['action' => 'view', base64_encode($result1->id)]);
          }else{
            $this->request->data['image'] = NULL;
            $this->Flash->success(__('Error has been occurred'));
          }
        }
      }         
    }
    public function order(){
      if($this->request->is('ajax')){
        $data = $_POST;
        if(count($data)>0){
                   $galleryTable = TableRegistry::get('GalleryImages');
                    foreach ($data['gallery'] as $key => $value) {
                      $query = $galleryTable->query();
                      $result = $query->update()
                        ->set(['position' => $key+1])
                        ->where(['id' => $value])
                        ->execute();

                    }
            echo "Success";die;         
               
        }else{
          echo "Error";die;
        } 
      }
    }  
    public function validatetitle(){
      if($this->request->is('ajax')){
        $title = $_POST['title'];
        $gallaryTable = TableRegistry::get('Gallery');         
        $galleries    = $gallaryTable->find()->where(['title'=> $title])->hydrate(false)->toArray();
        if(count($galleries) > 0){
          echo "found";die;  
        }else{
          echo "not found";die;
        } 
      }
    }
    public function validateedittitle(){
      if($this->request->is('ajax')){
        $title = $_POST['title'];
        $id = $_POST['id'];
        $gallaryTable = TableRegistry::get('Gallery');         
        $galleries    = $gallaryTable->find()->where(['title'=> $title, 'id !='=> $id])->hydrate(false)->toArray();
        if(count($galleries) > 0){
          echo "found";die;  
        }else{
          echo "not found";die;
        } 
      }
    }
}
