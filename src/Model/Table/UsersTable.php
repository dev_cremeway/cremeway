<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Devices
 * @property \Cake\ORM\Association\BelongsTo $Regions
 * @property \Cake\ORM\Association\BelongsTo $Areas
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $UserTypes
 * @property \Cake\ORM\Association\HasMany $Invoices
 * @property \Cake\ORM\Association\HasMany $ComplaintFeedbackSuggestions
 * @property \Cake\ORM\Association\HasMany $CustomOrders
 * @property \Cake\ORM\Association\HasMany $OrderTrackings
 * @property \Cake\ORM\Association\HasMany $Sales
 * @property \Cake\ORM\Association\HasMany $Transactions
 * @property \Cake\ORM\Association\HasMany $UserBalances
 * @property \Cake\ORM\Association\HasMany $UserContainers
 * @property \Cake\ORM\Association\HasMany $UserNotifications
 * @property \Cake\ORM\Association\HasMany $UserSubscriptions
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Acl.Acl', ['type' => 'requester']);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id', /*,
        'joinType' => 'INNER'*/
        ]);
        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id', /*,
        'joinType' => 'INNER'*/
        ]);
       /* $this->belongsTo('UserBalances', [
            'foreignKey' => 'user_id',
        ]);*/
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            //'joinType'   => 'INNER',
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            //'joinType'   => 'INNER',
        ]);
        $this->belongsTo('UserTypes', [
            'foreignKey' => 'user_type_id', /*,
        'joinType' => 'INNER'*/
        ]);
        $this->hasMany('Invoices', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('ComplaintFeedbackSuggestions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('CustomOrders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('OrderTrackings', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('RouteCustomers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Sales', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('UserBalances', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('UserContainers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserNotifications', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('UserSubscriptions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('SubscribedContainers', [
            'foreignKey' => 'user_id',
        ]);
    }

    /* public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity,
    \ArrayObject $options)
    {
    //$hasher = new DefaultPasswordHasher;
    $entity->password = md5($entity->password);
    return true;
    }*/

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('phoneNo', 'create')
            ->notEmpty('phoneNo');

        /*$validator
        ->requirePresence('latitude', 'create')
        ->notEmpty('latitude');

        $validator
        ->requirePresence('longitude', 'create')
        ->notEmpty('longitude');*/

        $validator
            ->requirePresence('houseNo', 'create')
            ->notEmpty('houseNo');

        /* $validator
        ->requirePresence('username', 'create')
        ->notEmpty('username')
        ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);*/

        /*  $validator
        ->requirePresence('password', 'create')
        ->notEmpty('password');
         */
        /*$validator
        ->requirePresence('image', 'create')
        ->notEmpty('image');
         */
        /*  $validator
        ->requirePresence('token', 'create')
        ->notEmpty('token');*/

        /*  $validator
        ->integer('status')
        ->requirePresence('status', 'create')
        ->notEmpty('status');*/

        /*$validator
        ->allowEmpty('otp');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        // $rules->add($rules->existsIn(['device_id'], 'Devices'));
        $rules->add($rules->existsIn(['region_id'], 'Regions'));
        $rules->add($rules->existsIn(['area_id'], 'Areas'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));
        //$rules->add($rules->existsIn(['user_type_id'], 'UserTypes'));

        return $rules;
    }
}
