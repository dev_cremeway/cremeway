<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSubscriptions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SubscriptionTypes
 * @property \Cake\ORM\Association\BelongsTo $Products
 * @property \Cake\ORM\Association\BelongsTo $UsersSubscriptionStatuses
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Units
 *
 * @method \App\Model\Entity\UserSubscription get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserSubscription newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserSubscription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserSubscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserSubscription findOrCreate($search, callable $callback = null, $options = [])
 */
class UserSubscriptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setTable('user_subscriptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SubscriptionTypes', [
            'foreignKey' => 'subscription_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UsersSubscriptionStatuses', [
            'foreignKey' => 'users_subscription_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Units', [
            'foreignKey' => 'unit_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('days', 'create')
            ->notEmpty('days');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        $validator
            ->decimal('subscriptions_total_amount')
            ->requirePresence('subscriptions_total_amount', 'create')
            ->notEmpty('subscriptions_total_amount');

        $validator
            ->dateTime('startdate')
            ->requirePresence('startdate', 'create')
            ->notEmpty('startdate');

        $validator
            ->dateTime('enddate')
            ->requirePresence('enddate', 'create')
            ->notEmpty('enddate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['subscription_type_id'], 'SubscriptionTypes'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['users_subscription_status_id'], 'UsersSubscriptionStatuses'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['unit_id'], 'Units'));

        return $rules;
    }
}
