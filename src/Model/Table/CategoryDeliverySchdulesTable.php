<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliverySchdules Model
 *
 * @property \Cake\ORM\Association\HasMany $Sales
 *
 * @method \App\Model\Entity\DeliverySchdule get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliverySchdule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliverySchdule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoryDeliverySchdulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setTable('category_delivery_schdules');
        //$this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('DeliverySchdules', [
            'foreignKey' => 'delivery_schdule_id'
        ]);
        
          $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);
          $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
        ]);
          $this->belongsTo('Areas', [
            'foreignKey' => 'area_id'
        ]);
        
    }
 
}
