<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliverySchdules Model
 *
 * @property \Cake\ORM\Association\HasMany $Sales
 *
 * @method \App\Model\Entity\DeliverySchdule get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliverySchdule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliverySchdule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliverySchdule findOrCreate($search, callable $callback = null, $options = [])
 */
class DeliverySchdulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setTable('delivery_schdules');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Sales', [
            'foreignKey' => 'delivery_schdule_id'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->time('start_time')
            ->requirePresence('start_time', 'create')
            ->notEmpty('start_time');

        $validator
            ->time('end_time')
            ->requirePresence('end_time', 'create')
            ->notEmpty('end_time');

        return $validator;
    }
}
