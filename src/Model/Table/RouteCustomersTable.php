<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Units
 * @property \Cake\ORM\Association\HasMany $ComplaintFeedbackSuggestions
 * @property \Cake\ORM\Association\HasMany $CustomOrders
 * @property \Cake\ORM\Association\HasMany $ProductChildren
 * @property \Cake\ORM\Association\HasMany $Sales
 * @property \Cake\ORM\Association\HasMany $UserSubscriptions
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RouteCustomersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setTable('route_customers');
        
        $this->setPrimaryKey('id');

        
       /* $this->hasMany('UserSubscriptions', [
            'foreignKey' => 'user_id'
            
        ]);
        $this->hasMany('CustomOrders', [
            'foreignKey' => 'user_id'
        ]);*/

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
            
        ]);
         $this->belongsTo('Regions', [
            'foreignKey' => 'region_id'
            
        ]);
          $this->belongsTo('Areas', [
            'foreignKey' => 'area_id'
            
        ]);
           $this->belongsTo('Routes', [
            'foreignKey' => 'route_id'
            
        ]);
         $this->belongsTo('DeliverySchdules', [
            'foreignKey' => 'delivery_schdule_id'
            
        ]);
        
        

    }

   
}
