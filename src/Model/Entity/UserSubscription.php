<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSubscription Entity
 *
 * @property int $id
 * @property int $subscription_type_id
 * @property string $days
 * @property int $product_id
 * @property int $quantity
 * @property int $users_subscription_status_id
 * @property int $user_id
 * @property int $unit_id
 * @property float $subscriptions_total_amount
 * @property \Cake\I18n\FrozenTime $startdate
 * @property \Cake\I18n\FrozenTime $enddate
 *
 * @property \App\Model\Entity\SubscriptionType $subscription_type
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\UsersSubscriptionStatus $users_subscription_status
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Unit $unit
 */
class UserSubscription extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    public function parentNode()
        {
            return null;
        }
}
