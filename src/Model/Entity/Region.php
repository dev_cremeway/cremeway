<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Region Entity
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\AreaRegionNotification[] $area_region_notifications
 * @property \App\Model\Entity\Area[] $areas
 * @property \App\Model\Entity\Sale[] $sales
 * @property \App\Model\Entity\User[] $users
 */
class Region extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    public function parentNode()
        {
            return null;
        }


        
        /*pr(
             ['Groups' => ['id' => 1]
              ,
             'Subs' => ['id' => 1]
             ]);die;*/
}
