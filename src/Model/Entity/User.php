<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $phoneNo
 * @property string $device_id
 * @property int $region_id
 * @property int $area_id
 * @property string $latitude
 * @property string $longitude
 * @property string $houseNo
 * @property string $username
 * @property string $password
 * @property int $group_id
 * @property string $image
 * @property string $token
 * @property int $status
 * @property string $otp
 * @property int $user_type_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Device $device
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Area $area
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\UserType $user_type
 * @property \App\Model\Entity\Invoice[] $invoices
 * @property \App\Model\Entity\ComplaintFeedbackSuggestion[] $complaint_feedback_suggestions
 * @property \App\Model\Entity\CustomOrder[] $custom_orders
 * @property \App\Model\Entity\OrderTracking[] $order_trackings
 * @property \App\Model\Entity\Sale[] $sales
 * @property \App\Model\Entity\Transaction[] $transactions
 * @property \App\Model\Entity\UserBalance[] $user_balances
 * @property \App\Model\Entity\UserContainer[] $user_containers
 * @property \App\Model\Entity\UserNotification[] $user_notifications
 * @property \App\Model\Entity\UserSubscription[] $user_subscriptions
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        // 'token'
    ];

    public function parentNode()
    {
        if (!$this->id) {
            return null;
        }
        if (isset($this->role_id)) {
            $roleId = $this->role_id;
        } else {
            $Users  = TableRegistry::get('Users');
            $user   = $Users->find('all', ['fields' => ['role_id']])->where(['id' => $this->id])->first();
            $roleId = $user->role_id;
        }
        if (!$roleId) {
            return null;
        }
        return ['Roles' => ['id' => $roleId]];
    }
}
