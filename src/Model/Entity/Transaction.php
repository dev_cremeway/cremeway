<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $id
 * @property int $user_id
 * @property float $debited
 * @property float $credited
 * @property string $onlinetransactionhistory
 * @property \Cake\I18n\FrozenTime $created
 * @property int $status
 * @property string $transaction_type
 * @property int $users_subscription_status_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\UsersSubscriptionStatus $users_subscription_status
 */
class Transaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    public function parentNode()
        {
            return null;
        }
}
