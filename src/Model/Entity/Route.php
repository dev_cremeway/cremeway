<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Route Entity
 *
 * @property int $id
 * @property string $name
 * @property int $region_id
 * @property int $area_id
 * @property \Cake\I18n\FrozenDate $created
 *
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Area $area
 * @property \App\Model\Entity\DriverRoute[] $driver_routes
 * @property \App\Model\Entity\RouteCustomer[] $route_customers
 */
class Route extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
