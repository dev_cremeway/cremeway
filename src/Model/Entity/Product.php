<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property float $price_per_unit
 * @property int $quantity
 * @property int $unit_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\ComplaintFeedbackSuggestion[] $complaint_feedback_suggestions
 * @property \App\Model\Entity\CustomOrder[] $custom_orders
 * @property \App\Model\Entity\ProductChild[] $product_children
 * @property \App\Model\Entity\Sale[] $sales
 * @property \App\Model\Entity\UserSubscription[] $user_subscriptions
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    public function parentNode()
        {
            return null;
        }
}
