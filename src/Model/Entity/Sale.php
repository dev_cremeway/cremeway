<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sale Entity
 *
 * @property int $id
 * @property int $delivery_schdule_id
 * @property \Cake\I18n\FrozenTime $date
 * @property int $product_id
 * @property float $product_price
 * @property int $region_id
 * @property int $area_id
 * @property int $user_id
 *
 * @property \App\Model\Entity\DeliverySchdule $delivery_schdule
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Area $area
 * @property \App\Model\Entity\User $user
 */
class Sale extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    public function parentNode()
        {
            return null;
        }
}
