<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

 
class DriverApiController extends AppController
{

	  public function initialize()
        {
            parent::initialize();
            $this->Auth->allow(['login','index','updateProfileDriver','logout','getAllRoute','getSchdule','getAllRouteCustomer','updateDelivery','startTracking','stopTracking','sendPushToCustomerFromDriver']);
        }

	         public function index()
			    {
			         echo "HI!!!!";die;
			    }
           private function validateLogin( $data ){

            	 $error = array();
            	 if( ! isset( $data['username'] ) || empty( $data['username'] )){
            	 	$error['messageText'] = "Please enter your username";
            	 	$error['messageCode'] = 201;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['password'] ) || empty( $data['password'] )){
            	 	$error['messageText'] = "Please enter your password";
            	 	$error['messageCode'] = 202;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['type_user'] ) || empty( $data['type_user'] )){
            	 	$error['messageText'] = "Please enter your role";
            	 	$error['messageCode'] = 203;
            	 	$error['successCode'] = 0; 
            	 }else if( $data['type_user'] != 'driver'){
            	 	$error['messageText'] = "Please enter your valid role";
            	 	$error['messageCode'] = 204;
            	 	$error['successCode'] = 0; 
            	 }else{
            	 	$error['messageCode'] = 200;
            	 }
              return $error;
            }

            private function updateToken( $userid ){
               
                 $userTable = TableRegistry::get('Users');
                 $user = $userTable->get($userid);

				 $user->token = \Cake\Utility\Text::uuid();
				 $user->modified = date('Y-m-d h:i:s');
				 if($userTable->save($user)){
				    return true;
				 } else {
				    return false;
				 }

            }
         
		    public function login()
		    {
			    	$response = array();
			    	if( $this->request->is('post') ){
                             
                             $data = $this->request->getData();
                             $error = $this->validateLogin($data);
                             if( $error['messageCode'] == 200 ){
                                
		                                $username = $data['username'];
		 
		                                $driverTable = TableRegistry::get('Users');
		                                $driverInfo = $driverTable
		                                                ->find()
													    ->select(['id','name'])
													    ->where(['username'=>$username])
													    ->andwhere(['password'=>md5($data['password'])])
													    ->toArray(); 
		                            
		                            if(count($driverInfo)>0)
		                            {    
			                                $updateToken = $this->updateToken($driverInfo[0]['id']);
			                                if($updateToken){

			                                	$driverInfo1 = $driverTable
			                                                ->find('all')
			                                                //->contain(['Regions','Areas'])
							                                ->where(['Users.id'=>$driverInfo[0]['id']])
														    ->toArray();

												//$driverInfo1[0]['houseNo'] = $driverInfo1[0]['houseNo'];/* .$driverInfo1[0]['area']['name'].' '.$driverInfo1[0]['region']['name'];*/
												//unset($driverInfo1[0]['area']);
												//unset($driverInfo1[0]['region']);		    

									            $error['messageText'] = "success";
							            	 	$error['messageCode'] = 200;
							            	 	$error['successCode'] = 1;
							            	 	$error['driverInfo'] = $driverInfo1;
							            	 	$currentDateTime = date('Y-m-d h:i:s');
												$newDateTime = date('h:i A', strtotime($currentDateTime));
												$error['loggedtime'] = date("H:i", strtotime($newDateTime));
														     
														    


			                                }else{

			                                	$error['messageText'] = "Please enter the valid login details.";
							            	 	$error['messageCode'] = 205;
							            	 	$error['successCode'] = 0; 

			                                }
	                                }else{
	                                	        $error['messageText'] = "Please enter the valid login details.";
							            	 	$error['messageCode'] = 205;
							            	 	$error['successCode'] = 0;
	                                } 

                             }
			         
			        }else{
			        $error['messageText'] = "Invalid Request";
            	 	$error['messageCode'] = 201;
            	 	$error['successCode'] = 0; 
			        }
			        $response = json_encode($error);
			        echo $response;die;
    	   }
         
          private function validateLogout( $data ){

          	     $error = array();
            	 if( ! isset( $data['id'] ) || empty( $data['id'] )){
            	 	$error['messageText'] = "User Id can not be empty";
            	 	$error['messageCode'] = 207;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['token'] ) || empty( $data['token'] )){
            	 	$error['messageText'] = "Token can not be empty";
            	 	$error['messageCode'] = 208;
            	 	$error['successCode'] = 0; 
            	 }else if( ! $this->notThisUser($data['id'],$data['token']) ){
            	 	$error['messageText'] = "Invlid user";
            	 	$error['messageCode'] = 209;
            	 	$error['successCode'] = 0; 
            	 }else{
            	 	$error['messageCode'] = 200;
            	 }
              return $error;



          }

           private function emptyToken( $userid ){
               
                 $userTable = TableRegistry::get('Users');
                 $user = $userTable->get($userid);

				 $user->token = '';
				 $user->modified = date('Y-m-d h:i:s');
				 if($userTable->save($user)){
				    return true;
				 } else {
				    return false;
				 }

            }

    	   public function logout(){
                
                $response = array();
		    	if( $this->request->is('post') ){
                        
                 $data = $this->request->getData();
                 $error = $this->validateLogout($data);
                 if( $error['messageCode'] == 200 ){


                    if( $this->emptyToken($data['id']) ){

                    	$error['messageText'] = "Logged out Successfully";
		        	 	$error['messageCode'] = 200;
		        	 	$error['successCode'] = 1; 

                    }

                 }

		    	}else{
		        $error['messageText'] = "Invalid Request";
        	 	$error['messageCode'] = 201;
        	 	$error['successCode'] = 0; 
		        }
		        $response = json_encode($error);
		        echo $response;die;

    	   }

    	  private function validateupdateProfileDriver( $data ){


    	  	   $error = array();
            	 if( ! isset( $data['user_id'] ) || empty( $data['user_id'] )){
            	 	$error['messageText'] = "User Id can not be empty";
            	 	$error['messageCode'] = 207;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['token'] ) || empty( $data['token'] )){
            	 	$error['messageText'] = "Token can not be empty";
            	 	$error['messageCode'] = 208;
            	 	$error['successCode'] = 0; 
            	 }else if( ! $this->notThisUser($data['user_id'],$data['token']) ){
            	 	$error['messageText'] = "Invlid user";
            	 	$error['messageCode'] = 209;
            	 	$error['successCode'] = 0; 
            	 }else{
            	 	$error['messageCode'] = 200;
            	 }
              return $error;



    	  } 
    	 private function validateName( $name ){
                  
                   $error = array();
                   if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$name)){
			            $error['messageText'] = "Invalid Name";
		        	 	$error['messageCode'] = 210;
		        	 	$error['successCode'] = 0;
			         }else{
			          $error['messageCode'] = 200 ;
			         }
			       return  $error; 
    	 } 
    	 private function validatPhone( $phone ){
               

                $error = array();
                   if( ! preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/', $phone) ){
			            $error['messageText'] = "Invalid Phone No.";
		        	 	$error['messageCode'] = 211;
		        	 	$error['successCode'] = 0;
			         }else{
			          $error['messageCode'] = 200 ;
			          }
			       return  $error; 


    	 }

    	  private function upload_image($content=null,$imgname=null,$dest=null,$extn=null){

		        if($content){
		                $dataarray=explode('base64,', $content);
		                if(count($dataarray)==1){
		                    $extn='jpg';
		                    
		                        if($extn){
		                            $img_name = $imgname."-".time();
		                            $data = $content;
		                            $data = str_replace(' ', '+', $data);
		                            $data = base64_decode($data);
		                            file_put_contents($dest.$img_name.".".$extn, $data);

		                            chmod($dest.$img_name.".".$extn, 0777);
		                            return $img_name.".".$extn;
		                        }else{
		                            return false;
		                        }
		                    
		                }else{
		                    return false;   
		                }
		        }else{
		            return false;
		        }
    } 

    private function validateImage($base64){
			
			$imgdata = base64_decode($base64);

			$f = finfo_open();

			$mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

			$mime_type = explode('/', $mime_type);
            if($mime_type[0] == 'image'){
    	 	  $response['messageCode'] = 200;
			}else{
               
                $response['messageText'] = "Invalid image";
	    	 	$response['messageCode'] = 212;
	    	 	$response['successCode'] = 0; 

            } 

  return $response;

    }


    	  public function updateProfileDriver(){

    	  	    $response = array();
    	  	    $imagename = '';
    	  	    if( $this->request->is('post') ){
                        
                 $data = $this->request->getData();
                 $response = $this->validateupdateProfileDriver($data);
                 
                 if( $response['messageCode'] == 200 ){

                         
                 	if( isset($data['name']) && ! empty( $data['name'] ) ){
                          
                          $response = $this->validateName($data['name']);

                          if($response['messageCode'] == 200){

                          	   if( isset($data['phone']) && ! empty( $data['phone'] ) ){

                          	   	$response = $this->validatPhone($data['phone']);

                          	   	if($response['messageCode'] == 200){

                                      
                                       if( isset($data['image']) && ! empty( $data['image'] ) ){


                                       	 
                                         
                                         $response = $this->validateImage($data['image']);
                                         
                                       	 
                                         if($response['messageCode'] == 200)
                                         {
                                       	 $imagename = $this->upload_image($data['image'],'driver','../webroot/img/images/');
                                       	 

                                          
                                          }

                                          
                                         
                                       }
                          	   	   }


                          	   }

                          }
                           
        	 	   
        	          }

                     
	                    
                  if($response['messageCode'] == 200)
                  {
	                     $userTable = TableRegistry::get('Users');
		                 $user = $userTable->get($data['user_id']);
                     
	                     if(isset($imagename) && ! empty($imagename)){
	                     $user->image = HTTP_ROOT.'/img/images/'.$imagename;	
	                     }
	                     if(isset($data['name']) && ! empty($data['name'])){
	                     $user->name = $data['name'];	
	                     } 
	                     if(isset($data['phone']) && ! empty($data['phone'])){
	                     $user->phoneNo = $data['phone'];	
	                     }
	                     if(isset($data['address']) && ! empty($data['address'])){
	                     $user->houseNo = $data['address'];	
	                     } 
						 $user->modified = date('Y-m-d h:i:s');
						 if($userTable->save($user)){

                            $driverTable = TableRegistry::get('Users');
						 	$driverInfo1 = $driverTable
	                            ->find('all')
	                           // ->contain(['Regions','Areas'])
	                            ->where(['Users.id'=>$data['user_id']])
							    ->toArray();



						 /*  $driverInfo1[0]['houseNo'] = $driverInfo1[0]['houseNo'] .$driverInfo1[0]['area']['name'].' '.$driverInfo1[0]['region']['name'];
												unset($driverInfo1[0]['area']);
												unset($driverInfo1[0]['region']);*/	    

						 	$response['messageText'] = "User Has beed updated Successfully";
			        	 	$response['messageCode'] = 206;
			        	 	$response['successCode'] = 1;
			        	 	$response['updatedInfo'] = $driverInfo1; 
						     
						 } else {
						    $response['messageText'] = "something went wrong";
			        	 	$response['messageCode'] = 201;
			        	 	$response['successCode'] = 0; 
						 }
			    }
             }

		    	}else{
		        $response['messageText'] = "Invalid Request";
        	 	$response['messageCode'] = 201;
        	 	$response['successCode'] = 0; 
		        }
		        echo json_encode($response);die;
		          

    	  }


    	   private function notThisUser( $id, $token ){
              

              $driverTable = TableRegistry::get('Users');
		                                $driverInfo = $driverTable
		                                                ->find()
													    ->select(['id','name'])
													    ->where(['id'=>$id])
													    ->andwhere(['token'=>$token])
													    ->toArray();
            if(count($driverInfo)>0){
            	return true;
            } return false;													     
                

          }

          public function getSchdule(){

		          	 $response = array();
		    	  	   if( $this->request->is('post') ){ 
		                    
		                    $data = $this->request->getData();
		                    $response = $this->validateupdateProfileDriver($data);

		                    if($response['messageCode'] == 200)
		                    {
			                    $driverRouteTable = TableRegistry::get('DeliverySchdules');
			                    $schdule = $driverRouteTable->find('all')->hydrate(false)->toArray();
			                   
	                            if(count($schdule)>0){
                                    
                                    foreach ($schdule as $key => $value) {
                                    
                                    $newDateTime = date('h:i A', strtotime($value['start_time']));
									$schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

									$newDateTime = date('h:i A', strtotime($value['end_time']));
									$schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));

                                    }

	                            	$response['messageText'] = "success";
				            	 	$response['messageCode'] = 200;
				            	 	$response['successCode'] = 1;
				            	 	$response['schduleInfo'] = $schdule;

	                            }else{

	                            	$response['messageText'] = "success";
				            	 	$response['messageCode'] = 200;
				            	 	$response['successCode'] = 1;
				            	 	$response['schduleInfo'] = count($schdule);
	                            } 
                        }  

		                


			    	  }else{
					        $response['messageText'] = "Invalid Request";
			        	 	$response['messageCode'] = 201;
			        	 	$response['successCode'] = 0; 
					        }
				        echo json_encode($response);die;  
		   
		       }

          	


    	  public function getAllRoute(){
                 

               $response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                    $data = $this->request->getData();
                    $response = $this->validateupdateProfileDriver($data);
                    
                  if($response['messageCode'] == 200){
                    $driverRouteTable = TableRegistry::get('DriverRoutes');
                     
                     
                     $driverRoute = $driverRouteTable->find('all')->where(['DriverRoutes.delivery_schdule_id'=>$data['schdule_id'],
                    	'DriverRoutes.user_id'=>$data['user_id']])->contain(['Routes'])->hydrate(false)->toArray();
                     
                     if(count($driverRoute)>0){

                            	$response['messageText'] = "success";
			            	 	$response['messageCode'] = 200;
			            	 	$response['successCode'] = 1;
			            	 	$response['routesInfo'] = $driverRoute;

                            }else{

                            	$response['messageText'] = "Driver does not have any route with this time";
			            	 	$response['messageCode'] = 261;
			            	 	$response['successCode'] = 1;
			            	 	$response['routesInfo'] = [];
                            }

              }


                


	    	  }else{
			        $response['messageText'] = "Invalid Request";
	        	 	$response['messageCode'] = 201;
	        	 	$response['successCode'] = 0; 
			        }
		        echo json_encode($response);die;  
   
       }

       private function validateupdateRouteDriver( $userId, $routeId, $schduleId ){

       	    $error = array();
       	    $driverRouteTable = TableRegistry::get('DriverRoutes');
             $isRouteBelongsToDriver = $driverRouteTable->find()->where(['DriverRoutes.route_id'=>$routeId,'DriverRoutes.user_id'=>$userId,'DriverRoutes.delivery_schdule_id'=>$schduleId])->count();
             
             if($isRouteBelongsToDriver == 1){
                  
                  $error['messageCode'] = 200;
             }else{

             	    $error['messageText'] = "This Route not Associated with this Driver.";
	        	 	$error['messageCode'] = 281;
	        	 	$error['successCode'] = 0; 
             }
             return $error;
       }

       private function returnDayName( $day = NULL ){

          if($day && $day != '')
          {
          	$name = $day;
   	   	  }else{
   	   	   $name = date('l');	
   	   	  }
       	   
       	   
       	   switch ($name) {
       	   	case 'Sunday':
       	   		 return 7;
       	   		break;
       	   		case 'Monday':
       	   		 return 1;
       	   		break;
       	   		case 'Tuesday':
       	   		 return 2;
       	   		break;
       	   		case 'Wednesday':
       	   		 return 3;
       	   		break;
       	   		case 'Thursday':
       	   		 return 4;
       	   		break;
       	   		case 'Friday':
       	   		 return 5;
       	   		break;
       	   		case 'Saturday':
       	   		 return 6;
       	   		break;
       	   	
       	   	default:
       	   		 
       	   		break;
       	   }

       }

       private function checkTodaySubscriptions($id, $type, $days, $schduleID ){

                     
                     $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                     $userSubscriptions = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id'=>$id])->first();

                      
                     if( count($userSubscriptions) > 0 ){

                     	    if($userSubscriptions['subscription_type']['subscription_type_name'] == 'weekly'){
                                    

                                    $days = explode('-', $userSubscriptions['days']);
                                    $todayName = $this->returnDayName();
                                    if( in_array($todayName, $days) ){
     
                                    	if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){

                                    		return true;
                                    	}
                                          return false;
                                    }
                                       return false;
                     	    }else if($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {

                     	            
                                   
                                   if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                    		return true;
                                    	}
                                          return false;


                     	            

                     	    


                     	    }else if($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate'){
                     	    	  

                               $startdate = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                               $subscriptionday = strtotime($startdate);
                               $now = time();
                               $datediff = $now - $subscriptionday;
                               $days = floor($datediff / (60 * 60 * 24));
                              

                               if($days%2==0){
 
                               	 if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                    		return true;
                                    	}
                                          return false;

                               }

                     	    }
                     }
                      return false;




       }


       private function factorySubscription( $data,$schduleID ){

                $final = array();

                foreach ($data as $key => $value) {
                $totalIndividualPrice = 0;	
                $final1 = array(); 
		             if( ( isset($value['custom_orders']) && ! empty( $value['custom_orders'] ) ) || ( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ) ) 
		            {
                      if( isset($value['custom_orders']) && ! empty( $value['custom_orders'] ) ){
                              
                               $temp1 = array(); 
                               foreach ($value['custom_orders'] as $k => $v) {
                                 $insideorders = array(); 
                                  $insideorders['name'] = $v['product']['name'];
                                  $insideorders['pro_id'] = $v['product']['id'];
                                  $insideorders['iscontainer'] = $v['product']['iscontainer'];
                                  $insideorders['quantity'] = $v['quantity'];
                                  $insideorders['unit'] = $v['product']['unit']['name'];
                                  $totalIndividualPrice = $totalIndividualPrice + $v['price'];
                                  $final1[] = $insideorders; 
                               } 
                          
                  	    }
                  	    if( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ){


                            $temp = array(); 
                               foreach ($value['user_subscriptions'] as $k => $v) {
                                 $insideorders = array();
                                 if( $this->checkTodaySubscriptions( $v['id'],$v['subscription_type_id'],$v['days'],$schduleID ) ){
     
		                                  $insideorders['name'] = $v['product']['name'];
		                                  $insideorders['pro_id'] = $v['product']['id'];
		                                  $insideorders['iscontainer'] = $v['product']['iscontainer'];
		                                  $insideorders['quantity'] = $v['quantity'];
		                                  $insideorders['unit'] = $v['product']['unit']['name'];
		                                  $totalIndividualPrice = $totalIndividualPrice + $v['subscriptions_total_amount'];
		                              
		                              }     
                                   if(count($insideorders)>0)
                                   {
                                    $final1[] = $insideorders;
                                   }
                               }
                              
                  	    }
                        
               if(count($final1)>0)
               {
                      

                       $isNotify = $this->isNotThisCustomer($schduleID,$value['id']);

                        if($isNotify){
                        $customerAddress['notified'] = 1;
                        }else{
                           $customerAddress['notified'] = 0;
                        }
                     
                        $containerInfo = $this->customerContainer($value['id']);
                        if($containerInfo){
                          
                           $customerAddress['containerNeedToBeCollect'] = $containerInfo;

                        }else{
                             
                             $customerAddress['containerNeedToBeCollect'] = 0;
                        }

                        $customerAddress['id'] = $value['id'];
                  	    $customerAddress['name'] = $value['name'];
                  	    $customerAddress['totalOrderSubscriptionPrice'] = $totalIndividualPrice;
                  	    $customerAddress['phoneNo'] = $value['phoneNo'];
                  	    $customerAddress['lat'] = $value['latitude'];
                  	    $customerAddress['lng'] = $value['longitude'];
                  	    $customerAddress['regionId'] = $value['region_id'];
                  	    $customerAddress['areaId'] = $value['area_id'];
                  	    $customerAddress['address'] = "#".$value['houseNo'].', '.$value['area']['name'] .' - '.$value['region']['name'];
                  	   
                  	    $highestInfo = array();
                        $highestInfo['subscriptionInfo'] = $final1;
                        $highestInfo['customerAddress'] = $customerAddress; 


                        $final[] = $highestInfo;
                    }    


                  	 }else{
                  	 	continue;
                  	 } 
                  } 
            	return $final; 
       }

     private function customerContainer($customerId){

     	 $usersContainerTable = TableRegistry::get('UserContainers');
         $usersContainer = $usersContainerTable->find()->select(['container_given'])->where(['user_id'=>$customerId])->toArray();
         if(isset($usersContainer)&&count($usersContainer)>0){
         	return $usersContainer[0]['container_given'];
         }else{
         	return false;
         }
     }  

      private function isNotThisCustomer($schduleID,$customerId){

      	      $routecustomersTable = TableRegistry::get('RouteCustomers');
      	      $routecustomers = $routecustomersTable->find()->where(['user_id'=>$customerId,'delivery_schdule_id'=>$schduleID,'notify_date'=>date('Y-m-d'),'is_notify'=>1])->count();
      	      return $routecustomers;


      } 


       private function findRouteRegionAreas( $routeId ){
               
       	      $routeCustomerTable = TableRegistry::get('RouteCustomers');
       	      $routeCustomer = $routeCustomerTable->find('all')->contain(['Regions','Areas'])->group(['RouteCustomers.region_id','RouteCustomers.area_id'])->where(['route_id'=>$routeId])->hydrate(false)->toArray();
       	      $area_region = array();
       	      foreach ($routeCustomer as $key => $value) {
       	      	  $local = array();
       	      	  $local['regionId'] = $value['region']['id'];
       	      	  $local['regioName'] = $value['region']['name'];
       	      	  $local['areaId'] = $value['area']['id'];
       	      	  $local['areaName'] = $value['area']['name'];
                  $area_region[]=$local;
       	      }
         return $area_region;
       }

       public function getAllRouteCustomer(){

       	$response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                    $data = $this->request->getData();
                    $response = $this->validateupdateProfileDriver($data);
                    
                    if($response['messageCode'] == 200){
                         
                        $response = $this->validateupdateRouteDriver($data['user_id'],$data['route_id'],$data['schdule_id']);
                         
                         if($response['messageCode'] == 200){

                         	$routeCustomerTable = TableRegistry::get('RouteCustomers');
                     	    $routeCustomerInfo = $routeCustomerTable->find()->select('user_id')->where(['status'=>0,'route_id'=>$data['route_id'],'delivery_schdule_id'=>$data['schdule_id'],'date <>'=>date('Y-m-d')])->hydrate(false)->toArray();
                     	   
                     	    $userIds = array();
                     	    if(count($routeCustomerInfo)>0){
                                    
                                    foreach ($routeCustomerInfo as $key => $value) {
                                    	  
                                    	  array_push($userIds, $value['user_id']);
                                    }
                                   
                                $userIds = array_unique($userIds);
                                $userTable = TableRegistry::get('Users');
						        $allUserSubOrder = $userTable->find('all')->contain([
						        	 'Regions',
						        	 'Areas',
						        	 'UserContainers',
						        	 'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query) {
								        return $query->where(['UserSubscriptions.users_subscription_status_id' => 1]);
								    },
						        	 
						        	 
						        	  'CustomOrders.Products.Units' => function (\Cake\ORM\Query $query) {
								        return $query->where(['CustomOrders.created' => date('Y-m-d'),
								        	'CustomOrders.status'=>0,'CustomOrders.delivery_schdule_id'=>$data['schdule_id']]);
								    }


						        	 ])->where(['Users.id IN'=>$userIds])->hydrate(false)->toArray();
                             //  pr($allUserSubOrder);die;
                                $itemsInfno = $this->factorySubscription($allUserSubOrder,$data['schdule_id']);
                                
                                $allAreaRegionThisRoutes = $this->findRouteRegionAreas($data['route_id']);
                                   
                                $response['messageText'] = "success";
			            	 	$response['messageCode'] = 200;
			            	 	$response['successCode'] = 1;
			            	 	$response['allRegionAreaThisRoute'] = $allAreaRegionThisRoutes;
			            	 	$response['routesCustomer'] = $itemsInfno;


                     	    }else{

                     	    	$response['messageText'] = "success";
			            	 	$response['messageCode'] = 200;
			            	 	$response['successCode'] = 1;
			            	 	$response['routesCustomer'] = [];

                     	    }
                         }  
                    }

	    	  }else{
			        $response['messageText'] = "Invalid Request";
	        	 	$response['messageCode'] = 201;
	        	 	$response['successCode'] = 0; 
			        }
		        echo json_encode($response);die;
       }

       private function validateCustomerId( $user_id ){
          $error = array();
       	  $userTable = TableRegistry::get('Users');
       	  $user = $userTable->find()->where(['id'=>$user_id])->count();
       	   if($user == 1){
                  
                  $error['messageCode'] = 200;
             }else{

             	    $error['messageText'] = "This Route not Associated with this Driver.";
	        	 	$error['messageCode'] = 201;
	        	 	$error['successCode'] = 0; 
             }
             return $error;

       }
       private function notThisUserUpdate($updateId){
              
              $userTable = TableRegistry::get('Users');
              $users = $userTable->find()->where(['id'=>$updateId])->count();

              if($users){
              	return true;
              }return false;

       }
       private function productExist( $productId ){
        
         $product = TableRegistry::get('Products');

          $users = $product->find()->where(['id'=>$productId])->count();
               
              if($users){
              	return true;
              }return false;

       }
       private function notValidItems($items){
                   
                  if(!isset($items[0]['name']) || count($items[0]['name'])==0){

                  	return true;
                  }else if(!isset($items[0]['id']) || count($items[0]['id'])==0){

                     return true;
                  }else if(!$this->productExist($items[0]['id'])){

                     return true;
                  }

       }
       private function validOneResponse($data){
 
             $error = array();
             $delivere = array('yes','no');
            	 if( ! isset( $data['userId'] ) || empty( $data['userId'] )){
            	 	$error['messageText'] = "User Id in raw dara can not be empty";
            	 	$error['messageCode'] = 400;
            	 	$error['successCode'] = 0; 
            	 }



            	 else if( ( ! isset( $data['containerGiven'] ) || empty( $data['containerGiven'] ) ) && ( $data['containerGiven'] != 0 )  ){
            	 	$error['messageText'] = "Container given can not be empty";
            	 	$error['messageCode'] = 401;
            	 	$error['successCode'] = 0; 
            	 }else if( ( ! isset( $data['containerCollect'] ) || empty( $data['containerCollect'] ) ) && ( $data['containerCollect'] != 0 ) ){
            	 	$error['messageText'] = "Container collect can not be empty";
            	 	$error['messageCode'] = 402;
            	 	$error['successCode'] = 0; 
            	 }




            	 else if( ! isset( $data['totalOrderSubscriptionPrice'] ) || empty( $data['totalOrderSubscriptionPrice'] )){
            	 	$error['messageText'] = "Total amount can not be empty";
            	 	$error['messageCode'] = 208;
            	 	$error['successCode'] = 0; 
            	 }

            	 else if( ! isset( $data['delivered'] ) || empty( $data['delivered'] )){
            	 	$error['messageText'] = "Delivered value  can not be empty";
            	 	$error['messageCode'] = 410;
            	 	$error['successCode'] = 0; 
            	 }else if(!in_array($data['delivered'], $delivere)){

            	 	$error['messageText'] = "Delivered value invlid";
            	 	$error['messageCode'] = 411;
            	 	$error['successCode'] = 0;

            	 }else if( ( $data['delivered'] == 'yes' ) && ( isset($data['reason']) ) ){
                     
                     $error['messageText'] = "Delivered value 1 but reason is present";
            	 	 $error['messageCode'] = 411;
            	 	 $error['successCode'] = 0;
            	 }else if( ( $data['delivered'] == 'no' ) && ( ! isset($data['reason']) ) ){
                     
                     $error['messageText'] = "Delivered value 0 but reason is absent";
            	 	 $error['messageCode'] = 411;
            	 	 $error['successCode'] = 0;
            	 }

            	 else if( ! $this->notThisUserUpdate($data['userId']) ){
            	 	$error['messageText'] = "Invlid user";
            	 	$error['messageCode'] = 403;
            	 	$error['successCode'] = 0; 
            	 }else if( $this->notValidItems($data['items']) ){
            	 	$error['messageText'] = "Please check the all items in Raw data properly";
            	 	$error['messageCode'] = 404;
            	 	$error['successCode'] = 0; 
            	 }else{
            	 	$error['messageCode'] = 200;
            	 }
 
              return $error;


       }
       private function validateOrderString( $data ){

 
            
          $res = json_decode( stripslashes( $data ),true );

		  if( $res === NULL )
		   {
		      $error['messageText'] = "Data format not supported";
	        	 	$error['messageCode'] = 363;
	        	 	$error['successCode'] = 0;
		   } else {

                 $error['messageCode'] = 200;   
				     
				}

	   	if($error['messageCode'] == 200){

                
                 foreach ($res as $key => $value) {
                           	 
                                  $error = $this->validOneResponse($value);  
                                 if(! $error['messageCode'] == 200){
                                      
                                      return $error;

                                 }    

                           }          

	   	} 		
 
         return $error;
       	   

       }

      
      private function notTimingExist($schduleId){

      	$delivery_schdule_Table = TableRegistry::get('DeliverySchdules');
      	$isExist = $delivery_schdule_Table->find()->where(['DeliverySchdules.id'=>$schduleId])->count();
      	if($isExist){
           return true;
      	}else{
        return false;
      	} 

      }
       private function validateupdateUpdateDelivery( $data ){


    	  	   $error = array();
            	 if( ! isset( $data['user_id'] ) || empty( $data['user_id'] )){
            	 	$error['messageText'] = "User Id can not be empty";
            	 	$error['messageCode'] = 207;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['token'] ) || empty( $data['token'] )){
            	 	$error['messageText'] = "Token can not be empty";
            	 	$error['messageCode'] = 208;
            	 	$error['successCode'] = 0; 
            	 }else if( ! isset( $data['schdule_id'] ) || empty( $data['schdule_id'] )){
            	 	$error['messageText'] = "Delivery Update Timing can not be empty";
            	 	$error['messageCode'] = 362;
            	 	$error['successCode'] = 0; 
            	 }else if( ! $this->notTimingExist($data['schdule_id']) ){
            	 	$error['messageText'] = "Invlid Timing Id";
            	 	$error['messageCode'] = 361;
            	 	$error['successCode'] = 0; 
            	 }else if( ! $this->notThisUser($data['user_id'],$data['token']) ){
            	 	$error['messageText'] = "Invlid user";
            	 	$error['messageCode'] = 209;
            	 	$error['successCode'] = 0; 
            	 }else{
            	 	$error['messageCode'] = 200;
            	 }
              return $error;



    	  }

     private function updateContainer($customer_Id, $containerGiven, $containerCollect){

     	           $usercontainerTable = TableRegistry::get('UserContainers');
     	           $usercontainer = $usercontainerTable->find()->where(['user_id'=>$customer_Id])->first();
     	          
     	           

                      
                      	if($containerCollect == 0)
                      	{    

                          $left_container_count = $usercontainer['container_given'] + $containerGiven;

                          $query = $usercontainerTable->query();
			              $result = $query->update()
					                    ->set(['container_given' => $containerGiven ,'container_collect' => $containerCollect,'left_container_count'=>$left_container_count])
					                    ->where(['user_id' => $customer_Id])
					                    ->execute();
					        if($result){
					        	return true;
					        }return false;
					    }else{


					    	$left_container_count = ( int ) ( ( $usercontainer['left_container_count'] + $containerGiven ) - $containerCollect );

                          $query = $usercontainerTable->query();
			              $result = $query->update()
					                    ->set(['container_given' => $containerGiven ,'container_collect' => $containerCollect,'left_container_count'=>$left_container_count])
					                    ->where(['user_id' => $customer_Id])
					                    ->execute();
					        if($result){
					        	return true;
					        }return false;

					    }




     	          
     } 	 

     private function alreadyUpdated( $customerId, $schduleId,$delivere,$reson,$items ){
     
 
	                
          if($delivere == "yes")
          {
	                $date = date('Y-m-d');
	                $status = 1;
	                $route_customerTable = TableRegistry::get('RouteCustomers');
               
                    $isUserExist = $route_customerTable->find()->where(['user_id'=>$customerId])->count(); 

	                   if($isUserExist)
	                   {
		                        $isUpdated = $route_customerTable->find()->where(['user_id'=>$customerId,'delivery_schdule_id'=>$schduleId,'date'=>$date,'status'=>$status])->count();
		 
		                        if($isUpdated){
		                        	return false;
		                        }else{
		                        	return true;
		                        }
	               }else{
	               	return false;
               }
        }else if($delivere == "no")
           {
    	        $date = date('Y-m-d');
                $status = 0;
                $route_customerTable = TableRegistry::get('RouteCustomers');
                $isUserExist = $route_customerTable->find()->where(['user_id'=>$customerId])->count(); 
                if($isUserExist)
                   {
		            $isUpdated = $route_customerTable->find()->where(['user_id'=>$customerId,'delivery_schdule_id'=>$schduleId,'date'=>$date,'status'=>$status])->count();
		 
                        if($isUpdated){
                        	return false;
                        }else{

                       /*update route customer for No*/

                       $query = $route_customerTable->query();
			             $result_update = $query->update()
			                    ->set(['date' => date('Y-m-d'),'status' => 0])
			                    ->where(['user_id' => $customerId,'delivery_schdule_id' => $schduleId])
			                    ->execute();


                       $rejected_ordersTable = TableRegistry::get('RejectedOrders');
                       $rejected_orders = $rejected_ordersTable->find()->where(['user_id'=>$customerId,'date'=>$date,'delivery_schdule_id'=>$schduleId])->count();

                        if(!$rejected_orders){
                           
                          $rejected_or = $rejected_ordersTable->newEntity();
                          $rejected_or->user_id = $customerId;
                          $rejected_or->reason = $reson;
                          $rejected_or->delivery_schdule_id = $schduleId;
                          $rejected_or->date = date('Y-m-d');
                          $resulted = $rejected_ordersTable->save($rejected_or);
                          if($resulted){
                                
                                $rejected_orders_itemTable = TableRegistry::get('RejectedOrderItems');
                                $producTable = TableRegistry::get('Products'); 
                                 foreach ($items['items'] as $k => $v) {
                                   
                                    $product = $producTable->find()->select(['price_per_unit'])->where(['id'=>$v['id']])->first();


                                  	$rejected_orders_items = $rejected_orders_itemTable->newEntity();
                                  	$rejected_orders_items->rejected_order_id = $resulted->id;
                                  	$rejected_orders_items->product_id = $v['id'];
                                  	$rejected_orders_items->qty = $v['quantity'];
                                  	$rejected_orders_items->price = ( $product['price_per_unit']*$v['quantity']);
                                  	$rejected_orders_items->date = date('Y-m-d');
                                  	$rejected_orders_itemTable->save($rejected_orders_items); 
                                  } 


                          }else{
                                
                                return false;
                          } 

                        }else{
                           return false;
                        }
                    }
	           }else{
	               	return false;
               }

        }else{
        	return false;
        }
    } 

      private function AddTransaction( $useritems, $schduleId,$totalPrice ){
                 $customOrderTable = TableRegistry::get('CustomOrders');
                 $transactionTable = TableRegistry::get('Transactions');
                 $userBalanceTable = TableRegistry::get('UserBalances');
                 if($totalPrice>0)
                 {

			     $checkTransactionExist =$transactionTable->find()->where(['user_id'=>$useritems['userId'],'transaction_amount_type'=>'Dr','created'=>date('Y-m-d'),'delivery_schdule_id'=>$schduleId,'status'=>1])->count();
                     
                     if(!$checkTransactionExist)
                     {
                          $transaction = $transactionTable->newEntity();
			              $transaction->user_id = $useritems['userId'];
			              $transaction->transaction_amount_type = 'Dr';
			              $transaction->amount = $totalPrice;
			              $transaction->created = date('Y-m-d'); 
			              $date = new \DateTime();
                          $transaction->time = date_format($date, 'H:i:s'); 
			              $transaction->delivery_schdule_id = $schduleId;
			              $transaction->status = 1;
			              $transaction->transaction_type = 'subscription/order';
			              if($transactionTable->save($transaction)){ 
			              	 
                            
                            $query = $customOrderTable->query();
								             $result = $query->update()
								                    ->set(['status' => 1])
								                    ->where(['created'=>date('Y-m-d'),'user_id' => $useritems['userId'],'delivery_schdule_id' => $schduleId])
								                    ->execute();

					       $userBalance = $userBalanceTable->find()->select(['balance'])->where(['user_id'=>$useritems['userId']])->first();
					       			                    
                           $leftBalance = ( $userBalance['balance'] - $totalPrice);
 

                           $query = $userBalanceTable->query();
								             $result = $query->update()
								                    ->set(['balance' => $leftBalance])
								                    ->where(['user_id'=>$useritems['userId']])
								                    ->execute();



			              }
			          } 


            }
           
      return true;

      } 


       public function updateDelivery(){
            

               $response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                    $data = $this->request->getData();
                   
                    $response = $this->validateupdateUpdateDelivery($data);
                      
                    if($response['messageCode'] == 200){
                     
                      
                      $response = $this->validateOrderString($data['data']);
                      
                      if( $response['messageCode'] == 200 ){


                      	$resultedString = json_decode($data['data'],true);
                         
                        $routecustomersTable = TableRegistry::get('RouteCustomers');

                        foreach ($resultedString as $key => $value) {




                                 $customer_Id = $value['userId'];
                                 $containerGiven = $value['containerGiven'];
                                 $containerCollect = $value['containerCollect'];
                                 $deliverdvalue = $value['delivered'];
                                 if(isset($value['reason'])&&!empty($value['reason'])){
                                 	$reson = $value['reason']; 
                                 }else{
                                 	$reson = '';
                                 }  
                           
                            
                            if( $this->alreadyUpdated( $customer_Id,$data['schdule_id'],$deliverdvalue,$reson,$value) )
                            {       

			                                 

                                  if($deliverdvalue == "yes")
                                  {      
			                                 $query = $routecustomersTable->query();
								             $result = $query->update()
								                    ->set(['date' => date('Y-m-d'),'status' => 1])
								                    ->where(['user_id' => $customer_Id,'delivery_schdule_id' => $data['schdule_id']])
								                    ->execute();
								              /*--- Cutomer Route Update END ---*/      
								              if($result){
			                                         
			                                       /*---update container ---*/
			                                        $updateContiner = $this->updateContainer($customer_Id,$containerGiven,$containerCollect);
			                                       /*---update container ---*/  

			                                      if($updateContiner){

			                                      	  $transactionUpdate = $this->AddTransaction($value,$data['schdule_id'],$value['totalOrderSubscriptionPrice']);
			                                      	  if($transactionUpdate){

			                                      	  	$response['messageText'] = "Updated Successfully";
									            	 	$response['messageCode'] = 331;
									            	 	$response['successCode'] = 1;
									            	  }else{
			                                             
			                                             $response['messageText'] = "Something went wrong Please try again";
									            	 	 $response['messageCode'] = 201;
									            	 	 $response['successCode'] = 0;
			                                      	  } 

			                                      } 


								              }
							           }   



					          }else{
					          	continue;
					          }

					         
 
					      }










                             $response['messageText'] = "Updated Successfully";
		            	 	 $response['messageCode'] = 331;
		            	 	 $response['successCode'] = 1; 

                                 
                      } 


                       
                     }
                 }else{
			        $response['messageText'] = "Invalid Request";
	        	 	$response['messageCode'] = 201;
	        	 	$response['successCode'] = 0; 
			        }
		         echo json_encode($response);die; 
       }

       private function validateLatLng( $data ){
                  
               //    preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $geoResults['latitude']);
               
               $response = array();

               if( ! isset( $data['lat'] ) || empty( $data['lat'] ) ){

               	$response['messageText'] = "Please enter Latitude";
        	 	$response['messageCode'] = 232;
        	 	$response['successCode'] = 0; 
               }else if( ! isset( $data['lng'] ) || empty( $data['lng'] ) ) {
                 
                $response['messageText'] = "Please enter Longitude";
        	 	$response['messageCode'] = 233;
        	 	$response['successCode'] = 0; 
               }else if(preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $data['lat']) ){

                $response['messageText'] = "Invalid Latitude";
        	 	$response['messageCode'] = 234;
        	 	$response['successCode'] = 0; 

               }else if(preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $data['lng']) ){

               	 $response['messageText'] = "Invalid Longitude";
        	 	 $response['messageCode'] = 235;
        	 	 $response['successCode'] = 0; 

               }

        if(count($response)==0){
             
             $response['messageCode'] = 200;
        }
        return $response;


       }


       public function startTracking(){

       	 $response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                    $data = $this->request->getData();
                   
                    $response = $this->validateupdateProfileDriver($data);
                      
                    if($response['messageCode'] == 200){

                         
                     $response = $this->validateLatLng($data);
                     
                     if($response['messageCode'] == 200){

                     	$orderTrackingTable = TableRegistry::get('OrderTrackings');
                     	
                        $orderTracking = $orderTrackingTable->find()->where(['user_id'=>$data['user_id']])->count();
                          
                       if($orderTracking == 0)
                       {	
		                     	$orderTracking = $orderTrackingTable->newEntity();
		                     	$orderTracking->user_id = $data['user_id'];
		                     	$orderTracking->lat = $data['lat'];
		                     	$orderTracking->lng = $data['lng'];
		                     	
		                     	
		                     	if($orderTrackingTable->save($orderTracking)){

		                     		$response['messageText'] = "Tracking Saved Successfully";
					        	 	$response['messageCode'] = 231;
					        	 	$response['successCode'] = 1; 
					        	 	$currentDateTime = date('Y-m-d h:i:s');
									$newDateTime = date('h:i A', strtotime($currentDateTime));
									$response['loggedtime'] = date("H:i", strtotime($newDateTime));

		                     	}
		                 }else{

		                 	     $query = $orderTrackingTable->query();
					             $result = $query->update()
					                    ->set(['lat' => $data['lat'],'lng' => $data['lng']])
					                    ->where(['user_id' => $data['user_id']])
					                    ->execute();
					             if($result){
					             	$response['messageText'] = "Tracking Saved Successfully";
					        	 	$response['messageCode'] = 231;
					        	 	$response['successCode'] = 1;
					        	 	$currentDateTime = date('Y-m-d h:i:s');
									$newDateTime = date('h:i A', strtotime($currentDateTime));
									$response['loggedtime'] = date("H:i", strtotime($newDateTime)); 
					             }else{
					             	$response['messageText'] = "Something Went Wrong Please try again";
					        	 	$response['messageCode'] = 201;
					        	 	$response['successCode'] = 0; 
					             }       

		                 } 	




                     }


                    }
                }else{
                    $response['messageText'] = "Invalid Request";
	        	 	$response['messageCode'] = 201;
	        	 	$response['successCode'] = 0; 
			        }
		         echo json_encode($response);die; 

       
   }


   public function stopTracking(){


       	 $response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                    $data = $this->request->getData();
                   
                    $response = $this->validateupdateProfileDriver($data);
                      
                    if($response['messageCode'] == 200){

                    	$orderTrackingTable = TableRegistry::get('OrderTrackings');
                    	$query = $orderTrackingTable->query();
					             $result = $query->update()
					                    ->set(['lat' => '','lng' => ''])
					                    ->where(['user_id' => $data['user_id']])
					                    ->execute();
					             if($result){
					             	$response['messageText'] = "Tracking Stoped Successfully";
					        	 	$response['messageCode'] = 200;
					        	 	$response['successCode'] = 1; 
					             }else{
					             	$response['messageText'] = "Something Went Wrong Please try again";
					        	 	$response['messageCode'] = 201;
					        	 	$response['successCode'] = 0; 
					             }       

                   }
             }else{
                $response['messageText'] = "Invalid Request";
        	 	$response['messageCode'] = 201;
        	 	$response['successCode'] = 0; 
		        }
	         echo json_encode($response);die;    	
   }


     /*--status code reserve for getCustomerBalance function Start from 2051 to 2100---*/

    public function sendPushToCustomerFromDriver(){ 
  
             
              $response = array();
    	  	   if( $this->request->is('post') ){ 
                    
                $data['user_id'] = $this->request->getData('driver_id');
                $data['token'] = $this->request->getData('token');
                $response = $this->validateupdateProfileDriver($data);
                if($response['messageCode'] == 200){

                   
                   $isDSTExist = $this->notTimingExist($this->request->getData('schdule_id'));
                   
                    if($isDSTExist)
                    {
                        
                        $customerId = $this->request->getData('customer_id');
                        if(! isset($customerId) || empty($customerId)){
                          
                          $response['messageText'] = "Customer id can not be empty";
	            	 	  $response['messageCode'] = 2051;
	            	 	  $response['successCode'] = 0;

                        }else{ 
                        
                        $checkUserExist = $this->isUserExist($this->request->getData('customer_id'));

                               if($checkUserExist){
                                 
						       		$userTable = TableRegistry::get('Users');
						    	  	$user = $userTable->find()->where(['id'=>$customerId,'type_user'=>'customer'])->select(['device_id'])->toArray();
						    	  	 if(count($user)>0){
                                          
                                        $deviceids = $user[0]['device_id'];
                                        $res = $this->sendFCM($deviceids);
                                        if($res){

                                           
                                        $this->updateRouteNotifyCustomer($this->request->getData('schdule_id'),$customerId);
                                            
                                        	$response['messageText'] = "Push Message Sent Successfully";
							        	 	$response['messageCode'] = 2054;
							        	 	$response['successCode'] = 1;

                                        }else{
                                             
                                            $response['messageText'] = "Something went wrong.Not Sent Push Message";
							        	 	$response['messageCode'] = 2055;
							        	 	$response['successCode'] = 1;
                                        }

						    	  	 }else{
                                        
                                        $response['messageText'] = "Device ID not Registered With Cremeway";
					            	 	$response['messageCode'] = 2053;
					            	 	$response['successCode'] = 0;
						    	  	 }  


                               }else{
                                   
                                   $response['messageText'] = "Customer id can not be recognized";
			            	 	   $response['messageCode'] = 2052;
			            	 	   $response['successCode'] = 0;

                               }    	
                        
                        }

                }else{
                	$response['messageText'] = "Invlid Timing Id";
            	 	$response['messageCode'] = 361;
            	 	$response['successCode'] = 0; 
                }
            }








              }else{
                $response['messageText'] = "Invalid Request";
        	 	$response['messageCode'] = 201;
        	 	$response['successCode'] = 0; 
		        }
	         echo json_encode($response);die;  

 
       
    }
    private function updateRouteNotifyCustomer($schdule_id,$customer_id){

         $route_customerTable =TableRegistry::get('RouteCustomers');
    	 $query = $route_customerTable->query();
			             $result_update = $query->update()
			                    ->set(['notify_date ' => date('Y-m-d'),'is_notify' => 1])
			                    ->where(['user_id' => $customer_id,'delivery_schdule_id' => $schdule_id])
			                    ->execute(); 
			                  return true;  

    }

    private function isUserExist( $customer_id ){

    	  $userTable = TableRegistry::get('Users');
    	  $user = $userTable->find()->where(['id'=>$customer_id,'type_user'=>'customer'])->count();
    	  if($user){
    	  	return true;
    	  }return false;

    }

    private function sendFCM($deviceid){
    	return true;
    	 $fcmApiKey = 'AIzaSyBBQ245rIcvNo-WMQwPsOUOVF8lvZyThmU';
       
       $url = 'https://fcm.googleapis.com/fcm/send'; 
      
       /*$deviceId = 'eu-C4spFaRM:APA91bHum5PFoqdHhABnaiOG1kxRDIZGaPEoZYo7N8gKgJDlz7lFo2flIyKY5WBkxpPTcNZOKXHNdg0OVUSARUfpADv-wD1tpsaCYBGH3DizDsr_RSz3oDCfL0_wKu2qzGNk3SBK6UED';*/
       $deviceId = $deviceid;
        
        $messageInfo = "Cremeway Driver Sunder has been Arrived Please ready to take milk";
        
        $dataArr = array();
        $id = array();
        $id[] = $deviceId;
		$dataArr['device_id'] = $id;
		$dataArr['message_key'] = $messageInfo; 


    	$registrationIds = $dataArr['device_id']; 
 
    	$message = $dataArr['message_key']; 
        $title = 'This is a title. title';

 
 
       
        $msg = array('message' => $message,'title'=>$title);
        $fields = array(
        	'registration_ids' => $registrationIds,
        	'data' => $msg,
        	'priority'      =>'high'
        	); 

 
        $headers = array(
            'Authorization: key=' . $fcmApiKey,
            'Content-Type: application/json'
        );
 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $url );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }else{
            
        } 
        curl_close($ch);   
        $res = json_decode($result);
        if($res->success == 1){     
        return true;
        }else{
        	return false;
        }
    
    }

















        



   

}
