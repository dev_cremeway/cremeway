<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use App\Controller\ExperttextingController;
use Twilio\Rest\Client;


 
class CustomerApiController extends AppController
{

    public function initialize()
        {
            parent::initialize();
            $this->Auth->allow(['sendOtp','getSchdule','getBalanceAndDeliveryInfo','getCustomerBalance','recomdedItemMilkManDetails','categoryProduct','verificationOtp','updateProfile','getAllRegions','getAllRegionsArea','searchProduct','updateOrder']);
        }

       /*---Start code for multilanguage from 1000-----*/

       /*--status code reserve for sendOtp function Start from 1000 to 1020---*/
       
       private function checkMobileNo( $mobileNo = NULL ){
                     
                      if($mobileNo){

                           $userTable = TableRegistry::get('Users');
                           $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer'])->count();
                           
                           if($user){
                            return true;
                           }else{
                            return false;
                           }

                      }else{
                        return false;
                      }
       }  

  












       private function validateLogin( $data ){

               $error = array();
               if( ! isset( $data['mobileNo'] ) || empty( $data['mobileNo'] )){
                $error['messageText'] = "Please enter your mobileNo";
                $error['messageCode'] = 1000;
                $error['successCode'] = 0; 
               }/*else if( $this->checkMobileNo($data['mobileNo'])){
                $error['messageText'] = "Mobile no. already exist.";
                $error['messageCode'] = 1001;
                $error['successCode'] = 0; 
               }*/else{
                $error['messageCode'] = 200;
               }
              return $error;
            }

           /* private function alreadyAuthorizedWithOtp($mobileNo){

                 if($mobileNo){

                           $userTable = TableRegistry::get('Users');
                           $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer','otp <>'=>0])->count();
                           if($user){
                            return true;
                           }else{
                            return false;
                           }
                           

            }
        }*/

        private function alreadyAuthorizedWithOtp($mobileNo){

                 if($mobileNo){

                           $userTable = TableRegistry::get('Users');
                          

                           $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer','otp <>'=>0,'verifed_otp'=>1])->count();

                           /* $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'type_user'=>'customer'])->count();*/
                          if($user){
                            return true;
                           }else{
                            return false;
                           }
                           

            }
        }



  private function updateOtp($otp,$mobile ){
               
                          
                $error = array();
               $userTable = TableRegistry::get('Users');
               $user = $userTable->find()->where(['phoneNo'=>$mobile])->count();
               if($user){
                       $query = $userTable->query();
                      $result = $query->update()
                      ->set(['otp'=>$otp,'verifed_otp'=>0])
                      ->where(['phoneNo' => $mobile])
                      ->execute();
                      if($result){
                        return true;
                      }else{
                        return false;
                      }
                               

               }else{

                          $userTable = TableRegistry::get('Users');
                          $newcustomer = $userTable->newEntity();
                          $newcustomer->verifed_otp = 0;
                          $newcustomer->otp = $otp;
                          $newcustomer->phoneNo = $mobile;
                          $newcustomer->created = date('Y-m-d');
                          $newcustomer->modified = date('Y-m-d');
                          $newcustomer->type_user = 'customer';
                          $newcustomer->status = 0;
                          $result = $userTable->save($newcustomer);
                         
                            if($result->id){
                              return true;
                            }else{
                              return false;
                            } 
                         }                         

            }

         private function updateToken( $mobile ){
               
                 $userTable = TableRegistry::get('Users');
                 $token = \Cake\Utility\Text::uuid();
                 $modified = date('Y-m-d h:i:s');
                 $query = $userTable->query();
                    $result = $query->update()
                              ->set(['token' => $token ,'modified' => $modified])
                              ->where(['phoneNo' => $mobile])
                              ->execute();
              if($result){
                return true;
              }else{
                return false;
              }                       

            }
        private function sendOtpToCustomer($otp,$mobileNo){
             
             require_once(ROOT . DS. 'vendor' . DS  . 'Twilio' . DS . 'autoload.php');
              $sid = 'ACaf9007c5da5a2f68a4fed37ee698ad8b';
               $token = '72c1413a90e3d1526e983167c1cc7030';
                $client = new Client($sid, $token);
                 $client->messages->create(
                     '+91'.$mobileNo,
                     array(
                        'from' => '+17603787142',
                        'body' => 'Your cremeway Verification code is '.$otp
                    )
                );


               return true;  

        }

        private function checkAddByAdmin( $mobileNo ){

           $error = array();
           $userTable = TableRegistry::get('Users');
           $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'status'=>1])->count();
           
           if($user){
            $error['messageCode'] = 200;
           }else{
            $error['messageCode'] = 201;
           }
       return $error;

        }


        private function verifedByOtp($mobileNo){

                  $error = array();
                  $userTable = TableRegistry::get('Users');
                  $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'otp IS NOT NULL','verifed_otp'=>1])->count();
           
                 if($user){
                  $error['messageCode'] = 200;
                 }else{
                  $error['messageCode'] = 201;
                 }
                return $error;

        
        }






 /*--status code reserve for VerificationOtp function Start from 2400 2440 ---*/

       public function verificationOtp(){
        
           $response = array();
          
           if( $this->request->is('post') ){

                   $data = $this->request->getData();
                   $error = $this->validateVerificationCode($data);
                   if($error['messageCode']==200){

                     $result = $this->updateAfterVerification($data);
                     if($result){

                                      $usersTable = TableRegistry::get('Users');
                                      $customerInfo = $usersTable
                                                        ->find('all')
                                                        //->contain(['Regions','Areas'])
                                                ->where(['Users.phoneNo'=>$data['mobileNo']])
                                        ->toArray();
                                    $error['customerInfo'] = $customerInfo;
                                    $currentDateTime = date('Y-m-d h:i:s');
                                    $newDateTime = date('h:i A', strtotime($currentDateTime));
                                    $error['loggedtime'] = date("H:i", strtotime($newDateTime));

                     }else{
                      $error['messageText'] = "Something went wrong please try again";
                      $error['successCode'] = 0;
                      $error['messageCode'] = 2403; 
                     }
                   
                   }
           }else{
                  $error['messageText'] = "Invalid Request";
                  $error['messageCode'] = 201;
                  $error['successCode'] = 0; 
            }
                  $response = json_encode($error);
                  echo $response;die;

                              

       }
  private function updateAfterVerification( $data ){

      $usersTable = TableRegistry::get('Users');
      $token = \Cake\Utility\Text::uuid();
      $modified = date('Y-m-d h:i:s');
      $verifed_otp = 1;
      $status = 0;
      $query = $usersTable->query();
                    $result = $query->update()
                              ->set(['device_id'=>$data['device_id'],'token' => $token ,'modified' => $modified,'verifed_otp'=>$verifed_otp,'status'=>$status])
                              ->where(['phoneNo' => $data['mobileNo']])
                              ->execute();
                   if($result){
                    return true;
                   }return false;           


  }     
  private function isMatchOtpMobile($mobileNo,$otp){

                  $error = array();
                  $userTable = TableRegistry::get('Users');
                  $user = $userTable->find()->where(['phoneNo'=>$mobileNo,'otp'=>$otp])->count();
                 if($user){
                  return true;
                 }return false;    

  }     

  private function validateVerificationCode( $data ){

               $error = array();
               if( ! isset( $data['mobileNo'] ) || empty( $data['mobileNo'] )){
                $error['messageText'] = "Please enter your mobileNo";
                $error['messageCode'] = 1000;
                $error['successCode'] = 0; 
               }elseif( ! isset( $data['otp_code'] ) || empty( $data['otp_code'] )){
                $error['messageText'] = "Please enter your otp code";
                $error['messageCode'] = 2401;
                $error['successCode'] = 0; 
               }elseif( ! isset( $data['device_id'] ) || empty( $data['device_id'] )){
                $error['messageText'] = "Please enter your device id";
                $error['messageCode'] = 2405;
                $error['successCode'] = 0; 
               }elseif( ! $this->isMatchOtpMobile($data['mobileNo'],$data['otp_code']) ){
                $error['messageText'] = "Invalid Details";
                $error['messageCode'] = 2402;
                $error['successCode'] = 0; 
               }else{
                $error['messageCode'] = 200;
               }
              return $error;
                

  }
 


         
         public function sendOtp(){

         $response = array();
          if( $this->request->is('post') ){

             $data = $this->request->getData();
             $error = $this->validateLogin($data);
              
             if($error['messageCode'] == 200)
             { 

                             /*$error = $this->checkAddByAdmin($data['mobileNo']);
                               
                              if($error['messageCode'] == 200)
                                {
                                           
                                            $updateToken = $this->updateToken($data['mobileNo']);
                                                if($updateToken){
                                                     $usersTable = TableRegistry::get('Users');
                                                      $customerInfo = $usersTable
                                                                        ->find('all')
                                                                        //->contain(['Regions','Areas'])
                                                                ->where(['Users.phoneNo'=>$data['mobileNo']])
                                                        ->toArray();
                                                      $error['customerInfo'] = $customerInfo;
                                                          $currentDateTime = date('Y-m-d h:i:s');
                                                    $newDateTime = date('h:i A', strtotime($currentDateTime));
                                                    $error['loggedtime'] = date("H:i", strtotime($newDateTime));
                                                                         
                                                        }else{
                                                            $error['messageText'] = "Something went wrong.Please try again";
                                                            $error['messageCode'] = 1002;
                                                            $error['successCode'] = 0;
                                                     }
                                 }else{ */

                                             /* $error = $this->verifedByOtp($data['mobileNo']);
                                              if($error['messageCode'] == 200){


                                                $usersTable = TableRegistry::get('Users');
                                                      $customerInfo = $usersTable
                                                                        ->find('all')
                                                                        //->contain(['Regions','Areas'])
                                                                ->where(['Users.phoneNo'=>$data['mobileNo']])
                                                        ->toArray();
                                                  $error['customerInfo'] = $customerInfo;
                                                      $currentDateTime = date('Y-m-d h:i:s');
                                                $newDateTime = date('h:i A', strtotime($currentDateTime));
                                                $error['loggedtime'] = date("H:i", strtotime($newDateTime));

                                                 


                                              }else{ */
                                                   $digits = 4;
                                                   $otp = rand(pow(10, $digits-1), pow(10, $digits)-1);
                                                   $updateOtp = $this->updateOtp($otp,$data['mobileNo']);
                                                   $sendOtp = $this->sendOtpToCustomer($otp,$data['mobileNo']);
                                                   if($sendOtp&&$updateOtp){
                                                        $error['messageText'] = "Otp has been sent to customer";
                                                        $error['messageCode'] = 1003;
                                                        //$error['messageCode'] = 200;
                                                        $error['successCode'] = 1; 

                                                   }else{
                                                        $error['messageText'] = "Something went wrong.Please try again";
                                                        $error['messageCode'] = 1004;
                                                        $error['successCode'] = 0;
                                                   }
                                                 //}

                                     //}
                              




             }
           }else{
                  $error['messageText'] = "Invalid Request";
                  $error['messageCode'] = 201;
                  $error['successCode'] = 0; 
            }
                  $response = json_encode($error);
                  echo $response;die;
     }













   /*--status code reserve for sendOtp function Start from 1020 to 1050---*/   

    private function validateupdateProfileCustomer( $data ){


             $error = array();
               if( ! isset( $data['user_id'] ) || empty( $data['user_id'] )){
                $error['messageText'] = "User id can not be empty";
                $error['messageCode'] = 1051;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['token'] ) || empty( $data['token'] )){
                $error['messageText'] = "Token can not be empty";
                $error['messageCode'] = 1052;
                $error['successCode'] = 0; 
               }else if( ! $this->notThisUser($data['user_id'],$data['token']) ){
                $error['messageText'] = "Invlid user";
                $error['messageCode'] = 1053;
                $error['successCode'] = 0; 
               }else{
                $error['messageCode'] = 200;
               }
              return $error;



        }


      public function getSchdule(){

                 $response = array();
                 if( $this->request->is('post') ){ 
                        
                        $data = $this->request->getData();
                        $response = $this->validateupdateProfileCustomer($data);

                        if($response['messageCode'] == 200)
                        {
                          $driverRouteTable = TableRegistry::get('DeliverySchdules');
                          $schdule = $driverRouteTable->find('all')->hydrate(false)->toArray();
                         
                              if(count($schdule)>0){
                                    
                                    foreach ($schdule as $key => $value) {
                                    
                                    $newDateTime = date('h:i A', strtotime($value['start_time']));
                  $schdule[$key]['start_time'] = date("H:i", strtotime($newDateTime));

                  $newDateTime = date('h:i A', strtotime($value['end_time']));
                  $schdule[$key]['end_time'] = date("H:i", strtotime($newDateTime));

                                    }

                                $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['schduleInfo'] = $schdule;

                              }else{

                                $response['messageText'] = "success";
                        $response['messageCode'] = 200;
                        $response['successCode'] = 1;
                        $response['schduleInfo'] = count($schdule);
                              } 
                        }  

                    


              }else{
                  $response['messageText'] = "Invalid Request";
                  $response['messageCode'] = 201;
                  $response['successCode'] = 0; 
                  }
                echo json_encode($response);die;  
       
           }



 
    private function notThisUser( $user_id, $token ){
              

              $driverTable = TableRegistry::get('Users');
            $driverInfo = $driverTable
                              ->find()
                  ->select(['id','name'])
                  ->where(['id'=>$user_id])
                  ->andwhere(['token'=>$token])
                  ->andwhere(['type_user'=>'customer'])
                  ->toArray();
            if(count($driverInfo)>0){
              return true;
            } return false;                              
                

          }


      
       /*--status code reserve for sendOtp function Start from 1051 to 1100---*/ 

        private function returnDayName( $day = NULL ){

          if($day && $day != '')
          {
            $name = $day;
          }else{
           $name = date('l'); 
          }
           
           
           switch ($name) {
            case 'Sunday':
               return 7;
              break;
              case 'Monday':
               return 1;
              break;
              case 'Tuesday':
               return 2;
              break;
              case 'Wednesday':
               return 3;
              break;
              case 'Thursday':
               return 4;
              break;
              case 'Friday':
               return 5;
              break;
              case 'Saturday':
               return 6;
              break;
            
            default:
               
              break;
           }

       }

       private function checkTodaySubscriptions($id, $type, $days, $schduleID ){
         //  echo $schduleID;
                     
                     $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
                     $userSubscriptions = $userSubscriptionsTable->find()->contain(['SubscriptionTypes'])->where(['UserSubscriptions.id'=>$id])->first();
//pr($userSubscriptions);die;
                      
                     if( count($userSubscriptions) > 0 ){

                      if($userSubscriptions['subscription_type']['subscription_type_name'] == 'everyday') {
                                   if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                        return true;
                                      }
                                          return false;

                          }else if($userSubscriptions['subscription_type']['subscription_type_name'] == 'alternate'){ 
                               $startdate = $userSubscriptions['startdate']->i18nFormat('YYY-MM-dd');
                               $subscriptionday = strtotime($startdate);
                               $now = time();
                               $datediff = $now - $subscriptionday;
                               $days = floor($datediff / (60 * 60 * 24));

                               if($days%2==0){
 
                                 if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                        return true;
                                      }else{
                                          return true;
                                      }

                               }else{


                                 
                                
                                if(in_array($schduleID,explode('-', $userSubscriptions['delivery_schdule_ids']) )){
 
                                       $date = date('Y-m-d');
                     $date1 = str_replace('-', '/', $date);
                     $tomorrow = date('m-d-Y',strtotime($date1 . "+1 days"));
                     $nextdate['comingdate'] = $tomorrow;
                                     return $nextdate;

                                      }else{
                                          return false;
                                      } 

                               }

                          }
                     }
                      return true;
       } 

      private function factorySubscription( $data,$schduleID ){
  //echo $schduleID;
  //pr($data);die;
                $final = array();
                foreach ($data as $key => $value) {
                $final1 = array(); 
                 if( ( isset($value['custom_orders']) && ! empty( $value['custom_orders'] ) ) || ( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ) ) 
                {
                        if( isset($value['user_subscriptions']) && ! empty( $value['user_subscriptions'] ) ){

                              foreach ($value['user_subscriptions'] as $k => $v) {
                                 $insideorders = array();
                                 $date = $this->checkTodaySubscriptions( $v['id'],$v['subscription_type_id'],$v['days'],$schduleID );
                                 
                                 if( isset( $date['comingdate'] ) && ! empty($date['comingdate']) ){
                                       
                                     
                                     $deliveryTimeTable = TableRegistry::get('DeliverySchdules');
                                     $deliveryTime = $deliveryTimeTable->find()->select(['name','start_time','end_time'])->where(['id'=>$schduleID])->first();  


                                    $insideorders['name'] = $v['product']['name'];
                                    $insideorders['price'] = $v['subscriptions_total_amount'];
                                    $insideorders['pro_id'] = $v['product']['id'];
                                    $insideorders['quantity'] = $v['quantity'];
                                    $insideorders['unit'] = $v['product']['unit']['name'];
                                    $insideorders['deliverydate'] = $date['comingdate'];
                                    $insideorders['timeToBeDeliver'] = $deliveryTime['name'];
                                    $insideorders['between'] = $deliveryTime['start_time'].'-'.$deliveryTime['end_time'];
                                   }else{  
                                          

                                         if($date)
                                          { 
                                          $deliveryTimeTable = TableRegistry::get('DeliverySchdules');
                                         $deliveryTime = $deliveryTimeTable->find()->select(['name','start_time','end_time'])->where(['id'=>$schduleID])->first(); 
                                        
                                      $insideorders['name'] = $v['product']['name'];
                                      $insideorders['price'] = $v['subscriptions_total_amount'];
                                      $insideorders['pro_id'] = $v['product']['id'];
                                      $insideorders['quantity'] = $v['quantity'];
                                      $insideorders['unit'] = $v['product']['unit']['name'];
                                      $insideorders['deliverydate'] = $date['comingdate'];
                                      $insideorders['timeToBeDeliver'] = $deliveryTime['name'];
                                      $insideorders['between'] = $deliveryTime['start_time'].'-'.$deliveryTime['end_time'];






                                     }else{
                                      return false;
                                     }




                                   }     
                                   if(count($insideorders)>0)
                                   {
                                    $final1[] = $insideorders;
                                   }
                               }
                              
                        }
                        
               if(count($final1)>0)
               {
                        $highestInfo = array();
                        $highestInfo['subscriptionInfo'] = $final1;
                        $final[] = $highestInfo;
                }
            }else{
                      continue;
                     } 
                  }
                if(isset($final[0]['subscriptionInfo'][0]['deliverydate']) && ! empty($final[0]['subscriptionInfo'][0]['deliverydate'])){
                  $a = explode('-',$final[0]['subscriptionInfo'][0]['deliverydate']);
                    $my_new_date = $a[2].'-'.$a[0].'-'.$a[1];
                   
                   $customordersTable = TableRegistry::get('CustomOrders');
                   $orderNext = $customordersTable->find('all')->where(['CustomOrders.user_id'=>$data[0]['id'],
                          'CustomOrders.delivery_schdule_id'=>$schduleID,'CustomOrders.status'=>0,'CustomOrders.created'=>$my_new_date
                    ])->contain(['Products','Units'])->toArray();

                      $orderInfo = array();
                      if(count($orderNext)>0){
                            
                             foreach ($orderNext as $key => $value) {
                                    $orderItem = array();
                                     $orderItem['name'] = $value['product']['name'];
                                     $orderItem['price'] = $value['price'];
                                     $orderItem['pro_id'] = $value['product']['id'];
                                     $orderItem['quantity'] = $value['product']['quantity'];
                                     $orderItem['unit'] = $value['unit']['name'];
                                     $orderInfo[] = $orderItem;
                             }

                             $final['customOrderInfo'] = $orderInfo;  
                      }  
                        
                }else{ 

                  $my_new_date = date('Y-m-d'); 
                   $customordersTable = TableRegistry::get('CustomOrders');
                  $orderNext = $customordersTable->find('all')->where(['CustomOrders.user_id'=>$data[0]['id'],
                          'CustomOrders.delivery_schdule_id'=>$schduleID,'CustomOrders.status'=>0,'CustomOrders.created'=>$my_new_date
                    ])->contain(['Products','Units'])->hydrate(false)->toArray(); 
                    // pr($orderNext);die;     
                      $orderInfo = array();
                      if(count($orderNext)>0){
                            
                             foreach ($orderNext as $key => $value) {
                                    $orderItem = array();
                                     $orderItem['name'] = $value['product']['name'];
                                     $orderItem['pro_id'] = $value['product']['id'];
                                     $orderItem['quantity'] = $value['quantity'];
                                     $orderItem['unit'] = $value['unit']['name'];
                                     $orderInfo[] = $orderItem;
                             }
 
                             $final['customOrderInfo'] = $orderInfo;  
                      }  


 
                } 

              return $final; 
       }

       private function checkItemThisTime($user_id,$schdule_id){

           $driverTable = TableRegistry::get('Users');
           $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
           $driverInfo = $driverTable
                              ->find()
                  ->select(['id'])
                  ->where(['id'=>$user_id,'type_user'=>'customer'])
                  ->toArray();

          $userId = $driverInfo[0]['id'];
          $userTable = TableRegistry::get('Users');
          $allUserSubOrder = $userTable->find('all')->contain([
            'UserSubscriptions.Products.Units' => function (\Cake\ORM\Query $query)  {
              return $query->where(['UserSubscriptions.users_subscription_status_id' => 1]);
          }
             ])->where(['Users.id'=>$userId])->hydrate(false)->toArray();
                
                
                if(count($allUserSubOrder)>0){
                   
                   $itemsInfno = $this->factorySubscription($allUserSubOrder,$schdule_id);
                   if(count($itemsInfno)>0)
                   {
                   return $itemsInfno;  
                   }else{
                    return false;
                   }

          }else{
            return false;
          }
           

       }
       private function checkItemAnotherTime($userid){
         

              $userSubscriptionsTable = TableRegistry::get('UserSubscriptions');
              $userSubscriptions = $userSubscriptionsTable->find('all')->where(['user_id'=>$userid])->toArray();
              $deliversSchdulesIds = array();
              foreach ($userSubscriptions as $key => $value) {

                      $d_sids = explode('-', $value['delivery_schdule_ids']);
                      foreach ($d_sids as $k => $v) {
                         array_push($deliversSchdulesIds, $v); 
                       }
              }

           $deliversSchdulesIds = array_unique($deliversSchdulesIds);

           $currentDateTime = date('Y-m-d h:i:s');
           $customerLoggedTime = date('h:i A', strtotime($currentDateTime));
           $dateObject = new \DateTime;
           $nowTime = $dateObject::createFromFormat('H:i A', $customerLoggedTime);
           $dayName = '';
           $iterationCount = 1;

           $deliverySchduleTable = TableRegistry::get('DeliverySchdules');

           $previous_diffrence = 0;
           $next_diffrence = 0;
           $finalDeliverySchdule = 0;
           foreach ($deliversSchdulesIds as $ke => $val) {

                       
                        $d_s = $deliverySchduleTable->find()->where(['id'=>$val])->first();
                        $start_time = $d_s['start_time'];
                        $end_time = $d_s['end_time'];
                        $d_s_t = $dateObject::createFromFormat('H:i A', $start_time);
                        $d_e_t = $dateObject::createFromFormat('H:i A', $end_time);
                        if( $nowTime < $d_s_t ){
                          $dayName = 'Today';
                          $next_diffrence_temp = $d_s_t->diff($nowTime);
                          
                          $day = $next_diffrence_temp->format('%d');
                          $hour = $next_diffrence_temp->format('%h');
                          $minute = $next_diffrence_temp->format('%i');
                          $next_diffrence = ( $day * 24 * 60) + ( $hour * 60 ) + $minute;
                          if($iterationCount == 1){
                            $previous_diffrence = $next_diffrence;
                            $finalDeliverySchdule = $val;

                          }else{

                                  if($next_diffrence < $previous_diffrence){
                                    $previous_diffrence = $next_diffrence;
                                    $finalDeliverySchdule = $val;
                                  }
                          }


                        }else{

                                   

                               
                          $dayName = 'Tomorrow'; 
                          $next_diffrence_temp = $d_s_t->diff($nowTime);
                          
                          $day = $next_diffrence_temp->format('%d');
                          $hour = $next_diffrence_temp->format('%h');
                          $minute = $next_diffrence_temp->format('%i');
                          $next_diffrence = ( $day * 24 * 60) + ( $hour * 60 ) + $minute;
                          
                          if($iterationCount == 1){
                            $previous_diffrence = $next_diffrence;
                            $finalDeliverySchdule = $val;
                          }else{

                                  if($next_diffrence > $previous_diffrence){
                                    $previous_diffrence = $next_diffrence;
                                    $finalDeliverySchdule = $val;
                                  }
                          }
                        }


                $iterationCount++;

              
           }
   
  $isItemFoundForThisTime = $this->checkItemThisTime($userid,$finalDeliverySchdule);
  $response = array();
  $response['itemss'] = $isItemFoundForThisTime;
  $response['d_s_i'] = $value['id'];
  $response['deliverydate1'] = $dayName;
   
  //echo $previous_diffrence;die;
  return $response; 



       }
       
       private function checkNextDelivery($data){ 
               
        $currentDateTime = date('Y-m-d h:i:s');
        $newDateTime = date('h:i A', strtotime($currentDateTime));  
        $response = array();
        $driverRouteTable = TableRegistry::get('DeliverySchdules');
              $schdule = $driverRouteTable->find('all')->hydrate(false)->toArray();
            //echo $newDateTime; 
            if(count($schdule)>0){
                  
                  foreach ($schdule as $key => $value) {
  
 
                  $current_time = $newDateTime;
                  $startTime = $value['start_time'];
                  $endTime = $value['end_time'];
                  $dateObject = new \DateTime;
                  $date1 = $dateObject::createFromFormat('H:i a', $current_time);
                  $date2 = $dateObject::createFromFormat('H:i a', $startTime);
                  $date3 = $dateObject::createFromFormat('H:i a', $endTime);
                  if ($date1 > $date2 && $date1 < $date3)
                  {
                                        
                                         $isItemFoundForThisTime = $this->checkItemThisTime($data['user_id'],$value['id']);
                                        $response['itemss'] = $isItemFoundForThisTime;
                                        $response['d_s_i'] = $value['id'];
                                        $response['deliverydate1'] = 'Today';
                                        return $response;

                  }else{

                        $isItemFoundForThisTime = $this->checkItemAnotherTime($data['user_id']);
                        //pr($isItemFoundForThisTime);die;
                        return $isItemFoundForThisTime; 

                  }    

                }

            }

       }
       private function getPrice( $orderInfo ){
          
              
              $totalPrice = 0;
              $producTable = TableRegistry::get('Products');
              
              if(isset($orderInfo['itemss'][0]['subscriptionInfo']))
              {
              foreach ($orderInfo['itemss'][0]['subscriptionInfo'] as $key => $value) {
                      
              //$product = $producTable->find()->select(['price_per_unit'])->where(['id'=>$value['pro_id']])->first();
              $totalPrice = $totalPrice +  $value['price'];
               }
             }


           if(isset($orderInfo['itemss']['customOrderInfo']))
           {

           foreach ($orderInfo['itemss']['customOrderInfo'] as $key => $value) {
           //$product1 = $producTable->find()->select(['price_per_unit'])->where(['id'=>$value['pro_id']])->first();
              $totalPrice = $totalPrice +  $value['price'];  
            }
            

            }       
               return $totalPrice;

       }
        public function getBalanceAndDeliveryInfo(){

                     $response = array();
                 if( $this->request->is('post') ){
                           $data = $this->request->getData();
                       $response = $this->validateupdateProfileCustomer($data);
                       if($response['messageCode'] == 200){
                           
                               $checkNextDeliveryTime = $this->checkNextDelivery($data);
                              $totalPrice = $this->getPrice($checkNextDeliveryTime);
                              if($checkNextDeliveryTime){
                             $response['messageText'] = "success";
                             $response['messageCode'] = 200;
                             $response['successCode'] = 1;
                             $response['subscriptionOrderTotalPrice'] = $totalPrice;
                             $response['customerBalance'] = $this->updatedCustomersBalance($data['user_id']);
                             $response['deliver_schdule_id'] = $checkNextDeliveryTime['d_s_i'];
                             $response['willDeliver'] = $checkNextDeliveryTime['deliverydate1'];
                             $response['subscriptionItems'] = $checkNextDeliveryTime['itemss'][0];
                             $response['orderItems'] = $checkNextDeliveryTime['itemss']['customOrderInfo'];
                                 }else{
                                  $response['messageText'] = "Not found any Subscriptio for this customer";
                            $response['messageCode'] = 1054;
                            $response['successCode'] = 0; 
                                 }  

                          } 


                  }else{
                  $response['messageText'] = "Invalid Request";
                  $response['messageCode'] = 201;
                  $response['successCode'] = 0; 
                  }
                echo json_encode($response);die; 

        }

 
   /*--status code reserve for getCustomerBalance function Start from 2000 to 2030---*/ 


      private function updatedCustomersBalance($user_id){
        $userBalanceTable = TableRegistry::get('UserBalances');
                   $userBalance = $userBalanceTable->find()->select(['balance'])->where(['user_id'=>$user_id])->toArray();
                   if(count($userBalance)>0){

                     return $userBalance[0]['balance'];

                   }else{

                       return 0;
                   }

      }

        /*--status code reserve for getCustomerBalance function Start from 2150 to 2200---*/ 


      private function getRecomandedItems(){
        $producTable = TableRegistry::get('Products');
        $products = $producTable->find('all')->order(['rand()'])->toArray();
        return $products;
      }  


       public function recomdedItemMilkManDetails(){

           
           $response = array();
           if( $this->request->is('post') ){
           
           
           $data = $this->request->getData();
           $response = $this->validateupdateProfileCustomer($data);
           if($response['messageCode'] == 200){

               $routeCustomerTable = TableRegistry::get('RouteCustomers');
               $routeCustomer = $routeCustomerTable->find()->select(['route_id'])->where(
                [
                  'user_id'=>$data['user_id'],
                  'delivery_schdule_id'=>$data['delivery_schdule_id']
                ])->toArray();
               
               if(isset($routeCustomer)&&count($routeCustomer)>0)
               {
                
                $routeid = $routeCustomer[0]['route_id'];
                $driverRouteTable = TableRegistry::get('DriverRoutes');
                $driverRoute = $driverRouteTable->find()->select([
                  'Users.name',
                  'Users.phoneNo',
                  'Users.latitude',
                  'Users.longitude'
                  ])->contain(['Users'])->where(['DriverRoutes.route_id'=>$routeid])->toArray();

                if(isset($driverRoute)&&count($driverRoute)>0){
                     
                     $recomandedItems = array();
                     $recomandedItems = $this->getRecomandedItems();
                     $response['messageText'] = "success";
                     $response['messageCode'] = 2152;
                     $response['successCode'] = 1;
                     $response['driverInfo'] = $driverRoute[0]['Users']; 
                     $response['recomandedItems'] = $recomandedItems;

                 
                }else{
                     
                  $response['messageText'] = "Not Found Any Driver for this route";
                  $response['messageCode'] = 2151;
                  $response['successCode'] = 0; 
                }
                  

              }else{
                  $response['messageText'] = "Not Found Any Driver for this route";
                  $response['messageCode'] = 2151;
                  $response['successCode'] = 0; 
              }
                

           }


           }else{
                  $response['messageText'] = "Invalid Request";
                  $response['messageCode'] = 201;
                  $response['successCode'] = 0; 
                  }
                echo json_encode($response);die; 




       }   


        /*--status code reserve for getCustomerBalance function Start from 2201 to 2250 ---*/ 

       public function categoryProduct(){


        $response = array();
           if( $this->request->is('post') ){
           
           
           $data = $this->request->getData();
           $response = $this->validateupdateProfileCustomer($data);
           if($response['messageCode'] == 200){
                 $category = array();
                 $categoryTable = TableRegistry::get('Categories');
                 $category = $categoryTable->find()->contain(['Products'])->count();
                 if(count($category)>0){
                   $categorylist = $categoryTable->find('all')->contain(['Products'])->hydrate(false)->toArray();
                   $response['messageText'] = "success";
                   $response['messageCode'] = 2251;
                   $response['successCode'] = 1;
                   $response['allCategoryProductsList'] = $categorylist; 

                 }else{
                      
                   $response['messageText'] = "Not Found and Product";
                   $response['messageCode'] = 2252;
                   $response['successCode'] = 1;
                   $response['allCategoryProductsList'] = $category;
                 }
           
           }


           }else{
                  $response['messageText'] = "Invalid Request";
                  $response['messageCode'] = 201;
                  $response['successCode'] = 0; 
                  }
                echo json_encode($response);die;

       }



         























        public function getCustomerBalance(){
         /*NOT IN USE NOW*/
          $response = array();
          if( $this->request->is('post') ){

              $data = $this->request->getData();
              $response = $this->validateupdateProfileCustomer($data);
               if($response['messageCode'] == 200){
                  
                   $userBalanceTable = TableRegistry::get('UserBalances');
                   $userBalance = $userBalanceTable->find()->select(['balance'])->where(['user_id'=>$data['user_id']])->toArray();
                   if(count($userBalance)>0){

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['customerBalance'] = $userBalance[0]['balance'];
                   }else{

                       $response['messageCode'] = 200;
                       $response['successCode'] = 1;
                       $response['customerBalance'] = [];
                   }

               }

           }else{
                  $response['messageText'] = "Invalid Request";
                  $response['messageCode'] = 201;
                  $response['successCode'] = 0; 
                  }
                echo json_encode($response);die;




        }


    

        public function getAllRegions(){


          $response = array();
          if( $this->request->is('post') ){

              $data = $this->request->getData();
              $response = $this->validateupdateProfileCustomer($data);
               if($response['messageCode'] == 200){

                    $regions = array();   
                    $regionTable = TableRegistry::get('Regions');
                    $regions = $regionTable->find('list')->toArray();
                    if(count($regions)>0){

                    $regionsList = array();
                    
                    foreach ($regions as $key => $value) {
                           
                            $temp = array();
                            $temp['regionId'] = $key;
                            $temp['regioName'] = $value;
                            array_push($regionsList, $temp);
                    }

                        
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;

                    }else{
                         
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['regionLists'] = $regionsList;
                    } 




                }

           }else{
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0; 
            }
          echo json_encode($response);die; 



                 


        }




     /*--status code reserve for getCustomerBalance function Start from 3101 to 3110 ---*/ 




      public function getAllRegionsArea(){


          $response = array();
          if( $this->request->is('post') ){

              $data = $this->request->getData();
              $response = $this->validateupdateProfileCustomer($data);
              if(!isset($data['region_id']) || empty( $data['region_id'] )){

                       
                      $response['messageText'] = "Region id can not be empty";
                      $response['messageCode'] = 3101;
                      $response['successCode'] = 0;
                      echo json_encode($response);die; 

              }

               if($response['messageCode'] == 200){

                    $regions = array();   
                    $areasTable = TableRegistry::get('Areas');
                    $areas = $areasTable->find('list')->where(['region_id'=>$data['region_id']])->toArray();
                    
                    $areaList = array();

                    if(count($areas)>0){
                     
                    $temp = array();
                    foreach ($areas as $key => $value) {
                         $temp['areaId'] = $key;
                         $temp['areaName'] = $value;
                         array_push($areaList, $temp);
                    }   

                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists'] = $areaList;

                    }else{
                         
                    $response['messageCode'] = 200;
                    $response['successCode'] = 1;
                    $response['areaLists'] = $areaList;
                    }

                }

           }else{
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0; 
            }
          echo json_encode($response);die; 



                 


        }


/*--status code reserve for getCustomerBalance function Start from 3111 to 3130 ---*/ 



        public function updateProfile(){

          $response = array();
          if( $this->request->is('post') ){

              $data = $this->request->getData();
              $response = $this->validateupdateProfileCustomer($data);
               if($response['messageCode'] == 200){

                $response = $this->validateupdateProfileUpdateCustomer($data);
                if($response['messageCode'] == 200){
                   $response = $this->validateImage($data['image']);
                     if($response['messageCode'] == 200)
                     {
                       
                       $imagename = $this->upload_image($data['image'],'customer','../webroot/img/images/');
                       $userTable = TableRegistry::get('Users');
                       $user = $userTable->get($data['user_id']);
                       $user->image = HTTP_ROOT.'/img/images/'.$imagename;  
                       $user->name = $data['name']; 
                       $user->email_id = $data['email']; 
                       $user->region_id = $data['region_id']; 
                       $user->area_id = $data['area_id']; 
                       $user->houseNo = $data['address']; 
                       $user->modified = date('Y-m-d h:i:s');
                       
                       if($userTable->save($user)){

                            $response['messageText'] = "Customer Has beed updated Successfully";
                            $response['messageCode'] = 3121;
                            $response['successCode'] = 1;
                            $response['updatedInfo'] = $user; 
                           
                       } else {
                            $response['messageText'] = "something went wrong";
                            $response['messageCode'] = 201;
                            $response['successCode'] = 0; 
                       } 
                    }
                  }
           } 

           }else{
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0; 
            }
          echo json_encode($response);die;  
        }


          private function validateupdateProfileUpdateCustomer( $data ){


             $error = array();
               if( ! isset( $data['area_id'] ) || empty( $data['area_id'] )){
                $error['messageText'] = "Area id can not be empty";
                $error['messageCode'] = 3112;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['region_id'] ) || empty( $data['region_id'] )){
                $error['messageText'] = "Region can not be empty";
                $error['messageCode'] = 3113;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['image'] ) || empty( $data['image'] )){
                $error['messageText'] = "Image can not be empty";
                $error['messageCode'] = 3114;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['name'] ) || empty( $data['name'] )){
                $error['messageText'] = "Name can not be empty";
                $error['messageCode'] = 3115;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['email'] ) || empty( $data['email'] )){
                $error['messageText'] = "Email can not be empty";
                $error['messageCode'] = 3116;
                $error['successCode'] = 0; 
               }else if( $this->isEmailAlreadyExist($data['email'],$data['user_id'])){
                $error['messageText'] = "This Email already Exist";
                $error['messageCode'] = 3117;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['address'] ) || empty( $data['address'] )){
                $error['messageText'] = "Address can not be empty";
                $error['messageCode'] = 3118;
                $error['successCode'] = 0; 
               }else if( ! $this->notThisAreaRegionRelate($data['area_id'],$data['region_id']) ){
                $error['messageText'] = "Area does not belongs to Region";
                $error['messageCode'] = 3119;
                $error['successCode'] = 0; 
               }else{
                $error['messageCode'] = 200;
               }
              return $error;



        }


       private function notThisAreaRegionRelate( $area_id,$region_id ){

           $areasTable = TableRegistry::get('Areas');
           $areas = $areasTable->find()->where(['id'=>$area_id,'region_id'=>$region_id])->count();
           if($areas){
            return true;
           }return false; 

       } 

      private  function isEmailAlreadyExist($email,$userid){

          $userTable = TableRegistry::get('Users');
          $user = $userTable->find()->where(['id <>'=>$userid,'email_id'=>$email])->count();
          if($user){
            return true;
          }return false;
               

      } 

       private function validateImage($base64){
      
      $imgdata = base64_decode($base64);

      $f = finfo_open();

      $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

      $mime_type = explode('/', $mime_type);
            if($mime_type[0] == 'image'){
          $response['messageCode'] = 200;
      }else{
               
                $response['messageText'] = "Invalid image";
                $response['messageCode'] = 3120;
                $response['successCode'] = 0; 

            } 

  return $response;

    }


        private function upload_image($content=null,$imgname=null,$dest=null,$extn=null){

            if($content){
                    $dataarray=explode('base64,', $content);
                    if(count($dataarray)==1){
                        $extn='jpg';
                        
                            if($extn){
                                $img_name = $imgname."-".time();
                                $data = $content;
                                $data = str_replace(' ', '+', $data);
                                $data = base64_decode($data);
                                file_put_contents($dest.$img_name.".".$extn, $data);

                                chmod($dest.$img_name.".".$extn, 0777);
                                return $img_name.".".$extn;
                            }else{
                                return false;
                            }
                        
                    }else{
                        return false;   
                    }
            }else{
                return false;
            }
    }


/*--status code reserve for searchResult function Start from 3131 to 3140 ---*/ 

          public function searchProduct(){
             
           $response = array();
          if( $this->request->is('get') ){
 
              $data = $_GET;
              $response = $this->validateupdateProfileCustomer($data);
               if($response['messageCode'] == 200){

               $product = array();

               $producTable = TableRegistry::get('Products');
               $product = $producTable->find('all')
                                                   ->where(['OR' => [
                                                  'name LIKE '=> "%".$data['search_param']."%",
                                                  'quantity LIKE '=> "%".$data['search_param']."  %"
                                              ]])->toArray();
               if(count($product)>0){
                   
                  $response['messageCode'] = 3141;
                  $response['successCode'] = 1;
                  $response['productLists'] = $product; 


               }else{
                  
                  $response['messageCode'] = 3141;
                  $response['successCode'] = 1;
                  $response['productLists'] = $product;
               }
               
                


            }
          }else{
            $response['messageText'] = "Invalid Request";
            $response['messageCode'] = 201;
            $response['successCode'] = 0; 
            }
          echo json_encode($response);die;
        }

     
 
   

/*--status code reserve for update Order Info function Start from 3150 to 3170 ---*/ 


   public function updateOrder(){

    $response = array();
          if( $this->request->is('post') ){

              $data = $this->request->getData();
              
              $response = $this->validateupdateProfileCustomer($data);
               if($response['messageCode'] == 200){

                  $response = $this->validateupdateTiming($data);
                  if($response['messageCode'] == 200){


                   $response = $this->isValidJson($data['updateorder'],$data['user_id'],$data['delivery_schdule_id']);
                   
                   if($response['messageCode'] == 200){
                       
                       $result = $this->updateOrderInfo($data);
                       if($result){
                         
                          $customorders = array();
                          $customordersTable = TableRegistry::get('CustomOrders');
                          $customorders = $customordersTable->find('all')->contain(['Products','DeliverySchdules'])->where(['user_id'=>$data['user_id'],'delivery_schdule_id'=>$data['delivery_schdule_id']])->toArray();
                         

                          if(count($customorders)>0)
                          {
                              $response['messageCode'] = 3155;
                              $response['successCode'] = 1;
                              $response['orderLists'] = $customorders;  
                          }else{
                              
                              $response['messageCode'] = 3155;
                              $response['successCode'] = 1;
                              $response['orderLists'] = $customorders; 

                       }
                    }
                        
               }    
                        

          }

        } 

   }else{
    $response['messageText'] = "Invalid Request";
    $response['messageCode'] = 201;
    $response['successCode'] = 0; 
    }
  echo json_encode($response);die; 

 }

 private  function validateupdateTiming($data){

      $error = array();
      if(!isset($data['delivery_schdule_id']) || empty($data['delivery_schdule_id'])){

        $error['messageText'] = "Delivery Update Timing can not be empty";
        $error['messageCode'] = 362;
        $error['successCode'] = 0; 
     }else{
        $error['messageCode'] = 200;
       }
      return $error;     

 }

 private function isValidJson($jsonHead,$user_id,$delivery_schdule_id){

     $res = json_decode( stripslashes( $jsonHead ),true );
 
      if( $res === NULL )
       {
              $error['messageText'] = "Data format not supported";
              $error['messageCode'] = 363;
              $error['successCode'] = 0;
       } else {

                 $error['messageCode'] = 200;   
             
        }

      if($error['messageCode'] == 200){

                
                 foreach ($res as $key => $value) {
                             
                                  $error = $this->validOneResponse($value,$user_id,$delivery_schdule_id);  
                                 if(! $error['messageCode'] == 200){
                                      
                                      return $error;

                                 }    

                           }          

      }     
 
         return $error;        

 }


 private function validOneResponse($data,$user_id,$delivery_schdule_id){

    $error = array();
    $deleted = array("yes","no");
    if( ! isset( $data['is_deleted'] ) || empty( $data['is_deleted'] )){
                $error['messageText'] = "Deleted signal values can not be empty";
                $error['messageCode'] = 3151;
                $error['successCode'] = 0; 
               }else if( ! isset( $data['pro_id'] ) || empty( $data['pro_id'] )){
                $error['messageText'] = "Container given can not be empty";
                $error['messageCode'] = 3152;
                $error['successCode'] = 0; 
               }else if( ! in_array($data['is_deleted'],$deleted) ){
                $error['messageText'] = "Invlid Delete signal values";
                $error['messageCode'] = 3153;
                $error['successCode'] = 0; 
               }elseif($this->productRelateToCustomer($data['pro_id'],$user_id,$delivery_schdule_id)){
                $error['messageText'] = "Invalid Products Customer";
                $error['messageCode'] = 3154;
                $error['successCode'] = 0; 
               }else{
                  $error['messageCode'] = 200;
                 }
 
              return $error;


 }


 private function productRelateToCustomer($pro_id,$user_id,$delivery_schdule_id){

       $customordersTable = TableRegistry::get('CustomOrders');
       $customorders = $customordersTable->find()->where(['user_id'=>$user_id,'product_id'=>$pro_id,'delivery_schdule_id'=>$delivery_schdule_id])->count();

       if($customorders>0){
        return false;
       }return true;
 }


 private function updateOrderInfo($data){
 
      $productdata = json_decode($data['updateorder'],true);
      
      $customordersTable = TableRegistry::get('CustomOrders');
      
      foreach ($productdata as $key => $value) {

         if(isset($value['is_deleted']) && $value['is_deleted'] == "yes"){
              $customordersRecord = $customordersTable->find('all')->where(
                                                         ['user_id'=>$data['user_id'],'product_id'=>$value['pro_id'],'delivery_schdule_id'=>$data['delivery_schdule_id']])->toArray(); 
              $customordersTable->id = $customordersRecord[0]['id'];
              $customordersTable->delete();
          }
          elseif(isset($value['is_deleted']) && $value['is_deleted'] == "no"){
                 
                $query = $customordersTable->query();
                $result = $query->update()
                          ->set(['quantity'=>$value['pro_qty']])
                          ->where(['user_id' => $data['user_id'],'product_id'=>$value['pro_id'],'delivery_schdule_id'=>$data['delivery_schdule_id']])
                          ->execute();    
      
          }


        
      }

     
        return true;



 }







  }       
 
