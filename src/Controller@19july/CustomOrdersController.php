<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CustomOrders Controller
 *
 * @property \App\Model\Table\CustomOrdersTable $CustomOrders
 *
 * @method \App\Model\Entity\CustomOrder[] paginate($object = null, array $settings = [])
 */
class CustomOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Products', 'Units']
        ];
        $customOrders = $this->paginate($this->CustomOrders);

        $this->set(compact('customOrders'));
        $this->set('_serialize', ['customOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Custom Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        /*$customOrder = $this->CustomOrders->get($id, [
            'contain' => ['Users', 'Products', 'Units']
        ]);

        $this->set('customOrder', $customOrder);
        $this->set('_serialize', ['customOrder']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customOrder = $this->CustomOrders->newEntity();
        if ($this->request->is('post')) {
            $customOrder = $this->CustomOrders->patchEntity($customOrder, $this->request->getData());
            if ($this->CustomOrders->save($customOrder)) {
                $this->Flash->success(__('The custom order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The custom order could not be saved. Please, try again.'));
        }
        $users = $this->CustomOrders->Users->find('list', ['limit' => 200]);
        $products = $this->CustomOrders->Products->find('list', ['limit' => 200]);
        $units = $this->CustomOrders->Units->find('list', ['limit' => 200]);
        $this->set(compact('customOrder', 'users', 'products', 'units'));
        $this->set('_serialize', ['customOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Custom Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customOrder = $this->CustomOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customOrder = $this->CustomOrders->patchEntity($customOrder, $this->request->getData());
            if ($this->CustomOrders->save($customOrder)) {
                $this->Flash->success(__('The custom order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The custom order could not be saved. Please, try again.'));
        }
        $users = $this->CustomOrders->Users->find('list', ['limit' => 200]);
        $products = $this->CustomOrders->Products->find('list', ['limit' => 200]);
        $units = $this->CustomOrders->Units->find('list', ['limit' => 200]);
        $this->set(compact('customOrder', 'users', 'products', 'units'));
        $this->set('_serialize', ['customOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Custom Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customOrder = $this->CustomOrders->get($id);
        if ($this->CustomOrders->delete($customOrder)) {
            $this->Flash->success(__('The custom order has been deleted.'));
        } else {
            $this->Flash->error(__('The custom order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
