<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Regions Controller
 *
 * @property \App\Model\Table\RegionsTable $Regions
 *
 * @method \App\Model\Entity\Region[] paginate($object = null, array $settings = [])
 */
class RegionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
     public function initialize()
        {
            parent::initialize();
            
            $this->Auth->allow();
        }
    public function index()
    {
        $regions = $this->paginate($this->Regions);

        $this->set(compact('regions'));
        $this->set('_serialize', ['regions']);
    }

    /**
     * View method
     *
     * @param string|null $id Region id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $region = $this->Regions->get($id, [
            'contain' => ['AreaRegionNotifications', 'Areas', 'Sales', 'Users']
        ]);

        $this->set('region', $region);
        $this->set('_serialize', ['region']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

     private function getAllRegions(){
          
           $regionTable = TableRegistry::get('Regions');
           $rList = $regionTable->find('all')
                                       ->hydrate(false)
                                       ->toArray();
 
           return $rList;
   }


   private function getRegion( $name ){

             $untTable = TableRegistry::get('Regions');
             $singleUnit = $untTable->find()
                                    ->where(['name'=>$name])
                                    ->count();
                                     
             return $singleUnit;
   }
    private function validateaddRegion( $data ){
                 
                  $error = array();
                  $data['name'] = trim($data['name']);
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the Regions name';
                  }else if( $this->getRegion( trim( $data['name'] ) ) ){
                    $error['name'] = 'Region name already taken';
                  }/*else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Region name';
                  }*/
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
       return $error;
    }


    public function add()
    {      
        /* $this->set('region',$this->getAllRegions());*/
        $this->paginate = [
            'limit' => 10,
            'conditions'=>['status'=>1],
            'order'=>['id DESC']
        ];
        $region = $this->paginate($this->Regions);
        $this->set(compact('region'));

          $error = array();
           $region = $this->Regions->newEntity();
            if ($this->request->is('post')) {
                 $error = $this->validateaddRegion($this->request->getData());
                  if($error['statuscode'] == 200)
                    {
                     $this->request->data['name'] = trim($this->request->getData('name'));
                     $region = $this->Regions->patchEntity($region, $this->request->getData());
                    if ($this->Regions->save($region)) {
                        $this->Flash->success(__('Region has been added successfully')); 
                        return $this->redirect(['action' => 'add']);
                    }
                 
            }else{
                 $this->set('error',$error);
                 $this->set('data',$this->request->getData());
            }
        }
         
    }

    /**
     * Edit method
     *
     * @param string|null $id Region id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */




    private function getifnameExistTwice( $id, $name ){


             $getname = TableRegistry::get('Regions');

             $getnameexist = $getname->find()
                                     ->where(['AND'=>['name'=>$name,'id <>'=>$id]])
                                     ->count();
             return $getnameexist;


     }
     
     private function validateEditRegion( $data ){

        $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Unit name can not be empty';
        }else if($this->getifnameExistTwice($data['id'],$data['name'])){

               $error['name'] = 'This unit already taken'; 
        }/*else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Region name';
        
        }*/

        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;

             
     }
    public function edit($id = null)
    {

        $error = array();

        
            if ($this->request->is(['patch', 'post', 'put'])) {

                 
                     $error = $this->validateEditRegion($this->request->getData());
                  
                      if($error['statuscode'] == 200)
                        {     
                          $region = $this->Regions->get($this->request->getData('id'), [
                            'contain' => []
                        ]);

                        $this->request->data['name'] = trim($this->request->getData('name')); 
                        $region = $this->Regions->patchEntity($region, $this->request->getData());
                        
                        
                        if ($this->Regions->save($region)) {  
                            $this->Flash->success(__('Region has been Updated successfully')); 
                            return $this->redirect(['action' => 'add']);
                        }
                    }else{
                             $this->set('error',$error);
                             $this->set('data',$this->request->getData());
                        }
            
        }else{
                $id = base64_decode($id);
                $Unit = $this->Regions->get($id, [
                    'contain' => []
                ]);    
                $this->set('data',$Unit);
      }
    }

    /**
     * Delete method
     *
     * @param string|null $id Region id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*$this->request->allowMethod(['post', 'delete']);*/
        $region = $this->Regions->get(base64_decode($id));
        $region->status = 0;
        if ($this->Regions->save($region)) {
            $this->Flash->success(__('The region has been deleted.'));
        } else {
            $this->Flash->error(__('The region could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add']);
    }
}
