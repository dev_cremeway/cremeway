<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * UserSubscriptions Controller
 *
 * @property \App\Model\Table\UserSubscriptionsTable $UserSubscriptions
 *
 * @method \App\Model\Entity\UserSubscription[] paginate($object = null, array $settings = [])
 */
class UserSubscriptionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
         
        $this->paginate = [
            'contain' => ['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses', 'Users', 'Units']
        ];
        $userSubscriptions = $this->paginate($this->UserSubscriptions);

        $this->set(compact('userSubscriptions'));
        $this->set('_serialize', ['userSubscriptions']);
    }



    public function calculateContainers(){

       if($_REQUEST){

          $globalQty = $_REQUEST['qty'];
          $product_id = $_REQUEST['pro_id'];
          $response = array();
          
          $productTable = TableRegistry::get('Products');
          $iscontainer = $productTable->find()->where(['id'=>$product_id,'iscontainer'=>1])->count();
          
          if($iscontainer > 0)
          {
              $containerRuleTable = TableRegistry::get('ContainerRules');
              $containerRule = $containerRuleTable->find('all')->where(['subscription_qty'=>$globalQty])->toArray();
              $totalConatiners = 0;
              if(count($containerRule))
              {
                foreach ($containerRule as $key => $value) {
                     
                      $totalConatiners = $totalConatiners + $value['container_count'];       
                 }
             }
             $response['status'] = 200;
             $response['containers'] = $totalConatiners;
             echo json_encode($response);die;
        }else{
          $response['status'] = 404;
          echo json_encode($response);die;
        }   







                    
        }else{
          echo false;die;
        }
  
   }

       

       public function getProduct(){

        if($_REQUEST){
                    $customer_id = $_REQUEST['customer_id']; 
                    $cat_id = $_REQUEST['cat_id'];
                    $productTable = TableRegistry::get('Products');
                    $product = $productTable->find('list')->where(['category_id'=>trim($cat_id),'is_subscribable'=>1])->toArray();
                    
                    /*----Get This category and user DS*/
                
                 $result = $this->checkCategoryDelivery($customer_id,$cat_id);
                 $results = array();
                 $results['products'] = $product;
                 $results['deliverytiming'] = $result;
                 echo json_encode($results);die;

        }else{
          echo false;die;
        }
    }

    public function getProductUnitName(){

       if($_REQUEST){
                   
                    $pro_id = $_REQUEST['pro_id'];
                    $productTable = TableRegistry::get('Products');
                    $unitId = $productTable->find()->where(['id'=>trim($pro_id)])->select(['unit_id'])->toArray();
                    
                    $uniTable = TableRegistry::get('Units');

                    $unitsName = $uniTable->find()->select(['name'])->where(['id'=>$unitId[0]['unit_id']])->toArray();
                    $name =  str_replace('"', '', $unitsName[0]['name']);
                    
                   $childProduct = $productTable->find('all')->where(['id'=>trim($pro_id)])->contain(['ProductChildren'])->toArray();
                   $productChieldres = array();

                   if(count($childProduct)>0){
                             
                              foreach ($childProduct[0]['product_children'] as $key => $value) {
                                     
                                      $temp = array();
                                      $temp['price'] = $value['price'];
                                      $temp['quantity'] = $value['quantity'];
                                      $temp['p_c_i'] = $value['id'];
                                      $productChieldres[] = $temp;
                              }
                   }

                    
                   $final = array();
                   $final['childproduct'] = $productChieldres;
                   $final['unitname'] = strtoupper($name);

                    echo json_encode($final);die;




        }else{
          echo false;die;
        }


    }


    public function getProductChildrenPriceAndCustomerBalance(){

      

        if($_REQUEST){

               /*pr($_REQUEST);die;  */   
               $response = array();
               $pro_c_id = $_REQUEST['pro_c_id'];
               $user_id = $_REQUEST['customer_id'];
               $userbalanceTable = TableRegistry::get('UserBalances');
               $balance = $userbalanceTable->find()->select(['balance'])->where(['user_id'=>$user_id])->first();

               $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
               $userSubscription = $userSubscriptionTable->find('all')->select(['subscriptions_total_amount'])->where(['user_id'=>$user_id])->toArray();

                $alreadySubscribedAmount = 0;

                if(count($userSubscription)>0){
                  foreach ($userSubscription as $key => $value) {
                        
                        $alreadySubscribedAmount = $alreadySubscribedAmount + $value['subscriptions_total_amount'];  

                  }
                }
 
                
                if($balance){

                 $productTable = TableRegistry::get('ProductChildren');  
                 $product = $productTable->find()->where(['id'=>$pro_c_id])->select(['price'])->toArray();
                 
                 $price = $product[0]['price'];
                 $totalPrice = $price;
                 
                 $response['statuscode'] = 301;
                 $response['subscriptionprice'] = $totalPrice;
                 $response['userbalance'] = $balance['balance'] - $alreadySubscribedAmount;


                }else{
                 $productTable = TableRegistry::get('ProductChildren');  
                 $product = $productTable->find()->where(['id'=>$pro_c_id])->select(['price'])->toArray();
                 $price = $product[0]['price'];
                 //$totalPrice = ( $price * $qty);
                 $response['statuscode'] = 300;
                 $response['subscriptionprice'] = $price;
                   
                    
                }

               echo json_encode($response);die;


        }else{
          echo false;die;
        }















    }

    public function getProductPriceAndCustomerBalance(){


       if($_REQUEST){

               /*pr($_REQUEST);die;  */   
               $response = array();
               $pro_id = $_REQUEST['pro_id'];
               $qty = $_REQUEST['qty'];
               $user_id = $_REQUEST['customer_id'];
               $userbalanceTable = TableRegistry::get('UserBalances');
               $balance = $userbalanceTable->find()->select(['balance'])->where(['user_id'=>$user_id])->first();

               $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
               $userSubscription = $userSubscriptionTable->find('all')->select(['subscriptions_total_amount'])->where(['user_id'=>$user_id])->toArray();

                $alreadySubscribedAmount = 0;

                if(count($userSubscription)>0){
                  foreach ($userSubscription as $key => $value) {
                        
                        $alreadySubscribedAmount = $alreadySubscribedAmount + $value['subscriptions_total_amount'];  

                  }
                }
 
                
                if($balance){

                 $productTable = TableRegistry::get('Products');  
                 $product = $productTable->find()->where(['id'=>$pro_id])->select(['price_per_unit'])->toArray();
                 
                 $price = $product[0]['price_per_unit'];
                 $totalPrice = ( $price * $qty);
                 
                 $response['statuscode'] = 301;
                 $response['subscriptionprice'] = $totalPrice;
                 $response['userbalance'] = $balance['balance'] - $alreadySubscribedAmount;


                }else{
                 $productTable = TableRegistry::get('Products');  
                 $product = $productTable->find()->where(['id'=>$pro_id])->select(['price_per_unit'])->toArray();
                 $price = $product[0]['price_per_unit'];
                 $totalPrice = ( $price * $qty);
                 $response['statuscode'] = 300;
                 $response['subscriptionprice'] = $totalPrice;
                   
                    
                }

               echo json_encode($response);die;


        }else{
          echo false;die;
        }

    }

    public function checkCategoryDelivery($customer_id,$cat_id){

      /*if($_REQUEST){*/

               
               $response = array();
               $cat_id = $cat_id;
               $customer_id = $customer_id;
               $getRegionAreaTable = TableRegistry::get('Users');
               $getRegionArea = $getRegionAreaTable->find()->select(['region_id','area_id'])->where(['id'=>$customer_id])->toArray();
                
               $region = $getRegionArea[0]['region_id'];
               $area = $getRegionArea[0]['area_id'];
               $category_delivery_schdulesTable = TableRegistry::get('CategoryDeliverySchdules');
               $category_delivery = $category_delivery_schdulesTable->find()->select(['CategoryDeliverySchdules.delivery_schdule_id','DeliverySchdules.name'])->where(['CategoryDeliverySchdules.category_id'=>$cat_id,'CategoryDeliverySchdules.region_id'=>$region,'CategoryDeliverySchdules.area_id'=>$area])->contain(['DeliverySchdules'])->toArray();
                 

                if(count($category_delivery) > 0){
                  
                 foreach ($category_delivery as $key => $value) {
                     
                       $temp = array();
                       $temp['d_s_id'] = $value['delivery_schdule_id'];
                       $temp['name'] = $value['delivery_schdule']['name'];
                       $response[] = $temp;
                  } 

                 } 
                return $response; 
    }
  







 
















    /**
     * View method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    public function view($id = null)
    {
        /*$userSubscription = $this->UserSubscriptions->get($id, [
            'contain' => ['SubscriptionTypes', 'Products', 'UsersSubscriptionStatuses', 'Users', 'Units']
        ]);

        $this->set('userSubscription', $userSubscription);
        $this->set('_serialize', ['userSubscription']);*/
    }
    public function deleteSubscription(){

         $id = base64_decode($_GET['id']);
         $subscription_types = TableRegistry::get('subscriptionTypes');
         $entity = $subscription_types->get($id);
         $result = $subscription_types->delete($entity);
         if($result){
            return $this->redirect(['action' => 'listSubscriptionsType']);
         }
       
    }

    private function getSubscriptonsTypes(){
        $subscription_types = TableRegistry::get('subscriptionTypes');
             $subscription_list = $subscription_types->find('all')
                                                      ->select(['id','subscription_type_name','created','modified'])
                                                      ->hydrate(false)
                                                      ->toArray();
              return $subscription_list;
    }





    public function customerSubscription(){
        
            
           if($_GET){


              $this->set('id',$_GET['id']); 
              $id = base64_decode($_GET['id']);
              $this->set('user_customer_id',$id);
            $UsersTable = TableRegistry::get('Users');  
            $users = $UsersTable->find()->contain(['Regions','Areas','UserBalances'])->where(['Users.id'=>$id])->first()->toArray();
            $this->set('data',$users);

            $usersBalanceTable = TableRegistry::get('UserBalances');
          $UsersTable = TableRegistry::get('Users');
          $usersBalance = $usersBalanceTable->find('all')->where(['user_id'=>$id])->select(['balance'])->first();
          $this->set('usersBalance',$usersBalance);

            $DeliverySchdulesTable = TableRegistry::get('DeliverySchdules');
            $subtype = TableRegistry::get('SubscriptionTypes');
            $DeliverySchdules = $DeliverySchdulesTable->find('list');
            $subscriptionTypes = $subtype->find('all')->select(['id','subscription_type_name'])->toArray();
              

              $usersTable = TableRegistry::get('Users');  
              $users = $usersTable->find('list')->where(['id'=>$id])->toArray(); 
               $categoryTable = TableRegistry::get('Categories');
               $categories = $categoryTable->find('list')->toArray();
               $containerTable = TableRegistry::get('Containers');
               $userContainer = $containerTable->find()->count();
              if($userContainer==0){
              $this->Flash->error(__('PLease add container first then add subscription.'));
              return $this->redirect('/Containers/add');
              
             }
             $units = $this->UserSubscriptions->Units->find('list')->where(['Units.name'=>'ltr']);
             $this->set(compact('categories','subscriptionTypes','users','DeliverySchdules'));


             $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
             $userSubscription = $userSubscriptionTable->find('all')->contain(['SubscriptionTypes','Products','UsersSubscriptionStatuses'])->where(['UserSubscriptions.user_id'=>$id])->toArray();
             $this->set('userSubscription',$userSubscription); 
            }






       if($this->request->is('post')){
        $data = $this->request->getData();
        
        
        $error = $this->validateAddSubscription($this->request->getData());
       if($error['statuscode'] == 200)
        {
            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $userSubscription =  $userSubscriptionTable->newEntity();
            $userSubscription->subscription_type_id = $data['subscription_type_id'];
            
            if(isset($data['days']) && !empty($data['days']))
            {
            $userSubscription->days = implode("-",$data['days']);
            $userSubscription->startdate = $data['start_date'];
            }else{
                $userSubscription->startdate = $data['start_date'];
                $userSubscription->days = '';
            }
            $userSubscription->product_id = $data['product_id'];
            $userSubscription->delivery_schdule_ids = implode("-",$data['delivery_schdule_ids']);
            
            if(isset($data['quantity']) && !empty($data['quantity'])){


           


             $userSubscription->quantity = $data['quantity'];
            

            }else{
              $userSubscription->quantity = $data['quantity_chield'];
              
            }
            
            $userSubscription->users_subscription_status_id = 1; 
            $userSubscription->user_id = $data['user_id'];

          
           $unitname = $this->getUnitNameAndId($data['product_id']);
           //pr($unitname);die;



            $userSubscription->unit_id = $unitname['id'];
            $userSubscription->unit_name = $unitname['name'];
            $userSubscription->summary = $data['summary'];
            $userSubscription->notes = $data['notes'];
            //$price = $data['subscription_price'];
           // $price = /*$this->getPriceSubscription($data['product_id'],$data['unit_id'],$data['quantity']);*/
            $userSubscription->subscriptions_total_amount = $data['subscription_price'];
            //$userSubscription->startdate = date('Y-m-d');
            $userSubscription->enddate = '2017-12-12';
            if($userSubscriptionTable->save($userSubscription)){

                 $saverouteCustomer = $this->addIntoRoute($data['user_id'],$data['delivery_schdule_ids']);
                  if($saverouteCustomer)
                  { 

                 
                   
                   /*---update product quantity -----*/

                      
                      $productTable = TableRegistry::get('Products');
                      $productqty = $productTable->find()->select(['quantity'])->where(['id'=>$data['product_id']])->first();
                      $leftQty = ( $productqty['quantity'] - $userSubscription->quantity); 
                      $query = $productTable->query();
                      $result = $query->update()
                      ->set(['quantity'=>$leftQty])
                      ->where(['id' => $data['product_id']])
                      ->execute();



                   /*-------------------------*/



                  /*----Save container---------*/
                  
                  
                 if(isset($data['subscription_containers']) && $data['subscription_containers'] > 0)
                 {
                      $userContainerTable = TableRegistry::get('UserContainers');
                      
                      $userContainer = $userContainerTable->find()->select(['container_given','id'])->where(['user_id'=>$data['user_id']])->toArray();
                     
                      if(isset($userContainer[0]['container_given'])){

                        $container_given_db =  $userContainer[0]['container_given'];
                        $container_given_db = $container_given_db + $data['subscription_containers'];

                        $query = $userContainerTable->query();
                        $query->update()
                            ->set(['container_given' => $container_given_db])
                            ->where(['id' => $userContainer[0]['id']])
                            ->execute();


                      }else{
                      $userContainer = $userContainerTable->newEntity();
                      $userContainer->container_given = $data['subscription_containers'];
                      $userContainer->user_id = $data['user_id'];
                      $userContainer->container_collect = 0;
                      $userContainer->left_container_count = 0;
                      $userContainerTable->save($userContainer);
                      }



                 }     
                  


                  /*----Save container---------*/




































                    
                  $error['statuscode'] = 200;
                  $error['name'] = "Subscription has been saved successfully";
                  echo json_encode($error);die;
                }else{
                  $error['statuscode'] = 201;
                  $error['name'] = "Something went erong please try again";
                  echo json_encode($error);die;
                }

            }else{
                  $error['statuscode'] = 201;
                  $error['name'] = "Something Went wrong.";
                  echo json_encode($error);die;
            }
        }else{
                  echo json_encode($error);die; 
        }   
     }  

    }

    public function listSubscriptionsType(){

             $this->set('subscription_list',$this->getSubscriptonsTypes());                                        
             if($this->request->is('post')){
                  $error = array();
                  $data = $this->request->data;
                  $subscrition_types = array('everyday','weekly','alternate');
                  
                  $subscription_type_name = $data['subscription_type_name'];
                   
                  $alreadySaved = $this->getSubscriptonsTypes();
                  $onlyname = array();
                  if( isset( $alreadySaved ) && count( $alreadySaved ) > 0 )
                  {
                      foreach ($alreadySaved as $key => $value) {
                        
                        $onlyname[] = $value['subscription_type_name'];            

                      }
                 }
                     
                  
                  if( ! in_array($subscription_type_name, $subscrition_types) ){
                          
                           $error['subscription_type_name'] = 'only ( everyday,weekly,alternate) values are required ';
                           $this->set('error',$error);
                  }else if( in_array($subscription_type_name, $onlyname) ){

                           $error['subscription_type_name'] = 'This Name Already saved';
                           $this->set('error',$error);
                  }
                  else{
                     $subscription_types = TableRegistry::get('subscriptionTypes');
                     $values = $subscription_types->newEntity();
                     $values->subscription_type_name = $subscription_type_name;
                     $values->created = date('Y-m-d h:i:s');
                     $values->modified = date('Y-m-d h:i:s');
                    if ($subscription_types->save($values)) {
                        $success['subscription_type_name'] = 'Saved Successfully!!!';
                           $this->set('success',$success);
                           return $this->redirect(['action' => 'listSubscriptionsType']);  
                          
                    }    
           }
      }   

    
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    private function getSubType($subscription_type_id){

           $subscription_typesTable = TableRegistry::get('SubscriptionTypes');
           $subscription = $subscription_typesTable->find('all')->where(['id'=>$subscription_type_id])->toArray();
          return $subscription['subscription_type_name'];  

    } 
    private function checkSubscription($userid,$productid,$delivery_schdule_ids,$subscription_type_id){

           $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
           $userSubscription = $userSubscriptionTable->find('all')->where([

                       'user_id'=>$userid,
                       'product_id'=>$productid
                       //'subscription_type_id'=>$subscription_type_id
            ])->toArray();
           if(count($userSubscription)<=0){
            return false;
           }else{
                 
                    return true;
                     /*$subscriptionType = $this->getSubType($subscription_type_id);
                     if($subscriptionType)*/

           }


    }
    private function validateAddSubscription( $data ){
              /*pr($data);die;*/

                  $error = array();
                   
                  if( ! isset( $data['user_id'] ) || empty( $data['user_id'] ) ) {
                     $error['name'] = 'Please select the user name';
                  }else if( ! isset( $data['product_id'] ) || empty( $data['product_id'] ) ){
                     $error['name'] = 'Please select the product';
                  }else if( ! isset( $data['delivery_schdule_ids'] ) || empty( $data['delivery_schdule_ids'] ) ){
                     $error['name'] = 'Please select Delivery Timing';
                  }else if( 

                    ( ! isset( $data['quantity'] ) || empty( $data['quantity'] ) )
                    &&
                    ( ! isset($data['quantity_chield']) || empty($data['quantity_chield']) )

                    ){
                     $error['name'] = 'Please select quantity';
                  }else if( 
                            ( ! isset( $data['days'] ) || empty( $data['days'] ) )
                            &&
                            ( ! isset( $data['start_date'] ) || empty( $data['start_date'] ) )
                           ){
                     $error['name'] = 'Please choose start date OR weekly days propely.';
                  }else if( ! isset( $data['subscription_type_id'] ) || empty( $data['subscription_type_id'] ) ){
                     $error['name'] = 'Please select the subscription type';
                  }else if( $this->checkSubscription($data['user_id'],$data['product_id'],$data['delivery_schdule_ids'],$data['subscription_type_id']) ){
                     $error['name'] = 'This subscription already taken by this customer';
                  }

                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                     $error['statuscode'] = 200;
                     }
                 
       return $error;


    }
    private function addIntoRoute($user_id,$delivery_schdule_ids){
               
                $usersTable = TableRegistry::get('Users');
                $users = $usersTable->find()->where(['id'=>$user_id])->select(['area_id','region_id'])->hydrate(false)->first();
                $region = $users['region_id'];
                $area = $users['area_id'];

                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                foreach ($delivery_schdule_ids as $key => $value) {
                          $d_s_id = $value;
                          /*echo $d_s_id;die;*/
                          $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id'=>$d_s_id,'region_id'=>$region,'area_id'=>$area,'user_id <>'=>$user_id])->hydrate(false)->toArray();
                         //pr($routeCustomer);die;
                          if(count($routeCustomer)>0){
                                $routeCustomers = $routeCustomerTable->newEntity();
                                $routeCustomers->user_id = $user_id;
                                $routeCustomers->route_id = $routeCustomer[0]['route_id'];
                                $routeCustomers->delivery_schdule_id = $d_s_id;
                                $routeCustomers->position = '';
                                $routeCustomers->date = '';
                                $routeCustomers->status = 0;
                                $routeCustomers->region_id = $region;
                                $routeCustomers->area_id = $area;
                                $routeCustomerTable->save($routeCustomers);
                          }else{


                             $routeCustomer = $routeCustomerTable->find('all')->where(['delivery_schdule_id'=>$d_s_id,'region_id'=>$region,'area_id'=>$area,'user_id IS NULL'])->hydrate(false)->toArray();
 

 
                             if(count($routeCustomer)>0)
                             {
                                 $query = $routeCustomerTable->query();
                                  $result = $query->update()
                                    ->set(['user_id' => $user_id])
                                    ->where(['id' => $routeCustomer[0]['id']])
                                    ->execute();
                              }





                          }



                } 
         return true;
    }
    private function getPriceSubscription($productId,$unitId,$qty){

                 $uniTable = TableRegistry::get('Units');
                 $productTable = TableRegistry::get('Products');
                 $unitname = $uniTable->find()->where(['id'=>$unitId])->first();
                 if($unitname['name'] == 'ltr'){
                   
                   $product = $productTable->find()->where(['id'=>$productId])->select(['price_per_unit'])->first();

                   return $qty*$product['price_per_unit'];


                 }if($unitname['name'] == 'kg'){

                  return $qty*$product['price_per_unit'];
                 
                 }if($unitname['name'] == 'ml'){
                    
                    

                 }if($unitname['name'] == 'gram'){
                  


                 }
     return 0;
    }



  public function getUnitNameAndId($pro_id){
      $productTable = TableRegistry::get('Products');
                      $unitId = $productTable->find()->where(['id'=>trim($pro_id)])->select(['unit_id'])->toArray();
                      
                      $uniTable = TableRegistry::get('Units');

                      $unitsName = $uniTable->find()->select(['name','id'])->where(['id'=>$unitId[0]['unit_id']])->toArray();
            
            $unit = array();
            $unit['name'] = $unitsName[0]['name'];
            $unit['id'] = $unitsName[0]['id'];
            return $unit;

  }






    public function add()
    {
          
        if($this->request->is('post')){
        $data = $this->request->getData();
        
        
        $error = $this->validateAddSubscription($this->request->getData());
       if($error['statuscode'] == 200)
        {
            $userSubscriptionTable = TableRegistry::get('UserSubscriptions');
            $userSubscription =  $userSubscriptionTable->newEntity();
            $userSubscription->subscription_type_id = $data['subscription_type_id'];
            
            if(isset($data['days']) && !empty($data['days']))
            {
            $userSubscription->days = implode("-",$data['days']);
            $userSubscription->startdate = $data['start_date'];
            }else{
                $userSubscription->startdate = $data['start_date'];
                $userSubscription->days = '';
            }
            $userSubscription->product_id = $data['product_id'];
            $userSubscription->delivery_schdule_ids = implode("-",$data['delivery_schdule_ids']);
            
            if(isset($data['quantity']) && !empty($data['quantity'])){


           


             $userSubscription->quantity = $data['quantity'];
            

            }else{
              $userSubscription->quantity = $data['quantity_chield'];
              
            }
            
            $userSubscription->users_subscription_status_id = 1; 
            $userSubscription->user_id = $data['user_id'];

          
           $unitname = $this->getUnitNameAndId($data['product_id']);
           //pr($unitname);die;



            $userSubscription->unit_id = $unitname['id'];
            $userSubscription->unit_name = $unitname['name'];
            $userSubscription->summary = $data['summary'];
            $userSubscription->notes = $data['notes'];
            //$price = $data['subscription_price'];
           // $price = /*$this->getPriceSubscription($data['product_id'],$data['unit_id'],$data['quantity']);*/
            $userSubscription->subscriptions_total_amount = $data['subscription_price'];
            //$userSubscription->startdate = date('Y-m-d');
            $userSubscription->enddate = '2017-12-12';
            if($userSubscriptionTable->save($userSubscription)){

                 $saverouteCustomer = $this->addIntoRoute($data['user_id'],$data['delivery_schdule_ids']);
                  if($saverouteCustomer)
                  {
                 
                  




















                  /*----Save container---------*/
                  
                  
                 if(isset($data['subscription_containers']) && $data['subscription_containers'] > 0)
                 {
                      $userContainerTable = TableRegistry::get('UserContainers');
                      
                      $userContainer = $userContainerTable->find()->select(['container_given','id'])->where(['user_id'=>$data['user_id']])->toArray();
                     
                      if(isset($userContainer[0]['container_given'])){

                        $container_given_db =  $userContainer[0]['container_given'];
                        $container_given_db = $container_given_db + $data['subscription_containers'];

                        $query = $userContainerTable->query();
                        $query->update()
                            ->set(['container_given' => $container_given_db])
                            ->where(['id' => $userContainer[0]['id']])
                            ->execute();


                      }else{
                      $userContainer = $userContainerTable->newEntity();
                      $userContainer->container_given = $data['subscription_containers'];
                      $userContainer->user_id = $data['user_id'];
                      $userContainer->container_collect = 0;
                      $userContainer->left_container_count = 0;
                      $userContainerTable->save($userContainer);
                      }



                 }     
                  


                  /*----Save container---------*/




































                    
                  $error['statuscode'] = 200;
                  $error['name'] = "Subscription has been saved successfully";
                  echo json_encode($error);die;
                }else{
                  $error['statuscode'] = 201;
                  $error['name'] = "Something went erong please try again";
                  echo json_encode($error);die;
                }

            }else{
                  $error['statuscode'] = 201;
                  $error['name'] = "Something Went wrong.";
                  echo json_encode($error);die;
            }
        }else{
                  echo json_encode($error);die; 
        }   
     }else{
            $DeliverySchdulesTable = TableRegistry::get('DeliverySchdules');
            $subtype = TableRegistry::get('SubscriptionTypes');
            $DeliverySchdules = $DeliverySchdulesTable->find('list');
            $subscriptionTypes = $subtype->find('all')->select(['id','subscription_type_name'])->toArray();
              /*$categoriesTable = TableRegistry::get('Categories');
              $categories = $categoriesTable->find('all')->select(['id'])->where(['name'=>'Milk'])->toArray();
              if(count($categories)>0)
              { 
                    
                 $productTable = TableRegistry::get('Products');
                 $products = $productTable->find('list')->where(['category_id'=>$categories[0]['id']])->toArray();
              }*/
           /* $usersSub = $this->UserSubscriptions->find('all')->select(['user_id'])->toArray();
            $usrIds = array();
            foreach ($usersSub as $key => $value) {
               array_push($usrIds, $value['user_id']);
             } 
            
             if(count($usrIds)>0)
             { 
             $usersTable = TableRegistry::get('Users');  
             $users = $usersTable->find('list')->where(['Users.id NOT IN'=>$usrIds,'Users.type_user'=>'customer'])->toArray();
             }else{
              $usersTable = TableRegistry::get('Users');  
              $users = $usersTable->find('list')->where(['Users.type_user'=>'customer'])->toArray();
             }*/ 

              $usersTable = TableRegistry::get('Users');  
              $users = $usersTable->find('list')->where(['Users.type_user'=>'customer','Users.status'=>1])->order(['id DESC'])->toArray(); 
               $categoryTable = TableRegistry::get('Categories');
               $categories = $categoryTable->find('list')->toArray();
               $containerTable = TableRegistry::get('Containers');
               $userContainer = $containerTable->find()->count();
              if($userContainer==0){
              $this->Flash->error(__('PLease add container first then add subscription.'));
              return $this->redirect('/Containers/add');
              
             }

             $units = $this->UserSubscriptions->Units->find('list')->where(['Units.name'=>'ltr']);
             $this->set(compact('categories','subscriptionTypes','users','DeliverySchdules'));
             
         }
         

    }

    /**
     * Edit method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userSubscription = $this->UserSubscriptions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userSubscription = $this->UserSubscriptions->patchEntity($userSubscription, $this->request->getData());
            if ($this->UserSubscriptions->save($userSubscription)) {
                $this->Flash->success(__('The user subscription has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user subscription could not be saved. Please, try again.'));
        }
        $subscriptionTypes = $this->UserSubscriptions->SubscriptionTypes->find('list', ['limit' => 200]);
        $products = $this->UserSubscriptions->Products->find('list', ['limit' => 200]);
        $usersSubscriptionStatuses = $this->UserSubscriptions->UsersSubscriptionStatuses->find('list', ['limit' => 200]);
        $users = $this->UserSubscriptions->Users->find('list', ['limit' => 200]);
        $units = $this->UserSubscriptions->Units->find('list', ['limit' => 200]);
        $this->set(compact('userSubscription', 'subscriptionTypes', 'products', 'usersSubscriptionStatuses', 'users', 'units'));
        $this->set('_serialize', ['userSubscription']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Subscription id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userSubscription = $this->UserSubscriptions->get($id);
        if ($this->UserSubscriptions->delete($userSubscription)) {
            $this->Flash->success(__('The user subscription has been deleted.'));
        } else {
            $this->Flash->error(__('The user subscription could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
}
