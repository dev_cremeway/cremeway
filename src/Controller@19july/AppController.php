<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Acl\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public function initialize()
    {   
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
       $this->loadComponent('Acl',[
                        'className' => 'Acl.Acl'
                    ]);
                
                $this->loadComponent('Auth', [
                    'authorize' => [
                        'Acl.Actions' => ['actionPath' => 'controllers/']
                    ],
                    'loginAction' => [
                        'plugin' => false,
                        'controller' => 'Users',
                        'action' => 'login'
                    ],
                    'loginRedirect' => [
                        'plugin' => false,
                        'controller' => 'Users',
                        'action' => 'index'
                    ],
                    'logoutRedirect' => [
                        'plugin' => false,
                        'controller' => 'Users',
                        'action' => 'login'
                    ],
                    'unauthorizedRedirect' => [
                        'controller' => 'Users',
                        'action' => 'login',
                        'prefix' => false
                    ],
                   // 'authError' => 'You are not authorized to access that location.',
                    'flash' => [
                        'element' => 'error'
                    ]
                ]); 
 
    }

    public function getControllers() {
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.', 
            '..', 
            'Component', 
            'AppController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }            
        }
          

        $onlyforadmin = array('Error','Pages','Permissions','Groups');  

         
       return array_values(array_diff($results,$onlyforadmin));
  }


    public function isAuthorized($user){ 
     if($user)
     {   
             $permissionsList = array(); 
             $this->Permissions =  TableRegistry::get('Permissions');
             $Aros = $this->Acl->Aro->find('threaded')->where(['foreign_key'=>$user['group_id']])->hydrate('false')->toArray();
             $Acos = $this->Acl->Aco->find('threaded')->where(['id'=>$Aros[0]['id']])->hydrate('false')->toArray();
              
             if($Acos[0]['alias'] == 'controllers'){

               $permissionsList['admin'] = 1;

             }else{ 
               
            $permission = TableRegistry::get('Permissions');
            $permissionlist = $permission->find('all')->where(['aro_id'=>$Aros[0]['id'],'_create'=>1,'_read'=>1,'_update'=>1,'_delete'=>1])->hydrate('false')->toArray();

            
            foreach ($permissionlist as $key => $value) {
                
              $Acos = $this->Acl->Aco->find('threaded')->where(['id'=>$value['aco_id']])->hydrate('false')->toArray();
               $permissionsList['controller'][] = $Acos[0]['alias'];

        } 
    }

           if(count($permissionsList) == 0){
            $permissionsList['controller'] = [];
           } 
           
          return $permissionsList;
      }
    

    }

     public function makeAuthorizedFromAdmin($user){
      
     if($user)
     {   
             $permissionsList = array(); 
             $this->Permissions =  TableRegistry::get('Permissions');
             $Aros = $this->Acl->Aro->find('threaded')->where(['foreign_key'=>$user['group_id']])->hydrate('false')->toArray();
             $Acos = $this->Acl->Aco->find('threaded')->where(['id'=>$Aros[0]['id']])->hydrate('false')->toArray();
              
             if($Acos[0]['alias'] == 'controllers'){

               $permissionsList['admin'] = 1;

             }else{ 
               
            $permission = TableRegistry::get('Permissions');
            $permissionlist = $permission->find('all')->where(['aro_id'=>$Aros[0]['id'],'_create'=>1,'_read'=>1,'_update'=>1,'_delete'=>1])->hydrate('false')->toArray();

            
            foreach ($permissionlist as $key => $value) {
                
              $Acos = $this->Acl->Aco->find('threaded')->where(['id'=>$value['aco_id']])->hydrate('false')->toArray();
               $permissionsList['controller'][] = $Acos[0]['alias'];

        } 
    }

           if(count($permissionsList) == 0){
            $permissionsList['controller'] = [];
           } 
           
          return $permissionsList;
      }
    

    }

    public function beforeFilter(Event $event)
    {   
        parent::beforeFilter($event);
        $this->Auth->allow('logout');
        $usersTable = TableRegistry::get('Users');
        $usersCNT = $usersTable->find()->where(['status'=>0,'type_user'=>'customer'])->count();
        $this->set('usersCNT',$usersCNT); 
        $permissionsList = array(); 
        $indexPermission = array();
            $user = $this->request->session()->read('Auth.User');

            if($user)
            {
                    

                    $permissionsList = $this->isAuthorized($user); 
                    
                    $requestIngController = $this->request->params['controller'];
                    $action = $this->request->params['action'];    
                    if($action!="logout")
                    {   
                        if(isset($permissionsList['admin']) && $permissionsList['admin'] == 1){
                           $this->set('permissionsList',$permissionsList);   
                        }elseif(in_array($requestIngController,$permissionsList['controller'])){

                            foreach ($permissionsList['controller'] as $k => $va) {
                             
                                    array_push($indexPermission,$va);
                                }
                                   
                           $this->set('permissionsList',$indexPermission); 
                        
                        }else{
                                
                                   $this->redirect(array('controller'=>$permissionsList['controller'][0],'action'=>'add'));
                        }
                    }    
            }else{
                
                     if (!$this->Auth->user()) {

                        }
              
            }
         
    } 
    public function beforeRender(Event $event)
    {   if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
