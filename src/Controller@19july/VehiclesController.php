<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Orm\TableRegistry;

/**
 * Vehicles Controller
 *
 * @property \App\Model\Table\VehiclesTable $Vehicles
 *
 * @method \App\Model\Entity\Vehicle[] paginate($object = null, array $settings = [])
 */
class VehiclesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        return $this->redirect(array('Controller'=>'VehiclesController','action'=>'add'));
        /*$vehicles = $this->paginate($this->Vehicles);

        $this->set(compact('vehicles'));
        $this->set('_serialize', ['vehicles']);*/
    }

    /**
     * View method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vehicle = $this->Vehicles->get($id, [
            'contain' => ['DriverVehicles']
        ]);

        $this->set('vehicle', $vehicle);
        $this->set('_serialize', ['vehicle']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    private function vehiclereadyExist( $vehicl_no ){

            $user = TableRegistry::get('Vehicles');
                    $isExist = $user->find()->where(['vehicle_no'=>$vehicl_no])->count();
                    if($isExist){
                        return true;
                    }return false; 
    }
    private function vehiclereadyExistWithDriver($user_id){

         $user = TableRegistry::get('DriverVehicles');
                    $isExist = $user->find()->where(['user_id'=>$user_id])->count();
                    if($isExist){
                        return true;
                    }return false; 

    }
   
    private function validateAddVehicle( $data ){
            
             $error = array();
             if( ! isset( $data['vehicle_no'] ) || empty( $data['vehicle_no'] ) ){
                 $error['vehicle_no'] = 'Vehicle No Can not be empty';
             }else if( ! isset( $data['user_id'] ) || empty( $data['user_id'] ) ){
                 $error['vehicle_no'] = 'Please select the driver';
             }else if( preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['vehicle_no'])){
                $error['vehicle_no'] = 'Invalid vehicle No.';
             }else if( $this->vehiclereadyExist( $data['vehicle_no'] ) ){
                $error['vehicle_no'] = 'Vehicle no already Exist';               
             }else if( $this->vehiclereadyExistWithDriver( $data['user_id'] ) ){
                $error['vehicle_no'] = 'This Driver already assigned to another Vehicle';               
             }

              if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
              return $error;
    }
  public function add(){


    $error = array();
        if ($this->request->is(['patch', 'post', 'put'])) { 
           $this->request->data['vehicle_no'] = strtoupper(trim($this->request->getData('vehicle_no'))); 
          $error = $this->validateAddVehicle($this->request->getData());

            if($error['statuscode'] == 200)
            {
                  

                  $vehiclesTable = TableRegistry::get('Vehicles');
                  $vehicles = $vehiclesTable->newEntity();
                  $vehicles->vehicle_no = $this->request->getData('vehicle_no');
                  $res = $vehiclesTable->save($vehicles);
                  if($res){
                    $driverVehicleTable = TableRegistry::get('DriverVehicles');
                    $driverVehicle = $driverVehicleTable->newEntity();
                    $driverVehicle->vehicle_id = $res->id;
                    $driverVehicle->user_id = $this->request->getData('user_id');
                    if($driverVehicleTable->save($driverVehicle)){
                         $this->Flash->success(__('Vehicles has been Added successfully')); 
                          return $this->redirect(['action' => 'lists']);
                    }else{
                         $this->Flash->error(__('Something Went Wrong')); 
                         return $this->redirect(['action' => 'lists']);
                    }

                  }else{
                     $this->Flash->error(__('Something Went Wrong')); 
                         return $this->redirect(['action' => 'lists']);
                  }


            }else{
                 $this->set('error',$error);
                 /*$this->request->data['id'] = $this->request->getData('vehicle_id');
                 $this->set('data',$this->request->getData());
 
                 $Unit = $this->Vehicles->get($this->request->getData('id'), [
                    'contain' => ['DriverVehicles' => function ($q) {
                                        return $q
                                            ->select(['DriverVehicles.id'])
                                            ->select(['DriverVehicles.user_id'])
                                            ->contain(['Users']);
                                    }]
                ]);    
                
                $this->set('data',$Unit);*/
                $this->set('data',$this->request->getData());
                $this->set('driver',$this->getAllDriverList());  
            } 
        }
        else{ 
               /* $id = base64_decode($id); 
                $Unit = $this->Vehicles->get($id, [
                    'contain' => ['DriverVehicles' => function ($q) {
                                        return $q
                                            ->select(['DriverVehicles.id'])
                                            ->select(['DriverVehicles.user_id'])
                                            ->contain(['Users']);
                                    }]
                ]);    
                
                $this->set('data',$Unit);*/
                $this->set('driver',$this->getAllDriverList()); 
      }


  }

    public function lists()
    {


        
        if ($this->request->is('post')) {
            

                    $error = array();
                    $this->request->data['vehicle_no'] = strtolower(trim($this->request->getData('vehicle_no')));
                    $error = $this->validateAddVehicle($this->request->getData());
                    if($error['statuscode'] == 200)
                    {
                            $vehicle = $this->Vehicles->newEntity();

                            $vehicle = $this->Vehicles->patchEntity($vehicle, $this->request->getData());
                            if ($this->Vehicles->save($vehicle)) {
                                $this->Flash->success(__('The vehicle has been saved.'));

                                return $this->redirect(['action' => 'add']);
                            }
                            $this->Flash->error(__('The vehicle could not be saved. Please, try again.'));
                    }else{
                         $this->set('error',$error);
                         $this->paginate = [
                                   'limit'=>10,
                                    'contain' => [
                                                    'DriverVehicles' => function ($q) {
                                                        return $q
                                                            ->select(['DriverVehicles.id'])
                                                            ->select(['DriverVehicles.user_id'])
                                                            ->contain(['Users']);
                                                    }
                                                ]
                            ];

                            $vehicle = $this->paginate($this->Vehicles)->toArray();
                            $this->set('data',$this->request->getData());
                            $this->set('vehicle', $vehicle);
                    }       
                





        }else{

            $this->paginate = [
                   'limit'=>10,
                   'order'=>['Vehicles.id DESC'],
                   'conditions'=>['Vehicles.status'=>1],
                    'contain' => [
                                    'DriverVehicles' => function ($q) {
                                        return $q
                                            ->select(['DriverVehicles.id'])
                                            ->select(['DriverVehicles.user_id'])
                                            ->contain(['Users']);
                                    }
                                ]
            ];

            $vehicle = $this->paginate($this->Vehicles)->toArray();
             
            
            $this->set('vehicle', $vehicle);
        }
        


    }

    /**
     * Edit method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    private function getDriverNotThis($vehicle_id, $driver_id){

             $untTable = TableRegistry::get('DriverVehicles');
             $singleUnit = $untTable->find()
                                    ->where(['user_id'=>$driver_id])
                                    ->andwhere(['vehicle_id <>'=>$vehicle_id])
                                    ->count();
                                    
             return $singleUnit;
             

 }


 private function vehiclereadyExistNotThis( $ID, $vehicl_no ){
            

            $user = TableRegistry::get('Vehicles');
                    $isExist = $user->find()->where(['vehicle_no'=>$vehicl_no,'id <>'=>$ID])->count();
                    if($isExist){
                        return true;
                    }return false; 
    }











   private function validateEditVehicle( $data ){


     $error = array();
                  $data['vehicle_no'] = trim($data['vehicle_no']);
                  if( ! isset( $data['vehicle_no'] ) || empty( $data['vehicle_no'] ) ) {
                     $error['vehicle_no'] = 'Please enter the vehicle number';
                  }else if($this->vehiclereadyExistNotThis($data['vehicle_id'],$data['vehicle_no'])){
                    $error['vehicle_no'] = 'Vehicle No. already exist.Please add another No.'; 
                  }
                  else if( $this->getDriverNotThis( $data['vehicle_id'] , $data['user_id'] ) ){
                    $error['vehicle_no'] = 'Driver Has already assigned to another Vehicle';
                  }
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
       return $error;

        

   } 
   private function getAllDriverList(){

    $driverTable = TableRegistry::get('Users');
    $driver = $driverTable->find('list')->where(['type_user'=>'driver'])->hydrate(false)->toArray();
    
    return $driver;

   } 
    public function edit($id = null)
    {
       
      $error = array();
        if ($this->request->is(['patch', 'post', 'put'])) {
           $this->request->data['vehicle_no'] = strtoupper(trim($this->request->getData('vehicle_no'))); 
          $error = $this->validateEditVehicle($this->request->getData());
            if($error['statuscode'] == 200)
            {
                $this->request->data['id'] = $this->request->getData('vehicle_id');
                 
                $unit = $this->Vehicles->get($this->request->getData('id'));
                $unit = $this->Vehicles->patchEntity($unit, $this->request->getData());
                if ($this->Vehicles->save($unit)) { 
                $articlesTable = TableRegistry::get('DriverVehicles');
                $flag = $this->request->getData('driver_vehicle_id');
                if( $flag )
                {
                  $article = $articlesTable->get($this->request->getData('driver_vehicle_id')); 
                 }else{
                    $article = $articlesTable->newEntity();
                 }
                 
                $article->user_id = $this->request->getData('user_id');
                $article->vehicle_id = $this->request->getData('vehicle_id');
                $articlesTable->save($article);
                $this->Flash->success(__('Vehicles has been Updated successfully')); 
                return $this->redirect(['action' => 'lists']);
               }
            }else{
                 $this->set('error',$error);
                 $this->request->data['id'] = $this->request->getData('vehicle_id');
                 $this->set('data',$this->request->getData());
 
                 $Unit = $this->Vehicles->get($this->request->getData('id'), [
                    'contain' => ['DriverVehicles' => function ($q) {
                                        return $q
                                            ->select(['DriverVehicles.id'])
                                            ->select(['DriverVehicles.user_id'])
                                            ->contain(['Users']);
                                    }]
                ]);    
                
                $this->set('data',$Unit);
                $this->set('driver',$this->getAllDriverList());  
            } 
        }
        else{ 
                $id = base64_decode($id); 
                $Unit = $this->Vehicles->get($id, [
                    'contain' => ['DriverVehicles' => function ($q) {
                                        return $q
                                            ->select(['DriverVehicles.id'])
                                            ->select(['DriverVehicles.user_id'])
                                            ->contain(['Users']);
                                    }]
                ]);    
                
                $this->set('data',$Unit);
                $this->set('driver',$this->getAllDriverList()); 
      }


    }

















    /**
     * Delete method
     *
     * @param string|null $id Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*$this->request->allowMethod(['post', 'delete']);*/
        $vehicle = $this->Vehicles->get(base64_decode($id));
        $vehicle->status = 0;
        if ($this->Vehicles->save($vehicle)) {
            $this->Flash->success(__('The vehicle has been deleted.'));
        } else {
            $this->Flash->error(__('The vehicle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'lists']);
    }
}
