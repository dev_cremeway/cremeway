<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[] paginate($object = null, array $settings = [])
 */
class RoutesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Categories', 'Units'],
            'limit'=>10
        ];
        $products = $this->paginate($this->Products);
        $this->set(compact('products'));*/
       
    }
     private function getroute( $name ){

            $areaTable = TableRegistry::get('Routes');
               $areaList = $areaTable->find('all')
                                     ->where(['name'=>$name])
                                     ->hydrate(false)
                                     ->toArray();
                                     if(count($areaList)>0){
                                        return true;
                                     }return false;


    }
    private function routeAlreadyAssignedWithSameTime($region_area,$deliverytiming){

           
          $routeCustomerTable = TableRegistry::get('RouteCustomers');
          
           foreach ($region_area as $key => $value) {

                  $region_area = explode('_', $value);
                  $region = $region_area[0];
                  $area = $region_area[1];
             
                 foreach ($deliverytiming as $k => $v) {
                    
                    $routeCustomer = $routeCustomerTable->find()->where(['region_id'=>$region,'area_id'=>$area,'delivery_schdule_id'=>$v])->count();
                     if($routeCustomer > 0 ){
                      return true;
                     }
                 
                }
                return false;
          }   

    }
    private function validateAddRoutes( $data ){

             $error = array();
                  $data['name'] = trim($data['name']);
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the Route name';
                  }else if( $this->getroute( trim( $data['name'] ) ) ){
                    $error['name'] = 'Route Name already exist';
                  }/*else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Route  name';
                  }*/else if( ! isset( $data['driver_id'] ) || empty( $data['driver_id'] ) ){
                     $error['name'] = 'Please select the driver';
                  }else if( ! isset( $data['deliverytiming'] ) || empty( $data['deliverytiming'] ) ){
                     $error['name'] = 'Please select Delivery Timing';
                  } else if( ! isset( $data['region_area'] ) || empty( $data['region_area'] ) ){
                     $error['name'] = 'Please fill delivery plcae values are properly';
                  }else if($this->routeAlreadyAssignedWithSameTime($data['region_area'],$data['deliverytiming'])){
                     $error['name'] = 'At this time this Region Area Route Assigned to other driver';
                  }
                  
                   
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                     $error['statuscode'] = 200;
                  }
         return $error;
   
    }

    private function getAllCustomer( $regionID, $areaID ){

         $usersTable = TableRegistry::get('Users');
         $subscriptionTable = TableRegistry::get('UserSubscriptions');
         $users = $usersTable->find('all')->select(['id'])->where(['region_id'=>$regionID,'area_id'=>$areaID,'type_user'=>'customer'])->hydrate(false)->toArray();
         $userHaveSubscriptionForThis = array(); 
         if(count($users)>0){

                   foreach ($users as $key => $value) {
                         
                        $subscription  = $subscriptionTable->find()->where(['user_id'=>$value['id']])->count();
                        if($subscription){
                                
                                 $ids = array();
                                 $ids['id'] = $value;
                                 $userHaveSubscriptionForThis[] = $value;                             

                        }
                   }
                  

         }


        return $userHaveSubscriptionForThis;  

    }

    private function SaverouteCustomer( $data, $routeID ){

                $routeCustomerTable = TableRegistry::get('RouteCustomers');
                foreach ($data['region_area'] as $key => $value) {
                              $region_area = explode('_', $value);
                              $region = $region_area[0];
                              $area = $region_area[1];
                              $getAllCustomer = $this->getAllCustomer($region,$area);
                              if(count($getAllCustomer)>0){
                                     foreach ($getAllCustomer as $k => $v) {
                                         foreach ($data['deliverytiming'] as $ke => $val) {
                                          $routeCustomer = $routeCustomerTable->newEntity();
                                          $routeCustomer->user_id = $v['id'];
                                          $routeCustomer->route_id = $routeID;
                                          $routeCustomer->delivery_schdule_id  = $val;
                                          $routeCustomer->position  = '';
                                          $routeCustomer->date  = '';
                                          $routeCustomer->region_id  = $region;
                                          $routeCustomer->area_id  = $area;
                                          $routeCustomer->status  = 0;
                                          //pr($routeCustomer);die;
                                          $routeCustomerTable->save($routeCustomer);
                                         }
                                     }
                              }else{
                                 
                                          foreach ($data['deliverytiming'] as $ke => $val) {
                                          $routeCustomer = $routeCustomerTable->newEntity();
                                          //$routeCustomer->user_id = $v['id'];
                                          $routeCustomer->route_id = $routeID;
                                          $routeCustomer->delivery_schdule_id  = $val;
                                          $routeCustomer->position  = '';
                                          $routeCustomer->date  = '';
                                          $routeCustomer->region_id  = $region;
                                          $routeCustomer->area_id  = $area;
                                          $routeCustomer->status  = 0;
                                          //pr($routeCustomer);die;
                                          $routeCustomerTable->save($routeCustomer);
                                         }
                              }
                }


   return true;

    }
   public function lists(){
           $driverroutesTable = TableRegistry::get('DriverRoutes'); 

           $this->paginate = [
                                   'limit'=>10,
                                    'contain' => [
                                                     'Routes',
                                                     'Users'
                                                ],
                                                'order'=>['Routes.id'=>'DESC'],
                                                'group'=>['DriverRoutes.user_id','DriverRoutes.route_id']
                            ];

          $routes = $this->paginate($driverroutesTable)->toArray();
          $this->set(compact('routes')); 

    }
    public function add(){

      

       if ($this->request->is('post')) {

         $data = $this->request->getData();
         
         

          $error = $this->validateAddRoutes($this->request->getData());
          if($error['statuscode'] == 200)
            {  
                  
                   $routesTable = TableRegistry::get('Routes');
                   $routes = $routesTable->newEntity();
                   $routes->name = trim($data['name']);
                   $routes->created = date('Y-m-d');
                   $result = $routesTable->save($routes);
                   $globalRes = '';
                   if($result->id){
                           
                           $driveroutesTable = TableRegistry::get('DriverRoutes');
                           
                           foreach ($data['deliverytiming'] as $key => $value) {

                           $driverroutes = $driveroutesTable->newEntity();
                           $driverroutes->route_id = $result->id;
                           $driverroutes->user_id = $data['driver_id'];
                           $driverroutes->tracking = '';
                           $driverroutes->delivery_schdule_id = $value;
                           $globalRes = $driveroutesTable->save($driverroutes);
                           }

                          if($globalRes){

                               /*-----save data in route customer table -----*/
                               $saved = $this->SaverouteCustomer($data,$result->id);
                               if($saved){
                                  
                                  $error['statuscode'] = 200;
                                  $error['name'] = "Saved successfully";
                                  echo json_encode($error);die; 

                               }else{
                                  
                                  $error['statuscode'] = 201;
                                  $error['name'] = "Please try again Something went wrong.";
                                  echo json_encode($error);die; 
                               }
                               /*-----save data in route customer table -----*/


                          }else{

                            $error['statuscode'] = 201;
                            $error['name'] = "Please try again Something went wrong.";
                            echo json_encode($error);die; 
                          } 
                   }else{

                    $error['statuscode'] = 201;
                    $error['name'] = "Please try again Something went wrong.";
                    echo json_encode($error);die; 
                   }
            }else{
             echo json_encode($error);die;
            }
          }else{
            $driverTable = TableRegistry::get('Users');
            $regionTable = TableRegistry::get('Regions');
            $drivers = $driverTable->find('list')->where(['type_user'=>'driver'])->hydrate(false)->toArray();
            $region_area_list = $regionTable->find()
                                        ->contain(['areas'])
                                        ->hydrate(false)
                                        /*->join([
                                        'table' => 'areas',
                                        'alias' => 'areas',
                                        'type' => 'INNER',
                                        'conditions' => 'Regions.id = areas.region_id',])*/
                                        ->toArray();
                                        /*pr($region_area_list);die;*/
            $DeliverySchdulesTables = TableRegistry::get('DeliverySchdules');
            $DeliverySchdules = $DeliverySchdulesTables->find('list');
            $routeCustomerTable = TableRegistry::get('RouteCustomers');
            $driverroutesTable = TableRegistry::get('DriverRoutes');
            

            $routes = $driverroutesTable->find('all')->contain([
                                       'Routes',
                                       'Users'
                                       ])->order(['Routes.id'=>'DESC'])->group(['DriverRoutes.user_id'])->hydrate(false)->toArray();
   
            $this->set(compact('routes','DeliverySchdules','drivers','region_area_list'));

          }
        } 

        public function order($id=NULL){

            if(isset($id)){

                     
                      $route_id = base64_decode($id);
                      $routeCustomerTable = TableRegistry::get('RouteCustomers');
                      $routeCustomer = $routeCustomerTable->find('all')->contain([
                        'Users',
                        'Regions',
                        'Areas'
                        ])->where([
                        'route_id'=>$route_id,
                        'user_id IS NOT NULL'
                        ])->group([
                         'RouteCustomers.user_id'
                        ])->order(['RouteCustomers.position'=>'ASC'])->hydrate(false)->toArray();
                       $this->set('routeCustomer',$routeCustomer); 

            }
            else if($this->request->is('ajax')){
              $data = $_POST;
              if(count($data)>0){
                         $routeCustomerTable = TableRegistry::get('RouteCustomers');
                          foreach ($data['user'] as $key => $value) {
                            $query = $routeCustomerTable->query();
                            $result = $query->update()
                              ->set(['position' => $key+1])
                              ->where(['user_id' => $value])
                              ->execute();

                          }
                  echo "Success";die;         
                     
              }else{
                echo "Error";die;
              } 
            }
            else{
              die('Something Went Wrong');
            }             



        }  





 





   
}
