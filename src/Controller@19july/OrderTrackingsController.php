<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrderTrackings Controller
 *
 * @property \App\Model\Table\OrderTrackingsTable $OrderTrackings
 *
 * @method \App\Model\Entity\OrderTracking[] paginate($object = null, array $settings = [])
 */
class OrderTrackingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $orderTrackings = $this->paginate($this->OrderTrackings);

        $this->set(compact('orderTrackings'));
        $this->set('_serialize', ['orderTrackings']);
    }

    /**
     * View method
     *
     * @param string|null $id Order Tracking id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $orderTracking = $this->OrderTrackings->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('orderTracking', $orderTracking);
        $this->set('_serialize', ['orderTracking']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderTracking = $this->OrderTrackings->newEntity();
        if ($this->request->is('post')) {
            $orderTracking = $this->OrderTrackings->patchEntity($orderTracking, $this->request->getData());
            if ($this->OrderTrackings->save($orderTracking)) {
                $this->Flash->success(__('The order tracking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order tracking could not be saved. Please, try again.'));
        }
        $users = $this->OrderTrackings->Users->find('list', ['limit' => 200]);
        $this->set(compact('orderTracking', 'users'));
        $this->set('_serialize', ['orderTracking']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Tracking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderTracking = $this->OrderTrackings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderTracking = $this->OrderTrackings->patchEntity($orderTracking, $this->request->getData());
            if ($this->OrderTrackings->save($orderTracking)) {
                $this->Flash->success(__('The order tracking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order tracking could not be saved. Please, try again.'));
        }
        $users = $this->OrderTrackings->Users->find('list', ['limit' => 200]);
        $this->set(compact('orderTracking', 'users'));
        $this->set('_serialize', ['orderTracking']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Tracking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderTracking = $this->OrderTrackings->get($id);
        if ($this->OrderTrackings->delete($orderTracking)) {
            $this->Flash->success(__('The order tracking has been deleted.'));
        } else {
            $this->Flash->error(__('The order tracking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
