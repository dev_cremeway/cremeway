<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Containers Controller
 *
 * @property \App\Model\Table\ContainersTable $Containers
 *
 * @method \App\Model\Entity\Container[] paginate($object = null, array $settings = [])
 */
class ContainersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $containers = $this->paginate($this->Containers);

        $this->set(compact('containers'));
        $this->set('_serialize', ['containers']);
    }
     /**
     * View method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $container = $this->Containers->get($id, [
            'contain' => ['UserContainers']
        ]);

        $this->set('container', $container);
        $this->set('_serialize', ['container']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    private function getAllContainers(){
          
           $unitTable = TableRegistry::get('Containers');
           $unitList = $unitTable->find('all')
                                       ->hydrate(false)
                                       ->toArray();

           return $unitList;
   }
    private function getAllUnits(){
          
           $unitTable = TableRegistry::get('Units');
           $unitList = $unitTable->find('list')
                                       ->hydrate(false)
                                       ->toArray();
 
           return $unitList;
   }
   private function getContainerWithThisunit($capacity, $unitId){


     $untTable = TableRegistry::get('Containers');
             $singleUnit = $untTable->find()
                                    ->where(['capacity'=>$capacity])
                                    ->andwhere(['unit_id'=>$unitId])
                                    ->count();
                                     
             return $singleUnit;
             


   }

   private  function validateaddContainers( $data ){

                $error = array();
                      
                      if( ! isset( $data['capacity'] ) || empty( $data['capacity'] ) ) {
                         $error['capacity'] = 'Please enter the capacity';
                      }else if( $this->getContainerWithThisunit( trim( $data['capacity'] ), $data['unit_id'] ) ){
                        $error['capacity'] = 'capacity with this Unit already taken';
                      }else if( ! preg_match('/^[0-9]{1,7}(?:\.[0-9]{1,2})?$/',$data['capacity']))
                        {
                           $error['capacity'] = 'Invalid capacity value';
                        }
                      if( count( $error ) > 0 ){
                        $error['statuscode'] = 201;
                      }else{
                        $error['statuscode'] = 200;
                      }
                return $error;
                  

   }  
    public function add()
    {

        
    // $this->set('containers',$this->getAllContainers());
     
     $unitTable = TableRegistry::get('Containers');
           $unitList = $unitTable->find('all');
                                       
                                       

      
      $this->paginate = [
            'limit' => 10
        ];
        $containers = $this->paginate($unitTable)->toArray();
        $this->set(compact('containers'));                                 


















     /*$this->set('unit',$this->getAllUnits());*/
      $unitTable = TableRegistry::get('Units');
      $unitList = $unitTable->find('list')->where(['name'=>'ltr'])
                                       ->hydrate(false)
                                       ->toArray();
       $this->set('unit',$unitList);
        $error = array();
        if ($this->request->is('post')) {
              
            $error = $this->validateaddContainers($this->request->getData());
            if($error['statuscode'] == 200)
            {
                $unit = $this->Containers->newEntity();
                $this->request->data['created'] = date('y-m-d h:i:s');
                $this->request->data['modified'] = date('y-m-d h:i:s');
                $unit = $this->Containers->patchEntity($unit, $this->request->getData());
                if ($this->Containers->save($unit)) {
                    $this->Flash->success(__('Containers has been Added successfully')); 
                    return $this->redirect(['action' => 'add']);
                }
              
            }else{
                 $this->set('error',$error);
                 $this->set('data',$this->request->getData());
            }


        }
    

    }

    /**
     * Edit method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */


    private function getContainerNotThis( $capacity, $unitId, $ID ){


          $untTable = TableRegistry::get('Containers');
             $singleUnit = $untTable->find()
                                    ->where(['capacity'=>$capacity])
                                    ->andwhere(['unit_id'=>$unitId])
                                    ->andwhere(['id <>'=>$ID])
                                    ->count();
                                    
             return $singleUnit;
             
           
    }

    private function validateEditContainers( $data ){
           

                $error = array();
                      $data['capacity'] = trim($data['capacity']);
                      if( ! isset( $data['capacity'] ) || empty( $data['capacity'] ) ) {
                         $error['capacity'] = 'Please enter the capacity';
                      }else if( $this->getContainerNotThis( trim( $data['capacity'] ), $data['unit_id'], $data['id'] ) ){
                        $error['capacity'] = 'capacity name already taken with this unit';
                      }else if( ! preg_match('/^[0-9]{1,7}(?:\.[0-9]{1,2})?$/',$data['capacity'])){
                       $error['capacity'] = 'Invalid capacity value';
                      }else if( empty($data['unit_id']) ){
                      $error['name'] = 'Please select Unit name';
                      }if( count( $error ) > 0 ){
                        $error['statuscode'] = 201;
                      }else{
                        $error['statuscode'] = 200;
                      }
           return $error;
    }
    public function edit($id = null)
    {
                
                  
                    if ($this->request->is(['patch', 'post', 'put'])) {
                       

                         $error = $this->validateEditContainers($this->request->getData());
                         
                       if($error['statuscode'] == 200)
                       { 
                         $container = $this->Containers->get($this->request->getData('id')); 
                         $container = $this->Containers->patchEntity($container, $this->request->getData());
                      
                        if ($this->Containers->save($container)) {
                            $this->Flash->success(__('The container has been Update successfully.'));

                            return $this->redirect(['action' => 'add']);
                        }
                        $this->Flash->error(__('The container could not be saved. Please, try again.'));
                    }else{

                             
                             $this->set('unit',$this->getAllUnits());   
                             $this->set('error',$error);
                             $this->set('data',$this->request->getData());
                    }
                    

                    }else{

                           /* $this->set('unit',$this->getAllUnits());    */

                           $unitTable = TableRegistry::get('Units');
                                 $unitList = $unitTable->find('list')->where(['name'=>'ltr'])
                                       ->hydrate(false)
                                       ->toArray();
                            
                            $this->set('unit',$unitList);


                            $id = base64_decode($id);
                            $Unit = $this->Containers->get($id, [
                                'contain' => []
                            ]);
                               
                            $this->set('data',$Unit);
                    }
                 
    


    }

    /**
     * Delete method
     *
     * @param string|null $id Container id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $container = $this->Containers->get($id);
        if ($this->Containers->delete($container)) {
            $this->Flash->success(__('The container has been deleted.'));
        } else {
            $this->Flash->error(__('The container could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function usersContainer(){

         $usercontainers =  TableRegistry::get('UserContainers'); 
         $page = 0; 
         $conditions = ['Users.type_user'=>'customer'];
         $this->paginate = [
                             'limit' => 10,
                             'page'=> $page,
                             'contain' => [
                                    'Users.Regions' => function ($q) {
                                        return $q
                                            ->select(['Regions.name'])
                                            ->contain(['Users']);
                                    },
                                    'Users.Areas' => function($q) {
                                        return $q
                                            ->select(['Areas.name'])
                                            ->contain(['Users']);
                                    }
                                ],
                                'conditions'=>$conditions
                                //'group'=>['user_id']
                        ];
                        $container = $this->paginate($usercontainers)->toArray();
                        //pr($container);die;
                        $this->set('container',$container);

       
    }
}
     