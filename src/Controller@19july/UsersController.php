<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
//use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function initialize()
        {   
            parent::initialize();
            $this->loadComponent('Paginator');
            $this->Auth->allow();
        }


     public function Activate($id){
          
           if($id){
               
                $id = base64_decode($id);



                  
                 $userTable = TableRegistry::get('Users');
                 $modified = date('Y-m-d h:i:s');
                         $query = $userTable->query();
                            $result = $query->update()
                                      ->set(['status' => 1 ,'modified' => $modified])
                                      ->where(['id' => $id])
                                      ->execute();
                      if($result){
                        $this->Flash->success(__('Customer Has beed Activated'));
                        return $this->redirect(['action' => 'view']); 
                      }else{
                         $this->Flash->error(__('Something went wrong. Please, try again.')); 
                      } 

           }else{
           return $this->redirect(['action' => 'view']); 
           }

     }
        
     
       public function login() {
        if ($this->request->is('post')) {


            $username = trim($this->request->getData('username'));
            $password =  trim($this->request->getData('password'));
            $this->request->data['username'] = $username;
            $this->request->data['password'] = $password;
            $user = $this->Auth->identify();
            
            $user = $this->Auth->identify();
             
             if($user)
             {
                      $this->Auth->setUser($user);
                      
                      $permissionsList = $this->isAuthorized($user);
                      //pr($permissionsList);die;

                  if( isset($permissionsList['controller']) && in_array('Users', $permissionsList['controller']))
                  { 
                    
                    return $this->redirect(array('controller'=>$permissionsList['controller'][0],'action'=>'view'));
                  
                  }else if(isset($permissionsList['admin']) && $permissionsList['admin'] == 1){
                     return $this->redirect($this->Auth->redirectUrl());
                  }else if( isset($permissionsList['controller']) && count($permissionsList['controller']) > 0 ){
                    
                    return $this->redirect(array('controller'=>$permissionsList['controller'][0],'action'=>'view'));
                  }else{
                    echo "Please ask to admin about permission";die;
                  }
               }else{
                  $this->Flash->error(__('Your username or password was incorrect.'));
               }       
             
         

           
        }
    }
    
            public function logout() {
            //$this->Flash->success(__('Good-Bye'));
            $this->redirect($this->Auth->logout());
        }       




    public function index()
    { 
      
         
        $this->paginate = [
            'contain' => ['Regions', 'Areas', 'Groups']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    






  public function driverProfile(){
 
        if($_GET){
          $this->set('id',$_GET['id']); 
          $id = base64_decode($_GET['id']);

          







          $driverRouteTable = TableRegistry::get('DriverRoutes');
          $driverRoute = $driverRouteTable->find('all')->contain(['Routes'])->where(['DriverRoutes.user_id'=>$id])->hydrate(false)->toArray();
           
          $routeIds = array();

          foreach ($driverRoute as $key => $value) {
                        
                         array_push($routeIds,$value['route_id']);

           } 
          
          $routeIds = array_unique($routeIds);

          $routeCustomerTable = TableRegistry::get('RouteCustomers');
          $routeCustomer = $routeCustomerTable->find('all')->where(['route_id IN'=>$routeIds])->contain(['DeliverySchdules','Regions','Areas','Routes'])->toArray();
       //  pr($routeCustomer);die;


         $this->set('driverRoute',$routeCustomer);













         $driverVehicleTable = TableRegistry::get('DriverVehicles');
          $driverVehicle = $driverVehicleTable->find('all')->contain(['Vehicles'])->where(['DriverVehicles.user_id'=>$id])->hydrate(false)->toArray();


          $usersTable = TableRegistry::get('Users');
          $users = $usersTable->find()->where(['Users.id'=>$id])->contain(['Regions','Areas'])->first()->toArray(); 
          $this->set('data',$users);
          $this->set('regions',$this->getAllRegionsList());
          $this->set('driverVehicle',$driverVehicle);
          $areaTable = TableRegistry::get('Areas');
          $this->set('areas',$areaTable->find('list')->toArray());


          


        }else{
            return $this->redirect($this->referer);
        }


 }
 public function addBalance(){

      if($_REQUEST){
        $userid = $_REQUEST['notaddidbeforethisusergetfrommakeidmohan'];
        $amount = $_REQUEST['amountnotneedtoberechargeonetime'];
        $UserBalancesTable = TableRegistry::get('UserBalances');
        $UserBalances = $UserBalancesTable->find()->where(['user_id'=>$userid])->select('balance')->first();
        $balanceamount = $UserBalances['balance'];
        $balanceamount = $balanceamount + $amount;
         
         $query = $UserBalancesTable->query();
         $result = $query->update()
                  ->set(['balance' => $balanceamount])
                  ->where(['user_id' => $userid])
                  ->execute();
        $transactionTable = TableRegistry::get('Transactions');
        
        $transaction = $transactionTable->newEntity();
                    $transaction->user_id = $userid;
                    $transaction->amount = $amount;
                    $transaction->transaction_amount_type = 'Cr';
                    $transaction->created = date('Y-m-d');
                    $date = new \DateTime();
                    $transaction->time = date_format($date, 'H:i:s');  
                    $transaction->transaction_type = 'Recharge By Admin';
                    if($transactionTable->save($transaction)){

                      echo "200";die;
                     }else{
                      echo "202";die;
                     }          


      }else{
        echo "202";die;
      } 
 }


 public function usersBalance(){

    if($_GET){
          $this->set('id',$_GET['id']); 
          $id = base64_decode($_GET['id']);
          $this->set('user_customer_id',$id);
          $usersBalanceTable = TableRegistry::get('UserBalances');
          $UsersTable = TableRegistry::get('Users');
          $usersBalance = $usersBalanceTable->find('all')->where(['user_id'=>$id])->select(['balance'])->first();
          $this->set('usersBalance',$usersBalance);

          $users = $UsersTable->find()->contain(['Regions','Areas','UserBalances'])->where(['Users.id'=>$id])->first()->toArray();
            $this->set('data',$users);

          $transactionTable = TableRegistry::get('Transactions');
          $transaction = $transactionTable->find('all')->contain(['DeliverySchdules'])->order(['Transactions.id DESC'])->where(['user_id'=>$id])->toArray();
          //pr($transaction);die;
          $this->set('transaction',$transaction);








        }else{
            return $this->redirect($this->referer);
        }

 }

 public function usersContainers(){

    if($_GET){
          $this->set('id',$_GET['id']); 
          $id = base64_decode($_GET['id']);
          $this->set('user_customer_id',$id);
          $usersBalanceTable = TableRegistry::get('UserBalances');
          $UsersTable = TableRegistry::get('Users');
          $usersBalance = $usersBalanceTable->find('all')->where(['user_id'=>$id])->select(['balance'])->first();
          $this->set('usersBalance',$usersBalance);
 
          $usersContainersTable = TableRegistry::get('UserContainers');
          $usersContainers = $usersContainersTable->find('all')->where(['user_id'=>$id])->toArray();

          $UsersTable = TableRegistry::get('Users');
          $users = $UsersTable->find()->contain(['Regions','Areas','UserBalances'])->where(['Users.id'=>$id])->first()->toArray();
            $this->set('data',$users);
          
          $this->set('container',$usersContainers);
          


        }else{
            return $this->redirect($this->referer);
        }

 }











  public function driver()
    { 


        if($_GET)
            {
                  
                          
                            $condition = ['Users.is_deleted'=>0];
                            $filterby = '';
                            $page = 0;
                            if(isset($_GET['filterby']) && ! empty($_GET['filterby'])){
                                $filterby = $_GET['filterby'];
                                 $condition = ['type_user'=>$_GET['filterby'],'Users.is_deleted'=>0];
                              }

                             if(isset($_GET['page']) && ! empty($_GET['page'])){
                                $page = $_GET['page'];
                              } 
                            $this->paginate = [
                            'limit' => 8,
                            'page'=> $page,
                            'order' => [
                                'Users.id' => 'desc'
                            ],
                            'conditions'=>$condition,
                            'contain' => ['Groups','Regions','Areas']
                        ];
                        $user = $this->paginate($this->Users)->toArray();
                        $this->set('user', $user);
                        $this->set('filterby',$filterby); 


            }else{
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'Users.id' => 'desc'
                ],
                'contain' => ['Groups','Regions','Areas'],
                'conditions'=>['Users.type_user'=>'driver','Users.is_deleted'=>0]
            ];
            $user = $this->paginate($this->Users)->toArray();
            
            $this->set('user', $user);
            } 
        
    }


 public function admin()
    { 
        $groupTable = TableRegistry::get('Groups');
          if($_GET)
            {
                  
                          
                            $condition = [];
                            
                            $page = 0;
                            if(isset($_GET['role_id']) && ! empty($_GET['role_id'])){
                               
                                 $condition = ['group_id'=>$_GET['role_id'],'Users.type_user'=>'admin'];
                              }

                             if(isset($_GET['page']) && ! empty($_GET['page'])){
                                $page = $_GET['page'];
                              } 
                            $this->paginate = [
                            'limit' => 10,
                            'page'=> $page,
                            'order' => [
                                'Users.id' => 'desc'
                            ],
                            'conditions'=>$condition
                            
                        ];
                        $user = $this->paginate($this->Users)->toArray();
                        $this->set('user', $user);
                        $this->set('role_id',$_GET['role_id']); 
                        $groups = $groupTable->find('list')->toArray(); 
                        $this->set('groups',$groups);


            }else{
            $this->paginate = [
                'limit' => 10,
                'order' => [
                    'Users.id' => 'desc'
                ],
                'conditions'=>['Users.type_user'=>'admin']
            ];
            $user = $this->paginate($this->Users)->toArray();
            $this->set('user', $user);
            
            $groups = $groupTable->find('list')->toArray(); 
            $this->set('groups',$groups);

            } 
        
    }




 private function getAllRegionsList(){
         
          $areaTable = TableRegistry::get('Regions');
          $regions = $areaTable->find('list')->hydrate(false)->toArray();
          return $regions;
    }




    public function view()
    { 


        if($_GET)
            {


              
                   
                       
                            $condition = ['Users.is_deleted'=>0];
                            $filterby = '';
                            $page = 0;

                            if(isset($_GET['page']) && ! empty($_GET['page'])){
                            $page = $_GET['page'];
                          } 


                            $this->paginate = [
                                        'limit' => 8,
                                        'page'=> $page,
                                        'order' => [
                                            'Users.id' => 'desc'
                                        ],
                                        'conditions'=>$condition,
                                        'contain' => ['Groups','Regions','Areas']
                                    ]; 








                            if(
                                    (isset($_GET['filterby']) && ! empty($_GET['filterby']))
                                    ||
                                    (isset($_GET['region_id']) && ! empty($_GET['area_id'])) 
                              ){
                                
                                          

                                 if(isset($_GET['filterby']) && ! empty($_GET['filterby']))
                                 {
                                          $filterby = $_GET['filterby'];
                                          
                                          if($filterby == 'active'){
                                           $condition = ['Users.status'=>1,'Users.type_user'=>'customer','Users.is_deleted'=>0];
                                          }
                                          if($filterby == 'deactive'){
                                            $condition = ['Users.status'=>0,'Users.type_user'=>'customer','Users.is_deleted'=>0];
                                          }

                                           
                                              $this->paginate = [
                                              'limit' => 8,
                                              'page'=> $page,
                                              'order' => [
                                                  'Users.id' => 'desc'
                                              ],
                                              'conditions'=>$condition,
                                              'contain' => ['Groups','Regions','Areas']
                                          ]; 


                                   }     

                                         



                               if(isset($_GET['region_id']) && isset($_GET['area_id'])){

                               $area_id = $_GET['area_id'];
                               $region_id = $_GET['region_id'];
                               $condition = ['Users.status'=>1,'Users.type_user'=>'customer','Users.is_deleted'=>0];
                               $this->paginate = [
                                              'limit' => 8,
                                              'page'=> $page,
                                              'order' => [
                                                  'Users.id' => 'desc'
                                              ],
                                              'conditions'=>$condition,
                                              'contain'=>[
                                                 'Groups',
                                                 'Regions' => function (\Cake\ORM\Query $query) use($region_id) {
                                                  return $query->where(['Users.region_id' => $region_id]);
                                                 },
                                                  'Areas' => function (\Cake\ORM\Query $query) use($area_id) {
                                                  return $query->where(['Users.area_id' => $area_id]);
                                                 }
                                            
                                            ] 
                                                       
                                          ]; 
                                  

                               } 

                               }

                         



                         

                        $user = $this->paginate($this->Users)->toArray();

                        $this->set('user', $user);
                        $this->set('filterby',$filterby);
                        $this->set('regions',$this->getAllRegionsList());
                        if(isset($area_id)&&isset($region_id))
                        {
                        $this->set('area_id',$area_id);
                        $this->set('region_id',$region_id);
                        $arealist = $this->getRegionsArea($region_id); 
                        $this->set('areas',$arealist);
                        }


            }else{
            $this->paginate = [
                'limit' => 8,
                'order' => [
                    'Users.id' => 'desc'
                ],
                'contain' => ['Groups','Regions','Areas'],
                'conditions'=>['Users.type_user'=>'customer','Users.is_deleted'=>0]
            ];
            $user = $this->paginate($this->Users)->toArray();
            $this->set('regions',$this->getAllRegionsList());
            $this->set('user', $user);
            } 
        
    }
 
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    
     public function getRegionsArea($regionId){
         
         $areaTable = TableRegistry::get('Areas'); 
         $areas = $areaTable->find('list')->where(['region_id'=>$regionId])->hydrate(false)->toArray();
         return $areas;
       }
          
    public function getArea(){
        if($_GET['id'])
        {
         $region_id = $_GET['id'];
         $areas = $this->Users->Areas->find('list')->where(['region_id'=>$region_id])->hydrate(false)->toArray();
         echo json_encode($areas);die; 
        }else{
            echo "Error";die;
        }
    }

    private function usernamealreadyExist( $username ){
               
                $user = TableRegistry::get('Users');
                $isExist = $user->find()->where(['username'=>$username])->count();
                if($isExist){
                    return true;
                }return false; 
    }
    private function mobilealreadyExist( $mobile_no ){
               
                $user = TableRegistry::get('Users');
                $isExist = $user->find()->where(['phoneNo'=>$mobile_no])->count();
                if($isExist){
                    return true;
                }return false; 
    }


    private function validateAddadmin($data){

        $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Name Can not be empty';
        }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
            $error['name'] = 'Please check Name name';
         }else if( ! isset( $data['username'] ) || empty( $data['username'] )) {
            $error['name'] = 'Username can not be empty';
         }else if( $this->usernamealreadyExist( $data['username'] ) ){
            $error['name'] = 'Username already Exist';               
         }else if( ! isset( $data['password'] ) || empty( $data['password'] )) {
            $error['name'] = 'Password can not be empty';
         }else if( ! preg_match('/^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_]).{6,10}$/', $data['password']) ){
            $error['name'] = 'Password must contain at least 8 characters, at least 1 number, letters and special characters';
         }else if( ! isset( $data['phoneNo'] ) || empty( $data['phoneNo'] ) ){
            $error['name'] = 'PhoneNo can not be empty';  
         }else if( ! preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/', $data['phoneNo']) ){
            $error['name'] = 'Invalid Phone No'; 
         }else if( $this->mobilealreadyExist( $data['phoneNo'] ) ){
            $error['name'] = 'Phone No Already Exist';
         }else if( ! isset( $data['houseNo'] ) || empty( $data['houseNo'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['region_id'] ) || empty( $data['region_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['area_id'] ) || empty( $data['area_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['group_id'] ) || empty( $data['group_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }
         
         if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
         return $error;


    }

    /*private function passwordgen($pass){
            $hasher = new DefaultPasswordHasher;
            $password = $hasher->hash(trim($pass));
            return $password;
    }*/

    public function adminadd(){

 

        if ($this->request->is('post')) { 


           $imagename = ''; 
            if($_FILES && $_FILES['image']['error'] == 0){
                  
                   $imagename = $this->uploadImage($_FILES);
                   
                    
                   if($imagename != 1){
                    $this->request->data['image'] = $imagename;
                   }else{
                    $this->request->data['image'] = NULL;
                   }

            }
             
            
           if($imagename != 1)
           {  
            
                    $error = $this->validateAddadmin($this->request->getData());
                   
                    if($error['statuscode'] == 200)
                    {
                            $user = $this->Users->newEntity();
                            $this->request->data['type_user'] = 'admin';  
                            $this->request->data['name'] = trim($this->request->getData('name'));
                            $this->request->data['username'] = trim($this->request->getData('username'));
                            $this->request->data['password'] = md5(trim($this->request->getData('password')));
                            $this->request->data['phoneNo'] = trim($this->request->getData('phoneNo'));
                            $this->request->data['houseNo'] = trim($this->request->getData('houseNo'));

                            $user = $this->Users->patchEntity($user, $this->request->getData());
                             
                            if ($this->Users->save($user)) {
                              
                                $this->Flash->success(__('The Admin has been saved.'));

                                return $this->redirect(['action' => 'admin']);
                            }
                            $this->Flash->error(__('The user could not be saved. Please, try again.'));
                    }else{

                         $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                         $groups = $this->Users->Groups->find('list')->hydrate(false)->toArray();
                         $this->set(compact('regions','groups'));
                         $this->set('error',$error);
                         $this->set('data',$this->request->getData());
                    }
               }else{
                  $error['name'] = 'Invalid Image';
                  $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                  $this->set(compact('regions'));
                  $this->set('error',$error);
                  $this->set('data',$this->request->getData());
             }     




        


        }else{
             $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
             $groups = $this->Users->Groups->find('list')->hydrate(false)->toArray();
             $this->set(compact('regions','groups'));
              
        }

    }







 private function validateAddUser($data){



        $error = array();
        $type = array('customer','driver');
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Name Can not be empty';
        }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
            $error['name'] = 'Please check Name name';
         }else if( ! isset( $data['username'] ) || empty( $data['username'] )) {
            $error['name'] = 'Username can not be empty';
         }else if( $this->usernamealreadyExist( $data['username'] ) ){
            $error['name'] = 'Username already Exist';               
         }else if( ! isset( $data['password'] ) || empty( $data['password'] )) {
            $error['name'] = 'Password can not be empty';
         }else if( ! preg_match('/^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_]).{6,10}$/', $data['password']) ){
            $error['name'] = 'Password must contain at least 8 characters, at least 1 number, letters and special characters';
         }else if( ! isset( $data['phoneNo'] ) || empty( $data['phoneNo'] ) ){
            $error['name'] = 'PhoneNo can not be empty';  
         }else if( ! preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/', $data['phoneNo']) ){
            $error['name'] = 'Invalid Phone No'; 
         }else if( $this->mobilealreadyExist( $data['phoneNo'] ) ){
            $error['name'] = 'Phone No Already Exist';
         }else if( ! isset( $data['houseNo'] ) || empty( $data['houseNo'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['region_id'] ) || empty( $data['region_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['area_id'] ) || empty( $data['area_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }/*else if( ! isset( $data['type_user'] ) || empty( $data['type_user'] ) ){
            $error['name'] = 'Please select either Driver OR Customer';
         }else if( ! in_array($data['type_user'], $type) ){
           $error['name'] = 'Invalid User Type';
         }*/
         
         if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
         return $error;



 }

 function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }

 private function uploadImage( $image ){
            $error = 0;
            if (isset($_FILES["image"])) {
            $tmpFile = $_FILES["image"]["tmp_name"];
            $extension = $this->getExtension($_FILES['image']['name']);
            $tme = time();
            $fileName = $_SERVER['DOCUMENT_ROOT'].'/creameway/webroot/img/images/'.$tme.'.'.$extension;

            $extensionArray = array('jpg','jpeg','png');
            
            list($width, $height) = getimagesize($tmpFile);
             
            if ($width == null && $height == null) {
                $error = 1;
                
            }
             
            if ( ! in_array($extension, $extensionArray) ) {
                
                $error = 1; 
            }
            else {

                if( move_uploaded_file($tmpFile, $fileName) ){
                    $error = HTTP_ROOT.'img/images/'.$tme.'.'.$extension; 
                }
            }
        }
        return $error;
 } 





    /*----------------Add by vipin--------------------------*/
  public function driverAdd(){

     if ($this->request->is('post')) { 
             $imagename = ''; 
            if($_FILES && $_FILES['image']['error'] == 0){
                  
                   $imagename = $this->uploadImage($_FILES);
                    
                   if($imagename != 1){
                    $this->request->data['image'] = $imagename;
                   }else{
                    $this->request->data['image'] = NULL;
                   }

            }
             
            
           if($imagename != 1)
           {
            
                $error = $this->validateAddUser($this->request->getData()); 
                                if($error['statuscode'] == 200)
                                { 
                                        $user = $this->Users->newEntity();  
                                        $this->request->data['name'] = trim($this->request->getData('name'));
                                        $this->request->data['phoneNo'] = trim($this->request->getData('phoneNo'));
                                        $this->request->data['houseNo'] = trim($this->request->getData('houseNo'));
                                        $this->request->data['username'] = trim($this->request->getData('username'));
                                        $this->request->data['password'] = md5(trim($this->request->getData('password')));
                                        $this->request->data['type_user'] = 'driver';
                                        
                                        
                                        $user = $this->Users->patchEntity($user, $this->request->getData());
                                        $resSave = $this->Users->save($user);
                                        if ($resSave) {
                                             
                                            


                                            $this->Flash->success(__('The Driver has been saved.'));

                                            return $this->redirect(['action' => 'driver']);
                                        }
                                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                                 }else{

                                     $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                                     $this->set(compact('regions'));
                                     $this->set('error',$error);
                                     $this->set('data',$this->request->getData());
                                 }

             }else{
                  $error['name'] = 'Invalid Image';
                  $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                  $this->set(compact('regions'));
                  $this->set('error',$error);
                  $this->set('data',$this->request->getData());
             }


        }else{
             $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
             $this->set(compact('regions'));
              
        }


  }
  /*-----------------Date----==============================*/



public function customerProfile(){
 
        if($_GET){
          $this->set('id',$_GET['id']); 
          $id = base64_decode($_GET['id']);
          $this->set('user_customer_id',$id);
          $UsersTable = TableRegistry::get('Users');
          $users = $UsersTable->find()->contain(['Regions','Areas','UserBalances'])->where(['Users.id'=>$id])->first()->toArray();
            $this->set('data',$users);
          $usersBalanceTable = TableRegistry::get('UserBalances');
          $usersBalance = $usersBalanceTable->find('all')->where(['user_id'=>$id])->select(['balance'])->first();
          $this->set('usersBalance',$usersBalance);
          
          $regionTable = TableRegistry::get('Regions');
          $areaTable = TableRegistry::get('Areas');

          $this->set('regions',$regionTable->find('list')->toArray());
          $this->set('areas',$areaTable->find('list')->toArray());
//pr($users);die;
         


        }else{
                

                if($this->request->is('post')){

                        $imagename = ''; 
                        if($_FILES && $_FILES['image']['error'] == 0){
                              
                               $imagename = $this->uploadImage($_FILES);
                                
                               if($imagename != 1){
                                $this->request->data['image'] = $imagename;
                               }else{
                                $this->request->data['image'] = NULL;
                               }

                        }



                    $error = $this->validateEditCustomer($this->request->getData());
                     if($error['statuscode'] == 200)
                      {
                        /*Save code will come*/
                      }else{

                        $this->set('id',$data['id']); 
                        $UsersTable = TableRegistry::get('Users');
                        $users = $UsersTable->find()->contain(['Regions','Areas','UserBalances'])->where(['Users.id'=>$data['id']])->first()->toArray();
                        
                        $regionTable = TableRegistry::get('Regions');
                        $areaTable = TableRegistry::get('Areas');

                        $this->set('regions',$regionTable->find('list')->toArray());
                        $this->set('areas',$areaTable->find('list')->toArray());
               
                        $this->set('data',$users);
                        $this->set('error',$error);
                         
                      } 
                                      





                     

                }else{
                 return $this->redirect($this->referer);
                }


            
        }


 }

 private function usernamealreadyExistEdit($username,$id){

    $userTable = TableRegistry::get('Users');
    $user = $userTable->find()->where(['id <>'=>$id,'username'=>$username])->count();
    if($user){
      return true;
    }return false;

 }

  private function mobilealreadyExistEdit($mobile,$id){

    $userTable = TableRegistry::get('Users');
    $user = $userTable->find()->where(['id <>'=>$id,'phoneNo'=>$mobile])->count();
    if($user){
      return true;
    }return false;

 }



 private function validateEditCustomer($data){



        $error = array();
        $type = array('customer','driver');
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Name Can not be empty';
        }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
            $error['name'] = 'Please check Name name';
         }else if( ! isset( $data['username'] ) || empty( $data['username'] )) {
            $error['name'] = 'Username can not be empty';
         }else if( $this->usernamealreadyExistEdit( $data['username'],$data['id'] ) ){
            $error['name'] = 'Username already Exist';               
         }/*else if( ! isset( $data['password'] ) || empty( $data['password'] )) {
            $error['name'] = 'Password can not be empty';
         }*//*else if( ! preg_match('/^(?=.*\d)(?=.*?[a-zA-Z])(?=.*?[\W_]).{6,10}$/', $data['password']) ){
            $error['name'] = 'Password must contain at least 8 characters, at least 1 number, letters and special characters';
         }*/else if( ! isset( $data['phoneNo'] ) || empty( $data['phoneNo'] ) ){
            $error['name'] = 'PhoneNo can not be empty';  
         }else if( ! preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/', $data['phoneNo']) ){
            $error['name'] = 'Invalid Phone No'; 
         }else if( $this->mobilealreadyExistEdit( $data['phoneNo'],$data['id'] ) ){
            $error['name'] = 'Phone No Already Exist';
         }else if( ! isset( $data['houseNo'] ) || empty( $data['houseNo'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['region_id'] ) || empty( $data['region_id'] ) ){
            $error['name'] = 'House No can not be empty';
         }else if( ! isset( $data['area_id'] ) || empty( $data['area_id'] ) ){
            $error['name'] = 'House No can not be empty';
         } 
         
         if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error['statuscode'] = 200;
                  }
         return $error;



 }









    public function add()
    { 
        if ($this->request->is('post')) { 
             $imagename = ''; 
            if($_FILES && $_FILES['image']['error'] == 0){
                  
                   $imagename = $this->uploadImage($_FILES);
                    
                   if($imagename != 1){
                    $this->request->data['image'] = $imagename;
                   }else{
                    $this->request->data['image'] = NULL;
                   }

            }
             
            
           if($imagename != 1)
           {
            
                $error = $this->validateAddUser($this->request->getData());




                 
                                if($error['statuscode'] == 200)
                                { 
                                        $user = $this->Users->newEntity();  
                                        $this->request->data['name'] = trim($this->request->getData('name'));
                                        $this->request->data['phoneNo'] = trim($this->request->getData('phoneNo'));
                                        $this->request->data['houseNo'] = trim($this->request->getData('houseNo'));
                                        $this->request->data['username'] = trim($this->request->getData('username'));
                                        $this->request->data['password'] = md5(trim($this->request->getData('password')));
                                        $this->request->data['type_user'] = 'customer';
                                        
                                        $normal_vip = $this->request->getData('user_cat_type');
                                        if(isset($normal_vip) && ! empty($normal_vip)){
 
                                          $this->request->data['customer_category'] = trim($this->request->getData('user_cat_type'));  
                                        }else{
                                          $this->request->data['customer_category'] = 'normal';
                                        } 
                                        $this->request->data['status'] = 1;
                                        $user = $this->Users->patchEntity($user, $this->request->getData());
                                        $resSave = $this->Users->save($user);
                                        if ($resSave) {
                                             
                                             $userbalanceTable = TableRegistry::get('UserBalances');
                                             $userbalance = $userbalanceTable->newEntity();
                                             $userbalance->balance = $this->request->getData('amount');
                                             $userbalance->user_id=$resSave->id;
                                             $userbalanceTable->save($userbalance);

                                             /*----Add in Transactions Table--------------*/
                                              $transactionTable = TableRegistry::get('Transactions');
                                              $transaction = $transactionTable->newEntity();
                                              $transaction->user_id = $resSave->id;
                                              $transaction->amount = $this->request->getData('amount');
                                              $transaction->transaction_amount_type = 'Cr';
                                              $transaction->created = date('Y-m-d'); 
                                              $date = new \DateTime();
                                              $transaction->time = date_format($date, 'H:i:s'); 
                                              $transaction->status = 1;
                                              $transaction->transaction_type = 'Recharge By Admin';
                                              $transactionTable->save($transaction);

                                             /*----Add in Transactions Table--------------*/












 
                                            $this->Flash->success(__('The Customer has been saved.Plase add Subscription for this Customer'));
                                            $id = base64_encode($resSave->id);
                                            return $this->redirect(['controller'=>'UserSubscriptions','action' => 'customerSubscription','?'=>['id'=>$id]]);
                                        }
                                        $this->Flash->error(__('The user could not be saved. Please, try again.'));
                                 }else{

                                     $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                                     $this->set(compact('regions'));
                                     $this->set('error',$error);
                                     $this->set('data',$this->request->getData());
                                 }

             }else{
                  $error['name'] = 'Invalid Image';
                  $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
                  $this->set(compact('regions'));
                  $this->set('error',$error);
                  $this->set('data',$this->request->getData());
             }


        }else{
             $regions = $this->Users->Regions->find('list')->hydrate(false)->toArray();
             $this->set(compact('regions'));
              
        }
        

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {   

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $devices = $this->Users->Devices->find('list', ['limit' => 200]);
        $regions = $this->Users->Regions->find('list', ['limit' => 200]);
        $areas = $this->Users->Areas->find('list', ['limit' => 200]);
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'devices', 'regions', 'areas', 'groups', 'userTypes'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null,$isDriver = null)
    {

        /*$this->request->allowMethod(['post', 'delete']);*/
        $user = $this->Users->get(base64_decode($id));
        $user->is_deleted = 1;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        if(isset($isDriver)&&!empty($isDriver)){
          return $this->redirect(['action' => 'driver']);

        }else{    
          return $this->redirect(['action' => 'view']);
         }
    

    }
}
