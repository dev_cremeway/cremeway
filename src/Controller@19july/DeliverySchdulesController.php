<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * DeliverySchdules Controller
 *
 * @property \App\Model\Table\DeliverySchdulesTable $DeliverySchdules
 *
 * @method \App\Model\Entity\DeliverySchdule[] paginate($object = null, array $settings = [])
 */
class DeliverySchdulesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {   
        return $this->redirect(array('Controller'=>'DeliverySchdules','action'=>'add'));
        $deliverySchdules = $this->paginate($this->DeliverySchdules);

        $this->set(compact('deliverySchdules'));
        $this->set('_serialize', ['deliverySchdules']);
    }

    /**
     * View method
     *
     * @param string|null $id Delivery Schdule id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        /*$deliverySchdule = $this->DeliverySchdules->get($id, [
            'contain' => ['Sales']
        ]);

        $this->set('deliverySchdule', $deliverySchdule);
        $this->set('_serialize', ['deliverySchdule']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

   private function getAllSchdule(){
          
           $schduleTable = TableRegistry::get('deliverySchdules');
           $schduleList = $schduleTable->find('all')
                                       ->where(['status'=>1])
                                       ->hydrate(false)
                                       ->order(['id DESC'])
                                       ->toArray();

           return $schduleList;
   }
    
    private function validateAddSchdule( $data ){
         
        $actualStartTime = date("H:i", strtotime($data['start_time']));
        $actualEndTime = date("H:i", strtotime($data['end_time']));
        
        $st_time    =   strtotime($actualStartTime);
        $end_time   =   strtotime($actualEndTime);

        $totalDiffrence = round(abs($end_time - $st_time) / 60,2); 
         



        $error = array();
        $schduleList = $this->getAllSchdule();
        $localSchdule = array();
        if(count($schduleList)>0){
            foreach ($schduleList as $key => $value) {
                $localSchdule[] = $value['name'];
            }
        }
        //$_ds = array('morning','evening','afternoon');
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Name can not be empty';
        }else if( ! isset( $data['start_time'] ) || empty( $data['start_time'] ) ){
            $error['start_time'] = 'Name can not be empty'; 
        }else if( ! isset( $data['end_time'] ) || empty( $data['end_time'] ) ){
            $error['end_time'] = 'Name can not be empty'; 
        }else if($st_time == $end_time){
             $error['name'] = 'Start Time and end time can not be same';   

        }else if($st_time >= $end_time){
             $error['name'] = 'End Time must be greater then Start time';  

        }else if($totalDiffrence > 300){
             $error['name'] = 'Please check start and end time again ?? so much diffrence found';   
        }/*else if( ! in_array($data['name'],$_ds)){

              $error['name'] = 'Invalid delivery name'; 
        }*/else if(in_array($data['name'], $localSchdule)){

               $error['name'] = 'Delivery Schdule already exists'; 
        }
        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
            
       return $error;
             

    }
   /* public function edit(){

         $id = base64_decode($_GET['id']);
         $ds_types = TableRegistry::get('deliverySchdules');
         $single_ds = $ds_types->get($id);
         $this->set('single_ds',$single_ds); 
    }*/

    public function add()
    {
        
        $this->set('dslist',$this->getAllSchdule());
        $deliverySchdule = $this->DeliverySchdules->newEntity();
        if ($this->request->is('post')) { 
           // pr($this->request->getData());die;
           $error = array();
           $error = $this->validateAddSchdule($this->request->getData());
            
           if($error['statuscode']==200)
           { 
                   $deliverySchdule = $this->DeliverySchdules->patchEntity($deliverySchdule, $this->request->getData());
                    if ($this->DeliverySchdules->save($deliverySchdule)) {
                         
                        $this->Flash->success(__('Delivery Schdule has been added successfully')); 
                        return $this->redirect(['action' => 'add']);
                    }
            }else{

                 $this->set('error',$error);
                 $this->set('data',$this->request->getData());
            }


             
        }
         
    
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery Schdule id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    
     
     private function getifnameExistTwice( $id, $name ){


             $getname = TableRegistry::get('deliverySchdules');

             $getnameexist = $getname->find()
                                     ->where(['AND'=>['name'=>$name,'id <>'=>$id]])
                                     ->count();
             return $getnameexist;


     }

    private function validateEditSchdule( $data ){

        $actualStartTime = date("H:i", strtotime($data['start_time']));
        $actualEndTime = date("H:i", strtotime($data['end_time']));
        
        $st_time    =   strtotime($actualStartTime);
        $end_time   =   strtotime($actualEndTime);

        $totalDiffrence = round(abs($end_time - $st_time) / 60,2);
   
        $error = array();
        $_ds = array('morning','evening','afternoon');
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'Name can not be empty';
        }else if( ! isset( $data['start_time'] ) || empty( $data['start_time'] ) ){
            $error['start_time'] = 'start_time can not be empty'; 
        }else if($st_time == $end_time){
             $error['name'] = 'Start Time and end time can not be same';   

        }else if($st_time >= $end_time){
             $error['name'] = 'End Time must be greater then Start time';      

        }else if($totalDiffrence > 300){
             $error['name'] = 'Please check start and end time again ?? so much diffrence found';   
        }else if( ! isset( $data['end_time'] ) || empty( $data['end_time'] ) ){
            $error['end_time'] = 'end_time can not be empty'; 
        }else if( ! in_array($data['name'],$_ds)){

              $error['name'] = 'Invalid delivery name'; 
        }else if($this->getifnameExistTwice($data['id'],$data['name'])){

               $error['name'] = 'This time already saved with another values'; 
        }
        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;
           

    }




    public function edit($id = null)
    {  
        $error = array();
        if ($this->request->is(['patch', 'post', 'put'])) {
            
                $error = $this->validateEditSchdule($this->request->getData()); 


                
                if($error['statuscode']==200)
                {
                        $deliverySchdule = $this->DeliverySchdules->get($this->request->getData('id'), [
                        'contain' => []
                    ]);

                    $deliverySchdule = $this->DeliverySchdules->patchEntity($deliverySchdule, $this->request->getData());
                    if ($this->DeliverySchdules->save($deliverySchdule)) {
                          $this->Flash->success(__('Delivery Schdule has been updated successfully')); 
                          return $this->redirect(['action' => 'add']);
                    }
                }else{

                 $this->set('error',$error);
                 $this->set('data',$this->request->getData());

                }
            
        }else{
                $id = base64_decode($id);
                $deliverySchdule = $this->DeliverySchdules->get($id, [
                    'contain' => []
                ]);    
                $this->set('data',$deliverySchdule);
      }
         
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery Schdule id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*$this->request->allowMethod(['post', 'delete']);*/
        $deliverySchdule = $this->DeliverySchdules->get(base64_decode($id));
        $deliverySchdule->status = 0;
        if ($this->DeliverySchdules->save($deliverySchdule)) {
            $this->Flash->success(__('The delivery schdule has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery schdule could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add']);
    }
}
