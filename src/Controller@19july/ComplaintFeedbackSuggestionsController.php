<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ComplaintFeedbackSuggestions Controller
 *
 * @property \App\Model\Table\ComplaintFeedbackSuggestionsTable $ComplaintFeedbackSuggestions
 *
 * @method \App\Model\Entity\ComplaintFeedbackSuggestion[] paginate($object = null, array $settings = [])
 */
class ComplaintFeedbackSuggestionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Products']
        ];
        $complaintFeedbackSuggestions = $this->paginate($this->ComplaintFeedbackSuggestions);

        $this->set(compact('complaintFeedbackSuggestions'));
        $this->set('_serialize', ['complaintFeedbackSuggestions']);
    }

    /**
     * View method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->get($id, [
            'contain' => ['Users', 'Products']
        ]);

        $this->set('complaintFeedbackSuggestion', $complaintFeedbackSuggestion);
        $this->set('_serialize', ['complaintFeedbackSuggestion']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->newEntity();
        if ($this->request->is('post')) {
            $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->patchEntity($complaintFeedbackSuggestion, $this->request->getData());
            if ($this->ComplaintFeedbackSuggestions->save($complaintFeedbackSuggestion)) {
                $this->Flash->success(__('The complaint feedback suggestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The complaint feedback suggestion could not be saved. Please, try again.'));
        }
        $users = $this->ComplaintFeedbackSuggestions->Users->find('list', ['limit' => 200]);
        $products = $this->ComplaintFeedbackSuggestions->Products->find('list', ['limit' => 200]);
        $this->set(compact('complaintFeedbackSuggestion', 'users', 'products'));
        $this->set('_serialize', ['complaintFeedbackSuggestion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->patchEntity($complaintFeedbackSuggestion, $this->request->getData());
            if ($this->ComplaintFeedbackSuggestions->save($complaintFeedbackSuggestion)) {
                $this->Flash->success(__('The complaint feedback suggestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The complaint feedback suggestion could not be saved. Please, try again.'));
        }
        $users = $this->ComplaintFeedbackSuggestions->Users->find('list', ['limit' => 200]);
        $products = $this->ComplaintFeedbackSuggestions->Products->find('list', ['limit' => 200]);
        $this->set(compact('complaintFeedbackSuggestion', 'users', 'products'));
        $this->set('_serialize', ['complaintFeedbackSuggestion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Complaint Feedback Suggestion id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $complaintFeedbackSuggestion = $this->ComplaintFeedbackSuggestions->get($id);
        if ($this->ComplaintFeedbackSuggestions->delete($complaintFeedbackSuggestion)) {
            $this->Flash->success(__('The complaint feedback suggestion has been deleted.'));
        } else {
            $this->Flash->error(__('The complaint feedback suggestion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
