<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[] paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        

       if($_GET)
        {
            
                           $condition = [];
                             
                            $page = 0;
                            if(isset($_GET['category_id']) && ! empty($_GET['category_id'])){
                                $filterby = $_GET['category_id'];
                                 $condition = ['category_id'=>$_GET['category_id'],'Products.status'=>1];
                              }

                             if(isset($_GET['page']) && ! empty($_GET['page'])){
                                $page = $_GET['page'];
                              } 
                            $this->paginate = [
                            'limit' => 10,
                            'page'=> $page,
                            'contain' => ['Categories', 'Units'],
                            'order' => [
                                'Categories.id' => 'desc'
                            ],
                            'conditions'=>$condition
                        ];
                        $products = $this->paginate($this->Products);
                        $this->set(compact('products'));
                        $this->set('category_id',$_GET['category_id']); 
                        $categoriesTable = TableRegistry::get('Categories');
                        $categories = $categoriesTable->find('list')->toArray();
                        $this->set('categories',$categories);

        }else{   
                $this->paginate = [
                    'contain' => ['Categories', 'Units'],
                    'limit'=>10,
                    'conditions'=>['Products.status'=>1],
                    'order'=>['Products.id DESC']
                ];
                $products = $this->paginate($this->Products);
                $this->set(compact('products'));
                $categoriesTable = TableRegistry::get('Categories');
                $categories = $categoriesTable->find('list')->toArray();
                $this->set('categories',$categories);
            }

       
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       /* $product = $this->Products->get($id, [
            'contain' => ['Categories', 'Units', 'ComplaintFeedbackSuggestions', 'CustomOrders', 'ProductChildren', 'Sales', 'UserSubscriptions']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

/* public function getUnit(){
        if($_GET['id'])
        {
         $unit_id = $_GET['id'];
         echo $unit_id;die;
         $areas = $this->Users->Areas->find('list')->where(['region_id'=>$region_id])->hydrate(false)->toArray();
         echo json_encode($areas);die; 
        }else{
            echo "Error";die;
        }
    }*/

   
   private function alreadyExistProductName( $prodctName ){

       $productTable = TableRegistry::get('Products');

       $product = $productTable->find('all')
                               ->where(['name'=>trim($prodctName)])->toArray();
                                
                      if(count($product)){
                        return true;
                      } return false;         

   }
   private function checkValuesEachArray( $chield ){

     
              foreach ($chield as $key => $value) {

                       if($value == ''){
                        return false;
                       }
               }
     return true;
   }
   private function validateChieldProduct( $qtys, $units, $prices ){
                
              
              if( $this->checkValuesEachArray($qtys) ){

                    if( $this->checkValuesEachArray($units) ){
                         
                         if( $this->checkValuesEachArray($prices) ){
                             return false;
                         }
                    }

              }
              return true;

   } 

        
    private function validateAddProduct( $data ){


        $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = "PLease enter the Product name";
        }/*else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
            $error['name'] = 'Please check product Name';
        }*/else if( $this->alreadyExistProductName( $data['name'] ) ){
            $error['name'] = 'Product Name already exist';
        }else if( ! isset( $data['quantity'] ) || empty( $data['quantity'] ) ){
            $error['name'] = "PLease enter the quantity";
        }else if( ! preg_match('/^\d+$/',$data['quantity']) ){
            $error['name'] = "PLease enter the valid quantity Number";
        }else if( ! isset( $data['category_id'] ) || empty( $data['category_id'] ) ){
            $error['name'] = "PLease select the category";  
        }else if( ! preg_match('/^\d+$/',$data['category_id']) ){
            $error['name'] = "PLease enter the valid category";
        }else if( ! isset( $data['unit_id'] ) || empty( $data['unit_id'] ) ){
            $error['name'] = "PLease select the unit";  
        }else if( ! preg_match('/^\d+$/',$data['unit_id']) ){
            $error['name'] = "PLease enter the valid unit";
        }/*else if( ! isset( $data['deliverytiming'] ) || count( $data['deliverytiming'] ) <= 0 ){
            $error['name'] = "PLease choose Delivery Timing";
        }*/else if( ! isset( $data['price_per_unit'] ) || empty( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the Price Per Unit";
        }else if( ! ctype_digit( $data['price_per_unit'] ) ){
            $error['name'] = "PLease enter the valid Price Per Unit";
        }/*else if( ! isset(  $data['region_area'] ) || count( $data['region_area'] ) <= 0 ){
            $error['name'] = "PLease select at lease on deliverable place";
        }*/else if( ! isset(  $data['childqty'] ) || empty($data['childqty'] ) ) {
            $error['name'] = "PLease enter Chile Product Quantity values";
        }else if( ! isset(  $data['childunit'] ) || empty( $data['childunit'] )){
            $error['name'] = "PLease enter Chile Product Unit values";
        }else if( ! isset(  $data['childprice'] ) || empty( $data['childprice'] ) ){
            $error['name'] = "PLease enter Chile Product Price values";
        }else if( $this->validateChieldProduct( $data['childqty'], $data['childunit'], $data['childprice']) ){
            $error['name'] = "PLease check the all chiled product add values properly";
        } 
 
        if( count( $error ) > 0 ){
            $error['statuscode'] = 201;
        }else{
            $error['statuscode'] = 200;
        }
 
        return $error;
               

    }    




    public function add()
    {
        
        
        if ($this->request->is('post')) {

             

            $data = $this->request->getData();
            
            
            
            $error = $this->validateAddProduct($data);
            
            if( $error['statuscode'] == 200 ){
                    $productTable = TableRegistry::get('Products');
                    $product = $productTable->newEntity();
                    $product->category_id = $data['category_id'];
                    $product->name = $data['name'];
                    $product->price_per_unit = $data['price_per_unit'];
                    $product->quantity = $data['quantity'];
                    $product->unit_id = $data['unit_id'];
                    $product->status = 1;
                     if(isset($data['is_subscribe']) && !empty($data['is_subscribe'])){

                           if($data['is_subscribe'] == "yes"){
                            $product->is_subscribable = 1;
                          }else{
                            $product->is_subscribable = 0;
                          }
                    }

                    if(isset($data['iscontainer']) && !empty($data['iscontainer'])){

                           if($data['iscontainer'] == 1){
                            $product->iscontainer = 1;
                          }else{
                            $product->iscontainer = 0;
                          }
                    }


                    $product->created = date('Y-m-d h:i:s');
                    $product->modified = date('Y-m-d h:i:s');
                    $result = $productTable->save($product);
                    $resultschdule = '';
                    $finalSaved = '';
                    if($result){

                                $productchildrenTable = TableRegistry::get('ProductChildren'); 
                                  
                                 
                                for($i=0;$i<count($data['childqty']);$i++){
                                      $productChieldren = $productchildrenTable->newEntity();
                                      $productChieldren->product_id = $result->id;
                                      $productChieldren->quantity = $data['childqty'][$i];
                                      $productChieldren->unit_id = $data['childunit'][$i];
                                      $productChieldren->price = $data['childprice'][$i];

                                      $finalSaved = $productchildrenTable->save($productChieldren); 
                                } 

                            
                    }

                 if($finalSaved){
                  $error['statuscode'] = 200;
                  $error['name'] = "Product Has been saved successfully";
                  echo json_encode($error);die;

                 }
                    

            }else{
                
                echo json_encode($error);die;
            }
             
        }else{
                
                $regionTable = TableRegistry::get('Regions');
                
                
              $categoriesTable = TableRegistry::get('Categories');
             /* $categories = $categoriesTable->find('all')->select(['id'])->where(['name'=>'Milk'])->toArray();
              if(count($categories)>0)
              { 
                 $isMilkAlreadyAdded = $this->Products->find('all')->where(['category_id'=>$categories[0]['id']])->count();    
              }
                
                if( isset( $isMilkAlreadyAdded ) && $isMilkAlreadyAdded > 0)
                { 
                    $categories = $this->Products->Categories->find('list')->where(['Categories.id NOT IN'=>$categories[0]['id']])->toArray();
                   
                 }else{*/
                   $categories = $this->Products->Categories->find('list')->toArray();
                 //}

                


                $units = $this->Products->Units->find('list');
                $DeliverySchdules = $this->Products->DeliverySchdules->find('list');
                $region_area_list = $regionTable->find()
                                        ->contain(['areas'])
                                        ->hydrate(false)
                                        ->join([
                                        'table' => 'areas',
                                        'alias' => 'areas',
                                        'type' => 'INNER',
                                        'conditions' => 'Regions.id = areas.region_id',
                                    ])->toArray();
                $this->set(compact('product', 'categories', 'units','DeliverySchdules','region_area_list'));
                
            }
    }

     
    public function edit($id = null)
    {
             
                die('In Progress');
             $id = base64_decode($id);


    }

   
    public function delete($id = null)
    {
       
        $id = base64_decode($id);
         
        $products = $this->Products->get($id);
        
        $products->status = 0;
        if ($this->Products->save($products)) {
            $this->Flash->success(__('The products has been deleted.'));
        } else {
            $this->Flash->error(__('The products could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);

      
    }
}
