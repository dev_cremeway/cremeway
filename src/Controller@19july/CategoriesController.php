<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[] paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $categories = $this->paginate($this->Categories);

        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {/*
        $category = $this->Categories->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    private function getAllcat(){
         $catTable = TableRegistry::get('Categories');
         $catList = $catTable->find('all')
                                    ->hydrate(false)
                                    ->toArray();
          return $catList;
    }

    public function deleteCategory(){


         $id = base64_decode($_GET['id']);
         $cate_types = TableRegistry::get('Categories');
         $entity = $cate_types->get($id);
         $result = $cate_types->delete($entity);
         if($result){
           $this->Flash->success(__('Category has been deleted successfully')); 
            return $this->redirect(['action' => 'add']); 
      }
    }
     private function getcategory( $name ){

            $areaTable = TableRegistry::get('categories');
               $areaList = $areaTable->find('all')
                                     ->where(['name'=>$name])
                                     ->hydrate(false)
                                     ->toArray();
                                     if(count($areaList)>0){
                                        return true;
                                     }return false;


    }
 
  public function validatedeliverytiming( $data ){
 
         $error = array();
         $duplicate = array();
         foreach ($data['deliverytiming'] as $key => $value) {
                
                    $isthree = count(explode('_', $value));
                    if($isthree < 3){
                        $error['name'] = 'Please check delivery timing with Area and Region';
                        return $error;
                    }else if( in_array($value, $duplicate) ){
                              
                       $error['name'] = 'Please check delivery timing with Area and Region values are properly';
                        return $error;

                    } 
             array_push($duplicate, $value);
         
         }
         return $error;

  }

    private function validateAddcategories( $data ){


                  $error = array();
                 /* $catName = array('Milk','Milk Product','Other');*/
                  $catName = array('Milk','Other');
                  $data['name'] = trim($data['name']);
                  if( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
                     $error['name'] = 'Please enter the category name';
                  }else if( $this->getcategory( trim( $data['name'] ) ) ){
                    $error['name'] = 'Category already exist';
                  }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check category name';
                  }else if( ! in_array($data['name'],$catName) ){
                       $error['name'] = 'Please add valid category name ( Milk,Other) are valid name'; 
                  }else if( ! isset( $data['deliverytiming'] ) || empty( $data['deliverytiming'] ) ){
                     $error['name'] = 'Please select Delivery Timing';
                  }else if( ! isset( $data['deliverytiming'] ) || empty( $data['deliverytiming'] ) ){
                    $error['name'] = 'Please select Delivery Timing properly';
                  }
                  
                   
                  if( count( $error ) > 0 ){
                    $error['statuscode'] = 201;
                  }else{
                    $error = $this->validatedeliverytiming($data);
                    if( count( $error ) > 0 ){
                     $error['statuscode'] = 201; 
                    }else{
                     $error['statuscode'] = 200;
                     }
                  }
       return $error;
    }

    public function add()
    {



        $category = $this->Categories->newEntity();
        $this->paginate = [
            'limit' => 10,
            'conditions'=>['status'=>1]
        ];
        $catList = $this->paginate($this->Categories);
        $this->set(compact('catList'));



        if ($this->request->is('post')) {
            $data = $this->request->getData();
            

            $error = $this->validateAddcategories($this->request->getData());

            if($error['statuscode'] == 200)
            {   
                        
                   
                    $categoryTable = TableRegistry::get('Categories');
                    $category = $categoryTable->newEntity();
                    $category->name = trim($this->request->getData('name'));
                    $category->status = 1;
                    $result = $categoryTable->save($category);
                    $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules'); 
                    if( $result->id ) {

                          foreach ($data['deliverytiming'] as $key => $value) {
                                
                                   
                                    $component = explode('_', $value);
                                    $region = $component[0];
                                    $area = $component[1];
                                    $timing = $component[2];
                                    $categoryDeliverySchdule = $categoryDeliverySchduleTable->newEntity();
                                    $categoryDeliverySchdule->category_id=$result->id;
                                    $categoryDeliverySchdule->delivery_schdule_id=$timing;
                                    $categoryDeliverySchdule->region_id=$region;
                                    $categoryDeliverySchdule->area_id=$area;
                                    $categoryDeliverySchdule->created=date('Y-m-d');
                                    $categoryDeliverySchdule->modified=date('Y-m-d');
                                    $categoryDeliverySchduleTable->save($categoryDeliverySchdule);

                            }  

                        
                    $error['statuscode'] = 200;
                    $error['name'] = "Categgory saved successfully";
                    echo json_encode($error);die;

                    }
                
                       
        }else{
                         echo json_encode($error);die;
            }


    }else {


                   
                $regionTable = TableRegistry::get('Regions');
                $DeliverySchdulesTables = TableRegistry::get('DeliverySchdules');
                $DeliverySchdules = $DeliverySchdulesTables->find('list')->toArray();
                $region_area_list = $regionTable->find()
                                        ->contain(['areas'])
                                        ->hydrate(false)
                                        /*->join([
                                        'table' => 'areas',
                                        'alias' => 'areas',
                                        'type' => 'INNER',
                                        'conditions' => 'Regions.id = areas.region_id',
                                    ])*/->toArray();
                $this->set(compact('DeliverySchdules','region_area_list'));




    }


         
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

     private function getifnameExistTwice( $id, $name ){


             $getname = TableRegistry::get('Categories');

             $getnameexist = $getname->find()
                                     ->where(['AND'=>['name'=>$name,'id <>'=>$id]])
                                     ->count();
             return $getnameexist;


     }
    private function validateEditCategory($data){

         $catName = array('Milk','Other');
         $error = array();
        if( ! isset( $data['name'] ) || empty( $data['name'] ) ){
            $error['name'] = 'categgory name can not be empty';
        }else if( ! in_array($data['name'],$catName) ){
                       $error['name'] = 'Please add valid category name ( Milk,Other) are valid name'; 
       }else if($this->getifnameExistTwice($data['id'],$data['name'])){

               $error['name'] = 'This categgory already taken'; 
        }else if(preg_match('/[\'^!£$%&*()}{@#~?><>,|=_+¬-]/',$data['name'])){
                     $error['name'] = 'Please check Category name';
        
        }

        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;

    } 
    public function edit($id = null)
    {
         
         
       $error = array();

        
            if ($this->request->is(['patch', 'post', 'put'])) { 
                     $error = $this->validateEditCategory($this->request->getData());
                      
                  
                      if($error['statuscode'] == 200)
                        {     
                         
                          $error = $this->validatedeliverytiming($this->request->getData());
                          if(count($error)>0){

                            $response['statuscode'] = 201;
                            $response['name'] = $error['name'];
                            echo json_encode($response);die;

                          }else{
                                 
                             

                             $data = $this->request->getData();
                             $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules');
                             $res = $categoryDeliverySchduleTable->deleteAll(['CategoryDeliverySchdules.category_id' => $data['id']]);
                             if($res){






                                        $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules'); 
                                         

                                              foreach ($data['deliverytiming'] as $key => $value) {
                                                    
                                                       
                                                        $component = explode('_', $value);
                                                        $region = $component[0];
                                                        $area = $component[1];
                                                        $timing = $component[2];
                                                        $categoryDeliverySchdule = $categoryDeliverySchduleTable->newEntity();
                                                        $categoryDeliverySchdule->category_id=$data['id'];
                                                        $categoryDeliverySchdule->delivery_schdule_id=$timing;
                                                        $categoryDeliverySchdule->region_id=$region;
                                                        $categoryDeliverySchdule->area_id=$area;
                                                        $categoryDeliverySchdule->created=date('Y-m-d');
                                                        $categoryDeliverySchdule->modified=date('Y-m-d');
                                                        $categoryDeliverySchduleTable->save($categoryDeliverySchdule);

                                                }  

                                
                                            $error['statuscode'] = 200;
                                            $error['name'] = "Categgory saved successfully";
                                            echo json_encode($error);die;

                                   
                                  
                                   
                             }else{
                               $error['statuscode'] = 201;
                                $error['name'] = "Something went wrong";
                                echo json_encode($error);die;
                             } 
                             

                             
                              



                          } 

                           

                    
                    }else{
                         echo json_encode($error);die;     
                        }
            
        }else{
                $id = base64_decode($id);
                $categoryDeliverySchduleTable = TableRegistry::get('CategoryDeliverySchdules');
                $categoryDeliverySchdule = $categoryDeliverySchduleTable->find('all')->contain(['DeliverySchdules','Categories'])->where(['CategoryDeliverySchdules.category_id'=>$id])->hydrate(false)->toArray();

                
                $regionTable = TableRegistry::get('Regions');
                $DeliverySchdulesTables = TableRegistry::get('DeliverySchdules');
                $DeliverySchdules = $DeliverySchdulesTables->find('list')->toArray();
                $region_area_list = $regionTable->find()
                                        ->contain(['areas'])
                                        ->hydrate(false)
                                        ->toArray();
             
                $allRegionAreas=array();
                $selectedRegionAreas=array();
                $selectedTiming=array();
                foreach ($region_area_list as $key => $value) {
                 

                            foreach ($value['areas'] as $k => $v) {
                                                   
                                   $temp = array();
                                   $temp['region_area_name'] = $value['name'] .'-'.$v['name'];
                                   $temp['region_area_id'] = $value['id'].'_'.$v['id'];
                                   $allRegionAreas[] = $temp;            

                                               }                   
                               
                }

               
              
              

                foreach ($categoryDeliverySchdule as $ke => $val) { 
                    
                    $temp1['region_area_id'] = $val['region_id'].'_'.$val['area_id'];
                    $temp1['deliverytimingid'] = $val['delivery_schdule']['id'];
                    $selectedRegionAreas[] = $temp1;
                } 


              

                foreach ($categoryDeliverySchdule as $kee => $vall) { 

                    $temp11['deliverytimingid'] = $val['delivery_schdule']['id'];
                    $selectedTiming[] = $temp11;
                   }



 //pr($selectedRegionAreas);die;                
// pr($allRegionAreas);
 //pr($selectedTiming);
 //pr($DeliverySchdules);die;
                
             





/*pr($selectedRegionAreas);*/

                $checked = array();
                $makeselectedseprate = array();
                foreach ($selectedRegionAreas as $kk => $vv) {
                       
                       $region_area_id = $vv['region_area_id'];

                       if(in_array($region_area_id, $checked))
                       {
                        $makeselectedseprate[$vv['region_area_id']]['deliverytimingid'][] = $vv['deliverytimingid'];
                       }else{
                         $makeselectedseprate[$vv['region_area_id']]['deliverytimingid'][] = $vv['deliverytimingid'];
                       }
                       
                       array_push($checked, $region_area_id);

                     
                }

              

/*pr($makeselectedseprate);die;
*/









                $this->set(compact('DeliverySchdules','selectedTiming','allRegionAreas','makeselectedseprate'));
                $this->set('data',$categoryDeliverySchdule[0]['category']);
      }
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        /*$this->request->allowMethod(['post', 'delete']);*/
        $category = $this->Categories->get(base64_decode($id));
        $category->status = 0;
        if ($this->Categories->save($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add']);
    }
}
