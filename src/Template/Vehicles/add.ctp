 
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  #err_div{
    color:red;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      //$(".success").fadeOut(4000);
  
  });

 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Add Vehicle
              </h1>
             
               
            </section>


    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
          
              <div class="box-header">

                <h3 class="box-title"></h3>

              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>
                 
                   <form action="" method="post" id="adminupload">
                      <div class="form-group" id="err_div"></div>
                           <div class="form-group required">

                           <?php if(isset($error['vehicle_no'])){
                                  ?>
                                  <p class="red"><?php echo $error['vehicle_no']; ?></p>
                                  <?php
                                  } ?>
                            
                                <label for="exampleInputEmail1">Vehicle No.</label>
                                
                                <input type="text" maxlength="50" required class="form-control" id="vehicle_no" name="vehicle_no" value="<?php if(isset($data['vehicle_no'])){ echo $data['vehicle_no']; } ?>">
                                
                                

                             </div> 





                             <div class="form-group required">
                            
                                <label>Driver</label>

                                <select class="form-control" required name="user_id">
                                <option value="">Select Driver</option>
                                    <?php if(isset($driver)&&count($driver)>0){
                                               
                                               foreach ($driver as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $value['id'] ?>"><?php echo $value['name']."(".$value['phoneNo'].")"; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Driver First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                                 <?php if(isset($error['driver_id'])){
                                  ?>
                                  <p class="red"><?php echo $error['driver_id']; ?></p>
                                  <?php
                                  } ?> 
                             </div> 

       
                           <!--  <input type="hidden" name="driver_vehicle_id" value="<?php echo $data['driver_vehicle']['id']; ?>">   --> 
                            <input type="hidden" name="status" value="1">
                           <!--  <input type="hidden" name="vehicle_id" value="<?php echo $data['id']; ?>">
 -->


                      </div>
                </div> 
                 <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



<script type="text/javascript">
   $(document).ready(function(){
        $("#btn_submit").click(function(){
          var vehicle_no = jQuery.trim( $("#vehicle_no").val() );
          
         if(vehicle_no == ''){
          $("#err_div").html("Please enter the vehicle_no");
         }else{
            
             $("#adminupload").submit();
         } 
 });
 });
</script>