<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  #err_div{
    color:red;
  }
</style>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
        <section class="content-header header_dashbord">
              <h1>
               Vehicle Listing
              </h1>
               <div class="customer_bts">
               <a href="<?php echo HTTP_ROOT ?>Vehicles/add" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Vehicle</button></a> 
             </div>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">

             <form class="sidebar-form search_bar" method="get" action="lists">
              <div class="input-group">
               <input type="text" placeholder="Search by Vehicle No....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
               

              

               <a href="<?php echo HTTP_ROOT ?>Vehicles/lists"><button type="button" class="btn btn-sm btn-info reset_filter">Reset filters</button></a>
            
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th><?php echo $this->Paginator->sort('Vehicles.vehicle_no', 'Vehicle No.'); ?></th>  
                   <th><?php echo $this->Paginator->sort('Users.name', 'Driver'); ?></th>
                   <th><?php echo $this->Paginator->sort('Users.phoneNo', 'Driver Phone'); ?></th>   
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                   if(isset($vehicle) && count($vehicle) > 0){ 
                       foreach ($vehicle as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['vehicle_no']; ?></td>
                      


                      
                      <td><?php 
                                  
                                  if( isset( $value['driver_vehicle'])){             
                                    echo $value['driver_vehicle']['user']['name']; 
                                   }else{
                                    echo "--";
                                   }
                      
                      ?></td>
                      
                      
                      <td><?php if( isset( $value['driver_vehicle'])){             
                                    echo $value['driver_vehicle']['user']['phoneNo']; 
                                   }else{
                                    echo "--";
                                   } ?></td>
                      


                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Vehicles/edit/<?php echo base64_encode($value['id']);  ?>">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>

                       <a  onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Vehicles/delete/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a>
                      </td>
                      





                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red">Not Any Vehicles Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>
            
            <?php

            if( count( $vehicle ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php } ?>















              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



          
          
          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>

<script type="text/javascript">
   $(document).ready(function(){
        $("#btn_submit").click(function(){
          var vehicle_no = jQuery.trim( $("#vehicle_no").val() );
          
         if(vehicle_no == ''){
          $("#err_div").html("Please enter the vehicle_no");
         }else{
            
             $("#adminupload").submit();
         } 
 });
 });
</script>



