<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Vehicle'), ['action' => 'edit', $vehicle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vehicle'), ['action' => 'delete', $vehicle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vehicle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Vehicles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vehicle'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Driver Vehicles'), ['controller' => 'DriverVehicles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver Vehicle'), ['controller' => 'DriverVehicles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vehicles view large-9 medium-8 columns content">
    <h3><?= h($vehicle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Vehicle No') ?></th>
            <td><?= h($vehicle->vehicle_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($vehicle->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Driver Vehicles') ?></h4>
        <?php if (!empty($vehicle->driver_vehicles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Vehicle Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($vehicle->driver_vehicles as $driverVehicles): ?>
            <tr>
                <td><?= h($driverVehicles->id) ?></td>
                <td><?= h($driverVehicles->vehicle_id) ?></td>
                <td><?= h($driverVehicles->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DriverVehicles', 'action' => 'view', $driverVehicles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DriverVehicles', 'action' => 'edit', $driverVehicles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DriverVehicles', 'action' => 'delete', $driverVehicles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverVehicles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
