<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Custom Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Units'), ['controller' => 'Units', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Unit'), ['controller' => 'Units', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="customOrders index large-9 medium-8 columns content">
    <h3><?= __('Custom Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('unit_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customOrders as $customOrder): ?>
            <tr>
                <td><?= $this->Number->format($customOrder->id) ?></td>
                <td><?= $customOrder->has('user') ? $this->Html->link($customOrder->user->name, ['controller' => 'Users', 'action' => 'view', $customOrder->user->id]) : '' ?></td>
                <td><?= $customOrder->has('product') ? $this->Html->link($customOrder->product->name, ['controller' => 'Products', 'action' => 'view', $customOrder->product->id]) : '' ?></td>
                <td><?= $this->Number->format($customOrder->quantity) ?></td>
                <td><?= $customOrder->has('unit') ? $this->Html->link($customOrder->unit->name, ['controller' => 'Units', 'action' => 'view', $customOrder->unit->id]) : '' ?></td>
                <td><?= $this->Number->format($customOrder->status) ?></td>
                <td><?= h($customOrder->created) ?></td>
                <td><?= h($customOrder->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $customOrder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customOrder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customOrder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
