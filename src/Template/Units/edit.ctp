
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });

            $(function () {
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        
 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Unit
              </h1>
              
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
              <div class="box-header">
                 
                 <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Units/add" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>

              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>

                    <form action="edit" method="post">
                      
                           <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

                             </div> 
       
                             
                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">



                      </div>
                </div> 
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



