
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
               Units Listing
              </h1>
               <div class="customer_bts">
            <!--    <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Unit</button></a>  -->
             </div>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">

           



           <form class="sidebar-form search_bar" method="get" action="add">
              <div class="input-group">
               <input type="text" placeholder="Search by Unit Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 

             <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT ?>Units/add" class="btn btn-sm btn-info btn-flat pull-left">Reset</a>
                 
              </div>





            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th><?php echo $this->Paginator->sort('Units.name', 'Unit Name'); ?></th>  
                   <th>Created</th> 
                   <th>Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($unit) && count($unit) > 0){ 
                       foreach ($unit as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><?php echo $value['created']->format('d-M-Y'); ?></td>
                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Units/edit/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>
                       <a onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Units/delete/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a>
                      </td> 
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Units were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>

                
                <?php

            if( count( $unit ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
                                <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php } ?>


              </div>
               
            </div>
             
          </div>



          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Add New Units</h3>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>
                   
                     <form action="add" method="post">
                      
                           <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

                             </div> 
                            
                            <input type="hidden" name="status" value="1">



                      </div>
                </div> 
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



