<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">

 <?php 
    
       if($this->request->session()->read('Auth.User.username') == 'sales_admin')
       {
          echo $this->Element('sales_admin');
       }else if($this->request->session()->read('Auth.User.username') == 'finance_admin'){
          echo $this->Element('sales_finance_admin');
       }
  ?>

</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
              </ol>
            </section>

    
    <!-- Main content -->
    <section class="content">
     

     

      <!-- Main row -->
       

          <div class="row">
            

            <div class="col-md-12">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Sales Report's</h3>

                  
                 </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                   
                     <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Region</th>
                    <th>Area</th>
                    <th>Date</th>
                  </tr>
                  </thead>
                  
                  <tbody>
                  
                  <tr>
                    <td>Ravindra Jain</td>
                    <td>+91 9878541239</td>
                    <td>H.N 60 North Chd sec 20</td>
                    <td>
                       Nort CHD
                    </td>

                    <td>
                       sec 20
                    </td>

                    <td>
                       22-05-2017
                    </td>

                  </tr> 
                  
                
                  </tbody>
                </table>
              </div>
                     
                   
                </div>
               
              </div>
               
            </div>
           
          </div>
           

          
        </div>
         

         
         
      </div>
      
    </section>
</div>



