<style type="text/css">
   .red{ color:red; } .success{ color: green; }
   .gallery td {font-size: 15px; font-weight: 400; }
   .gallery-head th { font-size: 16px; }
</style>
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header header_dashbord">
      <h1>
         Galleries
      </h1>
      <div class="customer_bts">
         <a href="<?php echo HTTP_ROOT ?>Gallery/add"><button type="button" class="btn btn-sm btn-info">Add Gallery</button></a>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      
      <div class="box-body">
         <div class="table-responsive">
            <table class="table no-margin">
               <thead>
                  <tr class="gallery-head">
                     <th>Id</th>
                     <th>Title</th>  
                     <!-- <th>description</th> -->
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php /* pr($galleries);die;*/
                     if(isset($galleries) && count($galleries) > 0){ 
                         foreach ($galleries as $key => $value) {
                       ?>
                  <tr class="gallery">
                    <td>
                      <?php echo $value['id']; ?>
                    </td>
                    <td>
                      <?php echo $value['title']; ?>
                    </td>
                    <!-- <td>
                      <?php //echo $value['description']; ?>
                    </td> -->
                    <td>
                      <a href="<?php echo HTTP_ROOT."gallery/view/".base64_encode($value['id']); ?>">
                        Edit
                      </a> &nbsp; &nbsp; &nbsp;
                      <a href="<?php echo HTTP_ROOT."gallery/deletegallery/".base64_encode($value['id']); ?>">
                        Delete
                      </a>  
                    </td>  
                  </tr>
                  <?php
                     } 
                     }else{
                     ?>
                  <tr colspan="4">
                     <td style="color:red">No gallery found</td>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
            <?php
               if( count( $galleries ) > 0 )
               {                                
                   ?>
            <div class="text-right">
               <div class="paginator">
                  <nav>
                     <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?> 
                     </ul>
                  </nav>
                  <?php echo $this->Paginator->counter(
                     'showing {{current}} records out of
                      {{count}} total'); ?>
               </div>
            </div>
            <?php } ?>
         </div>
         <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer -->
</div>
<!-- /.box -->
</div>
<!-- /.col -->
<!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>
<style type="text/css">
   .sidebar-form.search_bar {
   display: inline-block;
   margin: 0;
   vertical-align: bottom;
   width: 30%;
   }
   .customer_bts .btn.btn-sm.btn-info {
   border: medium none;
   font-size: 13px;
   padding: 8px 12px;
   text-transform: uppercase;
   }
   .header_dashbord h1 {
   display: inline-block;
   }
   .customer_bts {
   display: inline-block;
   float: right;
   }
   .box-tools.filter {
   display: inline-block !important;
   margin-left: 10px;
   position: relative;
   width: 9%;
   vertical-align: top;
   /* float: left; */
   }
   .filter #filterUser {
   height: 39px;
   position: relative;
   top: -2px;
   }
</style>