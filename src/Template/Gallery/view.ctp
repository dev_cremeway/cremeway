<?php /*echo $this->Html->css('imageuploader');
      echo $this->Html->script('imageuploader');*/
 ?>
<style type="text/css">
  #field2 { display: none }
  .thumb {width: 50px; height: 50px; margin: 0.2em -0.7em 0 0; }
  .remove_img_preview {position:relative; top:-25px; right:0px; background:black; color:white; border-radius:50px; font-size:0.9em; padding: 0 0.3em 0; text-align:center; cursor:pointer; }
  .remove_img_preview:before { content: "×"; }
  .form-group textarea { width: 45%; }
  .form-group input { width: 45%; }
  #preview { margin: 1%; }
  label {display: block; } 
  #sortable{list-style: none; }
  .inner {width: 100%; margin: 10px 0 0 0;padding: 1% 1% 1% 2%; height: auto; background: #9fdfff; float: left; }
  .sr_no {float: left; width: 10%; }
  .image_g { float: left; width: 50%; height: auto; }
  .action_g { width: 10%; float: left; }
  .head { float: left; width: 94%; margin: 2% 1% 1% 5%; font-size: 18px; font-weight: 700; }
</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header header_dashbord">
    <h1>
      Coupon Listing
    </h1>
    <!-- <div class="customer_bts">
      <a href="
        <?php echo HTTP_ROOT ?>Coupons/add" class="scrolltodown">
        <button type="button" class="btn btn-sm btn-info">Add New Coupon</button>
      </a>
    </div> -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="box box-info">
      <div class="box-header">
        <div class="box-tools pull-right">
          <a href="<?php echo HTTP_ROOT ?>Gallery" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">    
      <div class="uploader__box js-uploader__box l-center-box">
        <form action="" method="POST" enctype="multipart/form-data" id="gallery_form">
          <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" name="title" placeholder="Enter title" value="<?php echo @$mg['title']; ?>" id="title">
          </div>
          <div class="form-group"> 
            <label for="exampleInputEmail1">Description</label>
            <textarea name="description" placeholder="Description" rows="8"><?php echo isset($mg['description']) ? @$mg['description'] : ''; ?></textarea>
          </div>
          <div class="uploader__contents">
          <input type="hidden" name="gallery_id" value="<?php echo @$mg['id'] ?>" id="gallery_id1">
            <label class="button button--secondary" for="fileinput">Select Files</label>   
          <input id="files" type="file" value="" name="image[]" multiple="multiple" />
          <div id="preview">
          </div>                         
          <div id="files_img"></div>
            <!-- <input id="fileinput" name="image" class="uploader__file-input" type="file" multiple value="Select Files"> -->
          </div>
          <input class="button button--big-bottom btn-info btn" id="button_sub" type="button" value="Update gallery">
        </form>
      </div>

      <!-- order set --> 
      <div class="head">
        <!-- <div class="sr_no">
          Sr No.
        </div> -->                
        <div class="image_g">
          Image
        </div>
        <div class="action_g">
          Action
        </div>
      </div>
      <ul id="sortable">  
        <input type="checkbox" id="ckbCheckAll" />        
        <?php $i = 1;
           if(count($galleries)>0){
            foreach ($galleries as $key1 => $value1) {
          ?>
              <li class="inner" id="gallery_<?php echo $value1['id']; ?>">
                <div class="sr_no">
                  <input type="checkbox" name="gallary_id" class="gallary_id" value="<?php echo $value1['id']; ?>">                  
                </div>            
                <div class="image_g">
                  <img src="<?php echo HTTP_ROOT."webroot/". $value1['image']; ?>" style="max-width: 160px;">
                </div>
                <div class="action_g">
                  <!-- <a href="<?php echo HTTP_ROOT."gallery/delete/". $value1['id']; ?>" style="max-width: 160px;"> Delete </a> -->
                  <a value="<?php echo $value1['id']; ?>" class="delete_img" style="max-width: 160px;"> Delete </a>
                </div>
              </li>
          <?php
          $i++;
        } ?>
        <div style="float: left; margin: 1%">
          <a class="button button--big-bottom btn-info btn" id="delete_sub"> Delete</a>
        </div>  
        <?php
           }else{
            ?>
            <p>No images found in this gallery.</p>
            <?php
           }
         ?>
      </ul>
      <!-- order set -->

        <!-- <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Sr. No.</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="sortable">
              <?php $i = 1;
                   if(isset($galleries) && count($galleries) > 0){ 
                       foreach ($galleries as $key => $value) {
                     ?>
              <tr class="inner" id="gallery_<?php echo $value['id'] ?>">                
                <td>
                  <?php echo $i; ?>
                </td>                
                <td>
                  <img src="<?php echo HTTP_ROOT."webroot/img/images/resized/". $value['image']; ?>" style="max-width: 160px;">
                </td>
                <td>
                  <a href="<?php echo HTTP_ROOT."gallery/delete/". $value['id']; ?>" style="max-width: 160px;"> Delete </a>
                </td>
              </tr>
              <?php
              $i++;
                } 
              } else{ 
                ?>
              <tr>
                <td colspan="3" style="color:red;">No Images were found</td>
              </tr>
              <?php

                } ?>
            </tbody>
          </table>
          <?php

            if( count( $galleries ) > 0 )
            {                                
                ?>
          <div class="text-right">
            <div class="paginator">
              <nav>
                <ul class="pagination">
                  <?= $this->Paginator->prev('< ' . __('previous')) ?>
                  <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                  <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
              </nav>
              <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
            </div>
          </div>
          <?php } ?>
        </div> -->
      </div>
    </div>
  </section>
</div>
<style type="text/css">
  .delete_icon{ color: red; }
  .edit_icon{ margin-right: 10px; }
</style><!-- 
<script>
(function(){
            var options = {};
            $('.js-uploader__box').uploader(options);
        }());
</script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript">
window.onload = function() {
    if(!window.location.hash) {
        window.location = window.location + '#loaded';
        window.location.reload();
    }
}

$("#button_sub").click(function(){
    var title = $('#title').val();
    var id    = $('#gallery_id1').val();
  if( $.trim(title) == ''){
    alert("Please enter title.");
    return false;
  }else{
    jQuery.ajax({
            url: '<?php echo HTTP_ROOT ?>Gallery/validateedittitle',            
            cache: false,
            type: 'POST',
            data: {'title':title, 'id':id},
            success: function(result){
              qq = JSON.stringify(result);
              qq = qq.replace('" ', '');
              qq = qq.replace('"', '');
              if(qq ==="found"){
                alert("Gallery with this name already exists.");
               return false;
              }else{  
                $("#gallery_form").submit();
              }              
        }
      }); return false;
  }
});
$('#delete_sub').click(function() {
  if (confirm('Are you sure you want to delete selected images?')) {
        var gallery_ids = [];
        var deleted     = 0;
      $("input:checkbox[name=gallary_id]:checked").each(function(){
          gallery_ids.push($(this).val());
      });
      $.each(gallery_ids, function( index, value ) {
        gallery_id = value;
        var gallery_id1= "gallery_"+value+"";
        jQuery.ajax({
                url: '<?php echo HTTP_ROOT ?>Gallery/delete',            
                cache: false,
                type: 'POST',
                data: {'id':gallery_id},
                success: function(result){
                  qq =  JSON.stringify(result);
                  qq = qq.replace('" ', '');
                  qq = qq.replace('"', '');
                  if(qq ==="deleted"){
                    ///alert("Image deleted successfully.");
                    $("#"+gallery_id1+"").remove();
                    deleted = 1;
                  }else{
                    alert("All images can not be deleted.");
                    return false;
                  }              
            }
          });
    });
    if(deleted == 1){
     alert("Images deleted successfully"); 
    }
  }
});

$("#ckbCheckAll").click(function () {
        $(".gallary_id").prop('checked', $(this).prop('checked'));
    });
    
    $(".gallary_id").change(function(){
        if (!$(this).prop("checked")){
            $("#ckbCheckAll").prop("checked",false);
        }
    });

/*var i = 1;
  function handleFileSelect(event) 
{
    for (var j = 0; j < $(this).get(0).files.length; j++) {
      alert("done");
    var clone = $('#files').clone();
    clone.attr('id', 'field2');
    clone.attr('class', i+1);
    $('#files_img').append(clone);

    console.log(event)
    var input = this;
  if (input.files && input.files[j])
    {
      var reader = new FileReader();
        console.log(reader)
        reader.onload = (function (e)
        {
          var span = document.createElement('span');
        span.innerHTML = ['<img class="thumb" src="',e.target.result, '" title="', escape(e.name), '"/><span valuet="'+i+'" class="remove_img_preview"></span>'].join('');
      document.getElementById('preview').insertBefore(span, null);
      });
        reader.readAsDataURL(input.files[j]);
    }
    $('#files').val('');
    i++;
  }
}

    
                
$('#files').change(handleFileSelect); */

//day la su kien click vao nut x để xóa ảnh
$('#preview').on('click', '.remove_img_preview',function ()
{
  $(this).parent('span').remove();
    $(this).val("");
    var t = $(this).attr( "valuet" );
    $("."+t+"").remove();
}); 
$('.delete_img').click(function() {
  if (confirm('Are you sure you want to delete image?')) {
    var gallery_id = $(this).attr('value');
    var gallery_id1= $(this).closest('li').attr('id');
    jQuery.ajax({
            url: '<?php echo HTTP_ROOT ?>Gallery/delete',            
            cache: false,
            type: 'POST',
            data: {'id':gallery_id},
            success: function(result){
              qq =  JSON.stringify(result);
              qq = qq.replace('" ', '');
              qq = qq.replace('"', '');
              if(qq ==="deleted"){
                alert("Image deleted successfully.");
                $("#"+gallery_id1+"").remove();
              }else{
                alert("All images can not be deleted.");
              }              
        }
      });
  }
});
$(document).ready(function () {

    $('ul').sortable({
        axis: 'y',
        stop: function (event, ui) {
          var data = $(this).sortable('serialize');
            
            $.ajax({
                data: data,
                type: 'POST',
                url: '<?php echo HTTP_ROOT ?>Gallery/order'
            },function(res){
                  console.log(res);
                  if(res=="Success"){
                   window.location.reload();
                  }else{
                    alert('Something Went Wrong PLease try again')
                  }              
            });
  }
    });
});

</script>