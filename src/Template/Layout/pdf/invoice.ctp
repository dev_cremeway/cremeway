<?php 
if(@$finaldata[0]['messageCode']){ 
  echo @$finaldata[0]['messageText']; 
}else{  
?>
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <title>invoice</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <style type="text/css">
        body{
        font-family: 'Open Sans', sans-serif;
        overflow-x: auto;
        overflow-y: auto;
        }
        .section-one {
        padding: 4% 0%;
        }
        .invoice-title p {
        margin: 0px;
        font-size: 18px;
        font-weight: 600;
        }
        .info-block ul {
        padding: 0;
        list-style: none;
        }
        .info-block .col-sm-6 {
        padding: 0px;
        }
        .info-block ul li {
        font-weight: 600;
        height: 35px;
        border-bottom: 1px solid #eee;
        padding: 7px 0px;
        }
        .details-receiver {
        text-align: center;
        font-size: 18px;
        font-weight: 600;
        margin: 0px;
        padding: 18px 0px 0px 0px;
        }
        .section-one .table.table-bordered {
        margin: 20px 0px;
        border-bottom: none;
        }
        .section-one thead {
        background: #eee;
        }
        .section-one thead td {
        border-bottom: 0px !important;
        }
        .col-sm-12.foter-amount {
        height: 430px;
        background: #fcfcfc;
        padding: 17px 0px;
        }
        .col-sm-4.amount-words,.col-sm-2.common-seal {
        height: 100%;
        text-align: center;
        }
        .befor-tax ul {
        padding: 0px;
        list-style: none;
        }
        .befor-tax ul li {
        font-weight: 600;
        height: 35px;
        border-bottom: 1px solid #eee;
        padding: 7px 6px;
        }
        .bblck .col-sm-6,.bblck .col-sm-12 {
        padding: 0px;
        }
        .befor-tax ul {
        padding: 0px;
        list-style: none;
        border: 1px solid #e8e8e8;
        border-bottom: none;
        }
        .border-leftn {
        border-left: none !important;
        }
        .col-sm-4.amount-words p, .col-sm-2.common-seal p{
        position: absolute;
        left: 0;
        right: 0;
        bottom: 1px;
        font-weight: 600;
        }
        .panel .table-bordered .text-center strong {
        display: block;
        }
        .col-sm-12.foter-amount.cc{
        height: 270px;
        padding-left: 15px;
        }
        .col-sm-12.foter-amount.cc .col-sm-6 p {
        font-weight: 600;
        text-align: center;
        padding: 6px 0px;
        }
        .col-sm-12.foter-amount.cc .col-sm-8 {
        height: 100%;
        border: 1px solid #eee;
        }
        .col-sm-12.foter-amount.cc .col-sm-8 .col-sm-6 {
        height: 100%;
        }
        .col-sm-6.amount-words {
        border-right: 1px solid #eee;
        }
        .col-sm-12.text-center.bb li {
        line-height: auto !important;
        height: auto;
        }
        .col-sm-12.text-center.bb .height-large.sp {
        height: 145px;
        }
        #home2 .text-center.invoice-title p {
        font-size: 30px;
        font-weight: 600;
        }
        .header-defi {
        border: 1px solid #ddd;
        padding: 10px;
        font-weight: 600;
        font-size: 16px;
        text-align: center;
        width: 50%;
        display: inline-block;
        margin: -1px;
        }
        #home2 .table.table-bordered.text-center {
        margin-top: 0px;
        }
        #home2 .details-receiver {
        padding-bottom: 20px;
        }
        .noppading{
        padding: 0px !important;
        }
        .rightside {
        float: right;
        }
        .printicon {
        float: right;
        font-size: 20px;
        margin-top: -25px;
        }
        .printicon a {
        color: #555555;
        }
        .firsttable td , .secondtable td  {
        font-weight: 600;
        }
        .firsttable .pull-left{
        text-align: left;
        }
        .firsttable .pull-right{
        text-align: right;
        }
        .firsttable {
        width: 45%;
        float: left;
        }
        .firsttable tr td {
        padding: 10px 0px;
        }
        .firsttable tr {
        border-bottom: 1px solid #eee;
        }
        .secondtable .pull-left{
        text-align: left;
        }
        .secondtable .pull-right{
        text-align: right;
        }
        .secondtable {
        width: 45%;
        float: right;
        }
        .secondtable tr td {
        padding: 10px 0px;
        }
        .secondtable tr {
        border-bottom: 1px solid #eee;
        }
        .tird-table{
        width: 100%;
        }
        .tird-table td {
        text-align: center !important;
        height: 150px;
        border: 1px solid #eee;
        }
        .tird-table td table td {
        height: auto;
        }
        @media all {
        .page-break { display: none; }
        }
        @media print {
        .page-break { display: block; page-break-before: always; }
        }
      </style>
    </head>
    <body>
      <section class="section-one">
        <div class="container">
          <div class="col-sm-12">
            <div class="text-center invoice-title">
              <p>CREMEWAY DAIRY FARMS</p>
              <p>VILLAGE- FIROZEPUR, RAIPUR RANI, DISTRICT-PANCHKULA, HARYANA, 134204</p>
              <p>GSTIN    06AAKFC1124D1Z3</p>
              <p>BILL OF SUPPLY</p>
            </div>
            <hr>
          </div>
          <div class="col-sm-12" style="height: 270px;">
            <table class="firsttable">
              <tbody>
               <tr>
                  <td colspan="2"> <p class="details-receiver">&nbsp;</p></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Serial No</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['invoice_no'] ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Date of Issue</td>
                  <td style="width: 50%; text-align: right;"><?php echo date('d/m/Y'); ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">GSTIN</td>
                  <td style="width: 50%; text-align: right;">06AAKFC1124D1Z3</td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">State</td>
                  <td style="width: 50%; text-align: right;">Haryana</td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">State Code</td>
                  <td style="width: 50%; text-align: right;">06</td>
                </tr>
              </tbody>
            </table>
           
            <table class="secondtable">
              <tbody>
                <tr>
                  <td colspan="2"> <p class="details-receiver">Details of Receiver / Billed to</p></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Name</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['user_name'] ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Address</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['address'] ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Area</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['area_name'] ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Region</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['region_name'] ?></td>
                </tr>
                <tr>
                  <td style="width: 50%; text-align: left;">Contact No.</td>
                  <td style="width: 50%; text-align: right;"><?php echo $finaldata[0]['contact'] ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="page-break"></div>
          <div class="col-sm-12">
            <div class="panel">
              <div class="">
                <div class="table-responsive">
                  <table class="table table-bordered text-center">
                    <thead>
                      <tr>
                        <td class="text-center"><strong>Sr. No.</strong></td>
                        <td class="text-center"><strong>Date</strong></td>
                        <td class="text-center"><strong>PRODUCT NAME</strong></td>
                        <td class="text-center"><strong>QUANTITY</strong></td>
                        <td class="text-center"><strong>Rate</strong></td>
                        <td class="text-center"><strong>Credit amount</strong></td>
                        <td class="text-center"><strong>Debit amount</strong></td>
                        <td class="text-center"><strong>Balance</strong></td>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- foreach ($order->lineItems as $line) or some such thing here -->
                      <?php    
                        $supply_amount1 = 0; $total_cr_amount = 0;
                        $i = 1; $total_quantity = 0; $total_amount = 0; $total_discount = 0; $supply_amount = 0;              
                          foreach ($finaldata as $key => $value) { 
                            if($value['amount_type'] == "Dr"){
                              $total_amount     = $total_amount + $value['amount'];  
                            }else if($value['amount_type'] == "rejected"){
                              $total_amount     = $total_amount;
                            }else{
                                $total_cr_amount   = $total_cr_amount + $value['amount'];
                            }
                            if($value['transaction_type_id'] == 6){
                              $order_data = json_decode($value['order_data'], true);
                              foreach ($order_data as $key1 => $value1) {
                        
                                if($value1['userId'] == $value['user_id'] ){ 
                                  foreach ($value1['items'] as $keyv => $valuev) {                                              ?>
                      <tr class="m1">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value['created']; ?></td>
                        <td><?php echo $valuev['name']; ?></td>
                        <?php
                          if(@$valuev['quantity_child'] != 0){
                                $total_quantity = $total_quantity + $valuev['quantity_child'];                          
                                $supply_amount  = @$valuev['pricr_per_package']*@$valuev['quantity_child'];
                                 ?>
                        <td><?php echo @$valuev['quantity_child']; ?></td>
                        <td><?php echo $valuev['pro_price']; ?></td>
                        <?php
                          if($value['amount_type'] == "Dr"){ ?>
                        <td></td>
                        <td><?php echo @$valuev['pricr_per_package']*@$valuev['quantity_child']; ?></td>
                        <td><?php echo $supply_amount1 = $value['balance']; ?></td>
                        <?php    
                          }else{
                          ?>
                        <td></td>
                        <td></td>
                        <?php
                          }
                          }else{
                            if($value['amount_type'] == "Dr"){
                            if($valuev['quantity'] > 99){
                              $total_quantity = $total_quantity + $valuev['quantity'];   
                              $supply_amount  = $supply_amount + $valuev['pro_price'];
                            }else{
                              $total_quantity = $total_quantity + $valuev['quantity'];   
                              $supply_amount  = $supply_amount + $valuev['pro_price']*$valuev['quantity'];
                            }
                            }else if($value['amount_type'] == "rejected"){
                            $total_quantity = $total_quantity;
                            $supply_amount  = $supply_amount;
                            }else{
                            $total_quantity = $total_quantity;
                            $supply_amount  = $supply_amount;
                            }
                            
                            ?>
                          <?php
                            if($value['amount_type'] == "Dr"){  ?>
                              <td><?php echo $valuev['quantity']; ?></td>
                              
                              <?php
                              if($valuev['quantity'] > 99){ ?> 
                                <td><?php echo $valuev['pro_price']; ?></td>                                      
                                <td></td>
                                <td><?php echo $valuev['pro_price']; ?></td>
                                <td><?php echo $supply_amount1 = $value['balance']; ?></td>
                            <?php    
                              }else if($valuev['quantity'] < 1){ ?>   
                                <td><?php echo $valuev['pro_price'] / $valuev['quantity']; ?></td>            
                                <td></td>
                                <td><?php echo $valuev['pro_price']; ?></td>
                                <td><?php echo $supply_amount1 = $value['balance']; ?></td>
                            <?php    
                              }else{
                              ?>
                                <td><?php echo $valuev['pro_price']; ?></td>
                                <td></td>
                                <td><?php echo $valuev['pro_price']*$valuev['quantity']; ?></td>
                                <td><?php echo $supply_amount1 = $value['balance']; ?></td>
                            <?php 
                              }   
                            }else{
                              ?>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          <?php
                            } 
                          } $i++;
                          ?>
                      </tr>
                      <?php
                        }                                 
                          
                        }           
                        } 
                        }else{                          
                        ?>
                      <tr class="m1">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value['created']; ?></td>
                        <td><?php echo $value['transaction_type_name']; ?></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $value['amount']; ?></td>
                        <td></td>
                        <td><?php echo $supply_amount1 = $value['balance']; ?></td>
                      </tr>
                      <?php   
                        $i++;       
                        }                      
                        } 
                        ?>
                      <tr style="text-align: center; font-weight: 700">
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td><?php //echo $total_quantity; ?></td>
                        <td></td>
                        <td><?php echo $total_cr_amount; ?></td>
                        <td><?php echo $total_amount; ?></td>
                        <td><?php echo $supply_amount1; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="page-break"></div>
          <div class="col-sm-12 " style="text-align: left !important;">
           <h4>Note</h4>
            <p style="text-align: left; font-size: 14px; line-height: 20px;">      

          For online payments Bank details are as following<br/>
          <b>Name : Cremeway Dairy Farms<br/>
          HDFC A/c No 50200003786757<br/>
          IFSC CODE - HDFC0002446<br/>
          Sec 25/Panchkula</b><br/><br/>

          Please SMS UTR No and your address after making online payment on 7837155699<br/><br/>

          To enable us to serve you better kindly download CREMEWAY mobile app from PLAY STORE<br/><br/>

          Thanking you once again for your cooperation through our Journey.<br/>
          Team Cremeway<br/>
          0172-4626288<br/>
          </p>
          <!-- <div class="col-sm-12 foter-amount cc">
            <table class="tird-table">
                <tbody>
                  <tr>
                    <td style="width: 30%; text-align: left; padding-bottom: 210px;padding-top: 0px;">Total Invoice Amount in words:</td>
                    <td style="width: 30%; text-align: left;  padding-bottom: 210px;padding-top: 0px;">(common seal)</td>
                    <td style="width: 40%; text-align: left;">
                      <table>
                        <tbody>
                          <tr>
                            <td style="padding-top: 10px;padding-bottom: 10px;">Certified that particulars given above are true and correct.</td>
                          </tr>
                          <tr>
                            <td  class="height-large sp" style="width: 30%; text-align: left;  padding-bottom: 160px; padding-top: 10px;"></td>
                          </tr>
                           <tr>   
                            <td style="padding-top: 10px;padding-bottom: 10px;">Authorised Signatory</td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
            </table>
            </div> -->
        </div>
      </section>
    <?php } ?>