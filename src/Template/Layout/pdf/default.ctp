<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Cremeway';
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="/var/www/html/cremeway/webroot/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/var/www/html/cremeway/webroot/css/AdminLTE.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> 


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/jquery-jvectormap-1.2.2.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/_all-skins.min.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
   
    
    <div class="">    
        <?= $this->fetch('content') ?>
    </div>

    <footer>
    </footer>

</body>
</html>
