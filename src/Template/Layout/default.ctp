<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Cremeway';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cremeway Admin</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= $this->Html->meta('icon') ?>
    <?php echo $this->Html->css('bootstrap.min'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
    <!-- default jquery library files -->
    <?php echo $this->Html->script('jquery-2.2.3.min'); ?>
    <?php echo $this->Html->script('bootstrap.min'); ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    <?php echo $this->Html->script('moment.js');  ?>
    <?php  echo $this->Html->script('bootstrap-datetimepicker.js');   ?>

    <!-- Theme Js file start from herer -->
    <?php echo $this->Html->css('jquery-jvectormap-1.2.2'); ?>
    <?php echo $this->Html->css('AdminLTE.min');?>
    <?php echo $this->Html->css('_all-skins.min'); ?>
    
    <!-- custom js file for additional funtionality -->
    <?php echo $this->Html->script('fastclick.min'); ?>
    <?php echo $this->Html->script('app.min'); ?>
    <?php echo $this->Html->script('jquery.sparkline.min'); ?>
    <?php echo $this->Html->script('jquery-jvectormap-1.2.2.min'); ?>
    <?php echo $this->Html->script('jquery-jvectormap-world-mill-en'); ?>
    <?php echo $this->Html->script('jquery.slimscroll.min'); ?>
    <?php echo $this->Html->script('Chart.min'); ?>
    <?php echo $this->Html->script('demo'); ?>
   
<script type="text/javascript">
    function noBack() { window.history.forward(); }
    noBack();
    window.onload = noBack;
    window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
    window.onunload = function () { void (0); }
</script>

    
</head>
<body class="hold-transition skin-blue sidebar-mini">
   
    <?= $this->Flash->render() ?>
    <div class="">    
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
