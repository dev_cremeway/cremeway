<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Order Tracking'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orderTrackings index large-9 medium-8 columns content">
    <h3><?= __('Order Trackings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('driver_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orderTrackings as $orderTracking): ?>
            <tr>
                <td><?= $this->Number->format($orderTracking->id) ?></td>
                <td><?= $this->Number->format($orderTracking->driver_id) ?></td>
                <td><?= $orderTracking->has('user') ? $this->Html->link($orderTracking->user->name, ['controller' => 'Users', 'action' => 'view', $orderTracking->user->id]) : '' ?></td>
                <td><?= $this->Number->format($orderTracking->order_no) ?></td>
                <td><?= h($orderTracking->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $orderTracking->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $orderTracking->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $orderTracking->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderTracking->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
