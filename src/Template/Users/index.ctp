<?php //echo $this->Html->script('dashboard2'); ?>
<style type="text/css">
  .info-box-text{ white-space: normal;text-transform: capitalize }
</style>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
              
            </section>

    
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
      <div class="col-md-12">
        <h3> Subscriptions </h3> 
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
          <a herf="" style="cursor:pointer;"><div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/orders" target="blank">  
              <span class="info-box-number"> Generate Today Order's List Area Wise</span></a>
            </div>
            <!-- /.info-box-content -->
          </div></a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
       <?php /* <div class="col-md-3 col-sm-6 col-xs-12">
        <img src="<?php echo HTTP_ROOT; ?>img/loading.gif" id="gif" style="display: block;  z-index: 99999999999999999;position: absolute;margin-left: 24%;width: 54%; visibility: hidden;">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Milk Needed Today</span>
              <span class="info-box-number milk_count">0 Litres</span>
            </div>
          </div>
        </div> */ ?>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=noapp" target="blank">
                <span class="info-box-text">Users not using App</span>
              </a>
              <span class="info-box-number"><?php echo $totalUsersCountnodevice; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=negativebalance" target="blank">
                <span class="info-box-text">Users with negative Balance</span>
              </a>  
              <span class="info-box-number"> <?php echo $negativeBalance; ?>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
          <h3> Users </h3> 
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=active" target="blank"> 
                <span class="info-box-text">Active Customers</span>
              </a> 
              <span class="info-box-number"><?php echo $totalUsersCount; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=deactive" target="blank"> 
                <span class="info-box-text">Pending Customers</span>
              </a>
              <span class="info-box-number"><?php echo $newuserscount; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=noSubscription" target="blank">
                <span class="info-box-text">No/Paused Subscriptions Users</span>
              </a>  
              <span class="info-box-number"><?php echo $userswithnoSub; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <a href="<?php echo HTTP_ROOT; ?>Users/view?filterby=leftcontainer" target="blank">
                <span class="info-box-text">Users with Left Containers</span>
              </a>
              <span class="info-box-number"><?php echo $leftContainerCount; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
          <h3> Transactions </h3> 
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-bag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Sales Current Month</span>
              <span class="info-box-number">Rs <?php echo number_format($transactionsThisMonth, 2, '.', ''); ?> </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-bag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Coupon Recharges</span>
              <span class="info-box-number">  Rs <?php echo number_format($couponTransactions, 2, '.', ''); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-bag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Online Recharges</span>
              <span class="info-box-number">  Rs <?php echo number_format($onlineTransactions, 2, '.', ''); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>


        
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Unresolved Complaints</span>
              <span class="info-box-number"><?php echo $pendingcomplaintCount; ?></span>
            </div>
          </div>
        </div> -->
        <!-- /.col -->
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Unresolved Complaints </span>
              <span class="info-box-number"> <?php echo $pendingcomplaintCount; ?>  </span>
            </div>
            
          </div>
          
        </div> -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>
<?php /*<script type="text/javascript">
    
    $( document ).ready(function() { 
      document.getElementById("gif").style.visibility="visible";        
      //var date = new Date().toISOString().slice(0,10); 
      $.ajax({
        type: 'GET',
        url: "<?php echo HTTP_ROOT ?>Users/getdashboardData",
       // async:true,
       // data: {'date': date },
        success: function(result) {         
              $(".milk_count").text(""+result+" Litres");
              document.getElementById("gif").style.visibility= "hidden";
          }
      });
    });

</script> */ ?>