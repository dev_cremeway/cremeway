<!-- <style type="text/css">
  #map_wrapper {
    height: 500px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}

</style>  -->
<?php echo $this->Html->css('neo.css'); ?>
<?php echo $this->Html->css('samples.css'); ?>
<?php  echo $this->Html->script('ckeditor.js');   ?>
<?php  echo $this->Html->script('sample.js');   ?>
<?php echo $this->Html->css('sol');
      echo $this->Html->script('sol');
      //echo $this->Html->script('addproduct');
 ?>
 <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
   <script>
  tinymce.init({
    selector: '#mytextarea'
  });
  </script>
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
.qty_form{ position: relative; }

.heading_up{ width: 100%; padding: 0; display: inline-block; }

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
  
</style>
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });          

</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                <?php echo $data[0]['title']; ?> Page
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="box box-info">
      <div class="box-header">
        <!-- <div class="box-tools pull-right">
          <a href="<?php echo HTTP_ROOT?>Products/index" class="btn btn-sm btn-info btn-flat pull-left">Back          
          </a>
        </div> -->
      </div>

      <div class="box-body">
        <div class="row">
          <div class='col-sm-8'>
            <form id="addProductForm" method="post" action="<?php echo HTTP_ROOT ?>Users/pages" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php if(isset($data[0]['id'])){ echo $data[0]['id']; } ?>">
              <div class="form-group" id="err_div"></div>
              <div class="form-group" id="err_div_green" style="color:green;"></div>
              <div class="form-group required">
                <label for="exampleInputEmail1">Title</label>
                <input type="text"  class="form-control" id="title" name="title" value="<?php if(isset($data[0]['title'])){ echo $data[0]['title']; } ?>">
                </div>

                <div class="form-group required">
                <label for="exampleInputEmail1">Content</label>
                
                <textarea name="description" id="mytextarea" value="<?php if(isset($data[0]['description'])){ echo $data[0]['description']; } ?>"><?php if(isset($data[0]['description'])){ echo $data[0]['description']; } ?></textarea>

                </div>
                <input type="hidden" name="status" value="1">
                </div>
              </div>
              <input type="submit" id="button" class="btn btn-primary" value="Submit">
              </form>
              <div id='loadingmessage' style="display:none;">
                <img id="loading" src='
                                
                  <?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <script>
  initSample();
</script>
