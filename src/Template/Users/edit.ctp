<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Areas'), ['controller' => 'Areas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Area'), ['controller' => 'Areas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Complaint Feedback Suggestions'), ['controller' => 'ComplaintFeedbackSuggestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Complaint Feedback Suggestion'), ['controller' => 'ComplaintFeedbackSuggestions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Custom Orders'), ['controller' => 'CustomOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Custom Order'), ['controller' => 'CustomOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Trackings'), ['controller' => 'OrderTrackings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Tracking'), ['controller' => 'OrderTrackings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales'), ['controller' => 'Sales', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sale'), ['controller' => 'Sales', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Balances'), ['controller' => 'UserBalances', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Balance'), ['controller' => 'UserBalances', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Containers'), ['controller' => 'UserContainers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Container'), ['controller' => 'UserContainers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Notifications'), ['controller' => 'UserNotifications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Notification'), ['controller' => 'UserNotifications', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Subscriptions'), ['controller' => 'UserSubscriptions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Subscription'), ['controller' => 'UserSubscriptions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('phoneNo');
            echo $this->Form->control('device_id');
            echo $this->Form->control('region_id', ['options' => $regions]);
            echo $this->Form->control('area_id', ['options' => $areas]);
            echo $this->Form->control('latitude');
            echo $this->Form->control('longitude');
            echo $this->Form->control('houseNo');
            echo $this->Form->control('username');
            echo $this->Form->control('password');
            echo $this->Form->control('group_id', ['options' => $groups]);
            echo $this->Form->control('image');
            echo $this->Form->control('token');
            echo $this->Form->control('status');
            echo $this->Form->control('otp');
            echo $this->Form->control('user_type_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
