<?php ?>
<div class="box-body">
  <div class="">
    <label for="">Select Year: </label>
    <select id="invoiceyear" onchange="reloadlist()" name="select">
      <?php
      for($i = date('Y') ; $i > 2016; $i--){
        echo ($year==$i) ? "<option selected>$i</option>" : "<option>$i</option>" ;
        // echo "<option>$i</option>";
      }
      $months = ($year<date('Y')) ? 12 : date(m) ;
      ?>
    </select>
<input type="hidden" id="user_customer_id" value="<?php echo $user_customer_id ?>">
 <div class="order-total-price">
              <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Month</th>
                   <th>Download Invoice</th>
                  </tr>
                  </thead>
                  <tbody>
                   <?php
                       for ($m=1; $m<=$months; $m++) {
                         $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
                     ?>
                      <tr>
                      <td><?php echo $month;  ?></td>
                      <!-- <td>
                        <a target="_blank" title="Download Invoice"  href="<?php echo HTTP_ROOT ?>Transactions/invoice/<?php echo $user_customer_id.'/'.$m; ?>">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </a>
                      </td> -->
                      <td>
                        <a target="_blank" title="Download Invoice"  href="<?php echo HTTP_ROOT ?>Transactions/invoice?user_customer_id=<?php echo $user_customer_id; ?>&year=<?php echo $year; ?>&month=<?php echo $m; ?>">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </a>
                      </td>
                     </tr>

                <?php
                      }
                 ?>
                  </tbody>
                </table>


              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
