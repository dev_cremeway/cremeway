<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }
  
</style>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- /.box-header -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Settings</h3>
        <div class="box-tools pull-right">
          <a href="<?php echo HTTP_ROOT?>Users/view" class="btn btn-sm btn-info btn-flat pull-left">Back to List           
          </a>
        </div>
      </div>
      <div class="box-body"> 
        <div id="err_qty" style="display:none;color:red;"></div>
        <div class="row">
          <div class='col-sm-4'>
            <form action="<?php echo HTTP_ROOT ?>Users/versions" method="post" id="adminupload" enctype="multipart/form-data">
              
              <div class="form-group" id="err_div"></div>
              <div class="form-group ">
                <label for="message_notification">Select Application</label>
                <select class="form-control" id="getarea" required="" name="app_type">
                  <option value="DriverAppVersion">Driver App</option>
                  <option value="ConsumerAppVersion">Consumer App</option>
                </select>
              </div>

              <div class="form-group ">
                <label for="message_notification">Version</label>
                <input class="form-control" type="text" name="version" value="" >
              </div>
              
              <input type="submit" id="btn_submit" class="btn btn-primary" value="Submit">
              </form>
            </div>
          </div>
          <br> <br>
          <div class="row">
            <div class='col-sm-6'>
              <table class="table table-bordered text-center">
                <tr>
                  <th>Application Name</th>
                  <th>Version</th>
                </tr>
                <tr>
                  <td>
                    <?php echo $settings1['identifier'] ?>
                  </td>
                  <td>
                    <?php echo $settings1['configuration'] ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <?php echo $settings2['identifier'] ?>
                  </td>
                  <td>
                    <?php echo $settings2['configuration'] ?>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        
      </div>
    </section>
  </div>