<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }

</style>
<script type="text/javascript">
  $(document).ready(function(){

      $(".success").fadeOut(4000);

  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- /.box-header -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Payment Methods</h3>
      </div>
      <div class="box-body">
        <div id="err_qty" style="display:none;color:red;"></div>
        <div class="row">
          <div class='col-sm-4'>
            <label>Choose Payment method to be enabled.</label>
            <?php
            // echo "<pre>";
            // print_r($data[0]->status);
            // echo "</pre>";
            ?>
            <form action="<?php echo HTTP_ROOT ?>Users/paymentMethod" method="post" id="paymentMethod">
              <div class="form-group ">
                <label class="checkbox-inline">
                  <input type="checkbox" value="1" name="coupon" <?php echo ($data[0]->status==1) ? 'checked' : '' ; ?>>Coupon
                </label>
              </div>
              <div class="form-group ">
                <label class="checkbox-inline">
                  <input type="checkbox" value="1" name="paytm" <?php echo ($data[1]->status==1) ? 'checked' : '' ; ?>>PayTm
                </label>
              </div>
              <div class="form-group ">
                <label class="checkbox-inline">
                  <input type="checkbox" value="1" name="payumoney" <?php echo ($data[2]->status==1) ? 'checked' : '' ; ?>>PayUmoney
                </label>
              </div>

              <input type="submit" id="btn_submit" class="btn btn-primary" value="Submit">
            </form>
          </div>
        </div>
      </div>

      </div>
    </section>
  </div>

  <script type="text/javascript">

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
  </script>
