<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }

</style>
<script type="text/javascript">
  $(document).ready(function(){

      $(".success").fadeOut(4000);

  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add PayUmoney Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="">
              <div class="box-body">
                <?php if (isset($data)): ?>
                  <div class="form-group">
                    <label for="merchant_id" class="col-sm-2 control-label">Merchant Id</label>

                    <div class="col-sm-8">
                      <input type="text" value="<?php echo (isset($data)) ? $data[0]->id : '' ; ?>" class="form-control" id="merchant_id" disabled>
                      <input type="hidden" name="merchant_id" value="<?php echo (isset($data)) ? $data[0]->id : '' ; ?>">
                    </div>
                  </div>
                <?php endif; ?>
                <div class="form-group">
                  <label for="merchant_key" class="col-sm-2 control-label">merchant_key</label>

                  <div class="col-sm-8">
                    <input name="merchant_key" maxlength="100" type="text" value="<?php echo (isset($data)) ? $data[0]->merchant_key : '' ; ?>" class="form-control" id="merchant_key" placeholder="Merchant key here as provided by Payu" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="merchant_salt" class="col-sm-2 control-label">merchant_salt</label>

                  <div class="col-sm-8">
                    <input name="merchant_salt" maxlength="100" type="text" value="<?php echo (isset($data)) ? $data[0]->merchant_salt : '' ; ?>" class="form-control" id="merchant_salt" placeholder="Merchant Salt as provided by Payu" required>
                  </div>
                </div>
                <?php if (isset($data)): ?>
                  <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">status</label>

                    <div class="col-sm-8">
                      <select name="status" class="form-control" id="status">
                        <option <?php echo ($data[0]->status==0) ? 'selected' : '' ; ?> value="0">Disabled</option>
                        <option <?php echo ($data[0]->status==1) ? 'selected' : '' ; ?> value="1">Enabled</option>
                      </select>
                    </div>
                  </div>
                <?php endif; ?>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">description</label>

                  <div class="col-sm-8">
                    <textarea name="description" maxlength="200" type="text" class="form-control" id="description" placeholder="description"><?php echo (isset($data)) ? $data[0]->description : '' ; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button> -->
                <?php if (isset($data)): ?>
                  <a href="<?php echo HTTP_ROOT ?>Users/add-payumoney" class="btn btn-default pull-right">Add New</a>
                <?php endif; ?>
                <button type="submit" class="btn btn-info">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
  </div>

  <script type="text/javascript">

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
  </script>
