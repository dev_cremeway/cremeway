<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
</style>
<?php if(isset($region_id) && !empty($region_id)){
   ?>
<script>
   $(document).ready(function(){
     regionid = '<?php echo $region_id ?>';
   });
</script>
<?php
   } ?>
<script type="text/javascript">
   $(document).ready(function(){

      $('#latest_rec').change(function() {
          if($(this).is(":checked")) {
              $("#latest_45").submit();
          }else{
            window.location.replace('<?php echo HTTP_ROOT; ?>Users/view');
          }
      });

      $("#filterUser").on('change', function(){
             var filterby = this.value;

               if(filterby != ''){
                 $("#filteruserby").submit();
               }
          });

          $("#getarea").on('change', function(){
             var region_id = this.value;
             if(region_id != '')
             {
              $("#filterbyregion").submit();
            }
          });

           $("#ajaxarea").on('change', function(){
             var r_id = $("#getarea").find('option:selected').val();

             $("#set_region_id").val(r_id);

             $("#filterbyregionarea").submit();

             /*var region_id = this.value;
             regionid = region_id;
             var htmloption = '';
             $.ajax({
              url: "<?php //echo HTTP_ROOT ?>Users/getArea",
              cache: false,
              data:{'id':region_id},
              success: function(regionList){
                $("#getallarealist").show();
                 var targetHtml = $("#ajaxarea");
                  htmloption = '<option value="">Select Area</option>';
                 var regionList = JSON.parse(regionList);
                 var length = getLength(regionList);
                 if(length){
                      $.each( regionList, function( key, value ) {
                         htmloption+='<option value='+key+'>'+value+'</option>';

                       });
                     $("#filterbyregionarea").submit();
                 }else{
                    htmloption = '';
                    htmloption+='<option value="">Please add Area to this region First</option>';
                 }
                targetHtml.html(htmloption);
              }
            });*/

          });

          var getLength = function(obj) {
                  var i = 0, key;
                  for (key in obj) {
                      if (obj.hasOwnProperty(key)){
                          i++;
                      }
                  }
                  return i;
              };

   });
</script>
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header header_dashbord">
      <h1>
         Customers Listing
      </h1>
      <div class="customer_bts">
         <a href="<?php echo HTTP_ROOT ?>Users/add"><button type="button" class="btn btn-sm btn-info">Add New Customer</button></a>
         <!--    <a href="<?php echo HTTP_ROOT ?>Users/driverAdd"><button type="button" class="btn btn-sm btn-info ">Add New Driver</button></a>
            <a href="<?php echo HTTP_ROOT ?>Users/adminadd"><button type="button" class="btn btn-sm btn-info ">Add New Admin</button></a> -->
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header ">
            <div class="left-serach-option">
               <form class="sidebar-form search_bar" method="get" action="view" style="width: 64%; float: left;">
                  <div class="input-group">
                     <input type="text" placeholder="Search by name,phone no." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){ echo $querystring; } ?>" name="query">
                     <span class="input-group-btn">
                     <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                     </button>
                     </span>
                  </div>
               </form>
               <form action="view" style="width: 28%; float: left;" id="latest_45" method="get">
                  <div class="radio" style="float: left;">
                     <label>
                     <input id="latest_rec" <?php if(isset($latest_45)){ ?> checked="" <?php } ?>name="latest_45" value="yes" type="checkbox">Recent Customers
                     </label>
                  </div>
               </form>
            </div>
            <div class="right-filter-option">
               <div class="box-filter">
                  <form action="view" id="filteruserby" method="get">
                     <select class="form-control" name="filterby" id="filterUser">
                        <option value="">Filter</option>
                        <option value="active" <?php if(isset($filterby) && $filterby == 'active' || @$_GET['filterby'] == 'active') {
                           ?>
                           selected
                           <?php
                              } ?>>By Activated Customer</option>
                        <option value="deactive" <?php if(isset($filterby) && $filterby == 'deactive'  || @$_GET['filterby'] == 'deactive') {
                           ?>
                           selected
                           <?php
                              } ?>>By Deactivated Customer</option>
                     </select>
                     <input type="hidden" name="region_id" <?php if(@$_GET['area_id']){ ?> value="<?php echo $_GET['region_id'] ?>" <?php } ?>>
                     <input type="hidden" name="area_id" <?php if(@$_GET['area_id']){ ?> value="<?php echo $_GET['area_id'] ?>" <?php } ?>>
                  </form>
               </div>
               <div class="box-filter">
                  <form action="view" id="filterbyregion" method="get">
                     <select class="form-control" name="region_id" id="getarea">
                        <option value="">Filter By Region</option>
                        <?php if(isset($regions)&&count($regions)>0){
                           foreach ($regions as $key => $value) {
                               ?>
                        <option value="<?php echo $key ?>"
                           <?php
                              if(isset($region_id)&&!empty($region_id))
                              {
                                 if($key == $region_id) {
                                 ?>
                           selected="selected"
                           <?php
                              }
                              }
                              ?>
                           ><?php echo $value; ?></option>
                        <?php
                           }
                           }else{
                           ?>
                        <option>Please add Region First</option>
                        <?php
                           } ?>
                     </select>
                     <input type="hidden" name="filterby" <?php if(@$_GET['filterby']){ ?> value="<?php echo $_GET['filterby'] ?>" <?php } ?>>
                  </form>
               </div>
               <div class="box-tools box-filter" id="getallarealist">
                  <form action="view" id="filterbyregionarea" method="get">
                     <select class="form-control" id="ajaxarea" name="area_id">
                        <?php if(isset($areas)&&count($areas)>0){
                           foreach ($areas as $key => $value) {
                               ?>
                        <option value="<?php echo $key ?>"
                           <?php
                              if(isset($area_id)&&!empty($area_id))
                              {
                                 if($key == $area_id) {
                                 ?>
                           selected="selected"
                           <?php
                              }
                              }
                              ?>
                           ><?php echo $value; ?></option>
                        <?php
                           }
                           }else{
                           ?>
                        <option>Please add Area First</option>
                        <?php
                           } ?>
                     </select>
                     <input type="hidden" name="region_id" id="set_region_id">
                     <input type="hidden" name="filterby" <?php if(@$_GET['filterby']){ ?> value="<?php echo $_GET['filterby'] ?>" <?php } ?>>
                  </form>
               </div>
               <a href="<?php echo HTTP_ROOT ?>Users/view"><button type="button" class="btn btn-sm btn-info reset_filter">Reset filters</button></a>
            </div>
         </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
           <table class="table no-margin">
              <thead>
                 <tr>
                   <th><?php echo $this->Paginator->sort('Users.id', 'User Id'); ?></th>
                    <th><?php echo $this->Paginator->sort('Users.name', 'Customer Name'); ?></th>
                    <th><?php echo $this->Paginator->sort('Users.phoneNo', 'Phone No.'); ?></th>
                    <th>Area</th>
                    <th>Address</th>
                    <th>Last Recharge Amount</th>
                    <th>Last Recharge Date</th>
                    <th><?php echo $this->Paginator->sort('Users.status', 'Customer Status'); ?></th>
                    <th>Balance</th>
                    <th>Action</th>
                 </tr>
              </thead>
              <tbody>
                 <?php /* pr($user);die;*/
                    if(isset($user) && count($user) > 0){
                        foreach ($user as $key => $value) {
                      ?>
                 <tr  <?php if(!empty($value['device_id'])){ echo "style='background: #AFEEAE' "; } ?> >
                   <td><?php  echo $value['id'];  ?></td>
                    <td class="
                       <?php if( isset($value['user_balances'][0]['balance'] )
                          &&
                          $value['user_balances'][0]['balance'] <= CUSTOMER_LOW_BALANCE_ALERT ) { echo 'alert alert-danger'; } ?>">
                       <a target="_blank" href="<?php echo HTTP_ROOT ?>Users/customerProfile?id=<?php echo base64_encode($value['id']);?>"><?php echo $value['name']; ?></a>
                    </td>
                    <td>
                       <a target="_blank" href="<?php echo HTTP_ROOT ?>Users/customerProfile?id=<?php echo base64_encode($value['id']);?>">   <?php echo $value['phoneNo']; ?></a>
                    </td>
                    <td><?php
                       if(! empty(!empty($value['area']['name'])))
                       echo $value['area']['name'];
                       ?></td>
                    <td><?php
                       if(! empty(!empty($value['houseNo'])))
                       echo $value['houseNo'];
                       ?></td>
                       <td>
                        <?php echo (!empty($value->transactions)) ? $value->transactions[0]->amount : 'NA' ; ?>
                       </td>
                       <td>
                        <?php echo (!empty($value->transactions)) ? $value->transactions[0]->created->format('d M Y h:i A') : 'NA' ; ?>
                       </td>
                    <td><?php
                       if($value['type_user'] == 'customer'){

                              $active_deactive = $value['status'] == 0 ? "Deactive" : "Active";
                       }else{
                        $active_deactive = "Active";
                       }
                       echo $active_deactive;
                       ?></td>
                    <td><?php if(isset($value['user_balance']['balance'])) { echo number_format($value['user_balance']['balance'], 2, '.', ''); } else{ echo number_format(0, 2, '.', ''); }  ?></td>
                    <td style="width: 16%;">
                       <a style="margin-right: 5%;" onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Users/delete/<?php echo base64_encode($value['id']);  ?>">
                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                       </a>
                       <a target="_blank" title="Download Invoice"  href="<?php echo HTTP_ROOT ?>Transactions/invoice/<?php echo $value['id'].'/'.date('m'); ?>">
                         <i class="fa fa-file-pdf-o" aria-hidden="true"></i><?php //echo $this->Html->Image('invoice.png') ?></a>
                       <?php
                          if($value['status'] == 0 && $value['type_user'] == 'customer')
                          {
                          ?>
                       <a style="margin-left: 5%;" href="<?php echo HTTP_ROOT ?>Users/activate/<?php echo base64_encode($value['id']);  ?>">
                       Activate
                       </a>
                       <?php }else{
                          ?>
                       <a  style="margin-left: 5%;"  href="<?php echo HTTP_ROOT ?>Users/deActivate/<?php echo base64_encode($value['id']);  ?>">
                       Deactivate
                       </a>
                       <?php }
                          ?>
                     </td>
                 </tr>
                 <?php
                    }
                    }else{
                    ?>
                 <tr colspan="4">
                    <td style="color:red">Not Any Customers Found</td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
           <?php
              if( count( $user ) > 0 )
              {
                  ?>
           <div class="text-right">
              <div class="paginator">
                 <nav>
                    <ul class="pagination">
                       <?= $this->Paginator->prev('< ' . __('previous')) ?>
                       <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                       <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                 </nav>
                 <?php echo $this->Paginator->counter(
                    'showing {{current}} records out of
                     {{count}} total'); ?>
              </div>
           </div>
           <?php } ?>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer -->
</div>
<!-- /.box -->
</div>
<!-- /.col -->
<!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>
<style type="text/css">
   .sidebar-form.search_bar {
   display: inline-block;
   margin: 0;
   vertical-align: bottom;
   width: 30%;
   }
   .customer_bts .btn.btn-sm.btn-info {
   border: medium none;
   font-size: 13px;
   padding: 8px 12px;
   text-transform: uppercase;
   }
   .header_dashbord h1 {
   display: inline-block;
   }
   .customer_bts {
   display: inline-block;
   float: right;
   }
   .box-tools.filter {
   display: inline-block !important;
   margin-left: 10px;
   position: relative;
   width: 9%;
   vertical-align: top;
   /* float: left; */
   }
   .filter #filterUser {
   height: 39px;
   position: relative;
   top: -2px;
   }
</style>
