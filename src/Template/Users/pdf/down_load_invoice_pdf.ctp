          <div class="box-body">
              <div class="table-responsive">
            <table class="table no-margin des">
                  <thead>
                  <tr>
                   <th>Amount</th>
                   <th>Type</th>  
                   <th>Date</th> 
                   <th>Description</th> 
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($invoice) && count($invoice) > 0){ 
                       foreach ($invoice as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['amount']; ?></td>
                       <td><?php echo $value['transaction_amount_type']; ?></td>
                       <td><?php echo $value['created']->format('Y-m-d'); ?></td>
                       <td><?php echo $value['transaction_type']; ?></td>
                      
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="4" style="color:red;">No Transaction were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>
               </div>
               </div>
     
     <style>         
@media print {
  .des th{width:25%; margin-bottom:10px;}
}
.box-body{width:100%; text-align:center;}
.table{width:100%; margin: 0 auto;}
.des td{width:25%; text-align:center; font-size:14px; }
table{float:left !important;}
}
 </style>
