
<style type="text/css">
  .box.box-info {
  border-top-color: white;
}
.box {
  background: #ffffff none repeat scroll 0 0;
  border-radius: 3px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
margin:10% auto;
  position: relative;
  width: 30%;
}
body {
  background: black none repeat scroll 0 0;
  font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  font-weight: 400;
  overflow-x: hidden;
  overflow-y: auto;
}
#err_login{
  color: red;
  margin-left: 124px;
}
.logo_image {
    text-align: center;
}
body{ background:#fff !important;}
.logo_image img {
    width: 220px;
    margin: 3% auto;
}
.box {
    background: #f1f1f1;
    border-radius: 3px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    margin: 0 auto;
    position: relative;
    width: 24%;
    padding: 20px 20px;
}
#err_login {margin:0;}
#frm_sbmt label{ margin-bottom: 5px; }
.wave {
  background-attachment: scroll;
  background-clip: border-box;
  background-image: url("/cremeway//img/wave.png");
  background-origin: padding-box;
  background-repeat: repeat-x;
  background-size: auto 128px;
  bottom: 0;
  left: 0;
  min-height: 128px;
  position: absolute;
  right: 0;
}
.box-footer #sub_btn {
  background: #70b162 none repeat scroll 0 0;
  border: 1px solid #70b162;
  float: left;
  font-size: 15px;
  padding: 12px 40px;
  text-align: left;
  text-transform: uppercase;
  width: 100%; text-align: center;
}
.box-footer {background: none; padding: 0; margin-top: 30px;}
</style>
<div class="wave">

</div>
<div class="logo_image"><img src="<?php echo HTTP_ROOT ?>/img/login_logo.png"></div>
<div class="box box-info">
            
            
            <form class="form-horizontal" id="frm_sbmt" action="<?php echo HTTP_ROOT ?>users/login" method="post">
              <div class="box-body">

             
             <div class="form-group" id="err_login"></div>
                



                <div class="form-group">
                  <label for="inputEmail3"  class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-12">
                    <input type="text" id="u_n" class="form-control" name="username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-12">
                    <input type="password" id="p_w" name="password" class="form-control">
                  </div>
                </div>

                 

                
             
              <div class="box-footer">
                
                <input type="button" id="sub_btn" class="btn btn-info pull-right" value="login">
              </div>
               
            </form>
          </div>

<script type="text/javascript">
  $(document).ready(function(){
         
         $("#sub_btn").click(function(){
            var username = $("#u_n").val();
             if (hasWhiteSpace(username)) {
               $("#err_login").html("Invalid details."); 
             }else{
              var u_n = $.trim($("#u_n").val());
              var p_w = $.trim($("#p_w").val());
              if( u_n == ''  ){ 
                  $("#err_login").html("Please enter the username");    
                  }else if( p_w == ''  ){ 
                  $("#err_login").html("Please enter the password");    
                  }else{
                   $("#frm_sbmt").submit();
                  }
                }

         });

         function hasWhiteSpace(s) {
          return s.indexOf(' ') >= 0;
        }


        $("input").keypress(function(event) {
        if (event.which == 13) { 
            event.preventDefault();
             var username = $("#u_n").val();
             if (hasWhiteSpace(username)) {
                $("#err_login").html("Invalid details."); 
             }else{
              var u_n = $.trim($("#u_n").val());
              var p_w = $.trim($("#p_w").val());
              if( u_n == ''  ){ 
                  $("#err_login").html("Please enter the username");    
                  }else if( p_w == ''  ){ 
                  $("#err_login").html("Please enter the password");    
                  }else{
                   $("#frm_sbmt").submit();
                  }
                } 
        }
    });


  });
</script>          