 <style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }
  
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
}); 
          

</script>  
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
            
            <!-- /.box-header -->

          <div class="box box-info">

              <div class="box-header with-border">
                  <h3 class="box-title">Add New Customer</h3>
                  <div class="box-tools pull-right">
                   
                    <a href="<?php echo HTTP_ROOT?>Users/view" class="btn btn-sm btn-info btn-flat pull-left">Back to List </a>
                   
                </div>
              </div>

              
            
            <div class="box-body">
            
            <div id="err_qty" style="display:none;color:red;"></div>

             <div class="row">
                  <div class='col-sm-4'>
                  
                   <form action="add" method="post" id="adminupload" enctype="multipart/form-data">
                   
                   <div class="form-group" id="err_div"></div>
                      <div class="form-group required">

                         
                         <?php if(isset($error['name'])){
                          ?>
                          <p class="red"><?php echo $error['name']; ?></p>
                          <?php
                          } ?> 




                        <label for="exampleInputEmail1">Name</label>
                         
                         <input type="text" required class="form-control" id="name" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                         
                       
                    
                      </div>

                      
                        <!--div class="form-group required"> 

                        <label for="exampleInputEmail1">Username</label>
                         
                         <input type="text" required class="form-control" id="username" name="username" value="<?php if(isset($data['username'])){ echo $data['username']; } ?>">
                         
                       
                    
                      </div-->



                       <div class="form-group"> 

                        <label for="exampleInputEmail1">Email</label>
                         
                         <input type="text" required class="form-control" id="email_id" name="email_id" value="<?php if(isset($data['email_id'])){ echo $data['email_id']; } ?>">
                         
                       
                    
                      </div>
                       <div class="form-group required"> 

                        <label for="exampleInputEmail1">Password(Minimum 8 characters.)</label>
                         
                         <input maxlength="10" type="password" required class="form-control" id="password" name="password" value="<?php if(isset($data['password'])){ echo $data['password']; } ?>">
                         
                       
                    
                      </div>



                      <div class="form-group required"> 

                        <label for="exampleInputEmail1">Phone No.</label>
                         
                         <input type="text" maxlength="10" required class="form-control" id="phoneNo" name="phoneNo" value="<?php if(isset($data['phoneNo'])){ echo $data['phoneNo']; } ?>">
                         
                       
                    
                      </div>




                       <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Region</label>

                                <select class="form-control" id="getarea" required name="region_id">
                                <option value="">Select Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                               
                             </div>
                              <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Area</label>

                                <select class="form-control" id="ajaxarea" required name="area_id">
                                </select>
                                
                               
                             </div> 




                          <!--    <div class="form-group">
                            
                                <label for="exampleInputEmail1">Select Customer OR Driver </label>

                                <select class="form-control" id="customer" required name="type_user">
                                <option value="">Select Customer/Driver</option>
                                    <?php
                                    $usertypes = array('customer','driver');

                                     if(isset($usertypes)&&count($usertypes)>0){
                                               
                                               foreach ($usertypes as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $value ?>"><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                               
                             </div> -->



                     
                     <div class="form-group" id="parent_cat">
                            
                                <label for="exampleInputEmail1">Make this customer VIP</label>&nbsp;&nbsp;&nbsp;
                               <input type="checkbox" name="user_cat_type" value="vip">
                          </label> 


                                </div>

                             <div class="form-group required"> 

                                <label for="exampleInputEmail1">Address</label>
                                 
                                 <input type="text" required class="form-control" id="houseNo" name="houseNo" value="<?php if(isset($data['houseNo'])){ echo $data['houseNo']; } ?>">
                           </div> 


                         

                          <div class="form-group required"> 

                        <label for="exampleInputEmail1">Add Money</label>
                         
                         <input type="text" maxlength="10" required class="form-control" id="amountNo" name="amount" value="<?php if(isset($data['amount'])){ echo $data['amount']; } ?>">
                         
                       
                    
                      </div>

                         <div class="form-group">
                          <input type="file" name="image" class="file">
                           <div class="input-group col-xs-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                            </span>
                           </div>
                         </div>

                           <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 
                   </form>          
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>

<script type="text/javascript"> 

   $(document).ready(function(){

     /*$('#email_id').on('blur', function() { 
      var email = this.value;

      $.ajax({
            url: "isemailalreadyExist",
            cache: false,
            type: 'POST',
            data:{'email':email},
            success: function(result){                     
              if(result =="found"){
                $("#err_qty").show();
                $("#err_qty").text("This email already exists or invalid email.");
                $('#email_id').val('');
              }
              else{
                $("#err_qty").hide();
              }
            }

          });

    });
     $('#phoneNo').on('blur', function() { 
      var phone = this.value;

      $.ajax({
            url: "ismobilealreadyExistEdit",
            cache: false,
            type: 'POST',
            data:{'phone':phone},
            success: function(result){                     
              if(result =="found"){
                $("#err_qty").show();
                $("#err_qty").text("This mobile number already exists.Please choose another.");
                $('#phoneNo').val('');
              } else if(result =="invalid"){
                $("#err_qty").show();
                $("#err_qty").text("Invalid mobile number.");
                $('#phoneNo').val('');
              }
              else{
                $("#err_qty").hide();
              }
            }

          });

    });*/
     
    /*$(document).on('change','#customer',function(){ 
        
        var type = $("option:selected", this).val();
        customertype = type;
        var html = '';
        if(type == 'customer'){
          $("#parent_cat").show();
         $("#customertype").html('');
         html+='<div class="radio"><label><input type="radio" name="user_cat_type" value="vip">VIP</label></div><div class="radio"><label><input type="radio" name="user_cat_type" value="normal">NORMAL</label></div><div class="radio"><label><input name="user_cat_type" type="radio" value="regular">REGULAR</label></div>';
         $("#customertype").append(html);

        }else{
          $("#parent_cat").hide();
          $("#customertype").empty();
        }
 return;

    }); */
   isValidAmount = 0;
   $('#amountNo').on("input", function() {
       var dInput = this.value;
        if( ( isFloat(dInput) || isInteger(dInput)) && dInput != '') {
          isValidAmount = 1;
        }else{
          isValidAmount = 0;
          this.value = '';
          $("#err_qty").show();
          $("#err_qty").text("Please enter the valid amount");
          $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
          return false;
        }
   });

      function isFloat(n) {
        return !!(n % 1);
    }

    function isInteger(value) {
         if ((undefined === value) || (null === value)) {
            return false;
        }
        return value % 1 == 0;
    }

        $("#getarea").on('change', function(){
           var region_id = this.value;
           var htmloption = '';  
           $.ajax({
            url: "<?php echo HTTP_ROOT ?>Users/getArea",
            cache: false,
            data:{'id':region_id},
            success: function(regionList){
               var targetHtml = $("#ajaxarea");
                htmloption = '<option value="">Select Area</option>';               
               var regionList = JSON.parse(regionList);
               var length = getLength(regionList);
               if(length){ 
                    $.each( regionList, function( key, value ) { 
                       htmloption+='<option value='+key+'>'+value+'</option>';
                     });
               }else{
                  htmloption = '';
                  htmloption+='<option value="">Please add Area to this region First</option>';
               }
              targetHtml.html(htmloption); 
            }
          });

        }); 
        var getLength = function(obj) {
                  var i = 0, key;
                  for (key in obj) {
                      if (obj.hasOwnProperty(key)){
                          i++;
                      }
                  }
                  return i;
              }; 


          $("#btn_submit").click(function(){
        
         var name = jQuery.trim( $("#name").val() );
         var phone = jQuery.trim ( $("#phoneNo").val() );
         var houseNo = jQuery.trim ( $("#houseNo").val() );
         var username = jQuery.trim ( $("#username").val() );
         var password = jQuery.trim ( $("#password").val() );
         var region = $("#getarea option:selected").val();
         var area = $("#ajaxarea option:selected").val();
         var customer = $("#customer option:selected").val();
         var radio_buttons = $("input[name='user_cat_type']");
         var email_id = jQuery.trim($("#email_id").val() );
         var phoneno = /^\d{10}$/;
         $.ajax({
            url: "ismobilealreadyExistEdit",
            cache: false,
            type: 'POST',
            data:{'phone':phone},
            success: function(result){                     
              if(result =="found"){
                $("#err_div").html("");
                $("#err_div").text("This mobile number already exists.Please choose another.");
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              } else if(result =="invalid"){
                $("#err_div").html("");
                $("#err_div").text("Invalid mobile number.");
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              }else if((! phone.match(phoneno)))  
                  { 
                    $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow"); 
                    $("#err_div").html("");
                    $("#err_div").text("Please enter a correct phone No.");  
                  }else if (! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_id) && email_id != ""){           
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the valid Email id");
               }else if(name == ''){
                $("#err_div").html("Please enter the name");
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
               }/*else if(username == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the Username");
               }else if(email_id == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the Email id");
               }*/else if(password == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the password");
               }else if(password.length < 8 ){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter password of at least 8 character");
               }else if(phone == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the phone");
               }else if(region == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please Select The Region First");
               }else if(area == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please Select The Area First");
               }/*else if( radio_buttons.filter(':checked').length == 0){

                  $("#err_div").html("");
                  $("#err_div").html("Please select customer type either Normal,Regular Or VIP"); 
                  $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                  return false
              }*/else if(houseNo == ''){
                  $("#err_div").html("");
                  $("#err_div").html("Please enter the Address");
                  $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                    return false
               }else if(isValidAmount == 0){
                    $("#err_div").hide();
                    $("#err_div").show();
                    $("#err_div").html("Please enter the Money");
                    $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                    return false 
               }else{
                    $("#adminupload").submit();
                  }
              
            }

          });
         
         
 });
 });
</script>

 