

<style type="text/css">
   .red{
   color:red;
   }
   #err_div{
   color:red;
   }
   .success{
   color: green;
   }
   .file {
   visibility: hidden;
   position: absolute;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
      
       $(".success").fadeOut(4000);
   
   });
   
   $(document).on('click', '.browse', function(){
   var file = $(this).parent().parent().parent().find('.file');
   file.trigger('click');
   });
   
   $(document).on('change', '.file', function(){
   $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
   }); 
       
   
</script>  
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header">
      <h1>
         Dashboard
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- /.box-header -->
      <div class="box box-info">
         <div class="box-header with-border">
            <h3 class="box-title">Add New Admin</h3>
            <div class="box-tools pull-right">
               <a href="<?php echo HTTP_ROOT?>AclManager" class="btn btn-sm btn-info btn-flat pull-left">Back to List </a>
            </div>
         </div>
         <div class="box-body">
            <div class="row">
               <div class='col-sm-4'>
                  <form action="adminadd" method="post" id="adminupload" enctype="multipart/form-data">
                     <div class="form-group" id="err_div"></div>
                     <div class="form-group required">
                        <?php if(isset($error['name'])){
                           ?>
                        <p class="red"><?php echo $error['name']; ?></p>
                        <?php
                           } ?> 
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" required class="form-control" id="name" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                     </div>
                     <div class="form-group required"> 
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" required class="form-control" id="username" name="username" value="<?php if(isset($data['username'])){ echo $data['username']; } ?>">
                     </div>
                     <div class="form-group"> 
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" required class="form-control" id="email_id" name="email_id" value="<?php if(isset($data['email_id'])){ echo $data['email_id']; } ?>">
                     </div>
                     <div class="form-group required"> 
                        <label for="exampleInputEmail1">Password</label>
                        <input maxlength="10" type="password" required class="form-control" id="password" name="password" value="<?php if(isset($data['password'])){ echo $data['password']; } ?>">
                     </div>
                    <?php /* <div class="form-group required"> 
                        <label for="exampleInputEmail1">Phone No.</label>
                        <input type="text" maxlength="10" required class="form-control" id="phone" name="phoneNo" value="<?php if(isset($data['phoneNo'])){ echo $data['phoneNo']; } ?>">
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">Region</label>
                        <select class="form-control" id="getarea" required name="region_id">
                           <option value="">Select Region</option>
                           <?php if(isset($regions)&&count($regions)>0){
                              foreach ($regions as $key => $value) {
                                  ?>
                           <option value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                           <?php
                              }
                              
                              }else{
                              ?>
                           <option>Please add Region First</option>
                           <?php
                              } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">Area</label>
                        <select class="form-control" id="ajaxarea" required name="area_id">
                        </select>
                     </div>
                    */ ?> 
                     <div class="form-group required">
                        <label for="exampleInputEmail1">Select Group</label>
                        <select class="form-control" id="admingroup" required name="group_id">
                           <option value="">Select Group</option>
                           <?php if(isset($groups)&&count($groups)>0){
                              foreach ($groups as $key => $value) {
                                  ?>
                           <option value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                           <?php
                              }
                              
                              }else{
                              ?>
                           <option>Please add Group First</option>
                           <?php
                              } ?>
                        </select>
                     </div>
                     <div class="form-group required">
                        <label for="exampleInputEmail1">Select Role</label>
                        <select class="form-control" id="adminrole" required name="role_id">
                           <option value="">Select Role</option>
                           <?php if(isset($roles)&&count($roles)>0){
                              foreach ($roles as $key => $value) {
                                  ?>
                           <option value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                           <?php
                              }
                              
                              }else{
                              ?>
                           <option>Please add Role First</option>
                           <?php
                              } ?>
                        </select>
                     </div>
                     <div class="form-group"> 
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" required class="form-control" id="houseNo" name="houseNo" value="<?php if(isset($data['houseNo'])){ echo $data['houseNo']; } ?>">
                     </div>
                     <div class="form-group">
                        <input type="file" name="image" class="file">
                        <div class="input-group col-xs-12">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                           <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                           <span class="input-group-btn">
                           <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                           </span>
                        </div>
                     </div>
                     <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<script type="text/javascript">
   $(document).ready(function(){
         $("#getarea").on('change', function(){
            var region_id = this.value;
            var htmloption = '';  
            $.ajax({
             url: "<?php echo HTTP_ROOT ?>Users/getArea",
             cache: false,
             data:{'id':region_id},
             success: function(regionList){
                var targetHtml = $("#ajaxarea");
                 htmloption = '<option value="">Select Area</option>';               
                var regionList = JSON.parse(regionList);
                var length = getLength(regionList);
                if(length){ 
                     $.each( regionList, function( key, value ) { 
                        htmloption+='<option value='+key+'>'+value+'</option>';
                      });
                }else{
                   htmloption = '';
                   htmloption+='<option value="">Please add Area to this region First</option>';
                }
               targetHtml.html(htmloption); 
             }
           });
   
         }); 
         var getLength = function(obj) {
                   var i = 0, key;
                   for (key in obj) {
                       if (obj.hasOwnProperty(key)){
                           i++;
                       }
                   }
                   return i;
               }; 
   
   
   
   $("#btn_submit").click(function(){
         
          var name = jQuery.trim( $("#name").val() );
          var username = jQuery.trim ( $("#username").val() );
          var password = jQuery.trim ( $("#password").val() );
          var admingroup = $("#admingroup option:selected").val(); 
          var adminrole = $("#adminrole option:selected").val(); 
   
          if(name == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("Please enter the name");
          }else if(username == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("");
           $("#err_div").html("Please enter the Username");
          }else if(username.match(/\s/g)){
            $("#err_div").html("");
            $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            $("#err_div").html("Spaces are not allowed in username.");
          }else if(password == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("");
           $("#err_div").html("Please enter the password");
          }else if(admingroup == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("");
           $("#err_div").html("Please Select Admin Group First");
          } else if(adminrole == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("");
           $("#err_div").html("Please Select Admin Role First");
          } else{
              $("#adminupload").submit();
          } 
   });
        
   
   
   
   
   
   
   
   });
</script>

