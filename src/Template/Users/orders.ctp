<?php
  echo $this->Html->css('bootstrap.min');
   echo $this->Html->css('font-awesome');  
 //echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">  --> 


<script>

 $(function () {
   var bindDatePicker = function() {
    $(".date").datetimepicker({
        format:'YYYY-MM-DD',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      }
    }).find('input:first').on("blur",function () {
      // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
      // update the format if it's yyyy-mm-dd
      var date = parseDate($(this).val());

      if (! isValidDate(date)) {
        //create date based on momentjs (we have that)
        date = moment().format('YYYY-MM-DD');
      }

      $(this).val(date);
    });
  }
   
   var isValidDate = function(value, format) {
    format = format || false;
    // lets parse the date to the best of our knowledge
    if (format) {
      value = parseDate(value);
    }

    var timestamp = Date.parse(value);

    return isNaN(timestamp) == false;
   }
   
   var parseDate = function(value) {
    var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
    if (m)
      value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

    return value;
   }
   
   bindDatePicker();
 });
  /*$( function() {
    $( "#datepicker" ).datepicker({
  dateFormat: "yy-mm-dd",
  minDate: 0
});
  } );*/
  </script>  
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>invoice</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
    .red1 { color:red; }    
    .grey { background: #ccc; }
    .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
      body{
      font-family: 'Open Sans', sans-serif;
      overflow-x: visible;
      overflow-y: visible;
      }
      .section-one {
      padding: 4% 0%;
      }
      .invoice-title p {
      margin: 0px;
      font-size: 18px;
      font-weight: 600;
      }
      .info-block ul {
      padding: 0;
      list-style: none;
      }
      .info-block .col-sm-6 {
      padding: 0px;
      }
      .info-block ul li {
      font-weight: 600;
      height: 35px;
      border-bottom: 1px solid #eee;
      padding: 7px 0px;
      }
      .details-receiver {
      text-align: center;
      font-size: 18px;
      font-weight: 600;
      margin: 0px;
      padding: 18px 0px 0px 0px;
      }
      .section-one .table.table-bordered {
      margin: 20px 0px;
      border-bottom: none;
      }
      .section-one thead {
      background: #eee;
      }
      .section-one thead td {
      border-bottom: 0px !important;
      }
      .col-sm-12.foter-amount {
      height: 430px;
      background: #fcfcfc;
      padding: 17px 0px;
      }
      .col-sm-4.amount-words,.col-sm-2.common-seal {
      height: 100%;
      text-align: center;
      }
      .befor-tax ul {
      padding: 0px;
      list-style: none;
      }
      .befor-tax ul li {
      font-weight: 600;
      height: 35px;
      border-bottom: 1px solid #eee;
      padding: 7px 6px;
      }
      .bblck .col-sm-6,.bblck .col-sm-12 {
      padding: 0px;
      }
      .befor-tax ul {
      padding: 0px;
      list-style: none;
      border: 1px solid #e8e8e8;
      border-bottom: none;
      }
      .border-leftn {
      border-left: none !important;
      }
      .col-sm-4.amount-words p, .col-sm-2.common-seal p{
      position: absolute;
      left: 0;
      right: 0;
      bottom: 1px;
      font-weight: 600;
      }
      .panel .table-bordered .text-center strong {
      display: block;
      }
      .col-sm-12.foter-amount.cc{
      height: 270px;
      padding-left: 15px;
      }
      .col-sm-12.foter-amount.cc .col-sm-6 p {
      font-weight: 600;
      text-align: center;
      padding: 6px 0px;
      }
      .col-sm-12.foter-amount.cc .col-sm-8 {
      height: 100%;
      border: 1px solid #eee;
      }
      .col-sm-12.foter-amount.cc .col-sm-8 .col-sm-6 {
      height: 100%;
      }
      .col-sm-6.amount-words {
      border-right: 1px solid #eee;
      }
      .col-sm-12.text-center.bb li {
      line-height: auto !important;
      height: auto;
      }
      .col-sm-12.text-center.bb .height-large.sp {
      height: 145px;
      }
      #home2 .text-center.invoice-title p {
      font-size: 30px;
      font-weight: 600;
      }
      .header-defi {
      border: 1px solid #ddd;
      padding: 10px;
      font-weight: 600;
      font-size: 16px;
      text-align: center;
      width: 50%;
      display: inline-block;
      margin: -1px;
      }
      #home2 .table.table-bordered.text-center {
      margin-top: 0px;
      }
      #home2 .details-receiver {
      padding-bottom: 20px;
      }
      .noppading{
      padding: 0px !important;
      }
      .rightside {
      float: right;
      }
      .printicon {
      float: right;
      font-size: 20px;
      margin-top: -25px;
      }
      .printicon a {
      color: #555555;
      }
      @media all {
        .page-break { display: none; }
      }

      @media print {
        .page-break { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body>
          <div class="right-filter-option">
            <div class="box-filter">  
              <form action="" id="filteruserby" method="post">
                 <select class="form-control" name="filterby" id="filterUser">
                 <?php if (isset($_POST["submit"])) { ?>
                  <option value="all">All</option>
                  <?php  foreach ($routesf as $kr => $vr) {
                  ?>

                    <option <?php if( $_POST["filterby"] == $vr['route']['name'] ){ ?> selected <?php } ?> value="<?php echo $vr['route']['name']; ?>"> <?php echo $vr['route']['name']; ?></option>
                    
                  <?php } } else { ?>
                 <option value="all">All</option>
                  <?php foreach ($routesf as $kr => $vr) { ?>

                    <option value="<?php echo $vr['route']['name']; ?>"> <?php echo $vr['route']['name']; ?></option>
                    
                  <?php  } } ?>

                 </select>
                <!--  <input type="text" placeholder="Select Date" name="date" id="datepicker" value="<?php if (isset($_POST["submit"])) { echo $_POST["date"]; } ?>">  -->
                 <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="date" class="form-control" value="<?php if (isset($_POST["submit"])) { echo $_POST["date"]; } else { echo date('Y-m-d'); } ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div> 
                 <button type="reset" value="Reset" class="reset">Reset</button>
                 <input type="submit" value="submit" name="submit" id="submit">
              </form>
            </div> 
          </div>   
    <section id="home2" class="section-one">
      <div class="container">
        <div class="col-sm-12">
          <div class="text-center invoice-title">
            <p>Todays Order List</p>
            <div class="printicon"><a onclick="myFunction()"><i class="fa fa-print" aria-hidden="true"></i></a></div>
          </div>
          <hr>
          <?php $milkQty = 0; 
            foreach ($totalorders as $key1 => $value1) { 
                foreach ($value1['orders'][0]['subscriptionInfo'] as $key2 => $value2) {
                    if($value2['unit'] == 'liter' ){       
                           $milkQty = $milkQty + $value2['quantity'];
                         }
                  }         
              }
          ?>
          <p class="details-receiver">Containers<span class="rightside">Total Milk  <?php echo $milkQty; ?> ltr</span/></p>
        </div>
        <div class="col-sm-12">
          <div class="panel">
            <div class="">
              <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <td class="text-center"><strong>Route</strong></td>
                      <td class="text-center"><strong>Driver Name</strong></td>
                      <td class="text-center"><strong>2 ltr</strong></td>
                      <td class="text-center"><strong>1.5 ltr</strong></td>
                      <td class="text-center"><strong>1 ltr</strong></td>
                      <td class="text-center"><strong>Total</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  
                    $cont1Total = 0; $cont2Total = 0; $cont3Total = 0;
                    foreach ($routesf as $key => $value) { 
                          $cont1 = 0; $cont2 = 0; $cont3 = 0;
                  ?>
                    <tr class="m1 <?php echo str_replace(' ', '', $value['route']['name']); ?>">
                      <td><?php echo $value['route']['name'] ?></td>
                      <?php foreach ($totalorders as $key1 => $value1) { 
                            if($value1['route_id'] == $value['route_id'] ){
                      
                                $cont1 = $cont1 + $value1['cont_one']; 
                                $cont2 = $cont2 + $value1['cont_oneh']; 
                                $cont3 = $cont3 + $value1['cont_two'];
                            }           
                          }
                     ?>
                      <td><?php echo $value['user']['name']; ?></td>
                      <td><?php echo $cont3; ?></td>
                      <td><?php echo $cont2; ?></td>
                      <td><?php echo $cont1; ?></td>
                      <td><?php echo $cont3+$cont2+$cont1; ?></td>
                      <?php 
                          $cont1Total = $cont1Total + $cont1;
                          $cont2Total = $cont2Total + $cont2;
                          $cont3Total = $cont3Total + $cont3;
                      ?>
                    </tr>
                   <?php 
                    } 
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="page-break"></div>

  <?php 
    foreach ($routesf as $keey => $valuee) { 
      ?>

        <div class="col-sm-12 col4" id="<?php echo str_replace(' ', '', $valuee['route']['name']); ?>">
          <p class="details-receiver">Today's order List Route Wise</p>
          <div class="panel">
            <div class="col-sm-12 noppading">
              <div class="header-defi">
                <p>Region: Panchkula</p>
                <p>Driver Name: <?php echo $valuee['user']['name']; ?></p>
              </div>
              <div class="header-defi">
                <p>Route name: <?php echo $valuee['route']['name']; ?></p>
                <p>Route timings: <?php echo $valuee['delivery_schdule']['start_time']."-".$valuee['delivery_schdule']['end_time']; ?></p>
              </div>

              <?php
                $rowcount = 0;  $milkQty11 = 0; $cntOut = 0; $cntCollect = 0; 
                $product_count = 0; $product_count1 = 0; $product_count2 = 0;    
                foreach ($totalorders as $keyyy => $valye) {                     
                  if($valuee['route_id'] == $valye['route_id'] ){  
                    foreach ($valye['orders'][0]['subscriptionInfo'] as $keyyy1 => $valye1) { 
                       if($valye1['unit'] == 'liter' ){       
                             $milkQty11 = $milkQty11 + $valye1['quantity'];
                           }
                          $product_count++;
                    }
                    if(@$valye['orders']['customOrderInfo']) 
                    if(@$valye['orders']['customOrderInfo']) 
                    {
                      foreach ($valye['orders']['customOrderInfo'] as $keyyy11 => $valey11) {                              
                         if(@$vale11['unit'] == 'liter' ){       
                           $milkQty11 = $milkQty11 + $valey11['quantity'];
                         }
                         $product_count2++;
                      }
                    }

                    
                    $cntOut = $cntOut + $valye['cont_one'] + $valye['cont_oneh'] + $valye['cont_two'];
                    $cntCollect = $cntCollect + $valye['left_container_count'];
                    $rowcount++; 
                  }   
                } $product_count1 = $product_count1 + $product_count2 + $product_count;
              ?>

              <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <td class="text-center col-sm-2"><strong>Address (<?php echo $rowcount; ?>)</strong></td>
                      <td class="text-center col-sm-2"><strong>Area</strong></td>
                      <td class="text-center col-sm-2"><strong>COW Milk (<?php echo $milkQty11; ?>)</strong></td>
                      <td class="text-center col-sm-2"><strong>cntr out (<?php echo $cntOut; ?>)</strong></td>
                      <td class="text-center col-sm-2"><strong>cntr to be collect (<?php echo $cntCollect; ?>)</strong></td>
                      <td class="text-center col-sm-2"><strong>cntr collected (0)</strong></td>
                      <td class="text-center col-sm-2"><strong>PRODUCT (<?php echo $product_count1; ?>)</strong></td>
                      <td class="text-center col-sm-2"><strong>Status</strong></td>
                      <td class="text-center col-sm-2"><strong>Action</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  $i = 0;     
                    foreach ($totalorders as $keyy => $vale) {
                     $milkQty1 = 0; $products = ''; $price = 0; $price1 = 0; $conts = '';
                     $items = array(); $itemss = array(); $subids = array();
                      if($valuee['route_id'] == $vale['route_id'] ){  
                        foreach ($vale['orders'][0]['subscriptionInfo'] as $keyy1 => $vale1) {                              
                           if($vale1['unit'] == 'liter' ){       
                             $milkQty1 = $milkQty1 + $vale1['quantity'];
                           }
                           $products = $products.",".$vale1['name'];
                           $price = $price + $vale1['price'];

                           $items['id']                 = $vale1['pro_id'];
                           $items['name']               = $vale1['name'];
                           $items['quantity']           = $vale1['quantity'];
                           $items['sub_id']             = $vale1['subscription_id'];
                           $items['pro_price']          = $vale1['price_per_unit'];
                           $items['quantity_child']     = $vale1['quantity_child'];
                           $items['child_package_qty']  = $vale1['child_package_qty'];
                           $items['pro_child_id']       = $vale1['pro_child_id'];
                           $items['pricr_per_package']  = $vale1['pricr_per_package'];
                           $items['unit_name']          = $vale1['unit'];
                           $items['modify_data']        = $vale1['modify_data'];
                           $items['type']               = $vale1['type'];
                           array_push($itemss, $items);
                           array_push($subids, $vale1['subscription_id']);
                        }
                        if(@$vale['orders']['customOrderInfo']) 
                        {
                          foreach (@$vale['orders']['customOrderInfo'] as $keyy11 => $vale11) {                              
                             if($vale11['unit'] == 'liter' ){       
                               $milkQty1 = $milkQty1 + $vale11['quantity'];
                             }
                             if($vale11['pro_child_id'] != 0){
                              $products = $products.",".$vale11['name'].'('. $vale11['child_package_qty']*$vale11['quantity_child'].' '. $vale11['unit'] .')';
                             }else{
                               $products = $products.",".$vale11['name'].'('. $vale11['quantity'].' '. $vale11['unit'] .')';
                             }

                             $price1 = $price1 + @$vale11['price'];
                          }
                        }
                          $totalPrice = $price + $price1;
                        ?>
                    
                    <tr class="<?php
                        foreach ($itemss as $kuy => $vu) { if($vu['modify_data'] == 1){ echo "grey"; } } ?>">
                      
                      <td>
                          <input type="hidden" value="<?php echo $totalPrice; ?>">
                          <?php echo $vale['address']; ?>
                      </td>
                      <td><?php echo $vale['area_name']; ?></td>
                      <td><?php echo $milkQty1; ?></td>
                      <td>
                        <?php if($vale['cont_one']>0){ $vale['cont_one']= $vale['cont_one'] ."(1 ltr),"; }else{ $vale['cont_one'] ='';} 
                        if($vale['cont_oneh'] > 0){  $vale['cont_oneh']= $vale['cont_oneh']."(1.5 ltr),"; }else{ $vale['cont_oneh'] ='';} 
                        if($vale['cont_two'] > 0){ $vale['cont_two'] =$vale['cont_two']."(2 ltr)"; } else{ $vale['cont_two'] ='';} ; 
                        $conts = $vale['cont_one']. ''.$vale['cont_oneh'].''.$vale['cont_two']; echo rtrim($conts,",");?>                        
                      </td>
                      <td><?php echo $vale['left_container_count']; ?></td>
                      <td></td>
                      <td><?php echo ltrim($products,", "); ?></td>
                       <td class="status <?php if($vale['status']=='Pending'){ ?> red1<?php } ?>"> <?php echo $vale['status']; if($vale['status'] == 'Rejected'){ echo "<br>(".$vale['reason'].")"; } ?> </td>

                       <td class="text-center col-sm-2"><strong>
                       <?php if($vale['status'] == 'Pending'){ ?>
                       <button class="deliver" id="<?php echo $i ?>" userid="<?php echo $vale['user_id']; ?>" price="<?php echo $totalPrice; ?>" cntgiven="<?php echo $vale['cont_one'] + $vale['cont_oneh'] + $vale['cont_two']; ?>" cntcollect="<?php echo $vale['left_container_count']; ?>" schedule="<?php echo $vale['schedule_id']; ?>" subids='<?php echo json_encode($subids); ?>'  itemss='<?php echo json_encode($itemss); ?>'>Deliver Now</button>
                       <?php } ?>

                       </strong> 

                       </td>
                    </tr>
                    <?php

                      } 
                      $i++;
                    }                        
                ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="page-break"></div>
<?php } ?>


        
        
      </div>
    </section>

    <!--scripts
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>-->

<script type="text/javascript">
function myFunction() {
          window.print();
      }
$('#filterUser').change(function(){
  var filval  = $('select[name=filterby]').val();
  filval      = filval.replace(/\s/g, '');
  if(filval != 'all'){
    $(".col4").hide();
    $(".m1").hide();
    $("."+filval+"").show();
    $("#"+filval+"").show();
  }else{
    $(".col4").show();
    $(".m1").show();
  }
  });

$( document ).ready(function() {
  var filval = $('select[name=filterby]').val();
  filval      = filval.replace(/\s/g, '');
  if(filval != 'all'){  
    $(".col4").hide();
    $(".m1").hide();
    $("."+filval+"").show();
    $(".deliver").hide();
    $("#"+filval+"").show();
    }else{
    $(".col4").show();
    $(".m1").show();
  }
});

  jQuery(".reset").on('click',  function() {
    $(".col4").show();
    $(".m1").show();
    });
  jQuery(".deliver").on('click',  function() {
   if (confirm("Are you sure?")) {           
    var price           = $(this).attr('price');
    var id              = $(this).attr('id');
    var schedule_id     = $(this).attr('schedule');
    var user_id         = $(this).attr('userid');
    var cntgiven        = $(this).attr('cntgiven');
    var cntcollect      = $(this).attr('cntcollect');
    var itemss          = $(this).attr('itemss');
    var subids          = $(this).attr('subids');
    var date            = "<?php echo date('Y-m-d'); ?>";
    var data            = '[{"custom_ids":"[]","delivered":"yes","items":'+itemss+',"userId":"'+user_id+'","totalOrderSubscriptionPrice":"'+price+'","containerGiven":"'+cntgiven+'","subscription_ids":"'+subids+'","date":"'+date+'","containerCollect":"'+cntcollect+'"}]';
    $.ajax({
        type: 'POST',
        url: "<?php echo HTTP_ROOT ?>DriverApi/updateDeliveryadmin",
        cache : "false",
        data: {'data': data, 'schdule_id'  : schedule_id},
        success: function(result) {
          myarray = JSON.parse(result);
            if(myarray.messageCode == 331){
              alert("Delivered Successfully");
              $("#"+id+"").hide("");
              $("#"+id+"").closest('tr').find('.status').text("Delivered");
              $("#"+id+"").closest('tr').find('.status').removeClass( "red1");


            }
          }
      });
  }else{
    return false;
  }

});
</script>

