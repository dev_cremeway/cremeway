<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }
  
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
}); 
          

</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- /.box-header -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Settings</h3>
        <div class="box-tools pull-right">
          <a href="<?php echo HTTP_ROOT?>Users/view" class="btn btn-sm btn-info btn-flat pull-left">Back to List           
          </a>
        </div>
      </div>
      <div class="box-body"> 
        <div id="err_qty" style="display:none;color:red;"></div>
        <div class="row">
          <div class='col-sm-4'>
            <form action="<?php echo HTTP_ROOT ?>Users/settings" method="post" id="adminupload" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php if(isset($data[0]['id'])){ echo $data[0]['id']; } ?>">
              <div class="form-group" id="err_div"></div>
              <div class="form-group ">
                <label for="message_notification">Time before Subscriptions Edit(Only Integer Value)</label>
                <input class="form-control" type="text" name="configuration" value="<?php if(isset($data[0]['configuration'])){ echo $data[0]['configuration']; } ?>" maxlength=2 onkeypress="return isNumber(event)">
              </div>
              
              
              <input type="submit" id="btn_submit" class="btn btn-primary" value="Submit">
              </form>
            </div>
          </div>
        </div>
        
      </div>
    </section>
  </div>

  <script type="text/javascript">
    
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
  </script>