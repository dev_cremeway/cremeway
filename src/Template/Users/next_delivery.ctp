<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .te_ad {font-size: 14px !important; display: inline-block; float: left; width: 25%; margin: 0px 12% 5px 0px; }
  .subscription_qty {width: 10%; float: left; display: inline-block; }
</style>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="">
              <div class="order-total-price">
                  
                  <?php if(isset($response['subscriptionItems']) && count($response['subscriptionItems'])>0) {
                   ?>
                  <div class="delivery-time">
                   <p class="" >Delivery time:-<?php echo $response['subscriptionItems'][0]['timeToBeDeliver'].'   '.$response['subscriptionItems'][0]['between']; ?></p>
                   <p class="" style="margin-left: 5%;"> Delivery Date:-<?php echo $response['subscriptionItems'][0]['deliverydate']; ?></p>
                   <h6 class="">Total Order Price: <?php echo 'Rs '.$response['subscriptionOrderTotalPrice']; ?></h6>
                  </div>
                   <?php } ?>
                  <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Product Name</th>
                   <th>Price(Rs)</th>  
                   <th>Quantity</th>
                   <th>Unit</th>  
                   
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                   if(isset($response['subscriptionItems']) && count($response['subscriptionItems']) > 0){ 
                       foreach ($response['subscriptionItems'] as $key => $value) {
                     ?>
                      <tr>
                      <input type="hidden" id="subid" name="subid" value="<?php echo $value['subscription_id']; ?>"> 
                      <input type="hidden" name="price_per_unit" value="<?php if(@$value['pricr_per_package']!='' && @$value['pricr_per_package']!=0){echo $value['pricr_per_package'];}else{echo $value['price_per_unit'];} ?>">
                      <td><?php echo $value['name']; ?></td>
                      <td><input type="text" disabled="disabled" value="<?php echo number_format((float)$value['price'], 2, '.', ''); ?>" name="subscription_amount"></td>
                      <td>
                        <input type="text" disabled="disabled" name="subscription_qty" class="subscription_qty" value="<?php echo $value['quantity']; ?>" disabled> 
                        <p class="te_ad"><?php if(@$value['pro_child_id'] != 0 && $value['pro_child_id'] != ''){ echo "* (".$value['child_package_qty'].' '. $value['unit'] .")"; } ?> </p>
                         <a title="Edit" class="edit_sub">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a title="update" class="update_sub" style="display: none;">
                          <i class="fa fa-check" aria-hidden="true"></i>
                        </a>

                      </td> 
                      <td><?php echo strtoupper($value['unit']); ?></td> 
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Subscriptions were found for this time</td>
                  
                </tr>
                <?php

                } ?>


           <?php if(isset($response['orderItems']) && count($response['orderItems']) > 0){ 
                       foreach ($response['orderItems'] as $key => $value) { ?>
                     
                      <tr>
                      <input type="hidden" id="orderid" name="orderid" value="<?php echo $value['id']; ?>">
                      <input type="hidden" name="price_per_unit" value="<?php if(@$value['pricr_per_package']!='' && @$value['pricr_per_package']!=0){echo $value['pricr_per_package'];}else{echo $value['price_per_unit'];} ?>">
                      <td><?php echo $value['name']; ?></td>
                      <td><input type="text" name="subscription_amount"  disabled="disabled" readonly="readonly" value="<?php echo number_format((float)$value['price'], 2, '.', ''); ?>"></td>
                      <td>
                        <input type="text" disabled name="subscription_qty" class="subscription_qty" value="<?php echo $value['quantity']; ?>">
                        <p class="te_ad"><?php if(@$value['pro_child_id'] != 0 && $value['pro_child_id'] != ''){ echo "* (".$value['child_package_qty'].' '. $value['unit'] .")"; } ?> </p>
                           
                        <a title="Edit" class="edit_order">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a title="update" class="update_order" style="display: none;">
                          <i class="fa fa-check" aria-hidden="true"></i>
                        </a>  
                      </td> 
                      <td><?php echo strtoupper($value['unit']); ?></td> 
                      
                      </tr> 
                    
                <?php
                } 
              }  
                ?>
                 
                  </tbody>
                </table>       


              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div> 
          <!-- /.box -->
       



<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}
a{
  cursor: pointer;
}

</style>
<script type="text/javascript">
  $(".edit_order").on("click", function () {
    $(this).hide();
    $(this).closest("tr").find(".update_order").show();
    var price = $(this).closest("tr").find("input[name='subscription_amount']").val();
    var quantity = $(this).closest("tr").find("input[name='subscription_qty']").val();
       
    $(this).closest("tr").find("input[name='subscription_qty']").prop('disabled', false);
    
});

  $(document).on('change','.subscription_qty',function(){

      var price_per_unit = $(this).closest("tr").find("input[name='price_per_unit']").val();
      var quantity = $(this).closest("tr").find("input[name='subscription_qty']").val();
      var total_price = quantity * price_per_unit;
      $(this).closest("tr").find("input[name='subscription_amount']").val(total_price);
    });

  var user_id = $("#user_customer_id").val();

  $(".update_order").on("click", function () {
    $(this).closest("tr").find("input[name='subscription_qty']").prop('disabled', true);
   $(this).hide();
   $(this).closest("tr").find(".edit_order").show();
    var orderid = $(this).closest("tr").find("input[name='orderid']").val();
    var sub_quantity = $(this).closest("tr").find("input[name='subscription_qty']").val();
    var finalprice = $(this).closest("tr").find("input[name='subscription_amount']").val();
    $.ajax({
            url: "<?php echo HTTP_ROOT  ?>Users/modifyOrder",
            cache: false,
            type: 'POST',
            data:{'user_id':user_id,'s_id':orderid,'sub_qty':sub_quantity,'pr_price1':finalprice,},
            success: function(unitname){               
               //location.reload();
               alert("Order has been updated successfully");               

             }
          });
  });


  $(".edit_sub").on("click", function () {
    $(this).hide();
    $(this).closest("tr").find(".update_sub").show();
    var price = $(this).closest("tr").find("input[name='subscription_amount']").val();
    var quantity = $(this).closest("tr").find("input[name='subscription_qty']").val();
       
    $(this).closest("tr").find("input[name='subscription_qty']").prop('disabled', false);
    
});

  $(".update_sub").on("click", function () {
    $(this).closest("tr").find("input[name='subscription_qty']").prop('disabled', true);
    $(this).hide();
    $(this).closest("tr").find(".edit_sub").show();
    var orderid = $(this).closest("tr").find("input[name='subid']").val();
    var sub_quantity = $(this).closest("tr").find("input[name='subscription_qty']").val();
    var finalprice = $(this).closest("tr").find("input[name='subscription_amount']").val();
    $.ajax({
            url: "<?php echo HTTP_ROOT  ?>Users/modifySubscription",
            cache: false,
            type: 'POST',
            data:{'user_id':user_id,'s_id':orderid,'sub_qty':sub_quantity},
            success: function(unitname){               
               //location.reload();
               alert("Subscription has been modified successfully");              
               
             }
          });
  });




</script>

