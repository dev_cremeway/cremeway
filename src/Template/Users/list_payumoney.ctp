<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }

</style>
<script type="text/javascript">
  $(document).ready(function(){

      $(".success").fadeOut(4000);

  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- /.box-header -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">PayUmoney Accounts</h3>
        <a href="<?php echo HTTP_ROOT ?>Users/add-payumoney" class="btn btn-default pull-right">Add New</a>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Merchant Key</th>
            <th>Merchant Salt</th>
            <th>Status</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
          <?php foreach ($data as $key => $value) { ?>
            <tr>
              <td><?php echo htmlentities($value->id, ENT_QUOTES, 'utf-8'); ?> </td>
              <td><?php echo htmlentities($value->merchant_key, ENT_QUOTES, 'utf-8'); ?></td>
              <td><?php echo htmlentities($value->merchant_salt, ENT_QUOTES, 'utf-8'); ?></td>
              <td><span class="label label-success"><?php echo $value->status ?></span></td>
              <td><?php echo htmlentities($value->description, ENT_QUOTES, 'utf-8'); ?></td>
              <td>
                <a class="label label-warning" href="<?php echo HTTP_ROOT ?>Users/add-payumoney/<?php echo $value->id; ?>">Edit</a>
                <!-- <a class="label label-danger" href="">Delete</a> -->
              </td>
            </tr>
          <?php } ?>
        </table>
      </div>
      <!-- /.box-body -->


      </div>
    </section>
  </div>

  <script type="text/javascript">

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
  </script>
