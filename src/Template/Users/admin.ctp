<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>

<script type="text/javascript">
 $(document).ready(function(){
        $("#getarea").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
                  Admin Listing 
              </h1>
             
          <div class="customer_bts">
             
             
   <a href="<?php echo HTTP_ROOT ?>Users/adminadd"><button type="button" class="btn btn-sm btn-info ">Add New Admin</button></a>

     <a href="<?php echo HTTP_ROOT ?>permissions/index"><button type="button" class="btn btn-sm btn-info ">Manage Permissions</button></a>




             </div>
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            






 


            <div class="box-header ">
         

              
 

 

             




     
 <div class="left-serach-option">
 
              <div class="input-group">
                <input type="text" placeholder="Search by name,phone no." class="form-control" name="q">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
           
            </div> 
   <div class="right-filter-option">
               <div class="box-filter">  
              <form action="admin" id="filteruserby" method="get">
                            
                             
                            
                                <select class="form-control" name="role_id" id="getarea">
                                  <option value="">Filter By Role</option>
                                    <?php if(isset($groups)&&count($groups)>0){
                                               
                                               foreach ($groups as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($role_id)&&!empty($role_id))
                                                 {
                                                    if($key == $role_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add role First</option>  
                                        <?php
                                        } ?>
                                  
                                  
                                </select>
                                
                    </form>           
                              
                 
              </div>

<a href="<?php echo HTTP_ROOT ?>Users/admin"><button type="button" class="btn btn-sm btn-info">Reset filters</button></a>

              
 


            
            </div>

</div>






            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th> <?php echo $this->Paginator->sort('Users.name', 'Name'); ?></th>
                   <th><?php echo $this->Paginator->sort('Users.phoneNo', 'Phone No.'); ?></th> 
                   <th><?php echo $this->Paginator->sort('Groups.name', 'Role'); ?></th></th>
                   <th><?php echo $this->Paginator->sort('Users.email_id', 'Email'); ?></th>  
                   <th>Profile pic</th>  
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php /* pr($user);die;*/
                   if(isset($user) && count($user) > 0){ 
                       foreach ($user as $key => $value) {
                     ?>
                      <tr>
              <td><?php echo $value['name']; ?></td>
                      
                    


                      
                      
                      <td><?php echo $value['phoneNo']; ?></td>
                      
                      <td>

                        <?php 
                              
                               echo  $value['group']['name'];

                        ?></td>
                       
                      
                      <td><?php echo $value['email_id']; ?></td>
                      <td>
                       
                      <?php
                       if($value['image'] && ! empty($value['image'])){
                        ?>
                         <img style="height:50px;width:50px;" src="<?php echo $value['image'] ?>">
                        <?php
                       }else{
                        ?>
                        <img style="height:50px;width:50px;" src="<?php echo HTTP_ROOT."img/user.jpg"; ?>">
                        <?php
                       } 
                      ?> 
                      
                      
                      </td>


                      
                      
                      
                       
                      <td>

                      
                      
                      <!--<a class="underconstruction edit_icon" href="<?php echo HTTP_ROOT ?>Users/edit/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>-->

                       <a class="underconstruction delete_icon" href="<?php echo HTTP_ROOT ?>Users/delete/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a>

                        

                      
                    <?php
                       if($value['status'] == 0 && $value['type_user'] == 'customer')
                       {
                     ?>  

                    <a  href="<?php echo HTTP_ROOT ?>Users/Activate/<?php echo base64_encode($value['id']);  ?>">
                     Activate
                      </a>
                      <?php }  
                        ?>
                        

                      




                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red">Not Any Users Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>



<?php

            if( count( $user ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php } ?>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



           























          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>