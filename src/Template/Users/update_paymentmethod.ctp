<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }

</style>
<script type="text/javascript">
  $(document).ready(function(){

      $(".success").fadeOut(4000);

  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
                Dashboard
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Update <?php echo $data->method ?> Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="">
              <div class="box-body">
                <?php if (isset($data)): ?>
                  <div class="form-group">
                    <label for="method_id" class="col-sm-2 control-label">Method Id</label>

                    <div class="col-sm-8">
                      <input type="text" value="<?php echo (isset($data)) ? $data->id : '' ; ?>" class="form-control" id="method_id" disabled>
                    </div>
                    <input type="hidden" name="method_id" value="<?php echo (isset($data)) ? $data->id : '' ; ?>">
                  </div>

                  <div class="form-group">
                    <label for="method" class="col-sm-2 control-label">Method</label>

                    <div class="col-sm-8">
                      <input type="text" value="<?php echo (isset($data)) ? $data->method : '' ; ?>" class="form-control" id="method" disabled>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if ($data->method=='paytm'||$data->method=='payumoney'): ?>
                  <div class="form-group">
                    <label for="merchant_key" class="col-sm-2 control-label">Merchant key</label>

                    <div class="col-sm-8">
                      <input name="merchant_key" type="text" value="<?php echo (isset($data)) ? $data->merchant_key : '' ; ?>" class="form-control" id="merchant_key" placeholder="Merchant key downloaded from portal" maxlength="100" required>
                    </div>
                  </div>

                <?php endif; ?>
                <?php if ($data->method=='paytm'): ?>
                  <div class="form-group">
                    <label for="merchant_mid" class="col-sm-2 control-label">Merchant mid</label>

                    <div class="col-sm-8">
                      <input name="merchant_mid" type="text" value="<?php echo (isset($data)) ? $data->merchant_mid : '' ; ?>" maxlength="100" class="form-control" id="merchant_mid" placeholder="MID (Merchant ID) received from Paytm" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="merchant_website" class="col-sm-2 control-label">Merchant website</label>

                    <div class="col-sm-8">
                      <input name="merchant_website" type="text" value="<?php echo (isset($data)) ? $data->merchant_website : '' ; ?>" class="form-control" id="merchant_website" placeholder="Website name received from Paytm" maxlength="100" required>
                    </div>
                  </div>

                <?php endif; ?>
                <?php if ($data->method=='payumoney'): ?>
                  <div class="form-group">
                    <label for="merchant_salt" class="col-sm-2 control-label">merchant_salt</label>

                    <div class="col-sm-8">
                      <input name="merchant_salt" maxlength="100" type="text" value="<?php echo (isset($data)) ? $data->merchant_salt : '' ; ?>" class="form-control" id="merchant_salt" placeholder="Merchant Salt as provided by Payu" required>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (isset($data)): ?>
                  <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">status</label>

                    <div class="col-sm-8">
                      <select name="status" class="form-control" id="status">
                        <option <?php echo ($data->status==0) ? 'selected' : '' ; ?> value="0">Disabled</option>
                        <option <?php echo ($data->status==1) ? 'selected' : '' ; ?> value="1">Enabled</option>
                      </select>
                    </div>
                  </div>
                <?php endif; ?>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>

                  <div class="col-sm-8">
                    <textarea name="description" maxlength="200" type="text" class="form-control" id="description" placeholder="description"><?php echo (isset($data)) ? $data->description : '' ; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->

                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
  </div>

  <script type="text/javascript">

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
  </script>
