<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  } 
 
</style>

<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header header_dashbord">
      <div class="box box-info">
        <div class="box-header">
        <div class="">
        <?php
          if(isset($transactions) && count($transactions) > 0){ 
             foreach ($transactions as $key => $value) {
              echo "<h3>".$value['user']['name']."</h3>"; echo "<h4>".$value['id']."</h4>";;
             }
          } 
        ?>
        </div>
          <div class="box-tools pull-right">         
            <a href="<?php echo $this->request->referer(); ?>" class="btn btn-sm btn-info btn-flat pull-left">Back </a>         
          </div>  
          
        </div>
    
      </div>
    <div class="customer_bts">
      
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th> Item No. </th> 
                  <th> Product Name </th>
                  <th> Quantity </th>
                  <th> Type </th>
                  <th> Price </th>
                  <th> Status </th>
                </tr>
              </thead>
              <tbody>
              <?php if($transactions[0]['transaction_amount_type'] != 'Cr' && $transactions[0]['transaction_type_id'] != 7){ 
                   if(isset($transactions) && count($transactions) > 0){ 
                       foreach ($transactions as $key => $value) {
                        //if(isset($value['order_data'])){
                        if(isset($customOrders)){
                          //$items = json_decode($value['order_data'],TRUE);
                          //$items1 = '';
                          $i=1;
                          foreach ($customOrders as $keey => $valuee) {
                     ?>
                          <tr>
                            <td>
                              <?php echo $i; ?> 
                            </td>
                            <td><?php echo $valuee['product']['name']; ?></td>
                            <td> 
                              <?php echo $valuee['quantity']." ".$valuee['product']['unit']['name']; ?>                    
                            </td>
                            <td> Custom Order </td>
                            <td>
                              <?php echo $valuee['price']; ?> 
                            </td>     
                            <td><?php if($value['transaction_amount_type'] == 'rejected' && $value['rejected'] == 1 )  { echo "Rejected"; }else{ echo "Delivered"; } ?> </td>           
                            
                            
                          </tr>
                            <?php
                            $i++;
                          }
                }
                if(isset($userSub)){
                        
                          foreach ($userSub as $keey1 => $valuee1) {
                     ?>
                          <tr>
                            <td>
                              <?php echo $i; ?> 
                            </td>
                            <td><?php echo $valuee1['product']['name']; ?></td>
                            <td> 
                              <?php echo $valuee1['quantity']." ".$valuee1['product']['unit']['name']; ?>                    
                            </td>
                            <td>
                             Subscription
                            </td>
                            <td>
                              <?php echo number_format($valuee1['subscriptions_total_amount'], 2, '.', ''); ?> 
                            </td>                
                            <td><?php if($value['transaction_amount_type'] == 'rejected' && $value['rejected'] == 1 )  { echo "Rejected"; }else{ echo "Delivered"; } ?> </td>
                            
                          </tr>
                            <?php
                            $i++;
                          }
                }
              } ?>
              <tr colspan="4" style="font-size: 18px; font-weight: 900">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total Price</td>
                    <td><?php echo number_format($value['amount'], 2, '.', ''); ?></td>
                  </tr>
            <?php 
              }else{
                ?>
                  <tr colspan="4">
                    <td style="color:red">Not Any Transactions Found</td>
                  </tr>
                  <?php } ?>
                  <?php  
            }else{ $i=1;
              foreach ($transactions as $kkey => $vvalue) {
              ?>
              <tr>
                <td> <?php echo $i; ?> </td> 
                <td><?php echo $vvalue['transaction_type']['transaction_type_name']; ?></td>
                <td> -- </td>
                <td> <?php echo $vvalue['transaction_amount_type']; ?> </td>
                <td> <?php echo number_format($vvalue['amount'], 2, '.', ''); ?> </td>
                <td> <?php if($value['status'] == 1 || $value['status'] == "")  { echo "Successful"; }else{ echo "Unsuccessful"; } ?> </td>                
              </tr>
              <?php 
              if($value['transaction_amount_type'] == "Cr" && ($value['transaction_type_id'] == 2 || $value['transaction_type_id'] == 8) ){ 
                if($value['transaction_type_id'] == 2) {echo "<tr><td colspan='2'><h3>Paytm Response</h3></td></tr>";}
                else {echo "<tr><td colspan='2'><h3>PayuMoney Response</h3></td></tr>";}
                $paytm_res  = json_decode($value['order_data'],true);
                foreach ($paytm_res as $kp => $vp) { ?>

                  <tr>
                    <td><?php echo $kp;  ?></td>
                    <td colspan="5"><?php echo $vp;  ?></td>
                  </tr>
              <?php    
                }
                ?>
              
              <?php
              }
              }
              
            } ?>

                </tbody>
              </table>
            </div>
          
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
    display: inline-block !important;
    margin-left: 10px;
    position: relative;
    width: 9%;
    vertical-align: top;
    /* float: left; */
}
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>