<?php echo $this->Html->script('multiselect'); ?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }
  /* multiselect */
  .dropdown-menu{ top : 0; position: relative !important;min-width: 100%; }
  span.multiselect-native-select {
  position: relative
}
span.multiselect-native-select select {
  border: 0!important;
  clip: rect(0 0 0 0)!important;
  height: 1px!important;
  margin: -1px -1px -1px -3px!important;
  overflow: hidden!important;
  padding: 0!important;
  position: absolute!important;
  width: 1px!important;
  left: 50%;
  top: 30px
}
.multiselect-container {
  position: absolute;
  list-style-type: none;
  margin: 0;
  padding: 0
}
.multiselect-container .input-group {
  margin: 5px
}
.multiselect-container>li {
  padding: 0
}
.multiselect-container>li>a.multiselect-all label {
  font-weight: 700
}
.multiselect-container>li.multiselect-group label {
  margin: 0;
  padding: 3px 20px 3px 20px;
  height: 100%;
  font-weight: 700
}
.multiselect-container>li.multiselect-group-clickable label {
  cursor: pointer
}
.multiselect-container>li>a {
  padding: 0
}
.multiselect-container>li>a>label {
  margin: 0;
  height: 100%;
  cursor: pointer;
  font-weight: 400;
  padding: 3px 0 3px 30px
}
.multiselect-container>li>a>label.radio, .multiselect-container>li>a>label.checkbox {
  margin: 0
}
.multiselect-container>li>a>label>input[type=checkbox] {
  margin-bottom: 5px
}
.btn-group>.btn-group:nth-child(2)>.multiselect.btn {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px
}
.form-inline .multiselect-container label.checkbox, .form-inline .multiselect-container label.radio {
  padding: 3px 20px 3px 40px
}
.form-inline .multiselect-container li a label.checkbox input[type=checkbox], .form-inline .multiselect-container li a label.radio input[type=radio] {
  margin-left: -20px;
  margin-right: 0
}
  /* multiselect */
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
}); 
          

</script>  
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
            </section>    
    <!-- Main content -->
    <section class="content">     
            <!-- /.box-header -->
          <div class="box box-info">

              <div class="box-header with-border">
                  <h3 class="box-title">New Notification</h3>
                  <div class="box-tools pull-right">
                   
                    <a href="<?php echo HTTP_ROOT?>Users/view" class="btn btn-sm btn-info btn-flat pull-left">Back to List </a>
                   
                </div>
              </div>              
            
            <div class="box-body">
            
            <div id="err_qty" style="display:none;color:red;"></div>

             <div class="row">
                  <div class='col-sm-4'>
                  
                   <form action="<?php echo HTTP_ROOT ?>Users/customnotifications" method="post" id="adminupload" enctype="multipart/form-data">
                   
                      
                      <div class="form-group"> 
                        <label><input checked="" name="notifictaion_type" value="notification" type="radio">Send Notification</label>
                        <label><input name="notifictaion_type" value="sms" type="radio">Send Sms</label>                  
                      </div>

                      <div class="form-group">
                      <label for="message_notification">Select Area</label>
                        <select id="dates-field2" class="multiselect-ui form-control" multiple="multiple" name="area_id[]">
                          <?php foreach ($areas as $ke => $va) { ?>
                           <option value="<?php echo $ke ?>"><?php echo $va ?></option>
                         <?php } ?>
                        </select>
                      <!-- <select class="form-control" id="getunit" name="area_id">
                         <option value="">Select Area</option>
                         <?php foreach ($areas as $ke => $va) { ?>
                           <option value="<?php echo $ke ?>"><?php echo $va ?></option>
                         <?php } ?>
                      </select> -->
                      </div>
                      <div class="form-group"> 
                        <label for="message_notification">Message</label>
                        <textarea name="message" id="message" maxlength="256" rows="7" cols="108"></textarea>
                      </div>
                      <div class="form-group" id="err_div"></div>
                      <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 

                   </form>          
            </div> 
          </div> 
        </div> 

        <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th><?php echo $this->Paginator->sort('UserNotifications.id', 'Id'); ?></th>
                   <th><?php echo $this->Paginator->sort('UserNotifications.message', 'Message'); ?></th>
                   <th>Sent date</th>
                   
                    
                  </tr>
                  </thead>
                  <tbody> 
                  <?php  //print_r($user);die;
                   if(isset($user) && count($user) > 0){ 
                       foreach ($user as $key => $value) {
                     ?>
                      <tr>
              

              <td class="
               <?php if( isset($value['user_balances'][0]['balance'] )
                    &&
                  $value['user_balances'][0]['balance'] <= CUSTOMER_LOW_BALANCE_ALERT ) { echo 'alert alert-danger'; } ?>">

              <a target="_blank" href="<?php echo HTTP_ROOT ?>Users/customerProfile?id=<?php echo base64_encode($value['id']);?>"><?php echo $value['id']; ?></a>

              </td>
              <td>
                 <?php echo $value['message']; ?>
              </td>
              <td><?php echo $value['date']->format('d-M-Y'); ?></td>

                      </tr>                     
                <?php
                } 
              }else{
                ?>                 
                  <tr colspan="4"><td style="color:red">No notification found</td></tr>

                <?php } ?>

                  </tbody>
                </table>
                    <?php
                      if( count( $user ) > 0 )
                      {  ?>
                              <div class="text-right">
                                 <div class="paginator">
                                      <nav>
                                         <ul class="pagination">
                                              <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                               <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                              <?= $this->Paginator->next(__('next') . ' >') ?> 
                                          </ul>
                                      </nav>
                                    <?php echo $this->Paginator->counter(
                                      'showing {{current}} records out of
                                       {{count}} total'); ?>

                                </div>      
                              </div>
                    <?php } ?>
              </div>


      </div> 
    </section>
</div>

<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});

$("#btn_submit").on('click',function(){
  var area    = $("#dates-field2 option:selected").val();
  var message = $("#message").val();

  if((! $('#dates-field2').val() )) {
    alert("Please select an Area");
  }else if(message == ""){
    alert("Please enter a message");
  }else{
      $("#adminupload").submit();
    }
});

$("#message").on('keyup', function(event) {
    var currentString = $("#message").val()
    if (currentString.length >= 256 )  { 
    $("#err_div").html("Maximum 256 characters allowed"); 
    } else{
      $("#err_div").html('');
    }
});
</script>