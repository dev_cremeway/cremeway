<?php //echo $this->Html->script('driverprofileupdate'); ?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  ul#tabs {
    list-style-type: none;
    padding: 0;
    text-align: left;
}
ul#tabs li {
    display: inline-block;
   
    padding: 10px 23px;
    margin-bottom: 4px;
    color: #000;
    cursor: pointer;
}
ul#tabs li:hover {
    background-color: #ddd;
}
ul#tabs li.active {
    background-color: #337ab7; color:#fff; border-top:3px solid #3c8dbc;  padding: 10px 23px;
}
ul#tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
ul#tab li {
    display: none;
}
ul#tab li.active {
    display: block;
}
</style>
<script type="text/javascript">
 $(document).ready(function(){


     $(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});

$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
}); 

        $("#filterUser").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });

 $(document).ready(function(){
    $("ul#tabs li").click(function(e){
        if (!$(this).hasClass("active")) {
            var tabNum = $(this).index();
            var nthChild = tabNum+1;
            $("ul#tabs li.active").removeClass("active");
            $(this).addClass("active");
            $("ul#tab li.active").removeClass("active");
            $("ul#tab li:nth-child("+nthChild+")").addClass("active");
        }
    });
});
</script>
<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header header_dashbord">
    <h1>
                  Driver Profile Page 
              </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="box box-info">
      <div class="box-header ">
        <ul id="tabs">
          <li class="active"> Profile</li>
          <li>Vehicle Information</li>
          <li>Route</li>
        </ul>

        <div class="box-tools pull-right">
          <a href="<?php echo HTTP_ROOT; ?>Users/driver" class="btn btn-sm btn-info btn-flat pull-left">Back 
          </a>
        </div>
        
      </div>
      <div class="box-body">
        <ul id="tab">
          <li class="active">
            <div class="box-body">
              <!-- DESIGN WILL COME HERE -->
              <div id="resultspc"></div>
              <div id="err_qty" style="display:none;color:red;"></div>
              <div class="row">
                <div class='col-sm-4'>
                  <form action="driverProfile" method="post" id="adminupload" enctype="multipart/form-data">
                    <div class="form-group" id="err_div"></div>
                    <div class="form-group required">
                      <?php if(isset($error['name'])){
                          ?>
                      <p class="red">
                        <?php echo $error['name']; ?>
                      </p>
                      <?php
                          } ?>
                      <label for="exampleInputEmail1">Name</label>
                      <input type="text" required class="form-control" id="name" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                      </div>
                      <div class="form-group required">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" required class="form-control" id="username" name="username" value="<?php if(isset($data['username'])){ echo $data['username']; } ?>" disabled>
                        </div>
                        <!-- <div class="form-group">
                          <label for="exampleInputEmail1">Email Id</label>
                          <input type="text" class="form-control" id="emailid" name="email_id" value="<?php if(isset($data['email_id'])){ echo $data['email_id']; } ?>">
                          </div> -->
                          <div class="form-group required">
                            <label for="exampleInputEmail1">Password(Minimum 8 characters.)</label>
                            <input type="password" required class="form-control" id="password" name="password" value="acxsgcxxzs">
                            </div>
                            <div class="form-group required">
                              <label for="exampleInputEmail1">Phone No.</label>
                              <input type="text" maxlength="10" required class="form-control" id="phoneNo" name="phoneNo" value="<?php if(isset($data['phoneNo'])){ echo $data['phoneNo']; } ?>">
                              </div>
                              <!--<div class="form-group">
                                <label for="exampleInputEmail1">Region</label>
                                <input type="text" class="form-control" name="region_id" value="<?php if(isset($data['region_id'])) { echo $data['region_id']; } ?>">
                                 <select class="form-control" id="getarea" required name="region_id">
                                  <option value="">Select Region</option>
                                  <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                  <option value="<?php echo $key ?>"
                                                     
                                                     
                                    <?php if($key==$data['region']['id']){
                                                      ?>
                                                      selected="selected"
                                                      
                                    <?php
                                                     } ?>


                                                   >
                                    <?php echo strtoupper($value); ?>
                                  </option>
                                  <?php
                                               }

                                    }else{
                                        ?>
                                  <option>Please add Region First</option>
                                  <?php
                                        } ?>
                                </select> 
                              </div> -->
                             <!--  <div class="form-group">
                                <label for="exampleInputEmail1">Area</label>
                                <input type="text" class="form-control" name="area_id" value="<?php if(isset($data['area_id'])) { echo $data['area_id']; } ?>"> -->
                                <!-- <select class="form-control" id="ajaxarea" required name="area_id">
                                  <option value="">Select Area</option>
                                  <?php if(isset($areas)&&count($areas)>0){
                                               
                                               foreach ($areas as $key => $value) {
                                                   ?>
                                  <option value="<?php echo $key ?>"
                                                     
                                                     
                                    <?php if($key==$data['area']['id']){
                                                      ?> selected="selected" <?php
                                                      } ?> >
                                    <?php echo strtoupper($value); ?>
                                  </option>
                                  <?php
                                               }


                                    }else{
                                        ?>
                                  <option>Please add Area First</option>
                                  <?php
                                        } ?>
                                </select> 
                              </div>-->
                              <div class="form-group required">
                                <label for="exampleInputEmail1">Address</label>
                                <input type="text" required class="form-control" id="houseNo" name="houseNo" value="<?php if(isset($data['houseNo'])){ echo $data['houseNo']; } ?>">
                                </div>
                                <input type="hidden" id="user_customer_id" name="id" value="<?php echo $data['id'] ?>">
                                  <div class="form-group">
                                    <?php
                   
                    if(isset($data['image'])&&!empty($data['image'])){
                      ?>
                                    <img style="height:100px;width:100px;" src="
                                      <?php echo HTTP_ROOT.$data['image'] ?>">
                                      <?php
                    }
             ?>
                                    </div>
                                    <div class="form-group">
                                      <input type="file" name="image" class="file">
                                        <div class="input-group col-xs-12">
                                          <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-picture"></i>
                                          </span>
                                          <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                                            <span class="input-group-btn">
                                              <button class="browse btn btn-primary input-lg" type="button">
                                                <i class="glyphicon glyphicon-search"></i> Browse
                                              </button>
                                            </span>
                                          </div>
                                        </div>
                                        <input type="button" id="btn_submit" class="btn btn-primary" value="Update">
                                        </form>
                                      </div>
                                    </div>
                                    <!-- /.box-body -->
                                  </li>
                                  <li>
                                    <?php if(count($driverVehicle)>0) {

                                foreach ($driverVehicle as $k => $va) {
                                  ?>
                                    <p>
                                      <?php echo $va['vehicle']['vehicle_no'] ?>
                                    </p>
                                    <?php
                                }

                              }else {
                                ?>
                                    <p>No Vehicle Assigined To This Driver</p>
                                    <?php
                                }?>
                                  </li>
                                  <li>
                                    <div class="box-body">
                                      <div class="table-responsive">
                                        <table class="table no-margin">
                                          <thead>
                                            <tr>
                                              <th>Route Name</th>
                                              <th>Region</th>
                                              <th>Area</th>
                                              <th>Timing</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php

                   if(isset($driverRoute) && count($driverRoute) > 0){ 
                       foreach ($driverRoute as $key => $value) {
                     ?>
                                            <tr>
                                              <td>
                                                <?php echo $value['route']['name']; ?>
                                              </td>
                                              <td>
                                                <?php echo $value['region']['name']; ?>
                                              </td>
                                              <td>
                                                <?php echo $value['area']['name']; ?>
                                              </td>
                                              <td>
                                                <?php echo $value['delivery_schdule']['name'].' '.$value['delivery_schdule']['start_time'].' To '.$value['delivery_schdule']['end_time']; ?>
                                              </td>
                                            </tr>
                                            <?php
                } 
              } else{ 
                ?>
                                            <tr>
                                              <td colspan="3" style="color:red;">No Route were found</td>
                                            </tr>
                                            <?php

                } ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                  </div>
                  <style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}
 .red{
    color:red;
  }
  .success{
    color: green;
  }
  .file {
  visibility: hidden;
  position: absolute;
}
 #err_div{
    color:red;
  }
  
</style>
                  <style type="text/css">


.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>
<script type="text/javascript">
  
  $(document).ready(function(){
 

  $("#getarea").on('change', function(){

     
        var region_id = this.value;
        var i=0;
           var htmloption = '';  
           $.ajax({
            url: "<?php echo HTTP_ROOT ?>Users/getArea",
            cache: false,
            data:{'id':region_id},
            success: function(regionList){

               var targetHtml = $("#ajaxarea");
                //              
               var regionList = JSON.parse(regionList);
               var length = jQuery.isEmptyObject(regionList);
               if(!length){
                 $.each( regionList, function( key, value ) {
                         i++;
                       });
              }
              if(i > 1){
               htmloption = '<option value="">Select Area</option>'; 
             }
               if(!length){ 
                    $.each( regionList, function( key, value ) { 
                       htmloption+='<option value='+key+'>'+value+'</option>';
                     });
               }else{
                  htmloption = '';
                  htmloption+='<option value="">Please add Area to this region First</option>';
               }
              targetHtml.html(htmloption); 
            }
          });

        }); 


  $("#btn_submit").on('click',function(){

        
      var name = $.trim($("#name").val());  
      var username = $.trim($("#username").val());   
      var phoneNo = $.trim($("#phoneNo").val());   
      var region = $("#getarea option:selected").val();
      var area = $("#ajaxarea option:selected").val();
      var houseNo = $.trim($("#houseNo").val()); 
      //var email_id = $.trim($("#emailid").val());
      var password = jQuery.trim ( $("#password").val() );
      


        var user_id  = $('#user_customer_id').val();
        var phoneno = /^\d{10}$/;
        var result1;
       $.ajax({
          url: "ismobilealreadyExistEdit11",
          cache: false,
          type: 'POST',
          data:{'phone':phoneNo,'user_id':user_id},
          success: function(result){
            var result1 = result;
            if(result =="found"){ 
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("This mobile number already exists.Please choose another.");
              
              } else if(result =="invalid"){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Invalid mobile number.");
              } else if(name == ''){
              $("#err_div").html("Please enter the name");
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
             }else if(username == ''){
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("Please enter the Username");
             }/*else if(email_id == ''){
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("Please enter the Email id");
             }*/else if(password == ''){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter the password");
               }else if (/\s/.test(password)) {
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Spaces are not allowed in password");
               }else if(password.length < 8 ){
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                $("#err_div").html("");
                $("#err_div").html("Please enter password of at least 8 character");
               }else if(phoneNo == ''){
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("Please enter the phone");
             }else if((! phoneNo.match(phoneno)))  
              { 
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow"); 
                $("#err_div").html("");
                $("#err_div").text("Please enter a correct phone no");  
              }/*else if(region == ''){
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("Please Select The Region First");
             }else if(area == ''){
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              $("#err_div").html("");
              $("#err_div").html("Please Select The Area First");
             }*/ else if(houseNo == ''){
                $("#err_div").html("");
                $("#err_div").html("Please enter the Address");
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                return false
             }else{
                  $("#adminupload").submit();
                } 

        }
    });


               


  });

});
</script>