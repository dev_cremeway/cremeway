<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<?php 
   /*echo $this->Html->script('editsubscription');*/
   
   ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
   .file {
   visibility: hidden;
   position: absolute;
   }
   #err_div{
   color:red;
   }
   .qty_icon {
   position: absolute;
   right: 0;
   top: 34px;
   }
   .qty_form .qty_icon .fa {
   color: #367fa9;
   font-size: 32px;
   margin-right: 10px;
   }
   .qty_form{ position: relative; }
   .append input {
   padding: 0 10px;
   width: 90%;
   }
   .append select {
   padding: 0 10px;
   width: 90%;
   }
   .heading_up{ width: 100%; padding: 0; display: inline-block; }
   .heading_up li {
   display: inline-block;
   list-style: outside none none;
   width: 30%;
   }
   .append{ width: 100%; padding: 0; display: inline-block; }
   .append div {
   display: inline-flex;
   list-style: outside none none;
   width: 30%;
   }
   #loadingmessage {
   position: absolute;
   left: 0;
   top: 0;
   bottom: 0;
   right: 0;
   background: #000;
   opacity: 0.8;
   filter: alpha(opacity=80);
   }
   #loading {
   width: 50px;
   height: 50px;
   position: absolute;
   top: 50%;
   left: 50%;
   margin: -28px 0 0 -25px;
   }
   .weekDays-selector input {
   display: none!important;
   }
   .weekDays-selector input[type=checkbox] + label {
   display: inline-block;
   border-radius: 6px;
   background: #dddddd;
   height: 40px;
   width: 30px;
   margin-right: 3px;
   line-height: 40px;
   text-align: center;
   cursor: pointer;
   }
   .weekDays-selector input[type=checkbox]:checked + label {
   background: #2AD705;
   color: #ffffff;
   }
   .subscription_label{width:100%;}
   .subscription_amount{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:80px; background:#fff; border:1px solid #ddd; box-shadow::none;}
   .container_calculations{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:510px;background:#fff; border:1px solid #ddd; box-shadow::none;}
   .box-lining{display:inline-block; width:100%; }
   #forchieldProduct input[type="text"]{width:70px;}
   #forchieldProduct input[type="radio"]{position:relative; top:7px; margin-right:5px;}
   #forchieldProduct label{margin-left:10px;}
</style>
<?php
   echo $this->Html->script('moment.js');  
   echo $this->Html->script('bootstrap-datetimepicker.js');  
   ?>
<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
      
       $(".success").fadeOut(4000);
   
   });   
   
</script>
<script>
   $( function() {
     $( "#datepicker" ).datepicker({
   dateFormat: "yy-mm-dd",
   minDate: 0
   });
   } );
</script>
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header">
      <h1>
         Dashboard
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>

         <li><?= $this->Html->link(__('Back'), $this->request->referer()) ?></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header with-border">
            <h3 class="box-title">Edit Subscription</h3>
         </div>
         <div class="box-body">
            <div id="err_qty" style="display:none;color:red;"></div>
            <div class="row">
               <div class='col-sm-9'>
                  <form id="editsub_form" method="post" action="">
                     <tr>
                        <input type="hidden" id="user_id1" name="user_id" value="<?php echo $userSubscription['user_id'] ?>">
                        <input type="hidden" name="pro_child_id" value="<?php echo @$userSubscription['pro_child_id'] ?>">
                        <input type="hidden" id= "product_id" name="product_id" value="<?php echo $userSubscription['product_id'] ?>">
                        <input type="hidden" name="s_id" value="<?php echo $userSubscription['id'] ?>">
                        <input type="hidden" id="pr_price" name="pr_price" value="<?php echo $userSubscription['product']['price_per_unit'] ?>">
                        <input type="hidden" name="pr_id" value="<?php echo $userSubscription['product']['id'] ?>">
                        <input type="hidden" name="category_id" class="category_id" value="<?php echo $userSubscription['product']['category_id'] ?>">
                        <input type="hidden" name="container" class="container_count" value="">
                        <input type="hidden" name="old_container" class="container_count1" value="">
                     </tr>
                     <div class="form-group" id="err_div"></div>
                     <div class="form-group" id="err_div_green" style="color:green;"></div>
                     <div class="form-group">
                        <?php if(isset($error['name'])){
                           ?>
                        <p class="red"><?php echo $error['name']; } ?></p>
                        
                     </div>
                     
                    <div class="form-group" style="display: none">
                    
                       <label for="exampleInputEmail1">Select Category</label>
                       <select class="form-control" id="getcategory"  name="category_id">
                        <option rel="<?php echo $userSubscription['product']['category_id'] ?>" value="<?php echo $userSubscription['product']['category_id'] ?>" selected="selected"><?php echo $userSubscription['product']['category_id'] ?></option>
                      
                      </select>
                      
                    </div> 

                     <div class="form-group"> 
                        <label for="exampleInputEmail1">Product</label>
                        <input type="text"  class="form-control"  name="pro_name" value="<?php echo $userSubscription['product']['name']; ?>" disabled> 
                     </div>
                     <div class="box-lining">
                        <div class="row" id="forchieldProduct"></div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="form-group" style="position:relative;">
                                 <label for="exampleInputEmail1">Quantity</label>                                
                                 <input type="text"  class="qty form-control" id="manualquantity" name="sub_qty" value="<?php echo $userSubscription['quantity']; ?>" container="">
                                 <label style="top:30px; position:absolute; right:10px;" for="exampleInputEmail1" id="unitname"></label>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="form-group">
                                 <div id="price_subscriptionamount" style="display:none"> 
                                    <label for="exampleInputEmail1" class="subscription_label"><!-- Subscription Amount --> Product package price</label>
                                    <input readonly="readonly" name="subscription_price" type="text" id="subscriptionamount" class="subscription_amount"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="form-group">
                                 <div id="price_userbalance" style="display:none">  
                                    <label for="exampleInputEmail1" class="subscription_label">Customer Balance</label>         
                                    <input type="text" readonly="readonly" id="userbalance" class="subscription_amount">  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3">
                           <div id="appendforcalculatingcontainers"></div>
                        </div>
                     </div>
                     <div class="form-group" id="get_timingdelivery">
                     </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Selected Subscription Amount</label>
                     <input type="text" readonly="readonly" id="userbalance" name="pr_price1" class="subscription_amount" value="<?php echo $userSubscription['subscriptions_total_amount']; ?>">
                     </div>

                     <div class="form-group">                      


                        <label for="exampleInputEmail1">Select Subscription type</label>
                        <select class="form-control" id="subscriptiontypes"  name="sub_id">
                           <!-- <option value="<?php echo $userSubscription['subscription_type']['id']; ?>"><?php echo $userSubscription['subscription_type']['subscription_type_name']; ?></option> -->

          
                           <?php if(isset($subscriptionType)&&count($subscriptionType)>0){
                              foreach ($subscriptionType as $key => $value) {
                                  ?>
                           <option <?php if($userSubscription['subscription_type']['id'] == $value['id']) { ?> selected <?php } ?>
                           value="<?php echo $value['id']; ?>"> <?php echo $value['subscription_type_name']; ?></option>
                           <?php
                              }
                              
                              }else{
                              ?>
                           <option>Please add subscription Types First</option>
                           <?php
                              } ?>
                        </select>
                     </div>

                     <div class="form-group">
                        <label for="exampleInputEmail1">Delivery Schedule</label>

                        <select class="form-control" id="subscriptiontypes"  name="schedule_id">
                           <<!-- option value="<?php echo $userSubscription['delivery_schdule_ids'] ?>"><?php echo $userSubscription['delivery_schdule_ids']; ?></option> -->

                           <?php if(isset($getSchedule)&&count($getSchedule)>0){
                              foreach ($getSchedule as $ke => $val) {
                                  ?>
                           <option <?php if($userSubscription['delivery_schdule_ids'] == $val['d_s_id']) { ?> selected <?php } ?> value="<?php echo $val['d_s_id'] ?>"><?php echo $val['name']; ?></option>
                           <?php
                              }
                              
                              }else{
                              ?>
                           <option>No driver is available</option>
                           <?php
                              } ?>
                        </select> 

                        <?php /*
                      
                     if(isset($getSchedule)&&count($getSchedule)>0){
                              foreach ($getSchedule as $ke => $val) { ?>
                        <div class="checkbox">
                           <label>
                              <input <?php if($userSubscription['delivery_schdule_ids'] == $val['d_s_id']) { ?> checked <?php } ?>id="delivery_schdule_<?php echo $val['d_s_id'] ?>" name="schedule_id[]" value="<?php echo $val['d_s_id'] ?>" type="checkbox"><?php echo $val['name']; ?>
                           </label>
                        </div>
                        <?php
                              }
                              
                              }else{
                              ?>
                           <option>No driver is available for this route</option>
                           <?php
                              } */?>

                     </div>

                     <div class="form-group">
                        <div class="weekDays-selector" id="weekdays" style="display:none">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">Start Date</label>
                        <input type="text" name="start_date" class="form-control" id="datepicker" value="<?php echo $userSubscription['startdate']->format('Y-m-d'); ?>" >
                     </div>

                     <div class="form-group">
                        <label for="exampleInputEmail1">Notes</label>
                        <textarea name="notes" class="form-control"  value="<?php echo $userSubscription['notes']; ?>"><?php echo $userSubscription['notes']; ?> </textarea>
                     </div>

               </div>
            </div>
            <input type="submit" id="submit" class="btn btn-primary" value="Submit"> 
            </form> 
            <div id='loadingmessage' style="display:none;">
               <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
            </div>
         </div>
      </div>
</div>
</div> 
</section>
</div>

<script type="text/javascript">
  $(document).ready(function(){  
    var globalUserId = $('#user_id1').val();
    $("#subscriptionamount").val('');
    // $("#manualquantity").val('');
    $("#manually-show-default").show();
    var product_id = $("#product_id").val();
    globalProductId = product_id;
    var htmloption = '';  
     $.ajax({
      url: "<?php echo HTTP_ROOT  ?>Users/getProductUnitName",
      cache: false,
      data:{'pro_id':product_id},
      success: function(unitname){               
         var result = JSON.parse(unitname);
         console.log('result',result);
         var targetHtml = $("#unitname");
         targetHtml.html(result.unitname); 
         
         globalUnitNameProductParent = result.unitname;

         console.log('globalUnitNameProductParent',globalUnitNameProductParent);

         var chieldProduct = $("#forchieldProduct");
         var makeHtmlProductChieldrten = '';
         $.each(result.childproduct,function(key,value){

            globalUnitNameProductChildren = value.unit;

            makeHtmlProductChieldrten+='<div class="col-sm-3 form-inline"><div class="form-group" style="position:relative;"><input c_price="'+value.price+'" value="'+value.p_c_i+'" rel="'+value.quantity+'" type="radio" name="pro_child_id"><input value="'+value.quantity+'" readonly="readonly" type="text" class="form-control"><label>'+value.unit+'</label></div><br><br></div>';
         });
          chieldProduct.empty();
          chieldProduct.append(makeHtmlProductChieldrten);
          chieldProduct.append('<button type="button" class="btn btn-primary" id="enter-qty-manuall" style="display:none;">Enter Manually</button><br><input type="text"  name="sub_qty" id="qty1" style="display:none;">');
       }
    }); 

     $(document).on('click', 'input[name="pro_child_id"]', function () {

      var globalQty = 0;
      var $radio = $(this);      
      var price = $radio.attr('c_price');
      $("#pr_price").val(price);
   $("#enter-qty-manuall").show();
   $("#qty1").show();
   $("#appendforcalculatingcontainers").empty();
   if ($radio.data('waschecked') == true) { //alert('unchecked');
      globalchieldCheck = 0;
      globalchieldproductqty = 0;

      $("#setUnitName").val('');

      $("#manualquantity").prop("disabled", false);
      //$radio.prop('checked', false);
      // $radio.data('waschecked', false);
      $("#subscriptionamount").val('');

   } else {
      globalchieldCheck = 1;
      $("#setUnitName").val(globalUnitNameProductChildren);
      globalchieldproductqty = $radio.val();
      $("#manualquantity").val('');
      $("#manualquantity").prop("disabled", true);
      //$radio.prop('checked', true);
      //$radio.data('waschecked', true);
      var p_c_i = $radio.attr('value');
      console.log('p_c_i', p_c_i);

      $.ajax({
         url: "<?php echo HTTP_ROOT  ?>Users/getProductChildrenPriceAndCustomerBalance",
         cache: false,
         data: {
            'pro_c_id': p_c_i,
            'customer_id': globalUserId
         },
         success: function (response) {

            var result = JSON.parse(response);

            if (result.statuscode == 300) {
               $("#price_subscriptionamount").show();
               $("#price_userbalance").show();
               $("#userbalance").val(0);

               $("#subscriptionamount").val(result.subscriptionprice);
               $('html, body, .content-wrapper').animate({
                  scrollTop: 0
               }, "slow");
               $("#err_div").show();
               document.getElementById("submit").disabled = true;
               $("#err_div").text("Customer have low balance. Please recharge first.");
               setTimeout(function () {
                  $("#err_div").hide();
               }, 10000);

            } else {
               document.getElementById("submit").disabled = false;
               $("#price_subscriptionamount").show();
               $("#price_userbalance").show();

               $("#subscriptionamount").val(result.subscriptionprice);
               $("#userbalance").val(result.userbalance);
            }

            var html = $("#appendforcalculatingcontainers");

            console.log('globalQty', globalQty);
            console.log('globalchieldproductqty', globalchieldproductqty);
            if (globalQty > 0 || globalchieldproductqty > 0) {
               if (globalchieldproductqty > 0 && globalQty == 0) {
                  globalQty = globalchieldproductqty;
               }
               $.ajax({
                  url: "<?php echo HTTP_ROOT  ?>Users/calculateContainers",
                  cache: false,
                  data: {
                     'qty': globalchieldproductqty,
                     'pro_id': globalProductId
                  },
                  success: function (response) {
                     var cnt_cnt = JSON.parse(response);
                     if (cnt_cnt.status == 200 && cnt_cnt.containers > 0) {
                        document.getElementById("submit").disabled = false;
                        var cnt = '<label for="exampleInputEmail1">Calculated Continares</label> <input readonly="readonly" value="' + cnt_cnt.containers + '" name="subscription_containers" type="text" id="container-count" class="subscription_amount">';
                        console.log(response);
                        html.append(cnt);
                     } else if (cnt_cnt.status == 404) {

                        html.empty();

                     } else {
                        document.getElementById("submit").disabled = false;
                        var cnt = ' <input readonly="readonly" value="No Rule Found For Calculating Container For this quantity" name="subscription_containers" type="text" id="container-count" class="container_calculations">';
                        console.log(response);
                        html.append(cnt);
                     }


                  }
               });
            } else {

               $('html, body, .content-wrapper').animate({
                  scrollTop: 0
               }, "slow");
               $("#err_div").show();
               $("#err_div").text("Please enter the quantity");
               setTimeout(function () {
                  $("#err_div").hide();
               }, 2000);

            }

         }
      });
   }

   $radio.siblings('input[type="radio"]').data('waschecked', false);

});
$('#manualquantity').on("input", function () {
   $("#appendforcalculatingcontainers").empty();
   // $("#forchieldProduct").empty();

   console.log('globalUnitNameProductParent', globalUnitNameProductParent);
   var dInput = this.value;
   globalQty = dInput;
   if ((isFloat(dInput) || isInteger(dInput)) && dInput != '') {

      if (globalProductId != '' && globalUserId != '') {


         $("#setUnitName").val(globalUnitNameProductParent);
         $.ajax({
            url: "getProductPriceAndCustomerBalance",
            cache: false,
            data: {
               'pro_id': globalProductId,
               'qty': dInput,
               'customer_id': globalUserId
            },
            success: function (response) {

               var result = JSON.parse(response);

               if (result.statuscode == 300) {
                  $("#price_subscriptionamount").show();
                  $("#price_userbalance").show();
                  $("#userbalance").val(0);

                  $("#subscriptionamount").val(result.subscriptionprice);
                  $('html, body, .content-wrapper').animate({
                     scrollTop: 0
                  }, "slow");
                  $("#err_div").show();
                  document.getElementById("submit").disabled = true;
                  $("#err_div").text("Customer dont have the balance");
                  setTimeout(function () {
                     $("#err_div").hide();
                  }, 10000);

               } else if (result.statuscode == 308) {

                  $("#err_div").hide();
                  $("#err_div").show();
                  $('html, body, .content-wrapper').animate({
                     scrollTop: 0
                  }, "slow");
                  document.getElementById("submit").disabled = true;
                  $("#err_div").text("Entered quantity not available.");

                  setTimeout(function () {
                     $("#err_div").hide();
                  }, 10000);

                  return false;
               } else {

                  document.getElementById("submit").disabled = false;
                  $("#price_subscriptionamount").show();
                  $("#price_userbalance").show();

                  $("#subscriptionamount").val(result.subscriptionprice);
                  $("#userbalance").val(result.userbalance);
               }
               var html = $("#appendforcalculatingcontainers");

               console.log('globalQty', globalQty);
               console.log('globalchieldproductqty', globalchieldproductqty);
               if (globalQty > 0 || globalchieldproductqty > 0) {
                  if (globalchieldproductqty > 0 && globalQty == 0) {
                     globalQty = globalchieldproductqty;
                  }
                  $.ajax({
                     url: "<?php echo HTTP_ROOT  ?>Users/calculateContainers",
                     cache: false,
                     data: {
                        'qty': globalQty,
                        'pro_id': globalProductId
                     },
                     success: function (response) {
                        var cnt_cnt = JSON.parse(response);
                        if (cnt_cnt.status == 200 && cnt_cnt.containers > 0) {
                           document.getElementById("submit").disabled = false;
                           var cnt = ' <label for="exampleInputEmail1">Calculated Continares</label><input readonly="readonly" value="' + cnt_cnt.containers + '" name="subscription_containers" type="text" id="subscriptionamount" class="subscription_amount">';
                           console.log(response);
                           html.append(cnt);

                        } else if (cnt_cnt.status == 404) {

                           html.empty();

                        } else {
                           document.getElementById("submit").disabled = true;
                           var cnt = ' <input readonly="readonly" value="No Rule Found For Calculating Container For this quantity" name="subscription_containers" type="text" id="subscriptionamount" class="container_calculations">';
                           console.log(response);
                           html.append(cnt);
                        }
                     }
                  });
               } else {

                  $('html, body, .content-wrapper').animate({
                     scrollTop: 0
                  }, "slow");
                  $("#err_div").show();
                  $("#err_div").text("Please enter the quantity");
                  setTimeout(function () {
                     $("#err_div").hide();
                  }, 2000);
               }
            }
         });

      } else {
         this.value = '';
         $("#err_div").show();
         $('html, body, .content-wrapper').animate({
            scrollTop: 0
         }, "slow");
         $("#err_div").text("Please select Product and Customer First");
         setTimeout(function () {
            $("#err_div").hide();
         }, 3000);
      }

   } else {

      $("#err_div").show();
      $('html, body, .content-wrapper').animate({
         scrollTop: 0
      }, "slow");
      $("#err_div").text("Please enter valid quantity No.");
      $("#subscriptionamount").val('');
      setTimeout(function () {
         $("#err_div").hide();
      }, 5000);
   }
});

   var pr_id1 =  $("#product_id").val();
   var qty11 =  $("#manualquantity").val();
   var price1 = $("#pr_price").val();

   var subscription_amount1 = price1 * qty11;

   //$(".subscription_amount").attr('value',subscription_amount);

   $.ajax({
   type: 'POST',
   url: "<?php echo HTTP_ROOT  ?>Users/calculateContainers",
   cache : "false",                    
   data: {'pro_id': pr_id1, 'qty': qty11 },
   success: function(resultData) {
      myarray = JSON.parse(resultData);

   $(".container_count1").attr('value',myarray.containers);
   return;        
   }

   }); 
    $('.qty').bind('input', function() {

                      var pr_id =  $("#product_id").val();
                      var qty1 =  $(this).val();
                      var price = $("#pr_price").val();

                      var subscription_amount = price * qty1;
                      
                      $(".subscription_amount").attr('value',subscription_amount);

                      $.ajax({
                        type: 'POST',
                        url: "<?php echo HTTP_ROOT  ?>Users/calculateContainers",
                        cache : "false",                    
                        data: {'pro_id': pr_id, 'qty': qty1 },
                        success: function(resultData) {
                           myarray = JSON.parse(resultData);
                        
                        $(".container_count").attr('value',myarray.containers);
                        

                        return;        
                      
                    }

                  });
              });
});

$("#submit").click(function(){
   if($('input:radio:checked').length > 0){     
      var title = "checked";
   }else{
      var title = "not checked";
   }
   var qty1    = $('#qty1').val();
   var qty2    = $('#manualquantity').val();
   if(qty2 != "" && qty2 != 0){
      return true;
   }else if( $.trim(title) == "not checked" && qty1 == "" ){
      alert("Please select a package.");
      return false;
   }else if(qty1 == "" || qty1 == 0 ){
      alert("Please enter a valid quantity.");
      return false;
   }else{
      return true;
      $("#editsub_form").submit();
   }
});

</script>