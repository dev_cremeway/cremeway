<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<?php 
   /*echo $this->Html->script('editsubscription');*/
   //echo $this->Html->script('addcustomorder');
   ?>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  -->
<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
   .file {
   visibility: hidden;
   position: absolute;
   }
   #err_div{
   color:red;
   }
   .qty_icon {
   position: absolute;
   right: 0;
   top: 34px;
   }
   .qty_form .qty_icon .fa {
   color: #367fa9;
   font-size: 32px;
   margin-right: 10px;
   }
   .qty_form{ position: relative; }
   .append input {
   padding: 0 10px;
   width: 90%;
   }
   .append select {
   padding: 0 10px;
   width: 90%;
   }
   .heading_up{ width: 100%; padding: 0; display: inline-block; }
   .heading_up li {
   display: inline-block;
   list-style: outside none none;
   width: 30%;
   }
   .append{ width: 100%; padding: 0; display: inline-block; }
   .append div {
   display: inline-flex;
   list-style: outside none none;
   width: 30%;
   }
   #loadingmessage {
   position: absolute;
   left: 0;
   top: 0;
   bottom: 0;
   right: 0;
   background: #000;
   opacity: 0.8;
   filter: alpha(opacity=80);
   }
   #loading {
   width: 50px;
   height: 50px;
   position: absolute;
   top: 50%;
   left: 50%;
   margin: -28px 0 0 -25px;
   }
   .weekDays-selector input {
   display: none!important;
   }
   .weekDays-selector input[type=checkbox] + label {
   display: inline-block;
   border-radius: 6px;
   background: #dddddd;
   height: 40px;
   width: 30px;
   margin-right: 3px;
   line-height: 40px;
   text-align: center;
   cursor: pointer;
   }
   .weekDays-selector input[type=checkbox]:checked + label {
   background: #2AD705;
   color: #ffffff;
   }
   .subscription_label{width:100%;}
   .subscription_amount{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:80px; background:#fff; border:1px solid #ddd; box-shadow::none;}
   .container_calculations{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:510px;background:#fff; border:1px solid #ddd; box-shadow::none;}
   .box-lining{display:inline-block; width:100%; }
   #forchieldProduct input[type="text"]{width:70px;}
   #forchieldProduct input[type="radio"]{position:relative; top:7px; margin-right:5px;}
   #forchieldProduct label{margin-left:10px;}
</style>
<?php
   echo $this->Html->script('moment.js');  
   echo $this->Html->script('bootstrap-datetimepicker.js');  
   ?>
<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
      
       $(".success").fadeOut(4000);
   
   });   
   
</script>
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header">
      <h1>
         Dashboard
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>

         <li><?= $this->Html->link(__('Back'), $this->request->referer()) ?></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header with-border">
            <h3 class="box-title">Edit Subscription</h3>
         </div>
         <div class="box-body">
            <div id="err_qty" style="display:none;color:red;"></div>
            <div class="row">
               <div class='col-sm-9'>
                  <form id="editorder" name="editorder" method="post" action="">
                     <tr>
                        <input type="hidden" id="user_idq" name="user_id" value="<?php echo $userSubscription['user_id'] ?>">
                        <input type="hidden" id= "product_id" name="product_id" value="<?php echo $userSubscription['product_id'] ?>">
                        <input type="hidden" id= "pro_child_id" name="pro_child_id" value="<?php echo @$userSubscription['pro_child_id'] ?>">  
                        <input type="hidden" name="s_id" value="<?php echo $userSubscription['id'] ?>">
                        <input type="hidden" id="pr_price" name="pr_price" value="<?php echo $userSubscription['product']['price_per_unit'] ?>">
                        <input type="hidden" name="pr_id" value="<?php echo $userSubscription['product']['id'] ?>">
                        <input type="hidden" name="category_id" class="category_id" value="<?php echo $userSubscription['product']['category_id'] ?>">
                        <input type="hidden" name="container" class="container_count" value="">
                     </tr>
                     <div class="form-group" id="err_div"></div>
                     <div class="form-group" id="err_div_green" style="color:green;"></div>
                     <div class="form-group">
                        <?php if(isset($error['name'])){
                           ?>
                        <p class="red"><?php echo $error['name']; } ?></p>
                        
                     </div>
                     <div class="form-group">
                      <label for="exampleInputEmail1">Select Category</label>
                      <select class="form-control" id="getcategory1"  name="category_id" disabled="disabled">
                        <option value="">Select Category</option>
                        <?php if(isset($categories)&&count($categories)>0){                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                        <option <?php if( $key == $userSubscription['product']['category_id'] ) { ?> selected <?php } ?> rel="<?php echo $value; ?>" value="<?php echo $key ?>">
                          <?php echo $value; ?>
                        </option>
                        <?php
                                               }

                                    }else{
                                        ?>
                        <option>Please add Category First</option>
                        <?php
                                        } ?>
                      </select>
                    </div>                    

                     <!-- <div class="form-group"> 
                        <label for="exampleInputEmail1">Product</label>
                        <input type="text"  class="form-control"  name="pro_name" value="<?php echo $userSubscription['product']['name']; ?>" disabled> 
                     </div> -->
                     <input type="hidden" name="pro_name" value="<?php echo $userSubscription['product']['id'] ?>">
                      <div class="form-group">
                      <label for="exampleInputEmail1">Select Product</label>
                      <select class="form-control" id="getcategory_product1"  name="pro_name1" disabled="disabled">
                        <option value="">Select Product</option>
                        <?php if(isset($products)&&count($products)>0){                                               
                                               foreach ($products as $key => $value) {
                                                   ?>
                        <option <?php if( $key == $userSubscription['product']['id'] ) { ?> selected <?php } ?> rel="<?php echo $value; ?>" value="<?php echo $key ?>">
                          <?php echo $value; ?>
                        </option>
                        <?php
                                               }

                                    }else{
                                        ?>
                        <option>Please add Category First</option>
                        <?php
                                        } ?>
                      </select>
                    </div> 
                     <div class="box-lining">
                        <div class="row" id="forchieldProduct"></div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="form-group" style="position:relative;">
                                 <label for="exampleInputEmail1">Quantity</label>                                
                                 <input type="text"  class="qty form-control" id="manualquantity" name="sub_qty" value="<?php echo $userSubscription['quantity']; ?>" container="">
                                 <label style="top:30px; position:absolute; right:10px;" for="exampleInputEmail1" id="unitname"></label>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="form-group">
                                 <div id="price_subscriptionamount" style="display:none"> 
                                    <label for="exampleInputEmail1" class="subscription_label">Subscription Amount</label>
                                    <input readonly="readonly" name="subscription_price" type="text" id="subscriptionamount" class="subscription_amount"> 
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="form-group">
                                 <div id="price_userbalance" style="display:none">  
                                    <label for="exampleInputEmail1" class="subscription_label">Customer Balance</label>         
                                    <input type="text" readonly="readonly" id="userbalance" class="subscription_amount">  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3">
                           <div id="appendforcalculatingcontainers"></div>
                        </div>
                     </div>
                     <div class="form-group" id="get_timingdelivery">
                     </div>
                     <div class="box-lining">
                     <table>
                      <tbody>                        
                        <tr>  
                          <th>Order Amount(Rs)</th>
                        </tr>
                        <tr>
                          <td>
                            <input type="text" readonly="readonly" id="userbalance" name="pr_price1" class="subscription_amount" value="<?php echo $userSubscription['price']; ?>">
                          </td>  
                        </tr>                       
                       </tbody>
                     </table>
                     </div>

               </div>
            </div>
            <input type="button" id="submit1" class="btn btn-primary" value="Submit"> 
            </form> 
            <div id='loadingmessage' style="display:none;">
               <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
            </div>
         </div>
      </div>
</div>
</div> 
</section>
</div>

<script type="text/javascript">
  $(document).ready(function(){  
    $(document).on('change','#getcategory_product1',function(){
      $("#manualquantity").val('');
    });
    $("#submit1").click(function(e){
      e.preventDefault
      var product_id = $('#getcategory_product1 :selected').val();
      var quantity = $('#manualquantity').val();

      if( !product_id){
        alert('please select a product');
      } else if( !quantity ||quantity == 0 ){
        alert('please enter quantity');
      } else{
        $("form[name='editorder']").submit();
      }
      
    });

   var product_id = $("#product_id").val();
          globalProductId = product_id;
          var htmloption = '';  
          
    $('.qty').bind('input', function() {

        var pr_id         = $("#getcategory_product1").find('option:selected').val();
        var pro_child_id  = $("#pro_child_id").val();
        var qty1 =  $(this).val();
        if(pro_child_id != '' && pro_child_id != 0){
          $.ajax({
            url: "<?php echo HTTP_ROOT  ?>Users/getProductUnitNamechild",
            cache: false,
            data:{'pro_child_id':pro_child_id},
            success: function(unitname){               
               var result = JSON.parse(unitname);
               $(".subscription_amount").attr('value',result.price_per_unit*qty1);
             }
          });
        }else{
          $.ajax({
            url: "<?php echo HTTP_ROOT  ?>Users/getProductUnitName",
            cache: false,
            data:{'pro_id':pr_id},
            success: function(unitname){               
               var result = JSON.parse(unitname);
               $(".subscription_amount").attr('value',result.price_per_unit*qty1);
             }
          });
        }
        
    });


    $(document).on('change','#getcategory1',function(){ 
          globalbase_url = window.location.origin+'/cremeway/';
   globalProductId = 0;
   globalUserId = 0; 
   globalCategoryId = 0;
   anydiliveryplace = 0;
   lowBalance = 0;
   globalQty = 0;
   globalchieldCheck = 0;
   globalchieldproductqty = 0;
   globalUnitNameProductChildren = '';
   globalUnitNameProductParent = '';
   var globalUserId = $("#user_idq").val();
   $("#get_timingdelivery1").html('');
   $("#subscriptionamount1").val('');
   $("#manualquantity1").val('');       
   if(globalUserId == 0){

    $("#getcategory1").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");

               $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                          $("#err_div").show();
                           $("#err_div").text("Please select the customer first");
                           setTimeout(function(){
                            $("#err_div").hide(); 
                           },10000);
      return false;
    }

           $("#get_timingdelivery1").html('');
           $("#subscriptionamount1").val('');
           $("#manualquantity1").val('');
           var getcategory_id = $("option:selected", this).val();
           
           globalCategoryId = getcategory_id;
           var htmloption = '';  
           $.ajax({
            url: "<?php echo HTTP_ROOT ?>Users/getProduct",
            cache: false,
            data:{'cat_id':getcategory_id,'customer_id':globalUserId},
            success: function(regionList){
               var targetHtml = $("#getcategory_product1");
                htmloption = '<option value="">Select Product</option>';               
               var regionList = JSON.parse(regionList);
               var length = true;
               if(length){ 
                    $.each( regionList.products, function( key, value ) { 
                       htmloption+='<option value='+key+'>'+value+'</option>';
                     });
               }else{
                  htmloption = '';
                  htmloption+='<option value="">Please add subscribable Product in this category First</option>';
               }
              targetHtml.html(htmloption);

             
            if(length)
            {
                   var timing = '';
                   var timingHtml = $("#get_timingdelivery1");
                   //var length_dsid = getLength(regionList.deliverytiming);
                   var length_dsid = true;
                    if(length_dsid){ 
                      anydiliveryplace = 1;
                      $.each( regionList.deliverytiming, function( key, value ) { 

                        timing+='<div class="checkbox"><label><input type="checkbox"  id="delivery_schdule_'+value.d_s_id+'" name="delivery_schdule_ids[]" value='+value.d_s_id+'>'+value.name+'</label></div>';
                           });

                     }else{
                      $("#getcategory1").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
                      anydiliveryplace = 0;
                      document.getElementById("submit").disabled = true;
                        
                                 $("#err_div").show();
                                 $("#err_div").text("We are not provide any of the time this customers area");
                                 setTimeout(function(){
                                  $("#err_div").hide(); 
                                 },10000);
                        
                     }
                    //timingHtml.html(timing);

              }

            }




          });

        }); 
});

</script>