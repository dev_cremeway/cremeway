<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });
 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
             
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
              </ol>
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">List Subscription Type</h3>

              <div class="box-tools pull-right">
                 
                <!--   <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Subscription Type </a> -->
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Subscription Name</th>  
                   <th>Created date</th>
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(isset($subscription_list) && count($subscription_list > 0)){ 
                       foreach ($subscription_list as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['subscription_type_name']; ?></td>
                      <td><?php echo $value['created']->format('Y-m-d'); ?></td> 
                      <td>
                      <a class="underconstruction" href="<?php echo HTTP_ROOT ?>UserSubscriptions/deleteSubscription?id=<?php echo base64_encode($value['id']);  ?>">
                      <i class="icon ion-close-round" title="delete" style="cursor:pointer;"></i>
                      </a>
                      </td> 
                      </tr> 
                    
                <?php
                } 
              } ?>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Subscription Type</h3>

               
            </div>
            
            <div class="box-body">

                 <?php if(isset($success['subscription_type_name'])){
                ?>
                <div class="success"><?php echo $success['subscription_type_name']; ?></div>
                <?php
                } ?>
                 <form action="listSubscriptionsType" method="post">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    
                    <input type="text" required class="form-control" id="" name="subscription_type_name">
                    <?php if(isset($error['subscription_type_name'])){
                      ?>
                      <p class="red"><?php echo $error['subscription_type_name']; ?></p>
                      <?php
                      } ?>
                    
                  </div>
                  
                  <input type="submit" class="btn btn-primary" value="Submit">

                
                </form>

               
               
            </div>
            
          </div>
























          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



