 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  #appendforcalculatingcontainers > label {
    display: none;
}
</style>

<script type="text/javascript">
 $(document).ready(function(){
        $("#filterUser").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>


<script>
(function($, undefined){
  /*console.log("$", $('#datepicker'));*/
  $(".datepicker" ).datepicker({ 
    dateFormat: "yy-mm-dd",
    minDate: 1
  });
})($);
</script>


<?php 
  
   /*echo $this->Html->css('sol');*/
      echo $this->Html->script('addsubscription');
  
 ?>

<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}

.weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
  
.subscription_label{width:100%;}
.subscription_amount{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:80px; background:#fff; border:1px solid #ddd; box-shadow::none;}


.container_calculations{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:510px;background:#fff; border:1px solid #ddd; box-shadow::none;}


.box-lining{display:inline-block; width:100%; }
#forchieldProduct input[type="text"]{width:70px;}
#forchieldProduct input[type="radio"]{position:relative; top:7px; margin-right:5px;}
#forchieldProduct label{margin-left:10px;}

</style>

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>



<script>
(function($, undefined){
  /*console.log("$", $('#datepicker'));*/
  $("#datepicker1" ).datepicker({ 
    dateFormat: "yy-mm-dd",
    minDate: 1
  });
})($);
</script>
            <!-- /.box-header -->
            <div class="box-body">
   <div class="left_box" id="hide-box1">
     <h4 class="heading_title">
                    <a href="#">Customer Subscription</a>
                </h4>
            
                 <div class="table-responsive">

                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Product</th>
                   <th>Subscription Type</th>  
                   <th>Quantity</th>
                   <!-- <th>Unit</th>  --> 
                   <th>Amount</th>
                   <th>Started</th>
                   <th>Schedule</th>
                   <th>Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $ik = 0;
                   if(isset($userSubscription) && count($userSubscription) > 0){ 
                    //pr($userSubscription); die();
                       foreach ($userSubscription as $key => $value) {

                     ?>
                        <tr>
                        <input type="hidden" name="s_id" value="<?php echo $value['id'] ?>">
                        <input type="hidden" name="pr_price" value="<?php echo $value['product']['price_per_unit'] ?>">
                        <input type="hidden" name="pr_id" value="<?php echo $value['product']['id'] ?>">
                        <input type="hidden" name="category_id" class="category_id" value="<?php echo $value['product']['category_id'] ?>">

                        <td><?php echo $value['product']['name']; ?></td>
                        
                         <td sub_id="<?php echo $value['subscription_type']['id']; ?>"><?php //echo $value['subscription_type']['subscription_type_name']; ?>
                           <select name="sub_id" disabled>
                            <?php /*foreach ($subscriptionTypes as $k => $value1) { ?>
                             <option value="<?php echo $value1['id'] ?>"><?php echo $value1['subscription_type_name'] ?></option>
                             <?php } */ ?>
                             <?php 

                                $output = '';
                                for( $i=0; $i<count($subscriptionTypes); $i++ ) {
                                   $output .= '<option value= " ' .  $subscriptionTypes[$i]['id'] . ' " '
                                             . ( $value['subscription_type']['subscription_type_name'] == $subscriptionTypes[$i]['subscription_type_name'] ? 'selected="selected"' : '' ) . '>' 
                                             . $subscriptionTypes[$i]['subscription_type_name']
                                             . '</option>';
                                }
                                echo $output;
                             ?>
                           </select>


                         </td>
                        
                        <td><input style="min-width: 105px;" type="text" name="qty" class="qty" value="<?php echo $value['quantity']." ".strtoupper($value['unit_name']); ?>" id="cont<?php echo $ik ?>" container="" disabled></td>
                        <!-- <td><?php echo strtoupper($value['unit_name']); ?></td> -->

                        <td><?php echo 'Rs '.$value['subscriptions_total_amount'].'.00'; ?></td>
   
                        <td>
                          
                          <div class="form-group">
                              
                             <input type="text" name="start_date1" class="form-control datepicker" value="<?php echo @$value['startdate']->format('d-M-Y'); ?>" disabled>
                            </div> 

                        </td>
                        
                        <td id="get_timingdeliverya">
                            <select class="form-control" id="get_timingdeliverya<?php echo $ik; ?>"  name="delivery_schdule_ids" disabled>
                            <option> Click edit to change</option>
                            </select>
                        
                        </td>
                          
                        <td>
                        
                             
                             <?php if($value['users_subscription_status']['name'] == 'active') 
                              {
                             ?> 
                              <a onclick="return confirm('Are you sure you want to PAUSE this subscription?')" title="Pause Subscription" href="<?php echo HTTP_ROOT ?>UserSubscriptions/changeStatus?id=<?php echo base64_encode($value['id']) ?>&type=paused&redirected_ID=<?php echo base64_encode($value['user_id']) ?>">
                                   
                                    <i class="fa fa-pause"></i>
                              
                              </a>
                              <?php
                              } ?>

                               <?php if($value['users_subscription_status']['name'] == 'paused') 
                              {
                                if(@$data['status'] == 0){ ?>
                                <a onclick="return alert('Customer account is deactivated')" title="Start Subscription">
                                      <i class="fa fa-play"></i>
                                  </a>

                            <?php    
                                }else{
                             ?> 
                                  <a onclick="return confirm('Are you sure you want to START AGAIN this subscription?')" title="Start Subscription" href="<?php echo HTTP_ROOT ?>UserSubscriptions/changeStatus?id=<?php echo base64_encode($value['id']) ?>&type=play&redirected_ID=<?php echo base64_encode($value['user_id']) ?>">
                                      <i class="fa fa-play"></i>
                                  </a>
                              <?php
                                }
                              } ?>

                        
                        <a title="cancel Subscription" href="<?php echo HTTP_ROOT ?>UserSubscriptions/delete?id=<?php echo base64_encode($value['id']) ?>&type=delete&redirected_ID=<?php echo base64_encode($value['user_id']) ?>" onclick="return confirm('Are you sure you want to CANCEL this subscription?')" class="delete_icon" href="javascript:void(0)">
                             
                             <i class="fa fa-trash-o" aria-hidden="true"></i>

                        </a>

                        <a title="Edit" class="edit_sub" href="<?php echo HTTP_ROOT ?>UserSubscriptions/edit/<?php echo base64_encode($value['id']);  ?>" target="blank"><i class="fa fa-edit"></i></a>
                          <a style="display: none" title="Update subscription" onclick="return confirm('Are you sure you want to UPDATE this subscription?')" class="update_icon" href="javascript:void(0)">
                              <i class="fa fa-pencil"></i>
                          </a>

                        </td> 

                        

                        </tr> 
                    
                <?php
                $ik++;
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Subscription were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>
      </div>
       <div class="right_box">
     <a href="#" class="btn_add" id="btn1">Add Subscription</a>

   </div>
</div>
  <div class="left_box1" id="hide-box2">
     <h4 class="heading_title">
                    <a href="#">Add Subscription</a>
                    <div class="right_box">
     <a href="#" class="btn_add1" id="btn2">Customer Subscription</a>

   </div>
                </h4>
            
      <div id="err_qty" style="display:none;color:red;"></div>
             <div class="row">
                  <div class='col-sm-8'>
                   
                   <form id="addProductForm" method="post" action="customerSubscription">

                   
                   
                      
                           <div class="form-group">



                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>
                            
                                  

 
                                 <label for="exampleInputEmail1" style="display:none;">Select User</label>
                                
                                 <select class="form-control" id="userlist"  name="user_id" style="display:none;">
                                  
                                    <?php if(isset($users)&&count($users)>0){
                                               
                                               foreach ($users as $key => $value) {
                                                   ?>
                                              <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add User First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>




                              <div class="form-group">
 
                                 <label for="exampleInputEmail1">Select Category</label>
                                
                                 <select class="form-control" id="getcategory"  name="category_id">
                                   <option value="">Select Category</option>
                                    <?php if(isset($categories)&&count($categories)>0){
                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                                                   <option rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Category First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>


                               <div class="form-group"> 
 
                                 <label for="exampleInputEmail1">Product</label>
                                
                                   <select class="form-control" id="getcategory_product"  name="product_id">
                                   </select>
                                
                              </div>




                                
                    <div class="box-lining">
                      <div class="forcheif" id="forchieldProduct"></div>



                      
                      <div  class="manu-ally" id="manually-show-default" style="display:none;">
                        <div class="col-sm-12 padd-i">
                          <div class="form-group input-texts" style="position:relative;">
                             <label for="exampleInputEmail1">Enter Manually Quantity</label>                                
                              <input type="text"  class="form-control" id="manualquantity" name="quantity" value="<?php if(isset($data['quantity'])){ echo $data['quantity']; } ?>">
                              
                              <label style="top:30px; position:absolute; right:10px;" for="exampleInputEmail1" id="unitname"></label>
                          </div>
                        </div>                   
                      </div>

                      <input type="hidden" name="unit_name" id="setUnitName">


                      <div class="row">
                      <div class="col-md-12">

                      <table id="table1" style="display: none;" class="tbl-jkl" width="100%">
                      <tbody>
                      <tr>
                        <th>Subscription Amount(Rs.)</th>
                        <th>Customer Balance(Rs.)</th>
                        <th>Calculated Containers</th>
                      </tr>
                      <tr>
                      <td>
                        
                          <div class="form-group">
                            <div id="price_subscriptionamount" style="display:none"> 
                             <!-- <label for="exampleInputEmail1" class="subscription_label">Subscription Amount</label> -->
                                <input readonly="readonly" name="subscription_price" type="text" id="subscriptionamount" class="subscription_amount"> 
                        
                          </div>
                        </div>
                        </td>
                        <td>
                    
                          <div class="form-group">
                            <div id="price_userbalance" style="display:none">  
                              <!--<label for="exampleInputEmail1" class="subscription_label">Customer Balance</label> -->        
                              <input type="text" readonly="readonly" id="userbalance" class="subscription_amount">  
                             </div>
                         
                        </div>
                        </td>
                        <td>
                                   
              <!--  <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="opencheckboxes" title="Leave Unchecked if you dont want to add container"> Calculate Containers
              </label> -->

                      <div id="appendforcalculatingcontainers"></div>              
              
                    </td>
                    </tr>
                </div>
                </tbody>
                </table>
                    </div>
               </div>


<input type="hidden" id="user_customer_id" name="user_customer_id" value="<?php echo $user_customer_id ?>">

           <div class="row">

            </div>

                               
                               <div class="form-group" id="get_timingdelivery">
                                

                          <!--  <?php
                              if(isset($DeliverySchdules) && count($DeliverySchdules) > 0)
                              {
                                foreach ($DeliverySchdules as $key => $value) {
                                  
                              ?>
                                 
                               <div class="checkbox">
                                <label><input type="checkbox"  id="delivery_schdule_<?php echo $key;  ?>" name="delivery_schdule_ids[]" value="<?php echo $key; ?>"><?php echo strtoupper($value); ?></label>
                               </div>
                              
                              <?php
                              } 
                            }else{
                                ?>
                                <p class="red">Please add Delivery Schdules First</p>
                                <?php
                                } ?> -->


                         </div>



                   
                               


                        <div class="form-group">
 
                                 <label for="exampleInputEmail1">Select Subscription type</label>
                                
                                 <select class="form-control" id="subscriptiontypes"  name="subscription_type_id">
                                   <option value="">Select Subscription Types</option>
                                    <?php if(isset($subscriptionTypes)&&count($subscriptionTypes)>0){
                                               
                                               foreach ($subscriptionTypes as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $value['id'] ?>"><?php echo $value['subscription_type_name']; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add subscription Types First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div> 

                         <div class="form-group">
                            
                            <div class="weekDays-selector" id="weekdays" style="display:none">
                                    
                                  </div>
                          </div>


                        <div class="form-group">

                        <label for="exampleInputEmail1">Notes</label>
                              <textarea class="form-control" placeholer="Write Notes Here" name="notes"></textarea>
                        </div>

                        <div class="form-group">
                           <label for="exampleInputEmail1">Summary</label>   
                              <textarea id="summarySubscription" class="form-control" placeholer="Write Summary Here" name="summary"></textarea>
                        
                        </div>



                          <div class="form-group">
                             <label for="exampleInputEmail1">Start Date</label>
                           <input type="text" name="start_date" class="form-control datepicker" readonly="readonly"> 
                          </div> 


                  </div>
                  <input type="button" id="submit" class="btn btn-primary" value="Submit"> 
                </div> 
                 
                </form> 
                
                <div id='loadingmessage' style="display:none;">
                <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
               </div>

                





                </div>
            </div>
        </div>


       



   
<script>
             jQuery(document).ready(function(){
              jQuery("#btn1").click(function(){
                 jQuery("#hide-box1").hide(600);
                jQuery("#hide-box2").show();
              });
              jQuery("#btn2").click(function(){
                 jQuery("#hide-box2").hide(600);
                jQuery("#hide-box1").show();
              });

              jQuery(".update_icon").on('click',  function() {




                  var sub_qty =  $(this).closest("tr").find("input[name='qty']").val();
                  var user_id =  $("input[name=user_customer_id]").val();
                  var s_id =  $(this).closest("tr").find("input[name='s_id']").val();
                  var sub_id =  $(this).closest("tr").find("select[name='sub_id']").val();
                  var pr_price =  $(this).closest("tr").find("input[name='pr_price']").val();
                  var start_date = $(this).closest("tr").find("input[name='start_date1']").val();
                  var container_count =  $(this).closest("tr").find("input[name='qty']").attr('container');

                  
                  var schedule_id =  $(this).closest("tr").find("select[name='delivery_schdule_ids']").val();
                  
                    $.ajax({
                    type: 'POST',
                    url: "<?php echo HTTP_ROOT ?>UserSubscriptions/changeStatus",
                    cache : "false",
                    
                    data: {'sub_id': sub_id, 'sub_qty'  : sub_qty, 'user_id': user_id, 's_id': s_id, 'pr_price': pr_price, 'start_date': start_date, 'container_count': container_count,"schedule_id": schedule_id },
                    success: function(resultData) {         
                      if(resultData=="updated"){
                        alert("User Subscriptions updated");
                         $('#Subscriptions').load("<?php echo HTTP_ROOT ?>UserSubscriptions/customerSubscription", {'id': '<?php echo $id?>'}, function(data) {   

                        });
                         $('#Container').load("<?php echo HTTP_ROOT ?>Users/usersContainers", {'id': '<?php echo $id ?>'}, function(data) {   

              });
                      }
                    
                     }
                    });
    
                }); 

                  $('#manualquantity').bind('input', function() {  
                    $("#table1").show(); 
                    });
                  $('.qty').bind('input', function() {                    
                      
                      var qty_id = $(this).attr('id');
                      //alert("#"+qty_id+"");
                      //alert($(this).val()); // get the current value of the input field.
                      var pr_id =  $(this).closest("tr").find("input[name='pr_id']").val();
                      var qty1 =  $(this).val();
                      $.ajax({
                        type: 'POST',
                        url: "calculateContainers",
                        cache : "false",                    
                        data: {'pro_id': pr_id, 'qty': qty1 },
                        success: function(resultData) { 

                           myarray = JSON.parse(resultData); 
                        
                        $("#"+qty_id+"").attr('container',myarray.containers);
                        return;        
                      
                    }

                  });
              });



/*jQuery(".edit_sub").click(function(){

  $(this).closest("tr").find(".update_icon").toggle();
  $(this).closest("tr").find(".edit_sub").toggle();


  $(this).closest("tr").find("input[name='qty']").removeAttr("disabled");
    $(this).closest("tr").find("select[name='sub_id']").removeAttr("disabled");
    $(this).closest("tr").find("select[name='delivery_schdule_ids']").removeAttr("disabled");
    $(this).closest("tr").find("input[name='start_date1']").removeAttr("disabled");
    var sh_id = $(this).closest("tr").find("select[name='delivery_schdule_ids']").attr("id");
     


            $("#"+sh_id+"").html('');
           $("#subscriptionamount").val('');
           $("#manualquantity").val('');
           var getcategory_id = $(this).closest("tr").find("input[name='category_id']").val();
           var globalUserId = $("#user_customer_id").val();
           

           var htmloption = '';  
           $.ajax({
            url: "getProduct",
            cache: false,
            data:{'cat_id':getcategory_id,'customer_id':globalUserId},
            success: function(result){
                             
               var result = JSON.parse(result);
               
              $.each( result.deliverytiming, function( key, value ) {
               
              $("#"+sh_id+"").append("<option value="+value.d_s_id+">"+value.name+"</option>");
             });
            }

          });

});*/


             });
          </script>






            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



 

          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      


<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>













