  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">  
<?php 
  
   /*echo $this->Html->css('sol');*/
      echo $this->Html->script('addsubscription');
  
 ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}

.weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
  
.subscription_label{width:100%;}
.subscription_amount{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:80px; background:#fff; border:1px solid #ddd; box-shadow::none;}


.container_calculations{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:510px;background:#fff; border:1px solid #ddd; box-shadow::none;}


.box-lining{display:inline-block; width:100%; }
#forchieldProduct input[type="text"]{width:70px;}
#forchieldProduct input[type="radio"]{position:relative; top:7px; margin-right:5px;}
#forchieldProduct label{margin-left:10px;}

</style>
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>


<script>
  $( function() {
    $( "#datepicker" ).datepicker({
  dateFormat: "yy-mm-dd",
  minDate: 0
});
  } );
  </script>
  
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
             
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
              </ol>
            </section>

 
    <!-- Main content -->
    <section class="content">
          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Add New Users Subscription</h3>
              </div>
            
            <div class="box-body">
<div id="err_qty" style="display:none;color:red;"></div>
             <div class="row">
                  <div class='col-sm-9'>
                   
                   <form id="addProductForm" method="post" action="add">

                   
                   <div class="form-group" id="err_div"></div>
                   <div class="form-group" id="err_div_green" style="color:green;"></div>
                      
                           <div class="form-group">



                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

 
                                 <label for="exampleInputEmail1">Select User</label>
                                
                                 <select class="form-control" id="userlist"  name="user_id">
                                   <option value="">Select User</option>
                                    <?php if(isset($users)&&count($users)>0){
                                               
                                               foreach ($users as $key => $value) {
                                                   ?>
                                              <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add User First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>




                              <div class="form-group">
 
                                 <label for="exampleInputEmail1">Select Category</label>
                                
                                 <select class="form-control" id="getcategory"  name="category_id">
                                   <option value="">Select Category</option>
                                    <?php if(isset($categories)&&count($categories)>0){
                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                                                   <option rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Category First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>


                               <div class="form-group"> 
 
                                 <label for="exampleInputEmail1">Product</label>
                                
                                   <select class="form-control" id="getcategory_product"  name="product_id">
                                   </select>
                                
                              </div>




                                
                    <div class="box-lining">
                      <div class="row" id="forchieldProduct"></div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group" style="position:relative;">
                             <label for="exampleInputEmail1">Enter Manually Quantity</label>                                
                              <input type="text"  class="form-control" id="manualquantity" name="quantity" value="<?php if(isset($data['quantity'])){ echo $data['quantity']; } ?>">
                              
                              <label style="top:30px; position:absolute; right:10px;" for="exampleInputEmail1" id="unitname"></label>
                          </div>
                        </div>                   
                      </div>
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <div id="price_subscriptionamount" style="display:none"> 
                              <label for="exampleInputEmail1" class="subscription_label">Subscription Amount</label>
                                <input readonly="readonly" name="subscription_price" type="text" id="subscriptionamount" class="subscription_amount"> 
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <div id="price_userbalance" style="display:none">  
                              <label for="exampleInputEmail1" class="subscription_label">Customer Balance</label>         
                              <input type="text" readonly="readonly" id="userbalance" class="subscription_amount">  
                             </div>
                          </div>
                        </div>
                      </div>
                    </div>








           <div class="row">
             

             <div class="col-sm-3">
               
              <!--  <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="opencheckboxes" title="Leave Unchecked if you dont want to add container"> Calculate Containers
              </label> -->

              <div id="appendforcalculatingcontainers"></div>



              
              </div>
            



            </div>














                               
                               <div class="form-group" id="get_timingdeliveryy">
                                

                          <!--  <?php
                              if(isset($DeliverySchdules) && count($DeliverySchdules) > 0)
                              {
                                foreach ($DeliverySchdules as $key => $value) {
                                  
                              ?>
                                 
                               <div class="checkbox">
                                <label><input type="checkbox"  id="delivery_schdule_<?php echo $key;  ?>" name="delivery_schdule_ids[]" value="<?php echo $key; ?>"><?php echo strtoupper($value); ?></label>
                               </div>
                              
                              <?php
                              } 
                            }else{
                                ?>
                                <p class="red">Please add Delivery Schdules First</p>
                                <?php
                                } ?> -->


                         </div>


                        <div class="form-group">
 
                                 <label for="exampleInputEmail1">Select Subscription type</label>
                                
                                 <select class="form-control" id="subscriptiontypes"  name="subscription_type_id">
                                   <option value="">Select Subscription Types</option>
                                    <?php if(isset($subscriptionTypes)&&count($subscriptionTypes)>0){
                                               
                                               foreach ($subscriptionTypes as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $value['id'] ?>"><?php echo $value['subscription_type_name']; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add subscription Types First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div> 

                         <div class="form-group">
                            
                            <div class="weekDays-selector" id="weekdays" style="display:none">
                                    
                                  </div>
                          </div>


                        <div class="form-group">

                        <label for="exampleInputEmail1">Notes</label>
                              <textarea class="form-control" placeholer="Write Notes Here" name="notes"></textarea>
                        </div>

                        <div class="form-group">
                           <label for="exampleInputEmail1">Summary</label>   
                              <textarea class="form-control" placeholer="Write Summary Here" name="summary"></textarea>
                        
                        </div>









                          <div class="form-group">
                             <label for="exampleInputEmail1">Start Date</label>
                           <input type="text" name="start_date" class="form-control" id="datepicker">
                          </div> 


                  </div>
                </div> 
                 <input type="button" id="submit" class="btn btn-primary" value="Submit"> 
                </form> 
                
                <div id='loadingmessage' style="display:none;">
                <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
               </div>


            </div> 
          </div> 
        </div> 
      </div> 
    </section>

</div>

<script type="text/javascript">
  
        /*  $( "#subscriptiontypes" ).change(function() { 
             
            var what = $('#subscriptiontypes').find(":selected").text();
            
             if(what=="weekly"){

                    $("#weekdays").show();
                    $("#weekdays").append('<input type="checkbox" value="1" name="days[]" id="weekday-mon" class="weekday" /><label for="weekday-mon">M</label><input type="checkbox" value="2" name="days[]" id="weekday-tue" class="weekday" /><label for="weekday-tue">T</label><input type="checkbox" value="2" name="days[]" id="weekday-wed" class="weekday" /><label for="weekday-wed">W</label><input type="checkbox" value="4" name="days[]" id="weekday-thu" class="weekday" /><label for="weekday-thu">T</label><input type="checkbox" value="5" name="days[]" id="weekday-fri" class="weekday" /><label for="weekday-fri">F</label><input type="checkbox" value="6" name="days[]" id="weekday-sat" class="weekday" /><label for="weekday-sat">S</label><input type="checkbox" value="7" name="days[]" id="weekday-sun" class="weekday" /><label for="weekday-sun">S</label>');

             }else{
              $("#weekdays").empty();
             }

          });*/

 
</script>
 


