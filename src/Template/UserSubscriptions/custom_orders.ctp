
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
  <style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  #appendforcalculatingcontainers > label {
    display: none;
}
</style>
  <script type="text/javascript">
 $(document).ready(function(){
        $("#filterUser").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>
  <script>
(function($, undefined){
  /*console.log("$", $('#datepicker'));*/
  $(".datepicker" ).datepicker({ 
    dateFormat: "yy-mm-dd",
    minDate: 1
  });
})($);
</script>
  <?php 
  
   /*echo $this->Html->css('sol');*/
echo $this->Html->script('addcustomorder');
  
 ?>
  <style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}

.weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
  
.subscription_label{width:100%;}
.subscription_amount{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:80px; background:#fff; border:1px solid #ddd; box-shadow::none;}


.container_calculations{font-size:20px; font-weight:bold; height:60px; text-align:center; vertical-align:top; width:510px;background:#fff; border:1px solid #ddd; box-shadow::none;}


.box-lining{display:inline-block; width:100%; }
#forchieldProduct1 input[type="text"]{width:70px;}
#forchieldProduct1 input[type="radio"]{position:relative; top:7px; margin-right:5px;}
#forchieldProduct1 label{margin-left:10px;}

</style>
  <style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
  <script>
(function($, undefined){
  /*console.log("$", $('#datepicker'));*/
  $("#datepicker1" ).datepicker({ 
    dateFormat: "yy-mm-dd",
    minDate: 1
  });
})($);
</script>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="left_box" id="hide-box11">
      <h4 class="heading_title">
        <a href="#">Custom Order</a>
      </h4>
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>Product</th>
              <th>Quantity</th>
              <th>Amount</th>              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
                  $ik = 0; 
                   if(isset($customorder) && count($customorder) > 0){ 
                    
                       foreach ($customorder as $key => $value) {
                     ?>
            <tr>
              <input type="hidden" name="s_id" value="<?php echo $value['id'] ?>">
                <input type="hidden" name="pr_price" value="<?php echo $value['product']['price_per_unit'] ?>">
                  <input type="hidden" name="pr_id" value="<?php echo $value['product']['id'] ?>">
                    <input type="hidden" name="category_id" class="category_id" value="<?php echo $value['product']['category_id'] ?>">
                      <td>
                        <?php echo $value['product']['name']; ?>
                      </td>
                      <td>
                        <?php echo $value['quantity']; ?>
                      </td>
                      <td>
                        <?php echo 'Rs '.$value['price'].'.00'; ?>
                      </td>
                      <td>
                        <a title="Delete order" href="<?php echo HTTP_ROOT ?>UserSubscriptions/deletecustomorder?id=<?php echo base64_encode($value['id']) ?>&type=delete&redirected_ID=<?php echo base64_encode($value['user_id']) ?>" onclick="return confirm('Are you sure you want to delete this order?')" class="delete_icon" href="javascript:void(0)">                             
                             <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>

                        <a title="Edit order" class="edit_sub" href="<?php echo HTTP_ROOT ?>UserSubscriptions/editOrder/<?php echo base64_encode($value['id']);  ?>"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                    <?php
                $ik++;
                }  
              } else{ 
                ?>
                    <tr>
                      <td colspan="3" style="color:red;">No Subscription were found</td>
                    </tr>
                    <?php

                } ?>
                  </tbody>
                </table>
              </div>
              <div class="right_box">
                <a href="#" class="btn_add" id="btn11">Add Custom order</a>
              </div>
            </div>
            <div class="left_box1" id="hide-box21">
              <h4 class="heading_title">
                <a href="#">Add Custom order</a>
                <div class="right_box">
                  <a href="#" class="btn_add1" id="btn12">Custom Orders</a>
                </div>
              </h4>
              <div id="err_qty" style="display:none;color:red;"></div>
              <div class="row">
                <div class='col-sm-8'>
                  <form id="addProductForm" name="addcustomorder" method="post" action="addToNextDelivery">
                    <div class="form-group">
                      <?php if(isset($error['name'])){
                                  ?>
                      <p class="red">
                        <?php echo $error['name']; ?>
                      </p>
                      <?php
                                  } ?>
                      <label for="exampleInputEmail1" style="display:none;">Select User</label>
                      <select class="form-control" id="userlist"  name="user_id" style="display:none;">
                        <?php if(isset($users)&&count($users)>0){
                                               
                                               foreach ($users as $key => $value) {
                                                   ?>
                        <option value="<?php echo $key ?>">
                          <?php echo $value; ?>
                        </option>
                        <?php
                                               }

                                    }else{
                                        ?>
                        <option>Please add User First</option>
                        <?php
                                        } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Select Category</label>
                      <select class="form-control" id="getcategory1"  name="category_id">
                        <option value="">Select Category</option>
                        <?php if(isset($categories)&&count($categories)>0){
                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                        <option rel="
                          <?php echo $value; ?>" value="<?php echo $key ?>">
                          <?php echo $value; ?>
                        </option>
                        <?php
                                               }

                                    }else{
                                        ?>
                        <option>Please add Category First</option>
                        <?php
                                        } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Product</label>
                      <select class="form-control" id="getcategory_product1"  name="product_id"></select>
                    </div>
                    <div class="box-lining">
                      <div class="forcheif" id="forchieldProduct1"></div>
                      <div  class="manu-ally" id="manually-show-default1" style="display:none;">
                        <div class="col-sm-12 padd-i">
                          <div class="form-group input-texts" style="position:relative;">
                            <label for="exampleInputEmail1">Enter Manually Quantity</label>
                            <input type="text"  class="form-control" id="manualquantity1" name="quantity11" value="<?php if(isset($data['quantity'])){ echo $data['quantity']; } ?>">
                              <label style="top:30px; position:absolute; right:10px;" for="exampleInputEmail1" id="unitname1"></label>
                            </div>
                          </div>
                        </div>
                        <input type="hidden" name="unit_name" id="setUnitName">
                          <div class="row">
                            <div class="col-md-12">
                              <table id="table1" style="display: none;" class="tbl-jkl" width="100%">
                                <tbody>
                                  <tr>
                                    <th>Subscription Amount(Rs.)</th>
                                    <th>Customer Balance(Rs.)</th>
                                    <th>Calculated Containers</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="form-group">
                                        <div id="price_subscriptionamount12" style="display:none">
                                          <!-- <label for="exampleInputEmail1" class="subscription_label">Subscription Amount</label> -->
                                          <input readonly="readonly" name="subscription_price" type="text" id="subscriptionamount13" class="subscription_amount">
                                          </div>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="form-group">
                                          <div id="price_userbalance1" style="display:none">
                                            <!--<label for="exampleInputEmail1" class="subscription_label">Customer Balance</label> -->
                                            <input type="text" readonly="readonly" id="userbalance1" class="subscription_amount">
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <!--  <label class="form-check-label"><input class="form-check-input" type="checkbox" id="opencheckboxes" title="Leave Unchecked if you dont want to add container"> Calculate Containers
              </label> -->
                                          <div id="appendforcalculatingcontainers"></div>
                                        </td>
                                      </tr>
                                    </div>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <input type="hidden" id="user_customer_id" name="user_customer_id" value="<?php echo $user_customer_id ?>">
                            <input type="hidden" id="is_chield" name="is_chield" value="0">
                              <div class="row"></div>
                              <div class="form-group" id="get_timingdelivery1">
                                <!--  
                                <?php
                              if(isset($DeliverySchdules) && count($DeliverySchdules) > 0)
                              {
                                foreach ($DeliverySchdules as $key => $value) {
                                  
                              ?><div class="checkbox"><label><input type="checkbox"  id="delivery_schdule_
                                <?php echo $key;  ?>" name="delivery_schdule_ids[]" value="
                                <?php echo $key; ?>">
                                <?php echo strtoupper($value); ?></label></div>
                                <?php
                              } 
                            }else{
                                ?><p class="red">Please add Delivery Schdules First</p><?php
                                } ?> -->
                              </div>
                              <!-- <div class="form-group">
                                <label for="exampleInputEmail1">Select Subscription type</label>
                                <select class="form-control" id="subscriptiontypes"  name="subscription_type_id">
                                  <option value="">Select Subscription Types</option>
                                  <?php if(isset($subscriptionTypes)&&count($subscriptionTypes)>0){
                                               
                                               foreach ($subscriptionTypes as $key => $value) {
                                                   ?>
                                  <option value="
                                    <?php echo $value['id'] ?>">
                                    <?php echo $value['subscription_type_name']; ?>
                                  </option>
                                  <?php
                                               }

                                    }else{
                                        ?>
                                  <option>Please add subscription Types First</option>
                                  <?php
                                        } ?>
                                </select>
                              </div> -
                              <div class="form-group">
                                <div class="weekDays-selector" id="weekdays" style="display:none"></div>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Notes</label>
                                <textarea class="form-control" placeholer="Write Notes Here" name="notes"></textarea>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Summary</label>
                                <textarea id="summarySubscription" class="form-control" placeholer="Write Summary Here" name="summary"></textarea>
                              </div> -->
                              <div class="form-group">
                                <label for="exampleInputEmail1">Quantity</label>
                                <input type="text" name="quantity" id="quantityx" class="form-control">
                                </div>
                                <table>
                                  <div class="form-group orderprice" style="display: none;">
                                  <tr><th>Order Price(Rs)</th></tr>
                                  <tr><td> 
                                  <input readonly="readonly" type="text" name="orderprice" id="orderprice1" class="subscription_amount" value="0">
                                  </td></tr>
                                  </div>
                                </table>

                              </div>
                              <div class="form-group">
                                <input type="button" id="submit1" class="btn btn-primary" value="Submit">
                                </div>
                              </div>
                            </form>

                            <div id='loadingmessage' style="display:none;">
                              <img id="loading" src='
                                <?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
                              </div>
                            </div>
                          </div>
                        </div>
                                    </div>
                      <!-- /.box-body -->
                      <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                  </div>
                  <!-- /.col -->
                  <!-- /.col -->
                </div>
                <style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>
<script type="text/javascript">
  $(document).ready(function(){

    $("#btn11").click(function(){
      $("#hide-box11").hide(600);
      $("#hide-box21").show();
      
    });
    $("#btn12").click(function(){
      $("#hide-box21").hide(600);
      $("#hide-box11").show();
    });

    $("#submit1").click(function(){
      var product_id = $('#getcategory_product1 :selected').val();
      var quantity = $('#quantityx').val();
      var quantity1 = $('#qty1').val();
      if( !quantity ){
        quantity = quantity1;
      }
      if( !product_id){
        alert('please select a product');
      }else if( !quantity ){
        alert("please enter quantity");
      }else{
        $("form[name='addcustomorder']").submit();
      }
      
    });
  });
</script>