   
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
 echo $this->Html->script('linkpage.js');
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
 $(document).ready(function(){
        $("#filterUser").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>



<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
                Area Listing
              </h1>

              <div class="customer_bts">
               <!-- <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Area</button></a>  -->
             </div>
             
              
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">
            <div class="left-serach-option">
              
             
             <form class="sidebar-form search_bar" method="get" action="add">
              <div class="input-group">
               <input type="text" placeholder="Search by Area Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 



            </div>
            <div class="right-filter-option">
               <div class="box-filter">  
              <form action="add" id="filteruserby" method="get">
                                <select class="form-control" name="filterby" id="filterUser">
                                    <option value="">Filter By Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($filterby)&&!empty($filterby))
                                                 {
                                                    if($key == $filterby) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                </select>
                                
                    </form>           
                              
                 
              </div>
              <a href="<?php echo HTTP_ROOT ?>Areas/add" class="reset-filter"><button type="button" class="btn btn-sm btn-info">Reset filters</button></a>
             </div>






              

              <div class="box-tools pull-right">
                 
                 <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Subscription Type </a> -->
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                  <th><?php echo $this->Paginator->sort(
    'name', 
    array(
        'asc' => __('Area Name') . ' <i class="glyphicon glyphicon-chevron-up text-info pull-right">',
        'desc' => __('Area Name') . '<i class="glyphicon glyphicon-chevron-down text-info pull-right">'
    ),
    array(
        'escape' => false
   )
);

                   ?></th>
                    
                   <th><?php echo $this->Paginator->sort(
    'Regions.name', 
    array(
        'asc' => __('Region Name') . ' <i class="glyphicon glyphicon-chevron-up text-info pull-right">',
        'desc' => __('Region Name') . '<i class="glyphicon glyphicon-chevron-down text-info pull-right">'
    ),
    array(
        'escape' => false
   )
);

                   ?></th>  
                   <th><?php echo $this->Paginator->sort(
    'name', 
    array(
        'asc' => __('Created') . ' <i class="glyphicon glyphicon-chevron-up text-info pull-right">',
        'desc' => __('Created') . '<i class="glyphicon glyphicon-chevron-down text-info pull-right">'
    ),
    array(
        'escape' => false
   )
);

                   ?></th> 
                   <th>Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($areas) && count($areas) > 0){ 
                       foreach ($areas as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                       <td><?php echo $value['region']['name']; ?></td>
                      <td><?php echo $value['created']->format('d-M-Y'); ?></td>
                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Areas/edit/<?php echo base64_encode($value['id']);  ?>">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                      </a>
                     <!-- <a onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Areas/delete/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>

                      </a>-->

                      </td> 
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Area were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>


                 <?php

            if( count( $areas ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
                                          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php } ?>

              </div>
               
            </div>
             
          </div>



          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Add New Area</h3>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>
                   
                     <form action="add" method="post">

                     <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?> 
                      
                           <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Area Name</label>
                                
                                <input type="text" maxlength="50" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                
                             </div> 



                             <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Region</label>

                                <select class="form-control" required name="region_id">
                                <option value="">Select Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                                 <?php if(isset($error['region_id'])){
                                  ?>
                                  <p class="red"><?php echo $error['region_id']; ?></p>
                                  <?php
                                  } ?> 
                             </div> 

                             <div class="form-group">
                                <label for="exampleInputEmail1">Area Manger Name</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_name" value="<?php if(isset($data['manager_name'])){ echo $data['manager_name']; } ?>">         
                             </div> 

                             <div class="form-group ">
                                <label for="exampleInputEmail1">Area Manager Phone No.</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_phoneNo" value="<?php if(isset($data['manager_phoneNo'])){ echo $data['manager_phoneNo']; } ?>">         
                             </div> 

                             <div class="form-group ">
                                <label for="exampleInputEmail1">Area Manager Email</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_email" value="<?php if(isset($data['manager_email'])){ echo $data['manager_email']; } ?>">         
                             </div> 
                            
                            <input type="hidden" name="status" value="1">



                      </div>
                </div> 
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



<style type="text/css">
  .delete_icon{ color: red; }
  .edit_icon{ margin-right: 10px; }
</style>