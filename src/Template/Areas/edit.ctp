 
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      //$(".success").fadeOut(4000);
  
  });

 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Area
              </h1>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
              <div class="box-header">
                
                <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Areas/add" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>

                    <form action="<?php echo HTTP_ROOT ?>Areas/edit" method="post">
                      
                           <div class="form-group required">
                            
                                <label for="exampleInputEmail1 ">Name</label>
                                
                                <input type="text" maxlength="50" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

                             </div> 





                             <div class="form-group required">
                            
                                <label>Region</label>

                                <select class="form-control" required name="region_id">
                                <option value="">Select Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option <?php if($key==$data['region_id']) {
                                                    ?>
                                                    selected
                                                   <?php }?> value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                                 <?php if(isset($error['region_id'])){
                                  ?>
                                  <p class="red"><?php echo $error['region_id']; ?></p>
                                  <?php
                                  } ?> 
                             </div> 

                             <div class="form-group">
                                <label for="exampleInputEmail1">Area Manger Name</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_name" value="<?php if(isset($data['manager_name'])){ echo $data['manager_name']; } ?>">         
                             </div> 

                             <div class="form-group ">
                                <label for="exampleInputEmail1">Area Manager Phone No.</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_phoneNo" value="<?php if(isset($data['manager_phoneNo'])){ echo $data['manager_phoneNo']; } ?>">         
                             </div> 

                             <div class="form-group ">
                                <label for="exampleInputEmail1">Area Manager Email</label>
                                <input type="text" maxlength="50"  class="form-control" id="" name="manager_email" value="<?php if(isset($data['manager_email'])){ echo $data['manager_email']; } ?>">         
                             </div>

       
                             
                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">

 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 

                      </div>
                </div> 
                
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



