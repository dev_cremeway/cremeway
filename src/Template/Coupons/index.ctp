<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
                Coupon Listing
              </h1>

              <div class="customer_bts">
               <a href="<?php echo HTTP_ROOT ?>Coupons/add" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Coupon</button></a> 
             </div>
             
              
            </section>

    
    <!-- Main content -->
    <section class="content">
       
      <div class="box box-info">
            <div class="box-header">
            <div class="left-serach-option">
             <form class="sidebar-form search_bar" method="get" action="<?php echo HTTP_ROOT ?>Coupons/index">
              <div class="input-group">
                <input type="text" placeholder="Search by coupons name.." class="form-control" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn"  type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
            </div>
            <div class="right-filter-option">
               <div class="box-filter">  
              <form action="add" id="filteruserby" method="get">
                                <select class="form-control" name="filterby" id="filterUser">
                                    <option value="">Filter By Name</option>
                                </select>
                                
                    </form>           
                              
                 
              </div>
              <a href="<?php echo HTTP_ROOT ?>Coupons/view" class="reset-filter"><button type="button" class="btn btn-sm btn-info">Reset filters</button></a>
             </div>






              

              <div class="box-tools pull-right">
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                  <th>Name</th>
                  <th>Download CSV</th>  
                  <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($coupon) && count($coupon) > 0){ 
                       foreach ($coupon as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td>

                      
                            <a href="<?php echo HTTP_ROOT ?>Coupons/dCsv/<?php echo base64_encode($value['id']);  ?>">
                              <img style="height:40px;width:40px;" src="<?php echo HTTP_ROOT ?>img/csvicon.png">

                            </a> 
                      </td> 
                      <td><a class="edit_icon" href="<?php echo HTTP_ROOT ?>Coupons/view/<?php echo base64_encode($value['id']);  ?>">
                     View
                      </a>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Coupons/barcode?id=<?php echo base64_encode($value['id']);  ?>" target="blank">
                     Print Barcode
                      </a>
                      </td>
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Coupons were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>


                 <?php

            if( count( $coupon ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
         
 <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         

        </div>      
                                </div>
                                <?php } ?>

              </div>
               
            </div>
             
          </div>
 
    </section>
</div>



<style type="text/css">
  .delete_icon{ color: red; }
  .edit_icon{ margin-right: 10px; }
</style>