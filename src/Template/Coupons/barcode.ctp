<!DOCTYPE html>
<html lang="en">
<head>
  <title>invoice</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
      body{font-family:'Open Sans',sans-serif;overflow:auto}.section-one{padding:4% 0}.invoice-title p{margin:0;font-size:18px;font-weight:600}.info-block ul{padding:0;list-style:none}.info-block .col-sm-6{padding:0}.info-block ul li{font-weight:600;height:35px;border-bottom:1px solid #eee;padding:7px 0}.details-receiver{text-align:center;font-size:18px;font-weight:600;margin:0;padding:18px 0 0 0}.section-one .table.table-bordered{margin:20px 0;border-bottom:0}.section-one thead{background:#eee}.section-one thead td{border-bottom:0!important}.col-sm-12.foter-amount{height:430px;background:#fcfcfc;padding:17px 0}.col-sm-4.amount-words,.col-sm-2.common-seal{height:100%;text-align:center}.befor-tax ul{padding:0;list-style:none}.befor-tax ul li{font-weight:600;height:35px;border-bottom:1px solid #eee;padding:7px 6px}.bblck .col-sm-6,.bblck .col-sm-12{padding:0}.befor-tax ul{padding:0;list-style:none;border:1px solid #e8e8e8;border-bottom:0}.border-leftn{border-left:none!important}.col-sm-4.amount-words p,.col-sm-2.common-seal p{position:absolute;left:0;right:0;bottom:1px;font-weight:600}.panel .table-bordered .text-center strong{display:block}.col-sm-12.foter-amount.cc{height:270px;padding-left:15px}.col-sm-12.foter-amount.cc .col-sm-6 p{font-weight:600;text-align:center;padding:6px 0}.col-sm-12.foter-amount.cc .col-sm-8{height:100%;border:1px solid #eee}.col-sm-12.foter-amount.cc .col-sm-8 .col-sm-6{height:100%}.col-sm-6.amount-words{border-right:1px solid #eee}.col-sm-12.text-center.bb li{line-height:auto!important;height:auto}.col-sm-12.text-center.bb .height-large.sp{height:145px}#home2 .text-center.invoice-title p{font-size:30px;font-weight:600}.header-defi{border:1px solid #ddd;padding:10px;font-weight:600;font-size:16px;text-align:center;width:50%;display:inline-block;margin:-1px}#home2 .table.table-bordered.text-center{margin-top:0}#home2 .details-receiver{padding-bottom:20px}.noppading{padding:0!important}.rightside{float:right}.printicon{float:right;font-size:20px;margin-top:-25px}.printicon a{color:#555}
    </style>  
</head>
<body>
<section class="section-one">
  <div class="container">

    <div class="col-sm-12">
        <div class="printicon"><a onclick="myFunction()"><i class="fa fa-print" aria-hidden="true"></i></a></div>
       <div class="panel">
          <div class="">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align: center;padding: 20px 0px;"><img src="<?php echo HTTP_ROOT.'webroot/img/login_logo.png'?>" style="width: auto;height: 130px;"></th>
                  </tr>
                </thead>
                <tbody>
                  <!-- foreach ($order->lineItems as $line) or some such thing here -->
                <?php
                foreach ($coupon as $key => $value) {
                ?>    
                    <tr>
                      <td style="text-align: center;padding: 30px 0px;"><img src="<?php echo HTTP_ROOT.'webroot/barcode/image.php?code='.$value['s_c_c']?>" alt="Barcode" style="max-width: 160px;"></td>
                    </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
    </div>    
</section>  
<script type="text/javascript">
  function myFunction() {
            window.print();
        }
</script>