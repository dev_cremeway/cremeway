  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">  
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?> 
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
  #err_div{
    color:red;
  }

  .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      //$(".success").fadeOut(4000);
  
  });

 

</script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
  dateFormat: "yy-mm-dd",
  minDate: 0
});
  } );
  </script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Add Coupons
              </h1>
             
               
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
          
              <div class="box-header">

                <h3 class="box-title"></h3>

              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>
                 
                   <form action="" method="post" id="adminupload">
                      <div class="form-group" id="err_div"></div>
                         <?php if(isset($error)) {
                            ?>
                           <div class="form-group" id="err_div"><?php echo $error['name']; ?></div> 
                            <?php
                            } ?>


                           <div class="form-group required">

                          
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text" maxlength="100" required class="form-control" id="coupon_name" name="name">
                                
                                

                             </div> 





                             <div class="form-group required">

                             
                                
                                    <label for="exampleInputEmail1">Price/Coupon</label>
                                    
                                    <input type="text" maxlength="100" required class="form-control" id="price_value" name="price_value">
                                
                                

                             </div>


                             <div class="form-group required">
                                
                                    <label for="exampleInputEmail1">No Of Coupons</label>
                                    <input type="text" maxlength="100" required class="form-control" id="n_o_c" name="n_o_c">
                             </div> 
                             <div class="form-group required">
                             <label for="exampleInputEmail1">Valid Upto</label>
                           <input type="text" name="end_date" class="form-control" id="datepicker">
                          </div> 





                              

                      </div>
                </div> 
                 <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



<script type="text/javascript">
   $(document).ready(function(){

isValidAmount = 0;

isValidCouponeNo = 0;

 $('#price_value').on("input", function() {
       var dInput = this.value;
        if( ( isFloat(dInput) || isInteger(dInput)) && dInput != '') {
          isValidAmount = 1;
        }else{
          isValidAmount = 0;
          this.value = '';
          $("#err_div").show();
          $("#err_div").text("Please enter the valid amount");
          $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
          return false;
        }
   });

      function isFloat(n) {
        return !!(n % 1);
    }

    function isInteger(value) {
         if ((undefined === value) || (null === value)) {
            return false;
        }
        return value % 1 == 0;
    }



    $('#n_o_c').on("input", function() {
       var dInput = this.value;
        if( isInteger(dInput) && dInput != '') {
          isValidCouponeNo = 1;
        }else{
          isValidCouponeNo = 0;
          this.value = '';
          $("#err_div").show();
          $("#err_div").text("Please enter the valid No Of Coupons");
          $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
          return false;
        }
   });













    

        $("#btn_submit").click(function(){
          var vehicle_no = jQuery.trim( $("#coupon_name").val() );
          var price_value = jQuery.trim( $("#price_value").val() );
          var n_o_c = jQuery.trim( $("#n_o_c").val() );
          
         if(vehicle_no == ''){
          $("#err_div").html("Please enter the Coupon No");
         }else if(price_value == ''){
          $("#err_div").html("Please enter the price");
         }else if(price_value == ''){
          $("#err_div").html("Please enter the No Of coupons");
         }else{
            
            
             if(isValidCouponeNo && isValidAmount)
             {
              $("#adminupload").submit();
             }
         

         } 
 });
 });
</script>x