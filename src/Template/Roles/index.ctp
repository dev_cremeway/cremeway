<?php 

/**
 * CakePHP 3.x - Acl Manager
 * 
 * PHP version 5
 * 
 * index.ctp
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 * 
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 */

echo $this->Html->css('AclManager.default',['inline' => false]); 
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>

<script type="text/javascript">
 $(document).ready(function(){
        $("#getarea").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
    <section class="content-header header_dashbord">
      <h1>
         Role Listing 
      </h1>

      <div class="customer_bts">
         <a href="<?php echo HTTP_ROOT ?>Roles/add"><button type="button" class="btn btn-sm btn-info ">Add New Role</button></a>
         <a href="<?php echo HTTP_ROOT ?>Groups/index"><button type="button" class="btn btn-sm btn-info ">Groups</button></a>
         <a href="<?php echo HTTP_ROOT ?>AclManager"><button type="button" class="btn btn-sm btn-info ">Admins</button></a>
         
      </div>
   </section>
<div class="content">
<div class="box box-info">
  <div class="box-header with-border">
      <h3 class="box-title">Add New Role</h3>
      <div class="box-tools pull-right">
         <a href="<?php echo HTTP_ROOT?>AclManager" class="btn btn-sm btn-info btn-flat pull-left">Back to List </a>
      </div>
  </div>
<div class="box-body">
            <div class="table-responsive">
               <table class="table no-margin">
                  <thead>
                     <tr>
                        <th> <?php echo $this->Paginator->sort('Groups.name', 'Group Name'); ?></th>
                        <th> <?php echo $this->Paginator->sort('Roles.name', 'Role Name'); ?></th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                       
                            foreach ($roles as $key => $value) {
                          ?>
                     <tr>
                        <td><?php echo $value['group']['name']; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td>
                        <a href="<?php echo HTTP_ROOT ?>Roles/edit/<?php echo $value['id'];  ?>" class="edit_icon">
                            <i aria-hidden="true" class="fa fa-pencil-square-o"></i>
                        </a>
                        <?php if($value['id']!= 1){ ?> 
                           <a onclick="return confirm('Are you sure?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Roles/delete/<?php echo $value['id'];  ?>">
                           <i class="fa fa-trash-o" aria-hidden="true"></i>
                           </a>
                        <?php } ?>
                        </td>
                     </tr>
                     <?php
                        } 
                       
                        ?>
                   
                  </tbody>
               </table>
               <?php
                  if( count( $roles ) > 0 )
                  {                                
                      ?>
               <div class="text-right">
                  <div class="paginator">
                     <nav>
                        <ul class="pagination">
                           <?= $this->Paginator->prev('< ' . __('previous')) ?>
                           <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                           <?= $this->Paginator->next(__('next') . ' >') ?> 
                        </ul>
                     </nav>
                     <?php echo $this->Paginator->counter(
                        'showing {{current}} records out of
                         {{count}} total'); ?>
                  </div>
               </div>
               <?php }  ?>
            </div>
            <!-- /.table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
</div>
</div>
<style type="text/css">
   .sidebar-form.search_bar {
   display: inline-block;
   margin: 0;
   vertical-align: bottom;
   width: 30%;
   }
   .customer_bts .btn.btn-sm.btn-info {
   border: medium none;
   font-size: 13px;
   padding: 8px 12px;
   text-transform: uppercase;
   }
   .header_dashbord h1 {
   display: inline-block;
   }
   .customer_bts {
   display: inline-block;
   float: right;
   }
   .box-tools.filter {
   display: inline-block;
   margin-left: 10px;
   position: relative;
   width: 10%; vertical-align: top;
   } 
   .filter #filterUser {
   height: 39px;
   position: relative;
   top: -2px;
   }
</style>
