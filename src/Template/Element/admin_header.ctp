<!-- Logo -->
    <a href="<?php echo HTTP_ROOT ?>users" class="logo">
      
      <span class="logo-lg"><img src="<?php echo HTTP_ROOT ?>/img/cremeway_logo.png"></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
           
        
          <?php 
             if(!empty($currentUser) && $currentUser['image']){
                $image = HTTP_ROOT.$currentUser['image'];
             } else {
                $image = HTTP_ROOT."img/user2-160x160.jpg";
             }
          ?>
        
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image;?>" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image ?>" class="img-circle" alt="User Image">

                <p>
                   <?php echo $currentUser['username']?>
                </p>
              </li>
             
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo HTTP_ROOT ?>Users/editadmin/<?php echo base64_encode($currentUser['id'])?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo HTTP_ROOT ?>Users/logout" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>

    </nav>