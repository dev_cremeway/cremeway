 <?php echo $this->Html->script('addbalancerefund'); ?>
  <!-- Customer Profile Page --> 


  <div class="white_backgroound">
    <div class="left_address">
                  
                 <span><?php if(isset($data['name'])) {
                    echo $data['name']."  (".$data['id'].")";
                    } ?> <a href="<?php echo HTTP_ROOT ?>Users/customerProfile?id=<?php echo base64_encode($data['id']) ?>"> <i class="fa fa-pencil-square-o om" aria-hidden="true"></i></a></span>
                  <span><?php if(isset($data['phoneNo'])) {
                    echo $data['phoneNo'];
                    } ?></span>

                      <span><?php if(isset($data['email_id'])) {
                    echo $data['email_id'];
                    } ?></span>

                   <span><?php if(isset($data['region'])) {
                    echo ' # '.$data['houseNo'].' '.$data['area']['name'].','.$data['region']['name'];
                    } ?> </span>                



 <?php if(isset($usersBalance)){
                ?>
    </div>
                <div class="right_div">
                <p class=""><span class="small_text">Rs.</span> <?php echo number_format((float)$usersBalance['balance'], 2, '.', ''); ?>
                <?php
                } else {
                  ?>
                  </div> 
                   <div class="right_div">
                   <p class=""><span class="small_text">Rs.</span> 0.00

                  <?php
                  }?>
                  
                   <button id="myBtn" class="plus_money" data-toggle="modal" data-target="#myModal">+</button>
                </p>
                </div>
                
                </div>

<input type="hidden" id="user_customer_id" value="<?php echo $user_customer_id ?>">

 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<img src="<?php echo HTTP_ROOT?>img/loading.gif" id="gif" style="display: block;  z-index: 99999999999999999;position: absolute;margin-left: 33%;width: 27%; visibility: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            
            <div id="err_qty" style="display:none;color:red;"></div>
            <div id="success_qty" style="display:none;color:green;"></div>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  

            </div>
            <div class="modal-body">
            <p>
             <input type="radio" name="Recharge" value="refundableamount"> Refund
             <input type="radio" name="Recharge" id="rechargeableamount" value="rechargeableamount" class="re"> Recharge
             <input type="radio" name="Recharge" id="deductableamount" value="deductableamount" class="re"> Deduct
             
              </p>
              <div id="re-charge" class="rechargeableamount none">
            <input type="text" id="addprice" class="form-control" placeholder="Enter Recharge Amount ....">
            </div>
            <div id="re-fund" class="refundableamount none" >  
            <select class="selet_optn" id="getcategory">
                 <option value="">Please select the Refund Reason.</option>
                 <?php if(isset($refunds) && count($refunds) > 0 ) {
                         
                        foreach ($refunds as $key => $value) {
                    ?>
                          <option value="<?php echo   $value['id'] ?>"><?php echo   $value['refund_reasons'] ?></option>
                        <?php
                        }
                      } ?>
                  <option value="99">Other</option>    
            </select>
            <div id="getcategory_res"></div>
            <input type="text" id="addpriceRefund" class="form-control" placeholder="Enter Refund Amount ....">
            </div>

            <div id="deduct" class="deductableamount none" >
            <select class="selet_optn" id="getcategory1">
                 <option value="">Please select the Deduct Reason.</option>
                 <?php if(isset($refunds1) && count($refunds1) > 0 ) {
                         
                        foreach ($refunds1 as $key1 => $value1) {
                          ?>
                          <option value="<?php echo   $value1['id'] ?>"><?php echo   $value1['refund_reasons'] ?></option>
                          <?php
                        }
                      } ?>
              <option value="99">Other</option>
            </select>
            <div id="getcategory_res1"></div>
             <input type="text" id="addpricededuct" class="form-control" placeholder="Enter Deduct Amount ....">
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnRCHRGE" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>