<div id="err_qty" style="display:none;color:red;"></div> 
             <div class="row">
                  <div class='col-sm-6'> 
                   <form action="updateCustomerProfile" method="post" id="adminupload" enctype="multipart/form-data"> 
                   <div class="form-group " id="err_div"></div>
                      <div class="form-group required"> 
                         <?php if(isset($error['name'])){
                          ?>
                          <p class="red"><?php echo $error['name']; ?></p>
                          <?php
                          } ?> 
                        <label for="exampleInputEmail1">Name</label> 
                         <input type="text" required class="form-control" id="name" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>"> 
                      </div> 
                        <!--div class="form-group">  

                        <label for="exampleInputEmail1">Username</label> 
                         <input type="text" class="form-control" id="username" name="username" value="<?php if(isset($data['username'])){ echo $data['username']; } ?>"> 
                      </div-->

                       <div class="form-group"> 
                        <label for="exampleInputEmail1">Email</label>
                         
                         <input type="text" required class="form-control" id="email_id" name="email_id" value="<?php if(isset($data['email_id'])){ echo $data['email_id']; } ?>"> 
                      </div> 
                      <div class="form-group required"> 

                        <label for="exampleInputEmail1">Phone No.</label>
                         
                         <input type="text" maxlength="10" required class="form-control" id="phoneNo" name="phoneNo" value="<?php if(isset($data['phoneNo'])){ echo $data['phoneNo']; } ?>"> 
                    
                      </div> 
                       <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Region</label>

                                <select class="form-control" id="getarea" required name="region_id">
                                <option value="">Select Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"
                                                     
                                                     <?php if($key==$data['region']['id']){
                                                      ?>
                                                      selected="selected"
                                                      <?php
                                                     } ?>


                                                   ><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select> 
                             </div> 

                              <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Area</label>

                                <select class="form-control" id="ajaxarea" required name="area_id">


                                  <option value="">Select Area</option>
                                    <?php if(isset($areas)&&count($areas)>0){
                                               
                                               foreach ($areas as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"
                                                     
                                                     <?php if($key==$data['area']['id']){
                                                      ?>
                                                      selected="selected"
                                                      <?php
                                                     } ?>


                                                   ><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }


                                    }else{
                                        ?>
                                        <option>Please add Area First</option>  
                                        <?php
                                        } ?> 
                                </select> 
                             </div>  
                       
                     <div class="form-group" id="parent_cat"> 
                      <label for="exampleInputEmail1"><?php 

                     if(isset($data['customer_category']) && $data['customer_category'] == 'vip') {
                       
                       echo "Remove  ";
                     }else{
                       echo "Make  "; 
                      }                       
                      ?>this customer VIP</label>&nbsp;&nbsp;&nbsp;
                     

                     <input type="checkbox" name="user_cat_type" value="vip" <?php 

                     if(isset($data['customer_category']) && $data['customer_category'] == 'vip') {
                      ?>
                      checked="checked"
                      <?php
                      } ?>>

                    </label>   
                     </div>
                             <div class="form-group required"> 

                                <label for="exampleInputEmail1">Address</label>
                                 
                                 <input type="text" required class="form-control" id="houseNo" name="houseNo" value="<?php if(isset($data['houseNo'])){ echo $data['houseNo']; } ?>">
                           </div>
 
          <input type="hidden" name="id" value="<?php echo $data['id'] ?>">


            <div class="form-group required">
             <?php
                   
                    if(isset($data['image'])&&!empty($data['image'])){
                      ?>
                      <img style="height:100px;width:100px;" src="<?php echo HTTP_ROOT.$data['image'] ?>">
                      <?php
                    } else{ ?>
                        <img style="height:100px;width:100px;" src="<?php echo HTTP_ROOT."img/user.jpg"; ?>">
                        <?php
                    }
             ?>

            </div>     <div class="form-group required">
                          <input type="file" name="image" class="file">
                           <div class="input-group col-xs-12">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                            <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                            <span class="input-group-btn">
                              <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                            </span>
                           </div>
                         </div>

                          <input type="button" id="btn_submit" class="btn btn-primary" value="Update"> 
                   </form>          
            </div> 
          </div> 
