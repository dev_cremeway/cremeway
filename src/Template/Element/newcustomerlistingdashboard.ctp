 
 <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Image</th>
                    <th>Date</th>
                    <th>Action</th>  
                  </tr>
                  </thead>
                  
                  <tbody>
                  
                   <?php if(isset($newusersList) && count($newusersList) > 0)  { 
                     foreach ($newusersList as $key => $value) {
                     ?>
                     <tr>
                     <td><?php echo $value['name']; ?></td>
                     <td><?php echo $value['phoneNo']; ?></td>
                     <td><?php echo $value['houseNo'].' '.$value['area']['name'].', '.$value['region']['name']; ?></td>
                      <td>
                       
                      <?php
                       if($value['image'] && ! empty($value['image'])){
                        ?>
                         <img style="height:50px;width:50px;" src="<?php echo $value['image'] ?>">
                        <?php
                       }else{
                        ?>
                        <img style="height:50px;width:50px;" src="https://www.creators.com/static/img/default-user.png">
                        <?php
                       } 
                      ?> 
                      
                      
                      </td>
                      <td><?php echo $value['created']->format('Y-m-d'); ?></td> 
                      <td> 
                     
                    <?php
                       if($value['status'] == 0 && $value['type_user'] == 'customer')
                       {
                     ?> 
                    <a  href="<?php echo HTTP_ROOT ?>Users/Activate/<?php echo base64_encode($value['id']);  ?>">
                     Activate
                      </a>
                      <?php }  
                        ?>
                   
                    </td>
                     
                     </tr>

                    <?php 
                    }
                  } ?>
                
                  </tbody>
                </table>

                  <?php

            if( count( $newusersList ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
         


        </div>      
                                </div>
                                <?php } ?>


<script>
    $(document).ready(function(){
        $(".pagination li a").click(function(){ alert();
            $("#updated_div_id").load(this.href);
            return false;
        })
    });
</script>