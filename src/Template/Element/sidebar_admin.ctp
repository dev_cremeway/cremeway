<?php
use Acl\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
?>
<style type="text/css">
   .badge-notify{
   background:red;
   position:relative;
   top: -20px;
   left: -35px;
   }
</style>
<script type="text/javascript">
   (function($, undefined){
     $(document).ready(function(){
       $(".underconstruction").click(function(){
         alert("Under Construction now");
         return false;
       })
     })
   })($);

</script>
<?php

   $useraro['Users'] = $this->request->session()->read('Auth.User');
   $collection = new ComponentRegistry();
   $Acl = new AclComponent($collection, Configure::read('Acl'));
   //$isTouch = isset($permissionsList);


   //if( ! $isTouch ){

     ?>
<?php /* <script>
   window.location = "<?php echo HTTP_ROOT ?>Users/logout";

</script> */ ?>
<?php
   //}

   /* if(isset($permissionsList['admin']) && $permissionsList['admin'] == 1)
    {*/

   ?>
<section class="sidebar">
   <?php /*
   <div class="user-panel">
      <div class="pull-left image">
         <img src="
            <?php echo HTTP_ROOT ?>img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
         <p>Admin</p>
         <a href="javascript::void(0)" class="underconstruction">
         <i class="fa fa-circle text-success"></i> Super Admin
         </a>
      </div>
   </div>
   */ ?>
   <?php
      $action = $this->request->params['action'];
      $controller = $this->request->params['controller'];

      ?>
   <ul class="sidebar-menu">
      <li class=
         <?php if(($controller == 'Users' && $action != 'notifications') &&
            ( $action != 'view') && ( $action != 'contacts') && ( $action != 'settings') && ( $action != 'pages') && ($controller == 'Users' && $action != 'add') &&
            ( $action != 'admin') && ( $action != 'customnotifications') && ( $action != 'driver') && ( $action != 'driverProfile')  && $action !='adminadd' ) { echo 'active'; } ?>>
         <a href="
            <?php echo HTTP_ROOT ?>Users">
         <i class="fa fa-bars" aria-hidden="true"></i>
         <span>Dashboard</span>
         </a>
      </li>
  <?php
    if($Acl->check($useraro, 'controllers/Regions/add') || $Acl->check($useraro, 'controllers/Areas/add') || $Acl->check($useraro, 'controllers/DeliverySchdules/add') || $Acl->check($useraro, 'controllers/Categories/add') || $Acl->check($useraro, 'controllers/Units/add') || $Acl->check($useraro, 'controllers/Users/driver') || $Acl->check($useraro, 'controllers/Routes/lists') || $Acl->check($useraro, 'controllers/Vehicles/lists') )
      { ?>
      <li class="treeview
         <?php
            if(
                 ( $controller == 'Categories')
                 ||
                 ( $controller == 'Areas' )
                 ||
                 ( $controller == 'Regions' )
                 ||
                 ( $controller == 'Routes' )
                 ||
                 ( $controller == 'DeliverySchdules' )
                 ||
                 ( $controller == 'Units' )
                 ||
                 ( $controller == 'Users' && $action == 'driver' )
                 ||
                 ($controller == 'Users' && $action == 'driverAdd')
                 ||
                 ($controller == 'Users' && $action == 'driverProfile')
                 ||
                 ( $controller == 'Vehicles' )
                 ||
                 ( ( $controller == 'UserSubscriptions' ) && $action !='customerSubscription')
              )


             { echo 'active'; } ?>">
         <a href="#">
         <i class="fa fa-database" aria-hidden="true"></i>
         <span>Master Data</span>
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">
            <?php if($Acl->check($useraro, 'controllers/Regions/add')) {?>
               <li class="
                  <?php if(($controller == 'Regions' && $action == 'add') || ($controller == 'Regions' && $action == 'edit') ) { ?> active
                  <?php }?>">
                  <a href="<?php echo HTTP_ROOT ?>Regions/add">
                     Manage Region
                  </a>
               </li>
            <?php }?>
            <?php if($Acl->check($useraro, 'controllers/Areas/add')) {?>
               <li class="
                  <?php if(($controller == 'Areas' && $action == 'add') || ($controller == 'Areas' && $action == 'edit') ) { ?> active
                  <?php }?>">
                  <a href="<?php echo HTTP_ROOT ?>Areas/add">
                     Manage Area
                  </a>
               </li>
            <?php }?>
            <?php if($Acl->check($useraro, 'controllers/DeliverySchdules/add')) {?>
               <li  class="
                  <?php if(($controller == 'DeliverySchdules' && $action == 'add') ||($controller == 'DeliverySchdules' && $action == 'edit') ) { ?> active
                  <?php }?>">
                   <a href="<?php echo HTTP_ROOT ?>DeliverySchdules/add">
                     Manage Delivery Schedule
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Categories/add')) {?>
               <li class="
                  <?php if(($controller == 'Categories' && $action == 'add') ||($controller == 'Categories' && $action == 'edit') ) { ?> active
                  <?php }?>">
                  <a href="<?php echo HTTP_ROOT ?>Categories/add">
                     Manage Categories
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Units/add')) {?>
               <li class="
                  <?php if(($controller == 'Units' && $action == 'add') || ($controller == 'Units' && $action == 'edit') ) { ?> active
                  <?php }?>">

                  <a href="<?php echo HTTP_ROOT ?>Units/add">
                     Manage Unit
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Users/driver')) {?>
               <li class="treeview
                  <?php if(($controller == 'Users' && $action == 'driver') || ($controller == 'Users' && $action == 'driverAdd') || ($controller == 'Users' && $action == 'driverProfile') ) { ?> active
                  <?php }?>">
                  <a href="<?php echo HTTP_ROOT ?>Users/driver">
                     Manage Drivers
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Routes/lists')) {?>
               <li class="treeview
                  <?php if(($controller == 'Routes' && $action == 'lists') || ($controller == 'Routes' && $action == 'add') || ($controller == 'Routes' && $action == 'edit')) { ?> active
                  <?php }?>">
                  <a href="
                     <?php echo HTTP_ROOT ?>Routes/lists">Manage Route
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Vehicles/lists')) {?>
               <li class="treeview
                  <?php if(($controller == 'Vehicles' && $action == 'lists') || ($controller == 'Vehicles' && $action == 'add') || ($controller == 'Vehicles' && $action == 'edit') ) { ?> active
                  <?php }?>">
                  <a href="
                     <?php echo HTTP_ROOT ?>Vehicles/lists">Manage Vehicle
                  </a>
               </li>
            <?php } ?>
         </ul>
      </li>
<?php } if($Acl->check($useraro, 'controllers/Products/index')) {?>
         <li class=
            <?php if($controller == 'Products') { echo 'active'; } ?>>
            <a href="
               <?php echo HTTP_ROOT ?>Products/index">
            <i class="fa fa-product-hunt" aria-hidden="true"></i>
            <span>Manage Products</span>
            </a>
         </li>
      <?php }
      if($Acl->check($useraro, 'controllers/ComplaintFeedbackSuggestions/index') || $Acl->check($useraro, 'controllers/Users/view') ){
      ?>
      <li class="treeview
         <?php if( ( $controller == 'Users' && $action =='view' ) || ($controller == 'ComplaintFeedbackSuggestions') || ( $controller == 'Users' && $action =='add' ) || ( $controller == 'Users' && $action =='customerProfile' ) ) { echo 'active'; } ?>">
         <a href="<?php echo HTTP_ROOT ?>Users/view">
            <i class="fa fa-copyright" aria-hidden="true"></i>
            <span>Manage Customers</span>
            <?php
               if( isset($usersCNT) && $usersCNT > 0 )
               {
               ?>
            <span class="badge badge-notify">
            <?php echo $usersCNT ?>
            </span>
            <?php
               }
                 ?>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            <ul class="treeview-menu">
            <?php if($Acl->check($useraro, 'controllers/Users/view')) {?>
               <li class="
                  <?php if( ( $controller == 'Users' && $action =='view' ) || ( $controller == 'Users' && $action =='add' ) || ( $controller == 'Users' && $action =='customerProfile' )  ) { echo 'active'; } ?>">
                  <a href="
                     <?php echo HTTP_ROOT ?>Users/view">Customers
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/ComplaintFeedbackSuggestions/index')) { ?>
               <li class="treeview
                  <?php if($controller == 'ComplaintFeedbackSuggestions') { echo 'active'; } ?>">
               <a href="
                  <?php echo HTTP_ROOT ?>ComplaintFeedbackSuggestions/index">Complaint and Suggestion's
               </a>
               </li>
            <?php } ?>
         </ul>
         </a>
      </li>
      <?php } if($Acl->check($useraro, 'controllers/AclManager')) { ?>
         <li class=
            <?php if( ( ( $controller == 'Users' && $action =='admin') || ( $controller == 'Permissions' ) ) && ( ($action =='admin') || ($action =='edit') || ( $action == 'index' ) ) || ($controller == 'Users' && $action =='adminadd') ) { echo 'active'; } ?>>
            <a href="
               <?php echo HTTP_ROOT ?>AclManager">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>Manage Admin</span>
            </a>
         </li>
      <?php } ?>
      <?php if($Acl->check($useraro, 'controllers/Users/customnotifications')) { ?>
         <li class=
            <?php if($controller === 'Users' && $action =='customnotifications') { echo 'active'; } ?>>
            <a href="
               <?php echo HTTP_ROOT ?>Users/customnotifications" >
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            <span> Custom Notifications
            </span>
            </a>
         </li>
      <?php }
      if($Acl->check($useraro, 'controllers/Containers/usersContainer') || $Acl->check($useraro, 'controllers/Transactions') ) { ?>
      <li class="treeview
         <?php if($controller == 'Containers' || $controller == 'Reports' || $controller == 'Transactions' ) { echo 'active'; } ?>">
         <a href="#">
         <i class="fa fa-registered" aria-hidden="true"></i>
         <span>Reports</span>
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">
            <?php if($Acl->check($useraro, 'controllers/Containers/usersContainer')) { ?>
               <li class="treeview <?php if($controller == 'Containers' && $action == 'usersContainer') { echo 'active'; } ?>">
                  <a href="
                     <?php echo HTTP_ROOT ?>Containers/usersContainer">Container Report
                  </a>
               </li>
            <?php } ?>
            <?php if($Acl->check($useraro, 'controllers/Transactions')) { ?>
                  <li class="treeview <?php if($controller == 'Transactions' ) { echo 'active'; } ?>">
                        <a href="<?php echo HTTP_ROOT ?>Transactions">
                          Sales
                        </a>
                  </li>
            <?php } ?>

            <?php /* if($Acl->check($useraro, 'controllers/Reports/refusedProducts')) { ?>
               <li class="treeview <?php if($controller == 'Reports' && $action == 'refusedProducts') { echo 'active'; } ?>">
                  <a href="
                     <?php echo HTTP_ROOT ?>Reports/refusedProducts">Refused Product Report
                  </a>
               </li>
            <?php } */ ?>
         </ul>
      </li>
    <?php } if($Acl->check($useraro, 'controllers/DriverDelivery/index')) { ?>
         <li class="
            <?php if($controller == 'DriverDelivery') { echo 'active'; } ?>">
            <a href="
               <?php echo HTTP_ROOT ?>DriverDelivery/index">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <span>Driver's Delivery Map</span>
            </a>
         </li>
      <?php } ?>
      <?php if($Acl->check($useraro, 'controllers/Gallery/index')) { ?>
         <li class=
            <?php if($controller == 'Gallery') { echo 'active'; } ?>>
            <a href="
               <?php echo HTTP_ROOT ?>Gallery">
            <i class="fa fa-square-o" aria-hidden="true"></i>
            <span>Gallery</span>
            </a>
         </li>
      <?php } ?>
      <?php if($Acl->check($useraro, 'controllers/Coupons/index')) { ?>
         <li class=
            <?php if($controller == 'Coupons') { echo 'active'; } ?>>
            <a href="
               <?php echo HTTP_ROOT ?>Coupons">
            <i class="fa fa-square-o" aria-hidden="true"></i>
            <span>Recharge Coupons</span>
            </a>
         </li>
      <?php } ?>
      <?php if($Acl->check($useraro, 'controllers/Users/pages')) { ?>
         <li class="treeview
            <?php if( $controller == 'Users' && $action =='pages' ) { echo 'active'; } ?>">
            <a href="">
               <i class="fa fa-copyright" aria-hidden="true"></i>
               <span>Static Pages</span>
               <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
               </span>
               <ul class="treeview-menu">
                  <li class="
                     <?php if ( $controller == 'Users' && $action =='pages' ) { echo 'active'; } ?>">
            <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(1);  ?>">About Us
            </a>
            </li>
            <li class="treeview
               <?php if($controller == 'Users' && $action =='pages') { echo 'active'; } ?>">
            <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(2);  ?>">Privacy Policy
            </a>
            </li>
            <li class="treeview
                           <?php if($controller == 'Users' && $action =='pages') { echo 'active'; } ?>">
                           <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(3);  ?>">Disclaimer
                           </a>
                         </li>
                         <li class="treeview
                           <?php if($controller == 'Users' && $action =='pages') { echo 'active'; } ?>">
                           <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(4);  ?>">Terms and Conditions
                           </a>
                         </li>
                         <li class="treeview
                           <?php if($controller == 'Users' && $action =='pages') { echo 'active'; } ?>">
                           <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(5);  ?>">Refund and Cancellation
                           </a>
                         </li>
           <li class="treeview
               <?php if($controller == 'Users' && $action =='pages') { echo 'active'; } ?>">
            <a href="<?php echo HTTP_ROOT ?>Users/pages/<?php echo base64_encode(6);  ?>">Key Features
            </a>
            </li>
            </ul>
            </a>
         </li>
      <?php } ?>
      <?php if($Acl->check($useraro, 'controllers/Users/contacts')) { ?>
         <li class=
            <?php if($controller == 'Users' && $action == 'contacts') { echo 'active'; } ?>>
            <a href="<?php echo HTTP_ROOT ?>Users/contacts">
            <i class="fa fa-square-o" aria-hidden="true"></i>
            <span>Contact Us</span>
            </a>
         </li>
      <?php } ?>

      <?php if($Acl->check($useraro, 'controllers/Users/settings')) { ?>
         <li class="treeview
            <?php if( $controller == 'Users' && ($action =='settings' || $action =='listPaymentmethods' || $action =='updatePaymentmethod') ) { echo 'active'; } ?>">
            <a href="">
               <i class="fa fa-copyright" aria-hidden="true"></i>
               <span>Settings</span>
               <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
               </span>
               <ul class="treeview-menu">
                 <li class=
                    <?php if($controller == 'Users' && $action == 'settings') { echo 'active'; } ?>>
                    <a href="<?php echo HTTP_ROOT ?>Users/Settings">
                    <i class="fa fa-square-o" aria-hidden="true"></i>
                    <span>Settings</span>
                    </a>
                 </li>
                 <li class=
                    <?php if($controller == 'Users' && $action == 'listPaymentmethods') { echo 'active'; } ?>>
                    <a href="<?php echo HTTP_ROOT ?>Users/listPaymentmethods">
                    <i class="fa fa-square-o" aria-hidden="true"></i>
                    <span>Payment Method</span>
                    </a>
                 </li>
            </ul>
            </a>
         </li>
      <?php } ?>

      <!-- <li class="treeview
         <?php //if($controller == 'Users' && $action == 'notifications') { echo 'active'; } ?>">
         <a href="#">
         <i class="fa fa-bell" aria-hidden="true"></i>
         <span>Help</span>
         <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
         </span>
         </a>
         <ul class="treeview-menu">

         </ul>
      </li> -->
   </ul>
   </li>
   </ul>
</section>
