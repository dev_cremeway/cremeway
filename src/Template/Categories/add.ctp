

<style type="text/css">
   .red{
   color:red;
   }
   .success{
   color: green;
   }
   #loadingmessage {
   position: absolute;
   left: 0;
   top: 0;
   bottom: 0;
   right: 0;
   background: #000;
   opacity: 0.8;
   filter: alpha(opacity=80);
   }
   #loading {
   width: 50px;
   height: 50px;
   position: absolute;
   top: 50%;
   left: 50%;
   margin: -28px 0 0 -25px;
   }
   #err_div{
   color:red;
   }
   .delivvery_place_list{position:relative;}
   .delivvery_place_list .delete {
   position: absolute;
   right: 15px;
   top: 3px;
   }
   .header_dashbord h1 {
   display: inline-block;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
      
       $(".success").fadeOut(4000);
   
   });
   
   
</script>
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header header_dashbord">
      <h1>
         Category Lists
      </h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header">
            <form class="sidebar-form search_bar" method="get" action="add">
               <div class="input-group">
                  <input type="text" placeholder="Search by Category Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                     echo $querystring;
                     } ?>" name="query">
                  <span class="input-group-btn">
                  <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                  </button>
                  </span>
               </div>
            </form>
            <div class="box-tools pull-right">
               <a href="<?php echo HTTP_ROOT ?>Categories/add" class="btn btn-sm btn-info btn-flat pull-left">Reset</a>
            </div>
         </div>
         <!-- /.box-header -->
         <div class="box-body">
            <div class="table-responsive">
               <table class="table no-margin">
                  <thead>
                     <tr>
                        <th>Category Name</th>
                        <th>Created date</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        if(isset($catList) && count($catList) > 0){ 
                            foreach ($catList as $key => $value) {
                          ?>
                     <tr>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo $value['created']->format('d-M-Y'); ?></td>
                        <td>
                           <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Categories/edit/<?php echo base64_encode($value['id']);  ?>">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                           </a>
                           <!-- <a onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Categories/delete/<?php echo base64_encode($value['id']);  ?>">
                              <i class="fa fa-trash-o" aria-hidden="true"></i>
                              </a>-->
                        </td>
                     </tr>
                     <?php
                        } 
                        } else{ 
                        ?>
                     <tr>
                        <td colspan="3" style="color:red;">No Categories were found</td>
                     </tr>
                     <?php
                        } ?>
                  </tbody>
               </table>
               <?php
                  if( count( $catList ) > 0 )
                  {                                
                      ?>
               <?php /* <div class="text-right">
                  <div class="paginator">
                     <nav>
                        <ul class="pagination">
                           <?= $this->Paginator->prev('< ' . __('previous')) ?>
                           <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                           <?= $this->Paginator->next(__('next') . ' >') ?> 
                        </ul>
                     </nav>
                     <?php echo $this->Paginator->counter(
                        'showing {{current}} records out of
                         {{count}} total'); ?>
                  </div>
               </div> */ ?>
               <?php } ?>
            </div>
            <!-- /.table-responsive -->
         </div>
         <!-- /.box-body -->
         <!-- /.box-footer -->
      </div>
      <?php /*         <div class="box box-info">
         <div class="box-header with-border">
           <h3 class="box-title">Add New Category</h3>
         
            
         </div>
         
         <div class="box-body">
         
              <?php if(isset($success['name'])){
            ?>
      <div class="success"><?php echo $success['name']; ?></div>
      <?php
         } ?>
      <form action="add" method="post" id="addProductForm">
         <div class="form-group" id="err_div"></div>
         <div class="form-group" id="err_div_green" style="color:green;"></div>
         <div class="form-group required">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" maxlength="40" required class="form-control min_mum" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
            <input type="hidden" class="form-control" id="" name="status" value="1">
            <?php if(isset($error['name'])){
               ?>
            <p class="red"><?php echo $error['name']; ?></p>
            <?php
               } ?>
         </div>
         <div class="row add_div ">
            <div class="col-sm-12 form-group area_region">
               <label>Add delivery Places</label>
               <a href="javascript:void(0)" id="add" class="btn btn-primary pull-right">Add More</a>
               <br><br>
               <div class="row">
                  <div class="col-sm-4 select_box appendafterselectbox">
                     <select class="form-control selectchange">
                        <option>Select Region/Area</option>
                        <?php
                           if(isset($region_area_list) && count($region_area_list) > 0)
                           {
                             foreach ($region_area_list as $key => $value) {
                                
                             foreach ($value['areas'] as $k => $v) {
                               
                             
                           ?>
                        <option value="<?php echo $value['id'].'_'.$v['id'] ?>"><?php echo $v['name'].'-'.$value['name'] ?></option>
                        <?php 
                           }
                           }
                           }else{
                            ?>
                        <p class="red">Please add Region/Area First</p>
                        <?php
                           } ?>
                     </select>
                  </div>
                  <div class="col-sm-6 checkboxes forchekboxes">
                     <ul class="checkbox-section">
                        <?php
                           if(isset($DeliverySchdules) && count($DeliverySchdules) > 0)
                           {
                             foreach ($DeliverySchdules as $key => $value) {
                               
                           ?>
                        <li class="checkbox">
                           <label><input rel="<?php echo $key ?>" type="checkbox" value="<?php echo $key ?>" name="deliverytiming[]"><?php echo $value; ?></label>
                        </li>
                        <?php
                           } 
                           }else{
                             ?>
                        <p class="red">Please add Delivery Schdules Timing ( Morning,Evenning and Afternoon Timing ) First Before add Category</p>
                        <?php
                           } ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <input type="button" id="submit" class="btn btn-primary" value="Submit"> 
      </form>
      <div id='loadingmessage' style="display:none;">
         <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
      </div>
</div>
</div>
*/ ?>
<!-- RAMAN -->
<style type="text/css">
.add_div{margin-bottom: 30px;}
.checkbox-section {
margin: 0;
padding: 0;
}
ul.checkbox-section li.checkbox {
display: inline-block;
margin-right: 10px;
}
</style>
<!-- RAMAN --> 
<!-- /.box -->
</div>
<!-- /.col -->
<!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>
<script type="text/javascript">
   $(document).ready(function(){
   
     $(document).on('change','.selectchange',function(){
   
                
                var selectare = $(this).val();
                 
                // $(this).parent().siblings(".checkboxes").find('input[type=checkbox]').each(
                $(this).parent().next(".checkboxes").find('input[type=checkbox]').each(
   //$(".addtask").parent().next(".field")
                   function () {
                      
                      var timing = $(this).attr('rel');
                      
                      var are_region_timing_id = selectare+'_'+timing;
                      $(this).val('');
                      $(this).val(are_region_timing_id);    
                       
                
                 });
                 
     });
   
   
   
   
     var selectunithtml = $(".appendafterselectbox").html();
     var checkboxes = $(".forchekboxes").html();
     //var remove = '<a href="javascript:void(0)" class="btn btn-primary delete">Delete</a>';
     $("#add").click(function (e) {
        
      
       
   
       $(".area_region").append('<div class="row delivvery_place_list"><div class="col-sm-4 select_box appendafterselectbox">'+selectunithtml+'</div><div class="col-sm-6 checkboxes forchekboxes">'+checkboxes+'</div><a href="javascript:void(0)" class="btn btn-danger delete">Delete</a></div>');
       // $(".area_region").append(''); 
      // $(".area_region").append(remove);  
        
   
   
   
   });
          
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
   
   
   $("#add").click(function (e) {
    
   var selectunithtml = $("#regionareaselection").html();
   var timinghtml = $("#deliverytiming").html(); 
   
   $("#items").append('<div class="appendediv append"><div> <select class="form-control" name="region_area[]">'+selectunithtml+'</select></div><div>'+timinghtml+'</div><a href="javascript:void(0)" class="delete">Delete</a></div>'); });
   
   
   
   
   $("body").on("click", ".delete", function (e) {
     $(this).prev('div').remove();
     $(this).prev('div').remove();
     $(this).remove()
     
   });
   
   
   $("#submit").click(function(){
     $('#loadingmessage').show();
     $.ajax({
          type: 'POST',
          url:  "add",
          data: $('#addProductForm').serialize(), 
          success: function(response) {
             var isError = JSON.parse(response);
             if(isError.statuscode == 200){
                 $('#loadingmessage').hide();  
                 $("#err_div").hide();
                 $("#err_div_green").show();
                 $("#err_div_green").text(isError.name);
                 $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                 setTimeout(function(){
                   window.location.reload();
                 },1000);
                 
   
             }else{
                 $('#loadingmessage').hide(); 
                 $("#err_div").hide();
                 $("#err_div").text(isError.name);
                 $("#err_div").show();
                 $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
             }
          },
         error: function() {
              
         }
      });
   });
   });
</script>

