<html>
  <head>
  <script>
    var hash = '<?php echo $hash ?>';
    /*function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }*/
    $(document).ready(function(){
      /*if(hash == '') {
        return;
      }*/
      <?php if(!$formError) { ?>
              var payuForm = document.forms.payuForm;
              payuForm.submit();
      <?php } ?>
    });
  </script>
  </head>
  <body>  
    <!-- <h2>PayU Form</h2> -->
    <br/>
    <?php if($formError) { ?>
  
      <span style="color:red">Something went wrong.</span>
      <br/>
      <br/>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <table>
        <tr>
          <!-- <td><b>Mandatory Parameters</b></td> -->
        </tr>
        <tr>
          <!-- <td>Amount: </td> -->
          <td><input type="hidden" name="amount" value="<?php echo (empty($posted['amount'])) ? @$_GET['amount'] : $posted['amount'] ?>" /></td>
          <!-- <td>First Name: </td> -->
          <td><input type="hidden" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? @$_GET['firstname'] : $posted['firstname']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>Email: </td> -->
          <td><input type="hidden" name="email" id="email" value="<?php echo (empty($posted['email'])) ? @$_GET['email'] : $posted['email']; ?>" /></td>
          <!-- <td>Phone: </td> -->
          <td><input type="hidden" name="phone" value="<?php echo (empty($posted['phone'])) ? @$_GET['phone'] : $posted['phone']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>Product Info: </td> -->
          <td colspan="3" style="display: none"><textarea name="productinfo"><?php echo (empty($posted['productinfo'])) ? @$_GET['productinfo'] : $posted['productinfo'] ?></textarea></td>
        </tr>
        <tr>
          <!-- <td>Success URI: </td> -->
          <td colspan="3"><input type="hidden" name="surl" value="<?php echo HTTP_ROOT."CustomerApi/payumoney1"; /*echo (empty($posted['surl'])) ? '' : $posted['surl']*/ ?>" size="64" /></td>
        </tr>
        <tr>
          <!-- <td>Failure URI: </td> -->
          <td colspan="3"><input type="hidden" name="furl" value="<?php echo HTTP_ROOT."CustomerApi/payumoney1";/*echo (empty($posted['furl'])) ? '' : $posted['furl']*/ ?>" size="64" /></td>
        </tr>

        <tr>
          <td colspan="3"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>
        </tr>

        <!-- <tr>
          <td><b>Optional Parameters</b></td>
        </tr> -->
        <tr>
          <!-- <td>Last Name: </td> -->
          <td><input type="hidden" name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
          <!-- <td>Cancel URI: </td> -->
          <td><input type="hidden" name="curl" value="" /></td>
        </tr>
        <tr>
          <!-- <td>Address1: </td> -->
          <td><input type="hidden" name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
          <!-- <td>Address2: </td> -->
          <td><input type="hidden" name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>City: </td> -->
          <td><input type="hidden" name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
          <!-- <td>State: </td> -->
          <td><input type="hidden" name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>Country: </td> -->
          <td><input type="hidden" name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
          <!-- <td>Zipcode: </td> -->
          <td><input type="hidden" name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>UDF1: </td> -->
          <td><input type="hidden" name="udf1" value="<?php echo (empty($posted['udf1'])) ? @$_GET['id'] : $posted['udf1']; ?>" /></td>
          <!-- <td>UDF2: </td> -->
          <td><input type="hidden" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>UDF3: </td> -->
          <td><input type="hidden" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
          <!-- <td>UDF4: </td> -->
          <td><input type="hidden" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
        </tr>
        <tr>
          <!-- <td>UDF5: </td> -->
          <td><input type="hidden" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
          <!-- <td>PG: </td> -->
          <td><input type="hidden" name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
        </tr>
        <tr>
          <?php if(!$hash) { ?>
            <td colspan="4" style="display: none;"><input type="submit" value="Submit" /></td>
          <?php } ?>
        </tr>
      </table>
    </form>
  </body>
</html>