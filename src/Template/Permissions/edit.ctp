

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });

 

</script>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Dashboard
              </h1>
             
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
              </ol>
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Permission</h3>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-12'>

                    <form action="edit" method="post">
                      
                           <div class="form-group">
                            
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

                             </div> 
       
                             
                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">




                        <ul class="list_view">      
                              <?php
                               
                          foreach ($modules as $key=>$value) {
                            
                        ?>

                      <li>
                            <div class="form-group">
                            <div class="checkbox">
                             
                              <?php   
                              if($value == "ComplaintFeedbackSuggestions")
                              {
                               echo "COMPLAINT AND FEEDBACK SUGGESTIONS";
                              }else{
                                echo strtoupper($value); 
                              }

                             ?><label><input type="checkbox" <?php if( in_array($value, $listPermissions) ) { ?> checked <?php  } ?>   name="newpermission[]"   value="<?php echo $value; ?>"></label>



                            
                            </div>

                            </div>
                           </li> 

                          <?php  } ?>

                </ul>

                      </div>
                </div> 
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>

<style type="text/css">
  ul.list_view{margin:0px; padding:0px;}
  ul.list_view li{display:inline-block; float:left; width:25%;}
  ul.list_view li label input[type=checkbox]{margin-top:9px; margin-left: 5px;}
  .submit_btn{text-align:center; margin-top:15px;}
</style>



