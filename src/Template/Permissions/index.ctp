

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
          <section class="content-header header_dashbord">
              <h1>
              User Role Lists
              </h1>
               <div class="customer_bts">
               <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Role</button></a> 
             </div>
             
             
            </section>
    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">

            <form class="sidebar-form search_bar" method="get" action="#">
              <div class="input-group">
                <input type="text" placeholder="Search by Role.." class="form-control" name="q">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
               

              <div class="box-tools pull-right">
                 
                 <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Subscription Type </a> -->
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Role Name</th>  
                   <th>Created</th> 
                   <th>Edit</th> 
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($groupdata) && count($groupdata) > 0){ 
                       foreach ($groupdata as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><?php echo $value['created']->format('Y-m-d'); ?></td>
                      
                     <?php
                      $user = $this->request->session()->read('Auth.User');
                      if( $value['id'] != $user['group_id'] )
                      {
                     ?>
                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Permissions/edit/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>
                      </td> 
                    <?php } ?>

                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Role were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>
              </div>
               
            </div>
             
          </div>



          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Add New Role</h3>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-12'>
                   
                     <form action="<?php echo HTTP_ROOT ?>Permissions/index" method="post">
                      
                           <div class="form-group required">
                            
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                                <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>

                             </div> 


                          <div class="form-group">
                          <label>Assign Permission to this Role</label>
                          </div>    

                      <ul class="list_view">
                         <?php
                          
                          foreach ($modules as $value) {
                            
                        ?>
                        <li>
                            <div class="form-group">
                            <div class="checkbox">
                             
                             <?php   
                              if($value == "ComplaintFeedbackSuggestions")
                              {
                               echo "COMPLAINT AND FEEDBACK SUGGESTIONS";
                              }else{
                                echo strtoupper($value); 
                              }

                             ?> <label><input type="checkbox" name="permission[]"   value="<?php echo $value; ?>"></label>
                            
                            </div>

                            </div>
                            </li>

                          <?php  } ?>

                      </ul>

                          <input type="hidden" name="status" value="1">



                      </div>
                </div> 
                 <div class="form-group submit_btn">
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                 </div>
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>


<style type="text/css">
  ul.list_view{margin:0px; padding:0px;}
  ul.list_view li{display:inline-block; float:left; width:25%;}
  ul.list_view li label input[type=checkbox]{margin-top:9px; margin-left: 5px;}
  .submit_btn{text-align:center; margin-top:15px;}
</style>
  
