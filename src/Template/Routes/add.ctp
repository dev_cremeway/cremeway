 
<?php /*echo $this->Html->css('sol');
      echo $this->Html->script('sol');*/
 ?> 
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
  
</style>

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Add New Route
              </h1>
             
              
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       



          <div class="box box-info">
              <div class="box-header">

               <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Routes/lists" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>  
                 
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-8'>
                   
                   <form id="addProductForm" method="post" action="">


                   <div class="form-group" id="err_div"></div>
                   <div class="form-group" id="err_div_green" style="color:green;"></div> 
                      
                           <div class="form-group required">



                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>
                            
                                <label for="exampleInputEmail1">Route Name</label>
                                
                                <input type="text"  class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                              </div> 

                              <div class="form-group required">
 
                                 <label for="exampleInputEmail1">Assign To Driver</label>
                                
                                 <select class="form-control" id="getcategory"  name="driver_id">
                                   <option value="">Select Driver</option>
                                    <?php if(isset($drivers)&&count($drivers)>0){
                                               
                                               foreach ($drivers as $key => $value) {
                                                   ?>
                                                   <option rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Drivers First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>






       <div class="form-group required">
 
                                 <label for="exampleInputEmail1">Select Timing</label>
                                
                                 <select id="deliverytimingselection" class="form-control" name="deliverytiming">
                                   <option value="">Select Timing</option>
                                    <?php if(isset($DeliverySchdules)&&count($DeliverySchdules)>0){
                                               
                                               foreach ($DeliverySchdules as $key => $value) {
                                                   ?>
                                                   <option rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add DeliverySchdules First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>




                         
                         <div class="col-sm-6 checkboxes" id="show-after-response" style="display:none;"> 
 
                                 
                                
                                   
                                
                      </div>
   







        
                          


                   


                

                   


                  </div>
                </div> 
                 <input type="button" id="submit" class="btn btn-primary" value="Submit"> 
                </form> 
                
                <div id='loadingmessage' style="display:none;">
                <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
               </div>


            </div>  
          </div> 
        </div> 
      </div> 
    </section>








</div>


<script type="text/javascript"> 

$(document).ready(function(){
  
  $("#deliverytimingselection").on('change',function(){
      
        var timingId = $(this).val();

          
         $.ajax({
         type: 'POST',
         url:  "getAllAreaReginsList",
         data: {'deliveryschduleid':timingId}, 
         success: function(response) {
            var regionList = JSON.parse(response);
            if(regionList.statuscode == 200){   
                 
             var targetHtml = $("#show-after-response");

             htmloption = '<label for="exampleInputEmail1">Add places to this route</label>';               
               var length = getLength(regionList.data);
               if(length){ 


                    $.each( regionList.data, function( key, value ) { 
                       htmloption+='<li class="checkbox"><label><input type="checkbox" value="'+value.area_region_id+'" name="region_area[]">'+value.area_region_name+'</label></li>';
                     });
               
                   htmloption+='</ul>'

               }else{
                  htmloption = '';
                  htmloption+='<label for="exampleInputEmail1">Add Area Region First</label>';
               }
              $("#show-after-response").show(); 
              targetHtml.html("");
              targetHtml.append(htmloption);

               


            }else{
              $("#show-after-response").show();  
              var targetHtml = $("#show-after-response");
              htmloption = '<label for="exampleInputEmail1">No Region Area Found For This.</label>';
              targetHtml.html("");
              targetHtml.append(htmloption);

                 
            }
         },
        error: function() {
             
        }
     }); 
  });


 var getLength = function(obj) {
                  var i = 0, key;
                  for (key in obj) {
                      if (obj.hasOwnProperty(key)){
                          i++;
                      }
                  }
                  return i;
              };


}); 
$("#submit").click(function(){
    $('#loadingmessage').show();
    $.ajax({
         type: 'POST',
         url:  "add",
         data: $('#addProductForm').serialize(), 
         success: function(response) {
            var isError = JSON.parse(response);
            if(isError.statuscode == 200){$('#loadingmessage').hide();  
                $("#err_div").hide();
                $("#err_div_green").show();
                $("#err_div_green").text(isError.name);
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                setTimeout(function(){
                  window.location.reload();
                  window.location.replace("<?php echo HTTP_ROOT ?>Routes/lists");
                },1000);
                

            }else{
                $('#loadingmessage').hide(); 
                $("#err_div").hide();
                $("#err_div").text(isError.name);
                $("#err_div").show();
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            }
         },
        error: function() {
             
        }
     });
});

</script>




