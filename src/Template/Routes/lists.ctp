 
<?php echo $this->Html->css('sol');
      echo $this->Html->script('sol');
 ?> 
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }
.append input {
  padding: 0 10px;
  width: 90%;
}
.append select {
  padding: 0 10px;
  width: 90%;
}
      .heading_up{ width: 100%; padding: 0; display: inline-block; }
      .heading_up li {
  display: inline-block;
  list-style: outside none none;
  width: 30%;
}
  
   .append{ width: 100%; padding: 0; display: inline-block; }
.append div {
  display: inline-flex;
  list-style: outside none none;
  width: 30%;
}

#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
  
</style>

<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);


      $("#filterUser").on('change', function(){
           var region_id = this.value;
           if(region_id != '')
           {
            $("#driverform").submit();
          }
        }); 
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
      <section class="content-header header_dashbord">
              <h1>
                Route Listing
              </h1>
               <div class="customer_bts">
               <a href="<?php echo HTTP_ROOT ?>Routes/add"><button type="button" class="btn btn-sm btn-info">Add New Route</button></a> 
             </div>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       



          <div class="box box-info">
              <div class="box-header">
              <div class="left-serach-option">
                <form class="sidebar-form search_bar" method="get" action="lists">
              <div class="input-group">
               <input type="text" placeholder="Search by Routes Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 

             
      </div>

              <div class="right-filter-option">
               <div class="box-filter">  
  
              <form action="lists" id="driverform" method="get">
                            
                             
                            
                                <select class="form-control" name="driverid" id="filterUser">
                                  
                                  <option value="">Filter By Driver</option>
                                    <?php if(isset($driver)&&count($driver)>0){
                                               
                                               foreach ($driver as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($driver_id)&&!empty($driver_id))
                                                 {
                                                    if($key == $driver_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option value="">Please add Driver First</option>  
                                        <?php
                                        } ?>

                                  
                                  
                                </select>
                                
                    </form>           
                              
                 
              </div>
     <a href="<?php echo HTTP_ROOT ?>Routes/lists"><button type="button" class="btn btn-sm btn-info reset_filter">Reset filters</button></a>
             

</div>









                 
              </div>
            
             
              <!--  </div> -->


            <!-- </div>  -->


 

           <div class="table-responsive side-padding">
          
                <table class="table no-margin">

                  <thead>
                  <tr>
                   <th><?php echo $this->Paginator->sort('Routes.name', 'Route Name'); ?></th>  
                   <th><?php echo $this->Paginator->sort('Users.name', 'Route Driver Name'); ?></th> 
                   <th>Customer's List</th>  
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php  
                   if(isset($routes) && count($routes) > 0){ 
                       foreach ($routes as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['route']['name']; ?></td>
                      <td><?php 

                      if(isset($value['user']['name']) && ! empty($value['user']['name']) ){

                        echo $value['user']['name'];  
                      }else{
                         echo "No Any Driver Assigned to this Route";
                      }
                      ?>
                        



                      </td>
                      <td>

                      
                      <?php 
                        if(! isset($value['user']['name']) && empty($value['user']['name']) ){
                       ?>
                      <a href="<?php echo HTTP_ROOT ?>Routes/changeRouteDriver?route_id=<?php echo base64_encode($value['route']['id']);  ?>">
                     Change Driver
                      </a>
                      <?php } ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;



                       <a target="_new" href="<?php echo HTTP_ROOT ?>Routes/order/<?php echo base64_encode($value['route']['id']);  ?>">
                     Set Customer Order Delivery Priority
                      </a>
                      </td>
                      <td>

                        <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Routes/edit/<?php echo base64_encode($value['route']['id']);  ?>/<?php echo base64_encode( $value['delivery_schdule_id'] ) ?>">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                      </a>
                        
                      </td>
                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red">Not Any Routes Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>

                 <?php

            if( count( $routes ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                               <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
         
 <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         

        </div>      
                                </div>
                                <?php } ?>





              </div>
























          </div> 
        </div> 
      </div> 
    </section>








</div>


<script type="text/javascript"> 
$(function() {
    $('#regionareaselection').searchableOptionList({
        showSelectAll: true
    });
});
$("#submit").click(function(){
    $('#loadingmessage').show();
    $.ajax({
         type: 'POST',
         url:  "add",
         data: $('#addProductForm').serialize(), 
         success: function(response) {
            var isError = JSON.parse(response);
            if(isError.statuscode == 200){
                $('#loadingmessage').hide();  
                $("#err_div").show();
                $("#err_div").text(isError.name);
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
                setTimeout(function(){
                   window.location.reload();
                },3000);
                

            }else{
                $('#loadingmessage').hide(); 
              $("#err_div").show();
              $("#err_div").text(isError.name);
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            }
         },
        error: function() {
             
        }
     });
});

</script>




