<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Driver Vehicle'), ['action' => 'edit', $driverVehicle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Driver Vehicle'), ['action' => 'delete', $driverVehicle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverVehicle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Driver Vehicles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver Vehicle'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Vehicles'), ['controller' => 'Vehicles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vehicle'), ['controller' => 'Vehicles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="driverVehicles view large-9 medium-8 columns content">
    <h3><?= h($driverVehicle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Vehicle') ?></th>
            <td><?= $driverVehicle->has('vehicle') ? $this->Html->link($driverVehicle->vehicle->id, ['controller' => 'Vehicles', 'action' => 'view', $driverVehicle->vehicle->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $driverVehicle->has('user') ? $this->Html->link($driverVehicle->user->name, ['controller' => 'Users', 'action' => 'view', $driverVehicle->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($driverVehicle->id) ?></td>
        </tr>
    </table>
</div>
