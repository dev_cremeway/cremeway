

<style type="text/css">
   .red{
   color:red;
   }
   #err_div{
   color:red;
   }
   .success{
   color: green;
   }
   .file {
   visibility: hidden;
   position: absolute;
   }
</style>
<script type="text/javascript">
   $(document).ready(function(){
      
       $(".success").fadeOut(4000);
   
   });
   
   $(document).on('click', '.browse', function(){
   var file = $(this).parent().parent().parent().find('.file');
   file.trigger('click');
   });
   
   $(document).on('change', '.file', function(){
   $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
   }); 
       
   
</script>  
<header class="main-header">
   <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
   <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
   <section class="content-header">
      <h1>
         Dashboard
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- /.box-header -->
      <div class="box box-info">
         <div class="box-header with-border">
            <h3 class="box-title">Add New Group</h3>
            <div class="box-tools pull-right">
               <a href="<?php echo HTTP_ROOT?>Groups/index" class="btn btn-sm btn-info btn-flat pull-left">Back to List </a>
            </div>
         </div>
         <div class="box-body">
            <div class="row">
               <div class='col-sm-4'>
                  <form action="" method="post" id="adminupload" enctype="multipart/form-data">
                  <input type="hidden" name = "id" value="<?php echo $group['id']?>">
                     <div class="form-group" id="err_div"></div>
                     <div class="form-group required">
                        <?php if(isset($error['name'])){
                           ?>
                        <p class="red"><?php echo $error['name']; ?></p>
                        <?php
                           } ?> 
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" required class="form-control" id="name" name="name" value="<?php if(isset($group['name'])){ echo $group['name']; } ?>">
                     </div>
                     <input type="button" id="btn_submit" class="btn btn-primary" value="Submit"> 
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<script type="text/javascript">
   
   
   $("#btn_submit").click(function(){
         
          var name = jQuery.trim( $("#name").val() ); 
   
          if(name == ''){
           $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
           $("#err_div").html("Please enter the name");
          } else{
              $("#adminupload").submit();
          } 
   });
        
</script>

