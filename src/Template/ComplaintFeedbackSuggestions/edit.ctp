<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $complaintFeedbackSuggestion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $complaintFeedbackSuggestion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Complaint Feedback Suggestions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="complaintFeedbackSuggestions form large-9 medium-8 columns content">
    <?= $this->Form->create($complaintFeedbackSuggestion) ?>
    <fieldset>
        <legend><?= __('Edit Complaint Feedback Suggestion') ?></legend>
        <?php
            echo $this->Form->control('content');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('date');
            echo $this->Form->control('product_id', ['options' => $products]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
