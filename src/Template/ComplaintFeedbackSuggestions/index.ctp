<?php
   /*echo $this->Html->script('moment.js');  
   echo $this->Html->script('bootstrap-datetimepicker.js');*/  
  ?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

 <style type="text/css">
   .weekDays-selector input {
   display: none!important;
   }
   .weekDays-selector input[type=checkbox] + label {
   display: inline-block;
   border-radius: 6px;
   background: #dddddd;
   height: 40px;
   width: 30px;
   margin-right: 3px;
   line-height: 40px;
   text-align: center;
   cursor: pointer;
   }
   .weekDays-selector input[type=checkbox]:checked + label {
   background: #2AD705;
   color: #ffffff;
   }

    label{margin-left: 20px;}
    #datepicker{width:180px; margin: 0 20px 20px 20px;}
    #datepicker > span:hover{cursor: pointer;}
    .resolved{ color: green; }
    .box-filter{ text-align: left; }
 </style>  
<script type="text/javascript">
  $( function() {
     $( "#datepicker_start" ).datepicker({
       dateFormat: "yy-mm-dd",
       minDate: 0
     });
     $( "#datepicker_end" ).datepicker({
       dateFormat: "yy-mm-dd",
       minDate: 0
     });
  });
  
  (function($, undefined){
    $(document).ready(function() {       

    userId = '';  
    tableId = '';  
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "Read More";
    var lesstext = "Read Less";
    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });


    $(".resolve").click(function(){
      userId = $(this).attr('rel'); 
      tableId = $(this).attr('tabeleid'); 
       $.ajax({
            url: "<?php echo HTTP_ROOT ?>ComplaintFeedbackSuggestions/markResolve",
            cache: false,
            data:{'reciever_reciever_id':userId,'complaint_id':tableId},
            success: function(response){
              if(response==200){
                alert("Complaint marked as resolved successfully");
                $(this).hide();
                setTimeout(function(){
                  window.location.reload();
                },2000);
              
              }else{                                  
                  alert("Something Went wrong Please try again");
                  /*setTimeout(function(){
                    $("#myModal").hide();
                 window.location.reload();
              },4000);*/
              } 
                
            }
          });

  });


    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

 
  $(".reply-class").click(function(){
       
      userId = $(this).attr('rel'); 
      tableId = $(this).attr('tabeleid'); 
       $("#EnSureModal").modal(); 
  });

  $(".savereply").click(function(){
 
    var repliecomentNotEmpty = jQuery.trim($("#replied-comment").val());
    if(repliecomentNotEmpty ==''){
        alert('Please make comment.');
        return false;
    }else{
       $.ajax({
            url: "<?php echo HTTP_ROOT ?>ComplaintFeedbackSuggestions/replyComment",
            cache: false,
            data:{'reciever_reciever_id':userId,'reply_comment':repliecomentNotEmpty,'c_f_s':tableId},
            success: function(response){
              if(response==200){

              $("#success_qty").show();
              $("#success_qty").css('color:green');
              $("#success_qty").text("Reply has been sent successfully");
              setTimeout(function(){
                $("#myModal").hide();
                window.location.reload();
              },2000);
              
              }else{
                  $("#err_qty").show();
                   
                  $("#err_qty").text("Something Went wrong Please try again");
                  setTimeout(function(){
                    $("#myModal").hide();
                 window.location.reload();
              },4000);
              } 
                
            }
          });
    }

  })
});
})($); 

</script>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
 
a.morelink {
    text-decoration:none;
    outline: none;
}
.morecontent span {
    display: none;
}
.comment {
    width: 240px;
    background-color: #f0f0f0;
    margin: 2px;
}
</style> 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
             <h3><?= __('Complaint Feedback Suggestions') ?></h3> 
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header"> 


              <div class="left-serach-option">
                             
                            
                 <form class="sidebar-form search_bar" method="get" action="<?php echo HTTP_ROOT ?>ComplaintFeedbackSuggestions/index">
                              <div class="input-group">
                               <input type="text" placeholder="Search by Product Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                                   echo $querystring;
                                  } ?>" name="query">
                                    <span class="input-group-btn">
                                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                                      </button>
                                    </span>
                              </div>
                            </form> 
                              
                </div>

                <!-- filter by -->

                <div class="right-filter-option">
                  <div class="box-filter1">
                    <form action="" id="filteruserby" method="get">
                      <div class="form-group">
                        <div class="weekDays-selector" id="weekdays" style="display:none">
                        </div>
                     </div>
                     <div class="box-filter">
                        <label for="exampleInputEmail1">Type</label>
                        <input name="type" class="form-control hasDatepicker" id="" value="<?php if(isset($_GET['type']) && !empty($_GET['type']) ){ echo $_GET['type']; } ?>" type="text">
                      </div>

                      <div class="box-filter">
                        <label for="exampleInputEmail1">From</label>
                        <input name="start_date" class="form-control hasDatepicker" id="datepicker_start" value="<?php if(isset($_GET['start_date']) && !empty($_GET['start_date']) ){ echo $_GET['start_date']; } ?>" type="text" data-date-format="yyyy-mm-dd">
                      </div>
                      <div class="box-filter">
                        <label for="exampleInputEmail1">To</label>
                        <input name="end_date" class="form-control hasDatepicker" id="datepicker_end" value="<?php if(isset($_GET['end_date']) && !empty($_GET['end_date']) ){ echo $_GET['end_date']; } ?>" type="text" data-date-format="yyyy-mm-dd">
                      </div>
                      
                      <button type="submit" name="submit" class="btn btn-sm btn-info reset_filter">Filter</button>
                      <a href="<?php echo HTTP_ROOT; ?>ComplaintFeedbackSuggestions/index">
                        <button type="button" class="btn btn-sm btn-info reset_filter">Reset filters</button>
                      </a>  
                    
                    </form>
                  </div>
                  
                    
              </div> 

                <!-- filter by -->



              <div class="box-tools pull-right">
                 
                 <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Subscription Type </a> -->
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>

                <th>Id</th>
                <th><?= $this->Paginator->sort('user_id','User Name') ?></th>
                
                <th>Subject</th>
                <th>Complaint</th>
                <th><?= $this->Paginator->sort('date') ?></th>
                
                <th><?= $this->Paginator->sort('status') ?></th>
                <th><?= __('Actions') ?></th>
                 
                  </tr>
                  </thead>
                  <tbody>

                   <?php $i = 1; foreach ($complaintFeedbackSuggestions as $complaintFeedbackSuggestion): ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                
                                <td><?= $complaintFeedbackSuggestion->has('user') ? $this->Html->link($complaintFeedbackSuggestion->user->name, ['controller' => 'Users', 'action' => 'customerProfile','?'=>['id'=>base64_encode($complaintFeedbackSuggestion->user->id)]],['target' => '_blank']) : '' ?></td>                                 

                                <td><?php 
                                if($complaintFeedbackSuggestion->product_id == 0){
                                  echo $complaintFeedbackSuggestion->subject;
                                }else{
                                  foreach ($products as $key => $value) {
                                    if($value['id'] == $complaintFeedbackSuggestion->product_id){
                                      echo $value['name'];
                                      }
                                    }
                                  }
                                 ?></td>
                                
                                <td><div class="comment more"><?= $complaintFeedbackSuggestion->content; ?></div></td>

                                <td><?= h($complaintFeedbackSuggestion->date->format('d-M-Y h:i A')) ?></td>
                                
                                <td class="<?= $complaintFeedbackSuggestion->status == 0 ? 'pending':'resolved'; ?>"><?= $complaintFeedbackSuggestion->status == 0 ? 'Pending':'Resolved'; ?></td>

                                <td class="actions">
                                    <a href="javascript:void(0)" class="reply-class" rel="<?php echo base64_encode($complaintFeedbackSuggestion->user->id) ?>" tabeleid="<?php echo base64_encode($complaintFeedbackSuggestion->id);?>">Reply</a>
                                    <a href="view?id=<?php echo base64_encode($complaintFeedbackSuggestion->id);?>">View</a>
                                </td>
                                <?php
                                if($complaintFeedbackSuggestion->status != 1){ ?>
                                  <td>
                                    <a href="javascript:void(0)" class="resolve" rel="<?php echo base64_encode($complaintFeedbackSuggestion->user->id) ?>" tabeleid="<?php echo base64_encode($complaintFeedbackSuggestion->id);?>">
                                      <button type="button" class="btn btn-sm btn-info reset_filter">Resolve</button>
                                    </a>
                                  </td>
                                <?php } ?>

                            </tr>
                            <?php $i++; endforeach; ?> 
                  </tbody>

                </table>


                 <?php

            if( count( $complaintFeedbackSuggestions ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
                                          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php }else{
                                    ?>
                                      
                        <p  style="color:red;font-size:15px;">Not any Feedback OR complaint found</p>
                          
                       
                                    <?php
                                    } ?>

              </div>
               
            </div>
             
          </div>



          
      </div> 
    </section>
</div>



<style type="text/css">
  .delete_icon{ color: red; }
  .edit_icon{ margin-right: 10px; }
</style>



<div class="modal fade" id="EnSureModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

           <div id="success_qty" style="color:green;"></div>
           <div id="err_qty" style="color:red;"></div>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please make Your Reply Comment </h4>
            </div>
            <div class="modal-body">

            <input type="text" class="form-control" id="replied-comment">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-default savereply">Send Reply</button>
            </div>
        </div>
    </div>
</div>