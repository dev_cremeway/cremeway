<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
 
a.morelink {
    text-decoration:none;
    outline: none;
}
.morecontent span {
    display: none;
}
.comment {
    width: 360px;
    background-color: #f0f0f0;
    margin: 10px;
}
</style> 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
             <h3><?= __('Complaint Replies') ?></h3> 
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header"> 
              <div class="pull-left">
                <h3 style="display: inline-block; float: left">Complaint:- &nbsp;&nbsp;&nbsp; </h3>
                <h3 style="text-transform: uppercase; display: inline-block; float: left"> 
                <?php echo "   ".$complaints[0]['content']; ?>                
                </h3>
              </div>  
              <div class="box-tools pull-right">
                 <a href="<?php echo HTTP_ROOT; ?>/ComplaintFeedbackSuggestions/index" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date Time</th>
                    <th>Reply</th>                 
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    foreach (@$replies as $key => $value) {
                  ?>
                      <tr>
                        <td> <?php echo $value['datetime']->format('d-M-Y h:i A'); ?></td>
                        <td><?php echo $value['content']; ?></td>
                      </tr>
                  <?php
                    }
                  ?>    
                   
                  </tbody>

                </table>


                 <?php

            if( count( $replies ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
                                          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php }else{
                                    ?>
                                      
                        <p  style="color:red;font-size:15px;">No Reply till now for this complaint.</p>
                          
                       
                                    <?php
                                    } ?>

              </div>
               
            </div>
             
          </div>



          
      </div> 
    </section>
</div>



<style type="text/css">
  .delete_icon{ color: red; }
  .edit_icon{ margin-right: 10px; }
</style>



<div class="modal fade" id="EnSureModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

           <div id="success_qty" style="color:green;"></div>
           <div id="err_qty" style="color:red;"></div>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please make Your Reply Comment </h4>
            </div>
            <div class="modal-body">

            <input type="text" class="form-control" id="replied-comment">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-default savereply">Send Reply</button>
            </div>
        </div>
    </div>
</div>