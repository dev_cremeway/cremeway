<?php
  echo $this->Html->css('bootstrap.min');
   echo $this->Html->css('font-awesome');  
 //echo $this->Html->script('bootstrap-datetimepicker.js');  
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>invoice</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
    .red1 { color:red; }

    .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 30px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
      body{
      font-family: 'Open Sans', sans-serif;
      overflow-x: visible;
      overflow-y: visible;
      }
      .section-one {
      padding: 4% 0%;
      }
      .invoice-title p {
      margin: 0px;
      font-size: 18px;
      font-weight: 600;
      }
      .info-block ul {
      padding: 0;
      list-style: none;
      }
      .info-block .col-sm-6 {
      padding: 0px;
      }
      .info-block ul li {
      font-weight: 600;
      height: 35px;
      border-bottom: 1px solid #eee;
      padding: 7px 0px;
      }
      .details-receiver {
      text-align: center;
      font-size: 18px;
      font-weight: 600;
      margin: 0px;
      padding: 18px 0px 0px 0px;
      }
      .section-one .table.table-bordered {
      margin: 20px 0px;
      border-bottom: none;
      }
      .section-one thead {
      background: #eee;
      }
      .section-one thead td {
      border-bottom: 0px !important;
      }
      .col-sm-12.foter-amount {
      height: 430px;
      background: #fcfcfc;
      padding: 17px 0px;
      }
      .col-sm-4.amount-words,.col-sm-2.common-seal {
      height: 100%;
      text-align: center;
      }
      .befor-tax ul {
      padding: 0px;
      list-style: none;
      }
      .befor-tax ul li {
      font-weight: 600;
      height: 35px;
      border-bottom: 1px solid #eee;
      padding: 7px 6px;
      }
      .bblck .col-sm-6,.bblck .col-sm-12 {
      padding: 0px;
      }
      .befor-tax ul {
      padding: 0px;
      list-style: none;
      border: 1px solid #e8e8e8;
      border-bottom: none;
      }
      .border-leftn {
      border-left: none !important;
      }
      .col-sm-4.amount-words p, .col-sm-2.common-seal p{
      position: absolute;
      left: 0;
      right: 0;
      bottom: 1px;
      font-weight: 600;
      }
      .panel .table-bordered .text-center strong {
      display: block;
      }
      .col-sm-12.foter-amount.cc{
      height: 270px;
      padding-left: 15px;
      }
      .col-sm-12.foter-amount.cc .col-sm-6 p {
      font-weight: 600;
      text-align: center;
      padding: 6px 0px;
      }
      .col-sm-12.foter-amount.cc .col-sm-8 {
      height: 100%;
      border: 1px solid #eee;
      }
      .col-sm-12.foter-amount.cc .col-sm-8 .col-sm-6 {
      height: 100%;
      }
      .col-sm-6.amount-words {
      border-right: 1px solid #eee;
      }
      .col-sm-12.text-center.bb li {
      line-height: auto !important;
      height: auto;
      }
      .col-sm-12.text-center.bb .height-large.sp {
      height: 145px;
      }
      #home2 .text-center.invoice-title p {
      font-size: 30px;
      font-weight: 600;
      }
      .header-defi {
      border: 1px solid #ddd;
      padding: 10px;
      font-weight: 600;
      font-size: 16px;
      text-align: center;
      width: 50%;
      display: inline-block;
      margin: -1px;
      }
      #home2 .table.table-bordered.text-center {
      margin-top: 0px;
      }
      #home2 .details-receiver {
      padding-bottom: 20px;
      }
      .noppading{
      padding: 0px !important;
      }
      .rightside {
      float: right;
      }
      .printicon {
      float: right;
      font-size: 20px;
      margin-top: -25px;
      }
      .printicon a {
      color: #555555;
      }
      @media all {
        .page-break { display: none; }
      }

      @media print {
        .page-break { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body>
    <section id="home2" class="section-one">
      <div class="container">
        <div class="col-sm-12">
          <div class="panel">
            <div class="">
              <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <td class="text-center"><strong>DATE</strong></td>
                      <td class="text-center"><strong>PRODUCT NAME</strong></td>
                      <td class="text-center"><strong>QUANTITY</strong></td>
                      <td class="text-center"><strong>Rate</strong></td>
                      <td class="text-center"><strong>Amount</strong></td>
                      <td class="text-center"><strong>Less/Discount</strong></td>
                      <td class="text-center"><strong>Less/Discount</strong></td>
                      <td class="text-center"><strong>Supply Amount</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  
                  //pr($finaldata); die;                    
                    foreach ($finaldata as $key => $value) { 
                      $order_data = json_decode($value['order_data'], true);
                  ?>
                                          
                      <?php foreach ($order_data as $key1 => $value1) {

                              if($value1['userId'] == $value['user_id'] ){ 
                                foreach ($value1['items'] as $keyv => $valuev) {                                                      ?>
                                <tr class="m1">
                                  <td><?php echo $value['created'] ?></td>
                                  <td><?php echo $valuev['name'] ?></td>

                                  <?php
                                  if(@$valuev['quantity_child'] != 0){
                                        $temp['quantity']       = @$valuev['quantity_child'];
                                        $temp['amount']         = @$valuev['pricr_per_package']*@$valuev['quantity_child']; ?>
                                        <td><?php echo @$valuev['quantity_child']; ?></td>
                                        <td><?php echo @$valuev['pricr_per_package']*@$valuev['quantity_child']; ?></td>
                                  <?php
                                    }else{
                                        $temp['quantity']       = $valuev['quantity'];
                                        $temp['amount']         = $valuev['pro_price']*$valuev['quantity'];
                                        ?>
                                        <td><?php echo $valuev['quantity']; ?></td>
                                        <td><?php echo $valuev['pro_price']*$valuev['quantity']; ?></td>
                                        <?php 
                                    }
                                  ?>
                                  <td><?php echo $valuev['pro_price']; ?></td>
                                  <td><?php echo $valuev['pro_price']*$valuev['quantity']; ?></td>
                                  <td></td>
                                  <td><?php echo $valuev['pro_price']*$valuev['quantity']; ?></td> 
                                </tr>        
                              <?php
                                }                                 
                                  
                              }           
                          } 
                    } 
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="page-break"></div>      
        
      </div>
    </section>