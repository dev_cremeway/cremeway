<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Container'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Containers'), ['controller' => 'UserContainers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Container'), ['controller' => 'UserContainers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="containers index large-9 medium-8 columns content">
    <h3><?= __('Containers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('capacity') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($containers as $container): ?>
            <tr>
                <td><?= $this->Number->format($container->id) ?></td>
                <td><?= $this->Number->format($container->capacity) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $container->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $container->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $container->id], ['confirm' => __('Are you sure you want to delete # {0}?', $container->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
