<style type="text/css">
  .reddd{
    color:red;
  }
  .success{
    color: green;
  }
  .responsive{
    width: 50px;
    height: 50px;
  }
  .reddd {
  background-color: red;
}
.green {
  background-color: green;
}
.blue {
  background-color: blue;
}
.yellow {
  background-color: yellow;
}
.sphere {
  height: 15px;
  width: 14px;
  border-radius: 50%;
  text-align: center;
  vertical-align: middle;
  font-size: 500%;
  position: relative;
  
  display: inline-block;
  margin: 5%;
}
.sphere::after {
  background-color: rgba(255, 255, 255,0);
  content: '';
  height: 45%;
  width: 12%;
  position: absolute;
  top: 4%;
  left: 15%;
  border-radius: 50%;
  transform: rotate(40deg);
}
</style>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<?php //pr($container);die; ?>
<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
               Customer Left Containers Listing
              </h1>
               <!-- <div class="customer_bts">
               <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Container</button></a> 
             </div> -->
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">

             <div class="left-serach-option">
               <form class="sidebar-form search_bar" method="get" action="usersContainer">
              <div class="input-group">
               <input type="text" placeholder="Search by name,phone no." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 




 </div>

   <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT ?>Containers/usersContainer" class="btn btn-sm btn-info btn-flat pull-left">Reset</a> 

                   <a href="<?php echo HTTP_ROOT ?>Containers/dContainerCsv" class="btn btn-sm btn-info btn-flat pull-left">Download CSV</a>

                  
                 
              </div>



              


 
 
             
            </div>



            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Customer name</th> 
                    <th>Phone No.</th> 
                   <th>Address</th>
                   
                   <th>Container Left</th>  
                   <!-- <th>Action</th>   -->
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                   if(isset($container) && count($container) > 0){ 
                       foreach ($container as $key => $value) {
                     ?>
                      <tr>
                       <td><a target="_blank" href="<?php echo HTTP_ROOT ?>Users/customerProfile?id=<?php echo base64_encode($value['user']['id']);?>"><?php echo $value['user']['name']; ?></a></td>

                        <td><?php echo $value['user']['phoneNo'];?></td>

                      <td><?php echo $value['user']['area']['name'].'   ('.$value['user']['region']['name'].')'; ?></td>
                       
                                            
                      
                      <td>

                      <?php echo $value['left_container_count']?>

                      <?php if(isset($value['user']['subscribed_container']['container_count']) && !empty($value['user']['subscribed_container']['container_count'])){
                         if($value['left_container_count'] > $value['user']['subscribed_container']['container_count']){

                        ?>

                       <div class="sphere reddd"></div>
                        <?php
                        }else{
                          ?>
                          <div class="sphere reddd"></div>
                          <?php
                        }
                      }else{
                        ?>
                        <div class="sphere reddd"></div>
                        <?php
                      } ?>
                        


                      </td> 
                       
                     
                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red">Not Any Containers Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>

                 
                 <?php

            if( count( $container ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                  <div class="paginator">
                                    <nav>
                                       <ul class="pagination">
                                          <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                          <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                          <?= $this->Paginator->next(__('next') . ' >') ?> 
                                       </ul>
                                    </nav>
                                    <?php echo $this->Paginator->counter(
                                       'showing {{current}} records out of
                                        {{count}} total'); ?>
                                 </div>      
                                </div>
                                <?php } ?>























              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



           
          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



