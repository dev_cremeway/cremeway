<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
               Containers Listing
              </h1>
               <div class="customer_bts">
               <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Container</button></a> 

              <a href="<?php echo HTTP_ROOT ?>Containers/usersContainer"><button type="button" class="btn btn-sm btn-info">Customers Container</button></a>
              
             </div>
             
             
            </section>
    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">

             <form class="sidebar-form search_bar" method="get" action="#">
              <div class="input-group">
                <input type="text" placeholder="Search by Containers Capacity.." class="form-control" name="q">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
              
              <div class="box-tools pull-right">
                 
                 <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Container </a> -->
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Containers Capacity</th>  
                   <th>Unit</th>  
                   <th>Created date</th>
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                   if(isset($containers) && count($containers) > 0){ 
                       foreach ($containers as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['capacity']; ?></td>

                      <td><?php  foreach ($unit as $k => $v) {
                                
                                 if($k == $value['unit_id']){
                                    echo strtoupper($v);
                                 }
                         
                      }  ?></td>
                      
                      <td><?php echo $value['created']->format('Y-m-d'); ?></td> 
                       
                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Containers/edit/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>
                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red">Not Any Containers Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>



        <?php

            if( count( $containers ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
         


        </div>      
                                </div>
                                <?php } ?>












              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



         <!--  <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Containers</h3>

               
            </div>
            
            <div class="box-body">

                 <?php if(isset($success['name'])){
                ?>
                <div class="success"><?php echo $success['name']; ?></div>
                <?php
                } ?>
                 <form action="add" method="post">
               
                 


                 <div class="form-group">
                            
                                <label for="exampleInputEmail1">Unit</label>

                                <select class="form-control" required name="unit_id">
                                <option value="">Select unit</option>
                                    <?php if(isset($unit)&&count($unit)>0){
                                               
                                               foreach ($unit as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add unit First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                                 <?php if(isset($error['unit_id'])){
                                  ?>
                                  <p class="red"><?php echo $error['unit_id']; ?></p>
                                  <?php
                                  } ?> 
                             </div>                
   




                  <div class="form-group">
                    <label for="exampleInputEmail1">Capacity</label>
                    
                    <input type="text" required class="form-control" id="" name="capacity" value="<?php if(isset($data['capacity'])){ echo $data['capacity']; } ?>">
                    <input type="hidden" class="form-control" id="" name="status" value="1">
                    <?php if(isset($error['capacity'])){
                      ?>
                      <p class="red"><?php echo $error['capacity']; ?></p>
                      <?php
                      } ?>
                    
                  </div>
                  
                  <input type="submit" class="btn btn-primary" value="Submit">

                
                </form>

               
               
            </div> -->
            
          </div>


          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



