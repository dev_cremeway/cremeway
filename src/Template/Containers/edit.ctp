 
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Container
              </h1>
             
               
            </section>

    
    <!-- Main content -->
    <section class="content">
      
       

          <div class="box box-info">
              <div class="box-header">
              <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Containers/add" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>
                 
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>

                    <form action="" method="post">
                      
                           <div class="form-group">
                            
                                <label for="exampleInputEmail1">Capacity</label>
                                
                                <input type="text" required class="form-control" id="" name="capacity" value="<?php if(isset($data['capacity'])){ echo $data['capacity']; } ?>">
                                
                                <?php if(isset($error['capacity'])){
                                  ?>
                                  <p class="red"><?php echo $error['capacity']; ?></p>
                                  <?php
                                  } ?>

                             </div> 





                             <div class="form-group">
                            
                                <label>Unit</label>

                                <select class="form-control" required name="unit_id">
                                <option value="">Select Unit</option>
                                    <?php if(isset($unit)&&count($unit)>0){
                                               
                                               foreach ($unit as $key => $value) {
                                                   ?>
                                                   <option <?php if($key==$data['unit_id']) {
                                                    ?>
                                                    selected
                                                   <?php }?> value="<?php echo $key ?>"><?php echo strtoupper($value); ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add unit First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                                 <?php if(isset($error['unit_id'])){
                                  ?>
                                  <p class="red"><?php echo $error['unit_id']; ?></p>
                                  <?php
                                  } ?> 
                             </div> 









       
                             
                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">



                      </div>
                </div> 
                 <input type="submit" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>



