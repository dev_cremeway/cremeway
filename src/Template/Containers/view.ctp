<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Container'), ['action' => 'edit', $container->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Container'), ['action' => 'delete', $container->id], ['confirm' => __('Are you sure you want to delete # {0}?', $container->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Containers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Container'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Containers'), ['controller' => 'UserContainers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Container'), ['controller' => 'UserContainers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="containers view large-9 medium-8 columns content">
    <h3><?= h($container->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($container->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Capacity') ?></th>
            <td><?= $this->Number->format($container->capacity) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related User Containers') ?></h4>
        <?php if (!empty($container->user_containers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Container Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($container->user_containers as $userContainers): ?>
            <tr>
                <td><?= h($userContainers->id) ?></td>
                <td><?= h($userContainers->user_id) ?></td>
                <td><?= h($userContainers->container_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserContainers', 'action' => 'view', $userContainers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserContainers', 'action' => 'edit', $userContainers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserContainers', 'action' => 'delete', $userContainers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userContainers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
