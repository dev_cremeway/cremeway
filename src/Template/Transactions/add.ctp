<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Transactions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users Subscription Statuses'), ['controller' => 'UsersSubscriptionStatuses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Users Subscription Status'), ['controller' => 'UsersSubscriptionStatuses', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transactions form large-9 medium-8 columns content">
    <?= $this->Form->create($transaction) ?>
    <fieldset>
        <legend><?= __('Add Transaction') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('debited');
            echo $this->Form->control('credited');
            echo $this->Form->control('onlinetransactionhistory');
            echo $this->Form->control('status');
            echo $this->Form->control('transaction_type');
            echo $this->Form->control('users_subscription_status_id', ['options' => $usersSubscriptionStatuses]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
