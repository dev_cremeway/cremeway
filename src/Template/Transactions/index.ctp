<?php 
    echo $this->Html->css('daterangepicker');
    echo $this->Html->script('daterangepicker');
    echo $this->Html->script('editproduct');
 ?> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  } 
  .box-filter{
    width: 15%;
  } 
</style>

<header class="main-header">
  <?php echo $this->Element('admin_header'); ?>
</header>
<aside class="main-sidebar">
  <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
  <section class="content-header header_dashbord">
    <h1>
        Sales Report 
    </h1>
    <div class="customer_bts">
     <?php if (isset($_GET['transaction_type_id'])) { ?>
                  <a href="<?php echo HTTP_ROOT."transactions/transactionsCsv?schedule_id=". @$_GET['schedule_id']."&area_id=".@$_GET['area_id']."&payment_type=".@$_GET['payment_type']."&product_id=".@$_GET['product_id']."&transaction_type_id=".@$_GET['transaction_type_id']."&from=".@$_GET['from']."&to=".@$_GET['to'] ?>">
                    <button class="btn btn-sm btn-info btn-flat pull-left" type="button">Generate CSV</button>
                  </a>
              <?php 
                }else
                {
                ?> 
                <a href="<?php echo HTTP_ROOT; ?>transactions/transactionsCsv">
                    <button class="btn btn-sm btn-info btn-flat pull-left" type="button">Generate CSV</button>
                </a> 
              <?php 
                }
                ?> 
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="box box-info">
      <div class="box-header ">
        <div class="left-serach-option" style="width:100%;">
            <form action="<?php echo HTTP_ROOT?>transactions/index" method="GET">
              <div class="box-filter">
                  <input id="transaction_id" name="transaction_id" class="form-control" type="text"  value="<?php if (isset($_GET['transaction_id']) && !empty($_GET['transaction_id'])){ echo trim($_GET['transaction_id']); } ?>" placeholder="Transaction Id">                  
              </div>
              <div class="box-filter">
                  <select id="filterschedule" name="schedule_id" class="form-control">
                      <option value="">All Schedules</option>
                      <?php if(!empty($schedules)) {
                        foreach ($schedules as $key => $value) {
                        ?>
                        <option <?php if (isset($_GET['schedule_id']) && !empty($_GET['schedule_id']) && $_GET['schedule_id'] == $key) { ?> selected <?php } ?> value="<?php echo $key;?>"><?php echo $value;?></option>
                      <?php } }?>
                  </select>
              </div>
              <div class="box-filter">
                  <select id="filterarea" name="area_id" class="form-control">
                      <option value="">All Area</option>
                      <?php if(!empty($areas)) {
                        foreach ($areas as $key => $value) {
                        ?>
                        <option <?php if (isset($_GET['area_id']) && !empty($_GET['area_id']) && $_GET['area_id'] == $key) { ?> selected <?php } ?> value="<?php echo $key;?>"><?php echo $value;?></option>
                      <?php } }?>
                  </select>    
              </div>
              <div class="box-filter">
                  <select id="filterproduct" name="product_id" class="form-control">
                      <option value="">All Products</option>
                      <?php if(!empty($products)) {
                        foreach ($products as $key => $value) {
                        ?>
                        <option <?php if (isset($_GET['product_id']) && !empty($_GET['product_id']) && $_GET['product_id'] == $key) { ?> selected <?php } ?> value="<?php echo $key;?>"><?php echo $value;?></option>
                      <?php } }?>
                  </select>
              </div>

              <div class="box-filter">
                  <select id="payment_type" name="payment_type" class="form-control">
                      <option value="">Payment Types</option>
                      <option <?php if (isset($_GET['payment_type']) && !empty($_GET['payment_type']) && $_GET['payment_type'] == "Cr") { ?> selected <?php } ?> value="Cr">Cr</option>
                      <option <?php if (isset($_GET['payment_type']) && !empty($_GET['payment_type']) && $_GET['payment_type'] == "Dr") { ?> selected <?php } ?> value="Dr">Dr</option>
                      <option <?php if (isset($_GET['payment_type']) && !empty($_GET['payment_type']) && $_GET['payment_type'] == "rejected") { ?> selected <?php } ?> value="rejected">Rejected</option>
                      
                  </select>
              </div>
              
              <div class="box-filter">
                  <select id="filterpayment" name="transaction_type_id" class="form-control">
                      <option value="">Transaction Type</option>
                      <?php if(!empty($ttypes)) {
                        foreach ($ttypes as $key => $value) {
                        ?>
                        <option <?php if (isset($_GET['transaction_type_id']) && !empty($_GET['transaction_type_id']) && $_GET['transaction_type_id'] == $key) { ?> selected <?php } ?> value="<?php echo $key;?>"><?php echo $value;?></option>
                      <?php } ?>
                      <option <?php if (isset($_GET['transaction_type_id']) && !empty($_GET['transaction_type_id']) && $_GET['transaction_type_id'] == 10) { ?> selected <?php } ?> value="10">Successful online Recharge</option>
                      <option <?php if (isset($_GET['transaction_type_id']) && !empty($_GET['transaction_type_id']) && $_GET['transaction_type_id'] == 11) { ?> selected <?php } ?> value="11">Failed online Recharge</option>

            <?php     }?>

                  </select>
              </div>

              <div id="reportrange" class="box-filter" style="background: #fff; cursor: pointer; padding: 5px 5px; border: 1px solid #ccc; width: 27%">
                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;  
                  <span id="dates"><?php /*if (isset($_GET['from']) && !empty($_GET['from'])) { echo date("F d,  Y", strtotime($_GET['from']))." - ". date("F d,  Y", strtotime($_GET['to'])); } */ ?></span> <b class="caret"></b> <input type="hidden" name="from">
                  <input type="hidden" name="to">
              </div>

              <input type="submit" name="filter" value="Filter" class="btn btn-sm btn-info reset_filter"/>
              <a href="<?php echo HTTP_ROOT; ?>transactions/index">
                  <input value="Reset" class="btn btn-sm btn-info reset_filter" type="button">
              </a>
                 
            </form>
            <!-- <form method="post" action="<?php echo HTTP_ROOT; ?>transactions/transactionsCsv" name="csv">
              <input type="submit" name="csv" value="Generate CSV" class="btn btn-sm btn-info reset_filter"/>
            </form>  --> 
            
          </div>
          </div>
        </div>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>
                    <?php echo $this->Paginator->sort('Transactions.id', 'Transaction ID'); ?>
                  </th>
                  <th>
                       <?php echo $this->Paginator->sort('Users.name', 'Customer Name'); ?>
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('Transactions.amount', 'Amount'); ?>
                  </th>
                  <th>
                    Balance(Rs.)
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('Transactions.transaction_amount_type', 'Payment Type'); ?>
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('TransactionTypes.transaction_type_name', 'Transaction Type'); ?>
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('Transactions.created', 'Date'); ?>
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('DeliverySchdule.name', 'Schedule'); ?>
                  </th>
                  <th>
                    <?php echo $this->Paginator->sort('Users.Areas.name', 'Area'); ?>
                  </th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php /* pr($transactions);die;*/
                   if(isset($transactions) && count($transactions) > 0){ 
                       foreach ($transactions as $key => $value) {
                     ?>
                <tr>
                  <td> 
                    <?php echo $value['id']; ?>                    
                  </td>
                  <td><?php echo $value['user']['name']; ?></td>
                  <td>
                    <?php echo number_format($value['amount'], 2, '.', ''); ?> 
                  </td>
                  <td>
                    <?php echo number_format($value['balance'], 2, '.', ''); ?> 
                  </td>
                  <td style="text-transform: capitalize;">
                   <?php echo $value['transaction_amount_type']; ?>
                  </td>
                  
                  <td>
                      <?php
                      if($value['transaction_type_id'] == 2 || $value['transaction_type_id'] == 8){
                        if($value['status'] ==1){
                          if($value['transaction_type_id'] == 8){
                              echo 'Online Recharge'."(Successful)";
                          } else {
                             echo $value['transaction_type']['transaction_type_name']."(Successful)";
                          }
                        }else{
                            if($value['transaction_type_id'] == 8){
                              echo 'Online Recharge'."(Failed)";
                            } else {
                               echo $value['transaction_type']['transaction_type_name']."(Failed)";
                            }
                          
                        }
                      }else{
                        echo $value['transaction_type']['transaction_type_name'];
                      } 
                      
                       ?>
                  </td>
                  <td>
                    <?php echo $value['created']->format('Y-m-d'); ?> 
                  </td>
                  <td>
                    <?php echo $value['delivery_schdule']['name']; ?>
                  </td>
                  <td>
                    <?php echo $value['user']['area']['name']; ?>
                  </td>
                  <td>
                    <a href="<?php echo HTTP_ROOT; ?>Transactions/view?id=<?php echo base64_encode($value['id']);?>">
                      View
                    </a>
                  </td>
                  </tr>
                  <?php
                } 
              }else{
                ?>
                  <tr colspan="4">
                    <td style="color:red">Not Any Customers Found</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php

            if( count( $transactions ) > 0 )
            {                                
                ?>
              <div class="text-right">
                <div class="paginator">
                  <nav>
                    <ul class="pagination">
                      <?= $this->Paginator->prev('< ' . __('previous')) ?>
                      <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                      <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                  </nav>
                  <?php echo $this->Paginator->counter('showing {{current}} records out of {{count}} total'); ?> </div>
              </div>
              <?php } ?>
              
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </section>
</div>

<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
    display: inline-block !important;
    margin-left: 10px;
    position: relative;
    width: 9%;
    vertical-align: top;
    /* float: left; */
}
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>

<script type="text/javascript">
$(function() {
    <?php if (isset($_GET['from']) && !empty($_GET['from'])) 
          { ?> 
            var start = "<?php echo date("F d, Y", strtotime($_GET['from'])); ?>";
            var end   = "<?php echo date("F d, Y", strtotime($_GET['to'])); ?>";
            start     = moment(start);
            end       = moment(end);
    <?php }else
          { ?>
            var start = moment().subtract(29, 'days');
            var end = moment();
    <?php } ?>        

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#reportrange input[name=from]').val(start.format('YYYY-MM-DD'));
        $('#reportrange input[name=to]').val(end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});

Highcharts.chart('container', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Sales Reports of Cremeway'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { 
            month: '%e. %b',
            year: '%b'
        },
        title: {
            text: 'Date'
        }
    },
    yAxis: {
        title: {
            text: 'Sales in Rs.'
        },
        min: 0
    },
    tooltip: {
        //headerFormat: '<b>{series.name}</b><br>', {point.x:%e. %b}:
        pointFormat: ' Rs. {point.y:.2f}'
    },

    plotOptions: {
        spline: {
            marker: {
                enabled: true
            }
        }
    },

    series: [{
        //name: 'Winter 2012-2013',
        // Define the data points. All series have a dummy year
        // of 1970/71 in order to be compared on the same x axis. Note
        // that in JavaScript, months start at 0 for January, 1 for February etc.
        name: 'Dr.',
        color:'#2EFE2E',
        data: [
            <?php                
                foreach ($transactions1 as $ke => $val) {
                      $the_date = date("Y, m-1, d", strtotime($val['created']));               
                      echo $data = "[Date.UTC( " .$the_date. " ), ".$val['revenue']." ],";                    
                 } 
              ?>
        ]
    },
    {
        //name: 'Winter 2012-2013',
        // Define the data points. All series have a dummy year
        // of 1970/71 in order to be compared on the same x axis. Note
        // that in JavaScript, months start at 0 for January, 1 for February etc.
        name: 'Cr.',
        color:'#0080FF',
        data: [
            <?php                
                foreach ($transactions2 as $ke => $val) {
                      $the_date = date("Y, m-1, d", strtotime($val['created']));               
                      echo $data = "[Date.UTC( " .$the_date. " ), ".$val['revenue']." ],";                    
                 } 
              ?>
        ]
    },
    {
        //name: 'Winter 2012-2013',
        // Define the data points. All series have a dummy year
        // of 1970/71 in order to be compared on the same x axis. Note
        // that in JavaScript, months start at 0 for January, 1 for February etc.
        name: 'Rejected',
        color:'#FE2E2E',
        data: [
            <?php                
                foreach ($transactions3 as $ke => $val) {
                      $the_date = date("Y, m-1, d", strtotime($val['created']));               
                      echo $data = "[Date.UTC( " .$the_date. " ), ".$val['revenue']." ],";                    
                 } 
              ?>
        ]
    }]
});
</script>