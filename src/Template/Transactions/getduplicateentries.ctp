<?php 
use Cake\Event\Event;
use Cake\Event\EventManager;
use  Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

  $transactionTable=TableRegistry::get('Transactions');
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<style type="text/css">
  body { overflow: auto; } .active { background: #ccc; }
  .table > tbody > tr.active > td, .table > tbody > tr.active > th, .table > tbody > tr > td.active, .table > tbody > tr > th.active, .table > tfoot > tr.active > td, .table > tfoot > tr.active > th, .table > tfoot > tr > td.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > thead > tr.active > th, .table > thead > tr > td.active, .table > thead > tr > th.active{
    background: #ccc;
  }
  .red{color:red; } .success{color: green; } .weekDays-selector input {display: none!important; }
   .weekDays-selector input[type=checkbox] + label {display: inline-block; border-radius: 6px; background: #dddddd; height: 40px; width: 30px; margin-right: 3px; line-height: 40px; text-align: center; cursor: pointer; }
   .weekDays-selector input[type=checkbox]:checked + label {background: #2AD705; color: #ffffff; }
    label{margin-left: 20px;}
    #datepicker{width:180px; margin: 0 20px 20px 20px;}
    #datepicker > span:hover{cursor: pointer;}
    .resolved{ color: green; }
    .box-filter{ text-align: left; }
</style>
<script type="text/javascript">
  $( function() {
     $( "#datepicker_start" ).datepicker({
       dateFormat: "yy-mm-dd",
       minDate: 0
     });
     $( "#datepicker_end" ).datepicker({
       dateFormat: "yy-mm-dd",
       minDate: 0
     });
  });
</script>
              <form method="get" name="filter">
                <input type="text" id="datepicker_start" name="date" data-date-format="yyyy-mm-dd">
                <input type="submit" name="filter" value="filter">
              </form> 
                   
                  <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>User ID</th>
                    <th>Transaction ID</th>
                    <th>Transaction Type</th>  
                    <th>Transaction Amount</th>
                    <th>Remaining Balance</th>
                    <th>Transaction Description</th>
                   <th>Date</th>
                   <th>Area_id</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                  <form action="getduplicateentries" method="post">
                  <?php
                    $i = 1; $j= 1; 
                    $transaction_ids = '';
                    $user_ids = array();

                   if(isset($transaction) && count($transaction) > 0){ 
                       foreach ($transaction as $key => $value) {
                        
                    $transaction2 = $transactionTable->find('all')->contain(['Users'])->where(['Transactions.transaction_amount_type'=> 'Dr','Transactions.transaction_type_id'=> 6,'Transactions.user_id'=> $value['user_id'], 'Transactions.amount'=> $value['amount'], 'Transactions.created'=> $value['created'] ])->hydrate(false)->toArray();
                    if(count($transaction2) > 0){ 
                      foreach($transaction2 as $k => $v){ ?>
                        <tr class="<?php if($j % 2 == 0){ echo 'active';  } ?>">
                          <td>
                            <input type="hidden" name="cc[]" value="<?php echo $v['id']; ?>">
                            <input type="checkbox" name="delete_dup[]" value="<?php echo $v['user_id']; ?>">
                          </td>
                          <td><?php echo $v['user']['name']; ?></td>
                          <td><?php echo $v['user_id']; ?></td>
                          <td><?php echo $v['id']; ?></td>
                          <td><?php echo $v['transaction_amount_type']; ?></td>
                          <td><?php echo 'Rs '.$v['amount']; ?></td>
                          <td><?php if($v['balance'] != 0){ echo 'Rs '.$v['balance']; }else { echo "--"; } ?></td>
                          <td>
                             <?php 
                             if(isset($v['transaction_type']['transaction_type_name'])){
                              if($v['status']==0 && $v['payment_gateway_type'] == 'Paytm'){
                                echo $v['transaction_type']['transaction_type_name']."(failed)";
                              }else{
                              echo $v['transaction_type']['transaction_type_name'];
                              }
                             }
                              if(isset($v['refund_type']['refund_reasons'])){
                              echo ' ( '.$v['refund_type']['refund_reasons'] .' ) ';
                             }
                             ?>                                                 
                          </td>
                          <td><?php if(isset($v['created'])&&!empty($v['created'])){ echo $v['created']->format('d-M-Y'); } else { echo "----"; } ?>&nbsp;&nbsp;<?php if(isset($v['time'])&&!empty($v['time'])){ echo $v['time']->format('H:i:s'); } else { echo "----"; } ?></td> 
                          <td><?php echo $v['area_id']; ?></td>
                        </tr>
                        <?php
                        $transaction_ids = $transaction_ids.",".$v['user_id'];
                        array_push($user_ids, $v['user_id']);
                        $i++;
                      }
                    }
                    ?>
                    
                <?php
                  $j++;
                } ?>
                <tr>
                <?php 
                  ltrim($transaction_ids, ',');  
                  $user_ids = array_unique($user_ids); 
                  foreach ($user_ids as $ky => $vale) {
                    echo $vale.",";
                  }
                ?>                 
                </tr>
                <?php
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Transaction were found</td>
                  
                </tr>
                <?php

                } ?>
                <input type="submit" name="submit">
                </form>
                  </tbody>
                </table>


<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
  display: inline-block;
  margin-left: 10px;
  position: relative;
  width: 10%; vertical-align: top;
} 
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>