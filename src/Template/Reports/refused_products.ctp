 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
 

<script type="text/javascript">
 
 $(document).ready(function(){

   $("#getRefusedProductReportSCV").click(function(){

     $('#filtertorefusedproduct').attr('action', 'refusedProductsDCSV');
     $("#filtertorefusedproduct").submit();

  });
   
   $("#getRefusedProductReport").click(function(){
       $('#filtertorefusedproduct').attr('action', 'refusedProducts');
       var start_date = jQuery.trim($("#datepicker_start").val());
       var end_date = jQuery.trim($("#datepicker_end").val());
       var region_id = jQuery.trim($("#getarea").val());
       var area_id = jQuery.trim($("#ajaxarea").val());
       var filterUser = jQuery.trim($("#filterUser").val());
       var filterbytime = jQuery.trim($("#filterbytime").val());
       
       if( start_date == '' && end_date == '' &&  region_id == '' && area_id == '' && filterUser == '' && filterbytime == ''){
        alert("PLease select the values properly");
        return false;
       }else if( ( start_date != '' && end_date == '' ) || ( start_date == '' && end_date != '' ) ){
         
        alert("PLease select the Start and End Date properly");
        return false; 
       }else{
          $("#filtertorefusedproduct").submit();
        }

   });

             $("#datepicker_start" ).datepicker({ 
              dateFormat: "yy-mm-dd"
              //minDate: 1
            });
             $("#datepicker_end" ).datepicker({ 
              dateFormat: "yy-mm-dd"
              //minDate: 1
            });    

  $("#getarea").on('change', function(){  
           var r_id = $("#getarea").find('option:selected').val();
            
           $("#set_region_id").val(r_id); 

           $("#filterbyregionarea").submit();

           var region_id = this.value;
           regionid = region_id;
           var htmloption = '';  
           $.ajax({
            url: "<?php echo HTTP_ROOT ?>Users/getArea",
            cache: false,
            data:{'id':region_id},
            success: function(regionList){
              $("#getallarealist").show();
               var targetHtml = $("#ajaxarea");
                htmloption = '<option value="">Select Area</option>';               
               var regionList = JSON.parse(regionList);
               var length = getLength(regionList);
               if(length){ 
                    $.each( regionList, function( key, value ) { 
                       htmloption+='<option value='+key+'>'+value+'</option>';
                        
                     });
                   $("#filterbyregionarea").submit(); 
               }else{
                  htmloption = '';
                  htmloption+='<option value="">Please add Area to this region First</option>';
               }
              targetHtml.html(htmloption); 
            }
          });

        });

     var getLength = function(obj) {
                  var i = 0, key;
                  for (key in obj) {
                      if (obj.hasOwnProperty(key)){
                          i++;
                      }
                  }
                  return i;
              };  









         
 });
</script>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
        

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info"> 

            <div class="box-header "> 

                <div class="right-filter-option report-by-customer">
                             

             <form action="refusedProducts" id="filtertorefusedproduct" method="post">
                       <div class="box-filter">
                       <label for="exampleInputEmail1"><i class="fa fa-circle al-ti" aria-hidden="true"></i> Date Range</label>
                       <select class="form-control" name="period" id="filterbytime">
                        <option value="">All Times</option>
                        <option value="today" <?php if(isset($period) && $period == 'today') {
                           ?>
                           selected
                           <?php
                              } ?>>Today</option>
                        
                        <option value="lastweek" <?php if(isset($period) && $period == 'lastweek') {
                           ?>
                           selected
                           <?php
                              } ?>>Last Week</option>


                        <option value="thismonth" <?php if(isset($period) && $period == 'thismonth') {
                           ?>
                           selected
                           <?php
                              } ?>>This Month</option>

                       <option value="lastmonth" <?php if(isset($period) && $period == 'lastmonth') {
                           ?>
                           selected
                           <?php
                              } ?>>Last Month</option>


                       <option value="halfyearly" <?php if(isset($period) && $period == 'halfyearly') {
                           ?>
                           selected
                           <?php
                              } ?>>Half Yearly</option>  

                            <option value="thisyear" <?php if(isset($period) && $period == 'thisyear') {
                           ?>
                           selected
                           <?php
                              } ?>>This Year</option>                


                     </select>   
              </div> 
              <div class="box-filter-or">
                or
              </div>

                   
                <div class="box-filter">
                                         
                <div class="form-group">
                             <label for="exampleInputEmail1">Start Date</label>
                           <input type="text" name="start_date" class="form-control" id="datepicker_start" value="<?php if(isset( $start_date ) && ! empty( $start_date )) { echo $start_date; } ?>">
                          </div> 
                 </div>
                 <div class="box-filter">
                 <div class="form-group">
                             <label for="exampleInputEmail1">End Date</label>
                           <input type="text" name="end_date" class="form-control" id="datepicker_end" value="<?php if(isset( $end_date ) && ! empty( $end_date )) { echo $end_date; } ?>">
                          </div>          
                                            
                              
                </div>




                
             

     
     <input type="button" value="Submit" id="getRefusedProductReport" class="btn btn-sm btn-info reset_filter">

     <a href="<?php echo HTTP_ROOT ?>Reports/refusedProducts" class="btn btn-sm btn-info reset_filter">Reset Filter </a>

     

             
</div>

    

               


            
            </div>


            <!-- /.box-header -->
            <div class="box-body">
            <div class="cust-mer">
             <div class="cust-mer-heading">
             <div class="left-customer-heading">
             Refused Delivery Report
             </div>
             <div class="right-customer-form">
            <div class="box-filter">
                       <select class="form-control" name="user_id" id="filterUser">
                        <option value="">Report By Customer</option>

                         <?php if(isset($users)&&count($users)>0){
                                               
                                               foreach ($users as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($user_id)&&!empty($user_id))
                                                 {
                                                    if($key == $user_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Customer First</option>  
                                        <?php
                                        } ?>
                        
                     
                     </select>   
              </div>


            <div class="box-filter">
              <select class="form-control" name="region_id" id="getarea">
                                  <option value="">Report By Region</option>
                                    <?php if(isset($regions)&&count($regions)>0){
                                               
                                               foreach ($regions as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($region_id)&&!empty($region_id))
                                                 {
                                                    if($key == $region_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Region First</option>  
                                        <?php
                                        } ?>
                                  
                                  
                                </select>
                      </div>

        <div class="box-filter">
       

         <select class="form-control" id="ajaxarea" name="area_id">

                  <option value="">Report By Area</option>
                   <?php if(isset($areas)&&count($areas)>0){
                                               
                                               foreach ($areas as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($area_id)&&!empty($area_id))
                                                 {
                                                    if($key == $area_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Area First</option>  
                                        <?php
                                        } ?> 
                  </select> 
        </div> 
</form>
</div>
</div>

              <div class="table-responsive">
                  <table class="table no-margin">
               <thead>
                  <tr>
                      <th>Customer Name</th>
                     <th>Reason</th>
                     <th>Timing</th>
                     <th>Product</th>
                     <th>Quantity</th>
                     <th> Price</th>
                  </tr>
               </thead>
               <tbody>
               <?php if(isset($rejectedOrders) && count($rejectedOrders) > 0){
                foreach ($rejectedOrders as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value['user']['name']; ?></td>
                  <td><?php echo $value['reason']; ?></td>
                  <td><?php echo $value['delivery_schdule']['name']; ?></td>
                  <td><?php echo $value['rejected_order_items'][0]['product']['name']; ?></td>
                  <td><?php echo $value['rejected_order_items'][0]['qty']; ?></td>
                  <td><?php echo $value['rejected_order_items'][0]['price']; ?></td>
                </tr> 
                <?php
                } ?>

                <?php
                $set = isset($notshowcsv );
                if( $set != 1) {  ?>
                <input type="button" value="Download CSV" id="getRefusedProductReportSCV" class="btn btn-sm btn-info reset_filter refu-sed">
                <?php } ?>

                <?php
                }else{
                  ?>
                  <tr>
                  <td style="color:red;">Not Found Any Refused Product For this selection </td></tr>
                  <?php
                  } ?>
               </tbody>
            </table>



              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           </div>
            <!-- /.box-footer -->
          </div>



           























          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



<style type="text/css">
.sidebar-form.search_bar {
display: inline-block;
margin: 0;
vertical-align: bottom;
width: 30%;
}

.customer_bts .btn.btn-sm.btn-info {
  border: medium none;
  font-size: 13px;
  padding: 8px 12px;
  text-transform: uppercase;
}

.header_dashbord h1 {
  display: inline-block;
}
.customer_bts {
  display: inline-block;
  float: right;
}
.box-tools.filter {
    display: inline-block !important;
    margin-left: 10px;
    position: relative;
    width: 9%;
    vertical-align: top;
    /* float: left; */
}
.filter #filterUser {
  height: 39px;
  position: relative;
  top: -2px;
}

</style>