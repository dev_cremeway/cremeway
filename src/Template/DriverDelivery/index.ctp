<style type="text/css">
  #map_wrapper {
    height: 500px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
.sh_tbl::before {
    content: "";
    float: right;
    font-family: fontawesome;
    font-size: 15px;
    position: absolute;
    right: 0;
}
.sh_tbl{
    list-style: none;
}
.rout-mne table{
    table-layout: auto;
}
</style>  

<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmS9QQWofTpIbYDpWebbUP9lhbBBW5raA"></script>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
             <h3><?= __('Map') ?></h3> 
            </section>


<p id="oldval"></p>


    
    <!-- Main content -->
    <section class="content">

      
      <div class="box box-info">
            <div class=""> 

              <div class="box-tools pull-right">
                 
                 <!--  <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Add New Subscription Type </a> -->
                 
              </div>
            </div> 
            <!-- /.box-header -->
            <div class="box-body">



<div class="driver-div">
<div id="maps1">
<h2>Driver List</h2>
<div class="drive1">All Users</div>
        <?php //echo "Result is:"; 
    //pr($users1);
            foreach ($users1 as $key => $value) { 

                ?>
                <!--option value="<?php echo HTTP_ROOT ?>Users/driverProfile?id=<?php echo base64_encode($value['id']);?>" lat="<?php echo $value['latitude'];?>" long="<?php echo $value['longitude'];?>"><?php echo $value['name']; ?></option-->

            <label class="drive" dr_route='' dr_phone="<?php echo $value['phoneNo']; ?>" dr_name="<?php echo $value['name']; ?>" value="<?php echo $value['id'];?>" area_id ="<?php echo $value['area_id'];?>" region_id ="<?php echo $value['region_id'];?>" lat="<?php echo $value['order_tracking']['lat'];?>" long="<?php echo $value['order_tracking']['lng'];?>"><?php echo $value['name']; ?></label> <br>
            <?php }
        ?>
   </div>
</div>

<div class="map-divv">
<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>

</div>
<div class="routes-driver-div">
     <!-- routes of drivers -->
     <h4 class="route-lst">Route Customer Name </h4>
     <div class="rout-mne">
        <ul class="head_dr" id="head_dr" style="display: none;">  </ul>
       <table class="table no-margin" id="cust_list" style="display: none;">          

                  <tbody id="cust_list1">                   
                  <!--tr class="pend"> </tr>
                  <tr class="rejc"> </tr>
                  <tr class="acc"> </tr-->                   

                  </tbody>
</table>

    <!-- routes of drivers section end -->
</div>
</div>
         
               
            </div>
             
          </div>



          
      </div> 
    </section>
</div>

<?php 
$currentDate = date('Y-m-d');

?>
<script type="text/javascript">

/*$(".head_dr").click(function(){
jQuery(this).toggleClass('arr');
    $("#cust_list").toggle(1000);
});*/


$('body').on('click', '.sh_tbl', function() {
    $("#cust_list").hide();
    $("#cust_list").show(1000);
    valur   = $(this).attr('optvalue');
    valold  = $("#oldval").attr('optvalue');
    $("#oldval").attr("optvalue", valur);
    $("."+valold+"").hide();
    $("."+valur+"").show();

});

jQuery("#uname0").on('click',  function() {
    alert('driver_id sent');
    
});

jQuery(".box-body p").on('click',  function() {
    alert('driver_id sent');
    
});

    function getHour24(timeString)
{
    var time;
    var matches = timeString.match(/^(\d{1,2}):00 (\w{2})/);
    if (matches != null && matches.length == 3)
    {
        time = parseInt(matches[1]);
        if (matches[2] == 'PM')
        {
            time += 12;
        }
    }
    return time;
}


 jQuery(".drive").click(function () {
    /*$('.pend').html("");
    $('.rejc').html("");
    $('.acc').html(""); */
    $(".head_dr").css("display","none");
    $('.head_dr').html(""); 
    $('#cust_list1').html("");

    

$("#cust_list").hide();

        var lat = $( this ).attr('lat');
        var long = $( this ).attr('long');
        var driver_id = $( this ).attr('value');
        var dr_phone = $( this ).attr('dr_phone');
        var dr_name = $( this ).attr('dr_name');
        var area_id = $( this ).attr('area_id');
        var region_id = $( this ).attr('region_id');
        var currentDate = "<?php echo $currentDate ?>";
       
       $.ajax({
        type: 'POST',
        url: "",
        cache : "false",
        data: {'id'  : driver_id},
        success: function(resultData) {         
            if(resultData == 'noroute'){
            $(".head_dr").css("display","block");
            $('.head_dr').append('<tr><td>No Route found for this driver this time.</td></tr>');
        }else{
          $.ajax({
        type: 'POST',
        url: "",
        cache : "false",
        data: {'user_id': driver_id, 'schedule_id'  : resultData },
        success: function(resultData1) {
            $(".head_dr").css("display","block");
          if(! resultData1){

          $('#cust_list1').append('<tr><td>no customer found</td></tr>');
          return;
          }

          myarray = JSON.parse(resultData1);       
          var ij=0; 
            //$.each(myarray, function( key, value ) {
            $.each(myarray, function (index,value1) {
                var myar = value1;
                $('.head_dr').append("<li class='sh_tbl' optvalue="+value1[0].area_name+">"+value1[0].area_name+"</li>");
             $.each(myar, function( key, value ) {

              if(value.date){ var r_date  = value.date.substring(0,10);  }             

               if(r_date ==   currentDate && value.reason == "rejected"){ 
                $('#cust_list1').append('<tr style="display: none;" class="'+value.area_name+'"><td id="uname'+ij+'">'+value.name+'('+value.phoneNo+')('+value.region_area_name+'-'+value.area_name+')</td><td class="rejected"><i class="fa fa-circle" aria-hidden="true"></i></td></tr>');
                 } else if(r_date ==   currentDate){ 
                $('#cust_list1').append('<tr style="display: none;" class="'+value.area_name+'" ><td id="uname'+ij+'">'+value.name+'('+value.phoneNo+')('+value.region_area_name+'-'+value.area_name+')</td><td class="delivered"><i class="fa fa-circle" aria-hidden="true"></i></td></tr>');
                 } 
                else{
                     $('#cust_list1').append('<tr style="display: none;" class="'+value.area_name+'" ><td id="uname'+ij+'">'+value.name+'('+value.phoneNo+')('+value.region_area_name+'-'+value.area_name+')</td><td class="pending"><i class="fa fa-circle" aria-hidden="true"></i></td></tr>');
                }
                ij++;
            });
    });
         }
        });  
    }         

         }
        });

        var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers

    var markers = [   [dr_name, lat,long]    ];
                        
    // Info Window Content

    var infoWindowContent = [  [dr_name +"("+ dr_phone +")"]    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(12);
        google.maps.event.removeListener(boundsListener);
    });        

    });

    jQuery(function($) {
      initialize();
});

function initialize() {  
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    
var ai= [];
//var i = 0;
var markers = [];
var infoWindowContent =[];
$('#maps1').find('label').each(function(n) {
    //alert($(this).attr('lat'));
    
    //ai[n]= $(this).attr('lat');
     markers[n] =   ['MSS', $(this).attr('lat'),$(this).attr('long')] ; 
     infoWindowContent[n] = ['<p class="arealist" driver_id='+$(this).val()+'>'+$(this).attr('dr_name')+'('+$(this).attr('dr_phone')+')</p>']
    

});




    //var markers = [        ['MSS', 30.6601800,76.8605610],        ['MSS', 30.7056,76.8013]    ];                 
    // Info Window Content

    //var infoWindowContent = [   ['MSS'],  ['TSS']    ];


        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(12);
        google.maps.event.removeListener(boundsListener);
    });
    
}

jQuery(".drive1").click(function () {
    $("#cust_list").html("");
    $(".head_dr").html("");
    $(".head_dr").css("display","none");
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {     mapTypeId: 'roadmap'  };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
var ai= [];
var markers = [];
var infoWindowContent =[];
$('#maps1').find('label').each(function(n) {
     markers[n] =   ['MSS', $(this).attr('lat'),$(this).attr('long')] ; 
     infoWindowContent[n] = ['<p class="arealist" driver_id='+$(this).val()+'>'+$(this).attr('dr_name')+'('+$(this).attr('dr_phone')+')</p>']  
});
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({  position: position, map: map, title: markers[i][0] });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
        map.fitBounds(bounds);
    }
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(12);
        google.maps.event.removeListener(boundsListener);
    });
});

  
</script>
