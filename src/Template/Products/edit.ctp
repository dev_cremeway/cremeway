 
<?php echo $this->Html->css('sol');
      echo $this->Html->script('sol');
     // echo $this->Html->script('editproduct');
 ?> 
<style type="text/css">
      .red{
        color:red;
      }
      .success{
        color: green;
      }
      .file {
      visibility: hidden;
      position: absolute;
    }
     #err_div{
        color:red;
      }
      .qty_icon {
  position: absolute;
  right: 0;
  top: 34px;
}
.qty_form .qty_icon .fa {
  color: #367fa9;
  font-size: 32px;
  margin-right: 10px;
}
      .qty_form{ position: relative; }


      .heading_up{ width: 100%; padding: 0; display: inline-block; }

  


#loadingmessage {
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: #000;
    opacity: 0.8;
    filter: alpha(opacity=80);
}
#loading {
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -28px 0 0 -25px;
}
  
</style>
<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


          

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Product
              </h1>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content"> 


          <div class="box box-info">
              <div class="box-header">

                <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>Products/index" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>  
                
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-8'>
                   
                   <form id="addProductForm" method="post" action="" enctype="multipart/form-data">
                   <div class="form-group" id="err_div"></div>
                   <div class="form-group" id="err_div_green" style="color:green;"></div>   
                           <div class="form-group required">

                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>
                            
                                <label for="exampleInputEmail1">Name</label>
                                
                                <input type="text"  class="form-control" id="pro_name" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">
                                
                              </div>


                              
                              <div class="form-group required">

                          
                                <label for="exampleInputEmail1">Quantity(Stock)</label>
                                
                                <input type="text"  class="form-control" id="quantity" name="quantity" value="<?php if(isset($data['quantity'])){ echo $data['quantity']; } ?>">
                                
                              </div> 




<input type="hidden" id="pro_id" name="pro_id" value="<?php echo $data['id']; ?>">


                              <div class="form-group required">
 
                                 <label for="exampleInputEmail1">Category</label>
                                
                                 <select class="form-control" id="getcategory"  name="category_id">
                                   <option value="">Select Category</option>
                                    <?php if(isset($categories)&&count($categories)>0){
                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                                                   <option <?php if($key == $data['category_id']) { ?> selected <?php } ?> rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Category First</option>  
                                        <?php
                                        } ?>
                                   
                                
                                </select>
                                
                              </div>



                     <div class="form-group required">

                       <label for="exampleInputEmail1">Unit</label>
                      
                       <select class="form-control" id="getunit"  name="unit_id">
                         <option value="">Select Unit</option>
                          <?php if(isset($units)&&count($units)>0){
                                     
                                     foreach ($units as $key => $value) {
                                         ?>
                                         <option <?php if($key == $data['unit_id']) { ?> selected <?php } ?> rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                         <?php
                                     }

                          }else{
                              ?>
                              <option>Please add Unit First</option>  
                              <?php
                              } ?>
                         
                      
                      </select>
                      
                  </div> 



                  <div class="form-group">
                    <div class="checkbox">
                    <label><input <?php if($data['iscontainer'] == 1) { ?> checked <?php } ?> type="checkbox" name="iscontainer" value="1" title="Is this product deliver in container ??">Add Container</label>
                  </div>   
                  </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1"></label>

                    <div class="radio">
                      <label><input type="radio" <?php if($data['is_subscribable'] == 1) { ?> checked <?php } ?>  name="is_subscribe" value="yes">Subscribable Product</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" <?php if($data['is_subscribable'] == 0) { ?> checked <?php } ?> value="no" name="is_subscribe">Not Subscribable Product</label>
                    </div>
                  </div>

                         <div class="form-group required">
                          <label for="exampleInputEmail1">Price(Per/Unit)</label>
                          
                          <input type="text"  class="form-control" id="price_per_unit" name="price_per_unit" value="<?php if(isset($data['price_per_unit'])){ echo $data['price_per_unit']; } ?>">
                                
                        </div>
                        <div class="form-group col-sm-12" id="checkboxes">
                        <label class="col-sm-12"><input id="checkall" name="selectAll" type="checkbox">Select All</label>
                              <label class="col-sm-12" for="exampleInputEmail1">Select Areas</label>
                              
                              <!-- <select id="dates-field2" class="multiselect-ui form-control" multiple="multiple" name="areas[]">
                              <?php
                                foreach ($areas as $ak => $av) { ?>
                                  <option <?php if (in_array($ak, $productArea, TRUE)){ ?> selected <?php } ?> value="<?php echo $ak; ?>"><?php echo $av; ?></option>
                                <?php } ?>    
                              </select> -->
                              <?php
                              foreach ($areas as $ak => $av) { ?>
                              <div class="col-sm-4 checkbox">
                                <label><input <?php if (in_array($ak, $productArea, TRUE)){ ?> checked="" <?php } ?> name="areas[]" value="<?php echo $ak; ?>" type="checkbox"><?php echo $av; ?></label>
                              </div>
                              <?php } ?>
                        </div> 


                        <div class="form-group">
                          <label for="exampleInputEmail1">Product description</label>
                          <textarea class="form-control" id="description" name="description" value="<?php if(isset($data['description'])){ echo $data['description']; } ?>"><?php if(isset($data['description'])){ echo $data['description']; } ?></textarea>  
                          </div>

                        <div class="form-group">
                        <label for="exampleInputEmail1">Upload Image(Max size 96*96)</label>
                      <input type="file" name="image" id="photoInput" />
                      </div>
                      <div id="imgContainer">
                        <img src="<?php echo HTTP_ROOT.$data['image'] ?>" style="height: :96px;width: 96px">

                      </div>


                  <div class="form-group">
                   
                   <label for="exampleInputEmail1">Add Child Product</label>


                 <div class="form-group qty_form ">

                  <a class="pull-right btn btn-success" href="javascript:void(0)" id="add">Add More</a>


                   <div  class="heading_up">
                   <div class="row">
                      <div class="col-sm-4">Quantity</div>
                       <div class="col-sm-4">Units</div>

                   <div class="col-sm-2">Price</div>
                   <div class="col-sm-2">Stock_quantity</div>
                     </div>
                  </div>

                   <div class="row append" id="items">
                   


 <?php if(isset($data['product_children'])&&count($data['product_children'])>0){

    $ix = 1;
                                               
       foreach ($data['product_children'] as $kk => $vl) {
           ?>
                 
                    


                    <div class="parentchield">
                    <input type="hidden" class="pro_child_id" name="pro_child_id[]" value="<?php echo $vl['id'] ?>">
                    <div class="col-sm-4 form-group"> <input type="text"  class="form-control" name="childqty[]" value="<?php echo $vl['quantity'] ?>" ></div>


                     <div class="col-sm-4 form-group">
                     <select id="selectunithtml" class="form-control" name="childunit[]">
                     <option value="">Select Unit</option>
                                    <?php if(isset($units)&&count($units)>0){
                                               
                                               foreach ($units as $key => $value) {
                                                   ?>
                                                   <option <?php if($key == $vl['unit_id']) { ?> selected <?php } ?> rel="<?php echo $value; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Unit First</option>  
                                        <?php
                                        } ?>
                      </select>
                      </div>
                      <div class="col-sm-2 form-group"> <input  class="form-control"  name="childprice[]" value="<?php echo $vl['price'] ?>" type="text"></div>
                      <div class="col-sm-2 form-group"> <input  class="form-control"  name="childstock[]" value="<?php echo $vl['in_stock'] ?>" type="text"></div>
                      
                      <a href="javascript:void(0)" class="col-sm-12 text-right delete delete1 form-group">Delete</a>
                     
                      </div>



<?php 
$ix++;
} } ?>
                  </div>
                 


                 </div>

                  </div>      
                <input type="hidden" name="status" value="1">



                  </div>
                </div> 
                 <input type="button" id="button"  class="btn btn-primary" value="Submit">  
                </form> 
                
                <div id='loadingmessage' style="display:none;">
                <img id="loading" src='<?php echo HTTP_ROOT ?>img/ajax-loader.gif'/>
               </div>


            </div> 
          </div> 
        </div> 
      </div> 
    </section>








</div>


<script type="text/javascript">

var $submitBtn = $('#addProductForm').find('input:button'),
  $photoInput = $('#photoInput'),
  $imgContainer = $('#imgContainer');

$('#photoInput').change(function() {

  /*var filename = $("#photoInput").val();
        var extension = filename.replace(/^.*\./, '');
        if (extension == filename) {
            extension = '';
        } else {
            extension = extension.toLowerCase();
        }
        if(extension !="png"){
          alert("Please upload a png image");
          return false;
        }*/


  $photoInput.removeData('imageWidth');
  $imgContainer.hide().empty();

  var file = this.files[0];

  if (file.type.match('image/png')) {
    $submitBtn.attr('disabled', true);

    var reader = new FileReader();

    reader.onload = function() {
      var $img = $('<img />').attr({ src: reader.result });

      $img.on('load', function() {
        
        
        $imgContainer.append($img).show();
        var imageWidth = $img.width();
        var imageHeight = $img.height();
        $photoInput.data('imageWidth', imageWidth);
        if (imageWidth > 96 || imageHeight > 96) {
          alert("Please upload a png image of 96*96");
          $("#photoInput").val("");
          $("#imgContainer").hide();
        } else {
          //alert("Valid image size");
          //$img.css({ width: '400px', height: '200px' });
        }
        $submitBtn.attr('disabled', false);        
        validator.element($photoInput);
      });
    }

    reader.readAsDataURL(file);
  } else {
    alert("Please upload a png image of 96*96");
    $("#photoInput").val("");
    validator.element($photoInput);
  }
});






$(function() {
    $('#regionareaselection').searchableOptionList({
        showSelectAll: true
    });
}); 


  /* $(document).ready(function(){
        $("#getcategory").on('change', function(){
           var getcategory_id = $(this).find('option:selected').attr('rel');
           alert(getcategory_id);
           var htmloption = '';  
           $.ajax({
            url: "<?php //echo HTTP_ROOT ?>Products/getUnit",
            cache: false,
            data:{'id':getcategory_id},
            success: function(regionList){
               var targetHtml = $("#getcategoryunit");
                htmloption = '<option value="">Select Unit</option>';               
               var regionList = JSON.parse(regionList);
               var length = getLength(regionList);
               if(length){ 
                    $.each( regionList, function( key, value ) { 
                       htmloption+='<option value='+key+'>'+value+'</option>';
                     });
               }else{
                  htmloption = '';
                  htmloption+='<option value="">Please add Unit First</option>';
               }
              targetHtml.html(htmloption); 
            }
          });

        }); 

         var getLength = function(obj) {
                  var i = 0, key;
                  for (key in obj) {
                      if (obj.hasOwnProperty(key)){
                          i++;
                      }
                  }
                  return i;
              }; 

        
 });*/
 $(document).ready(function(){


$("#add").click(function (e) {
   
 var selectunithtml = $("#selectunithtml").html(); 

 if( typeof selectunithtml == 'undefined' ){
    
    var selectunithtml = $("#getunit").html(); 
 }

 $("#items").append('<div class="col-sm-12 appendediv append"><div class="row"><div class="col-sm-4 form-group"> <input  class="form-control" required="" name="childqty[]" value="" type="text"></div><div class="col-sm-4 form-group"> <select class="form-control" name="childunit[]">'+selectunithtml+'</select></div><div class="col-sm-2 form-group"> <input  class="form-control" required="" name="childprice[]" value="" type="text"></div><div class="col-sm-2 form-group"> <input class="form-control" name="childstock[]" value="" type="text"></div><a href="javascript:void(0)" class="col-sm-12 text-right delete form-group">Delete</a></div></div>'); });




$("body").on("click", ".delete", function (e) {
  $(this).parent("div").remove();
});
$("body").on("click", ".delete1", function (e) {
  var pro_id = $(this).parent().find('.pro_child_id').val();
  $.ajax({
         type: 'POST',
         cache: false,
         url:  "<?php echo HTTP_ROOT ?>Products/deletechild",
         data: {'pro_id':pro_id}, 
         success: function(result) { 
         }
       });
});
$("#button").click(function(){
    $('#loadingmessage').show();
    var prodctName = $("#pro_name").val();
    var pro_id = $("#pro_id").val();
    var quantity = $("#quantity").val();
    var getcategory = $("#getcategory option:selected").val();
    var getunit = $("#getunit option:selected").val();
    var price = $("#price_per_unit").val();
    $.ajax({
         type: 'POST',
         cache: false,
         url:  "<?php echo HTTP_ROOT ?>Products/alreadyExisteditProductName1",
         data: {'pro_id':pro_id,'prodctName':prodctName}, 
         success: function(result) {            
            if(result == "found"){
                $('#loadingmessage').hide();
                $("#err_div").html('Product with this name already exists. Please choose another name.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");

            } else if(prodctName == ''){
              $('#loadingmessage').hide();
              $("#err_div").html('Please enter product name.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            } else if(quantity == ''){
              $('#loadingmessage').hide();
              $("#err_div").html('Please enter a valid quantity.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            } else if(getcategory == ''){
              $('#loadingmessage').hide();
              $("#err_div").html('Please select a category.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            } else if(getunit == ''){
              $('#loadingmessage').hide();
              $("#err_div").html('Please select a unit.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            } else if(price == ''){
              $('#loadingmessage').hide();
              $("#err_div").html('Please enter price.');
                $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
            } else{
                  $("#addProductForm").submit();
                }
         }
     });
});

  $('#checkall').click(function() {
    var checked = $(this).prop('checked');
    $('#checkboxes').find('input:checkbox').prop('checked', checked);
  });


});
</script>



