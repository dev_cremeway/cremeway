<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>

<script type="text/javascript">
 $(document).ready(function(){
        $("#getarea").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>
 
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
        <section class="content-header header_dashbord">
              <h1>
              Product Listing
              </h1>
               <div class="customer_bts">
               <a href="<?php echo HTTP_ROOT  ?>products/add" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Product</button></a> 
             </div>
             
             
            </section>

    
    <!-- Main content -->
    <section class="content">
      
      <div class="box box-info">
            <div class="box-header">
 <div class="left-serach-option">
             
            
 <form class="sidebar-form search_bar" method="get" action="<?php echo HTTP_ROOT ?>Products/index">
              <div class="input-group">
               <input type="text" placeholder="Search by Product Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
              
</div>
                



              <div class="right-filter-option">
               <div class="box-filter"> 
                 
                <form action="index" id="filteruserby" method="get">
                            
                                
                                <select class="form-control" name="category_id" id="getarea">
                                  <option value="">Filter By Category</option>
                                    <?php if(isset($categories)&&count($categories)>0){
                                               
                                               foreach ($categories as $key => $value) {
                                                   ?>
                                                   <option value="<?php echo $key ?>"

                                                   <?php
                                                 if(isset($category_id)&&!empty($category_id))
                                                 {
                                                    if($key == $category_id) {
                                                    ?>
                                                    selected="selected"
                                                    <?php
                                                   }
                                                   } 

                                                   ?>
                                                   ><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    }else{
                                        ?>
                                        <option>Please add Category First</option>  
                                        <?php
                                        } ?>
                                  
                                  
                                </select>
                             
                    </form>          
                              
                 
              </div>
            <a href="<?php echo HTTP_ROOT ?>Products/index"><button type="button" class="btn btn-sm btn-info">Reset filters</button></a>
</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>
                      
                   <?php echo $this->Paginator->sort('Products.name', 'Name'); ?>


                   </th>  
                   <th> <?php echo $this->Paginator->sort('Categories.name', 'Category'); ?></th> 
                   <th> <?php echo $this->Paginator->sort('Products.price_per_unit', 'Price Per Unit'); ?></th>  
                   <th><?php echo $this->Paginator->sort('Products.quantity', 'Quantity'); ?></th>
                   <!-- <th><?php echo $this->Paginator->sort('Units.name', 'Unit'); ?></th> -->
                   <th>Created date</th>
                   <th>Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php /* pr($user);die;*/
                   if(isset($products) && count($products) > 0){ 
                       foreach ($products as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><?php echo $value['category']['name']; ?></td>
                       <td><?php echo "Rs. ".$value['price_per_unit']."/".$value['unit']['name']; ?></td>
                      <td><?php echo $value['quantity']; ?></td>
                      <!-- <td><?php echo $value['unit']['name']; ?></td> -->
                      <td><?php echo $value['created']->format('d-M-Y'); ?></td>
                      <td>
                      
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>Products/edit/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>
                      
                      
                      <a onclick="return confirm('Are you sure you want to delete?')" class="delete_icon"  href="<?php echo HTTP_ROOT ?>Products/delete/<?php echo base64_encode($value['id']);  ?>">
                     <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a>  

                      </td>
                      </tr> 
                    
                <?php
                } 
              }else{
                ?>
                 
                  <tr colspan="4"><td style="color:red"> Product Not Found</td></tr>

                <?php } ?>

                  </tbody>
                </table>



<?php

            if( count( $products ) > 0 )
            {                                
                ?>
      <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
          <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
              <?php } ?>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
          </div>



           























          <!-- /.box -->
        </div>
        <!-- /.col -->

         
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</div>



