<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
     // $(".success").fadeOut(4000);
  
  });


            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });

            $(function () {
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        
 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header">
              <h1>
                Edit Delivery Schedule
              </h1>
            </section>
    <!-- Main content -->
    <section class="content">

          <div class="box box-info">
              <div class="box-header">

                    <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT?>DeliverySchdules/add" class="btn btn-sm btn-info btn-flat pull-left">Back </a>
                 
              </div>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>

                    <form action="" id="adminupload1" method="post" onsubmit="return confirm('Are You sure that delivery start and  delivery end time is correct ?');">
                      
                           <div class="form-group required">

                              <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">

                             </div> 
       
                            <div class="form-group required">
                               <label for="exampleInputEmail1">Delivery Start Time</label>
                                <div class='input-group date' id='datetimepicker3'>
                                    <input required type='text' name="start_time" class="form-control" value="<?php if(isset($data['start_time'])){ echo $data['start_time']; } ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group required">
                               <label for="exampleInputEmail1">Delivery End Time</label>
                                <div class='input-group date' id='datetimepicker4'>
                                    <input required type='text' name="end_time" class="form-control" value="<?php if(isset($data['end_time'])){ echo $data['end_time']; } ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group required">
                            <label for="exampleInputEmail1">Order Edit Time</label>
                              <input type="text" required class="form-control" id="edit_time" name="edit_time" value="<?php if(isset($data['edit_time'])){ echo $data['edit_time']; } ?>" maxlength="2" />
                             </div> 

                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">


                      </div>
                </div> 
                 <input type="button" id="submit1" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>

<script type="text/javascript">
  $("#submit1").click(function(){
    var edit_time = $("input[name=edit_time]").val();
    if( edit_time > 24 ){
      alert("Please enter a value less than 24");
      $("input[name=edit_time]").val(' ');
    }else if(edit_time <= 0){
      alert("Please enter a valid Order Edit Time");
    }else{
      $("#adminupload1").submit();
    } 

  });
</script>

