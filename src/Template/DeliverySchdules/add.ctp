<?php
 echo $this->Html->script('moment.js');  
 echo $this->Html->script('bootstrap-datetimepicker.js');  
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
     
      $(".success").fadeOut(4000);
  
  });


            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });

            $(function () {
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        
 

</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>

<div class="content-wrapper">
         <section class="content-header header_dashbord">
              <h1>
                Delivery Schedule's List
              </h1>
               <div class="customer_bts">
              <!--  <a href="javascript:void(0)" class="scrolltodown"><button type="button" class="btn btn-sm btn-info">Add New Delivery Schedule</button></a>  -->
             </div>             
            </section>    
    <!-- Main content -->
    <section class="content">      
      <div class="box box-info">
            <div class="box-header">
           <form class="sidebar-form search_bar" method="get" action="add">
              <div class="input-group">
               <input type="text" placeholder="Search by Schedule Name....." class="form-control" value="<?php if(isset($querystring) && !empty($querystring) ){
                   echo $querystring;
                  } ?>" name="query">
                    <span class="input-group-btn">
                      <button class="btn btn-flat" id="search-btn" type="submit"><i class="fa fa-search"></i>
                      </button>
                    </span>
              </div>
            </form> 
               

             <div class="box-tools pull-right">
                 
                  <a href="<?php echo HTTP_ROOT ?>DeliverySchdules/add" class="btn btn-sm btn-info btn-flat pull-left">Reset</a>
                 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                   <th>Schedule Name</th>  
                   <th>Start Time</th>
                   <th>End Time</th>
                   <th>Edit</th>  
                  </tr>
                  </thead>
                  <tbody>
                  <?php

                   if(isset($dslist) && count($dslist) > 0){ 
                       foreach ($dslist as $key => $value) {
                     ?>
                      <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><?php echo $value['start_time']; ?></td>
                      <td><?php echo $value['end_time']; ?></td>  
                      <td>
                      <a class="edit_icon" href="<?php echo HTTP_ROOT ?>DeliverySchdules/edit/<?php echo base64_encode($value['id']);  ?>">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </a>
                      <!--  <a onclick="return confirm('Are you sure you want to delete?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>DeliverySchdules/delete/<?php echo base64_encode($value['id']);  ?>">
                      <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a> -->
                      </td> 
                      </tr> 
                    
                <?php
                } 
              } else{ 
                ?>
                <tr>
                <td colspan="3" style="color:red;">No Delivery Schdule were found</td>
                  
                </tr>
                <?php

                } ?>

                  </tbody>
                </table>


                 <?php

            if( count( $dslist ) > 0 )
            {                                
                ?>
                                <div class="text-right">
                                       <div class="paginator">
                                            <nav>
                                                 <ul class="pagination">
                                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                                     <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                                                    <?= $this->Paginator->next(__('next') . ' >') ?> 
                                                </ul>
                                            </nav>
                                             <?php echo $this->Paginator->counter(
              'showing {{current}} records out of
               {{count}} total'); ?>
         


        </div>      
                                </div>
                                <?php } ?>
              </div>
               
            </div>
             
          </div>



          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Add New Delivery Schedule</h3>
              </div>
            
            <div class="box-body">

             <div class="row">
                  <div class='col-sm-4'>

                     <?php if(isset($success['name'])){
                    ?>
                    <div class="success"><?php echo $success['name']; ?></div>
                    <?php
                    } ?>
                     <form action="add" id="adminupload2" method="post" onsubmit="return confirm('Are You sure that delivery start and  delivery end time is correct ?');">
                      
                           <div class="form-group required">

                            <?php if(isset($error['name'])){
                                  ?>
                                  <p class="red"><?php echo $error['name']; ?></p>
                                  <?php
                                  } ?>
                              

                              <?php
                               //$schdulename = array('morning','evening','afternoon'); 
                              ?>
                              <label for="exampleInputEmail1">Timing</label>
<!-- 
                                <select class="form-control" required name="name">
                                <option value="">Select Timing</option>
                                    <?php if(isset($schdulename)&&count($schdulename)>0){
                                               
                                               foreach ($schdulename as $key => $value) {
                                                   ?>
                                                    <option value="<?php echo $value ?>"><?php echo $value; ?></option>
                                                   <?php
                                               }

                                    } ?>
                                   
                                
                                </select> -->
                                
                                <input type="text" required class="form-control" id="" name="name" value="<?php if(isset($data['name'])){ echo $data['name']; } ?>">


                             </div> 
       
                            <div class="form-group required">
                               <label for="exampleInputEmail1">Delivery Start Time</label>
                                <div class='input-group date' id='datetimepicker3'>
                                    <input required type='text' name="start_time" class="form-control" value="<?php if(isset($data['start_time'])){ echo $data['start_time']; } ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group required">
                               <label for="exampleInputEmail1">Delivery End Time</label>
                                <div class='input-group date' id='datetimepicker4'>
                                    <input required type='text' name="end_time" class="form-control" value="<?php if(isset($data['end_time'])){ echo $data['end_time']; } ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group required">
                               <label for="exampleInputEmail1">Order Edit Time</label>
                                
                                    <input required type='text' name="edit_time" class="form-control" value="<?php if(isset($data['edit_time'])){ echo $data['edit_time']; } ?>" maxlength="2" />
                                
                            </div>

                            <input type="hidden" name="status" value="1">

                      </div>
                </div> 
                 <input type="button" id="submit1" class="btn btn-primary" value="Submit"> 
                </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </section>
</div>

<script type="text/javascript">
  $("#submit1").click(function(){
    var edit_time = $("input[name=edit_time]").val();
    if( edit_time > 24 ){
      alert("Please enter a value less than 24");
      $("input[name=edit_time]").val(' ');
    }else if(edit_time <= 0){
      alert("Please enter a valid Order Edit Time");
    }else{
      $("#adminupload2").submit();
    } 

  });
</script>