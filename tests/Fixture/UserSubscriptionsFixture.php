<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserSubscriptionsFixture
 *
 */
class UserSubscriptionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'subscription_type_id' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'days' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'product_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'users_subscription_status_id' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'unit_id' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'subscriptions_total_amount' => ['type' => 'decimal', 'length' => 6, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'startdate' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'enddate' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'user_subscriptions_fk0' => ['type' => 'index', 'columns' => ['subscription_type_id'], 'length' => []],
            'user_subscriptions_fk1' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
            'user_subscriptions_fk2' => ['type' => 'index', 'columns' => ['users_subscription_status_id'], 'length' => []],
            'user_subscriptions_fk3' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'user_subscriptions_fk4' => ['type' => 'index', 'columns' => ['unit_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'user_subscriptions_fk0' => ['type' => 'foreign', 'columns' => ['subscription_type_id'], 'references' => ['subscription_types', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'user_subscriptions_fk1' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'user_subscriptions_fk2' => ['type' => 'foreign', 'columns' => ['users_subscription_status_id'], 'references' => ['users_subscription_statuses', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'user_subscriptions_fk3' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'user_subscriptions_fk4' => ['type' => 'foreign', 'columns' => ['unit_id'], 'references' => ['units', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'subscription_type_id' => 1,
            'days' => 'Lorem ipsum dolor sit amet',
            'product_id' => 1,
            'quantity' => 1,
            'users_subscription_status_id' => 1,
            'user_id' => 1,
            'unit_id' => 1,
            'subscriptions_total_amount' => 1.5,
            'startdate' => '2017-05-15 06:06:46',
            'enddate' => '2017-05-15 06:06:46'
        ],
    ];
}
