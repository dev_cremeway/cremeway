<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComplaintFeedbackSuggestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComplaintFeedbackSuggestionsTable Test Case
 */
class ComplaintFeedbackSuggestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComplaintFeedbackSuggestionsTable
     */
    public $ComplaintFeedbackSuggestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.complaint_feedback_suggestions',
        'app.users',
        'app.devices',
        'app.regions',
        'app.area_region_notifications',
        'app.areas',
        'app.sales',
        'app.delivery_schdules',
        'app.products',
        'app.categories',
        'app.units',
        'app.custom_orders',
        'app.product_children',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.users_subscription_statuses',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.order_trackings',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ComplaintFeedbackSuggestions') ? [] : ['className' => 'App\Model\Table\ComplaintFeedbackSuggestionsTable'];
        $this->ComplaintFeedbackSuggestions = TableRegistry::get('ComplaintFeedbackSuggestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ComplaintFeedbackSuggestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
