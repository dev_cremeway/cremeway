<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomOrdersTable Test Case
 */
class CustomOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomOrdersTable
     */
    public $CustomOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.custom_orders',
        'app.users',
        'app.devices',
        'app.regions',
        'app.areas',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.complaint_feedback_suggestions',
        'app.order_trackings',
        'app.sales',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.products',
        'app.categories',
        'app.units',
        'app.product_children',
        'app.users_subscription_statuses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CustomOrders') ? [] : ['className' => 'App\Model\Table\CustomOrdersTable'];
        $this->CustomOrders = TableRegistry::get('CustomOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
