<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DeliverySchdulesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DeliverySchdulesTable Test Case
 */
class DeliverySchdulesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DeliverySchdulesTable
     */
    public $DeliverySchdules;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.delivery_schdules',
        'app.sales',
        'app.products',
        'app.categories',
        'app.units',
        'app.custom_orders',
        'app.users',
        'app.devices',
        'app.regions',
        'app.area_region_notifications',
        'app.areas',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.complaint_feedback_suggestions',
        'app.order_trackings',
        'app.transactions',
        'app.users_subscription_statuses',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.product_children'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DeliverySchdules') ? [] : ['className' => 'App\Model\Table\DeliverySchdulesTable'];
        $this->DeliverySchdules = TableRegistry::get('DeliverySchdules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DeliverySchdules);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
