<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserSubscriptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserSubscriptionsTable Test Case
 */
class UserSubscriptionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserSubscriptionsTable
     */
    public $UserSubscriptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_subscriptions',
        'app.subscription_types',
        'app.products',
        'app.users_subscription_statuses',
        'app.users',
        'app.devices',
        'app.regions',
        'app.areas',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.complaint_feedback_suggestions',
        'app.custom_orders',
        'app.order_trackings',
        'app.sales',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications',
        'app.units'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserSubscriptions') ? [] : ['className' => 'App\Model\Table\UserSubscriptionsTable'];
        $this->UserSubscriptions = TableRegistry::get('UserSubscriptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserSubscriptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
