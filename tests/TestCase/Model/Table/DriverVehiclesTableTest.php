<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DriverVehiclesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DriverVehiclesTable Test Case
 */
class DriverVehiclesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DriverVehiclesTable
     */
    public $DriverVehicles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.driver_vehicles',
        'app.vehicles',
        'app.users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.regions',
        'app.area_region_notifications',
        'app.areas',
        'app.sales',
        'app.delivery_schdules',
        'app.products',
        'app.categories',
        'app.units',
        'app.custom_orders',
        'app.product_children',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.users_subscription_statuses',
        'app.complaint_feedback_suggestions',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.order_trackings',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DriverVehicles') ? [] : ['className' => 'App\Model\Table\DriverVehiclesTable'];
        $this->DriverVehicles = TableRegistry::get('DriverVehicles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DriverVehicles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
