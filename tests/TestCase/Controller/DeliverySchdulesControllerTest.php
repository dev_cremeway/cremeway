<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DeliverySchdulesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DeliverySchdulesController Test Case
 */
class DeliverySchdulesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.delivery_schdules',
        'app.sales',
        'app.products',
        'app.categories',
        'app.units',
        'app.custom_orders',
        'app.users',
        'app.devices',
        'app.regions',
        'app.area_region_notifications',
        'app.areas',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.complaint_feedback_suggestions',
        'app.order_trackings',
        'app.transactions',
        'app.users_subscription_statuses',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.product_children'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
