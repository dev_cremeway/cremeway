<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OrderTrackingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OrderTrackingsController Test Case
 */
class OrderTrackingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.order_trackings',
        'app.users',
        'app.devices',
        'app.regions',
        'app.area_region_notifications',
        'app.areas',
        'app.sales',
        'app.delivery_schdules',
        'app.products',
        'app.categories',
        'app.units',
        'app.custom_orders',
        'app.product_children',
        'app.user_subscriptions',
        'app.subscription_types',
        'app.users_subscription_statuses',
        'app.complaint_feedback_suggestions',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
