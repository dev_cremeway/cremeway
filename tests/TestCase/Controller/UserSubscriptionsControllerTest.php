<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UserSubscriptionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UserSubscriptionsController Test Case
 */
class UserSubscriptionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_subscriptions',
        'app.subscription_types',
        'app.products',
        'app.users_subscription_statuses',
        'app.users',
        'app.devices',
        'app.regions',
        'app.areas',
        'app.groups',
        'app.user_types',
        'app.invoices',
        'app.complaint_feedback_suggestions',
        'app.custom_orders',
        'app.order_trackings',
        'app.sales',
        'app.transactions',
        'app.user_balances',
        'app.user_containers',
        'app.user_notifications',
        'app.units'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
