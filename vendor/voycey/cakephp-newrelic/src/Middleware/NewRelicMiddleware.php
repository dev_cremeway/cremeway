<?php
namespace Voycey\NewRelic\Middleware;

use Cake\Utility\Inflector;

class NewRelicMiddleware
{

    public function __invoke($request, $response, $next)
    {   
        $name = $this->nameTransaction($request);
        if(strpos($name, 'versionCode') !== false){
            $response = $next($request, $response);
            return $response;
        }
        if (strpos($name, 'customer-api') !== false || strpos($name, 'driver-api') !== false) {
           // pr($name);die;
            if (extension_loaded('newrelic')) {
                newrelic_name_transaction($name);
            }
            $response = $next($request, $response);
            return $response;
        }
        $response = $next($request, $response);
        if (extension_loaded('newrelic')) {
            newrelic_name_transaction($name);
        }

        return $response;
    }

    /**
     * @param $request
     * @return string
     */
    public function nameTransaction($request)
    {
        $plugin     = $request->getAttribute('params')['plugin'];
        $controller = $request->getAttribute('params')['controller'];
        $action     = $request->getAttribute('params')['action'];
        $passed     = implode('/', $request->getAttribute('params')['pass']);

        $transaction = Inflector::dasherize($controller) . '/' . $action . '/' . $passed;

        if ($plugin !== null) {
            $transaction = Inflector::dasherize($plugin) . '/' . $transaction;
        }

        return $transaction;
    }
}
