<?php 

/**
 * CakePHP 3.x - Acl Manager
 * 
 * PHP version 5
 * 
 * index.ctp
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 * 
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 */

echo $this->Html->css('AclManager.default',['inline' => false]); 
?>
<style type="text/css">
  .red{
    color:red;
  }
  .success{
    color: green;
  }
</style>

<script type="text/javascript">
 $(document).ready(function(){
        $("#getarea").on('change', function(){
           var filterby = this.value;
            
             if(filterby != ''){
               $("#filteruserby").submit();
             }
        }); 
         
 });
</script>
<header class="main-header">
    <?php echo $this->Element('admin_header'); ?>  
</header>
<aside class="main-sidebar">
 <?php echo $this->Element('sidebar_admin'); ?>
</aside>
<div class="content-wrapper">
    <section class="content-header header_dashbord">
      <h1>
         Admin Listing 
      </h1>

      <div class="customer_bts">
         <a href="<?php echo HTTP_ROOT ?>Users/adminadd"><button type="button" class="btn btn-sm btn-info ">Add New Admin</button></a>
         <a href="<?php echo HTTP_ROOT ?>Groups/index"><button type="button" class="btn btn-sm btn-info ">Groups</button></a>
         <a href="<?php echo HTTP_ROOT ?>Roles/index"><button type="button" class="btn btn-sm btn-info ">Roles</button></a>
      </div>
      <div class="box box-info">
         <div class="box-header ">
            <!-- <div class="left-serach-option">
               <div class="input-group">
                  <input type="text" placeholder="Search by name,phone no." class="form-control" name="q">
                  <span class="input-group-btn">
                  <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i>
                  </button>
                  </span>
               </div>
            </div> -->
            <div class="right-filter-option">
               <div class="box-filter">
                  <form action="<?php echo HTTP_ROOT ?>AclManager/index" id="filteruserby" method="get">
                     <select class="form-control" name="role_id" id="getarea">
                        <option value="">Filter By Role</option>
                        <?php  if(isset($groups)&&count($groups)>0){
                           foreach ($groups as $key => $value) {
                               ?>
                        <option value="<?php echo $key ?>"
                           <?php
                              if(isset($role_id)&&!empty($role_id))
                              {
                                 if($key == $role_id) {
                                 ?>
                           selected="selected"
                           <?php
                              }
                              } 
                              
                              ?>
                           ><?php echo $value; ?></option>
                        <?php
                           }
                           
                           }else{
                           ?>
                        <option>Please add role First</option>
                        <?php
                           } ?>
                     </select>
                  </form>
               </div>
               <a href="<?php echo HTTP_ROOT ?>AclManager/index"><button type="button" class="btn btn-sm btn-info">Reset filters</button></a>
            </div>
         </div>
   </section>
<div class="content">


<div class="row panel">
    <div class="col-xs-12 col-md-4 ">
        <h3><?php echo __('Manage'); ?></h3>
        <ul class="options">
            <?php foreach ($manage as $k => $item): ?>
            <li><?php echo $this->Html->link(__('Manage {0}', strtolower($item)), ['controller' => 'Acl', 'action' => 'Permissions', $item]); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-xs-12 col-md-4 ">
        <h3><?php echo __('Update'); ?></h3>
        <ul class="options">
            <li><?php echo $this->Html->link(__('Update ACOs'), ['controller' => 'Acl', 'action' => 'UpdateAcos']); ?></li>
            <li><?php echo $this->Html->link(__('Update AROs'), ['controller' => 'Acl', 'action' => 'UpdateAros']); ?></li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-4 ">
        <h3><?php echo __('Drop and restore'); ?></h3>
        <ul class="options">
            <li><?php echo $this->Html->link(__('Revoke permissions and set defaults'), ['controller' => 'Acl', 'action' => 'RevokePerms'], ['confirm' => __('Do you really want to revoke all permissions? This will remove all above assigned permissions and set defaults. Only first item of last ARO will have access to panel.')]); ?></li>
            <li><?php echo $this->Html->link(__('Drop ACOs and AROs'), ['controller' => 'Acl', 'action' => 'drop'], ['confirm' => __('Do you really want delete ACOs and AROs? This will remove all above assigned permissions.')]); ?></li>
            <li><?php echo $this->Html->link(__('Update ACOs and AROs and set default values'), ['controller' => 'Acl', 'action' => 'defaults'], ['confirm' => __('Do you want restore defaults? This will remove all above assigned permissions. Only first item of last ARO will have access to panel.')]); ?></li>
        </ul>
    </div>
</div>
<div class="box-body">
            <div class="table-responsive">
               <table class="table no-margin">
                  <thead>
                     <tr>
                        <th> <?php echo $this->Paginator->sort('Users.name', 'Name'); ?></th>
                        <th><?php //echo $this->Paginator->sort('Users.phoneNo', 'Phone No.'); ?></th>
                        <th><?php echo $this->Paginator->sort('Groups.name', 'Group'); ?></th>
                        <th><?php echo $this->Paginator->sort('Roles.name', 'Role'); ?></th>
                        <th><?php //echo $this->Paginator->sort('Users.email_id', 'Email'); ?></th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        if(isset($user) && count($user) > 0){ 
                            foreach ($user as $key => $value) {
                          ?>
                     <tr>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php //echo $value['phoneNo']; ?></td>
                        <td>
                           <?php echo  $value['group']['name'];?>
                        </td>
                        <td>
                           <?php echo  $value['role']['name']; ?>
                        </td>
                        <td><?php //echo $value['email_id']; ?></td>
                        
                        <td>
                        <a href="<?php echo HTTP_ROOT ?>Users/editadmin/<?php echo base64_encode($value['id']);  ?>" class="edit_icon">
                            <i aria-hidden="true" class="fa fa-pencil-square-o"></i>
                        </a>
                        <?php if($value['id']!= 206){ ?> 
                           <a onclick="return confirm('Are you sure?')" class="delete_icon" href="<?php echo HTTP_ROOT ?>Users/delete/<?php echo base64_encode($value['id']);  ?>">
                           <i class="fa fa-trash-o" aria-hidden="true"></i>
                           </a>
                        <?php } ?>
                     </tr>
                     <?php
                        } 
                        }else{
                        ?>
                     <tr colspan="4">
                        <td style="color:red">Not Any Users Found</td>
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>
               <?php
                  if( count( $user ) > 0 )
                  {                                
                      ?>
               <div class="text-right">
                  <div class="paginator">
                     <nav>
                        <ul class="pagination">
                           <?= $this->Paginator->prev('< ' . __('previous')) ?>
                           <?= $this->Paginator->numbers(['first' => 'First page']); ?>
                           <?= $this->Paginator->next(__('next') . ' >') ?> 
                        </ul>
                     </nav>
                     <?php echo $this->Paginator->counter(
                        'showing {{current}} records out of
                         {{count}} total'); ?>
                  </div>
               </div>
               <?php }  ?>
            </div>
            <!-- /.table-responsive -->
         </div>
         <!-- /.box-body -->
</div>
</div>
<style type="text/css">
   .sidebar-form.search_bar {
   display: inline-block;
   margin: 0;
   vertical-align: bottom;
   width: 30%;
   }
   .customer_bts .btn.btn-sm.btn-info {
   border: medium none;
   font-size: 13px;
   padding: 8px 12px;
   text-transform: uppercase;
   }
   .header_dashbord h1 {
   display: inline-block;
   }
   .customer_bts {
   display: inline-block;
   float: right;
   }
   .box-tools.filter {
   display: inline-block;
   margin-left: 10px;
   position: relative;
   width: 10%; vertical-align: top;
   } 
   .filter #filterUser {
   height: 39px;
   position: relative;
   top: -2px;
   }
</style>
