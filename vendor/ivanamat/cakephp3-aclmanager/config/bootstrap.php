<?php

/**
 * CakePHP 3.x - Acl Manager
 * 
 * PHP version 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 * 
 * @author   Ivan Amat <dev@ivanamat.es>
 * @copyright     Copyright 2016, Iván Amat
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://github.com/ivanamat/cakephp3-aclmanager
 *
 * @author        Frédéric Massart - FMCorz.net
 * @copyright     Copyright 2011, Frédéric Massart
 * @link          http://github.com/FMCorz/AclManager
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

use Cake\Core\Configure;

/**
 * List of AROs (Class aliases)
 * Order is important! Parent to Children
 */
// Configure::write('AclManager.aros', array('Groups', 'Roles', 'Users'));

/**
 * Limit used to paginate AROs
 * Replace {alias} with ARO alias
 * Configure::write('AclManager.{alias}.limit', 3)
 */
// Configure::write('AclManager.Role.limit', 3);

/**
 * Routing Prefix
 * Set the prefix you would like to restrict the plugin to
 * @see Configure::read('Routing.prefixes')
 */
// Configure::write('AclManager.prefix', 'admin');

/**
 * Ugly identation?
 * Turn off when using CSS
 */
Configure::write('AclManager.uglyIdent', true);
				
/**
 * Actions to ignore when looking for new ACOs
 * Format: 'action', 'Controller/action' or 'Plugin.Controller/action'
 */
if(!Configure::check('AclManager.ignoreActions')) {
    Configure::write('AclManager.ignoreActions', array('isAuthorized','Pages/display','savedInActivity','pushnotifications','checkItemAnotherTime','updatedCustomersBalance','getPrice','checkNextDelivery','factorySubscription','getModifydata','addpush','sendmail','addpush1','sendSMS','CustomerApi/notify','CustomerApi/verificationOtp','CustomerApi/sendOtp','CustomerApi/getSchdule','CustomerApi/getBalanceAndDeliveryInfo','CustomerApi/recomdedItemMilkManDetails','CustomerApi/categoryProduct','CustomerApi/getCustomerBalance','CustomerApi/getAllRegions','CustomerApi/getAllRegionsArea','CustomerApi/updateProfile','CustomerApi/updateOrder','CustomerApi/addSubscription','CustomerApi/validateUserRegion','CustomerApi/getUnitNameAndId','CustomerApi/getCategoriesAndSubscriptionTypes','CustomerApi/checkCategoryDeliverThisCustomer','CustomerApi/getProductUnitNameAndChieldren','CustomerApi/getProductChildrenPrice','CustomerApi/getProductPriceManually','CustomerApi/activeDeActiveSubscriptionList','CustomerApi/changeSubscriptionStatus','CustomerApi/addToNextDelivery','CustomerApi/getUserSubs','CustomerApi/getCalendraEvent','CustomerApi/logout','CustomerApi/gettransactions','CustomerApi/aboutus','CustomerApi/paytm','CustomerApi/payumoney','CustomerApi/createchecksum','CustomerApi/verifychecksum','CustomerApi/paytmurl','CustomerApi/addtransaction','CustomerApi/getorderhistory','CustomerApi/getnotifications','CustomerApi/updateusersubscriptions','CustomerApi/addfeedback','CustomerApi/feedbacklist','CustomerApi/reedemcoupon','CustomerApi/balancenotifications','CustomerApi/pushnotificationstoall','CustomerApi/getdailymilk','CustomerApi/deletecustomorder','CustomerApi/getAllSubscriptions','CustomerApi/getDateRange','CustomerApi/contactus','CustomerApi/settings','CustomerApi/getProfile','CustomerApi/getProfileInfo','CustomerApi/changeOrder','DriverApi/index','DriverApi/login','DriverApi/logout','DriverApi/updateProfileDriver','DriverApi/getSchdule','DriverApi/getAllCustomer','DriverApi/getAllRoute','DriverApi/getAllRouteCustomer','DriverApi/updateDelivery','DriverApi/startTracking','DriverApi/stopTracking','DriverApi/sendPushToCustomerFromDriver','DebugKit/Requests','DebugKit/view','DebugKit/MailPreview','DebugKit/index','DebugKit/sent','DebugKit/email','DebugKit/Composer','DebugKit/checkDependencies','DebugKit/Toolbar','DebugKit/clearCache','DebugKit/Panels','DebugKit/index','DebugKit/view','Regions/delete','Areas/delete','DeliverySchdules/delete','Categories/deleteCategory','Categories/validatedeliverytiming','Categories/delete','getCouponCode'));
}

/**
 * List of ARO models to load
 * Use only if AclManager.aros aliases are different than model name
 */
// Configure::write('AclManager.models', array('Group', 'Customer'));

/**
 * END OF USER SETTINGS
 */

/**
 * AclManager settings
 */
Configure::write("AclManager.version", "1.0b");
if (!is_array(Configure::read('AclManager.aros'))) {
	Configure::write('AclManager.aros', array(Configure::read('AclManager.aros')));
}
if (!is_array(Configure::read('AclManager.ignoreActions'))) {
	Configure::write('AclManager.ignoreActions', array(Configure::read('AclManager.ignoreActions')));
}
if (!Configure::read('AclManager.models')) {
	Configure::write('AclManager.models', Configure::read('AclManager.aros'));
}
