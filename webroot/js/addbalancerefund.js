(function($, undefined){
  $(document).ready(function(){

  superGlobalRecharge = 0;
  superGlobalRechargeRefund = 0;
  superGlobalrefundReason = '';

     isValidAmount = 0;
       $('#addprice,#addpriceRefund,#addpricededuct').on("input", function() {
           var dInput = this.value;
            if( ( isFloat(dInput) || isInteger(dInput)) && dInput != '') {
              isValidAmount = 1;
              $("#err_qty").hide();
            }else{
              isValidAmount = 0;
              this.value = '';
              $("#err_qty").show();
              $("#err_qty").text("Please enter the valid amount");
              $('html, body, .content-wrapper').animate({ scrollTop: 0 }, "slow");
              return false;
            }
       });

          function isFloat(n) {
            return !!(n % 1);
        }

        function isInteger(value) {
             if ((undefined === value) || (null === value)) {
                return false;
            }
            return value % 1 == 0;
        }

     $(document).on('change','#getcategory',function(){

        superGlobalrefundReason = $("option:selected", this).val();

        if(superGlobalrefundReason == 99){
          $("#getcategory_res").append("<input type='text' id='other_reason' class='form-control' placeholder='Enter reason'>");
        }
        if(superGlobalrefundReason != 99){
          $("#getcategory_res").html("");
        }

     });
     $(document).on('change','#getcategory1',function(){

        superGlobaldeductReason = $("option:selected", this).val();
        if(superGlobaldeductReason == 99){
          $("#getcategory_res1").append("<input type='text' id='other_reason' class='form-control' placeholder='Enter reason'>");
        }
        if(superGlobaldeductReason != 99){
          $("#getcategory_res1").html("");
        }

     });



     $("#btnRCHRGE").click(function(){
       if(isValidAmount && superGlobalRecharge == 1 && superGlobalRechargeRefund == 0 && superGlobalRechargededuct == 0){

          $("#btnRCHRGE").attr('disabled',true);
          $('#gif').css('visibility', 'visible');
          var userid=$("#user_customer_id").val();
          var amount = $("#addprice").val();
          $.ajax({
            url: "addBalance",
            cache: false,
            data:{'notaddidbeforethisusergetfrommakeidmohan':userid,'amountnotneedtoberechargeonetime':amount},
            success: function(response){
              if(response==200){

              $("#success_qty").show();
              $("#success_qty").css('color:green');
              $("#success_qty").text("Amount has been added successfully");
              $('#gif').css('visibility', 'hidden');
              setTimeout(function(){
                $("#myModal").hide();
                window.location.reload();
              },2000);

              }else{
                  $("#err_qty").show();

                  $("#err_qty").text("Something Went wrong Please try again");
                  setTimeout(function(){
                    $("#myModal").hide();
                 window.location.reload();
              },2000);
              }

            }
          });



        }else if(isValidAmount && superGlobalRecharge == 0 && superGlobalRechargeRefund == 1 && superGlobalRechargededuct == 0 && superGlobalrefundReason != ''){


                  var userid=$("#user_customer_id").val();
                  var amount = $("#addpriceRefund").val();
                  var refund_reason = $("#getcategory").val();
                  if(superGlobalrefundReason == 99){
                    var refund_reason = $("#other_reason").val();
                  }

                  var r_r = superGlobalrefundReason;

                  $.ajax({
                    url: "addBalance",
                    cache: false,
                    data:{'notaddidbeforethisusergetfrommakeidmohan':userid,'amountnotneedtoberechargeonetime':amount,'r_r':r_r,'refund_reason': refund_reason},
                    success: function(response){
                      if(response==200){

                      $("#success_qty").show();
                      $("#success_qty").css('color:green');
                      $("#success_qty").text("Amount has been added successfully");
                      setTimeout(function(){
                        $("#myModal").hide();
                        window.location.reload();
                      },2000);

                      }else{
                          $("#err_qty").show();

                          $("#err_qty").text("Something Went wrong Please try again");
                          setTimeout(function(){
                            $("#myModal").hide();
                         window.location.reload();
                      },2000);
                      }

                    }
                  });

        }else if(isValidAmount && superGlobalRechargededuct == 1 && superGlobalRecharge == 0 && superGlobalRechargeRefund == 0 && superGlobaldeductReason != ''){


                  var userid=$("#user_customer_id").val();
                  var amount = $("#addpricededuct").val();
                  var d_r = superGlobaldeductReason;
                  var refund_reason = $("#getcategory1").val();
                  if(d_r == 99){
                    refund_reason = $("#other_reason").val();
                  }

                  $.ajax({
                    url: "addBalance",
                    cache: false,
                    data:{'notaddidbeforethisusergetfrommakeidmohan':userid,'amountnotneedtoberechargeonetime':amount,'d_r':d_r,'refund_reason': refund_reason},
                    success: function(response){
                      if(response==200){

                      $("#success_qty").show();
                      $("#success_qty").css('color:green');
                      $("#success_qty").text("Amount has been deducted successfully");
                      setTimeout(function(){
                        $("#myModal").hide();
                        window.location.reload();
                      },2000);

                      }else{
                          $("#err_qty").show();

                          $("#err_qty").text("Something Went wrong Please try again");
                          setTimeout(function(){
                            $("#myModal").hide();
                         window.location.reload();
                      },2000);
                      }

                    }
                  });

        }else{
          alert('Please select the values properly.');
          return false;
        }


     });



  $('input[type="radio"]').click(function(){

       if($(this).attr("value")=="rechargeableamount"){

             superGlobalRecharge        = 1;
             superGlobalRechargeRefund  = 0;
             superGlobalRechargededuct  = 0;

             $(".rechargeableamount").show();
             $(".refundableamount").hide();
             $(".deductableamount").hide();
        }
         if($(this).attr("value")=="refundableamount"){
             superGlobalRecharge        = 0;
             superGlobalRechargeRefund  = 1;
             superGlobalRechargededuct  = 0;
             $(".rechargeableamount").hide();
             $(".refundableamount").show();
             $(".deductableamount").hide();

        }

       if($(this).attr("value")=="deductableamount"){
             superGlobalRecharge        = 0;
             superGlobalRechargeRefund  = 0;
             superGlobalRechargededuct  = 1;
             $(".deductableamount").show();
             $(".refundableamount").hide();
             $(".rechargeableamount").hide();
        }

    });
    $('input[type="radio"]').trigger('click');



});

})($);
