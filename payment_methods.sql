-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2017 at 09:33 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 5.6.32-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cremeway_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL,
  `method` varchar(20) NOT NULL COMMENT 'coupon,paytm,payumoney',
  `status` int(1) NOT NULL DEFAULT '0',
  `merchant_key` varchar(100) NOT NULL,
  `merchant_mid` varchar(100) NOT NULL,
  `merchant_salt` varchar(100) NOT NULL,
  `merchant_website` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `method`, `status`, `merchant_key`, `merchant_mid`, `merchant_salt`, `merchant_website`, `description`, `created`, `modified`) VALUES
(1, 'coupon', 0, '', '', '', '', 'coupon', '2017-12-26 00:00:00', '2017-12-27 13:24:29'),
(2, 'paytm', 1, 'dsfdsf3w4', 'dsfdsf243r43', '', 'sdfdsf', 'sdfds', '2017-12-26 00:00:00', '2017-12-27 13:29:11'),
(3, 'payumoney', 0, 'hellosad223', '', '234rfewe', '', 'test', '2017-12-26 00:00:00', '2017-12-27 13:43:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
